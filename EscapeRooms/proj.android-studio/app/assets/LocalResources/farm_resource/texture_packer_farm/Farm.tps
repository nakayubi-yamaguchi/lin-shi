<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.3.1</string>
        <key>fileName</key>
        <string>/Users/yamaguchi/Desktop/Working/ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/AmusementPark.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../../../amusementpark_resource_3/atlas_p{n}.plist</filename>
            </struct>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>10</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/etc/room_image_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/banana.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/bear.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/bucket.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/coin.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/cup.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/fastpass.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/item_sup_25_correct.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/item_sup_25_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/lever.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/magichand.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/money.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/monkey.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/passport.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/photo.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/pinky_doll.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/popcorn.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/stamp.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/stamp_blue.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/stamp_green.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/stamp_red.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/star.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/stick.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/stick_anim.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/stick_anim_light.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/stick_light.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/stick_off.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/stick_on.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/stick_star.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/stick_star_light.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/ticket.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/usb.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/water.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/item/wrench.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>125,125,250,250</rect>
                <key>scale9Paddings</key>
                <rect>125,125,250,250</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/etc/room_image_clip.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>281,500,563,1000</rect>
                <key>scale9Paddings</key>
                <rect>281,500,563,1000</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/etc/title.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>150,38,300,75</rect>
                <key>scale9Paddings</key>
                <rect>150,38,300,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/back_13_bar.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_10_light.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_10_screen.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_10_usb.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_0_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_0_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_0_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_1_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_1_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_1_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_2_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_2_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_2_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_2_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_3_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_3_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_3_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_11_3_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_12_clip.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_12_clip_door.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_12_door.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_12_eye_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_12_eye_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_12_lid.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_13_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_13_bar.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_14_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_14_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_14_2_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_14_3_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_14_4_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_14_5_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_14_6_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_14_7_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_15_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_15_magichand.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_0_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_1_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_2_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_2_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_3_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_3_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_4_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_4_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_5_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_5_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_6_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_6_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_7_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_16_7_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_18_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_18_0_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_18_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_18_1_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_18_2_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_18_2_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_18_3_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_18_3_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_18_4_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_18_4_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_18_5_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_18_5_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_1_cart_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_1_cart_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_1_cart_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_1_cart_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_1_cart_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_1_cart_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_1_cart_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_1_cart_7.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_1_jetcoaster.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_1_saku.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_1_wheel.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_1_wheel_end.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_1_wheel_end_.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_23_balloon_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_23_balloon_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_23_balloon_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_23_balloon_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_23_banana.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_23_clip.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_23_coaster.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_23_pig.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_23_rail.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_24_cloud.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_24_pig_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_24_pig_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_24_pig_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_24_pig_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_24_pig_banana.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_24_popcorn.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_25_board.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_25_bucket_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_25_bucket_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_25_cover.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_25_hand.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_25_water.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_26_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_26_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_26_bar.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_26_board.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_26_coaster.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_26_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_26_open.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_26_rail.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_27_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_27_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_27_board.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_27_coaster.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_27_lever.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_27_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_27_open.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_27_rail.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_28_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_28_0_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_28_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_28_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_28_correct.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_28_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_2_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_2_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_2_balloon_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_2_balloon_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_2_balloon_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_2_balloon_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_2_bar.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_2_board.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_2_clip.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_2_coaster.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_2_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_2_open.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_2_pig.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_2_rail.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_30_board.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_30_bolt_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_30_bolt_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_30_wrench_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_30_wrench_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_31_balloon_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_31_balloon_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_31_jetcoaster.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_31_other.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_31_syuniku_open.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_32_balloon_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_32_balloon_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_32_balloon_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_32_jetcoaster.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_32_saku.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_33_balloon_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_33_balloon_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_33_balloon_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_33_balloon_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_33_clip.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_33_saku.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_33_syuniku_open.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_34_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_34_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_34_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_34_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_balloon_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_balloon_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_balloon_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_balloon_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_clip.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_cloud.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_hanko.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_pig_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_pig_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_pig_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_pig_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_pig_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_pig_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_saku.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_stamp_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_stamp_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_stampcard.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_syuniku.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_35_syuniku_open.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_3_coaster.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_3_cover.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_3_pig_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_3_pig_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_3_rail.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_3_sheet.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_40_coaster.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_40_cover.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_40_rail.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_40_sheet.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_41_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_41_star.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_42_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_42_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_42_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_43_cloud.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_43_pig_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_43_pig_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_43_pig_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_43_popcorn_case.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_44_cloud.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_44_pig_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_44_ticket.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_46_piero_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_46_piero_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_46_piero_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_46_pig_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_46_pig_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_46_pig_doll.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_46_ring_pattern.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_47_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_47_piero_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_47_piero_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_47_piero_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_47_piero_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_47_pig_doll.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_48_correct.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_48_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_48_piero_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_48_piero_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_49_piero_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_49_piero_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_4_alpha.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_4_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_4_pig_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_4_pig_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_4_pig_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_50_bucket_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_50_bucket_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_50_doll.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_50_pig_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_50_pig_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_50_pig_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_50_pig_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_50_ring.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_51_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_51_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_51_monkey.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_51_open.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_51_stamp.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_51_usb.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_0_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_0_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_1_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_1_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_2_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_2_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_2_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_3_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_3_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_3_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_4_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_4_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_53_4_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_55_banana_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_55_banana_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_55_banana_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_55_banana_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_55_clip.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_55_doll.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_55_lid.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_55_monkey_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_55_monkey_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_55_monkey_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_55_monkey_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_56_correct.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_56_hanko.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_56_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_56_pig_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_56_pig_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_56_pig_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_56_pig_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_56_pig_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_56_pig_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_56_stamp_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_56_stamp_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_56_stampcard.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_56_syuniku.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_56_syuniku_open.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_5_center.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_5_clip.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_5_correct.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_5_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_61_cloud.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_61_light.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_61_pig_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_61_pig_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_62_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_62_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_62_bear.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_62_conductor.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_62_conductor_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_62_lever1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_62_lever2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_62_stick.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_63_clip_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_63_clip_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_63_pass.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_63_ticket.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_0_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_0_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_1_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_1_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_2_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_2_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_2_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_3_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_3_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_3_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_4_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_4_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_4_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_5_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_5_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_5_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_6_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_6_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_64_6_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_66_cloud.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_66_pass.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_66_pig_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_0_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_0_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_0_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_1_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_1_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_1_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_2_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_2_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_2_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_2_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_3_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_3_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_3_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_67_3_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_69_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_69_dollar.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_69_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_69_stamp.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_6_cloud.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_6_coin.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_6_money.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_6_pig_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_70_correct.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_70_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_72_hanko.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_72_pig_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_72_pig_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_72_pig_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_72_pig_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_72_pig_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_72_pig_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_72_stamp_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_72_stamp_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_72_stampcard.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_72_syuniku.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_72_syuniku_open.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_75_stamp.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_0_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_0_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_0_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_0_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_0_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_1_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_1_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_1_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_1_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_2_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_2_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_2_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_2_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_2_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_2_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_3_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_3_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_3_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_3_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_3_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_3_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_4_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_4_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_4_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_4_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_4_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_4_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_5_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_5_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_5_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_5_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_5_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_5_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_card.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_cover.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_76_open.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_77_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_77_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_77_card.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_77_cup.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_77_wrench.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_0_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_0_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_0_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_0_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_0_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_1_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_1_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_1_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_1_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_1_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_2_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_2_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_2_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_2_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_2_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_2_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_3_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_3_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_3_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_3_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_3_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_3_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_4_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_4_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_4_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_4_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_4_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_4_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_5_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_5_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_5_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_5_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_5_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_5_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_cover_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_78_cover_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_0_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_0_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_0_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_0_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_0_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_1_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_1_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_1_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_1_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_1_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_2_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_2_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_2_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_2_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_2_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_2_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_3_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_3_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_3_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_3_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_3_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_3_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_4_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_4_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_4_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_4_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_4_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_4_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_5_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_5_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_5_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_5_4.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_5_5.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_5_6.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_camera.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_camera_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_camera_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_cheki.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_cheki_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_cheki_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_correct.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_cover.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_mistake.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_79_open.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_7_cloud.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_7_money.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_7_pig_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_7_pig_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_7_pig_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_0_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_0_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_1_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_1_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_2_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_2_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_2_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_3_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_3_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_3_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_4_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_4_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_80_4_3.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_81_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_81_0_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_81_1_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_81_1_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_81_2_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_81_2_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_81_3_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_81_3_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_9_0_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_9_arm.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_9_blue.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_9_bucket.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_9_close.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_9_light_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_9_light_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_9_open.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_9_pig_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_9_pig_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_9_pink.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_9_usb.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>150,165,300,330</rect>
                <key>scale9Paddings</key>
                <rect>150,165,300,330</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/screem_0.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/screem_1.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/screem_2.png</key>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/screem_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,38,175,75</rect>
                <key>scale9Paddings</key>
                <rect>88,38,175,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../ローカルリポジトリ_EscapeRooms/amusementpark_resource/texture_packer_amusementpark/supplements/sup_5_arm_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>128,192,256,384</rect>
                <key>scale9Paddings</key>
                <rect>128,192,256,384</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>item</filename>
            <filename>supplements</filename>
            <filename>etc</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
