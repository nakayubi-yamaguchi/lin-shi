LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos/audio/include)

LOCAL_MODULE := MyGame_shared

LOCAL_MODULE_FILENAME := libMyGame

FILE_LIST := $(wildcard $(LOCAL_PATH)/../../../Classes/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../../Classes/Utils/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../../Classes/UIParts/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../../Classes/Adfurikun/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../../Classes/Adjust/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../../Classes/AdX/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../../Classes/Fluct/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../../Classes/FluctAdX/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../../Classes/RewardPlayer/*.cpp)


LOCAL_SRC_FILES := hellocpp/main.cpp\
                   $(FILE_LIST:$(LOCAL_PATH)/%=%)


INCLUDE_PATH := $(LOCAL_PATH)/../../../Classes
INCLUDE_PATH += $(LOCAL_PATH)/../../../Classes/Utils
INCLUDE_PATH += $(LOCAL_PATH)/../../../Classes/Fluct
INCLUDE_PATH += $(LOCAL_PATH)/../../../Classes/AdX
INCLUDE_PATH += $(LOCAL_PATH)/../../../Classes/RewardPlayer
INCLUDE_PATH += $(LOCAL_PATH)/../../../Classes/FluctAdX

LOCAL_C_INCLUDES := $(INCLUDE_PATH)

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END

