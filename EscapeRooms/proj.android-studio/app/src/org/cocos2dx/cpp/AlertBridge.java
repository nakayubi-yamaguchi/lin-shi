package org.cocos2dx.cpp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;

import org.cocos2dx.lib.Cocos2dxActivity;

/**
 * Created by yamaguchinarita on 2016/08/09.
 */
public class AlertBridge {

    private static final String LOG_TAG = "Test_AlertBridge";
    private static Cocos2dxActivity mActivity;

    //アラート
    static final int BTN_ALERT_NONE = -1;
    static final int BTN_ALERT_POSITIVE = 0;
    static final int BTN_ALERT_NEUTRAL = 1;
    static final int BTN_ALERT_NEGATIVE = 2;
    public static native void alertClosedCPP(int tag,int num);

    public AlertBridge(Cocos2dxActivity activity) {mActivity = activity;}

    //アラート
    static public void showAlert(final int tag, final String title, final String msg,
                                 final String negativeBtn, final String positiveBtn, final String neutralBtn) {

        log("アラート表示するよー");

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                log("メインスレッド内");
                alert(tag, title, msg, negativeBtn, positiveBtn, neutralBtn);
            }
        });
    }

    static public void alert(final int alertId, String title, String msg, String positiveBtn, String neutralBtn,String negativeBtn) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setCancelable(false);
        if (positiveBtn != null) {
            alertDialogBuilder.setPositiveButton(positiveBtn, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    if (arg1 == DialogInterface.BUTTON_POSITIVE) {
                        mActivity.runOnGLThread(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        alertClosedCPP(alertId, BTN_ALERT_POSITIVE);
                                    }
                                }
                        );
                    }
                }});
        }
        if (neutralBtn != null) {
            alertDialogBuilder.setNeutralButton(neutralBtn, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    if (arg1 == DialogInterface.BUTTON_NEUTRAL)
                    {
                        mActivity.runOnGLThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        alertClosedCPP(alertId, BTN_ALERT_NEUTRAL);
                                                    }
                                                });
                    }
                }});
        }
        alertDialogBuilder.setNegativeButton(negativeBtn, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                if (arg1 == DialogInterface.BUTTON_NEGATIVE) {

                    mActivity.runOnGLThread(new Runnable() {
                        @Override
                        public void run() {
                            alertClosedCPP(alertId, BTN_ALERT_NEGATIVE);
                        }
                    });
                }
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private static void log(String message){
        Log.d(LOG_TAG, message);
    }
}
