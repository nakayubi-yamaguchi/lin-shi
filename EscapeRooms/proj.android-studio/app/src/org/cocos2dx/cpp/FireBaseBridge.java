package org.cocos2dx.cpp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageException;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StreamDownloadTask;
import com.ryohei.haruki.EscapeRooms.R;

import org.cocos2dx.lib.BuildConfig;
import org.cocos2dx.lib.Cocos2dxActivity;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


/**
 * Created by yamaguchinarita on 2016/07/20.
 */
public class FireBaseBridge {


    private static final String LOG_TAG = "Firebase_Test";

    private static Cocos2dxActivity mActivity;

    //firebase
    private static FirebaseAnalytics mFirebaseAnalytics;
    private static FirebaseDatabase mDatabase;
    private static StorageReference mStorage;
    private static FirebaseRemoteConfig mFirebaseRemoteConfig;

    private static boolean isConnected;
    private static double m_timestamp;
    private static double m_time_offset;
    static int iconCount=0;

    private static Map<String, ValueEventListener> listenerMap = new HashMap<String ,ValueEventListener>();//ひとまずはmapの読み込みのみに対応

    //Java->C側のメソッド呼び出し

    public static native void gotAlerts(String[] urls,String[] messages,boolean[] isNews);
    public static native void clickHouseAlertUrl(boolean selected);
    public static native void gotIcons(String[] urls,byte[][] bytes,boolean[] isNews);
    public static native void gotStringValue(String string,String key);
    public static native void gotIntValue(int intValue, String key,boolean successed);
    public static native void gotMapValue(String[] keys, long[] values, String key, boolean successed);
    public static native void gotValue(Object valueObject, String key, boolean successed);


    public static native void finishedTransaction(String key, boolean successed);
    public static native void gotURL(String url, String fileName, boolean successed);
    public static native void gotFile(byte[] bytes, String fileName, boolean successed, int errorcode, String errorDescription);
    public static native void completeFetch(int type,String key);


    public FireBaseBridge(Cocos2dxActivity activity)
    {
        mActivity = activity;
    }

    //firebase
    static public void setUpFirebase(String strURL)
    {
        //アナリティクス
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mActivity);
        //データベース
        mDatabase = FirebaseDatabase.getInstance();
        //ストレージ
        mStorage= FirebaseStorage.getInstance().getReferenceFromUrl(strURL);
    }

    //自社アラート
    static  public void loadingAlertHouseAd()
    {
        Log.d(LOG_TAG,"アラートロード開始");
        mDatabase.getReference("Alerts").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int count=(int)dataSnapshot.getChildrenCount();

                final String messages[] = new String[count];
                final String urls[] = new String[count];
                final boolean isNews[]=new boolean[count];

                //言語設定判別
                Locale locale=Locale.getDefault();
                String key;
                if (Locale.JAPAN.equals(locale)){
                    key="message";
                }
                else{
                    key="message_en";
                }

                int i=0;//格納数カウンタ

                for (DataSnapshot child : dataSnapshot.getChildren())
                {//取得情報を処理
                    String url=child.child("androidUrl").getValue(String.class);
                    if (url!=null) {//android対応しているもののみ
                        String message = child.child(key).getValue(String.class);

                        if (message!=null){//言語対応しているもののみ
                            String id=child.child("bundleID").getValue(String.class);
                            if (!id.equals(mActivity.getPackageName())){//自分は出さない
                                messages[i]=message;
                                urls[i]=url;
                                isNews[i]=child.child("isNew").getValue(boolean.class);
                                i++;
                            }
                        }
                    }
                }

                //要素数を変更
                final String _messages[] = new String[i];
                final String _urls[] = new String[i];
                final boolean _isNews[]=new boolean[i];

                int y=0;

                while (y<i){
                    _messages[y]=messages[y];
                    _urls[y]=urls[y];
                    _isNews[y]=isNews[y];
                    y++;
                }

                //callback
                mActivity.runOnGLThread(new Runnable() {//GLthreadで実行
                    @Override
                    public void run() {
                        gotAlerts(_urls, _messages,_isNews);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(LOG_TAG, "Failed to read value.", error.toException());
            }
        });
    }


    public static void showHouseAlert(String message){
        final String m=message;
        mActivity.runOnUiThread(new Runnable() {//UI threadで実行 じゃないと起動時呼んだら落ちます
            @Override
            public void run() {
                AlertDialog.Builder alert = new AlertDialog.Builder(mActivity);
                alert.setTitle("");
                alert.setMessage(m);
                alert.setCancelable(false);
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clickHouseAlertUrl(true);
                    }
                });

                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clickHouseAlertUrl(false);
                    }
                });
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
            }
        });
    }

    //自社アイコン
    public static void loadingIconHouseAd(){
        Log.d(LOG_TAG,"アイコンロード開始");
        mDatabase.getReference("Icons").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int count=(int)dataSnapshot.getChildrenCount();
                //検索成功時の処理
                final String fileNames[] = new String[count];
                final String urls[] = new String[count];
                final boolean isNews[]=new boolean[count];

                for (DataSnapshot child : dataSnapshot.getChildren())
                {//取得情報を処理
                    String url=child.child("androidUrl").getValue(String.class);
                    if (url!=null) {//android対応しているもののみ
                        String fileName = child.child("fileName").getValue(String.class);
                        String id=child.child("bundleID").getValue(String.class);

                        if (!id.equals(mActivity.getPackageName())){//自分は出さない
                            String type=child.child("type").getValue(String.class);
                            if (!type.equals("util"))//注 type指定
                            {
                                fileNames[iconCount]=fileName;
                                urls[iconCount]=url;
                                isNews[iconCount]=child.child("isNew").getValue(boolean.class);
                                iconCount++;
                            }
                        }
                    }
                }

                //要素数を格納数に合わせる
                final String _fileNames[] = new String[iconCount];
                final String _urls[] = new String[iconCount];
                final boolean _isNews[]=new boolean[iconCount];
                final byte[][] _bytes=new byte[iconCount][];

                int y=0;

                while (y<iconCount){
                    _fileNames[y]=fileNames[y];
                    _urls[y]=urls[y];
                    _isNews[y]=isNews[y];

                    Log.d(LOG_TAG,_fileNames[y]);
                    y++;
                }

                //fileNameの格納終了。
                //データ取得開始
                iconCount=0;//一旦リセット
                final long ONE_MEGA_BYTE=1024*1024;
                for (int i=0;i<y;i++){

                    Log.d(LOG_TAG,"ファイルを読み込みます。"+ i);
                    StorageReference filePathRef=mStorage.child(fileNames[i]);

                    final int _i=i;

                    filePathRef.getBytes(ONE_MEGA_BYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            Log.d(LOG_TAG,fileNames[_i]+"Success");

                            _bytes[_i]=bytes;
                            iconCount++;

                            if (_bytes.length==iconCount){
                                //格納終了
                                Log.d(LOG_TAG,"格納終了");

                                mActivity.runOnGLThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        gotIcons(_urls,_bytes,_isNews);
                                    }
                                });
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d(LOG_TAG,fileNames[_i]+"Failed download");
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(LOG_TAG,"アイコン取得失敗");
            }
        });
    }

    //Analytics
    public static void analyticsWithName(String eventName){
        mFirebaseAnalytics.logEvent(eventName,null);
        Log.d(LOG_TAG,"イベント登録"+eventName);
    }

    public static void analyticsWithParameters(String eventName,String[] keys,String[] values){

        Bundle params = new Bundle();
        for (int i=0;i<keys.length;i++){
            params.putString(keys[i],values[i]);
            Log.d(LOG_TAG,eventName+"イベント登録パラメータ"+eventName+keys[i]+":"+values[i]);

        }
        mFirebaseAnalytics.logEvent(eventName,params);
    }

    //任意の型取得
    public static void loadingStringValue(final String key)
    {
        Log.d(LOG_TAG,"string型とるで"+key);
        mDatabase.getReference(key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String string=dataSnapshot.getValue().toString();
                Log.d(LOG_TAG,"string型取得"+string);

                //callback
                mActivity.runOnGLThread(new Runnable() {//GLthreadで実行
                    @Override
                    public void run() {
                        gotStringValue(string,key);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void checkConnected()
    {
        final DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                final boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    System.out.println("connected");
                } else {
                    System.out.println("not connected");
                }

                log("firebaseの接続する状態が変更しました。connected:::"+connected);

                isConnected = connected;
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.err.println("Listener was cancelled");
            }
        });
    }

    public static boolean getIsConnected()
    {
        log("firebase接続状況"+isConnected);

        return isConnected;
    }

    public static void loadingIntValue(final String key)
    {
        Log.d(LOG_TAG,"int型とるで"+key);
        mDatabase.getReference(key).addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {


                final int intValue = (dataSnapshot.getValue()==null)? 0 : dataSnapshot.getValue(int.class);
                Log.d(LOG_TAG,"int型取得"+intValue);

                //callback
                mActivity.runOnGLThread(new Runnable() {//GLthreadで実行
                    @Override
                    public void run() {
                        gotIntValue(intValue, key, dataSnapshot.getValue()!=null);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //callback
                Log.d(LOG_TAG,"int型取");

                mActivity.runOnGLThread(new Runnable() {//GLthreadで実行
                    @Override
                    public void run() {
                        gotIntValue(0, key, false);
                    }
                });
            }
        });


        Log.d(LOG_TAG,"int型とるでで"+mDatabase.getReference().toString());

    }

    public static void loadingMapValue(final String key)
    {
        ValueEventListener valueEvent = new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {

                log("mapデータを取得しました。"+dataSnapshot);


                //log("読み取りマップは"+listenerMap);
                //読み込みが終わったら、保持しない
                if (listenerMap.containsKey(key))
                    listenerMap.remove(key);


                int count = (int) dataSnapshot.getChildrenCount();
                //検索成功時の処理
                final String _keys[] = new String[count];
                final long _values[] = new long[count];

                int i = 0;
                for (DataSnapshot child : dataSnapshot.getChildren()) {//取得情報を処理

                    log("チャイルドです"+child);
                    log("チャイルドのキー" + child.getKey());
                    log("チャイルドのバリュー" + child.getValue());

                    long value;

                    //mapのバリューはintかboolを想定
                    if (child.getValue() instanceof Boolean){
                        log("チャイルドの型はboolです。");
                        value = child.getValue(boolean.class) ? 1 : 0;
                    }
                    else {//int型
                        log("チャイルドの型はintです");
                        value = child.getValue(long.class);
                    }


                    _keys[i] = child.getKey();
                    _values[i] = value;
                    i++;
                }

                //callback
                mActivity.runOnGLThread(new Runnable() {//GLthreadで実行
                    @Override
                    public void run() {
                        gotMapValue(_keys, _values, key, !(dataSnapshot.getValue() == null));
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //callback
                final String _keys[] = new String[0];
                final long _values[] = new long[0];

                log("読み取りキャンセルマップは"+listenerMap);

                //読み込みが終わったら、保持しない
                if (listenerMap.containsKey(key))
                    listenerMap.remove(key);
                mActivity.runOnGLThread(new Runnable() {//GLthreadで実行
                    @Override
                    public void run() {
                        gotMapValue(_keys, _values, key, false);
                    }
                });
            }
        };

        log("読み込んだキーは"+key);
        mDatabase.getReference(key).addListenerForSingleValueEvent(valueEvent);

        if (listenerMap.containsKey(key)) {
            log("すでに存在しているリスナーを再度保持している！！！！");
        }

        listenerMap.put(key, valueEvent);

        log("追加時マップは"+listenerMap);
    }

    public static void loadingValue(final String key)
    {
        ValueEventListener valueEvent = new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {

                log("mapデータを取得しました。"+dataSnapshot.getValue());

                //log("読み取りマップは"+listenerMap);
                //読み込みが終わったら、保持しない
                if (listenerMap.containsKey(key))
                    listenerMap.remove(key);


                /*
                int count = (int) dataSnapshot.getChildrenCount();
                //検索成功時の処理
                final String _keys[] = new String[count];
                final long _values[] = new long[count];


                int i = 0;

                for (DataSnapshot child : dataSnapshot.getChildren()) {//取得情報を処理

                    log("チャイルドです"+child);
                    log("チャイルドのキー" + child.getKey());
                    log("チャイルドのバリュー" + child.getValue());

                    long value;

                    //mapのバリューはintかboolを想定
                    if (child.getValue() instanceof Boolean){
                        log("チャイルドの型はboolです。");
                        value = child.getValue(boolean.class) ? 1 : 0;
                    }
                    else {//int型
                        log("チャイルドの型はintです");
                        value = child.getValue(long.class);
                    }


                    _keys[i] = child.getKey();
                    _values[i] = value;
                    i++;
                }
                */

                for (DataSnapshot child : dataSnapshot.getChildren()) {//取得情報を処理


                    log("マップのクラスは"+child.getValue().getClass().getName());

                }
                //callback
                mActivity.runOnGLThread(new Runnable() {//GLthreadで実行
                    @Override
                    public void run() {

                        boolean success = (dataSnapshot.getValue() != null);


                        log("成功しているかどうか"+dataSnapshot.getValue().getClass().getName());

                        gotValue(dataSnapshot.getValue(), key, success);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                //読み込みが終わったら、保持しない
                if (listenerMap.containsKey(key))
                    listenerMap.remove(key);
                mActivity.runOnGLThread(new Runnable() {//GLthreadで実行
                    @Override
                    public void run() {
                        gotValue(null, key, false);
                    }
                });
            }
        };

        mDatabase.getReference(key).addListenerForSingleValueEvent(valueEvent);

        if (listenerMap.containsKey(key)) {
            log("すでに存在しているリスナーを再度保持している！！！！");
        }

        listenerMap.put(key, valueEvent);

    }

    /*
    private static Object transformToObject(DataSnapshot dataSnapshot)
    {
        Object object;
        if (dataSnapshot.getValue() instanceof Integer || dataSnapshot.getValue() instanceof String) {
            object = dataSnapshot.getValue();
        }
        else if (dataSnapshot.getValue() instanceof Array) {
            log("現状、配列の型が取得されることはないはず。これ呼ばれたら、おかしい。");

        }
        else if (dataSnapshot.getValue() instanceof Map) {

            int count = (int) dataSnapshot.getChildrenCount();
            //検索成功時の処理
            final String _keys[] = new String[count];
            final Object _values[] = new Object[count];

            int i = 0;

            for (DataSnapshot child : dataSnapshot.getChildren()) {//取得情報を処理

                //mapのバリューはintかboolを想定
                if (child.getValue() instanceof Boolean){
                    log("チャイルドの型はboolです。");
                    value = child.getValue(boolean.class) ? 1 : 0;
                }
                else {//int型
                    log("チャイルドの型はintです");
                    value = child.getValue(long.class);
                }


                _keys[i] = child.getKey();
                _values[i] = value;
                i++;
            }

        }
        else {
            log("上記以外の型が取得されました。");
        }

        return object;
    }*/

    public static void setValue(final String childKey, final String[] keys, final String[] values, final String callbackKey)
    {
        Map<String, Object> map = new HashMap<>();

        int i = 0;
        for (String key : keys) {
            map.put(key, values[i]);

            i++;
        }

        mDatabase.getReference(childKey).setValue(map, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                finishedTransaction(callbackKey, databaseError == null);

                log("データのセット完了しました。"+(databaseError == null));
            }
        });
    }


    public static void transactionPostWithIncrement(final String childKey, final String[] incrementKeys, final String[] decrementKeys, final String callbackKey) {

        log("アップデートメソッド"+childKey);

        mDatabase.getReference(childKey).runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {

                log("現在のデータ"+mutableData);

                /*
                if (mutableData.getValue() == null) {
                    return Transaction.success(mutableData);
                }*/

                Map<String, Object> map = new HashMap<>();

                for (MutableData data : mutableData.getChildren())
                {
                    map.put(data.getKey(), data.getValue());
                }

                log("現在のマップ"+map);

                for (String key : incrementKeys)
                {
                    log("キーは"+ key + "ゲットバリュー" + mutableData.getValue());

                    Object increValue = map.get(key);

                    int increNum = (increValue ==null) ? 0 : new Integer(increValue.toString());
                    increNum ++;

                    map.put(key, increNum);
                }


                for (String key : decrementKeys)
                {
                    Object decreValue = map.get(key);
                    int decreNum = (decreValue ==null) ? 0 :new Integer(decreValue.toString());
                    decreNum --;

                    map.put(key, decreNum);
                }

                log("書き換え後のマップデータ"+map);

                // Set value and report transaction success
                mutableData.setValue(map);

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(final DatabaseError databaseError, final boolean committed,
                                   DataSnapshot dataSnapshot) {
                // Transaction completed
                mActivity.runOnGLThread(new Runnable() {//GLthreadで実行
                    @Override
                    public void run() {
                        finishedTransaction(callbackKey, committed);
                    }
                });
            }
        });
    }

    public static void transactionPost(final String childKey, final String[] incrementKeys, final String[] incrementValues, final String callbackKey)
    {
        mDatabase.getReference(childKey).runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {

                log("現在のデータ"+mutableData);

                Map<String, Object> map = new HashMap<>();

                //サーバー上の既存データをmapに変換
                for (MutableData data : mutableData.getChildren()) {
                    map.put(data.getKey(), data.getValue());
                }

                log("現在のマップ"+map);

                //サーバー上にセットするデータに変換する
                int i = 0;
                for (String key : incrementKeys) {
                    log("キーは"+ key + "ゲットバリュー" + mutableData.getValue());

                    Object increValue = map.get(key);

                    //サーバー上に存在しないデータなら、0。あるなら格納。
                    int increNum = (increValue == null) ? 0 : Integer.valueOf(increValue.toString());
                    increNum += Integer.valueOf(incrementValues[i]);

                    map.put(key, increNum);

                    i++;
                }

                log("トランザクション書き換え後のマップデータ"+map);

                // Set value and report transaction success
                mutableData.setValue(map);

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(final DatabaseError databaseError, final boolean committed,
                                   DataSnapshot dataSnapshot) {
                // Transaction completed
                mActivity.runOnGLThread(new Runnable() {//GLthreadで実行
                    @Override
                    public void run() {
                        finishedTransaction(callbackKey, committed);
                    }
                });
            }
        });
    }

    public static void getURL(final String downloadFile)
    {
        Log.d(LOG_TAG,"urlをとるで"+downloadFile);
        StorageReference filePathRef=mStorage.child(downloadFile);

        filePathRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(final Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                //callback
                log("url取得完了"+uri);
                mActivity.runOnGLThread(new Runnable() {//GLthreadで実行
                    @Override
                    public void run() {
                        if (uri == null) {
                            gotURL("", downloadFile, false);
                        }
                        else {
                            gotURL(uri.toString(), downloadFile, true);
                        }
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
                StorageException ex = (StorageException)exception;
                int errorCode = ex.getErrorCode();

                log(errorCode+"url取得失敗"+ex.getMessage()+"全てのエラーは"+ex);

                mActivity.runOnGLThread(new Runnable() {//GLthreadで実行
                    @Override
                    public void run() {
                        gotURL("",downloadFile, false);
                    }
                });
            }
        });
    }

    public static void removeAllObserver(final String keyName)
    {
        log(keyName+"の全ての読み取りを停止させる"+listenerMap.containsKey(keyName));

        //未完成：よくわからないけどうまくいっていない。削除した後に、loadingMapのコールバック呼ばれている.
        if (listenerMap.containsKey(keyName)) {

            log(keyName+"の読み取りを停止させる"+listenerMap.get(keyName));

            mDatabase.getReference(keyName).removeEventListener(listenerMap.get(keyName));
            listenerMap.remove(keyName);

        }
    }

    public static void downloadFile_test(final String downloadFile) {
        final StorageReference strageRef = mStorage.child(downloadFile);

        strageRef.getStream().addOnProgressListener(new OnProgressListener<StreamDownloadTask.TaskSnapshot>() {
            @Override
            public void onProgress(StreamDownloadTask.TaskSnapshot taskSnapshot) {
                //I used Log.v() method to print the amount of bytes on Logcat
                Log.v("DLは進捗", taskSnapshot.getBytesTransferred() + " bytes transferred");
            }
        })
                .addOnCompleteListener(new OnCompleteListener<StreamDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<StreamDownloadTask.TaskSnapshot> task) {
                        log(" DLは完了しました");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        log(" DLは失敗しました"+e);

                    }
                });
    }

    public static void downloadFile(final String downloadFile) throws IOException {

        log("DLは開始"+downloadFile);
        final StorageReference strageRef = mStorage.child(downloadFile);

        final long ONE_MEGABYTE = 1024 * 1024;

        strageRef.getBytes(ONE_MEGABYTE*50).addOnCompleteListener(new OnCompleteListener<byte[]>() {
            @Override
            public void onComplete(@NonNull Task<byte[]> task) {

                final boolean successed = task.isSuccessful();

                if (!successed) {
                    //gotFile(new byte[0], downloadFile, false);
                    return;
                }

                final byte[] bytes = task.getResult();
                log("DLはダウンロードしたバイトは"+bytes+"長さは"+bytes.length);

                if (bytes.length>0){

                    mActivity.runOnGLThread(new Runnable() {
                        @Override
                        public void run() {
                            gotFile(bytes, downloadFile, true, 0, "");
                        }
                    });
                }
                else{//エラー時は架空のバイトを返す
                    mActivity.runOnGLThread(new Runnable() {
                        @Override
                        public void run() {
                            gotFile(new byte[0], downloadFile, false, 0, "取得したデータが破損しています。");
                        }
                    });
                }
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {


                log("エラーの種類"+
                        StorageException.ERROR_UNKNOWN+","+
                        StorageException.ERROR_OBJECT_NOT_FOUND+","+
                        StorageException.ERROR_BUCKET_NOT_FOUND+","+
                        StorageException.ERROR_PROJECT_NOT_FOUND+","+
                        StorageException.ERROR_QUOTA_EXCEEDED+","+
                        StorageException.ERROR_NOT_AUTHENTICATED+","+
                        StorageException.ERROR_NOT_AUTHORIZED+","+
                        StorageException.ERROR_RETRY_LIMIT_EXCEEDED+","+
                        StorageException.ERROR_INVALID_CHECKSUM+","+
                        StorageException.ERROR_CANCELED);

                final Exception exception = e;
                final int errorCode = ((StorageException) exception).getErrorCode();

                mActivity.runOnGLThread(new Runnable() {
                    @Override
                    public void run() {
                        gotFile(new byte[0], downloadFile, false, errorCode, exception.getLocalizedMessage());
                    }
                });
            }
        });
    }

    public static void loadingTimestamp(final boolean keepSync)
    {
        log("比較用");

        DatabaseReference offsetRef = FirebaseDatabase.getInstance().getReference(".info/serverTimeOffset");
        offsetRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                double offset = snapshot.getValue(Double.class);
                double estimatedServerTimeMs = System.currentTimeMillis() + offset;


                m_timestamp = estimatedServerTimeMs/1000;
                log("比較用"+m_timestamp);

                if (keepSync) {
                    m_time_offset = System.currentTimeMillis();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.err.println("Listener was cancelled");
            }
        });
    }

    public static double getServerTimestamp()
    {
        double diff = 0;
        if (m_time_offset > 0) {//同期対象
            double now = System.currentTimeMillis();
            diff = (now-m_time_offset)/1000;//タイムスタンプ読み取り完了からの経過時間
        }
        log("差分は"+diff);

        double server_date = m_timestamp + diff;
        log("サーバータイムスタンプ"+server_date);

        return server_date;
    }



    public static void log(String text){
        Log.d(LOG_TAG, text);
    }



    public static void initRemoteConfig(boolean debugMode)
    {
        log("分析 initRomoteConfig");
        //シングルトンの取得
        mFirebaseRemoteConfig=FirebaseRemoteConfig.getInstance();

        // Create a Remote Config Setting to enable developer mode, which you can use to increase
        // the number of fetches available per hour during development. See Best Practices in the
        // README for more information.
        // [START enable_dev_mode]
        if (debugMode)
        {
            log("分析 Debug Mode RemoteConfig=true");
            FirebaseRemoteConfigSettings configSettings= new FirebaseRemoteConfigSettings.Builder()
                    .setDeveloperModeEnabled(BuildConfig.DEBUG)
                    .build();
            mFirebaseRemoteConfig.setConfigSettings(configSettings);
        }

        // [END enable_dev_mode]

        // Set default Remote Config parameter values. An app uses the in-app default values, and
        // when you need to adjust those defaults, you set an updated value for only the values you
        // want to change in the Firebase console. See Best Practices in the README for more
        // information.
        // [START set_default_values]
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
        // [END set_default_values]

        //Log.d("分析:IID_TOKEN", FirebaseInstanceId.getInstance().getToken());
    }

    public static void fetchRemoteConfig(final String key)
    {
        log("分析 : Remote Config Key: "+key);


        //これセットで頻繁にfetchすることがなくなる
        long cacheExpiration = 3600*24;
        // If your app is using developer mode, cacheExpiration is set to 0, so each fetch will
        // retrieve values from the service.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            log("分析:debug用のexpirationをセット");
            cacheExpiration = 60;
        }

        //remote configから値を取得する
        mFirebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener(mActivity, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    log("分析 fetch成功 activate前"+(int)mFirebaseRemoteConfig.getDouble(key));

                    // After config data is successfully fetched, it must be activated before newly fetched
                    // values are returned.
                    mFirebaseRemoteConfig.activateFetched();

                    log("分析 fetch成功"+(int)mFirebaseRemoteConfig.getDouble(key));
                }
                else{
                    log("分析 fetchに失敗");
                }

                mActivity.runOnGLThread(new Runnable() {
                    @Override
                    public void run() {
                        completeFetch((int)mFirebaseRemoteConfig.getDouble(key),key);
                    }
                });
            }
        });
    }
}
