package org.cocos2dx.cpp;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;

import org.cocos2dx.lib.Cocos2dxActivity;

import java.util.Calendar;

/**
 * Created by yamaguchinarita on 2016/06/17.
 */
public class NotificationBridge {

    private static final String LOG_TAG = "Test_Notification";
    private static Cocos2dxActivity mActivity;

    public NotificationBridge(Cocos2dxActivity activity) {mActivity = activity;}

    //通知
    public static void fireLocalNotification(String message, int interval, int requestCode) {
        //requestCodeを使ってそれぞれを区別する
        Log.v(LOG_TAG, interval+"秒後に通知発行 fireLocalNotification"+"requestCode : "+requestCode);
        PendingIntent sender = getPendingIntent(message, requestCode);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, interval);

        AlarmManager am = (AlarmManager)mActivity.getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
    }

    public static void cancelLocalNotification(int requestCode) {
        Log.v(LOG_TAG, "通知をキャンセル cancelLocalNotification"+requestCode);
        PendingIntent sender = getPendingIntent(null, requestCode);
        AlarmManager am = (AlarmManager)mActivity.getSystemService(Context.ALARM_SERVICE);
        am.cancel(sender);
    }

    private static PendingIntent getPendingIntent(String message, int tag) {

        Intent i = new Intent(mActivity.getApplicationContext(), LocalNotificationReceiver.class);
        i.putExtra("notification_id", tag);
        i.putExtra("message", message);
        PendingIntent sender = PendingIntent.getBroadcast(mActivity, tag, i, PendingIntent.FLAG_UPDATE_CURRENT);
        return sender;
    }

    // 登録されているローカル通知をすべて削除する
    public static void cancelAllLocalNotification(int min,int max) {
        Log.v(LOG_TAG,"通知を削除 cancelAllLocalNotification");
        for (int requestCode = min; requestCode <= max; ++requestCode) {
            cancelLocalNotification(requestCode);
        }
    }

    public static void subscribeToTopic(String[] topics) {

        final String[] f_topics = topics;
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i<f_topics.length;i++){
                    String topic = f_topics[i];
                    subscribeToTopic(topic);
                }
            }
        });
    }


    public static void subscribeToTopic(String topic) {
        FirebaseMessaging.getInstance().subscribeToTopic(topic);
    }
}
