package org.cocos2dx.cpp;

import android.util.Log;

import org.cocos2dx.lib.Cocos2dxActivity;

import jp.fluct.fluctsdk.FluctErrorCode;
import jp.fluct.fluctsdk.FluctRewardedVideo;

public class FluctRewardBridge {

    private static final String LOG_TAG = "FluctRewardBridge";

    private static Cocos2dxActivity mActivity;
    private static FluctRewardedVideo mRewardedVideo;
    private static String mGroupID;
    private static String mUnitID;
    private static boolean prepared;
    private static boolean completed;

    public static native void didStart();
    public static native void didClose(boolean completed);

    public FluctRewardBridge(Cocos2dxActivity activity) {
        mActivity = activity;
        log("初期化します");
    }

    public static void loadFluctReward(final String groupID, final String unitID) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                createFluctReward(groupID, unitID);
                mRewardedVideo.loadAd();
            }
        });
    }

    public static boolean isReady() {
        return prepared;
    }

    public static void show() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mRewardedVideo.isAdLoaded()) {
                    mRewardedVideo.show();
                }
            }
        });
    }

    private static void createFluctReward(String groupID, String unitID) {
        mGroupID = groupID;
        mUnitID = unitID;

        mRewardedVideo = FluctRewardedVideo.getInstance(groupID, unitID, mActivity);
        mRewardedVideo.setListener(new FluctRewardedVideo.Listener() {
            @Override
            public void onShouldReward(String groupID, String unitID) {
                completed = true;
                log("onShouldReward");
            }

            @Override
            public void onLoaded(String groupID, String unitID) {
                prepared = true;
                log("onLoaded");
            }

            @Override
            public void onFailedToLoad(String groupID, String unitID, FluctErrorCode fluctErrorCode) {
                log("onFailedToLoad");
            }

            @Override
            public void onOpened(String groupID, String unitID) {
                log("onOpened");
            }

            @Override
            public void onStarted(String s, String s1) {
                log("onStarted");
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        didStart();
                    }
                });

            }

            @Override
            public void onFailedToPlay(String groupID, String unitID, FluctErrorCode fluctErrorCode) {
                log("onFailedToPlay");
            }

            @Override
            public void onClosed(String s, String s1) {
                final boolean mCompleted = completed;
                prepared = false;
                completed = false;
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        didClose(mCompleted);
                    }
                });
            }
        });
    }

    private static void log(String text) {
        Log.d(LOG_TAG, text);
    }

}
