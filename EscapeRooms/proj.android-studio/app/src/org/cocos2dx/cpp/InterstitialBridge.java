package org.cocos2dx.cpp;

import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;

import org.cocos2dx.lib.Cocos2dxActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yamaguchinarita on 2016/06/15.
 */
public class InterstitialBridge {

    private static final String LOG_TAG = "Test_Interstitial";

    private static Cocos2dxActivity mActivity;

    private static Map<String, PublisherInterstitialAd> mAdmobMap;
    private static Map<String, Boolean> mIsReadyMap;

    public static native void didClose();


    public InterstitialBridge(Cocos2dxActivity activity) {
        mActivity = activity;
        mAdmobMap = new HashMap<>();
        mIsReadyMap = new HashMap<>();
    }

    //AdmobIS関連
    public static void loadingInterstitial(final String appId) {

        Log.d(LOG_TAG, "アドモブのインターステイシャルの読み込みを開始");
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final PublisherInterstitialAd interstitialAd = new PublisherInterstitialAd(mActivity);
                interstitialAd.setAdUnitId(appId);
                interstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        log("interstitial is loaded.["+interstitialAd.getAdUnitId()+"]");
                        log("ローディング状況"+interstitialAd.isLoaded());

                        //準備完了
                        mIsReadyMap.put(interstitialAd.getAdUnitId(),true);
                    }

                    @Override
                    public void onAdClosed() {
                        Log.d(LOG_TAG, "広告を閉じました。");
                        loadingInterstitial(interstitialAd.getAdUnitId());

                        //広告を閉じたらチャラ。
                        mIsReadyMap.put(interstitialAd.getAdUnitId(),false);

                        mActivity.runOnGLThread(new Runnable() {
                            @Override
                            public void run() {
                                didClose();
                            }
                        });
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        Log.d(LOG_TAG, "ローディング失敗"+errorCode);
                        //ローディング失敗時は準備未完了
                        mIsReadyMap.put(interstitialAd.getAdUnitId(),false);
                    }
                });
                PublisherAdRequest adRequest = new PublisherAdRequest.Builder()
                        .addTestDevice("B6E7FD092B5C22050D3428D25EFFA65A")
                        .build();
                interstitialAd.loadAd(adRequest);

                mAdmobMap.put(appId, interstitialAd);

                log("準備完了マップに追加する"+mIsReadyMap+"IDは"+appId);
            }
        });
    }

    public static boolean isReady(final String appId){

        log("準備マップ"+mIsReadyMap);

        if (!mIsReadyMap.containsKey(appId))//無いキーだったら、準備できていない
            return false;

        return mIsReadyMap.get(appId);
    }

    public static void showIS(final String appId){
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                PublisherInterstitialAd showInterstitial = mAdmobMap.get(appId);
                if (showInterstitial != null && showInterstitial.isLoaded()) {
                    Log.d(LOG_TAG, "ISの読み込みが完了しているので、表示します。"+showInterstitial.getMediationAdapterClassName());

                    showInterstitial.show();
                } else {
                    Log.d(LOG_TAG, "ISの読み込みが完了していない。");
                }
            }
        });
    }

    private static void log(String logText){
        Log.d(LOG_TAG,logText);
    }
}
