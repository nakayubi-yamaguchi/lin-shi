package org.cocos2dx.cpp;

import android.util.Log;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import org.cocos2dx.lib.Cocos2dxActivity;

public class AdXRewardBridge {

    private static final String LOG_TAG = "AdXRewardBridge";

    private static Cocos2dxActivity mActivity;
    private static RewardedVideoAd mAd;
    private static String mAdUnitID;
    private static boolean prepared;
    private static boolean completed;

    public AdXRewardBridge(Cocos2dxActivity activity) {
        mActivity = activity;

        log("初期化します");
    }

    public static native void didStart();

    public static native void didClose(boolean completed);

    public static native void didFail(int errorCode);

    public static void log(String text) {
        Log.d(LOG_TAG, text);
    }

    private static void createAdXReward() {
        mAd = MobileAds.getRewardedVideoAdInstance(mActivity);
        mAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewardedVideoAdLoaded() {
                prepared = true;
                log("onRewardedVideoAdLoaded");
            }

            @Override
            public void onRewardedVideoAdOpened() {
                log("onRewardedVideoAdOpened");
            }

            @Override
            public void onRewardedVideoStarted() {
                mActivity.runOnGLThread(new Runnable() {
                    @Override
                    public void run() {
                        didStart();
                    }
                });
                log("onRewardedVideoStarted");
            }

            @Override
            public void onRewardedVideoAdClosed() {
                log("onRewardedVideoAdClosed");


                final boolean m_completed = completed;
                prepared = false;
                completed = false;

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        didClose(m_completed);
                    }
                });
            }

            @Override
            public void onRewarded(RewardItem rewardItem) {
                completed = true;
                log("オンリワード" + completed);
            }

            @Override
            public void onRewardedVideoAdLeftApplication() {

            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int errorCode) {
                log("onRewardedVideoAdFailedToLoad" + errorCode);
                final int code = errorCode;
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        didFail(code);
                    }
                });
            }

            @Override
            public void onRewardedVideoCompleted() {
                log("コンプリート" + completed);
            }
        });
    }

    public static void loadAdXReward(final String adUnitID) {
        mAdUnitID = adUnitID;
        log("AdXをローディングします " + mAdUnitID);
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                createAdXReward();
                mAd.loadAd(mAdUnitID, new PublisherAdRequest.Builder().build());
            }
        });
    }

    public static boolean isReady() {
        return prepared;
    }

    public static void show() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mAd.isLoaded()) {
                    log("AdXを表示します " + mAd.getMediationAdapterClassName());
                    mAd.show();
                }
            }
        });
    }

    public void onResume() {
        if (mAd != null) {
            mAd.resume(mActivity);
            log("AdXリワードをリジューム");
        }
    }

    public void onPause() {
        if (mAd != null) {
            mAd.pause(mActivity);
            log("AdXリワードをポーズ");
        }
    }

    public void onDestroy() {
        if (mAd != null) {
            mAd.destroy(mActivity);
            log("AdXリワードをデストロイ");
        }
    }
}
