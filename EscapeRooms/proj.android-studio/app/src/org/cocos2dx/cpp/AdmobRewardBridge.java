package org.cocos2dx.cpp;

import android.util.Log;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import org.cocos2dx.lib.Cocos2dxActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;


/**
 * Created by yamaguchinarita on 2016/07/01.
 */
public class AdmobRewardBridge {
    private static final String LOG_TAG = "Test_AdmobRewardBridge";

    private static Cocos2dxActivity mActivity;
    private static RewardedVideoAd mAd;
    private static boolean completed;
    private static boolean isPrepared;
    private static String mAppID;
    private static int loadingCount;//読み込み失敗した時の再読み込みカウント

    public static native void didStart();
    public static native void didClose(boolean completed);

    public AdmobRewardBridge(Cocos2dxActivity activity) {
        mActivity = activity;

        log("初期化します。");
    }

    public static void createAdmobReward()
    {
        // Use an activity context to get the rewarded video instance.
        mAd = MobileAds.getRewardedVideoAdInstance(mActivity);


        mAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public  void onRewardedVideoCompleted(){
                log("コンプリート"+completed);

            }

            @Override
            public void onRewarded(RewardItem reward) {

                completed = true;
                log("オンリワード"+completed);
            }

            @Override
            public void onRewardedVideoAdLeftApplication() {

            }

            @Override
            public void onRewardedVideoAdClosed() {
                log("onRewardedVideoAdClosed");


                final boolean m_completed = completed;
                completed = false;
                isPrepared = false;

                mActivity.runOnGLThread(new Runnable() {
                    @Override
                    public void run() {
                        didClose(m_completed);
                    }
                });

            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int errorCode) {
                log("onRewardedVideoAdFailedToLoad" + errorCode);

                if (errorCode == 3 && loadingCount < 10) {//在庫切れエラー&&再読み込みカウントが10より小さい
                    loadingAdmobReward(mAppID);
                    loadingCount++;
                }
                else {
                    //log("読み込みに失敗しているので、1分後に再読み込み");

                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadingAdmobReward(mAppID);
                            //log("Admobを再読み込みしまっせ。");
                        }
                    }, 1000*60);

                }
            }

            @Override
            public void onRewardedVideoAdLoaded() {

                loadingCount = 0;//再読み込みカウントリセット
                isPrepared = true;
                log("onRewardedVideoAdLoaded");

            }

            @Override
            public void onRewardedVideoAdOpened() {
                log("onRewardedVideoAdOpened");


            }

            @Override
            public void onRewardedVideoStarted() {
                mActivity.runOnGLThread(new Runnable() {
                    @Override
                    public void run() {
                        didStart();
                    }
                });
                log("onRewardedVideoStarted");
            }
        });

        log("クリエイトおおおお");
    }

    public static void loadingAdmobReward(final String appID)
    {
        mAppID = appID;
        log("アドモブをローディングします"+mAppID);
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                createAdmobReward();
                mAd.loadAd(appID, new AdRequest.Builder().build());
            }
        });
    }

    public static boolean isReady()
    {
        log("androidのadmob動画広告の準備状況"+isPrepared);

        return isPrepared;
    }

    public static void show() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isReady()) {
                    log("Admobを表示します。"+mAd.getMediationAdapterClassName());

                    mAd.show();
                }
            }
        });
    }

    public void onResume() {

        if (mAd != null) {
            mAd.resume(mActivity);
            log("アドモブリワードをリジューム");
        }

    }

    public void onPause() {

        if (mAd != null) {
            mAd.pause(mActivity);
            log("アドモブリワードをポーズ");
        }

    }

    public void onDestroy() {
        if (mAd != null) {
            mAd.destroy(mActivity);
            log("アドモブリワードをデストロイ");
        }
    }


    public static void log(String text){Log.d(LOG_TAG, text);}
}
