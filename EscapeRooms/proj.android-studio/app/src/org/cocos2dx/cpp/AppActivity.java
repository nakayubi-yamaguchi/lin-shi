/****************************************************************************
Copyright (c) 2015 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package org.cocos2dx.cpp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import org.cocos2dx.lib.Cocos2dxActivity;


public class AppActivity extends Cocos2dxActivity {
    private static final String LOG_TAG = "AppActivity";

    FireBaseBridge mFireBaseBridge;
    BannerBridge mBannerBridge;
    NativeBridge mNativeBridge;
    InterstitialBridge mInterstitialBridge;
    AlertBridge mAlertBridge;
    AdmobRewardBridge mAdmobRewardBridge;
    NotificationBridge mNotificationBridge;
    ItemStoreBridge mItemStoreRenewal;
    AdXRewardBridge mAdXRewardBridge;
    FluctRewardBridge mFluctRewardBridge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFireBaseBridge = new FireBaseBridge(this);
        mBannerBridge = new BannerBridge(this);
        mNativeBridge = new NativeBridge(this);
        mInterstitialBridge = new InterstitialBridge(this);
        mAlertBridge = new AlertBridge(this);
        mAdmobRewardBridge = new AdmobRewardBridge(this);
        mNotificationBridge = new NotificationBridge(this);

        mItemStoreRenewal = new ItemStoreBridge(this);

        mAdXRewardBridge = new AdXRewardBridge(this);
        mFluctRewardBridge = new FluctRewardBridge(this);

        log("オンクリエイト");

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (getGLSurfaceView()!=null) {
                    getGLSurfaceView().setMultipleTouchEnabled(false);
                }
            }
        });

    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // your stuff or nothing
    }

    @Override
    protected void onStart() {
        Log.d(LOG_TAG, "オンスタート");
        super.onStart();
    }

    @Override
    protected void onResume() {

        Log.d(LOG_TAG, "オンリジューム");

        super.onResume();

        mAdmobRewardBridge.onResume();
        mAdXRewardBridge.onResume();

        mBannerBridge.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(LOG_TAG, "オンポウズ");

        mBannerBridge.onPause();

        mAdmobRewardBridge.onPause();
        mAdXRewardBridge.onPause();


        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d(LOG_TAG, "オンストップ");

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(LOG_TAG, "オンデストロイ");

        mBannerBridge.onDestroy();

        mAdmobRewardBridge.onDestroy();
        mAdXRewardBridge.onDestroy();

        mItemStoreRenewal.onDestroy();

        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d(LOG_TAG,"オンアクティビティリザルト"+requestCode+"結果"+resultCode+ "データ"+data);

    }
    public static void log(String text){
        Log.d(LOG_TAG, text);
    }

}
