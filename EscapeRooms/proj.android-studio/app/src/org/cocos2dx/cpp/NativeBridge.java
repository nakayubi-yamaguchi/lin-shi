package org.cocos2dx.cpp;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import com.ryohei.haruki.EscapeRooms.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by yamaguchinarita on 2016/07/01.
 */
public class NativeBridge {
    private static final String LOG_TAG = "Test_NativeBridge";

    private static Activity mActivity;

    public NativeBridge(Activity activity) {mActivity = activity;}

    //リンクを開きます。
    // 指定URLをWebブラウザで開く
    public static void openUrl(String msg)
    {
        Log.d(LOG_TAG, "リンクを開きます");
        Uri uri = Uri.parse(msg);
        Intent i = new Intent(Intent.ACTION_VIEW,uri);
        mActivity.startActivity(i);
    }

    //ツイート 終わりまで全部
    public static void postTweet(String string) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, string);
        mActivity.startActivity(intent);
    }

    public static void postTweet(String message, String imagePath) {//注 AndroidManifestへ外部ストレージ保存のパーミッションが書かれているか確認すること
        //パッケージを指定しない。
        // インテントを作成する。
        Intent intent = new Intent(Intent.ACTION_SEND);
        // インテントにメッセージを追加する
        intent.putExtra(Intent.EXTRA_TEXT, message);

        // 画像パスが指定されている場合は画像の処理を行う
        if (!imagePath.equals("")) {

            try {
                // 画像パスからURIを作成する
                Uri originalImageUri = Uri.parse(imagePath);

                // 画像の拡張子を取得する
                String ext = getExtension(originalImageUri);

                // 読み込めるように外部ストレージへと画像をコピーする
                Uri readableFileUri = saveImageToExternalDirectory(originalImageUri);

                // 画像のコピーに成功した場合はインテントに格納する
                if (readableFileUri != null) {
                    intent.setType("image/" + ext);
                    intent.putExtra(Intent.EXTRA_STREAM, readableFileUri);
                }

            } catch (Exception e) {
                // 画像のコピーに失敗
            }
        }

        try {
            // アクティビティを呼び出す。
            mActivity.startActivity(intent);

        } catch (ActivityNotFoundException e) {
            // Twitterクライアントがない場合は代わりにブラウザでTwitterのサイトを開くようにする。
            // 画像を付けることはできないため、テキストメッセージだけを付ける。

            // Twitter投稿用インテントに設定するURLを作成する。
            // TwitterのURLのパラメータにメッセージを設定する。
            // ハッシュタグの#の文字はそのままでは処理できないため、URLエンコードの%23に置換する。
            String twitterUrl = String.format("http://twitter.com/share?text=%s", message).replaceAll("#", "%23");

            // ブラウザ表示用のインテントを作成する
            Intent intentForBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(twitterUrl));

            // アクティビティを呼び出す。
            mActivity.startActivity(intentForBrowser);
        }
    }

    private static String getFilename(Uri imagePath) {

        // URLの最後のセグメントを返す
        return imagePath.getLastPathSegment();
    }

    /**
     * ファイルを外部ストレージへ保存する。
     * @param imageUri 元画像のURI
     * @return 外部ストレージのURI
     */
    private static Uri saveImageToExternalDirectory(Uri imageUri) {

        // コピー先ファイルのFileインスタンスを作成する
        File dst = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), getFilename(imageUri));

        // コピー元ファイルのFileインスタンスを作成する
        File src = new File(imageUri.getPath());

        try {

            // ファイルをコピーする
            copyFile(src, dst);

        } catch (Exception e) {

            return null;
        }

        // コピー先ファイルからURIを作成して返す
        return Uri.fromFile(dst);
    }

    /**
     * ファイルをコピーする。
     * @param src コピー元ファイル
     * @param dst コピー先ファイル
     * @throws Exception ファイルアクセス異常
     */
    private static void copyFile(File src, File dst) throws Exception {

        // コピー元ファイルの入力ストリームを作成する
        InputStream in = new FileInputStream(src);

        // コピー先ファイルの出力ストリームを作成数r
        OutputStream out = new FileOutputStream(dst);

        // コピー元ファイルの内容を読み込み、コピー先ファイルへ書き込んでいく
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }

        // 入出力ストリームを閉じる
        in.close();
        out.close();
    }

    /**
     * パスからファイルの拡張子を取得する。
     * @param imagePath 画像ファイルのパス
     * @return 画像ファイルの拡張子
     */
    private static String getExtension(Uri imagePath) {

        // URIからパス文字列を取得する
        String pathString = imagePath.getPath();

        // 一番後ろの"."以降の文字列を切り出して返す
        return pathString.substring(pathString.lastIndexOf(".") + 1);
    }
    //ツイート 終わり

    //パッケージ名を指定して共有 共有したいアプリのパッケージ名を渡す
    public static void share(String packageName, String message, String imagePath) {

        Log.d(LOG_TAG,"パッケージは"+packageName);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, message);
        intent.setPackage(packageName);   // パッケージをそのまま指定

        // 画像パスが指定されている場合は画像の処理を行う
        if (!imagePath.equals("")) {

            try {
                // 画像パスからURIを作成する
                Uri originalImageUri = Uri.parse(imagePath);

                // 画像の拡張子を取得する
                String ext = getExtension(originalImageUri);

                // 読み込めるように外部ストレージへと画像をコピーする
                Uri readableFileUri = saveImageToExternalDirectory(originalImageUri);

                // 画像のコピーに成功した場合はインテントに格納する
                if (readableFileUri != null) {
                    Log.d(LOG_TAG,"画像コピー成功"+ext);
                    intent.setType("image/" + ext);
                    intent.putExtra(Intent.EXTRA_STREAM, readableFileUri);
                }

            } catch (Exception e) {
                // 画像のコピーに失敗
                Log.d(LOG_TAG,"画像コピー失敗");
            }
        }


        try {
            mActivity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // 指定パッケージのアプリがインストールされていないか
            // ACTION_SENDに対応していないか
            Log.d(LOG_TAG, "指定パッケージが応答しません");

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mActivity,
                            "The app is not installed",
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public static String getCurrentLanguage(){
        String lang=Locale.getDefault().toString();

        return lang;
    }

    public static boolean isTablet() {
        return mActivity.getResources().getConfiguration().smallestScreenWidthDp >= 600;
    }

    public static String getOSVersion() {
        return String.valueOf(Build.VERSION.SDK_INT);
    }

    public static int availableStorageMB() {
        long freeByteSize = 0;
        String state = Environment.getExternalStorageState();

        int freeMb = 60;//なぜか空き容量が取得できない人がいる用。→MEDIA_MOUNTEDが原因?ダウンロードできるようにしておいても、できない場合はどうせFirebaseで弾かれる
        //外部保存領域が使用可能かチェック
        if(Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
        {
            //サイズ取得
            freeByteSize = Environment.getExternalStorageDirectory().getFreeSpace();

            int Mb = 1024 * 1024;


            long longFreeMb = freeByteSize/Mb;
            freeMb = (int) longFreeMb;
        }
        Log.d(LOG_TAG, "ストレージの状態は"+Environment.getExternalStorageDirectory() +"ステート" + Environment.getExternalStorageState());

        Log.d(LOG_TAG,"マウント"+Environment.MEDIA_MOUNTED.equals(state) +"リード"+
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state));
        Log.d(LOG_TAG,"空き容量は" + freeMb);
        return freeMb;
    }

    public static boolean isOnline()
    {
        ConnectivityManager connMgr = (ConnectivityManager)
                mActivity.getSystemService(mActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        // You should always check isConnected(), since isConnected()
        // handles cases like unstable network state.

        return (networkInfo != null && networkInfo.isConnected());
    }


    public static String transformThreeComma(float num) {

        //数値の表示フォーマットを設定する
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.getDefault());

        /*DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern(String.valueOf(NumberFormat.getNumberInstance()));*/

        String transed = nf.format(num);

        Log.d(LOG_TAG,"3点カンマ後" + transed);
        return transed;
    }

    public static void vibration()
    {
        ((Vibrator)mActivity.getSystemService(mActivity.VIBRATOR_SERVICE)).vibrate(1000);
    }

    public static String createUUID(){
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}

