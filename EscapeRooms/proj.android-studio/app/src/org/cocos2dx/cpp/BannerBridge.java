package org.cocos2dx.cpp;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;

import org.cocos2dx.lib.Cocos2dxActivity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import jp.tjkapp.adfurikunsdk.AdfurikunLayout;

/**
 * Created by yamaguchinarita on 2016/06/15.
 */
public class BannerBridge {

    private static final String LOG_TAG = "Test_Banner";

    private static Cocos2dxActivity mActivity;
    private static Map<String, Object> mAdmobMap;
    private static Map<String, AdfurikunLayout> mPanelMap;

    private static int LIFECYCLE_RESUME = 1;
    private static int LIFECYCLE_PAUSE = 2;
    private static int LIFECYCLE_DESTROY = 3;
    /**
     * onStart()/onResume()を判断するためのフラグ<br/>
     */
    private static int mActivityLifecycleState;

    /**
     * コンストラクタです。<br/>
     * 指定されたActivityのインスタンスを内部で保持します。<br/>
     */
    public BannerBridge(Cocos2dxActivity activity) {
        mActivity = activity;
        mAdmobMap = new HashMap<>();

        mPanelMap = new HashMap<>();
    }

    public static void createNativeAd(final String bannerID, final float x, final float y,
            final float width, final float height) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                log("android広告を表示しまっせーーーー" + bannerID);

                if (mPanelMap.get(bannerID) == null) {//ないなら新規作成
                    // レイアウト設定
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                            (int) width, (int) height);

                    layoutParams.setMargins((int) x, (int) y, 0, 0);

                    AdfurikunLayout adfurikunView = new AdfurikunLayout(mActivity);
                    adfurikunView.setAdfurikunAppKey(bannerID);
                    adfurikunView.setTransitionType(AdfurikunLayout.TRANSITION_RANDOM);
                    //adfurikunView.setBackgroundColor(Color.YELLOW);

                    mActivity.addContentView(adfurikunView, layoutParams);

                    mPanelMap.put(bannerID, adfurikunView);
                }
            }
        });
    }

    public static void showPanelAd(final String bannerID) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                log("android広告を更新しまっせーーーー");

                AdfurikunLayout adfurikun = mPanelMap.get(bannerID);

                if (adfurikun == null) {
                    log("指定されたIDのパネル広告は作成されていません.これ呼ばれたら異常事態");
                    return;
                }

                adfurikun.setVisibility(View.VISIBLE);
                adfurikun.startRotateAd();
                adfurikun.restartRotateAd();

                if (mActivityLifecycleState == LIFECYCLE_RESUME) {
                    adfurikun.onResume();
                } else if (mActivityLifecycleState == LIFECYCLE_PAUSE) {
                    adfurikun.onPause();
                } else if (mActivityLifecycleState == LIFECYCLE_DESTROY) {
                    adfurikun.destroy();
                }
            }
        });
    }

    public static void hideAllPanelAd() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                log("android広告をかくしまっせーーーー");

                for (String bannerId : mPanelMap.keySet()) {
                    hidePanelAd(bannerId);
                }
            }
        });
    }

    public static void hidePanelAd(final String bannerID) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                log("android広告をかくしーーー" + bannerID);

                AdfurikunLayout adfurikun = mPanelMap.get(bannerID);

                if (adfurikun == null) {
                    log("指定されたIDのパネル広告は作成されていません。");
                    return;
                }

                if (adfurikun.getVisibility() == View.INVISIBLE) {
                    log("すでに表示されていない。");
                    return;
                }

                adfurikun.setVisibility(View.INVISIBLE);
                adfurikun.stopRotateAd();
            }
        });
    }

    public static void removeAllPanelAd() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                for (String bannerId : mPanelMap.keySet()) {
                    AdfurikunLayout adfurikun = mPanelMap.get(bannerId);
                    adfurikun.stopRotateAd();

                    adfurikun.removeAllViews();
                }

                mPanelMap.clear();
            }
        });
    }

    public static void configure(final String appId) {

        Log.d(LOG_TAG, "アプリIDは" + appId);
        MobileAds.initialize(mActivity, appId);
    }

    public static float getBannerWidth() {
        return (float) AdSize.SMART_BANNER.getWidthInPixels(mActivity);
    }

    public static float getBannerHeight() {
        return (float) AdSize.SMART_BANNER.getHeightInPixels(mActivity);
    }

    public static float getRectangleHeight() {
        return (float) AdSize.MEDIUM_RECTANGLE.getHeightInPixels(mActivity);
    }

    public static void createAdmob(final String appId, final boolean isBottom, final int tag) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (mAdmobMap.containsKey(appId)) {
                    log("すでにこのバナーは貼られているよ");
                    return;
                }

                //位置を調整するために一枚噛ませる
                RelativeLayout parentlayout = new RelativeLayout(mActivity);
                RelativeLayout.LayoutParams pLayoutParam = new RelativeLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                mActivity.addContentView(parentlayout, pLayoutParam);

                // レイアウト設定
                mActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                //int width = getDisplaySize(mActivity.getWindowManager().getDefaultDisplay()).x;
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                //layoutParams.gravity = (Gravity.BOTTOM | Gravity.CENTER);


                //最下部にバナーをセット
                if (isBottom) {
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                }

                // AdMobバナー追加
                AdView admobView = new AdView(mActivity);

                //ID変更すること
                admobView.setAdUnitId(appId);
                admobView.setTag(tag);
                admobView.setAdSize(AdSize.SMART_BANNER);

                admobView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        Log.d(LOG_TAG, "広告をロード完了。");
                    }

                    @Override
                    public void onAdClosed() {
                        Log.d(LOG_TAG, "広告を閉じました。");
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {

                        Log.d(LOG_TAG, "ローディング失敗");

                        switch (errorCode) {
                            case AdRequest.ERROR_CODE_INTERNAL_ERROR:
                                Log.d(LOG_TAG, "ローディング失敗1");

                                break;

                            case AdRequest.ERROR_CODE_INVALID_REQUEST:
                                Log.d(LOG_TAG, "ローディング失敗2");

                                break;

                            case AdRequest.ERROR_CODE_NETWORK_ERROR:
                                Log.d(LOG_TAG, "ローディング失敗3");

                                break;

                            case AdRequest.ERROR_CODE_NO_FILL:
                                Log.d(LOG_TAG, "ローディング失敗4");

                                break;

                            default:
                                break;

                        }
                    }
                });
                admobView.setLayoutParams(layoutParams);
                parentlayout.addView(admobView);

                // リクエスト開始
                AdRequest adRequest = new AdRequest.Builder()
                        //.addTestDevice("B6E7FD092B5C22050D3428D25EFFA65A")
                        .build();

                admobView.setBackgroundColor(Color.TRANSPARENT);

                admobView.loadAd(adRequest);

                // ライフサイクルでの実行を補完
                if (mActivityLifecycleState == LIFECYCLE_RESUME) {
                    admobView.resume();
                }

                if (mActivityLifecycleState == LIFECYCLE_PAUSE) {
                    admobView.pause();
                }

                if (mActivityLifecycleState == LIFECYCLE_DESTROY) {
                    admobView.destroy();
                }

                mAdmobMap.put(appId, admobView);
            }
        });
    }

    public static void createAdxBanner(final String appId, final boolean isBottom, final int tag) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (mAdmobMap.containsKey(appId)) {
                    log("すでにこのバナーは貼られているよ");
                    return;
                }

                // レイアウト設定
                mActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                int width = getDisplaySize(mActivity.getWindowManager().getDefaultDisplay()).x;
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(width,
                        LinearLayout.LayoutParams.WRAP_CONTENT);

                if (isBottom) {
                    layoutParams.gravity = (Gravity.BOTTOM | Gravity.CENTER);
                } else {
                    layoutParams.gravity = (Gravity.TOP | Gravity.CENTER);
                }

                // DFPバナー追加
                PublisherAdView dfpView = new PublisherAdView(mActivity);

                //dfpView.setBackgroundColor(Color.BLUE);

                //ID変更すること
                dfpView.setAdUnitId(appId);
                dfpView.setAdSizes(AdSize.SMART_BANNER);
                dfpView.setTag(tag);

                dfpView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        Log.d(LOG_TAG, "広告をロード完了。");
                    }

                    @Override
                    public void onAdClosed() {
                        Log.d(LOG_TAG, "広告を閉じました。");
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        Log.d(LOG_TAG, "ローディング失敗");

                        switch (errorCode) {
                            case AdRequest.ERROR_CODE_INTERNAL_ERROR:
                                Log.d(LOG_TAG, "ローディング失敗1");

                                break;

                            case AdRequest.ERROR_CODE_INVALID_REQUEST:
                                Log.d(LOG_TAG, "ローディング失敗2");

                                break;

                            case AdRequest.ERROR_CODE_NETWORK_ERROR:
                                Log.d(LOG_TAG, "ローディング失敗3");

                                break;

                            case AdRequest.ERROR_CODE_NO_FILL:
                                Log.d(LOG_TAG, "ローディング失敗4");

                                break;

                            default:
                                break;
                        }
                    }
                });

                mActivity.addContentView(dfpView, layoutParams);

                //どの位置に来るか検証する。
                //dfpView.setTranslationX(0);
                log("バナーの位置を調整します。");
                dfpView.setTranslationY(dfpView.getTranslationY());


                // リクエスト開始
                PublisherAdRequest publisherRequest = new PublisherAdRequest.Builder()
                        //.addTestDevice("B6E7FD092B5C22050D3428D25EFFA65A")
                        .build();


                dfpView.loadAd(publisherRequest);

                dfpView.setBackgroundColor(Color.TRANSPARENT);
                //dfpView.setBackgroundColor(Color.BLUE);


                // ライフサイクルでの実行を補完
                if (mActivityLifecycleState == LIFECYCLE_RESUME) {
                    log("バナーをりしゅ");
                    dfpView.resume();
                }

                if (mActivityLifecycleState == LIFECYCLE_PAUSE) {
                    log("バナーをポズ");

                    dfpView.pause();
                }

                if (mActivityLifecycleState == LIFECYCLE_DESTROY) {
                    log("バナーをです");

                    dfpView.destroy();
                }

                mAdmobMap.put(appId, dfpView);
                log(appId + "の広告バナーを追加します。");
            }
        });
    }

    public static void createBanner(final String appId, final float x, final float y,
            final int tag) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (mAdmobMap.containsKey(appId)) {
                    log("すでにこのバナーは貼られているよ");
                    return;
                }

                //位置を調整するために一枚噛ませる
                RelativeLayout parentlayout = new RelativeLayout(mActivity);
                RelativeLayout.LayoutParams pLayoutParam = new RelativeLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                mActivity.addContentView(parentlayout, pLayoutParam);

                // レイアウト設定
                mActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                //int width = getDisplaySize(mActivity.getWindowManager().getDefaultDisplay()).x;
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                //layoutParams.gravity = (Gravity.BOTTOM | Gravity.CENTER);


                // AdMobバナー追加.publisherViewを使用
                // DFPバナー追加
                PublisherAdView dfpView = new PublisherAdView(mActivity);
                //dfpView.setBackgroundColor(Color.BLUE);

                //ID変更すること
                dfpView.setAdUnitId(appId);
                dfpView.setAdSizes(AdSize.SMART_BANNER);
                dfpView.setTag(tag);

                //位置調整。c++側で位置調整。基準点はバナーの左上。
                dfpView.setTranslationX(x);
                dfpView.setTranslationY(y);
                log("バナーの位置を調整します。");


                dfpView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        Log.d(LOG_TAG, "広告をロード完了。");
                    }

                    @Override
                    public void onAdClosed() {
                        Log.d(LOG_TAG, "広告を閉じました。");
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        Log.d(LOG_TAG, "ローディング失敗");

                        switch (errorCode) {
                            case AdRequest.ERROR_CODE_INTERNAL_ERROR:
                                Log.d(LOG_TAG, "ローディング失敗1");

                                break;

                            case AdRequest.ERROR_CODE_INVALID_REQUEST:
                                Log.d(LOG_TAG, "ローディング失敗2");

                                break;

                            case AdRequest.ERROR_CODE_NETWORK_ERROR:
                                Log.d(LOG_TAG, "ローディング失敗3");

                                break;

                            case AdRequest.ERROR_CODE_NO_FILL:
                                Log.d(LOG_TAG, "ローディング失敗4");

                                break;

                            default:
                                break;
                        }
                    }
                });

                mActivity.addContentView(dfpView, layoutParams);


                // リクエスト開始
                PublisherAdRequest publisherRequest = new PublisherAdRequest.Builder()
                        //.addTestDevice("B6E7FD092B5C22050D3428D25EFFA65A")
                        .build();


                dfpView.loadAd(publisherRequest);
                dfpView.setBackgroundColor(Color.TRANSPARENT);


                // ライフサイクルでの実行を補完
                if (mActivityLifecycleState == LIFECYCLE_RESUME) {
                    log("バナーをりしゅ");
                    dfpView.resume();
                }

                if (mActivityLifecycleState == LIFECYCLE_PAUSE) {
                    log("バナーをポズ");

                    dfpView.pause();
                }

                if (mActivityLifecycleState == LIFECYCLE_DESTROY) {
                    log("バナーをです");

                    dfpView.destroy();
                }

                mAdmobMap.put(appId, dfpView);
                log(appId + "の広告バナーを追加します。");
            }
        });
    }

    public static void createRectangle(final String appId, final float x, final float y) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AdView admobView;
                if (mAdmobMap.containsKey(appId)) {
                    log("すでにこのバナーは貼られているよ");

                    admobView = (AdView) mAdmobMap.get(appId);
                    admobView.setVisibility(View.VISIBLE);
                } else {
                    Log.d(LOG_TAG, "レクタングルを生成します");

                    //位置を調整するために一枚噛ませる
                    RelativeLayout parentlayout = new RelativeLayout(mActivity);
                    RelativeLayout.LayoutParams pLayoutParam = new RelativeLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    mActivity.addContentView(parentlayout, pLayoutParam);

                    // レイアウト設定
                    mActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    //int width = getDisplaySize(mActivity.getWindowManager().getDefaultDisplay()
                    // ).x;
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    //layoutParams.gravity = (Gravity.BOTTOM | Gravity.CENTER);

                    // AdMobバナー追加
                    admobView = new AdView(mActivity);

                    //ID変更すること
                    admobView.setAdUnitId(appId);
                    admobView.setAdSize(AdSize.MEDIUM_RECTANGLE);

                    admobView.setAdListener(new AdListener() {
                        @Override
                        public void onAdLoaded() {
                            Log.d(LOG_TAG, "レクタングル広告をロード完了。");
                        }

                        @Override
                        public void onAdClosed() {
                            Log.d(LOG_TAG, "レクタングル広告を閉じました。");
                        }

                        @Override
                        public void onAdFailedToLoad(int errorCode) {

                            Log.d(LOG_TAG, "レクタングルローディング失敗");

                            switch (errorCode) {
                                case AdRequest.ERROR_CODE_INTERNAL_ERROR:
                                    Log.d(LOG_TAG, "レクタングルローディング失敗1");

                                    break;

                                case AdRequest.ERROR_CODE_INVALID_REQUEST:
                                    Log.d(LOG_TAG, "レクタングルローディング失敗2");

                                    break;

                                case AdRequest.ERROR_CODE_NETWORK_ERROR:
                                    Log.d(LOG_TAG, "レクタングルローディング失敗3");

                                    break;

                                case AdRequest.ERROR_CODE_NO_FILL:
                                    Log.d(LOG_TAG, "レクタングルローディング失敗4");

                                    break;

                                default:
                                    break;

                            }
                        }
                    });
                    admobView.setLayoutParams(layoutParams);
                    parentlayout.addView(admobView);

                    // リクエスト開始
                    AdRequest adRequest = new AdRequest.Builder()
                            //.addTestDevice("B6E7FD092B5C22050D3428D25EFFA65A")
                            .build();

                    admobView.loadAd(adRequest);


                    mAdmobMap.put(appId, admobView);
                }

                admobView.setTranslationX(
                        x - AdSize.MEDIUM_RECTANGLE.getWidthInPixels(mActivity) / 2);
                admobView.setTranslationY(y - getRectangleHeight() / 2);

                // ライフサイクルでの実行を補完
                if (mActivityLifecycleState == LIFECYCLE_RESUME) {
                    log("レクタングルResume");
                    admobView.resume();
                }

                if (mActivityLifecycleState == LIFECYCLE_PAUSE) {
                    log("レクタングルPause");
                    admobView.pause();
                }

                if (mActivityLifecycleState == LIFECYCLE_DESTROY) {
                    log("レクタングルDestroy");
                    admobView.destroy();
                }

                admobView.setBackgroundColor(Color.TRANSPARENT);
            }
        });
    }

    public static void hideBanner(final String appId) {


        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Object adObject = mAdmobMap.get(appId);

                if (adObject instanceof AdView) {//Adviewなら
                    log("バナー広告隠します。" + appId);

                    AdView banner = (AdView) adObject;
                    banner.setVisibility(AdView.GONE);

                } else if (adObject instanceof PublisherAdView) {//画面から削除
                    log("バナー広告隠します_" + appId);

                    PublisherAdView banner = (PublisherAdView) adObject;
                    banner.setVisibility(AdView.GONE);
                }
            }
        });
    }

    public static void removeBanner(final String appId) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                log("指定した広告IDを削除いたします。" + appId);
                if (mAdmobMap.containsKey(appId)) {//

                    log("バナー広告を削除いたします。");

                    Object adObject = mAdmobMap.get(appId);

                    if (adObject instanceof AdView) {//Adviewなら
                        log("バナー広告2を削除します" + appId);

                        AdView banner = (AdView) adObject;
                        banner.removeAllViews();
                    } else if (adObject instanceof PublisherAdView) {//画面から削除
                        log("バナー広告3を削除します" + appId);

                        PublisherAdView banner = (PublisherAdView) adObject;
                        banner.removeAllViews();
                    } else {
                        log("AdmobでもAdxでもないものがバナーに追加されている。");
                    }
                    //配列から削除
                    mAdmobMap.remove(appId);
                } else {
                    log("指定されたバナーIDは表示されていません。");
                }
            }
        });
    }

    public static void removeBanner() {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (String key : mAdmobMap.keySet()) {
                    Object adObject = mAdmobMap.get(key);
                    if (adObject instanceof AdView) {//Adviewなら
                        AdView banner = (AdView) adObject;
                        banner.removeAllViews();
                    } else if (adObject instanceof PublisherAdView) {//画面から削除
                        PublisherAdView banner = (PublisherAdView) adObject;
                        banner.removeAllViews();
                    } else {
                        log("AdmobでもAdxでもないものがバナーに追加されている。");
                    }
                }
                mAdmobMap.clear();
            }
        });
    }

    // ディスプレイサイズを取得するためのヘルパー
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private static Point getDisplaySize(Display d) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            Point p = new Point(0, 0);
            d.getSize(p);
            return p;
        }
        try {
            Method getWidth = Display.class.getMethod("getWidth", new Class[]{});
            Method getHeight = Display.class.getMethod("getHeight", new Class[]{});
            return new Point(
                    ((Integer) getWidth.invoke(d, (Object[]) null)).intValue(),
                    ((Integer) getHeight.invoke(d, (Object[]) null)).intValue());
        } catch (NoSuchMethodException e2) {
            return new Point(-1, -1);
        } catch (IllegalArgumentException e2) {
            return new Point(-2, -2);
        } catch (IllegalAccessException e2) {
            return new Point(-3, -3);
        } catch (InvocationTargetException e2) {
            return new Point(-4, -4);
        }
    }

    public static void log(String text) {
        Log.d(LOG_TAG, text);
    }

    public void onResume() {

        log("バナーをリジューム");
        mActivityLifecycleState = LIFECYCLE_RESUME;

        Object[] objects = mAdmobMap.values().toArray();
        for (Object adObject : objects) {
            if (adObject instanceof AdView) {//Adviewなら

                AdView banner = (AdView) adObject;
                banner.resume();
            } else if (adObject instanceof PublisherAdView) {//画面から削除
                PublisherAdView banner = (PublisherAdView) adObject;
                banner.resume();
            } else {
                log("AdmobでもAdxでもないものがバナーに追加されている。");
            }
        }

        if (mPanelMap != null) {
            for (String bannerId : mPanelMap.keySet()) {
                AdfurikunLayout adfuri = mPanelMap.get(bannerId);

                if (adfuri != null) {
                    adfuri.onResume();
                }
            }
        }
    }

    public void onPause() {
        log("バナーをポーズ");

        mActivityLifecycleState = LIFECYCLE_PAUSE;
        Object[] objects = mAdmobMap.values().toArray();
        for (Object adObject : objects) {
            if (adObject instanceof AdView) {//Adviewなら
                AdView banner = (AdView) adObject;
                banner.pause();
            } else if (adObject instanceof PublisherAdView) {//画面から削除
                PublisherAdView banner = (PublisherAdView) adObject;
                banner.pause();
            } else {
                log("AdmobでもAdxでもないものがバナーに追加されている。");
            }
        }

        if (mPanelMap != null) {
            for (String bannerId : mPanelMap.keySet()) {
                AdfurikunLayout adfuri = mPanelMap.get(bannerId);

                if (adfuri != null) {
                    adfuri.onPause();
                }
            }
        }
    }

    public void onDestroy() {
        log("バナーをデストロイ");
        Object[] objects = mAdmobMap.values().toArray();

        for (Object adObject : objects) {
            if (adObject instanceof AdView) {//Adviewなら
                AdView banner = (AdView) adObject;
                banner.destroy();
            } else if (adObject instanceof PublisherAdView) {//画面から削除
                PublisherAdView banner = (PublisherAdView) adObject;
                banner.destroy();
            } else {
                log("AdmobでもAdxでもないものがバナーに追加されている。");
            }
        }

        if (mPanelMap != null) {
            for (String bannerId : mPanelMap.keySet()) {
                AdfurikunLayout adfuri = mPanelMap.get(bannerId);

                if (adfuri != null) {
                    adfuri.destroy();
                }
            }
        }

        mActivityLifecycleState = LIFECYCLE_DESTROY;
    }
}
