package org.cocos2dx.cpp;

import android.support.annotation.Nullable;
import android.util.Log;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.android.billingclient.api.SkuDetails;

import org.cocos2dx.lib.Cocos2dxActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ItemStoreBridge {

    private static final String LOG_TAG = "ItemStore_Test";

    private static Cocos2dxActivity mActivity;
    public static BillingClient mBillingClient;

    private static String mItemID;
    private static boolean mIsConsumable;//消費型であるかどうか
    private static boolean mIsRestore;
    private static Map<String, SkuDetails> mSkuDetailsMap;


    public static native void didFinishPayment(String itemID, boolean isRestore);
    public static native void didFinishSubscriptionPayment(String itemID, boolean isRestore, String expiredSt);

    public static native void didHappenedError(String itemID, boolean isRestore, int errorCode);
    public static native void didHappenedSubscriptionError(String itemID, boolean isRestore, int errorCode);


    public ItemStoreBridge(Cocos2dxActivity activity) {
        Log.d(LOG_TAG, "Item storeニュー");

        mActivity = activity;
        mSkuDetailsMap = new HashMap<>();

        setUpItemStore();
    }

    public static void setUpItemStore() {
        Log.d(LOG_TAG, "Item storeセットアップ");

        mBillingClient = BillingClient.newBuilder(mActivity).setListener(m_purchaseUpdateListener).build();

        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponse) {
                if (billingResponse == BillingClient.BillingResponse.OK) {
                    Log.i(LOG_TAG, "onBillingSetupFinished() response: " + billingResponse);
                } else {
                    Log.w(LOG_TAG, "onBillingSetupFinished() error code: " + billingResponse);
                }
            }
            @Override
            public void onBillingServiceDisconnected() {
                Log.w(LOG_TAG, "onBillingServiceDisconnected()");
            }
        });
    }

    private static void startServiceConnectionIfNeeded(final Runnable executeOnSuccess) {
        if (mBillingClient.isReady()) {
            if (executeOnSuccess != null) {
                executeOnSuccess.run();
            }
        } else {
            mBillingClient.startConnection(new BillingClientStateListener() {
                @Override
                public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponse) {
                    if (billingResponse == BillingClient.BillingResponse.OK) {
                        Log.i(LOG_TAG, "onBillingSetupFinished() response: " + billingResponse);
                        if (executeOnSuccess != null) {
                            executeOnSuccess.run();
                        }
                    } else {
                        Log.w(LOG_TAG, "onBillingSetupFinished() error code: " + billingResponse);
                    }
                }
                @Override
                public void onBillingServiceDisconnected() {
                    Log.w(LOG_TAG, "onBillingServiceDisconnected()");
                }
            });
        }
    }

    static PurchasesUpdatedListener m_purchaseUpdateListener = new PurchasesUpdatedListener() {
        @Override
        public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
            log("購入状態が変更されました。" + responseCode + "購入状態があるかどうか" + purchases);

            if (responseCode == BillingClient.BillingResponse.OK
                    && purchases != null) {
                if (mIsConsumable) {//消費型のアイテムは消費せねば。
                    for (final Purchase purchase : purchases) {
                        log("購入したアイテムは" + purchase.getSku());

                        if (purchase.getSku().equals(mItemID)) {
                            mBillingClient.consumeAsync(purchase.getPurchaseToken(), new ConsumeResponseListener() {
                                @Override
                                public void onConsumeResponse(int responseCode, String purchaseToken) {
                                    log("アイテムを消費しました。"+responseCode+"購入トークンは"+purchaseToken);

                                    //どうせ買う時に消費されているかどうかチェックするから、厳密に判定する必要ない。
                                    mActivity.runOnGLThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            didFinishPayment(purchase.getSku(), mIsRestore);
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
                else {//非消費型のアイテムを購入。
                    mActivity.runOnGLThread(new Runnable() {
                        @Override
                        public void run() {
                            didFinishPayment(mItemID, mIsRestore);
                        }
                    });
                }
            } /*else if (responseCode == BillingClient.BillingResponse.USER_CANCELED) {
                // Handle an error caused by a user cancelling the purchase flow.
            }*/ else {
                // Handle any other error codes.
                mActivity.runOnGLThread(new Runnable() {
                    @Override
                    public void run() {
                        didHappenedError(mItemID, mIsRestore, 5);//ErrorCode5はandroidエラー
                    }
                });
            }
        }
    };

    public static void loadingStoreInformation(final String[] itemKeys)
    {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //log("ストアのアイテム情報をローディングする"+mBillingClient);

                if (mBillingClient == null) return;//ヘルパーがない場合

                final List<String> additionalSkuList = new ArrayList<String>();

                for (String itemKey : itemKeys) {
                    //log("アイテムキー"+itemKey);
                    additionalSkuList.add(itemKey);
                }

                Runnable executeOnConnectedService = new Runnable() {
                    @Override
                    public void run() {

                        SkuDetailsParams skuDetailsParams = SkuDetailsParams.newBuilder()
                                .setSkusList(additionalSkuList).setType(BillingClient.SkuType.INAPP).build();

                        mBillingClient.querySkuDetailsAsync(skuDetailsParams, new SkuDetailsResponseListener() {
                            @Override
                            public void onSkuDetailsResponse(int responseCode, List<com.android.billingclient.api.SkuDetails> skuDetailsList) {

                                if (responseCode == BillingClient.BillingResponse.OK && skuDetailsList != null) {
                                    for (SkuDetails details : skuDetailsList) {
                                        log( "Got a SKU: " + details);
                                        log("skuを表示します"+ details.getSku());

                                        String itemId = details.getSku();

                                        //skudetails
                                        mSkuDetailsMap.put(itemId, details);
                                    }
                                }
                            }
                        });
                    }
                };

                // If Billing client was disconnected, we retry 1 time
                // and if success, execute the query
                startServiceConnectionIfNeeded(executeOnConnectedService);
            }
        });
    }

    public static String getDescription(final String itemID) {

        if (!mSkuDetailsMap.containsKey(itemID)) return "";

        return mSkuDetailsMap.get(itemID).getDescription();
    }

    public static String getPrice(final String itemID) {

        if (!mSkuDetailsMap.containsKey(itemID)) return "";

        double price = (double)mSkuDetailsMap.get(itemID).getPriceAmountMicros()/1000000;
        return String.valueOf(price);
    }

    public static String getLocalPrice(final String itemID) {

        if (!mSkuDetailsMap.containsKey(itemID)) return "";

        return mSkuDetailsMap.get(itemID).getPrice();
    }


    private static void log(String logText){
        Log.d(LOG_TAG,logText);
    }



    public static void startPurchase(final String itemID, final boolean isRestore, final boolean isConsumable){

        Runnable executeOnConnectedService = new Runnable() {
            @Override
            public void run() {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mItemID = itemID;
                        mIsConsumable = isConsumable;
                        mIsRestore = isRestore;

                        //所有状態を確認して、購入させる。
                        checkOwn();
                    }
                });
            }
        };

        startServiceConnectionIfNeeded(executeOnConnectedService);
    }

    private static void checkOwn(){

        Purchase.PurchasesResult purchasesResult = mBillingClient.queryPurchases(BillingClient.SkuType.INAPP);

        String purchaseToken = "";
        if (purchasesResult != null) {
            if (purchasesResult.getPurchasesList() != null) {

                if (purchasesResult.getPurchasesList().size() > 0) {

                    for (Purchase result : purchasesResult.getPurchasesList()) {

                        log("購入履歴があるやつは"+result.getSku());

                        if (result.getSku().equals(mItemID)) {//購入しようとしているアイテムの購入履歴がある。
                            purchaseToken = result.getPurchaseToken();
                        }

                        /*所有状態になってしまった、アイテムをすべて消費する方法。
                        mBillingClient.consumeAsync(result.getPurchaseToken(), new ConsumeResponseListener() {
                            @Override
                            public void onConsumeResponse(int responseCode, String purchaseToken) {
                                log("アイテムを消費しました。"+responseCode+"購入トークンは"+purchaseToken);

                            }
                        });*/
                    }
                }
            }
        }
        log("所有状態を確認します"+purchaseToken);

        if (!purchaseToken.equals("")) {//購入履歴がある
            if (!mIsConsumable){//非消費型
                if (mIsRestore){
                    mActivity.runOnGLThread(new Runnable() {
                        @Override
                        public void run() {
                            didFinishPayment(mItemID, mIsRestore);
                        }
                    });
                }
                else{
                    //購入履歴があったから自動で復元する
                    mActivity.runOnGLThread(new Runnable() {
                        @Override
                        public void run() {
                            log("購入履歴がある");
                            didHappenedError(mItemID, mIsRestore, 6);//ErrorCode6:購入履歴あるのに再度購入しようとした
                        }
                    });
                }
            }
            else {//消費型
                mBillingClient.consumeAsync(purchaseToken, new ConsumeResponseListener() {
                    @Override
                    public void onConsumeResponse(int responseCode, String purchaseToken) {
                        log("アイテムを消費しました。"+responseCode+"購入トークンは"+purchaseToken);

                        if (responseCode == BillingClient.BillingResponse.OK) {
                            purchaseAction();
                        }
                        else {
                            mActivity.runOnGLThread(new Runnable() {
                                @Override
                                public void run() {
                                    log("購入したアイテムの消費に失敗しています。");
                                    didHappenedError(mItemID, mIsRestore, 6);//ErrorCode6:消費型でアイテムが所有されているのに、消費できない。
                                }
                            });
                        }
                    }
                });
            }
        }
        else {//購入履歴なし
            if (mIsRestore && !mIsConsumable){//リストア希望ならcocosに戻してリストア失敗処理
                log("リストアしようとしたけど、購入履歴がありません。");
                mActivity.runOnGLThread(new Runnable() {
                    @Override
                    public void run() {
                        didHappenedError(mItemID, mIsRestore, 2);//ErrorCode2:購入履歴なし
                    }
                });
            }
            else {//購入希望で、アイテムの所持をしていなかった。
                log("購入希望でアイテムを所持していなかった。");
                purchaseAction();
            }
        }
    }

    private static void purchaseAction() {

        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder().setSkuDetails(mSkuDetailsMap.get(mItemID)).build();
        Integer responseCode = mBillingClient.launchBillingFlow(mActivity, billingFlowParams);

        //log("responceCodeは"+responseCode);

    }

    public void onDestroy() {
        Log.d(LOG_TAG, "デストロイ");

        mBillingClient.endConnection();

    }

}
