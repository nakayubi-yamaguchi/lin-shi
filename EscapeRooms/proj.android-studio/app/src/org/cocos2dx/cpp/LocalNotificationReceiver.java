package org.cocos2dx.cpp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.ryohei.haruki.EscapeRooms.R;

public class LocalNotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int notificationId = intent.getIntExtra("notification_id", 0);

        Log.v("","通知を受信 onReceive requestCode"+notificationId);

        String message = intent.getStringExtra("message");


        final Intent notificationIntent = new Intent(context, AppActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(context, notificationId, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentTitle(context.getString(R.string.app_name)+notificationId);//通知を開いた時のタイトル
        builder.setContentText(message);//通知を開いた時のメッセージ
        builder.setSmallIcon(R.mipmap.ic_launcher);//ステータスバーに表示されるアイコン
        builder.setLargeIcon(largeIcon);
        builder.setTicker("ticker");//ステータスバーに表示されるテキスト 4.4まで
        builder.setAutoCancel(true);//通知をタップで消去
        builder.setDefaults(Notification.DEFAULT_ALL);
        builder.setContentIntent(contentIntent);//通知が選択された時に起動されるIntent

        NotificationManager manager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }
}
