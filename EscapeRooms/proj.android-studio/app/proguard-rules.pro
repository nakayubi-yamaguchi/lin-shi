# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in E:\developSoftware\Android\SDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
# <-- Cocos2dx -->
-keep class org.cocos2dx.lib.** { *; }
-keep class com.chukong.cocosplay.** { *; }
-keep class org.cocos2dx.cpp.** { *; }
-keep class jp.tjkapp.adfurikun.movieinterstitial.cocos2dx.** { *; }
-keep class jp.tjkapp.adfurikun.movienative.cocos2dx.** { *; }
-keep class jp.tjkapp.adfurikun.moviereward.cocos2dx.** { *; }

# <!-- 動画SDK ProGuard設定 ->
# MovieReward
-keep interface jp.tjkapp.adfurikunsdk.moviereward.**
-keep class jp.tjkapp.adfurikunsdk.moviereward.** {
public *;
}
-dontwarn jp.tjkapp.adfurikunsdk.moviereward.**

-keep class com.glossomads.** { *; }
-dontwarn android.webkit.**


## Google ver3.8時に入れないとapk書き出し失敗するようになった。
-dontwarn com.google.firebase.**
-dontwarn com.google.android.gms.**
-dontwarn android.support.v4.**

## Google Serviceのメソッド数多いやつら。


##Firebase
-dontwarn org.w3c.dom.**
-dontwarn org.joda.time.**
-dontwarn org.shaded.apache.**
-dontwarn org.ietf.jgss.**
-dontwarn com.firebase.**
-dontnote com.firebase.client.core.GaePlatform

-keepattributes Signature
-keepattributes *Annotation*
-keepattributes InnerClasses,EnclosingMethod

-keep class com.ryohei.haruki.EscapeRooms.models.** { *; }

# Basic ProGuard rules for Firebase Android SDK 2.0.0+
-keep class com.firebase.** { *; }

-keepnames class com.fasterxml.jackson.** { *; }
-keepnames class javax.servlet.** { *; }
-keepnames class org.ietf.jgss.** { *; }


## Applovin
-keep interface com.applovin.sdk.**
-keep class com.applovin.** { *; }
-dontwarn com.applovin.**

## UnityAds
## AARに含まれます。

## Adcolony
-keep interface com.jirbo.adcolony.**
-keep class com.jirbo.adcolony.** { *; }
-dontwarn com.jirbo.adcolony.**

## Maio for Admob Mediation
-keep class com.maio.** { *; }
-dontwarn com.maio.**
##-keep class jp.maio.sdk.android.** { *; }
##-dontwarn jp.maio.sdk.android.**

## AdColony for Admob Mediation
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}
-keepclassmembers class com.adcolony.sdk.ADCNative** {
    *;
 }

##Facebook for Admob Mediation
-keepclassmembers class * implements java.io.Serializable {
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keepnames class com.facebook.FacebookActivity
-keepnames class com.facebook.CustomTabActivity

-keep class com.facebook.all.All

## 課金
-keep public class com.android.vending.billing.IInAppBillingService {
    public static com.android.vending.billing.IInAppBillingService asInterface(android.os.IBinder);
    public android.os.Bundle getSkuDetails(int, java.lang.String, java.lang.String, android.os.Bundle);
}



##Tapjoy
-keep class com.tapjoy.** { *; }
-keep class com.moat.** { *; }
-keepattributes JavascriptInterface
-keep class * extends java.util.ListResourceBundle {
protected Object[][] getContents();
}
-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
public static final *** NULL;
}
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
@com.google.android.gms.common.annotation.KeepName *;
}
-keepnames class * implements android.os.Parcelable {
public static final ** CREATOR;
}
-keep class com.google.android.gms.ads.identifier.** { *; }
-dontwarn com.tapjoy.**

## Vungle
-keep class com.vungle.** { *; }
-keep class javax.inject.*
-keep class dagger.*
-keepattributes *Annotation*
-keepattributes Signature
-dontwarn com.vungle.publisher.**


## Nend for Admob Mediation
-keep class net.nend.android.** { *; }
-dontwarn net.nend.android.**

## Support for Movie Reward Unity
-keep interface com.unity3d.player.**
-keep class com.unity3d.player.**
-dontwarn com.unity3d.player.**
-dontnote


##Google Play Services用
-keep class com.google.protobuf.zzc
-keep class com.google.protobuf.zzd
-keep class com.google.protobuf.zze

-keep class com.google.android.gms.dynamic.IObjectWrapper
-keep class com.google.android.gms.internal.zzuq
-keep class com.google.android.gms.internal.oo
-keep class com.google.android.gms.internal.oh
-keep class com.google.android.gms.internal.zzcgl
-keep class com.google.android.gms.internal.ql
-keep class com.google.android.gms.internal.zzcem
-keep class com.google.android.gms.tagmanager.zzce
-keep class com.google.android.gms.tagmanager.zzcn
-keep class com.google.android.gms.plus.PlusOneButton$OnPlusOneClickListener
-keep class com.google.android.gms.measurement.AppMeasurement$zza
-keep class com.google.android.gms.measurement.AppMeasurement$OnEventListener
-keep class com.google.android.gms.measurement.AppMeasurement$EventInterceptor

-keep class com.google.android.gms.ads.mediation.MediationAdRequest
-keep class com.google.android.gms.ads.mediation.MediationBannerListener
-keep class com.google.android.gms.ads.AdSize
-keep class com.google.android.gms.ads.mediation.MediationInterstitialListener
-keep class com.google.android.gms.ads.mediation.MediationNativeListener
-keep class com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdListener
-keep class com.google.android.gms.ads.InterstitialAd
-keep class com.google.android.gms.ads.AdListener
-keep class com.google.android.gms.ads.Correlator
-keep class com.google.android.gms.ads.formats.NativeAd
-keep class com.google.android.gms.ads.mediation.NativeMediationAdRequest
-keep class com.google.android.gms.ads.formats.MediaView
-keep class com.google.android.gms.ads.formats.AdChoicesView
-keep class com.google.android.gms.ads.mediation.NativeMediationAdRequest
-keep class com.google.android.gms.ads.VideoOptions
-keep class com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener
-keep class com.google.android.gms.ads.mediation.customevent.CustomEventInterstitialListener
-keep class com.google.android.gms.ads.doubleclick.AppEventListener
-keep class com.google.android.gms.ads.mediation.customevent.CustomEventBannerListener
-keep class com.google.android.gms.ads.mediation.customevent.CustomEventNativeListener
-keep class com.google.android.gms.ads.mediation.customevent.CustomEventExtras

-keep class com.google.ads.mediation.MediationServerParameters
-keep class com.google.ads.mediation.NetworkExtras
-keep class com.google.ads.mediation.MediationInterstitialListener
-keep class com.google.ads.mediation.customevent.CustomEventServerParameters
-keep class com.google.ads.mediation.MediationBannerListener
-keep class com.google.ads.AdSize
-keep class com.google.ads.mediation.MediationAdRequest
-keep class com.google.ads.mediation.customevent.CustomEventBannerListener
-keep class com.google.ads.mediation.customevent.CustomEventInterstitialListener

-keep class com.google.firebase.FirebaseApp
-keep class com.google.firebase.database.connection.idl.zzah
-keep class com.google.firebase.database.connection.idl.zzq
-keep class com.google.firebase.database.connection.idl.zzw
-keep class com.google.firebase.database.connection.idl.zzk

-dontnote com.google.protobuf.zzc
-dontnote com.google.protobuf.zzd
-dontnote com.google.protobuf.zze
-dontnote com.google.android.gms.internal.q
-dontnote com.google.android.gms.internal.zzcem

-keep class com.google.android.gms.cast.framework.OptionsProvider

##Fluct
-keep class jp.fluct.mediation.** { *; }

-keep class android.support.** { *; }
-keep interface android.support.** { *; }
-dontwarn android.support.**

##UnityAds
-keep class com.unity3d.ads.** { *; }

# Keep all classes in Unity Services package
-keep class com.unity3d.services.** {
   *;
}
-dontwarn com.google.ar.core.**

# For communication with AdColony's WebView
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

# Keep ADCNative class members unobfuscated
-keepclassmembers class com.adcolony.sdk.ADCNative** {
    *;
}

# For removing warnings due to lack of Multi-Window support
-dontwarn android.app.Activity


##Adjust
-keep public class com.adjust.sdk.** { *; }
-keep class com.google.android.gms.common.ConnectionResult {
    int SUCCESS;
}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient {
    com.google.android.gms.ads.identifier.AdvertisingIdClient$Info getAdvertisingIdInfo(android.content.Context);
}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info {
    java.lang.String getId();
    boolean isLimitAdTrackingEnabled();
}
-keep public class com.android.installreferrer.** { *; }
-keep public class com.android.installreferrer.** { *; }
