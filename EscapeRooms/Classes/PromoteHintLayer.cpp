//
//  PromoteHintLayer.cpp
//  Princess
//
//  Created by ナカユビ on 2017/09/10.
//
//

#include "PromoteHintLayer.h"
#include "DataManager.h"
#include "ConfirmAlertLayer.h"

#include "ShopMenuLayer.h"
#include "Utils/Common.h"
#include "Utils/UtilsMethods.h"
#include "Utils/ContentsAlert.h"
#include "Utils/RewardMovieManager.h"
#include "UIParts/MenuItemScale.h"
#include "SupplementsManager.h"
#include "AnalyticsManager.h"

#include "UIParts/CollectionViewCell.h"
#include "Utils/ValueHelper.h"
#include "EscapeCollectionCell.h"

PromoteHintLayer* PromoteHintLayer::create(const ccMenuCallback &callback)
{
    auto node =new PromoteHintLayer();
    if (node && node->init(callback)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool PromoteHintLayer::init() {
    return init(nullptr);
}

bool PromoteHintLayer::init(const ccMenuCallback &callback)
{
    if (!CustomAlert::init(" ", createMessage(), {"CLOSE", ""}/*ボタンを小さくする裏技。""を加える*/, [callback,this](Ref*ref){
        DataManager::sharedManager()->setIsShowingPromoteHint(false);
        
        for (auto key : temporaryKeysOnHint) {
            DataManager::sharedManager()->setTemporaryFlag(key, 0);
        }
        temporaryKeysOnHint.clear();
        
        Common::performProcessForDebug([](){
            //閉じるボタンを押したら、フラグが一つ進む仕様に。
            //DataManager::sharedManager()->setEnableFlag(DataManager::sharedManager()->getFlagIDForHint());
        }, nullptr);
        
        callback(ref);
    })) {
        return false;
    }
    
    //背景の透過度。ちょっと暗めの設定にします。
    m_backOpacity = 200;
    
    DataManager::sharedManager()->setIsShowingPromoteHint(true);

    flagID=DataManager::sharedManager()->getFlagIDForHint();
    log("ヒントフラグ %f",flagID);
    
    //flag包括map
    auto map = DataManager::sharedManager()->getFlagMapForFlag(flagID);
    
    isEscapeRoomsGrammer = DataManager::sharedManager()->isEscapeRoomsGrammerForFlag();
    allHintVector = DataManager::sharedManager()->getHintArray(true);
    
    log("ヒントの配列を取得しました。");
    
    if (isEscapeRoomsGrammer) {
        for (auto hintValue : allHintVector) {
            auto hintMap = hintValue.asValueMap();
            
            if (DataManager::sharedManager()->getHintType(hintMap) == HintType_Answer) {
                useImageInAnswerMap = hintMap;
            }
        }
    }
    else {
        //直読み込みにするか
        useImageInAnswerMap=DataManager::sharedManager()->getUseImageInAnswerValueMap(map);
    }
   
    hintCount = DataManager::sharedManager()->getHintCount();
    isCollection = EscapeDataManager::getInstance()->getPlayTitleIsCollectionDisplayForHint();
    aspectRatio = 1.1;
    
    loadReleasedNum();

    createLayout();
    
    createProgressBar();
    
    if (isCollection) {
        //コレクション方式
        createCollection();
    }
    else {
        //ヒントボタンを生成する。テーブル形式
        createHintButtonPart();
    }
   
    //所持コイン数が変動した場合の通知登録
    DataManager::sharedManager()->addChangeCoinNotification(this, [this](EventCustom *){
        //通知が飛んできたら、ラベルのテキストを更新する。
        messageLabel->setString(createMessage());
    });
    
    //自動表示した場合
    if (DataManager::sharedManager()->checkAutoPromoteHint())
    {
        AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_AutoPromote_Show);
    }

    
    return true;
}

#pragma mark- 初期化
void PromoteHintLayer::loadReleasedNum()
{
    m_releasedHintNum = DataManager::sharedManager()->getReleasedHintNum(flagID,hintCount);

    m_releasedHints = DataManager::sharedManager()->getReleaseHintVector();
    
    //デバッグモード
    Common::performProcessForDebug([this](){
        //m_releasedHintNum=4;
    }, nullptr);
}

void PromoteHintLayer::createLayout()
{
    //mainSpriteの幅定義。
    minSize.width = (NativeBridge::isTablet())? getContentSize().width*.65 : getContentSize().width*.9;
  
    float hintBackWidth = minSize.width*.95;
    hintButtonWidth = hintBackWidth*.9;
    hintButtonHeight = hintButtonWidth/4;
    hintButtonSpace = hintButtonHeight/3;
    
    int displayCollectionCount = getDisplayHintCount();
    auto hintBackSize = Size(hintBackWidth, hintButtonSpace*(displayCollectionCount+1)+hintButtonHeight*displayCollectionCount);
    
    if (isCollection) {
        m_inColumnCount = 3;//横にどれだけ並べるか
        //セルのサイズ
        float width = hintBackSize.width/(m_inColumnCount + .3);
        cellSize = Size(width, width*aspectRatio);
        
        hintBackSize.height = getCollectionHeight();
    }
    
    TTFConfig messageTtfConfig(messageLabel
                               ->getTTFConfig().fontFilePath,
                               messageLabel
                               ->getTTFConfig().fontSize*.8,
                               GlyphCollection::DYNAMIC);
    messageLabel->setTTFConfig(messageTtfConfig);
    messageLabel->setWidth(hintBackWidth*.85);
    messageLabel->setAlignment(cocos2d::TextHAlignment::LEFT , cocos2d::TextVAlignment::CENTER);
    setMessageLineHeight(messageLabel->getLineHeight()*1.1);
    
    //ヒントを表示する画面
    m_hintSprite = Sprite::create();
    m_hintSprite->setTextureRect(Rect(0, 0, hintBackSize.width, hintBackSize.height));
    m_hintSprite->setColor(Common::getColorFromHex(WhiteColor2));
    m_hintSprite->setCascadeOpacityEnabled(true);
    mainSprite->addChild(m_hintSprite);
    
    //メインスプライトの位置調整
    layoutMainSprite();
}

void PromoteHintLayer::createProgressBar()
{//サイズはtitleLabelを元に作成する
    
    auto barWidth=mainSprite->getContentSize().width*.8;
    auto barheight=titleLabel->getContentSize().height*.75;
    auto space=4;
    auto spr=Sprite::create();
    spr->setTextureRect(Rect(0, 0, barWidth-space*2, barheight-space*2));
    spr->setColor(/*Color3B::GREEN*/Common::getColorFromHex(GreenColor));
    auto spr_back=Sprite::create();
    spr_back->setTextureRect(Rect(0, 0, barWidth, barheight));
    spr_back->setColor(Color3B::GRAY);
    

    progressBar=GaugeSprite::create(spr,spr_back);
    setProgressBarPos();
    
    progressBar->setPercentage(DataManager::sharedManager()->getProgress(), true, .5);
    
    std::string bartext=StringUtils::format("%.0f",DataManager::sharedManager()->getProgress());
    auto fontSize=(barheight-space*2)*.8;
    progressBar->createDisplayLabel(bartext,fontSize);
    progressBar->setDisplayLabelPos(Vec2(progressBar->getContentSize().width-progressBar->displayLabel->getContentSize().width*.5-space-fontSize, 0));
    progressBar->createTitleLabel(DataManager::sharedManager()->getSystemMessage("clearState"), (barheight-space*2)*.8);
    progressBar->setTitleLabelPos(Vec2(progressBar->titleLabel->getContentSize().width/2+space, progressBar->getContentSize().height));
    
    auto percent=Label::createWithTTF("%", LocalizeFont, fontSize*.8);
    percent->setTextColor(Color4B::WHITE);
    percent->enableOutline(Color4B(Common::getColorFromHex(LightBlackColor)),progressBar->displayLabel->getOutlineSize());
    percent->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    percent->setPosition(progressBar->displayLabel->getPosition().x+progressBar->displayLabel->getContentSize().width/2+percent->getContentSize().width*.6,
                         progressBar->displayLabel->getPosition().y-(progressBar->displayLabel->getContentSize().height/2-percent->getContentSize().height/2));
    progressBar->addChild(percent);

    
    mainSprite->addChild(progressBar);
}

void PromoteHintLayer::createHintButtonPart()
{
    //リロード用。すでに存在しているchildを削除
    for (auto child : m_hintSprite->getChildren()) {
        child->removeFromParent();
    }

    //ボタンを生成
    Vector<MenuItem*> menuItems;
    
    for (int i = 0; i < hintCount; i++) {
        auto buttonBack = Sprite::create();
        buttonBack->setTextureRect(Rect(0, 0, hintButtonWidth, hintButtonHeight));
        buttonBack->setColor(Common::getColorFromHex(MyColor));
        buttonBack->setCascadeOpacityEnabled(true);
        
        //立体的に見せるよう
        auto h_sprite_back = Sprite::create();
        h_sprite_back->setTextureRect(Rect(0, 0, buttonBack->getContentSize().width, buttonBack->getContentSize().height*.04));
        h_sprite_back->setColor(Common::getColorFromHex(BlackColor));
        h_sprite_back->setOpacity(180);
        h_sprite_back->setPosition(buttonBack->getContentSize().width/2, -h_sprite_back->getContentSize().height/2);
        buttonBack->addChild(h_sprite_back);

        //ヒントの解放状態に応じて、ラベルを貼り付け
        addButtonNameLabel(buttonBack, i);
        
        //ボタン押下処理
        auto hintMenuItem = MenuItemScale::create(buttonBack, buttonBack, [i, this](Ref*sender){
            this->pushShowHintButton(i);
        });
        hintMenuItem->setTappedScale(.95);
        hintMenuItem->setPosition(m_hintSprite->getContentSize().width/2, m_hintSprite->getContentSize().height-(hintButtonSpace+hintButtonHeight)*i-(hintButtonSpace+hintButtonHeight/2));
        menuItems.pushBack(hintMenuItem);
    }
    
    auto hintMenu = Menu::createWithArray(menuItems);
    hintMenu->setPosition(0, 0);
    m_hintSprite->addChild(hintMenu);
}

void PromoteHintLayer::createCollection()
{
    //レイアウト作成
    CollectionLayout layout = getCollectionLayout();
    
    m_collectionView = NCCollectionView::create(layout, this,  m_hintSprite->getContentSize());
    m_collectionView->setDelegate(this);
    m_collectionView->setScrollBarEnabled(false);
    m_collectionView->setPosition(Vec2(0, 0));
    m_hintSprite->addChild(m_collectionView);
}


#pragma mark - 画像アラート表示
void PromoteHintLayer::showDetailAlert(int index)
{
    if (isAnswer(index)) {
        showAnswerImage();
    }
    else {
        showHintAlert(index);
    }
}

void PromoteHintLayer::showAnswerImage()
{
    auto sprite = createAnswerSprite(ShowType_Answer_Big);
    Sprite* contents;
    if (NativeBridge::isTablet()) {
        auto back = Sprite::create();
        back->setTextureRect(Rect(0, 0, getContentSize().width, getContentSize().width*.7));
        back->setCascadeOpacityEnabled(true);
        
        sprite->setPosition(back->getContentSize()/2);
        sprite->setScale(back->getContentSize().height*.9/sprite->getContentSize().height);
        back->addChild(sprite);
        
        contents = back;
    }
    else {
        contents = sprite;
    }
    
    auto imageAlert=ContentsAlert::create(contents, "Answer", "", {"OK"},[](Ref*ref){
    },
                                          [this,sprite](Ref*ref){
                                              //ボタンを押した瞬間のコールバック
                                              if (useImageInAnswerMap["key"].asString()==CustomActionNameOnBack_Pattern) {
                                                  //パターンの線分をこのたいみんぐで消去
                                                  sprite->removeAllChildren();
                                              }
                                          });
    imageAlert->showAlert(this);
}

void PromoteHintLayer::showHintAlert(int index)
{//画像でのヒント表示
    auto hintMap = allHintVector.at(index).asValueMap();
    
    HintType hintType;
    if (isEscapeRoomsGrammer) {
        hintType = DataManager::sharedManager()->getHintType(hintMap);
    }
    else {
        bool hasBackID = hasBackId(hintMap);
        if (hasBackID || !hintMap["fileName"].isNull()) {
            hintType = HintType_Hint;
        }
        else {
            hintType = HintType_Commentary;//これもヒントだが、解説としておけば、カスタムアラートが表示される。
        }
    }
    
    //アラートを表示
    std::string title = getHintTitleName(index);
    std::string text = getHintText(index);
    
    switch (hintType) {
        
        case HintType_Commentary:{
            auto hintAlert = CustomAlert::create(title, text.c_str(), {"OK"}, nullptr);
            hintAlert->showAlert(this);
            break;
        }
            
        default:{//ヒント系なら
            
            Sprite* contents;
            
            if (NativeBridge::isTablet()) {
                auto back = Sprite::create();
                back->setTextureRect(Rect(0, 0, getContentSize().width, getContentSize().width*.7));
                back->setCascadeOpacityEnabled(true);
                
                auto sprite = createHintSprite(index,ShowType_Hint_Big);
                sprite->setPosition(back->getContentSize()/2);
                sprite->setScale(back->getContentSize().height*.9/sprite->getContentSize().height);
                log("スプライトのサイズ{%f, %f}", sprite->getContentSize().width, sprite->getContentSize().height);
                back->addChild(sprite);
                
                contents = back;
            }
            else {
                contents = createHintSprite(index,ShowType_Hint_Big);
            }
            
            auto imageAlert=ContentsAlert::create(contents, title, text, {"OK"}, true,  [](Ref*ref){},nullptr);
            
            imageAlert->showAlert(this, [this](Ref*ref){
                
            });
            break;
        }
    }
}

void PromoteHintLayer::pushShowHintButton(int index)
{
    if (!isReleasedHint(index)) {//ヒント未解放
        //何番目のヒントを表示しようとしているか記憶
        DataManager::sharedManager()->setTemporaryHintIndex(index);
        
        auto useCoinAction = [this](int selectedNum){//コイン使用アラート
            if (selectedNum == 1) {//
                bool enoughCoin = DataManager::sharedManager()->isEnoughCoin(CoinSpot_Hint);
                if (enoughCoin) {//コインが十分にある。
                    //コインを使います。
                    Common::playSE("pikon.mp3");
                    NativeBridge::vibration3D();
                    
                    DataManager::sharedManager()->useCoin(CoinSpot_Hint);
                    this->recordReleasedHint(true);
                    
                    auto useCoinText = StringUtils::format(DataManager::sharedManager()->getSystemMessage("useCoin").c_str(),CoinSpot_Hint);
                    UtilsMethods::showThanksAlert(useCoinText.c_str(), [](Ref* sender){
                        //ヒントを表示する設定を
                    });
                }
                else {
                    auto shortageSt = DataManager::sharedManager()->getSystemMessage("shortageCoin");
                    auto shortageAlert = ContentsAlert::create("shortage.png", AlertTitle_INFORMATION, shortageSt.c_str(), {DataManager::sharedManager()->getSystemMessage("close"), DataManager::sharedManager()->getSystemMessage("shop")}, [this](Ref* sender){
                        
                        auto al = (ContentsAlert*)sender;
                        auto selectedNum = al->getSelectedButtonNum();
                        
                        if (selectedNum == 1) {
                            auto shopLayer = ShopMenuLayer::create(nullptr);
                            shopLayer->showAlert(this);
                        }
                    });
                    shortageAlert->showAlert(this);
                }
            }
        };
        
        auto useCoinConfirmAction = [this, useCoinAction](){//コイン使用確認アラート表示
            auto useText = StringUtils::format(DataManager::sharedManager()->getSystemMessage("hintConfirmText").c_str(), CoinSpot_Hint);
            
            auto confirmSt = DataManager::sharedManager()->getSystemMessage("confirm");
            auto confirmAl = CustomAlert::create(confirmSt.c_str(), useText.c_str(), {"NO", "YES"}, [useCoinAction](Ref* sender){
                auto al = (CustomAlert*)sender;
                int selectedNum = al->getSelectedButtonNum();
                useCoinAction(selectedNum);
            });
            confirmAl->showAlert();
        };
        
        bool isEnoughCoin = DataManager::sharedManager()->isEnoughCoin(CoinSpot_Hint);
        std::string appTextKey;
        std::vector<std::string> buttons;
        bool displayClose=false;
        displayClose=true;
        if (isEnoughCoin) {
            //コインor動画
            appTextKey="hintConfirmTextCoinOrVideo";//ヒント取得確認
            buttons={DataManager::sharedManager()->getSystemMessage("view_video_button"), DataManager::sharedManager()->getSystemMessage("usingCoin")};
        }
        else{
            //コイン不足 動画orショップ
            appTextKey="confirmPromoteHintWithVideo";//ヒント取得確認
            buttons={DataManager::sharedManager()->getSystemMessage("view_video_button"), DataManager::sharedManager()->getSystemMessage("shop")};
        }
        
        auto hintAlertText = StringUtils::format(DataManager::sharedManager()->getSystemMessage(appTextKey).c_str() ,CoinSpot_Hint);
        
        auto hintAlert = CustomAlert::create(getDisplayNameInList(index).c_str(), hintAlertText.c_str(), buttons, [index, this, isEnoughCoin,displayClose, useCoinConfirmAction, useCoinAction](Ref* sender){
            
            //------movie表示ラムダ
            auto showMovieAlert=[this]{
                bool isPrepared = RewardMovieManager::sharedManager()->isPrepared();
                if (isPrepared) {//動画準備完了
                    RewardMovieManager::sharedManager()->play([this](bool complete){
                        if (complete) {
                            this->recordReleasedHint(false);
                        }
                        else{
                            auto alert = ContentsAlert::create("failed.png", DataManager::sharedManager()->getSystemMessage("error"), DataManager::sharedManager()->getSystemMessage("video_error_not_end"), {"OK"}, nullptr);
                            alert->showAlert();
                        }
                    });
                }
                else {
                    auto message = DataManager::sharedManager()->getSystemMessage("no_video");
                    if (Common::isAndroid()) {
                        message += DataManager::sharedManager()->getSystemMessage("no_video_android");
                    }
                    
                    auto alert = ContentsAlert::create("failed.png", DataManager::sharedManager()->getSystemMessage("error"), message.c_str(), {"OK"}, nullptr);
                    alert->showAlert();
                }
            };
            //------------------------------
            
            auto al = (CustomAlert*)sender;
            int selectedNum = al->getSelectedButtonNum();
            
            //何番目のヒントを表示しようとしているか記憶
            DataManager::sharedManager()->setTemporaryHintIndex(index);
            
            if (isEnoughCoin) {
                if (selectedNum == 0) {//動画を見る
                    showMovieAlert();
                }
                else{
                    useCoinAction(selectedNum);
                }
            }
            else {//コインがない場合
                if (selectedNum == 0) {//動画を見る
                    showMovieAlert();
                }
                else{//ショップ行く。
                    auto shopLayer = ShopMenuLayer::create([](Ref* sender){
                        
                    });
                    shopLayer->showAlert(this);
                }
            }
        });
        
        //右上に閉じるボタンを
        if (displayClose) {
            hintAlert->displayCloseButton([](){
                //閉じるボタンを押したら、一時的なヒントインデックスを空にする
                DataManager::sharedManager()->setTemporaryHintIndex(0);
            });
        }
        
        hintAlert->showAlert();
    }
    else
    {//解放済み
        this->showDetailAlert(index);
    }
}


void PromoteHintLayer::recordReleasedHint(bool byCoin)
{
    int temporaryHintIndex = DataManager::sharedManager()->getTemporaryHintIndex();
    DataManager::sharedManager()->setReleasedHintIndex(temporaryHintIndex);
    
    //成績として、記録
    DataManager::sharedManager()->record(RecordType_Hint);
    
    loadReleasedNum();
    
    if (isCollection) {
        
        //m_hintSprite->removeFromParent();
        
        const char* key = "どやだや";
        Common::startTimeForDebug(key);
        /*
        createLayout();
        setProgressBarPos();
        createCollection();*/
        
        //解放状態を更新して、ヒントスプライトのサイズを変更する
        Rect rect = m_hintSprite->getTextureRect();
        m_hintSprite->setTextureRect(Rect(rect.origin.x, rect.origin.y, rect.size.width, getCollectionHeight()));
        
        //メインスプライトの位置調整。
        layoutMainSprite();
        
        setProgressBarPos();
        
        //コレクションをリロード
        m_collectionView->setCollectionLayout(getCollectionLayout());
        m_collectionView->setContentSize(m_hintSprite->getContentSize());
        m_collectionView->reloadData();
        
        Common::stopTimeForDebug(key);
        //log("コレクションの存在うむ%d", (m_collectionView != NULL));
    }
    else {
        //ヒントのボタンたちの内容を更新するメソッドを呼び出し
        createHintButtonPart();
    }
    
    //使用方法を分析
    if (byCoin) {
        AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_GetHint_ByCoin);
    }
    else{
        AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_GetHint_ByMovie);
    }
    
    //
    //自動表示した場合
    if (DataManager::sharedManager()->checkAutoPromoteHint())
    {
        AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_AutoPromote_GotHint);
    }
}

#pragma mark- UI作成,配置関連
void PromoteHintLayer::layoutMainSprite()
{
    float contentsSpace = space - titleLabel->getHeight()/2;
    minSize.height = space*2 + messageLabel->getContentSize().height + contentsSpace + m_hintSprite->getContentSize().height + m_menuItems.at(0)->getPositionY()*2;
    
    //メインスプライトとタイトルラベルの位置を変更
    mainSprite->setTextureRect(Rect(0, 0, minSize.width, minSize.height));
    arrange();//メインスプライトのサイズをいじったら再整列。
    
    //ラベルを整列
    titleLabel->setPositionY(mainSprite->getContentSize().height-space);
    messageLabel->setPositionY(titleLabel->getPositionY()-space-messageLabel->getBoundingBox().size.height/2);
    
    //ヒント画像を位置調整
    m_hintSprite->setPosition(mainSprite->getContentSize().width/2, space*2+m_hintSprite->getContentSize().height/2);
}

Sprite* PromoteHintLayer::createHintSprite(int index, ShowType showType)
{
    auto hintMap = allHintVector.at(index).asValueMap();
    
    auto ID=hintMap["id"].asInt();
    auto isItem=(hintMap["isItem"].asBool()||hintMap["onItem"].asBool());
    int angle=0;
    
    Size imageSize;
    std::string fileName;
    Sprite*sprite=NULL;
    if(!hintMap["fileName"].isNull()){
        fileName=hintMap["fileName"].asString();
        sprite=EscapeStageSprite::createWithSpriteFileName(fileName);
    }
    else if (isItem) {
        //白背景にaddする
        if (!hintMap["angle"].isNull()) {
            angle=hintMap["angle"].asInt();
            auto angles=DataManager::sharedManager()->getItemData(ID)["angles"].asValueMap()["images"].asValueVector();
            fileName=angles.at(angle-1).asString();
        }
        else{
            fileName=DataManager::sharedManager()->getItemData(ID)["name"].asString();
        }
        sprite=EscapeStageSprite::createWithSpriteFileName(fileName);
    }
    else{
        if (ID>0) {
            fileName=DataManager::sharedManager()->getBackName(ID);
            sprite=EscapeStageSprite::createWithSpriteFileName(fileName);
        }
    }
    
    //temporary flagの処理
    if ((!hintMap["shelfIndex"].isNull()||!hintMap["shelfID"].isNull())&&
        !isItem) {
        
        auto index=(!hintMap["shelfIndex"].isNull()?hintMap["shelfIndex"].asInt():hintMap["shelfID"].asInt());
        //該当switchのflagを立ててあげる
        auto backMap=DataManager::sharedManager()->getBackData(ID);
        auto switchMap=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Switch,backMap);
        auto sprMap=switchMap["sprites"].asValueVector().at(index).asValueMap();
        
        //強制的にindexを1にセット
        auto switchSpr=SwitchSprite::create(sprMap, Value(), switchMap["name"].asString(), index, switchMap["temporaryFlagKey"].asString(),showType);
        switchSpr->setIndex(1);
        //saveし、立てたキーを格納する(close時に開放)
        temporaryKeysOnHint.push_back(switchSpr->save(showType));
    }
    
    //sprite生成後分岐 保管画像を貼っていく
    if (ID>0) {
        sprite->setCascadeOpacityEnabled(true);
        
        if (isItem) {
            imageSize=sprite->getContentSize();
            SupplementsManager::getInstance()->addSupplementToPopUp(sprite, ID, angle, true, showType);
        }
        else{
            SupplementsManager::getInstance()->addSupplementToMain(sprite, ID, true,showType);
            SupplementsManager::getInstance()->customBackAction(sprite,ID,DataManager::sharedManager()->getBackData(ID)["customActionKey"].asString(),showType);
            imageSize=Size(sprite->getContentSize().width, sprite->getContentSize().width*showImageRatio.y/showImageRatio.x);
        }
    }
    
    //指の表示
    if (!hintMap["x"].isNull()||
        !hintMap["vec"].isNull()) {
        auto vecValue=hintMap["vec"];
        if (vecValue.isNull()) {
            //指差しアニメ
            auto beginingPoint=Vec2(hintMap["x"].asFloat(), hintMap["y"].asFloat());
            SupplementsManager::getInstance()->addFinger(sprite, beginingPoint,true);
        }
        else{
            auto vec=hintMap["vec"].asValueVector();
            auto beginingPoint=Vec2(vec.at(0).asValueMap()["x"].asFloat(), vec.at(0).asValueMap()["y"].asFloat());
            auto finger=SupplementsManager::getInstance()->addFinger(sprite, beginingPoint,false);
            finger->dragAnimate(vec, sprite);
        }
    }
    
    //itemBoxの表示
    if (!hintMap["useItemID"].isNull()) {
        auto useItemID=hintMap["useItemID"].asInt();
        auto itemWidth=sprite->getContentSize().width/6;
        auto space=sprite->getContentSize().width*.02;
        auto itemBox=SupplementsManager::getInstance()->createItemBox(useItemID);
        itemBox->setScale(itemWidth/itemBox->getContentSize().width);
        itemBox->setLocalZOrder(999);
        itemBox->setPosition(space+itemWidth/2,sprite->getContentSize().height/2+imageSize.height/2-itemWidth/2-space);
        sprite->addChild(itemBox);
        
        //animate
        itemBox->runAction(Repeat::create(Sequence::create(DelayTime::create(.5),
                                                           EaseIn::create(ScaleBy::create(.2, 1.1), 1.5),
                                                           EaseOut::create(ScaleBy::create(.2, 1.0/1.1), 1.5),
                                                           DelayTime::create(.5)
                                                           , NULL),
                                          -1));
    }

    if (isItem) {
        auto back=Sprite::create();
        back->setCascadeOpacityEnabled(true);
        back->setTextureRect(Rect(0, 0, sprite->getContentSize().width, sprite->getContentSize().width*aspectRatio));
        sprite->setPosition(back->getContentSize()/2);
        back->addChild(sprite);
        return back;
    }
    else{
        return sprite;
    }
}

Sprite* PromoteHintLayer::createAnswerSprite(ShowType showType)
{
    /**useImage情報マップ*/
    ValueMap map=useImageInAnswerMap;
    auto onItem=(map["isItem"].asBool()||map["onItem"].asBool());

    auto backNum=map["id"].asInt();
    auto key=map["key"].asString();
    auto backName=map["name"].asString();
    auto angle=map["angle"].asInt();
    
    //keyを自動補完
    if (key.size()==0) {
        ValueMap customMap;
        if (onItem) {
            customMap=DataManager::sharedManager()->getItemData(backNum);
        }
        else{
            customMap=DataManager::sharedManager()->getBackData(backNum);
        }
        
        for (auto v : customMap) {
            auto customKey=v.first;
            if (customKey==CustomActionNameOnBack_Order||
                customKey==CustomActionNameOnBack_Label||
                customKey==CustomActionNameOnBack_Rotate||
                customKey==CustomActionNameOnBack_Slider||
                customKey==CustomActionNameOnBack_Switch||
                customKey==CustomActionNameOnBack_Pattern||
                customKey==CustomActionNameOnBack_Input||
                customKey==CustomActionNameOnBack_InputRotate||
                customKey==CustomActionNameOnBack_Changing
                ) {
                key=customKey;
                break;
            }
        }
    }
    
    ValueMap backMap;
    if (onItem) {
        backMap=DataManager::sharedManager()->getItemData(backNum);
    }
    else{
        backMap=DataManager::sharedManager()->getBackData(backNum);
    }
    
    //backを自動補完
    if (backName.size()==0) {
        if (onItem) {
            if(angle==0){
                backName=backMap["name"].asString();
            }
            else{
                auto angles=backMap["angles"].asValueMap();
                backName=angles["images"].asValueVector().at(angle-1).asString();//anglesは一つ目がindex0
            }
        }
        else{
            backName=StringUtils::format("back_%d.png",backNum);
        }
    }
    
    auto sprite = EscapeStageSprite::createWithSpriteFileName(backName);
    sprite->setCascadeOpacityEnabled(true);
    
    
    //補完画像をadd
    if (!onItem) {
        SupplementsManager::getInstance()->addSupplementToMain(sprite, backNum);
        SupplementsManager::getInstance()->addAnchorSpritesToMain(sprite, backMap);
    }
    else{
        SupplementsManager::getInstance()->addSupplementToPopUp(sprite, backNum, angle, false, showType);
    }
    
    //CustomParts
    if (key==CustomActionNameOnBack_Label) {
        SupplementsManager::getInstance()->addLabelSupplement(sprite, onItem, backMap, showType);
    }
    else if (key==CustomActionNameOnBack_Switch) {
        SupplementsManager::getInstance()->addSwitchSupplements(sprite, onItem, backMap, showType);
    }
    else if (key==CustomActionNameOnBack_Rotate) {
        SupplementsManager::getInstance()->addRotateSupplements(sprite, onItem, backMap, showType);
    }
    else if (key==CustomActionNameOnBack_Order) {
        SupplementsManager::getInstance()->addOrderSupplements(sprite, onItem, backMap, showType);
    }
    else if (key==CustomActionNameOnBack_Slider) {
        SupplementsManager::getInstance()->addSliderSupplements(sprite, onItem, backMap, showType);
    }
    else if (key==CustomActionNameOnBack_Pattern) {
        SupplementsManager::getInstance()->addPatternSprite(sprite, onItem, backMap, showType);
    }
    else if (key==CustomActionNameOnBack_Input) {
        SupplementsManager::getInstance()->addInputSupplement(sprite, onItem, backMap, showType);
    }
    else if (key==CustomActionNameOnBack_InputRotate) {
        SupplementsManager::getInstance()->addInputRotateSupplement(sprite, onItem, backMap, showType);
    }
    else if (key==CustomActionNameOnBack_Changing) {
        SupplementsManager::getInstance()->addChangingSupplement(sprite, onItem, backMap, showType);
    }
    else{
        auto manager=SupplementsManager::getInstance()->getCustomActionManager();
        manager->showAnswerAction(key, sprite);
        manager->showAnswerAction(key, sprite,showType);
    }
    
    SupplementsManager::getInstance()->customBackAction(sprite, backNum, backMap["customActionKey"].asString(), showType);

    if (onItem) {
        
        auto back=Sprite::create();
        back->setCascadeOpacityEnabled(true);
        back->setTextureRect(Rect(0, 0, sprite->getContentSize().width, sprite->getContentSize().width*aspectRatio));
        sprite->setPosition(back->getContentSize()/2);
        
        back->addChild(sprite);
        return back;
    }
    else{
        return sprite;
    }
}


void PromoteHintLayer::addButtonNameLabel(Sprite *parent, int index)
{
    bool isReleased = isReleasedHint(index);
    
    auto labelAdjustment = [parent](Label* label){
        label->setWidth(parent->getContentSize().width*.85);
        label->setTextColor(Color4B(Common::getColorFromHex(WhiteColor2)));
        label->setPosition(parent->getContentSize()/2);
        parent->addChild(label);
    };
    
    if (!isReleased) {//ヒント未解放
        auto buttonNameLabel = Label::createWithTTF(getDisplayNameInList(index).c_str(), Common::getUsableFontPath(HiraginoMaruFont), hintButtonHeight/5);
        buttonNameLabel->enableBold();
        buttonNameLabel->setAlignment(cocos2d::TextHAlignment::CENTER);
        
        /*
        if (index>m_releasedHintNum+1) {//ヒント見ちゃいけないやつはさらにグレーかける
            auto graySprite = Sprite::create();
            graySprite->setTextureRect(parent->getTextureRect());
            graySprite->setPosition(parent->getContentSize()/2);
            graySprite->setColor(Common::getColorFromHex(BlackColor));
            graySprite->setOpacity(100);
            parent->addChild(graySprite);
        }*/
        
        labelAdjustment(buttonNameLabel);
    }
    else
    {//開放済み
        auto releasedLabel = [labelAdjustment, index, this](){
            auto message = getDisplayNameInList(index);//DataManager::sharedManager()->getHintMessage(index);
            auto buttonNameLabel = Label::createWithTTF(message, Common::getUsableFontPath(HiraginoMaruFont), hintButtonHeight/5.5);
            buttonNameLabel->setAlignment(cocos2d::TextHAlignment::LEFT);
            labelAdjustment(buttonNameLabel);
            
            if (isCollection) {
                buttonNameLabel->setAlignment(cocos2d::TextHAlignment::CENTER);
            }
        };
        
        if (isCollection) {
            Sprite*sprite=NULL;
            if (isAnswer(index)) {
                sprite = createAnswerSprite(ShowType_Answer);
            }
            else {
                sprite = createHintSprite(index,ShowType_Hint);
            }
            
            if (sprite != NULL) {
                
                //はみ出し防止
                auto stencil=Sprite::create();
                stencil->setTextureRect(Rect(0, 0, sprite->getContentSize().width, sprite->getContentSize().height));
                
                auto clip=ClippingNode::create(stencil);
                clip->setAlphaThreshold(0);
                clip->setInverted(false);
                clip->setName("clip");
                clip->setCascadeOpacityEnabled(true);
                clip->addChild(sprite);
                clip->setScale(cellSize.width*.95/sprite->getContentSize().width);
                clip->setPosition(parent->getContentSize()/2);
                
                parent->addChild(clip);
            }
            else {
                releasedLabel();
            }
        }
        else {
            releasedLabel();
        }
    }
}

void PromoteHintLayer::setProgressBarPos()
{
    progressBar->setPosition(titleLabel->getPosition().x,titleLabel->getPosition().y);
}

CollectionLayout PromoteHintLayer::getCollectionLayout()
{
    int columnCount = MIN(m_inColumnCount, getDisplayHintCount());

    Size collectionViewSize = m_hintSprite->getContentSize();
    CollectionLayout layout;
    layout.inColumnCount = m_inColumnCount;
    layout.spaceInRow = (collectionViewSize.height-m_rowCount*cellSize.height)/(m_rowCount+1);
    layout.spaceInColumn = (collectionViewSize.width-columnCount*cellSize.width)/(columnCount+1);
    
    return layout;
}

float PromoteHintLayer::getCollectionHeight()
{
    int displayCollectionCount = getDisplayHintCount();
    
    //最新の行数に更新
    m_rowCount = displayCollectionCount/m_inColumnCount + ((displayCollectionCount%m_inColumnCount > 0)? 1 : 0);
    
    return m_rowCount * cellSize.height * 1.2;//行数 * セルの高さ * 1.2倍くらいに調整
}

#pragma mark- 表示テキスト関連
std::string PromoteHintLayer::createMessage()
{
    std::string message;
    if (DataManager::sharedManager()->checkAutoPromoteHint())
    {//自動表示した場合
        message=DataManager::sharedManager()->getSystemMessage("promoteHint_auto");
    }
    else
    {//コインがある場合、ない場合で分岐
        if (DataManager::sharedManager()->isEnoughCoin(CoinSpot_Hint)) {
            message = StringUtils::format(DataManager::sharedManager()->getSystemMessage("promoteHint").c_str(), DataManager::sharedManager()->getCoinSt().c_str(), CoinSpot_Hint);
        }
        else{
            message = StringUtils::format(DataManager::sharedManager()->getSystemMessage("promoteHintWithVideo").c_str(), DataManager::sharedManager()->getCoinSt().c_str(), CoinSpot_Hint);
        }
    }
    
    return message;
}

std::string PromoteHintLayer::getDisplayNameInList(int hintIndex)
{
    std::string displayButtonText ="";
    if (isReleasedHint(hintIndex)) {//解放済み
        auto hintMap = allHintVector.at(hintIndex).asValueMap();
        HintType hintType = DataManager::sharedManager()->getHintType(hintMap);
        if (isEscapeRoomsGrammer) {//plist内が配列
            switch (hintType) {
                case HintType_Hint:
                    displayButtonText = StringUtils::format(DataManager::sharedManager()->getSystemMessage("lookHint").c_str(),hintIndex+1);
                    
                    break;
                case HintType_Answer:
                    displayButtonText = DataManager::sharedManager()->getSystemMessage("answerImage");
                    
                    break;
                case HintType_Commentary:
                    displayButtonText = DataManager::sharedManager()->getSystemMessage("lookCommentary");
                    
                    break;
                default:
                    break;
            }
        }
        else {
            displayButtonText = hintMap[Common::getLocalizeCode()].asString();
            if (displayButtonText.size()==0) {
                //hintImageタイプの場合は ヒントを見る テキストを表示する
                displayButtonText=StringUtils::format(DataManager::sharedManager()->getSystemMessage("lookHint").c_str(),hintIndex+1);
            }
        }
    }
    else {//未解放
        displayButtonText = getHintTitleName(hintIndex);
    }
    
    return displayButtonText;
}

std::string PromoteHintLayer::getHintTitleName(int hintIndex)
{
    HintType hintType = HintType_Hint;
    if (isEscapeRoomsGrammer) {
        hintType = DataManager::sharedManager()->getHintType(allHintVector.at(hintIndex).asValueMap());
    }
    else {
        if (hintCount > 1 && hintIndex == hintCount-1) {//ヒントが二つ以上あって、かつ最後のボタンなら、answer
            hintType = HintType_Answer;
        }
    }
    
    std::string hintName;
    switch (hintType) {
        case HintType_Hint:
        case HintType_Hint_Mystery:
        case HintType_Hint_Mistake:
        case HintType_Hint_GetItem:
        case HintType_Hint_UseItem:
            hintName = DataManager::sharedManager()->getSystemMessage("hint") + StringUtils::format("%d", hintIndex+1);
            break;
        case HintType_Hint_Location:
            hintName = DataManager::sharedManager()->getSystemMessage("answerLocation");//DataManager::sharedManager()->getSystemMessage("hint");
            break;
        case HintType_Answer:
            hintName = DataManager::sharedManager()->getSystemMessage("answer");
            break;
        case HintType_Commentary:
            hintName = DataManager::sharedManager()->getSystemMessage("commentary");
            break;
        default:
            break;
    }
    
    return hintName;
}

std::string PromoteHintLayer::getHintText(int hintIndex)
{
    auto hintMap = allHintVector.at(hintIndex).asValueMap();
    std::string text;
    
    HintType hintType = DataManager::sharedManager()->getHintType(hintMap);

    switch (hintType) {
        case HintType_Hint:
            break;
            
        case HintType_Hint_Mystery:
            text = DataManager::sharedManager()->getSystemMessage("hintTextForMystery");

            break;

        case HintType_Hint_Location:
            text = DataManager::sharedManager()->getSystemMessage("hintTextForLocation");

            break;
            
        case HintType_Hint_GetItem:
            text = DataManager::sharedManager()->getSystemMessage("hintTextForGetItem");

            break;

        case HintType_Hint_UseItem:
            text = DataManager::sharedManager()->getSystemMessage("hintTextForUseItem");

            break;
            
        case HintType_Hint_Mistake:
            text = DataManager::sharedManager()->getSystemMessage("hintTextForMistake");

            break;
       
        default:
            break;
    }
    
    if (!hintMap["text"].isNull()) {//新バージョン
        text = Common::getLocalizeText(hintMap["text"].asValueMap());
    }
    else {
        if (!hasBackId(hintMap)) {//backIDを指定していない
            text = hintMap[Common::getLocalizeCode().c_str()].asString();
        }
    }
    
    return text;
}

#pragma mark- 判定関連
bool PromoteHintLayer::hasBackId(ValueMap hintMap)
{//インドネシアテキスト用とbackid指定用キー被っているた問題
    return (!hintMap["id"].isNull() && hintMap["id"].getType() == Value::Type::INTEGER);
}

bool PromoteHintLayer::isReleasedHint(int index)
{
    return ValueHelper::isContainsValue(m_releasedHints, Value(index));
}

bool PromoteHintLayer::isAnswer(int index)
{
    if (isEscapeRoomsGrammer) {
        HintType hintType = DataManager::sharedManager()->getHintType(allHintVector.at(index).asValueMap());
        if (hintType == HintType_Answer) {
            return true;
        }
    }
    else {
        if (useImageInAnswerMap.size()>0&&
            index==hintCount-1) {//answerを画像で
            return true;
        }
    }
    return false;
}

int PromoteHintLayer::getDisplayHintCount()
{
    int count = hintCount;
    return count;
}

#pragma mark- コレクションビューのデータソース
ssize_t PromoteHintLayer::numberOfCellsInCollectionView(NCCollectionView *collection)
{
    return getDisplayHintCount();
}

Size PromoteHintLayer::collectionCellSizeForIndex(NCCollectionView *collection, ssize_t idx)
{
    return cellSize;
}

CollectionViewCell* PromoteHintLayer::collectionCellAtIndex(NCCollectionView *collection, ssize_t idx)
{
    auto back = Sprite::create();
    back->setTextureRect(Rect(0, 0, cellSize.width, cellSize.height));
    back->setCascadeOpacityEnabled(true);
    back->setColor(Common::getColorFromHex(MyColor));
    
    addButtonNameLabel(back, (int)idx);
    
    auto cell = CollectionViewCell::create(back, back);
    cell->setTappedScale(.95);
    
    log("コレクションをひょじします");
    
    return cell;
}

#pragma mark- コレクションビューデリゲート
void PromoteHintLayer::collectionCellTouched(NCCollectionView* collection, CollectionViewCell* cell)
{
    Common::playClick();
    log("タッチされております。");
    int index = (int)cell->getIdx();
    
    pushShowHintButton(index);
    //showDetailAlert(index);
}

#pragma mark- 解放
PromoteHintLayer::~PromoteHintLayer()
{
    log("Hint促進レイヤーを解放する。");
}
