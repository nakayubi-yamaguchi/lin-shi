//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "HalloweenActionManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "ZOrders.h"

using namespace cocos2d;

HalloweenActionManager* HalloweenActionManager::manager =NULL;

HalloweenActionManager* HalloweenActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new HalloweenActionManager();
    }
    return manager;
}

#pragma mark - Back
void HalloweenActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if (key=="spider") {        
        std::vector<float>stopHeights={.9,.66,.78,.9,.55};
        auto space=parent->getContentSize().width*.15;
        auto maxHeight=.95;
        Vector<FiniteTimeAction*>actions;
        
        if (backID==1) {
            space=parent->getContentSize().width*.075;
            stopHeights={.7,.6,.65,.7,.55};
            maxHeight=.725;
        }

        for(int i=0;i<5;i++){
            auto spider=Sprite::createWithSpriteFrameName(StringUtils::format("sup_%d_spider.png",backID));
            spider->setPosition(parent->getContentSize().width/2+space*i, parent->getContentSize().height*maxHeight);
            parent->addChild(spider);
            
            //animation
            auto stopHeight=stopHeights.at(i);
            auto duration_up=3*(stopHeight/maxHeight);
            auto duration_up_1=3*((maxHeight-stopHeight)/maxHeight);
            auto duration_down=3;
            
            auto down=EaseInOut::create(MoveTo::create(duration_down,Vec2(spider->getPosition().x, parent->getContentSize().height*.5)), 1.5);

            auto up=EaseInOut::create(MoveTo::create(duration_up, Vec2(spider->getPosition().x, parent->getContentSize().height*stopHeight)),1.5);
            auto delay=DelayTime::create(1);
            auto up_1=EaseInOut::create(MoveTo::create(duration_up_1,Vec2(spider->getPosition().x, parent->getContentSize().height*maxHeight)), 1.5);
        
            auto seq=TargetedAction::create(spider, Sequence::create(down,up,delay,up_1,delay->clone(),NULL));
            actions.pushBack(Repeat::create(seq,UINT_MAX));
        }
        
        //cover
        parent->runAction(Spawn::create(actions));
    }
    else if (key=="cat"){
        Vector<Sprite*>cats;
        bool canAnimate=DataManager::sharedManager()->getEnableFlagWithFlagID(6);
        std::vector<std::string>fileNames;
        std::vector<Vec2>vecs={Vec2(.26, .25),Vec2(.5, .25),Vec2(.75, .25),Vec2(.26, .25),Vec2(.5, .25),Vec2(.75, .25)};
        if(canAnimate){
            fileNames={"sup_5_cat_0_1.png","sup_5_cat_1_1.png","sup_5_cat_2_1.png","sup_5_cat_0_0.png","sup_5_cat_1_0.png","sup_5_cat_2_0.png"};
        }
        else{
            fileNames={"sup_5_cat_0_0.png","sup_5_no_hat.png","sup_5_cat_2_0.png"};
        }
        
        int counter=0;
        for (auto filename:fileNames) {
            auto spr=AnchorSprite::create(vecs.at(counter), parent->getContentSize(), filename);
            parent->addChild(spr);
            if(counter<3&&canAnimate){
                spr->setOpacity(0);
            }
            cats.pushBack(spr);
            counter++;
        }
        
        //animation
        if(canAnimate){
            auto duration=1.0;
            
            Vector<FiniteTimeAction*>actions;
            std::vector<Color3B>colors={Color3B::GREEN,Color3B::RED,Color3B::BLUE};
            std::vector<Vec2>vecs={Vec2(.205, .415),Vec2(.47, .415),Vec2(.715, .415)};
            for (int i=0; i<3; i++) {
                auto cloud=AnchorSprite::create(vecs.at(i), parent->getContentSize(),StringUtils::format("sup_5_cloud_%d.png",i));
                cloud->setOpacity(0);
                cloud->setColor(colors.at(i));
                parent->addChild(cloud);
                
                auto scale_oridinal=cloud->getScale();
                cloud->setScale(.1);
                
                auto cat_0=Spawn::create(TargetedAction::create(cats.at(i), FadeIn::create(duration/2)),TargetedAction::create(cats.at(i+3), FadeOut::create(duration)), NULL);
                auto cat_1=Spawn::create(TargetedAction::create(cats.at(i), FadeOut::create(duration)),TargetedAction::create(cats.at(i+3), FadeIn::create(duration/2)), NULL);
                auto seq_0=Sequence::create(cat_0,DelayTime::create(.5),cat_1, NULL);
                auto music_0=CallFunc::create([i]{
                    Common::playSE("cat_0.mp3");
                });
                auto cloud_action=TargetedAction::create(cloud, Sequence::create(Spawn::create(FadeIn::create(duration/3),ScaleTo::create(duration/6, scale_oridinal), NULL) ,DelayTime::create(duration/2),Spawn::create(FadeOut::create(duration),ScaleTo::create(duration/6, .01), NULL) , NULL));
                
                auto spawn=Spawn::create(seq_0,music_0,cloud_action, NULL);
                
                actions.pushBack(spawn);
            }
            
            auto action_0=actions.at(0);
            auto action_1=actions.at(1);
            auto action_2=actions.at(2);
            
            auto repeat=Repeat::create(Sequence::create(DelayTime::create(.5),action_0,action_1,action_2,action_1->clone(),action_0->clone(),action_2->clone(),DelayTime::create(.7), NULL), UINT_MAX);
            
            parent->runAction(repeat);
        }
    }
    else if(key=="tower")
    {
        auto angle=15;
        auto duration=1.0;
        auto rate=1.5;
        
        if (backID==21) {
            angle=5;
        }
        
        auto tower=parent->getChildByName(StringUtils::format("sup_%d_tower.png",backID));
        
        if(DataManager::sharedManager()->getEnableFlagWithFlagID(20))
        {
            std::vector<std::string>names={"0_1","2_4","3_2","4_3","7_1"};
            for (auto name : names) {
                auto spr=Sprite::createWithSpriteFrameName(StringUtils::format("sup_%d_%s.png",backID,name.c_str()));
                spr->setPosition(parent->getContentSize()/2);
                tower->addChild(spr);
            }
            
            auto rotate_start=Sequence::create(DelayTime::create(1),NULL);
            auto rotate_0=EaseInOut::create(RotateBy::create(duration, -angle),rate);
            auto rotate_1=EaseInOut::create(RotateBy::create(duration, angle),rate);
            auto rotate_2=EaseInOut::create(RotateBy::create(duration*2, -angle*2),rate);
            auto rotate_3=EaseInOut::create(RotateBy::create(duration*2, angle*2),rate);
            
            auto seq=TargetedAction::create(tower, Sequence::create(rotate_start,rotate_0,rotate_3,rotate_0->clone(),rotate_1,rotate_2,rotate_1->clone(),rotate_0->clone(),rotate_3->clone(),rotate_0->clone(),NULL));
            
            parent->runAction(Repeat::create(seq, UINT_MAX));
        }
    }
    else if(key=="ghost"){
        auto isMinigeme=DataManager::sharedManager()->isPlayingMinigame();

        if (isMinigeme) {
            auto mistake=parent->getChildByName<Sprite*>("sup_38_mistake_1.png");
            mistake->setOpacity(0);
        }
        
        if(DataManager::sharedManager()->getOtherFlag("dark").asInt()==1){
            Vector<FiniteTimeAction*>actions;
            auto distance=parent->getContentSize().height*.025;
            
            for (int i=0; i<4; i++) {
                if(i==1&&isMinigeme)
                {//まちがい用アニメーション
                    auto delay=DelayTime::create(5);
                    auto fadein=FadeIn::create(1);
                    auto move=Repeat::create(Sequence::create(EaseInOut::create( MoveBy::create(.5, Vec2(0, distance)),1.5),
                                                              EaseInOut::create( MoveBy::create(.5, Vec2(0, -distance)),1.5)
                                                              , NULL), UINT_MAX);
                    auto call=CallFunc::create([this]{DataManager::sharedManager()->setTemporaryFlag("ghost", 1);});

                    auto spawn=Spawn::create(Sequence::create(fadein,call, NULL),move, NULL);
                    auto mistake=parent->getChildByName<Sprite*>("sup_38_mistake_1.png");
                    mistake->runAction(Sequence::create(delay,spawn, NULL));
                    
                    continue;
                }
                
                auto spr=Sprite::createWithSpriteFrameName(StringUtils::format("sup_38_%d.png",i));
                spr->setPosition(parent->getContentSize()/2);
                spr->setOpacity(0);
                spr->setLocalZOrder(2);
                parent->addChild(spr);
                
                auto appear=Sequence::create(FadeIn::create(1),
                                             CallFunc::create([]{Common::playSE("ghost.mp3");}),
                                             DelayTime::create(.5),
                                             FadeOut::create(.5),
                                             NULL);
                auto move=Repeat::create(Sequence::create(EaseInOut::create( MoveBy::create(.5, Vec2(0, distance)),1.5),
                                                          EaseInOut::create( MoveBy::create(.5, Vec2(0, -distance)),1.5)
                                                          , NULL), 2);
                
                auto spawn=TargetedAction::create(spr, Spawn::create(appear,move, NULL));
                actions.pushBack(spawn);
            }
            
            if(isMinigeme){
                auto seq=Sequence::create(DelayTime::create(1),actions.at(1),actions.at(2),actions.at(0),actions.at(1),actions.at(2), NULL);
                
                parent->runAction(Repeat::create(seq, UINT_MAX));
            }
            else{
                auto seq=Sequence::create(DelayTime::create(1),actions.at(1),actions.at(2),actions.at(0),actions.at(3),actions.at(1),actions.at(2), NULL);
                
                parent->runAction(Repeat::create(seq, UINT_MAX));
            }
        }
    }
    else if(key=="grave"){
        if(!DataManager::sharedManager()->getEnableFlagWithFlagID(32)){
            return;
        }
        auto spawn_hand=createHands(parent);
        auto spawn_sands=createSands(parent, 1);
        auto seq=Sequence::create(DelayTime::create(2),Spawn::create(spawn_hand,spawn_sands, NULL), NULL);
        
        parent->runAction(Repeat::create(seq, UINT_MAX));
    }
    else if(key=="macaron"){
        if(DataManager::sharedManager()->isPlayingMinigame()){
            auto mistake=parent->getChildByName<Sprite*>("sup_14_mistake.png");
            
            auto delay=DelayTime::create(1.5);
            auto move=EaseIn::create(MoveBy::create(1.5, Vec2(0, parent->getContentSize().height*.12)), 1.5);
            auto call=CallFunc::create([this]{DataManager::sharedManager()->setTemporaryFlag("macaron", 1);});
            auto seq=TargetedAction::create(mistake, Sequence::create(delay,move,call, NULL));
            
            parent->runAction(seq);
        }
    }
}

#pragma mark - Touch
void HalloweenActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="candle")
    {
        Common::playSE("huo.mp3");
        
        auto particle=ParticleSystemQuad::create("fire.plist");
        particle->setAutoRemoveOnFinish(true);
        particle->setPosition(parent->getContentSize().width*.52,parent->getContentSize().height*.625);
        particle->setStartSize(parent->getContentSize().width*.08);
        particle->setDuration(5);
        particle->setLocalZOrder(Zorder_Main+2);
        parent->addChild(particle);
        
        auto delay=DelayTime::create(1);
        auto candle=parent->getChildByName("sup_23_candle.png");
        candle->setLocalZOrder(Zorder_Main+1);
        
        auto moon=Sprite::createWithSpriteFrameName("sup_23_moon.png");
        moon->setPosition(parent->getContentSize()/2);
        moon->setOpacity(0);
        parent->addChild(moon);
        
        //animate
        auto move_fire=TargetedAction::create(particle, MoveBy::create(5, Vec2(0, -parent->getContentSize().height*.2)));
        auto spawn_candle=TargetedAction::create(candle,Spawn::create(ScaleTo::create(5, candle->getScale(),0.5),
                                                                      EaseIn::create(FadeOut::create(5), 2) , NULL));
        auto fadein_moon=TargetedAction::create(moon, FadeIn::create(5));
        auto spawn_0=Spawn::create(move_fire,spawn_candle,fadein_moon, NULL);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(delay,spawn_0,call, NULL));
    }
    else if(key=="monkey"){
        Common::playSE("p.mp3");
        
        //give ring
        auto anim_0=Sprite::createWithSpriteFrameName("anim_19_0.png");
        anim_0->setPosition(parent->getContentSize()/2);
        anim_0->setOpacity(0);
        parent->addChild(anim_0);
        
        auto spawn_0=TargetedAction::create(anim_0,Spawn::create(EaseOut::create(MoveBy::create(1, Vec2(0, parent->getContentSize().height*.1)),1.5),
                                                                 FadeIn::create(.5),NULL));
        
        //scene change
        auto anim_1=Sprite::createWithSpriteFrameName("anim_19_1.png");
        anim_1->setPosition(parent->getContentSize()/2);
        anim_1->setOpacity(0);
        parent->addChild(anim_1);
        
        Vector<Sprite*>rings;
        for (int i=0; i<5; i++) {
            auto ring=AnchorSprite::create(Vec2(.595, .4), parent->getContentSize(), "anim_19_ring.png");
            ring->setOpacity(0);
            
            anim_1->addChild(ring);
            rings.pushBack(ring);
        }
        
        Vector<Sprite*>arms;
        for (int i=0; i<2; i++) {
            auto arm=Sprite::createWithSpriteFrameName(StringUtils::format("anim_19_arm_%d.png",i));
            arm->setPosition(parent->getContentSize()/2);
            arm->setOpacity(0);
            
            anim_1->addChild(arm);
            arms.pushBack(arm);
        }
        
        //appear
        auto fadein_0=Spawn::create(TargetedAction::create(anim_1, FadeIn::create(1)),
                                                           TargetedAction::create(rings.at(0), FadeIn::create(1)),
                                    TargetedAction::create(arms.at(0), FadeIn::create(1)), NULL);
        
        //throw
        auto move_ring=[rings,parent](int index){
            std::vector<Vec2>vecs={Vec2(-.2, .4),Vec2(0.05, .4),Vec2(.05, .3),Vec2(-.05, .4),Vec2(-.05, .3)};
            auto distanceX=parent->getContentSize().width*vecs.at(index).x;
            auto distanceY=parent->getContentSize().height*vecs.at(index).y;

            auto height=parent->getContentSize().height*.5;
            auto spawn=TargetedAction::create(rings.at(index), Spawn::create(JumpBy::create(.5,Vec2(distanceX, distanceY), height, 1),
                                                                             FadeOut::create(.5),
                                                                             ScaleBy::create(.5, .1),
                                                                             CallFunc::create([]{
                Common::playSE("hyu.mp3");
            }), NULL));
            return spawn;
        };
        
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<5; i++) {
            if (i>0) {
                auto setup=Spawn::create(TargetedAction::create(rings.at(i), FadeIn::create(.1)),
                                         TargetedAction::create(arms.at(1), FadeOut::create(.1)), NULL);
                actions.pushBack(setup);
            }
            auto arm_action=TargetedAction::create(arms.at(1),FadeIn::create(.2));
            auto spawn=Spawn::create(arm_action,move_ring(i), NULL);
            actions.pushBack(spawn);
        }
        
        auto throwSeq=Sequence::create(actions);
        
        //scene change
        auto back=Sprite::createWithSpriteFrameName("back_20.png");
        back->setPosition(parent->getContentSize()/2);
        back->setOpacity(0);
        parent->addChild(back);
        
        Vector<FiniteTimeAction*>actions_1;
        for (int i=0; i<5; i++) {
            auto ring=Sprite::createWithSpriteFrameName(StringUtils::format("sup_20_ring_%d.png",i));
            ring->setPosition(parent->getContentSize()/2);
            ring->setOpacity(0);
            back->addChild(ring);
            
            auto sound=CallFunc::create([]{
                Common::playSE("koto.mp3");
            });
            auto fadein=TargetedAction::create(ring,FadeIn::create(.2));
            actions_1.pushBack(Spawn::create(sound,fadein, NULL));
        }
        
        auto fadein_1=TargetedAction::create(back, FadeIn::create(1));
        auto ring_seq=Sequence::create(actions_1);
        
        //scene change
        auto back_1=Sprite::createWithSpriteFrameName("back_19.png");
        back_1->setPosition(parent->getContentSize()/2);
        back_1->setOpacity(0);
        parent->addChild(back_1);
        
        auto fadein_2=TargetedAction::create(back_1, FadeIn::create(1));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn_0,DelayTime::create(.5),fadein_0,throwSeq,DelayTime::create(1),fadein_1,DelayTime::create(1),ring_seq,DelayTime::create(.5),fadein_2,call, NULL));
    }
    else if(key=="pig")
    {
        Common::playSE("pig_1.mp3");
        
        auto pig=parent->getChildByName<Sprite*>("sup_16_pig.png");
        auto duration_scale=.2;
        
        auto original_scale=pig->getScale();
        auto scale_0=ScaleTo::create(duration_scale, original_scale*1.1, original_scale*.9);
        auto scale_1=ScaleTo::create(duration_scale, original_scale, original_scale*1.05);
        auto scale_2=ScaleTo::create(duration_scale, original_scale);
        
        auto seq_pig=TargetedAction::create(pig,Sequence::create(scale_0,scale_1,scale_2, NULL));
        
        //
        auto angle=-5;
        auto duration=.15;
        auto left=parent->getChildByName<Sprite*>("sup_16_arm_left.png");
        auto right=parent->getChildByName<Sprite*>("sup_16_arm_right.png");
        auto stick=parent->getChildByName<Sprite*>("sup_16_stick.png");

        auto rotate_0=TargetedAction::create(left,Repeat::create(Sequence::create(RotateBy::create(duration, angle),
                                                                   RotateBy::create(duration, -angle), NULL),2));
        
        auto rotate_1=TargetedAction::create(right,Repeat::create(Sequence::create(RotateBy::create(duration, -angle),RotateBy::create(duration, angle), NULL),2));
        auto rotate_2=TargetedAction::create(stick,Repeat::create(Sequence::create(RotateBy::create(duration, -angle),RotateBy::create(duration, angle), NULL),2));
        auto spawn_arm=Spawn::create(rotate_0,rotate_1,rotate_2, NULL);
        
        //吹き出し
        auto cloud=AnchorSprite::create(Vec2(.6, .675), parent->getContentSize(), "sup_16_cloud.png");
        cloud->setOpacity(0);
        cloud->setScale(.1);
        parent->addChild(cloud);
        
        auto scale_3=EaseIn::create(Spawn::create(FadeIn::create(.3),ScaleTo::create(.3, 1), NULL), 1.5);
        auto scale_4=EaseOut::create(Spawn::create(FadeOut::create(.3),ScaleTo::create(.3, .1), NULL), 1.5);
        auto seq_cloud=TargetedAction::create(cloud,Sequence::create(scale_3,DelayTime::create(1),scale_4,RemoveSelf::create(), NULL));
        
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        
        parent->runAction(Sequence::create(Spawn::create(seq_pig,seq_cloud,spawn_arm, NULL),call,NULL));
    }
    else if(key=="scop")
    {
        Common::playSE("p.mp3");
        auto scop=AnchorSprite::create(Vec2(.61, .375), parent->getContentSize(), "anim_44_scop.png");
        scop->setOpacity(0);
        parent->addChild(scop);
        
        //animate
        auto distance=parent->getContentSize().width*.1;
        auto angle=10;
        auto duration_dig=.25;
        auto first=Sequence::create(RotateBy::create(.1, -angle),MoveBy::create(.5, Vec2(distance, distance)),FadeIn::create(.5), NULL);
        
        auto dig=Repeat::create(Sequence::create(EaseIn::create(Spawn::create(MoveBy::create(duration_dig, Vec2(-distance, -distance)),
                                                                              RotateBy::create(duration_dig, angle), NULL),1.5),
                                                 CallFunc::create([]{Common::playSE("dig.mp3");}),DelayTime::create(.3),
                                                 EaseOut::create(Spawn::create(MoveBy::create(duration_dig, Vec2(distance, distance)),
                                                                               RotateBy::create(duration_dig, -angle), NULL), 1.5), NULL), 3);
        
        auto fadeout=FadeOut::create(.5);
        auto seq_scop=TargetedAction::create(scop, Sequence::create(first,dig,fadeout, NULL));
        
        
        auto sand=createSands(parent, 0);
        //手
        auto sand_1=createSands(parent, 1);
        
        auto spawn=createHands(parent);
        
        auto spawn_hand=Spawn::create(sand_1,spawn, NULL);
        
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_scop,DelayTime::create(.5),sand,DelayTime::create(.5),spawn_hand,call, NULL));
    }
    else if(key=="clear"){
        Common::playSE("gacha.mp3");
        auto open=Sprite::createWithSpriteFrameName("sup_1_open.png");
        open->setPosition(parent->getContentSize()/2);
        parent->addChild(open);
        
        auto delay=DelayTime::create(.5);
        auto call=CallFunc::create([this]{
            auto storyLayer = StoryLayer::create(StoryKey_Ending, [this](Ref* sen){
                EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

                auto story1=StoryLayer::create(StoryKey_Ending2, [this](Ref*ref){
                    Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
                });
                Director::getInstance()->getRunningScene()->addChild(story1);
            });
            Director::getInstance()->getRunningScene()->addChild(storyLayer);
        });
        
        parent->runAction(Sequence::create(delay,call, NULL));
    }
    else if(key=="cantclear"){
        auto storyLayer = StoryLayer::create(StoryKey_NotEnd, [callback](Ref* sen){
            callback(true);
        });
        Director::getInstance()->getRunningScene()->addChild(storyLayer);
    }
    else{
        callback(true);
    }
}

FiniteTimeAction * HalloweenActionManager::createSands(cocos2d::Node *parent,int type)
{
    //土
    Vector<FiniteTimeAction*>actions;
    
    std::vector<Vec2>poses={Vec2(.1, .34),Vec2(.5, .34),Vec2(.9, .34)};
    for (int i=0;i<6;i++) {
        auto call=CallFunc::create([poses,parent,i]{
            auto pos=poses.at(i%3);
            auto particle=ParticleSystemQuad::create("particle_shower.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width*pos.x,parent->getContentSize().height*pos.y);
            particle->setStartSize(parent->getContentSize().width*.01);
            particle->setEndSize(particle->getStartSize()/2);
            
            if(i<3){
                Common::playSE("za.mp3");
                particle->setLife(.3);
                particle->setDuration(.2);
            }
            else{
                Common::playSE("zaa.mp3");
                Common::playSE("zombie_0.mp3");
                particle->setLife(.5);
                particle->setDuration(.5);
            }
            
            parent->addChild(particle);
        });
        
        auto seq=Sequence::create(call,DelayTime::create(.5), NULL);
        actions.pushBack(seq);
    }
    
    if (type==0) {
        return Sequence::create(actions.at(1),actions.at(0),actions.at(2),NULL);
    }
    
    return Spawn::create(actions.at(3),actions.at(4),actions.at(5),NULL);
}

Spawn* HalloweenActionManager::createHands(cocos2d::Node *parent)
{
    Vector<FiniteTimeAction*>actions_hand;
    auto distance_hand=.185*parent->getContentSize().height;
    for (int i=0; i<3; i++) {
        auto hand=Sprite::createWithSpriteFrameName(StringUtils::format("sup_44_%d.png",i));
        hand->setPosition(parent->getContentSize().width/2,parent->getContentSize().height/2-distance_hand);
        hand->setLocalZOrder(.5);
        parent->addChild(hand);
        
        auto move=TargetedAction::create(hand,Sequence::create(EaseIn::create(MoveBy::create(2.5, Vec2(0, distance_hand)), 1.5),
                                                               DelayTime::create(4),
                                                               CallFunc::create([]{Common::playSE("zombie_1.mp3");
            Common::playSE("zuzuzu.mp3");
        }), EaseOut::create(MoveBy::create(1.5, Vec2(0, -distance_hand)), 1.5), NULL));
        
        actions_hand.pushBack(move);
    }
    
    return Spawn::create(actions_hand);
}

#pragma mark - item
void HalloweenActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{
    if(key=="candy"){
        parent->itemImage->setSpriteFrame("candy_1.png");
        auto story=StoryLayer::create("candy", [callback](Ref*ref){callback(true);});
        Director::getInstance()->getRunningScene()->addChild(story);
    }
}

#pragma mark - Clear Scene
void HalloweenActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    if (key == CustomAction_Clear) {
        Vector<FiniteTimeAction*>actions;
        for(int i=0;i<2;i++){
            auto spr=Sprite::createWithSpriteFrameName(StringUtils::format("clear_ghost_%d.png",i));
            if(!spr){
                return;
            }
            spr->setPosition(parent->getContentSize()/2);
            spr->setOpacity(0);
            parent->addChild(spr);
            
            auto distance=parent->getContentSize().height*.05;
            auto delay=DelayTime::create(.5*i);
            auto fadein=FadeIn::create(1);
            auto move=Repeat::create(Sequence::create(EaseInOut::create(MoveBy::create(.75, Vec2(0, distance)),1.5),EaseInOut::create(MoveBy::create(.75, Vec2(0, -distance)),1.5), NULL),3);
            auto spawn=Spawn::create(fadein,move, NULL);
            auto fadeout=FadeOut::create(1);
            auto delay_1=DelayTime::create(.5);
            auto seq=TargetedAction::create(spr, Sequence::create(delay,spawn,fadeout,delay_1, NULL));
            
            parent->runAction(Repeat::create(seq, UINT_MAX));
        }
    }
    else if (key==CustomAction_ReviewLayer){
        DataManager::sharedManager()->removeItemWithID(13);
    }
}
