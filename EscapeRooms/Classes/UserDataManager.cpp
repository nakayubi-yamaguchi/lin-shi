//
//  UserDataManager.cpp
//  Exit
//
//  Created by 成田凌平 on 2019/02/02.
//
//

#include "UserDataManager.h"
#include "NotificationKeys.h"
#include "Utils/NativeBridge.h"
#include "Utils/FireBaseBridge.h"
#include "EscapeDataManager.h"//datalistsのパスを読むため
#include "Utils/ValueHelper.h"
#include "DataManager.h"
#include "Utils/Common.h"

//local mapのキーと、database上のキーの名前として使用
#define KeyName_UserID "userID"
#define KeyName_Coin "coins"
#define KeyName_UsedCoin "usedCoins"

#define KeyName_GotStampDateByMovie "gotStampDate"
#define KeyName_TotalStamp "totalStamp"
#define KeyName_TotalStampByMovie "totalStampByMovie"
#define KeyName_NowStampCount "nowStampCount"
#define KeyName_LaunchCount "launchCount"
#define KeyName_ClearStages "clearStages"//これはdatabase上のみで使用するキー

#define KeyName_Achievement_LaunchCount "achieve_launchCount"
#define KeyName_Achievement_TotalStampByMovie "achieve_totalStampByMovie"
#define KeyName_Achievement_TotalStamp "achieve_totalStamp"
#define KeyName_Achievement_ClearTitle "achieve_cleartitle"
#define KeyName_Achievement_UsedCoin "achieve_usedCoin"

using namespace cocos2d;

UserDataManager* UserDataManager::manager =NULL;

UserDataManager* UserDataManager::sharedManager()
{
    return getInstance();
}

UserDataManager* UserDataManager::getInstance()
{
    if (manager==NULL) {
        manager=new UserDataManager();
        manager->init();
    }
    
    return manager;
}

void UserDataManager::init()
{
    loadUserData();
}

#pragma mark - ロード
void UserDataManager::loadUserData()
{//plistからデータを読み込む
    map = FileUtils::getInstance()->getValueMapFromFile(getDataListPath()+FileName_UserRecordData);
    
    //マイグレーションするときに、useridをセットする。
    //createUserID();
    
    if (map.size()>0) {
        //setUserID(map[KeyName_UserID].asString());
        //setCoin(map[KeyName_Coin].asInt());
        //setUsedCoin(map[KeyName_UsedCoin].asInt());
        //setGotStampDateByMovie(map[KeyName_GotStampDateByMovie].asDouble());
        //setTotalStampCount(map[KeyName_TotalStamp].asInt());
        //setTotalStampCountByMovie(map[KeyName_TotalStampByMovie].asInt());
        //setNowStampCount(map[KeyName_NowStampCount].asInt());
        //setLaunchCount(map[KeyName_LaunchCount].asInt());
        
        
        
        //
        /*
        setAchievementLaunchCount(map[KeyName_Achievement_LaunchCount].asInt());
        setAchievementTotalStampByMovie(map[KeyName_TotalStampByMovie].asInt());
        setAchievementTotalStamp(map[KeyName_TotalStamp].asInt());
        setAchievementClearCount(map[KeyName_Achievement_ClearTitle].asInt());
        setAchievementUsedCoin(map[KeyName_Achievement_UsedCoin].asInt());
         */
    }
}

#pragma mark ユーザID
void UserDataManager::createUserID()
{
    if (getUserID().size()==0) {
        setUserID(NativeBridge::createUUID());
        log("ユーザIDを生成します : %s",UserDataManager::sharedManager()->getUserID().c_str());
        map[KeyName_UserID]=getUserID();
    }
    else{
        log("読み込んだユーザID : %s",getUserID().c_str());
    }
}

#pragma mark - ログイン日数
void UserDataManager::incrementLaunchCount()
{
    setLaunchCount(getLaunchCount()+1);
    map[KeyName_LaunchCount]=getLaunchCount();
    
    save();
}

#pragma mark - コイン
void UserDataManager::addCoin(int added, bool doSave)
{
    setCoin(added+getCoin());
    //コインの量に増減があった場合は変更通知を飛ばす
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyChangedOwnedCoin);
    map[KeyName_Coin]=getCoin();
    
    //累計使用コイン
    if (added<0) {
        setUsedCoin(getUsedCoin()-added);
        map[KeyName_UsedCoin]=getUsedCoin();
    }
    
    //save,post
    if (doSave) {
        save();
    }
}

bool UserDataManager::isEnoughCoin(int used)
{
    return (getCoin() >= used);
}

std::string UserDataManager::getCoinSt()
{
    float num = (float)getCoin();
    return NativeBridge::transformThreeComma(num);
}

void UserDataManager::addChangeCoinNotification(Node *parent, const std::function<void (EventCustom *)> &callback)
{
    //通知を登録します。
    auto changeOwnCoinListner=EventListenerCustom::create(NotificationKeyChangedOwnedCoin, callback);
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(changeOwnCoinListner, parent);
}

void UserDataManager::purchasedBeginnersPack(int dailyBonusCoin, int dailyPeriod)
{
    auto serverDate = FireBaseBridge::getInstance()->getServerTimestamp();
    
    setGotDailyBonusDate(serverDate);
    setDailyBonusCount(dailyPeriod);
    setDailyBonusCoin(dailyBonusCoin);
    
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyAchievementBadge);
}

bool UserDataManager::canGotDailyCoin()
{
    if (getDailyBonusCount() == 0) return false;//デイリーボーナスを貰い切った or 未購入
    
    auto todayServerDate = FireBaseBridge::getInstance()->getServerTimestamp();
    
    auto todayServerLongDate = Common::transformTimeToDate(todayServerDate);
    auto lastGotLongDate = Common::transformTimeToDate(getGotDailyBonusDate());
    log("本日は%lld,最後に受け取ったのは%lld",todayServerLongDate, lastGotLongDate);
    if (todayServerLongDate <= lastGotLongDate) return false;
    
    return true;
}

void UserDataManager::addDailyBonus()
{
    //残りカウントを更新
    auto bonusCount = getDailyBonusCount()-1;
    setDailyBonusCount(bonusCount);
    
    //受取日を更新
    auto todayServerDate = FireBaseBridge::getInstance()->getServerTimestamp();
    setGotDailyBonusDate(todayServerDate);
    
    //付与
    addCoin(getDailyBonusCoin(),true);
    
    log("デイリーボーナスコインを受け取った%d::%f::%d", bonusCount, todayServerDate, getDailyBonusCoin());
}

#pragma mark - スタンプ
void UserDataManager::setGotStampDate()
{
    auto serverDate = (double)FireBaseBridge::getInstance()->getServerTimestamp();

    log("%s,受け取り サーバーdateを保存します%f",__func__,serverDate);
    map[KeyName_UserID]=serverDate;
}

void UserDataManager::addTotalStamp(int add)
{
    setTotalStampCount(getTotalStampCount()+add);
    map[KeyName_TotalStamp]=getTotalStampCount();
}

void UserDataManager::addTotalStampByMovie(int add)
{
    setTotalStampCountByMovie(getTotalStampCountByMovie()+add);
    map[KeyName_TotalStampByMovie]=getTotalStampCountByMovie();
}

void UserDataManager::setNowStampCount(int var)
{
    nowStampCount=var;
    map[KeyName_NowStampCount]=nowStampCount;
}

int UserDataManager::getNowStampCount()
{
    return nowStampCount;
}

#pragma mark - アチーブメント
/*
void UserDataManager::recordAchieve(Achievement var, int value)
{
    switch (var) {
        case Achievement_Login:
            setAchievementLaunchCount(value);
            map[KeyName_Achievement_LaunchCount]=getAchievementLaunchCount();
            break;
        case Achievement_TotalStampByMovie:
            setAchievementTotalStampByMovie(value);
            map[KeyName_Achievement_TotalStampByMovie]=getAchievementTotalStampByMovie();
            break;
        case Achievement_ClearTitle:
            setAchievementClearCount(value);
            map[KeyName_Achievement_ClearTitle]=getAchievementClearCount();
            break;
        case Achievement_TotalStamp:
            setAchievementTotalStamp(value);
            map[KeyName_Achievement_TotalStamp]=getAchievementTotalStamp();
            break;
        case Achievement_UsedCoin:
            setAchievementUsedCoin(value);
            map[KeyName_Achievement_UsedCoin]=getAchievementUsedCoin();
            break;
        default:
            break;
    }
    
    save();

    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyAchievementBadge);
}

int UserDataManager::getAchieveNowPoint(Achievement achievement)
{
    int num=-1;
    switch (achievement) {
        case Achievement_Login:
            num=getLaunchCount();
            break;
            
        case Achievement_TotalStampByMovie:
            num=getTotalStampCountByMovie();
            break;
        case Achievement_ClearTitle:
            num=DataManager::sharedManager()->getTotalStageRecord(RecordKey_ClearGame);
            break;
        case Achievement_TotalStamp:
            num=getTotalStampCount();
            break;
        case Achievement_UsedCoin:
            num=getUsedCoin();
            break;
        
        default:
            break;
    }
    
    return num;
    /*if (achievement==Achievement_StageClear)
    {//recordDataからの読み込み
        return 0;//getTotalStageRecord(RecordKey_ClearGame);
    }
    else if(achievement==Achievement_BuyBeginnersPack)
    {//1or0
        return 0;//(UserDefault::getInstance()->getIntegerForKey(UserKey_DailyBonusCoin, 0) != 0);
    }
    else if
    {//UserData
        return getNowAchievementPoint(achievement);//UserDefault::getInstance()->getIntegerForKey(getAchievementKey(achievement).c_str());
    }
}*/

/*
ValueMap UserDataManager::getAchievementFileMap()
{
    static auto map=FileUtils::getInstance()->getValueMapFromFile(FilePath_Achievement);
    return map;
}

int UserDataManager::getAchievementTargetPoint(Achievement achievement)
{
    //取得済みのポイントを取得
    int point=1;//UserDefault::getInstance()->getIntegerForKey(getGotAchievementKey(achievement).c_str());
    
    switch (achievement) {
        case Achievement_Login:
            point=getAchievementLaunchCount();
            break;
            
        default:
            break;
    }
    
    auto map=getAchievementFileMap()[getAchievementFileKey(achievement)].asValueMap();
    
    auto points=map["achievePoints"].asValueVector();//達成報酬を得られるポイントの配列
    
    for (auto value : points) {
        if (value.asInt()>point) {
            return value.asInt();
        }
    }
    
    //全て達成済みの場合は0を返す
    return 0;
}

std::string UserDataManager::getAchievementFileKey(Achievement achievement)
{
    switch (achievement) {
        case Achievement_Login:
            return "LoginDayCount";
            break;
        case Achievement_TotalStampByMovie:
            return "UserKey_TotalStampByMovie";
            break;
        case Achievement_PrePlay:
            return "UserKey_PrePlayCount";
            break;
        case Achievement_StageClear:
            return "StageClear";
            break;
        case Achievement_BuyBeginnersPack:
            return "BuyBeginnersPack";
            break;
        default:
            break;
    }
    
    return "";
}

int UserDataManager::getNowAchievementPoint(Achievement achievement)
{
    switch (achievement) {
        case Achievement_Login:
            return getLaunchCount();
            break;
            
        default:
            break;
    }
    
    return -1;
}

void UserDataManager::setAchievementPoint(Achievement achievement)
{//達成した項目を記録
    switch (achievement) {
        case Achievement_Login:
            this->setAchievementLaunchCount(getLaunchCount());
            map[KeyName_Achievement_LaunchCount]=getLaunchCount();
            break;
            
        default:
            break;
    }
    
    save();
    
    auto key_gotAchieve=getGotAchievementKey(achievement);
    int point=getAchievementPoint(achievement);
    UserDefault::getInstance()->setIntegerForKey(key_gotAchieve.c_str(), point);*/
    
    //Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyAchievementBadge);
//}*/

#pragma mark - 投票
ValueMap UserDataManager::getVoteDataMap()
{
    auto voteDataValue = map[VoteKey_VoteData];
    ValueMap voteDataMap;
    if (!voteDataValue.isNull()) {
        voteDataMap = voteDataValue.asValueMap();
    }
    
    return voteDataMap;
}

bool UserDataManager::isVotedToday(const char *keyName)
{
    ValueMap voteDataMap = getVoteDataMap();
    if (voteDataMap.size()>0) {
        //version.plistのkey_name(TEST_VOTE)
        if (!voteDataMap[keyName].isNull()) {
            ValueVector keyNameVector = voteDataMap[keyName].asValueVector();
            double todayTimestamp = FireBaseBridge::getInstance()->getServerTimestamp();
            long long today = Common::dateWithTimeStamp(TimeUnit_Day, todayTimestamp);
            for (Value value : keyNameVector) {
                auto voteValueMap = value.asValueMap();
                double voteTimestamp = voteValueMap[VoteKey_Timestamp].asDouble();
                long long voteDate = Common::dateWithTimeStamp(TimeUnit_Day, voteTimestamp);
                log("投票日は%lld:::本日は%lld", voteDate, today);
                if (voteDate == today) {
                    return true;
                }
            }
        }
    }
    
    return false;
}

void UserDataManager::saveVoteData(const char *keyName, const char *voteKey, Value voteValue, int voteCount)
{
    auto voteDataValue = map[VoteKey_VoteData];
    
    ValueMap voteDataMap;
    if (!voteDataValue.isNull()) {
        voteDataMap = voteDataValue.asValueMap();
    }
    else {//ない場合はデフォルトをセット
        map[VoteKey_VoteData] = voteDataMap;
    }
    
    //version.plistのkey_name(TEST_VOTE)
    ValueVector keyNameVector;
    if (!voteDataMap[keyName].isNull()) {
        keyNameVector = voteDataMap[keyName].asValueVector();
    }
    
    //保存される投票データ
    ValueMap savedMap;
    double timeStamp = FireBaseBridge::getInstance()->getServerTimestamp();
    long long date = Common::dateWithTimeStamp(TimeUnit_Day, timeStamp);
    log("%lldに投票したことを記録します。",date);
    
    ValueVector timestamps;
    
    savedMap[VoteKey_Timestamp] = Value(timeStamp);
    savedMap[VoteKey_VoteCount] = Value(voteCount);
    savedMap[voteKey] = voteValue;
    keyNameVector.push_back(Value(savedMap));
    
    map[VoteKey_VoteData].asValueMap()[keyName] = Value(keyNameVector);
    
    save();
}

#pragma mark - 保存
void UserDataManager::save()
{
    //plistにして保存
    auto dataListPath=getDataListPath();
    
    bool isExistDir = FileUtils::getInstance()->isDirectoryExist(dataListPath);
    if (!isExistDir) {
        log("ディレクトリを作成します");

        FileUtils::getInstance()->createDirectory(dataListPath);
    }
    
    auto fullPath = dataListPath + FileName_UserRecordData;
    auto success=FileUtils::getInstance()->writeValueMapToFile(map, fullPath);
    
    ValueHelper::dumpValue(Value(map));
    log("フルパス map:%ld ユーザplistを保存 : %d : %s",map.size(),success,fullPath.c_str());

    //postし直す.マイグレーションが済んでからポストすればいい。
    //postUserData();
}

std::string UserDataManager::getDataListPath()
{
    return StringUtils::format("%s%s",FileUtils::getInstance()->getWritablePath().c_str(), DataListsDir);
}

#pragma mark - ポスト
void UserDataManager::postUserData()
{
    if (getUserID().size()==0) {
        return;
    }
    
    auto dir = StringUtils::format("%s_%s%s/", Users, FirebaseDownloadDir, getUserID().c_str());
     
     std::map<std::string, int>map={
         {KeyName_Coin,getCoin()},
         {KeyName_ClearStages,DataManager::sharedManager()->getTotalStageRecord(RecordKey_ClearGame)},
         {KeyName_LaunchCount,getLaunchCount()},
         {KeyName_UsedCoin,getUsedCoin()},

         //{"paid",getIsPaidUser()},
         //{"gotDailyBonusDate",getGotDailyBonusDate()}
         //{"prePlay",getNowPoint(Achievement_PrePlay)},
         //{"timestamp",FireBaseBridge::getInstance()->getServerTimestamp()}
     };
     
     log("ユーザーズのデータは%s",dir.c_str());
     //丸ごと上書き
     FireBaseBridge::getInstance()->setValue(dir, map, [](bool successed){
     log("トランザクションは終わりです。%d",successed);
     });
}
