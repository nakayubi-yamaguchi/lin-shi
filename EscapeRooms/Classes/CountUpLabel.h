//
//  CountUpLabel.h
//  ObjectRoom
//
//  Created by 成田凌平 on 2017/02/01.
//
//

#ifndef __ObjectRoom__CountUpLabel__
#define __ObjectRoom__CountUpLabel__

#include "cocos2d.h"
USING_NS_CC;

class CountUpLabel:public Label
{
public:
    static CountUpLabel*create(ValueMap map,Value answerMapV,Size parentSize,int number,int backNum,std::string _fontPath,float _fontSize,bool useBM);
    static CountUpLabel*createWithBM(ValueMap map,Value answerMapV,Size parentSize,int number,int backNum,std::string _fontPath,float _fontSize);

    bool init(ValueMap map,Value answerMapV,Size parentSize,int number,int backNum,std::string _fontPath,float _fontSize,bool useBM);

    void setStrings(std::vector<std::string>vec);
    void checkMemory();
    void countUp();
    
    void setOutline(ValueMap map);
    
    CC_SYNTHESIZE(int, number, Number);//何番目のラベルか
    CC_SYNTHESIZE(int, backNum, BackNum);//表示する背景ID
    CC_SYNTHESIZE(bool, memory, Memory);//インデックスの変更をプレイ中に保持するか
    CC_SYNTHESIZE(std::string, playSE, PlaySE);//タッチ時の効果音
    
    std::string getTemporaryKey();

private:
    std::vector<std::string>strings;
};

#endif /* defined(__ObjectRoom__CountUpLabel__) */
