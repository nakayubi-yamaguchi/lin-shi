//
//  ClearScene.cpp
//
//  Created by yamaguchinarita on 2016/12/22.

#include "ClearScene.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "ReviewLayer.h"
#include "Utils/FireBaseBridge.h"
#include "DataManager.h"
#include "EscapeDataManager.h"
#include "UIParts/MenuItemScale.h"

#include "Utils/CustomAlert.h"
#include "Utils/NativeAdManager.h"
#include "Utils/UtilsMethods.h"
#include "EscapeLoadingScene.h"
#include "ConfirmAlertLayer.h"
#include "Utils/ContentsAlert.h"
#include "ShopMenuLayer.h"
#include "Utils/RewardMovieManager.h"
#include "Utils/NotificationBridge.h"
#include "AnalyticsManager.h"
#include "EscapeStageSprite.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"

#define UserDefaultKey_ClearMinigameMovie "clearminigamemovie"

using namespace cocos2d;
Scene* ClearScene::createScene() {
	auto scene = Scene::create();
	auto layer = ClearScene::create();
	scene->addChild(layer);
	
    return scene;
}

bool ClearScene::init() {
    if (!LayerColor::initWithColor(Color4B::WHITE)) {
        return false;
    }
    
    //通知を登録する前にこれまで登録した分を削除する。
    for (int i=NotificationTag_Login; i<NotificationTag_Max; i++) {
        NotificationBridge::cancelLocalNotification(i);
    }
    
    DataManager::sharedManager()->record(RecordType_StopDate);//記録を止める
    
    sendClearData();
    
    createSuccessObject();
    createNextButton();
    createShareMenu();
    createParticle();
    
    //動画ネイティブを非表示にする
    SupplementsManager::getInstance()->hidePanel();

    return true;
}

// トランジション終了時に呼ばれる
void ClearScene::onEnterTransitionDidFinish()
{
    Common::playSE("clap.mp3");
}

#pragma mark - FireBase
void ClearScene::sendClearData()
{
    //送信履歴がない新規ユーザーのみ送信されます。ファネル分析ようの初クリアデータを送信
    if (EscapeDataManager::getInstance()->getTypePlayTitleCondition() != TitleCondition_TUTORIAL) {
        AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_ClearFirstStage);
    }
    
    //ステージごとのクリアデータを記録 １ユーザあたり一回しか送りません
    if (!DataManager::sharedManager()->getEternalRecordDataOnPlay(RecordKey_ClearGame).asBool()) {
        
        DataManager::sharedManager()->recordEternalDataOnPlay(RecordKey_ClearGame, Value(true));
        //log("プレイステージのクリア状態は%d",DataManager::sharedManager()->getEternalRecordDataOnPlay(RecordKey_ClearGame).asBool());

        //ユーザデータの書き換え
        DataManager::sharedManager()->postUserData();
        
        auto titleName = EscapeDataManager::getInstance()->getPlayTitleName();
        std::transform(titleName.begin(), titleName.begin()+1, titleName.begin(), ::toupper);
        auto fbDir = StringUtils::format("%s%s/%s/",FirebaseDownloadDir, ClearData, titleName.c_str());
        
        std::map<std::string, int>map={
            {"time",round(DataManager::sharedManager()->getRecordData(RecordType_StopDate).asFloat()/60)},//分単位で四捨五入するで
            {"players",1},
            {"hint",DataManager::sharedManager()->getRecordData(RecordType_Hint).asInt()}
        };
        
        FireBaseBridge::getInstance()->transactionPost(fbDir, map,[](bool successed){
            log("トランザクションは終わりです。%d",successed);
        });
    }
    else if(DataManager::sharedManager()->isPlayingMinigame() && !DataManager::sharedManager()->getEternalRecordDataOnPlay(RecordKey_ClearMiniGame).asBool()){
        DataManager::sharedManager()->recordEternalDataOnPlay(RecordKey_ClearMiniGame, Value(true));
    }
}

#pragma mark -
void ClearScene::createSuccessObject()
{    
    auto objectSprite = EscapeStageSprite::createWithSpriteFileName("room_image_1.png");
    
    objectSprite->setScale(getContentSize().height*.4/objectSprite->getContentSize().height);
    objectSprite->setPosition(getContentSize().width/2, getContentSize().height*.65);
    
    addChild(objectSprite);
    
    //カスタムアクション
    auto customActionManager = SupplementsManager::getInstance()->getCustomActionManager();
    customActionManager->customAction(CustomAction_Clear, objectSprite);
    
    //おめでとうラベル
    auto congratulationsLabel = Label::createWithTTF("Congratulations!", MyFont, objectSprite->getContentSize().height*.125);
    congratulationsLabel->setTextColor(Color4B::BLACK);
    congratulationsLabel->setPosition(objectSprite->getContentSize().width/2,objectSprite->getContentSize().height+congratulationsLabel->getContentSize().height/2);
    objectSprite->addChild(congratulationsLabel);

    //記録ラベル
    auto time=DataManager::sharedManager()->getRecordData(RecordType_StartDate).asInt();
    auto hint=DataManager::sharedManager()->getRecordData(RecordType_Hint).asInt();
    auto string=StringUtils::format("Time %02d:%02d:%02d\nHint     %d",time/3600,(time/60)%60,time%60,hint);
    auto recordTitleLabel=Label::createWithTTF(string,MyFont, objectSprite->getContentSize().height*.1);
    recordTitleLabel->setTextColor(Color4B::BLACK);
    recordTitleLabel->setPosition(objectSprite->getContentSize().width/2, -recordTitleLabel->getContentSize().height*.75);
    objectSprite->addChild(recordTitleLabel);
}

void ClearScene::createParticle()
{
    auto customManager=SupplementsManager::getInstance()->getCustomActionManager();
    customManager->clearParticleAction(this);
}

void ClearScene::createNextButton()
{
    auto titleType = EscapeDataManager::getInstance()->getTypePlayTitleCondition();
    std::string labelText = "NEXT!";
    if (titleType == TitleCondition_TUTORIAL) labelText = "!CLOSE!";
    
    auto sprite=Sprite::create();
    sprite->setColor(Color3B::ORANGE);
    sprite->setName("next");
    sprite->setTextureRect(Rect(0, 0, getContentSize().width*.4, getContentSize().width*.1));
    auto label=Label::createWithTTF(labelText, MyFont, getContentSize().width*.05);
    label->setAlignment(TextHAlignment::CENTER);
    label->setPosition(sprite->getContentSize()/2);
    sprite->addChild(label);
    
    auto menuitem=MenuItemScale::create(sprite, sprite, [this,titleType](Ref*ref){
        if (titleType == TitleCondition_TUTORIAL) {
            
            //通知のアラートがまだ出ていない場合に全データダウンロードのゆるい設定。必ずしもダウンロードしなくていいからね。
            if (UserDefault::getInstance()->getBoolForKey("RegistNotification") == false) {
                
                auto needDLSize = EscapeDataManager::getInstance()->getNeedDownloadTitleIndexVector();
                auto message = DataManager::sharedManager()->getSystemMessage((needDLSize.size()==0)? "tutorial_clear": "tutorial_clear_withDL");

                auto alert = CustomAlert::create(AlertTitle_INFORMATION, message.c_str(), {"OK"}, [](Ref *sender){
                    Common::stopBgm();
                    //AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_CloseClearScene);
                    Director::getInstance()->replaceScene(TransitionFade::create(.5, EscapeLoadingScene::createScene({LoadingType_RemoveCathe, LoadingType_AllLoadTitle}),Color3B::WHITE));
                });
                alert->showAlert(this);
            }
            else {
                Common::stopBgm();
                Director::getInstance()->replaceScene(TransitionFade::create(.5, EscapeLoadingScene::createScene(LoadingType_RemoveCathe),Color3B::WHITE));
            }
        }
        else {
            this->showStarReviewLayer();
        }
    });
    
    auto menu=Menu::create(menuitem, NULL);
    menu->setPosition(getContentSize().width/2,getContentSize().height*.275);

    addChild(menu);
}

void ClearScene::createShareMenu()
{
    std::vector<std::string>filenames;
    filenames.push_back("twitter.png");
    filenames.push_back("facebook.png");
    filenames.push_back("line.png");
    
    auto isFriend = UserDefault::getInstance()->getBoolForKey(Promote_Key_IsLINEAtFriend);
    auto iconFileName = Common::localize("", "line@icon.png", "", "", "kakaoicon.png");
    if (iconFileName.size() != 0 && !isFriend)
            filenames.push_back(iconFileName);
    
    float iconSize=getContentSize().height*.08;
    
    Vector<MenuItem*> shareMenuItems;
    for (int i=0; i<filenames.size(); i++) {
        
        auto normalSprite=Sprite::create(filenames[i]);
        auto selectedSprite=Sprite::create(filenames[i]);
        selectedSprite->setOpacity(200);
        
        auto shareMenuItem=MenuItemSprite::create(normalSprite, selectedSprite, [this](Ref*sender){
            
            auto senderItem=(MenuItemSprite*)sender;
            auto appName=DataManager::sharedManager()->getAppName();
            auto localize=Common::localize(StringUtils::format("%s！",appName.c_str()), StringUtils::format("「%s」からの脱出成功！",appName.c_str()));
            auto localize_escape=Common::localize("Escape", "脱出ゲーム");
            auto localize_corporation=Common::localize("NAKAYUBI", "ナカユビのアプリ");
            auto text=StringUtils::format("%s\n\n%s\n\n#%s #%s #%s #EscapeRooms",localize.c_str(),MyURL, localize_escape.c_str(),DataManager::sharedManager()->getHashTag().c_str(),localize_corporation.c_str());
            
            switch (senderItem->getTag()) {
                case 0:
                    Common::shareWithImage(text.c_str(), ShareType_Twitter);
                    //分析
                    AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName("Share_twitter");

                    break;
                case 1:
                    Common::shareWithImage(text.c_str(), ShareType_FaceBook);
                    AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName("Share_facebook");
                    break;
                case 2:
                    NativeBridge::openWithLINEText(text);
                    AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName("Share_line");
                    break;
                    
                case 3:
                    log("ライン＠のボタンがおされました。");
                    UtilsMethods::showPromoteBecomingFriendsPopup();
                    break;
                    
                default:
                    break;
            }
            
        });
        shareMenuItem->setScale(iconSize/shareMenuItem->getBoundingBox().size.height);
        shareMenuItem->setTag(i);
        
        shareMenuItems.pushBack(shareMenuItem);
    }
    
    auto shareMenu=Menu::createWithArray(shareMenuItems);
    shareMenu->setPosition(getContentSize().width/2 ,BannerBridge::getBannerHeight()+iconSize);
    shareMenu->alignItemsHorizontallyWithPadding(iconSize*.25);
    addChild(shareMenu);
}

#pragma mark - レビューレイヤを表示しようとする アラート表示
void ClearScene::showAlertBeforeReviewLayer()
{
    //動画の準備フラグ
    bool isPrepared = RewardMovieManager::sharedManager()->isPrepared();

    if (DataManager::sharedManager()->isPlayingMinigame())
    {//ミニゲームクリア 報酬アラート 4.2で廃止
        /*auto key=StringUtils::format("%s_%s",UserDefaultKey_ClearMinigameMovie,EscapeDataManager::getInstance()->getPlayTitleName().c_str());
        if (!UserDefault::getInstance()->getBoolForKey(key.c_str())) {
            auto videoMessage= DataManager::sharedManager()->getSystemMessage("getCoin");
            auto closeMessage= DataManager::sharedManager()->getSystemMessage("close");
            auto message=DataManager::sharedManager()->getSystemMessage("clearMinigame");
            
            auto alert = ConfirmAlertLayer::create(ConfirmAlertType_Twice, message, [isPrepared,this,key](Ref*sender){
                auto al = (CustomAlert*)sender;
                int selectedNum = al->getSelectedButtonNum();
                
                if (selectedNum == 0) {
                    this->displayReviewLayer();
                }
                else {//ビデオボタン
                    if (isPrepared) {//動画準備完了
                        RewardMovieManager::sharedManager()->play([this,key](bool complete){
                            if (complete) {//成功
                                log("クリアボーナスの受け取りを記憶 %s",key.c_str());
                                AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_GetMinigameBonus);
                                UserDefault::getInstance()->setBoolForKey(key.c_str(), true);
                                DataManager::sharedManager()->addCoin(5);
                                auto message=StringUtils::format(DataManager::sharedManager()->getSystemMessage("getCoinWithVideo").c_str(),"5");
                                auto alert=CustomAlert::create("", message.c_str(), {DataManager::sharedManager()->getSystemMessage("close")}, [this](Ref*ref){
                                    displayReviewLayer();
                                });
                                alert->showAlert(this);
                            }
                            else{
                                auto alert = ContentsAlert::create("failed.png", DataManager::sharedManager()->getSystemMessage("error"), DataManager::sharedManager()->getSystemMessage("video_error_not_end"), {"OK"}, [this](Ref* sender){
                                    this->showAlertBeforeReviewLayer();
                                });
                                alert->showAlert(this);
                            }
                        });
                    }
                    else {
                        auto alert = ContentsAlert::create("failed.png", DataManager::sharedManager()->getSystemMessage("error"), DataManager::sharedManager()->getSystemMessage("no_video"), {"OK"}, [this](Ref* sender){
                            this->showAlertBeforeReviewLayer();
                        });
                        alert->showAlert(this);
                    }
                }
            });
            
            alert->showAlert();
        }
        else{*/
            displayReviewLayer();
        //}
    }
    else
    {//ミニゲーム解放アラート
        int useCoinNum = CoinSpot_Minigame;
        auto videoButton = DataManager::sharedManager()->getSystemMessage("view_video_button");
        auto usingCoin = DataManager::sharedManager()->getSystemMessage("usingCoin");
        auto message = StringUtils::format(DataManager::sharedManager()->getSystemMessage("promoteMinigame").c_str(), DataManager::sharedManager()->getCoinSt().c_str(), useCoinNum);
        
        auto alert = CustomAlert::create(AlertTitle_INFORMATION, message.c_str(), {videoButton, usingCoin}, [this, useCoinNum,isPrepared](Ref* sender){
            auto al = (CustomAlert*)sender;
            int selectedNum = al->getSelectedButtonNum();
            
            if (selectedNum == 0) {//ビデオボタン
                if (isPrepared) {//動画準備完了
                    RewardMovieManager::sharedManager()->play([this](bool complete){
                        if (complete) {//成功
                            AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_GetMinigame_ByMovie);
                            displayReviewLayer();
                        }
                        else{
                            auto alert = ContentsAlert::create("failed.png", DataManager::sharedManager()->getSystemMessage("error"), DataManager::sharedManager()->getSystemMessage("video_error_not_end"), {"OK"}, [this](Ref* sender){
                                this->showAlertBeforeReviewLayer();
                            });
                            alert->showAlert();
                        }
                    });
                }
                else {
                    auto message = DataManager::sharedManager()->getSystemMessage("no_video");
                    if (Common::isAndroid()) {
                        message += DataManager::sharedManager()->getSystemMessage("no_video_android");
                    }
                    
                    auto alert = ContentsAlert::create("failed.png", DataManager::sharedManager()->getSystemMessage("error"), message.c_str(), {"OK"}, [this](Ref* sender){
                        this->showAlertBeforeReviewLayer();
                    });
                    alert->showAlert();
                }
            }
            else {//コイン使用
                bool enoughCoin = DataManager::sharedManager()->isEnoughCoin(useCoinNum);
                if (enoughCoin) {//コインが十分にある
                    auto confirmSt = DataManager::sharedManager()->getSystemMessage("confirm");
                    auto preConfirmSt = StringUtils::format(DataManager::sharedManager()->getSystemMessage("minigameConfirmText").c_str(), useCoinNum);
                    
                    auto confirmAl = CustomAlert::create(confirmSt.c_str(), preConfirmSt.c_str(), {"NO", "YES"}, [this, useCoinNum](Ref* sender){
                        
                        auto al = (ContentsAlert*)sender;
                        auto selectedNum = al->getSelectedButtonNum();
                        
                        if (selectedNum == 0) {
                            this->showAlertBeforeReviewLayer();
                        }
                        else if (selectedNum == 1) {//コイン使って、ミニゲームスタート
                            //コインを使います。
                            AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_GetMinigame_ByCoin);

                            DataManager::sharedManager()->useCoin(useCoinNum);
                            
                            auto useCoinSt = DataManager::sharedManager()->getSystemMessage("useCoinWithMinigame");
                            UtilsMethods::showThanksAlert(useCoinSt.c_str(), [this](Ref* sender){
                                displayReviewLayer();
                            });
                        }
                    });
                    confirmAl->showAlert(this);
                }
                else{//コインが足りない
                    auto closeSt = DataManager::sharedManager()->getSystemMessage("close");
                    auto shopSt = DataManager::sharedManager()->getSystemMessage("shop");
                    auto shortageSt = DataManager::sharedManager()->getSystemMessage("shortageCoin");
                    
                    auto shortageAlert = ContentsAlert::create("shortage.png", AlertTitle_INFORMATION, shortageSt.c_str(), {closeSt.c_str(), shopSt.c_str()}, [this](Ref* sender){
                        
                        auto al = (ContentsAlert*)sender;
                        auto selectedNum = al->getSelectedButtonNum();
                        
                        if (selectedNum == 0) {
                            this->showAlertBeforeReviewLayer();
                        }
                        else if (selectedNum == 1) {
                            auto shopLayer = ShopMenuLayer::create([this](Ref* sender){
                                this->showAlertBeforeReviewLayer();
                            });
                            shopLayer->showAlert(this);
                        }
                    });
                    shortageAlert->showAlert(this);
                }
            }
        });
        alert->displayCloseButton([this](){

            auto closeAlert = ConfirmAlertLayer::create(ConfirmAlertType_Twice, DataManager::sharedManager()->getSystemMessage("backtotop").c_str(), [this](Ref* sender){
                auto al = (ContentsAlert*)sender;
                auto selectedNum = al->getSelectedButtonNum();
                
                if (selectedNum == 0) {//閉じない
                    this->showAlertBeforeReviewLayer();
                }
                else if (selectedNum == 1) {//閉じる。
                    Common::stopBgm();
                    Director::getInstance()->replaceScene(TransitionFade::create(.5,EscapeLoadingScene::createScene(LoadingType_RemoveCathe),Color3B::WHITE));
                }
            });
            
            closeAlert->showAlert(this);
        });
        alert->showAlert(this);
    }
}

void ClearScene::displayReviewLayer()
{
    auto reviewLayer = ReviewLayer::create();
    this->addChild(reviewLayer);
}

void ClearScene::showStarReviewLayer()
{
    auto star = StarReviewLayer::create();
    star->setDelegate(this);
    this->addChild(star);
}

#pragma mark - StarReview Delegate
void ClearScene::didClosedStarsReviewLayer()
{
    auto pastReview = DataManager::sharedManager()->getEternalRecordDataOnPlay(RecordKey_Review).asInt();
        
    static int count = 0;
    if (pastReview>=4 && count%4 == 0) {//まずはテストでios10.3と11でレビュー促進.1起動で4回に1回レビューを頼む
        if (NativeBridge::requestReview()) {//リクエストレビュー書く時はレビューレイヤーが速攻出ていい。iosのみ
            showAlertBeforeReviewLayer();
        }
        else if(!UserDefault::getInstance()->getBoolForKey("Review")) {//レビュー促進アラート:試しに一度だけ表示する。
            auto title = Common::localize("Please", "お願い", "请", "請", "부탁");
            auto message = Common::localize("How was the application? If you do not mind, please evaluate the application as well.", "\"Escape Rooms\"はいかがでしたか？よろしければアプリにも評価をお願いいたします。", "应用程序如何？ 如果您不介意，请评估该应用程序。", "應用程序如何？ 如果您不介意，請評估該應用程序。", "응용 프로그램은 어땠습니까? 괜찮 으시면 앱도 평가를 부탁드립니다.");
            
            auto promoteReview = CustomAlert::create(title, message, {"CLOSE","OK"}, [this](Ref* sender){
                auto alert = (CustomAlert*)sender;
                auto selectedNum = alert->getSelectedButtonNum();

                if (selectedNum == 1) {//OK
                    auto url = Common::getReviewUrl();
                    Application::getInstance()->openURL(MyURL);
                    
                    UserDefault::getInstance()->setBoolForKey("Review", true);
                }
                //ボタンをタップしたらレビューレイヤー表示
                this->showAlertBeforeReviewLayer();
            });
            promoteReview->setCloseDuration(.5);
            promoteReview->showAlert(this);
        }
    }
    else {//レビュー促進しないので、速攻レビューレイヤー表示
        showAlertBeforeReviewLayer();
    }
    
    count++;
}

#pragma mark- 解放
ClearScene::~ClearScene()
{
    log("クリアシーンを解放します。");
}

