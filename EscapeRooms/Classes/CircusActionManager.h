//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__CircusActionManager__
#define __EscapeContainer__CircusActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class CircusActionManager:public CustomActionManager
{
public:
    static CircusActionManager* manager;
    static CircusActionManager* getInstance();
    
    //over ride
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void customAction(std::string key,Node*parent);
    void storyAction(std::string key,Node*parent,const onFinished& callback);
    void showAnswerAction(std::string key, cocos2d::Node *parent);
    
private:
    FiniteTimeAction* moveHatAction(Node*node,Node*parent);
    FiniteTimeAction* movePillarAction(Node*parent,Node*pillar,int backID,bool animate);
    FiniteTimeAction* hammerGaugeAction(Node*parent,int backID,bool animate);
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
