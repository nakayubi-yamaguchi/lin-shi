//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "NakayubiCorporationActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"

using namespace cocos2d;

NakayubiCorporationActionManager* NakayubiCorporationActionManager::manager =NULL;

NakayubiCorporationActionManager* NakayubiCorporationActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new NakayubiCorporationActionManager();
    }
    return manager;
}

#pragma mark - Back
void NakayubiCorporationActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="stomach")
    {
        Vector<FiniteTimeAction*>actions;
        std::vector<Vec2>vecs={Vec2(.3, .33),Vec2(.375, .4),Vec2(.54, .4)};
        auto duration=.5;
        for (int i=0;i<3;i++) {
            auto sup=AnchorSprite::create(vecs.at(i), parent->getContentSize(), true, StringUtils::format("sup_62_%d.png",i));
            parent->addChild(sup);
            
            auto scale_oridinal=sup->getScale();
            sup->setScale(scale_oridinal*.1);
            
            auto seq=TargetedAction::create(sup,Sequence::create(Spawn::create(FadeIn::create(duration*.5),
                                                                               EaseIn::create(ScaleTo::create(duration, scale_oridinal), 1.5), NULL),
                                                                 createSoundAction("hungry.mp3"),
                                                                 DelayTime::create(duration*.5),
                                                                 Spawn::create(FadeOut::create(duration*.5),
                                                                                NULL),
                                                                 ScaleTo::create(.1, scale_oridinal*.1),
                                                                 DelayTime::create(.5), NULL));
            
            actions.pushBack(seq);
        }
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),actions.at(0),actions.at(1),actions.at(2),actions.at(0)->clone(),actions.at(2)->clone(),actions.at(1)->clone(),DelayTime::create(1), NULL),UINT_MAX));
    }
    else if(key=="back_26"&&DataManager::sharedManager()->getEnableFlagWithFlagID(20)){
        auto roulette=createRoulette(parent);
        roulette->setRotation(36);
        parent->addChild(roulette);
    }
    
}

#pragma mark - Touch
void NakayubiCorporationActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="eraser")
    {//黒板消し
        Common::playSE("p.mp3");
        
        auto stain=parent->getChildByName<Sprite*>("sup_15_dirt.png");
        auto mark=createSpriteToCenter("sup_15_mark.png", true, parent);
        parent->addChild(mark);
        
        auto item=AnchorSprite::create(Vec2(.51, .4), parent->getContentSize(),"anim_15_eraser.png");
        item->setOpacity(0);
        parent->addChild(item);
        
        //fadein
        auto fadein=FadeIn::create(1);
        auto sound=CallFunc::create([this]{
            Common::playSE("goshigoshi.mp3");
        });
        //move
        auto distance=parent->getContentSize().width*.05;
        auto duration=.2;
        Vector<FiniteTimeAction*>moves;
        for (int i=1; i<6; i++) {
            auto move=MoveBy::create(duration+i*.025, Vec2(distance*i*pow(-1, i)+distance*.8*(i/2), distance*.75*i*pow(-1, i)-distance*.25*(i/2)));
            moves.pushBack(move);
        }
        
        auto seq=TargetedAction::create(item, Sequence::create(fadein,sound,Sequence::create(moves), NULL));
        auto change=Spawn::create(TargetedAction::create(item, FadeOut::create(.7)),
                                  createChangeAction(1, .7, stain, mark), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq,change,DelayTime::create(.5),call,NULL));
    }
    else if(key=="othello"){
        Common::playSE("p.mp3");
        //オセロアニメ
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<3; i++) {
            auto sup=createSpriteToCenter(StringUtils::format("anim_18_%d.png",i), true, parent);
            parent->addChild(sup);
            
            actions.pushBack(TargetedAction::create(sup, Sequence::create(FadeIn::create(.7),
                                                                          createSoundAction("kacha.mp3"),
                                                                          DelayTime::create(1), NULL)));
        }
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="bread"){
        //トースター正解
        auto bread=parent->getChildByName<Sprite*>("sup_48_bread.png");
        auto baked=createSpriteToCenter("sup_48_baked_bread.png", true, parent);
        baked->setPositionY(parent->getContentSize().height*.175);
        parent->addChild(baked);
        
        auto cover=createSpriteToCenter("sup_48_cover.png", false, parent);
        cover->setLocalZOrder(1);
        parent->addChild(cover);
        
        auto delay=DelayTime::create(1);
        auto change=Spawn::create(createChangeAction(2, 1.5, bread, baked),
                                  createSoundAction("timer.mp3"),
                                  CallFunc::create([parent]{
            auto particle=ParticleSystemQuad::create("smoke.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width/2, parent->getContentSize().height*.6);
            particle->setPosVar(Vec2(parent->getContentSize().width*.2, 0));
            particle->setSpeed(parent->getContentSize().height*.1);
            particle->setTotalParticles(50);
            particle->setAngle(90);
            particle->setAngleVar(10);
            particle->setDuration(3);
            particle->setLife(2);
            particle->setStartSize(parent->getContentSize().width*.1);
            particle->setEndSize(parent->getContentSize().width*.2);
            parent->addChild(particle);
        }), NULL);
        auto baked_action=TargetedAction::create(baked, Sequence::create(change,
                                                                         DelayTime::create(2),
                                                                         createSoundAction("tin.mp3"),
                                                                         EaseOut::create(MoveTo::create(.2, parent->getContentSize()/2), 2), NULL));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(delay,baked_action,delay->clone(),call, NULL));
    }
    else if(key=="openwork"){
        //作業部屋を開く
        auto back=createSpriteToCenter("back_4.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        SupplementsManager::getInstance()->addSupplementToMain(back, 4);
        
        auto open=createSpriteToCenter("sup_4_open.png", true, back);
        open->setCascadeOpacityEnabled(true);
        back->addChild(open);
        
        auto pig=createSpriteToCenter("sup_4_pig_0.png", false, parent);
        open->addChild(pig);
        
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto fadein_open=TargetedAction::create(open,Spawn::create(FadeIn::create(1),
                                                                   createSoundAction("su.mp3"), NULL));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_back,delay,fadein_open,delay->clone(),call, NULL));
    }
    else if(key=="printer")
    {//プリンター
        Common::playSE("p.mp3");
        auto paper_0=createSpriteToCenter("sup_36_paper_0.png", true, parent);
        parent->addChild(paper_0);
        
        auto paper_1=createSpriteToCenter("sup_36_paper_1.png", true, parent);
        paper_1->setPositionY(parent->getContentSize().height*.65);
        parent->addChild(paper_1);
        auto scale_oridinal=paper_1->getScale();
        paper_1->setScaleY(scale_oridinal*.5);
        
        auto distance_0=parent->getContentSize().height*.05;
        auto repeat_count=5;
        
        auto fadein_0=TargetedAction::create(paper_0, FadeIn::create(1));
        auto move_0=TargetedAction::create(paper_0, Repeat::create(Sequence::create(DelayTime::create(.5),CallFunc::create([]{Common::playSE("printer_0.mp3");}),Spawn::create(MoveBy::create(.2, Vec2(0, -distance_0/repeat_count)),ScaleBy::create(.2, 1,.75), NULL), NULL), repeat_count));
        
        auto fadein_1=TargetedAction::create(paper_1, FadeIn::create(2));//delay混み
        auto sound=CallFunc::create([]{Common::playSE("printer_1.mp3");});
        auto move_1=TargetedAction::create(paper_1,EaseOut::create(Spawn::create(MoveTo::create(1.5, parent->getContentSize()/2),
                                                                                 ScaleTo::create(1.5, scale_oridinal), NULL),1.5));
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_0,move_0,fadein_1,sound,move_1,DelayTime::create(1),call, NULL));
    }
    else if(key=="kettle"){
        auto kettle=AnchorSprite::create(Vec2(.25, .54), parent->getContentSize(), "sup_57_kettle.png");
        kettle->setOpacity(0);
        parent->addChild(kettle);
        
        //
        auto angle=-20;
        auto distance_x=parent->getContentSize().width*.2;
        
        auto fadein_kettle=TargetedAction::create(kettle,FadeIn::create(1));
        auto rotate_kettle=TargetedAction::create(kettle,Sequence::create(EaseIn::create(RotateBy::create(1, angle), 1.5),
                                                                                    createSoundAction("jobojobo.mp3"),
                                                                          DelayTime::create(1),
                                                                                    EaseIn::create(RotateBy::create(.4, -angle),1.5), NULL));
        
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<4; i++) {
            actions.pushBack(rotate_kettle->clone());
            
            auto sup=createSpriteToCenter(StringUtils::format("sup_57_%d.png",i), true, parent);
            parent->addChild(sup);
            auto fadein=TargetedAction::create(sup, FadeIn::create(.2));
            actions.pushBack(fadein);
            
            auto move=TargetedAction::create(kettle,MoveBy::create(.5, Vec2(distance_x, 0)));
            actions.pushBack(move);
        }
        
        auto seq_kettle=Sequence::create(actions);
        auto fadeout=TargetedAction::create(kettle, FadeOut::create(.5));
        auto delay=DelayTime::create(.5);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_kettle,seq_kettle,fadeout,delay,call, NULL));
    }
    else if(key=="conro"){
        //コンロ正解
        auto back=createSpriteToCenter("back_31.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 31);
        
        auto kettle=back->getChildByName("sup_31_kettle.png");
        kettle->setLocalZOrder(1);
        auto shade=back->getChildByName("sup_31_shade.png");
        
        //fire
        auto batch=ParticleBatchNode::create("particle_fire.png");
        batch->setCascadeOpacityEnabled(true);
        back->addChild(batch);
        
        auto fadein=TargetedAction::create(back, FadeIn::create(2));
        auto delay=DelayTime::create(1);
        auto fire=CallFunc::create([batch,back]{
            Common::playSE("huo.mp3");
            
            auto fire=ParticleSystemQuad::create("fire.plist");
            fire->setName("particle");
            fire->setAutoRemoveOnFinish(true);
            fire->setTotalParticles(200);
            fire->setStartColor(Color4F(.2, .2, .8, 1));
            fire->setEndColor(Color4F(.2, .2, .8, 0));
            fire->setStartSize(back->getContentSize().width*.06);
            fire->setEndSize(back->getContentSize().width*.1);
            fire->setLife(.2);
            fire->setSpeed(back->getContentSize().height*.1);
            fire->setAngle(90);
            fire->setAngleVar(45);
            fire->setPosVar(Vec2(back->getContentSize().width*.05, back->getContentSize().height*.02));
            fire->setPosition(back->getContentSize().width*.325,back->getContentSize().height*.48);
            batch->addChild(fire);
        });
        auto fadeout=Spawn::create(TargetedAction::create(kettle, FadeOut::create(1)),
                                   TargetedAction::create(shade, FadeOut::create(1)),
                                    NULL);
        auto stopfire=CallFunc::create([batch]{
            auto fire=batch->getChildByName<ParticleSystemQuad*>("particle");
            if (fire) {
                fire->setDuration(.5);
            }
        });
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,fire,DelayTime::create(3),fadeout,stopfire,delay->clone(),call, NULL));
    }
    else if(key=="aircon")
    {//
        Common::playSE("pi_3.mp3");
        auto on=createSpriteToCenter("sup_32_on.png", true, parent);
        parent->addChild(on);
        
        //成田顔面
        auto back=createSpriteToCenter("back_63.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        SupplementsManager::getInstance()->addSupplementToMain(back, 63,true);
        //SupplementsManager::getInstance()->addAnchorSpritesToMain(back, 63);
        
        //pcをon
        auto pc_anime=createSpriteToCenter("back_26.png", true, parent);
        pc_anime->setCascadeOpacityEnabled(true);
        parent->addChild(pc_anime);
        auto screen=createSpriteToCenter("sup_26_screen.png", true, parent);
        pc_anime->addChild(screen);
        auto arm=AnchorSprite::create(Vec2(1, .25), parent->getContentSize(), true,"sup_26_arm.png");
        pc_anime->addChild(arm);
        
        auto sweat=back->getChildByName<Sprite*>("sup_63_sweat.png");
        auto left=back->getChildByName("sup_63_left.png");
        auto right=back->getChildByName("sup_63_right.png");
        auto angle=15;
        
        
        auto delay=DelayTime::create(.5);
        auto on_fadein=Spawn::create(TargetedAction::create(on, FadeIn::create(1)),
                                     createSoundAction("weeen.mp3"), NULL);
        auto delay_1=DelayTime::create(1);
        auto back_fadein=TargetedAction::create(back, FadeIn::create(1));
        auto sweat_fadeout=Sequence::create(TargetedAction::create(sweat, FadeOut::create(1)),
                                         createSoundAction("pinpon.mp3"), NULL);
        auto arm_action=Spawn::create(swingAction(angle, .2, left, 1.5,true,2),
                                      swingAction(-angle, .2, right, 1.5, true,2),
                                      createSoundAction("pig.mp3"), NULL);
        auto vec=Vec2(parent->getContentSize().width*.1, -parent->getContentSize().height*.1);
        auto angle_1=-10;
        
        auto pc_fadein=TargetedAction::create(pc_anime, FadeIn::create(1));
        auto switch_action=TargetedAction::create(arm, Sequence::create(FadeIn::create(1),
                                                                        EaseIn::create(Spawn::create(RotateBy::create(.5, angle_1),
                                                                                                     MoveBy::create(.5, vec), NULL), 1.5),
                                                                        createSoundAction("keyboard.mp3"),
                                                                     DelayTime::create(.3),
                                                                        EaseIn::create(Spawn::create(RotateBy::create(.5, -angle_1),
                                                                                                     MoveBy::create(.5, Vec2(-vec.x, -vec.y)), NULL), 1.5),
                                                                        FadeOut::create(.5),
                                                                         NULL));
        auto remove_back=TargetedAction::create(back, RemoveSelf::create());
        auto screen_fadein=Sequence::create(TargetedAction::create(screen, FadeIn::create(.7)),
                                            createSoundAction("pinpon.mp3"), NULL);
        auto fadeout_back=TargetedAction::create(pc_anime, FadeOut::create(1));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(delay,on_fadein,delay_1,back_fadein,delay_1->clone(),sweat_fadeout,delay->clone(),arm_action,delay_1->clone(),pc_fadein,delay->clone(),switch_action,remove_back,delay_1->clone(),screen_fadein,delay_1->clone(),fadeout_back,delay_1->clone(),call, NULL));
    }
    else if(key=="pc"){
        auto duration_fadein=1;
        
        //back
        auto screen=createSpriteToCenter("sup_26_screen_1.png", true, parent);
        screen->setCascadeOpacityEnabled(true);
        parent->addChild(screen);
        
        //sup
        auto roulette=createRoulette(parent);
        screen->addChild(roulette);
        
        auto needle=createSpriteToCenter("sup_26_needle.png", false, parent);
        screen->addChild(needle);
        
        auto delay=DelayTime::create(1.2);
        
        auto fadein=TargetedAction::create(screen, FadeIn::create(duration_fadein));
        
        //rouletteスタート
        auto sound_drum=CallFunc::create([this](){
            Common::playSE("drumroll.mp3");//4s
        });
        
        auto angle=360*8+72/2;
        auto rotate=TargetedAction::create(roulette,EaseOut::create(RotateBy::create(4,angle),3));
        
        auto call=CallFunc::create([this,callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(DelayTime::create(.7),fadein,delay,sound_drum,rotate,delay->clone(),call,NULL));
    }
    else if(key=="female"){
        Common::playSE("koto.mp3");
        auto apple=createSpriteToCenter("anim_27_0.png", true, parent);
        parent->addChild(apple);
        auto apple_pig=createSpriteToCenter("sup_27_applepig.png", true, parent);
        parent->addChild(apple_pig);
        
        auto back=createSpriteToCenter("back_43.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        SupplementsManager::getInstance()->addSupplementToMain(back, 43);
        
        
        auto pig=back->getChildByName<Sprite*>("sup_43_pig.png");
        auto pig_1=createSpriteToCenter("anim_43_pig.png", true, parent);
        back->addChild(pig_1);
        
        auto fade_apple=TargetedAction::create(apple, FadeIn::create(.7));
        auto delay=DelayTime::create(1);
        auto fade_back=Spawn::create(TargetedAction::create(back, FadeIn::create(1)),
                                    NULL);
        auto change_apple=createChangeAction(.1, .1, apple, apple_pig);
        auto change_pig=Spawn::create(createChangeAction(1, .7, pig, pig_1),
                                      createSoundAction("pig_1.mp3"), NULL);
        auto fade_pig=TargetedAction::create(pig_1, FadeOut::create(1));
        
        auto fade_back_1=TargetedAction::create(back, FadeOut::create(2));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fade_apple,delay,fade_back,change_apple,delay->clone(),change_pig,DelayTime::create(.7),fade_pig,delay->clone(),createSoundAction("paku.mp3"),delay->clone(),fade_back_1,delay->clone(),call, NULL));
    }
    else if(key.compare(0,4,"wake")==0){
        Common::playSE("hit.mp3");
        auto index=atoi(key.substr(5,1).c_str());
        auto pig=parent->getChildByName<Sprite*>("sup_45_pig.png");
        auto correct=false;
        
        AnswerManager::getInstance()->appendPasscode(StringUtils::format("%d",index));
        if (AnswerManager::getInstance()->getPasscode()=="11010") {
            correct=true;
        }
        else if(AnswerManager::getInstance()->getPasscode().size()==5){
            AnswerManager::getInstance()->resetPasscode();
        }
        
        auto angle=-15*pow(-1, index);
        
        auto rotate=swingAction(angle, .1, pig, 1.2);

        auto call=CallFunc::create([callback,correct]{
            callback(correct);
        });
        parent->runAction(Sequence::create(rotate,call, NULL));
    }
    else if(key=="clear"){
        auto anim_wake_0=createSpriteToCenter("anim_45_0.png", true, parent);
        parent->addChild(anim_wake_0);
        auto anim_wake_1=createSpriteToCenter("anim_45_1.png", true, parent);
        parent->addChild(anim_wake_1);
        
        auto seq_wake=Sequence::create(createSoundAction("pinpon.mp3"),
                                       TargetedAction::create(anim_wake_0, FadeIn::create(1)),
                                       DelayTime::create(1),
                                       createSoundAction("pig.mp3"),
                                       TargetedAction::create(anim_wake_1, FadeIn::create(1)), NULL);
        auto delay=DelayTime::create(1);
        auto addStory=CallFunc::create([parent]{
            auto story=StoryLayer::create("ending", [parent]{
                EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

                auto duration_delay=2;
                auto duration_fade=1;
                Vector<Sprite*>frames;
                for (int i=0; i<4; i++) {
                    auto frame=Sprite::createWithSpriteFrameName(StringUtils::format("ending_%d.png",i));
                    frame->setPosition(parent->getContentSize()/2);
                    frame->setOpacity(0);
                    parent->addChild(frame);
                    
                    frames.pushBack(frame);
                }
                
                //最初
                auto delay=DelayTime::create(.5);
                //0
                auto fadein_0=TargetedAction::create(frames.at(0), FadeIn::create(duration_fade));
                auto delay_0=DelayTime::create(duration_delay);
                auto sound_keyboard=CallFunc::create([](){
                    Common::playSE("keyboard_1.mp3");
                });
                
                //1
                auto fadein_1=TargetedAction::create(frames.at(1), FadeIn::create(duration_fade));
                
                //2
                auto fadein_2=TargetedAction::create(frames.at(2), FadeIn::create(duration_fade));
                
                //3
                frames.at(3)->setScale(.2);
                auto spawn=TargetedAction::create(frames.at(3), Spawn::create(FadeIn::create(duration_fade*1.5),EaseIn::create(ScaleTo::create(duration_fade*1.5, 1),2), NULL));
                auto sound_pig=CallFunc::create([](){
                    Common::playSE("pig.mp3");
                    Common::playSE("pig_1.mp3");
                    Common::playSE("pig_1.mp3");
                });
                
                auto call=CallFunc::create([parent](){
                    auto story = StoryLayer::create("ending1", [](Ref* sen){
                        Director::getInstance()->replaceScene(TransitionFade::create(8, ClearScene::createScene(), Color3B::WHITE));
                    });
                    Director::getInstance()->getRunningScene()->addChild(story);
                });
                
                parent->runAction(Sequence::create(delay,fadein_0,sound_keyboard,delay_0,fadein_1,sound_keyboard->clone(),delay_0->clone(),fadein_2,sound_keyboard->clone(),delay_0->clone(),spawn,sound_pig,delay_0->clone(),call, NULL));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        parent->runAction(Sequence::create(delay,seq_wake,delay->clone(),addStory, NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark ドラッグ
void NakayubiCorporationActionManager::dragAction(std::string key,cocos2d::Node *parent, Vec2 pos)
{
    
}

#pragma mark - Item
void NakayubiCorporationActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{
    if (key=="iphone") {

    }
}

#pragma mark - Nakayubi
Sprite* NakayubiCorporationActionManager::createRoulette(cocos2d::Node *parent)
{
    auto roulette= AnchorSprite::create(Vec2(.5, .4925), parent->getContentSize(),"sup_26_roulette.png");
    return roulette;
}
