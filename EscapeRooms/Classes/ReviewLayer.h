//
//  ReviewLayer.h
//  Kebab
//
//  Created by yamaguchinarita on 2016/12/22.
//
//

#ifndef __Kebab__ReviewLayer__
#define __Kebab__ReviewLayer__

#include "cocos2d.h"
#include "Utils/AlertBridge.h"

typedef enum{
    ClearType_Normal=0,
    ClearType_MiniGame
}ClearType;

USING_NS_CC;
class ReviewLayer : public cocos2d::LayerColor,native_plugin::AlertBridgeDelegate
{
public:
    virtual bool init();
    CREATE_FUNC(ReviewLayer);
    
    ~ReviewLayer();

private:
    void createListener();
    void createLabel();
    void createMenu();

    void alertDismissed(int tag, int buttonIndex);
    LayerColor* createDisableLayer();
    
    CC_SYNTHESIZE(ClearType, clearType, ClearType);
};

#endif /* defined(__Kebab__ReviewLayer__) */
