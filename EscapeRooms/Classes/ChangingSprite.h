//
//  ChangingSprites.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/12/13.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________ChangingSprites_____
#define _____PROJECTNAMEASIDENTIFIER________ChangingSprites_____

#include "cocos2d.h"
#include "EscapeStageSprite.h"

USING_NS_CC;
class ChangingSprite:public EscapeStageSprite
{
public:
    /**命名規則は name_number_index.png indexsは9個まで(未確認)*/
    static ChangingSprite*create(std::string name,ValueMap map,Value answerV,int number);
    bool init(std::string name,ValueMap map,Value answerV,int number);
    void startAnimate();
    void stopAnimate(ccMenuCallback callback);
    
    CC_SYNTHESIZE(float, delay, Delay);
    /**fadein_duration*/
    CC_SYNTHESIZE(float, duration, Duration);
    /**name create時に受け取ったもの png抜き*/
    CC_SYNTHESIZE(std::string, nameCreated, NameCreated);
    /**この順にanimateする*/
    CC_SYNTHESIZE(ValueVector, indexs, Indexs);
    /**表示中のindex*/
    CC_SYNTHESIZE(int, index, Index);
    /**デフォルトのindex*/
    CC_SYNTHESIZE(int, defIndex, DefIndex);
    /**なんばんめのsprか*/
    CC_SYNTHESIZE(int, number, Number);
    /**変更時のSE*/
    CC_SYNTHESIZE(std::string, playSE, PlaySE);
    /**アニメーションをできるか*/
    CC_SYNTHESIZE(bool, disabled, Disabled);
    /**ストップされているか*/
    CC_SYNTHESIZE(bool, stoped, Stoped);
    /**ストップ時のbounce係数*/
    CC_SYNTHESIZE(float, deflection, Deflection);
private:
    /**animate中のspr*/
    //CC_SYNTHESIZE(Sprite*, childSpr, ChildSpr);
    CC_SYNTHESIZE(std::string, childSprName, ChildSprName);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
