//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__TempleActionManager__
#define __EscapeContainer__TempleActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class TempleActionManager:public CustomActionManager
{
public:
    static TempleActionManager* manager;
    static TempleActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
private:

};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
