//
//  SwitchSprite.cpp
//  BalentienRoom
//
//  Created by 成田凌平 on 2017/02/10.
//
//

#include "SwitchSprite.h"
#include "DataManager.h"
#include "Utils/Common.h"
#include "SupplementsManager.h"

using namespace cocos2d;

SwitchSprite* SwitchSprite::create(ValueMap map, cocos2d::Value answerMapV, std::string name,int number,std::string temporaryFlagKey)
{
    return create(map, answerMapV, name, number, temporaryFlagKey, ShowType_Normal);
}

SwitchSprite* SwitchSprite::create(ValueMap map, cocos2d::Value answerMapV, std::string name,int number,std::string temporaryFlagKey,ShowType type)
{
    auto node=new SwitchSprite();
    if (node&&node->init(map,answerMapV,name,number,temporaryFlagKey,type)) {
        node->autorelease();
        return node;
    }
    CC_SAFE_RELEASE(node);
    
    return node;
}

bool SwitchSprite::init(ValueMap map, cocos2d::Value answerMapV, std::string _name,int number,std::string temporaryFlagKey,ShowType type)
{
    auto empty=map["defaultEmpty"].asBool();
    auto index=map["index"].asInt();
    auto minIndex=map["minIndex"].asInt();
    if (index<minIndex) {
        index=minIndex;
    }
    
    auto name=StringUtils::format("%s_%d",_name.c_str(),number);
    
    //連動させるフラグがある時
    auto flagKey=map["temporaryFlagKey"].asString();
    if (flagKey.size()==0&&temporaryFlagKey.size()>0) {
        flagKey=StringUtils::format("%s_%d",temporaryFlagKey.c_str(),number);
    }
    setCustumFlagKey(flagKey);
    
    //一時的に保持されているindexがあるか確認する
    auto memoryData=DataManager::sharedManager()->getTemporaryFlag(getTemporaryKey(name,type));
    if (!memoryData.isNull()) {
        index=memoryData.asInt();
    }
    
    //フラグによる固定チェック
    if (!answerMapV.isNull()) {
        if (answerMapV.asValueMap()["secure"].asBool()) {
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(answerMapV.asValueMap()["enFlagID"].asInt())) {
                auto answer=answerMapV.asValueMap()["answer"].asString();
                index=atoi(answer.substr(number,1).c_str());
            }
        }
    }
    
    if(empty&&
       index==minIndex)
    {//この場合まだ画像をセットしない
        if (!Sprite::init()){
            return false;
        }
    }
    else if (!EscapeStageSprite::init(StringUtils::format("%s_%d.png",name.c_str(),index))) {
        return false;
    }
    
    auto count=map["count"].asInt();
    setMaxCount(count);
    setFileName(name);
    setIndex(index);
    setMinIndex(minIndex);
    setDefaultEmpty(empty);
    setLocalZOrder(map["zOrder"].asInt());
    
    auto memory=map["memory"].asBool();
    setMemory(memory);
    
    setPlaySE(map["playSE"].asString());
    
    //アニメーション情報
    if (!map["animate"].isNull()) {
        animateMap=map["animate"].asValueMap();
    }
    
    //表示していいか
    if (!SupplementsManager::getInstance()->checkAddable(map)) {
        setEnabled(false);
        setOpacity(0);
    }
    else{
        setEnabled(true);
    }
    
    //補完画像
    if (!map["supplements"].isNull()) {
        supplementsMap=map["supplements"].asValueMap();
        addSupplements();
    }
    
    //保存
    if (getEnabled()) {
        save(type);
    }
    
    return true;
}

#pragma mark - 補完
void SwitchSprite::addSupplements()
{
    //いったんreset
    for (int i=(int)this->getChildrenCount()-1; i>=0; i--) {
        auto sup=getChildren().at(i);
        sup->removeFromParent();
    }
    getChildren().clear();
    
    setCascadeOpacityEnabled(true);
    
    if (supplementsMap.size()>0) {
        auto supMapV=supplementsMap[StringUtils::format("%d",getIndex())];//現在indexに対するsupplements情報
        if (!supMapV.isNull()) {
            auto supMap=supMapV.asValueMap();
            for (auto v : supMap) {
                auto flag=atoi(v.first.c_str());

                if (DataManager::sharedManager()->getEnableFlagWithFlagID(flag)||flag==0) {
                    auto fileVec=v.second.asValueVector();
                    for (auto fileMapV : fileVec) {
                        auto fileMap=fileMapV.asValueMap();
                        auto removeFlagID=fileMap["removeFlagID"].asFloat();
                        if (removeFlagID!=0&&DataManager::sharedManager()->getEnableFlagWithFlagID(removeFlagID)) {
                            continue;
                        }
                        
                        SupplementsManager::getInstance()->addSupplementToCenter(this, fileMap["name"].asString(), false, true);
                    }
                }
            }
        }
    }
}

#pragma mark -
void SwitchSprite::switching(ccMenuCallback callback)
{
    auto maxIndex=getMaxCount()+getMinIndex();
    auto nextIndex=(getIndex()+1)%maxIndex;
    nextIndex=MAX(nextIndex, getMinIndex());
    switching(callback, nextIndex);
}

void SwitchSprite::switching(ccMenuCallback callback, int index)
{
    switching(callback, index, true);
}

void SwitchSprite::switching(ccMenuCallback callback, int index, bool sound)
{
    if(!getEnabled())
    {//タッチできない状態
        callback(NULL);
        return;
    }
    
    if(getPlaySE().size()>0&&sound){
        Common::playSE(getPlaySE().c_str());
    }
    
    //indexの入れ替え
    setIndex(index);
    save();
    
    //テクスチャの入れ替え
    changeTexture(callback, getIndex());
}

void SwitchSprite::changeTexture(cocos2d::ccMenuCallback callback, int index)
{
    if (getDefaultEmpty()&&index==0)
    {//画像なし
        if(animateMap.size()>0){
            if (!animateMap["playSE"].isNull()) {
                Common::playSE(animateMap["playSE"].asString().c_str());
            }
            
            auto fadeout=FadeOut::create(animateMap["duration"].asFloat());
            auto call=CallFunc::create([this,callback](){
                this->addSupplements();
                if (callback) {
                    callback(NULL);
                }
            });
            runAction(Sequence::create(fadeout,call, NULL));
        }
        else{
            setOpacity(0);
            addSupplements();
            if (callback) {
                callback(NULL);
            }
        }
    }
    else{
        auto next_name=StringUtils::format("%s_%d.png",getFileName().c_str(),index);
        auto spr_next = EscapeStageSprite::createWithSpriteFileName(next_name);
        
        //defaultEmptyの場合に備えscaleをsetし直す
        setScale(getParent()->getContentSize().width/spr_next->getContentSize().width);

        if(animateMap.size()>0)
        {//animate
            spr_next->setOpacity(0);
            spr_next->setPosition(getPosition());
            spr_next->setLocalZOrder(getLocalZOrder());
            addSupplements();
            getParent()->addChild(spr_next);
            
            if (!animateMap["playSE"].isNull()) {
                Common::playSE(animateMap["playSE"].asString().c_str());
            }
            auto duration=animateMap["duration"].asFloat();
            auto fades=Spawn::create(TargetedAction::create(spr_next,FadeIn::create(duration*.7)),
                                     TargetedAction::create(this, FadeOut::create(duration)), NULL);
            auto call=CallFunc::create([this,callback,spr_next](){
                this->setOpacity(255);
                setSpriteFrame(spr_next->getSpriteFrame());
                spr_next->removeFromParent();
                addSupplements();
                
                if (callback) {
                    callback(NULL);
                }
            });
            runAction(Sequence::create(fades,call, NULL));
        }
        else{
            setSpriteFrame(spr_next->getSpriteFrame());
            setOpacity(255);
            addSupplements();
            if (callback) {
                callback(NULL);
            }
        }
    }
}

std::string SwitchSprite::getTemporaryKey(ShowType type)
{
    return getTemporaryKey(getFileName(), type);
}

std::string SwitchSprite::getTemporaryKey(std::string name, ShowType type)
{
    if(getCustumFlagKey().size()>0)
    {//指定のキーがある
        return getCustumFlagKey();
    }
    
    auto key=StringUtils::format("switch_%s",name.c_str());
    key=SupplementsManager::getInstance()->appendKey(key, type);
    
    return key;
}

std::string SwitchSprite::save()
{
    return save(ShowType_Normal);
}

std::string SwitchSprite::save(ShowType type)
{
    auto key=getTemporaryKey(getFileName(),type);
    return save(key);
}

std::string SwitchSprite::save(std::string key)
{
    if (getMemory()) {
        DataManager::sharedManager()->setTemporaryFlag(key, getIndex());
    }
    
    return key;
}

