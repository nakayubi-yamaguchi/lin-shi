//
//  StoryLayer.cpp
//  BalentienRoom
//
//  Created by yamaguchinarita on 2017/02/11.
//

#include "StoryLayer.h"
#include "Utils/Common.h"
#include "DataManager.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "Utils/AudioManager.h"
#include "EscapeStageSprite.h"


using namespace cocos2d;

StoryLayer* StoryLayer::create(std::string messageName, const ccMenuCallback &callback)
{
    auto node =new StoryLayer;
    if (node&&node->init(0,messageName, callback, nullptr)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

StoryLayer* StoryLayer::create(std::string messageName, const storyCallBack &callback)
{
    auto node =new StoryLayer;
    if (node&&node->init(0,messageName, nullptr, callback)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

#pragma mark -
std::vector<Message> StoryLayer::getMessageVector(std::string messageName)
{
    std::vector<Message> messages = {};
    
    auto messagesFromPlist = DataManager::sharedManager()->getStoryData(messageName);

    for (auto message : messagesFromPlist) {
        auto messageMap = message.asValueMap();
        
        auto speakerName = messageMap["speakerName"];
        auto speakerData = DataManager::sharedManager()->getSpeakerData(speakerName.asString().c_str());

        //スピーカー情報があれば、優先する。
        auto isSpeakerInfo = (speakerData.size() > 0);
        auto charaValue = (isSpeakerInfo)? speakerData["characterInfo"] : messageMap["characterInfo"];
        //
        auto charasValue=messageMap["characterInfos"];
        
        auto localizeTexts = messageMap["texts"].asValueMap();
        
        //preload
        auto soundName=messageMap["soundName"].asString();//過去のもそのまま使えるように。
        auto seName = messageMap["playSE"];
        if (!seName.isNull()) {//"playSE"があるならこっち優先
            soundName=seName.asString();
        }
        
        if (soundName.size()>0) {
            EscapeDataManager::getInstance()->preloadSound(soundName.c_str());
        }
        
        auto playEndingBGM = messageMap["playEndingBGM"].asBool();
        if (playEndingBGM) {
            auto filename=DataManager::sharedManager()->getBgmName(true);
            EscapeDataManager::getInstance()->preloadSound(filename.c_str());
        }
        
        //メッセージ構造体生成.空のものにはデフォルト値をセットする。
        auto backColorCode = DataManager::sharedManager()->getColorCodeData((isSpeakerInfo)? speakerData["backColorCode"].asString(): messageMap["backColorCode"].asString());
        auto textColorCode = DataManager::sharedManager()->getColorCodeData((isSpeakerInfo)? speakerData["textColorCode"].asString():messageMap["textColorCode"].asString());

        Message messageStruct;
        messageStruct.text = localizeTexts[Common::localize("en", "ja", "ch", "tw", "ko")].asString();
        messageStruct.backColor = (backColorCode.size() == 0) ? (Common::getColorFromHex("000000")): (Common::getColorFromHex(backColorCode));
        messageStruct.textColor = (textColorCode.size() == 0) ? (Color4B(Common::getColorFromHex("ffffff"))) : (Color4B(Common::getColorFromHex(textColorCode)));
        //messageStruct.soundName = soundName;
        messageStruct.backId = messageMap["backId"].asString();
        if (!messageMap["backID"].isNull()) {
            messageStruct.backId = messageMap["backID"].asString();
        }
        messageStruct.imageName = messageMap["imageName"].asString();
        messageStruct.customActionKey=messageMap["customActionKey"].asString();
        messageStruct.playSE = soundName;
        messageStruct.playEndingBGM = playEndingBGM;

        CharacterInfo charaInfo;
        if (!charaValue.isNull()) {
            auto map = charaValue.asValueMap();
            messageStruct.characterInfos.push_back(getCharacterInfo(map));
        }
        if (!charasValue.isNull()) {
            auto vec = charasValue.asValueVector();
            for (auto v : vec) {
                messageStruct.characterInfos.push_back(getCharacterInfo(v.asValueMap()));
            }
        }
        
        messages.push_back(messageStruct);
    }
    return messages;
}

CharacterPosition StoryLayer::transformCharaPosition(std::string positionName)
{
    CharacterPosition charaPosi;
    if (positionName == "left") {
        charaPosi = CharacterPosition_Left;
    }
    else if (positionName == "leftout") {
        charaPosi = CharacterPosition_LeftOut;
    }
    else if (positionName == "center") {
        charaPosi = CharacterPosition_Center;
    }
    else if (positionName == "right") {
        charaPosi = CharacterPosition_Right;
    }
    else {
        charaPosi = CharacterPosition_None;
    }
    
    return charaPosi;
}

#pragma mark - Initialise
bool StoryLayer::init(float delay,std::string messageName, const ccMenuCallback &callback, const storyCallBack &callback_onfinish)
{
    if (!LayerColor::initWithColor(Color4B(0, 0, 0, 0))) {
        return false;
    }
    
    std::vector<Message> messages = getMessageVector(messageName);

    
    setCanClose(false);
    setEndDelay(false);
    setLocalZOrder(999);
    
    addTouchListener();
    
    m_orgCallback = callback;
    m_orgCallback_onFinished=callback_onfinish;
    m_showMessageNum = 0;
    m_showMessages = messages;
    m_firstShowDelay = delay;
    
    //ストーリーレイヤーを表示する場合はパネル広告を隠す。
    SupplementsManager::getInstance()->hidePanel();

    show();
    
    return true;
}

#pragma mark- プライベートメソッド
void StoryLayer::show()
{
    auto message = getShowMessage();
    
    //音再生
    if (message.playSE.size() > 0) {
        EscapeDataManager::getInstance()->playSound(message.playSE.c_str(), false);
    }

    //エンディングBGM再生
    if (message.playEndingBGM) {
        auto filename=DataManager::sharedManager()->getBgmName(true);
        EscapeDataManager::getInstance()->playSound(filename.c_str(), true);
    }
    
    //間隔をあけてからストーリーを表示するアクション。
    Vector<FiniteTimeAction*> actions;
    if (m_firstShowDelay > 0) {//最初時間を開ける指定がされていれば。
        actions.pushBack(DelayTime::create(m_firstShowDelay));
    }
    
    if (message.backId.size()>0) {//背景を変更する場合。
        auto changeBack = CallFunc::create([this, message](){
            DataManager::sharedManager()->setNowBack(atoi(message.backId.c_str()), ReplaceTypeSlowFadeIn);
        });
        auto delay = DelayTime::create(1);
        
        actions.pushBack(Spawn::create(changeBack, delay, NULL));
    }
    
    auto callfunc = CallFunc::create([this, message](){
        createMessageLabel();
        
        createCharacter();
        
        //バー表示までの待ち時間を終了します。
        this->setEndDelay(true);
        
        showCutIn();
    });
    actions.pushBack(callfunc);

    runAction(Sequence::create(actions));
}

void StoryLayer::showCutIn()
{
    auto showMessage = getShowMessage();
    
    //カットイン用のスプライトを表示
    cutinSprite = Sprite::create();
    cutinSprite->setTextureRect(Rect(0, 0, getContentSize().width, getContentSize().height*.25));
    cutinSprite->setPosition(getContentSize()/2);
    cutinSprite->setColor(showMessage.backColor);
    cutinSprite->setOpacity(0);
    addChild(cutinSprite);
    
    Vector<cocos2d::FiniteTimeAction *> arrayOfFadeActions;
    arrayOfFadeActions.pushBack(TargetedAction::create(cutinSprite, FadeTo::create(.5, 180)));
    arrayOfFadeActions.pushBack(TargetedAction::create(showLabel, FadeIn::create(.5)));
    
    if (showMessage.imageName.size()>0) {
        arrayOfFadeActions.pushBack(TargetedAction::create(showImage, FadeIn::create(.5)));
    }
    
    //キャラクターがいる場合
    if (showMessage.characterInfos.size()>0) {
        //カットインを小さく
        cutinSprite->setTextureRect(Rect(0, 0, getContentSize().width, getContentSize().height*.2));
        
        //カットインラベルを下へ移動
        auto mainSpriteRect = DataManager::sharedManager()->getMainSpriteRect();
        cutinSprite->setPositionY(mainSpriteRect.origin.y+cutinSprite->getBoundingBox().size.height/2);
        
        showLabel->setPositionY(cutinSprite->getPositionY());
        
        //イメージも移動
        if (showMessage.imageName.size()>0)
            showImage->setPositionY(cutinSprite->getPositionY());
        
        //キャラ画像チェック
        for (auto info : showMessage.characterInfos) {
            if (info.fileName.size()>0) {
                auto node=this->getChildByName(info.fileName);
                //Y座標を調整
                auto slideCharaRatio = info.slideRatio;//カットインバーに食い込ませるキャラの割合
                auto positionY = mainSpriteRect.origin.y + cutinSprite->getBoundingBox().size.height - node->getBoundingBox().size.height*slideCharaRatio + node->getBoundingBox().size.height/2;
                node->setPositionY(positionY);
                
                //フェードインアニメーション
                arrayOfFadeActions.pushBack(TargetedAction::create(node, FadeIn::create(.5)));
            }
        }
    }
    
    auto callback = CallFunc::create([this,showMessage](){
        //customActionがあれば呼び出す
        SupplementsManager::getInstance()->getCustomActionManager()->storyAction(showMessage.customActionKey, this, [this](bool success){
            //アニメーションが終わったので、タッチで閉じる許可を与える。
            log("アニメーションが終わったのでタッチで閉じる許可を与える");
            this->setCanClose(true);
        });
    });
    
    auto spawnFadein = Spawn::create(arrayOfFadeActions);
    auto sequence = Sequence::create(spawnFadein, callback, NULL);
    
    runAction(sequence);
}

void StoryLayer::createMessageLabel()
{
    auto showMessage = getShowMessage();
    
    showLabel = Label::createWithTTF(showMessage.text, Common::getUsableFontPath(MyFont), getContentSize().height*.03);
    showLabel->setTextColor(showMessage.textColor);
    showLabel->setWidth(getContentSize().width*.85);
    showLabel->setOpacity(0);
    showLabel->setPosition(getContentSize()/2);
    addChild(showLabel,2);
    
    //イメージ画像
    auto imageName = showMessage.imageName;
    if (imageName.size() == 0) return;
    
    auto imageSide = getContentSize().height*.15;
    auto imageInset = imageSide*1.25;
    auto spriteName = "sprite";
    if (!getChildByName(spriteName)) {
        showImage = EscapeStageSprite::createWithSpriteFileName(imageName);
        showImage->setName(spriteName);
        addChild(showImage, 2);
    }
    else {
        showImage->setSpriteFrame(imageName);
    }
    showImage->setPosition(getContentSize().width-imageInset/2, getContentSize().height/2);
    showImage->setScale(imageSide/showImage->getContentSize().width, imageSide/showImage->getContentSize().height);
    showImage->setOpacity(0);
    
    auto labelStartPosition = showLabel->getPositionX()-showLabel->getWidth()/2;
    auto labelEndPosition = getContentSize().width-imageInset;
    showLabel->setWidth(labelEndPosition-labelStartPosition);
    showLabel->setPositionX((labelEndPosition+labelStartPosition)/2);
}

void StoryLayer::createCharacter()
{
    auto showMessage = getShowMessage();

    for (auto characterInfo : showMessage.characterInfos) {
        //イメージ画像
        auto showCharacterName = characterInfo.fileName;
        if (showCharacterName.size() == 0) return;
        
        auto width = getContentSize().width*.4;
        auto showCharacter = EscapeStageSprite::createWithSpriteFileName(showCharacterName);
        showCharacter->setName(showCharacterName);
        showCharacter->setCascadeOpacityEnabled(true);
        showCharacter->setOpacity(0);
        addChild(showCharacter);
        
        for (auto filename : characterInfo.supplements) {
            auto sup=EscapeStageSprite::createWithSpriteFileName(filename.asString());
            sup->setName(filename.asString());
            sup->setPosition(showCharacter->getContentSize()/2);
            showCharacter->addChild(sup);
        }
        
        showCharacter->setScale(width/showCharacter->getContentSize().width);
        
        //X座標を決める
        float positionX = 0;
        switch (characterInfo.position) {
            case CharacterPosition_Left:
                positionX = width/2;
                break;
            case CharacterPosition_LeftOut:
                positionX = -width/2;
                break;
            case CharacterPosition_Center:
                positionX = getContentSize().width/2;
                break;
                
            case CharacterPosition_Right:
                positionX = getContentSize().width-width/2;
                break;
            default:
                break;
        }
        
        //Y座標はShowCutinで調整されるから適当
        auto mainRect = DataManager::sharedManager()->getMainSpriteRect();
        showCharacter->setPosition(positionX, mainRect.origin.y+mainRect.size.height/2);
    }
}

void StoryLayer::removeAnimation()
{
    auto showMessage = getShowMessage();
    
    //表示中のメッセージ取得したら、次のメッセージにしてOK。
    m_showMessageNum++;
    
    Vector<FiniteTimeAction*> animation;
    auto labelhide = TargetedAction::create(showLabel, FadeOut::create(.3));
    auto labelremove = TargetedAction::create(showLabel, RemoveSelf::create());
    auto sequencelabel =Sequence::create(labelhide, labelremove, NULL);
    
    Vector<FiniteTimeAction*> fadeAnim;
    fadeAnim.pushBack(sequencelabel);
    
    if (showMessage.imageName.size()>0) {//imageはfadeoutしてremove
        auto imageSequence = Sequence::create(
                                              TargetedAction::create(showImage, FadeOut::create(.3)),
                                              TargetedAction::create(showImage, RemoveSelf::create()), NULL);
        fadeAnim.pushBack(imageSequence);
    }
    
    for (auto info : showMessage.characterInfos) {
        if (info.fileName.size()>0) {
            auto node=this->getChildByName(info.fileName);
            auto imageSequence = Sequence::create(
                                                  TargetedAction::create(node, FadeOut::create(.3)),
                                                  TargetedAction::create(node, RemoveSelf::create()), NULL);
            fadeAnim.pushBack(imageSequence);
        }
    }
    
    
    for (auto child : cutinSprite->getChildren()) {
        fadeAnim.pushBack(TargetedAction::create(child, FadeOut::create(.3)));
    }
    auto fadeout = TargetedAction::create(cutinSprite, FadeOut::create(.3));
    auto cutinremove = TargetedAction::create(cutinSprite, RemoveSelf::create());
    auto sequencecutin = Sequence::create(fadeout, cutinremove, NULL);
    
    fadeAnim.pushBack(sequencecutin);
    auto spawn = Spawn::create(fadeAnim);
    
    auto callback = CallFunc::create([this](){
    
        Common::stopAllSE();
        
        if (m_showMessageNum == m_showMessages.size()) {//表示し終わったら
            if (m_orgCallback) {//コールバック
                m_orgCallback(this);
            }
            else if(m_orgCallback_onFinished){
                m_orgCallback_onFinished();
            }
            
            this->removeFromParent();
        }
        else{
            show();
        }
    });

    auto sequence = Sequence::create(spawn, callback,  NULL);
    
    runAction(sequence);
}

Message StoryLayer::getShowMessage()
{
    return m_showMessages.at(m_showMessageNum);
}

CharacterInfo StoryLayer::getCharacterInfo(ValueMap map)
{
    CharacterInfo charaInfo;
    charaInfo.fileName = map["fileName"].asString();
    charaInfo.position = transformCharaPosition(map["position"].asString());
    
    if (!map["supplements"].isNull()) {
        charaInfo.supplements = map["supplements"].asValueVector();
    } else {
        charaInfo.supplements = ValueVector();
    }
    
    if (!map["slideRatio"].isNull()) {
        charaInfo.slideRatio = map["slideRatio"].asFloat();
    } else {
        charaInfo.slideRatio = .3;//デフォルトはキャラの高さ*.3食い込ませる。
    }
    
    return charaInfo;
}

#pragma mark- タッチ
void StoryLayer::addTouchListener()
{
    auto listener=EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);//タッチ透過しない
    
    listener->onTouchBegan=[this](Touch*touch, Event*event)
    {//trueじゃないとlayer下のnodeがタッチ有効になっちゃう
        return true;
    };
    
    listener->onTouchEnded=[this](Touch*touch, Event*event)
    {
        if (!this->getEndDelay())
        {//表示途中
            return ;
        }
        
        if (getCanClose()) {//閉じていい許可が出ていたら
            this->setCanClose(false);
            
            //切り替えなどの設定があれば実行
            auto key=StringUtils::format("%d",m_showMessageNum);
            if (!replaceMap[key].isNull()) {
                DataManager::sharedManager()->setNowBack(replaceMap[key].asInt(),ReplaceTypeNone);
            }
            
            this->removeAnimation();
        }
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

#pragma mark- カスタムアクション
void StoryLayer::setReplace(int index,int backNum)
{
    replaceMap[StringUtils::format("%d",index)]=backNum;
}

#pragma mark- 解放
StoryLayer::~StoryLayer()
{
    log("ストーリーレイヤーを解放します。");
}


