//
//  BonusLayer.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/02/24.
//
//

#include "BonusLayer.h"
#include "Utils/CustomAlert.h"
#include "DataManager.h"
#include "Utils/Common.h"
#include "Utils/ContentsAlert.h"
#include "Utils/FireBaseBridge.h"
#include "Utils/RewardMovieManager.h"
#include "AnalyticsManager.h"

//show->lottery

using namespace cocos2d;

#define MaxLotteryCount 3//１日の最大抽選回数

BonusLayer* BonusLayer::create(const onFinish &callback)
{
    auto node=new BonusLayer();
    if (node&&node->init(callback)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool BonusLayer::init(const onFinish &callback)
{
    if (!LayerColor::initWithColor(Color4B(220, 220, 220, 0))) {
        return false;
    }

    lotteryCount=UserDefault::getInstance()->getIntegerForKey(getKeyOfLottelyCountToday().c_str());

    mCallBack=callback;
    
    backGroundLayer=LayerColor::create(Color4B(0, 0, 0, 0));
    addChild(backGroundLayer);
    
    setCascadeOpacityEnabled(false);
    addTouchListener();
    show(!(lotteryCount==0));
    
    Common::playBGM("casino.mp3", true, .25);
    
    
    return true;
}

#pragma mark - 抽選
void BonusLayer::lottery()
{//抽選とデータ付与
    lotteryCount++;

    auto ratio_ultraRare=Ratio_UltraRare;
    auto ratio_superRare=Ratio_SuperRare;
    auto ratio_rare=Ratio_Rare;
    auto ratio_normal=Ratio_Normal;

    if (isRare) {
        ratio_rare=(Ratio)(Ratio_Normal+Ratio_Normal);
        ratio_normal=(Ratio)0;
    }

    auto total=ratio_ultraRare+ratio_superRare+ratio_rare+ratio_normal;
    auto number=RandomHelper::random_int(1, total);
    
    //initialize
    doors.clear();
    performance_back=Performance_Back_None;
    performance_nu=Performance_Nu_None;
    performance_roulette=Performance_Roulette_None;
    performance_crowd=Performance_Crowd_None;
    
    //set lotterly
    if (number<ratio_ultraRare) {
        lot.rarelity=Rarelity_UltraRare;
        lot.doorColor=DoorColor_Gold;
        lot.coin=10+RandomHelper::random_int(0, 20);;
    }
    else if (number<ratio_ultraRare+ratio_superRare){
        lot.rarelity=Rarelity_SuperRare;
        lot.doorColor=DoorColor_Red;
        lot.coin=5+RandomHelper::random_int(0, 2);
    }
    else if (number<ratio_ultraRare+ratio_superRare+ratio_rare){
        lot.rarelity=Rarelity_Rare;
        lot.doorColor=DoorColor_Green;
        lot.coin=2+RandomHelper::random_int(0, 1);
    }
    else{
        lot.rarelity=Rarelity_Normal;
        lot.doorColor=DoorColor_White;
        lot.coin=1;
    }
    
    //初回起動時のログボ
    /*if (firstBonus) {
        log("初回ログインボーナス");
        lot.rarelity=Rarelity_UltraRare;
        lot.coin=10;
        lot.doorColor=DoorColor_Gold;
    }*/
    
    //ドア変化演出抽選
    auto random=RandomHelper::random_int(0, 999);
    
    //背景演出割合計算用
    std::vector<int> ratio_back_nakayubi={10,0,0,0};//金の場合1.0％で出現
    std::vector<int> ratio_back_red={400,500,0,0};//金の場合40.0-1％で出現 赤の場合50%で出現
    std::vector<int> ratio_back_green={500,650,250,0};//金の場合50.0-400％で出現 赤の場合65-50%で出現 緑の場合25%で出現

    //ぬっ演出
    std::vector<int> ratio_nu_gold={100,0,0,0};//金の場合10.0％で出現
    std::vector<int> ratio_nu_many={700,300,50,0};//金の場合70.0％-10で出現 赤の場合50%で出現 緑の場合5%で出現
    std::vector<int> ratio_nu_one={800,700,700,0};//金の場合80.0-70％で出現 赤の場合70-30%で出現 緑の場合70-5%で出現

    //群演出
    std::vector<int> ratio_crowd={800,150,0,0};
    
    //ルーレット演出
    std::vector<int> ratio_roulette_reverce={330,330,0,0};//逆回転 金の場合33.0％で出現 赤の場合33%で出現
    
    //ドア種類分岐
    if(lot.rarelity==Rarelity_UltraRare)
    {//大当たり
        //door
        if (random<10)
        {//全部中指
            lot.doorColor=DoorColor_Nakayubi;
            setDoors({DoorColor_Nakayubi,DoorColor_Nakayubi,DoorColor_Nakayubi,DoorColor_Nakayubi});
        }
        else if (random<50)
        {//全部金
            setDoors({0,0,0,0});
        }
        else if(random<100)
        {//ナカユビ柄
            lot.doorColor=DoorColor_Nakayubi;
            setDoors({DoorColor_Nakayubi,1,2,3});
        }
        else if (random<200)
        {//赤*3
            setDoors({0,1,1,1});
        }
        else if(random<400)
        {//白が金
            setDoors({DoorColor_Gold,1,2,DoorColor_Gold});
        }
        else if (random<600)
        {//白が赤
            setDoors({0,1,2,1});
        }
        else if (random<700)
        {//緑が赤
            setDoors({0,1,1,3});
        }
        else if (random<900)
        {//緑が金
            setDoors({0,1,0,3});
        }
        else{
            setDoors({0,1,2,3});
        }
    }
    else if (lot.rarelity==Rarelity_SuperRare)
    {//
        //door
        if (random<100)
        {//白が金(ガセ)
            setDoors({DoorColor_Gold,1,2,DoorColor_Gold});
        }
        else if (random<200)
        {//赤*3
            setDoors({0,1,1,1});
        }
        else if (random<400)
        {//白が赤
            setDoors({0,1,2,1});
        }
        else if (random<500)
        {//白が緑
            setDoors({0,1,2,2});
        }
        else if (random<600)
        {//緑が赤
            setDoors({0,1,1,3});
        }
        else if(random<650){
            //緑が金
            setDoors({0,1,0,3});
        }
        else{
            setDoors({0,1,2,3});
        }
    }
    else if (lot.rarelity==Rarelity_Rare){
        if (random<100)
        {//白が金(ガセ)
            setDoors({DoorColor_Gold,1,2,DoorColor_Gold});
        }
        else if (random<300)
        {//白が緑
            setDoors({0,1,2,2});
        }
        else if (random<400)
        {//白が赤(ガセ)
            setDoors({0,1,2,1});
        }
        else{
            setDoors({0,1,2,3});
        }
    }
    else
    {//外れorドア変化なし
        if (random<40)
        {//緑が赤(ガセ)
            setDoors({0,1,1,3});
        }
        else if(random<70){
            //緑が金(ガセ)
            setDoors({0,1,0,3});
        }
        else{
            setDoors({0,1,2,3});
        }
    }

    //背景種類分岐
    auto random_back=RandomHelper::random_int(0, 999);
    if (ratio_back_nakayubi.at(lot.rarelity)>random_back) {
        performance_back=Performance_Back_Nakayubi;
    }
    else if (ratio_back_red.at(lot.rarelity)>random_back) {
        performance_back=Performance_Back_Red;
    }
    else if (ratio_back_green.at(lot.rarelity)>random_back) {
        performance_back=Performance_Back_Green;
    }
    
    //ぬっ抽選
    auto random_nu=RandomHelper::random_int(0, 999);
    if (ratio_nu_gold.at(lot.rarelity)>random_nu) {
        performance_nu=Performance_Nu_Gold;
    }
    else if (ratio_nu_many.at(lot.rarelity)>random_nu) {
        performance_nu=Performance_Nu_Many;
    }
    else if (ratio_nu_one.at(lot.rarelity)>random_nu) {
        performance_nu=Performance_Nu_One;
    }
    
    //ルーレット抽選
    auto random_roulette=RandomHelper::random_int(0, 999);
    if (ratio_roulette_reverce.at(lot.rarelity)>random_roulette) {
        performance_roulette=Performance_Roulette_Reverse;
    }
    
    //群
    auto random_crowd=RandomHelper::random_int(0, 999);
    if (ratio_crowd.at(lot.rarelity)>random_crowd) {
        performance_crowd=Performance_Crowd_Pig;
    }
    
    log("抽選結果%d rarity:%d coin:%d pos%d",number,lot.rarelity,lot.coin,stopPosIndex);
    
    //データに反映する
    save();
}

void BonusLayer::save()
{
    DataManager::sharedManager()->addCoin(lot.coin);
    //DataManager::sharedManager()->incrementRouletteCount();
    
    if(lotteryCount==1)
    {//受け取り日付を記録
        DataManager::sharedManager()->saveDate(UserKey_LoginBonusDate);
    }
    
    //受け取り回数を加算
    UserDefault::getInstance()->setIntegerForKey(getKeyOfLottelyCountToday().c_str(), lotteryCount);
    UserDefault::getInstance()->setIntegerForKey(UserKey_GetRouletteTotal, 1+UserDefault::getInstance()->getIntegerForKey(UserKey_GetRouletteTotal));
}

#pragma mark ドアをセット
void BonusLayer::setDoors(std::vector<int> colors)
{
    int pos=0;
    std::vector<int>poses;//止まる可能性のあるposition
    for (auto doorColor:colors) {
        doors.push_back(doorColor);
        if (doorColor==lot.doorColor) {
            poses.push_back(pos);
        }
        pos++;
    }
    
    //止まる可能性のあるposからrandomで一つ選択
    stopPosIndex=poses.at(RandomHelper::random_int(0, 99)%poses.size());
}

#pragma mark 背景演出
void BonusLayer::flashBackColor()
{
    auto duration=1.0f;

    if (performance_back==Performance_Back_None) {
        return;
    }
    else if(performance_back==Performance_Back_Nakayubi){
        Common::playSE("alert.mp3");

        auto container=Layer::create();
        container->setCascadeOpacityEnabled(true);
        container->setOpacity(0);
        backGroundLayer->addChild(container);
        
        auto fileName="logomark.png";
        
        auto width=getContentSize().width*.25;
        for (int x=0; x<getContentSize().width/width; x++) {
            for (int y=0; y<getContentSize().height/width; y++) {
                auto spr=Sprite::createWithSpriteFrameName(fileName);
                spr->setScale(width/spr->getContentSize().width);
                spr->setPosition(width/2+width*x,width/2+width*y);
                container->addChild(spr);
            }
        }
        
        auto rep=TargetedAction::create(container,Repeat::create(Sequence::create(FadeIn::create(duration),
                                                 FadeOut::create(duration), NULL), 3));
        auto rem=TargetedAction::create(container, RemoveSelf::create());
        
        runAction(Sequence::create(rep,rem, NULL));
    }
    else{
        auto color=getColor();
        auto color_1=color;
        
        if (performance_back==Performance_Back_Red){
            color_1=Color3B(150, 0, 0);
        }
        else if (performance_back==Performance_Back_Green){
            color_1=Color3B(0, 150, 0);
        }
        
        auto rep=Repeat::create(Sequence::create(Common::createSoundAction("shine.mp3")
                                                 ,TintTo::create(duration,color_1),
                                                 TintTo::create(duration, color), NULL), 3);
        
        runAction(rep);
    }
}

void BonusLayer::addNuPig()
{//ぶたがぬっ
    if (performance_nu==Performance_Nu_None) {
        return;
    }
    else{
        auto pig_action=[this](float pos_x,std::string sound,bool gold){
            auto back=getChildByName("back");

            auto width=getContentSize().width*.5;
            
            std::string fileName="pig_nu.png";
            if (gold) {
                fileName="pig_nu_gold.png";
            }
            
            auto pig=Sprite::createWithSpriteFrameName(fileName);
            pig->setPosition(pos_x,getContentSize().height*.5+width/2);
            pig->setScale(width/pig->getContentSize().width);
            backGroundLayer->addChild(pig);
            
            auto duration_delay=1.5;
            auto duration_move=1.2;
            if (performance_nu==Performance_Nu_Many) {
                duration_delay=1.0;
            }
            
            auto move=Sequence::create(MoveTo::create(duration_move, Vec2(pig->getPosition().x, back->getPosition().y+back->getBoundingBox().size.height/2+pig->getBoundingBox().size.height/2)),
                                       Common::createSoundAction(sound),
                                       DelayTime::create(duration_delay),
                                       MoveTo::create(duration_move, Vec2(pos_x, getContentSize().height/2)),
                                       RemoveSelf::create(), NULL);
            
            return TargetedAction::create(pig, move);
        };
        
        if (performance_nu==Performance_Nu_One) {
            auto seq=Sequence::create(pig_action(getContentSize().width/2,"sa.mp3",false), NULL);
            runAction(seq);
        }
        else if(performance_nu==Performance_Nu_Gold){
            auto seq=Sequence::create(pig_action(getContentSize().width/2,"flash2.mp3",true),
                                      Spawn::create(pig_action(getContentSize().width*.2,"",true),
                                                    pig_action(getContentSize().width*.8,"flash2.mp3",true),
                                                    pig_action(getContentSize().width*.5,"",true), NULL), NULL);
            runAction(seq);
        }
        else
        {//many
            auto seq=Sequence::create(pig_action(getContentSize().width/2,"sa.mp3",false),
                                      Spawn::create(pig_action(getContentSize().width*.2,"",false),
                                                    pig_action(getContentSize().width*.8,"pig.mp3",false),
                                                    pig_action(getContentSize().width*.5,"",false), NULL), NULL);
            runAction(seq);
        }
    }
}

void BonusLayer::addCrowd()
{
    if (performance_crowd==Performance_Crowd_Pig) {
        Vector<FiniteTimeAction*>actions;
        auto width=getContentSize().width*.2;
        
        Vector<SpriteFrame*>frames;
        for(int i=0;i<2;i++){
            auto s=Sprite::createWithSpriteFrameName(StringUtils::format("pig_run_%d.png",i));
            frames.pushBack(s->getSpriteFrame());
        }
        
        //
        auto animation=Animation::createWithSpriteFrames(frames);
        animation->setDelayPerUnit(10.0f/60.0f);//??フレームぶん表示
        animation->setRestoreOriginalFrame(true);
        animation->setLoops(UINT_MAX);
        
        
        for (int x=0; x<5; x++) {
            for (int y=0; y<5; y++) {
                auto pig=Sprite::createWithSpriteFrameName("pig_run_0.png");
                pig->setLocalZOrder(1);
                pig->setScale(width/pig->getContentSize().width);
                pig->setPosition(getContentSize().width+width/2+width*x+width/2*(y%2),getContentSize().height/2-width*2+width*y);
                addChild(pig);
                
                actions.pushBack(TargetedAction::create(pig,Sequence::create(DelayTime::create(1),
                                                                             Spawn::create(MoveBy::create(4, Vec2(-getContentSize().width*2.5, 0)),
                                                                                           Animate::create(animation),
                                                                                           NULL),
                                                                             RemoveSelf::create(), NULL)));
            }
        }
        
        runAction(Sequence::create(Common::createSoundAction("horce_run.mp3"),Spawn::create(actions), NULL));
    }
}

#pragma mark - UI
void BonusLayer::show(bool fromCollection)
{
    auto showAlert=CallFunc::create([this,fromCollection]{
        auto title = DataManager::sharedManager()->getSystemMessage("login_title");
        auto rest=MaxLotteryCount-lotteryCount;
        auto message = StringUtils::format(DataManager::sharedManager()->getSystemMessage("login_message").c_str(),rest);
        std::vector<std::string>buttons;
        if (fromCollection) {
            buttons={DataManager::sharedManager()->getSystemMessage("login_onemore"),"Close"};
            if (rest==0) {
                buttons.erase(buttons.begin());
            }
            else if (rest==1){
                isRare=true;
                message.append(DataManager::sharedManager()->getSystemMessage("login_rare"));
            }
        }
        else{
            buttons={"Start","Skip"};
        }
        
        auto alert = CustomAlert::create(title, message, buttons, [this,fromCollection,rest](Ref* sender){
            auto customAlert = (CustomAlert*)sender;
            int selectedNum = customAlert->getSelectedButtonNum();
            
            if (fromCollection) {
                if (selectedNum==0&&rest>0) {
                    this->checkConditionOfReward();
                }
                else{
                    this->close();
                }
            }
            else{
                this->lottery();
                
                if (selectedNum==0) {
                    this->createRoulette();
                }
                else{
                    this->showResultAlert();
                }
            }
        });
        alert->showAlert(this);
    });
    
    runAction(Sequence::create(FadeTo::create(.3, 255),showAlert, NULL));
}

void BonusLayer::createRoulette()
{
    auto back=Sprite::createWithSpriteFrameName("wall.png");
    back->setName("back");
    back->setCascadeOpacityEnabled(true);
    back->setPosition(getContentSize()/2);
    back->setOpacity(0);
    back->setScale(.01);
    addChild(back);
    
    //ドアを並べる
    auto width=back->getContentSize().width*.4;
    std::vector<Vec2>poses;
    for (int i=0; i<4; i++) {
        auto fileName=StringUtils::format("door_%d.png",doors.at(i));
        auto door=Sprite::createWithSpriteFrameName(fileName);
        door->setScale(width/door->getContentSize().width);
        door->setName(StringUtils::format("door_%d",i));
        door->setLocalZOrder(1);
        door->setPosition(back->getContentSize().width/2-width/2*pow(-1, i),
                          back->getContentSize().height/2+width/2*pow(-1, i/2));
        back->addChild(door);
        
        poses.push_back(door->getPosition());
    }
    
    //selected door
    auto selected_door=back->getChildByName(StringUtils::format("door_%d",stopPosIndex));
    
    //selector
    auto door_selector=Sprite::createWithSpriteFrameName("door_selector.png");
    door_selector->setPosition(poses.at(0));
    door_selector->setScale(width/door_selector->getContentSize().width);
    door_selector->setOpacity(0);
    back->addChild(door_selector);
    
    //pig
    auto pig=Sprite::createWithSpriteFrameName("door_pig.png");
    pig->setPosition(selected_door->getPosition().x+selected_door->getContentSize().width*.2,selected_door->getPosition().y);
    pig->setScale(width/pig->getContentSize().width);
    pig->setOpacity(0);
    back->addChild(pig);
    
    //open
    auto open=Sprite::createWithSpriteFrameName(StringUtils::format("door_open_%d.png",lot.doorColor));
    open->setPosition(selected_door->getPosition());
    open->setScale(width/open->getContentSize().width);
    open->setLocalZOrder(1);
    open->setOpacity(0);
    back->addChild(open);
    
    //animation
    Vector<FiniteTimeAction*>actions;
    int moveCount=17+stopPosIndex;
    auto ease_count=1+RandomHelper::random_int(0, 3);//ラスト1-4回は極端に減衰
    
    if(performance_roulette==Performance_Roulette_Reverse){
        moveCount=21-stopPosIndex;
    }

    for (int i=0; i<moveCount; i++) {
        auto number=.05f+(float)RandomHelper::random_int(0, 25)*.001;//基準となるsocond .05~.075
        auto duration=number*(1+i/2)+.2*(i+ease_count+1-moveCount)*(i/(moveCount-ease_count));
        actions.pushBack(DelayTime::create(duration));
        auto call=CallFunc::create([this,door_selector,poses,i,moveCount]{
            Common::playSE("pi_3.mp3");
            
            auto index=i%poses.size();
            if(performance_roulette==Performance_Roulette_Reverse){
                index=(poses.size()-i%poses.size())%poses.size();
            }
            
            door_selector->setPosition(poses.at(index));
        });
        actions.pushBack(call);
    }
    
    auto shake_distance=back->getContentSize().width*.01;
    auto shake_duration=.1f;
    auto pig_duration=.3f;
    
    //action
    auto delay_0=DelayTime::create(.3);
    auto show=TargetedAction::create(back,Spawn::create(Sequence::create(ScaleTo::create(.2,1.1*getContentSize().width/back->getContentSize().width),
                                                                         ScaleTo::create(.1,getContentSize().width/back->getContentSize().width), NULL),
                                                        FadeIn::create(.3),
                                                        TargetedAction::create(door_selector, FadeIn::create(.3)), NULL));
    auto delay_rotate=DelayTime::create(.7);
    
    auto call_flashBack=CallFunc::create([this]{this->flashBackColor();});//背景点滅演出
    auto call_nu=CallFunc::create([this]{this->addNuPig();});//ぬっ演出
    auto call_crowd=CallFunc::create([this]{this->addCrowd();});//群演出
    
    auto seq=Sequence::create(actions);//ルーレット回転
    
    auto delay_stop=DelayTime::create(1);
    auto sound_stop=Common::createSoundAction("pinpon.mp3");
    auto flash_selector=TargetedAction::create(door_selector, Repeat::create(Sequence::create(FadeOut::create(.3),
                                                                                              FadeIn::create(.3), NULL), 2));//選択完了
    auto delay_1=DelayTime::create(.5);
    auto shake_sound=Common::createSoundAction("gatagata.mp3");
    auto shake_door=TargetedAction::create(selected_door, Repeat::create(EaseIn::create(Sequence::create(MoveBy::create(shake_duration, Vec2(shake_distance, 0)),
                                                                                                         MoveBy::create(shake_duration*2, Vec2(-shake_distance*2, 0)),
                                                                                                         MoveBy::create(shake_duration, Vec2(shake_distance, 0)), NULL), 1.5) , 3));
    auto fadein_pig=TargetedAction::create(pig, Spawn::create(MoveTo::create(pig_duration, selected_door->getPosition()),
                                                              FadeIn::create(0),
                                                              Common::createSoundAction("nyu.mp3"), NULL));
    auto fadein_open=TargetedAction::create(open, Spawn::create(Common::createSoundAction("gacha.mp3"),
                                                                FadeIn::create(.1), NULL));
    auto particle=CallFunc::create([this,back,open]{
        Common::playSE("chalin.mp3");
        
        BlendFunc blend;
        blend.src = GL_ONE;
        blend.dst = GL_ONE_MINUS_SRC_ALPHA;
        
        auto particle=ParticleSystemQuad::create("particle_shower.plist");
        particle->setTexture(Sprite::create("particle_coin.png")->getTexture());
        particle->setAutoRemoveOnFinish(true);
        particle->setLocalZOrder(1);
        particle->setDuration(.1);
        particle->setLife(3);
        particle->setBlendFunc(blend);
        particle->setStartColor(Color4F(255, 255, 255, 255));
        particle->setEndColor(Color4F(255, 255, 255, 0));
        particle->setStartSize(open->getContentSize().height*.1);
        particle->setEndSize(particle->getStartSize());
        particle->setGravity(Vec2(0, -open->getContentSize().height*1));
        particle->setTotalParticles(lot.coin*30);
        particle->setPosition(open->getPosition());
        particle->resetSystem();
        back->addChild(particle);
    });
    auto delay_call=DelayTime::create(2);
    auto call=CallFunc::create([this]{this->showResultAlert();});
    
    runAction(Sequence::create(delay_0,show,delay_rotate,call_flashBack,call_nu,call_crowd,seq,delay_stop,sound_stop,flash_selector,shake_sound,shake_door,delay_1,fadein_pig,delay_1->clone(),fadein_open,particle,delay_call,call, NULL));
}

void BonusLayer::showResultAlert()
{
    Common::playSE("p.mp3");
    
    auto closeSt = DataManager::sharedManager()->getSystemMessage("close");
    auto onemoreSt = DataManager::sharedManager()->getSystemMessage("login_onemore");
    auto getcoinSt =StringUtils::format(DataManager::sharedManager()->getSystemMessage("login_getcoin").c_str(),lot.coin,MaxLotteryCount-lotteryCount);
    auto canGetMoreLottery=(lotteryCount<MaxLotteryCount);
    
    //分析 次が最後ならrare以上確定かどうかを分類
    if (lotteryCount+1==MaxLotteryCount) {
        isRare=true;
        getcoinSt.append(DataManager::sharedManager()->getSystemMessage("login_rare"));
    }
    
    //促進アラート表示を記録
    if (lotteryCount==1) {
        AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_GetRoulette_0);
    }
    else if (lotteryCount==2){
        AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_GetRoulette_1);
    }

    //まだ抽選できる場合はメニューを追加
    std::vector<std::string>buts;
    buts.push_back(closeSt);
    if (canGetMoreLottery) {
        buts.push_back(onemoreSt);
    }
    
    //imageは2パターン用意
    auto alert = ContentsAlert::create(StringUtils::format("get_coin_%d.png",RandomHelper::random_int(0, 1)), "", StringUtils::format(getcoinSt.c_str(),lot.coin), buts, [this](Ref* sender){
        
        auto al = (ContentsAlert*)sender;
        auto selectedNum = al->getSelectedButtonNum();
        
        if (selectedNum == 0) {
            this->close();
        }
        else
        {//再抽選
            this->checkConditionOfReward();
        }
    });
    alert->showAlert(this);
}

void BonusLayer::checkConditionOfReward()
{
    if (!RewardMovieManager::sharedManager()->isPrepared()) {//動画の読み込みができていない。
        auto message =DataManager::sharedManager()->getSystemMessage("no_video");
        
        if (Common::isAndroid()) {
            message += DataManager::sharedManager()->getSystemMessage("no_video_android");
        }
        
        auto confirmAlert = CustomAlert::create("", message, {"Close","Reload"}, [this](Ref*ref){
            auto _ref=(CustomAlert*)ref;
            if (_ref->getSelectedButtonNum()==0) {
                this->close();
            }
            else{
                this->checkConditionOfReward();
            }
        });
        confirmAlert->showAlert(this);
    }
    else {//動画の再生準備が整っている
        RewardMovieManager::sharedManager()->play([this](bool complete){
            if (complete) {
                this->restart();
            }
            else{
                log("再生に失敗した");
                close();
            }
        });
    }
}

void BonusLayer::restart()
{
    //次がラスト
    if (lotteryCount+1==MaxLotteryCount) {
        AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_GetRoulette_2);
    }
    
    auto back=this->getChildByName("back");
    if(back){
        auto disapper=TargetedAction::create(back, Spawn::create(ScaleBy::create(.3, .1),
                                                                 FadeOut::create(.3), NULL));
        auto remove=TargetedAction::create(back, RemoveSelf::create());
        auto call=CallFunc::create([this]{this->show(false);});
        
        this->runAction(Sequence::create(disapper,remove,call, NULL));
    }
    else{
        show(false);
    }
}

void BonusLayer::close()
{
    auto back=getChildByName("back");
    auto duration=.5;
    runAction(Sequence::create(Spawn::create(TargetedAction::create(back, FadeOut::create(duration)),
                                             FadeOut::create(duration), NULL),
                               CallFunc::create([this]{
        Common::stopBgm();
        mCallBack();}),
                               RemoveSelf::create(), NULL));
}

std::string BonusLayer::getKeyOfLottelyCountToday()
{
    auto todayServerDate = FireBaseBridge::getInstance()->getServerTimestamp();
    auto key_today=StringUtils::format(UserKey_GetRoulette,Common::transformTimeToDate(todayServerDate));
    
    return key_today;
}

#pragma mark- タッチ
void BonusLayer::addTouchListener()
{
    auto listener=EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);//タッチ透過しない
    
    listener->onTouchBegan=[this](Touch*touch, Event*event)
    {
        return true;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}
