//
//  PatternSprite.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/04/17.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________PatternSprite_____
#define _____PROJECTNAMEASIDENTIFIER________PatternSprite_____

#include "cocos2d.h"
USING_NS_CC;

typedef std::function<void(bool success)> onFinished;

class PatternSprite:public Sprite
{
public:
    static PatternSprite* create(Node*parent);
    bool init(Node*parent);
    /**parent基準のタッチ座標を渡す*/
    void drag(Vec2 pos,bool onItem);
    /**描画されている線、点を全て消す*/
    void clear(bool onItem);
    /**タッチ終了後に呼び出す ドラッグ中のラインを削除*/
    void end(bool onItem);
    /**正解判定*/
    void check(Node*parent,ValueMap answerMap,ValueMap map,bool onItem,const onFinished& callback);
    /**線の始点*/
    Vec2 beginPos;
    Vector<DrawNode*>drawNodes;
    
    DrawNode* drawLineOnPattern(Vec2 start,Vec2 end);
    void drawDotOnPattern(Vec2 pos);
    Vec2 getCenterOnStartArea(int index);
    
    /**描画開始点の原点(default左下)*/
    CC_SYNTHESIZE(bool, centeringAnchor, CenteringAnchor);
    /**描画開始点の座標(default左下原点) 終了点にも使う*/
    CC_SYNTHESIZE(std::vector<Vec2>, startAreas, StartAreas);
    CC_SYNTHESIZE(float, lineWidth, LineWidth);
    CC_SYNTHESIZE(Color4F, lineColor, LineColor);
    /**描画開始点からの開始範囲幅 正方形*/
    CC_SYNTHESIZE(float, startAreaWidth, StartAreaWidth);
    /**描画開始点を利用した終了範囲幅 開始範囲の中央にポジショニングする 正方形*/
    CC_SYNTHESIZE(float, endAreaWidth, EndAreaWidth);
    CC_SYNTHESIZE(std::string, answer, Answer);

    /**点追加時効果音*/
    CC_SYNTHESIZE(std::string, playSE, PlaySE);
private:
    void addNewLine(Vec2 pos,int index);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
