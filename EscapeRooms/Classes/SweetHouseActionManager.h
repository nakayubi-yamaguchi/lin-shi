//
//  IzakayaActionManager.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/10/17.
//
//

#ifndef __EscapeContainer__SweetHouseActionManagerManager__
#define __EscapeContainer__SweetHouseActionManagerManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class SweetHouseActionManager:public CustomActionManager
{
public:
    static SweetHouseActionManager* manager;
    static SweetHouseActionManager* getInstance();
    
    //over ride
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void customAction(std::string key,Node*parent);
    void storyAction(std::string key, cocos2d::Node *parent, const onFinished &callback);
    void showAnswerAction(std::string key, cocos2d::Node *parent);
    
private:
    FiniteTimeAction*moveHat(Node*parent,Node*hat,bool animate);
    FiniteTimeAction*candyPullAction(Node*parent,int index);
    FiniteTimeAction*addSkullSmoke(Node*parent,int backID);
    FiniteTimeAction*giveGunAction(Node*parent,Node*gun,bool animate);
    FiniteTimeAction*addSmoke(Node*parent,int backID);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
