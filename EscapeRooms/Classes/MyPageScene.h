//
//  MyPageScene.h
//  EscapeRooms
//
//  Created by yamaguchinarita on 2018/03/19.
//
//

#ifndef _____EscapeRooms________MyPageScene_____
#define _____EscapeRooms________MyPageScene_____

#include "cocos2d.h"

USING_NS_CC;

class MyPageScene : public cocos2d::LayerColor
{
public:
    virtual bool init();
    static Scene *createScene();
    CREATE_FUNC(MyPageScene);
    
    
private:
    void createMain();
    
    void createMenues();
    
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
