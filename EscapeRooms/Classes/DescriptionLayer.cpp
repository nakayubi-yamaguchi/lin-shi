//
//  DescriptionLayer.cpp
//  Princess
//
//  Created by 成田凌平 on 2017/09/09.
//
//

#include "DescriptionLayer.h"
#include "Utils/Common.h"
#include "SupplementsManager.h"
#include "DataManager.h"
#include "EscapeDataManager.h"
#include "AnalyticsManager.h"
#include "UIParts/TextMenuItem.h"

using namespace cocos2d;

DescriptionLayer* DescriptionLayer::create(const cocos2d::ccMenuCallback &callback, float opacity, bool singleImage)
{
    auto node =new DescriptionLayer();
    if (node && node->init(callback,opacity,singleImage)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool DescriptionLayer::init() {

    return init(nullptr,200,false);
}

bool DescriptionLayer::init(const cocos2d::ccMenuCallback &callback, float opacity, bool singleImage)
{
    if (!LayerColor::initWithColor(Color4B(0, 0, 0, opacity))) {
        return false;
    }
    
    mCallback = callback;
    mOpacity=opacity;
    showIndex=0;
    
    if (!singleImage) {
        setUseMultiImage(true);
    }
    
    if (singleImage||EscapeDataManager::getInstance()->getTypePlayTitleCondition()==TitleCondition_TUTORIAL) {
        endIndex=0;
    }
    else{
        endIndex=2;
    }
    
    //やり方説明画面では、ネイティブ広告を非表示。
    SupplementsManager::getInstance()->hidePanel();

    setOpacity(0);
    // 初期化
    addTouchListener();
    
    createMain();
    
    return true;
}

#pragma mark- UI
void DescriptionLayer::createMain()
{
    setCascadeOpacityEnabled(false);
    
    Vector<FiniteTimeAction*>actions;
    bool tutorial=(EscapeDataManager::getInstance()->getTypePlayTitleCondition()==TitleCondition_TUTORIAL);
    
    auto image_width=getContentSize().width*.25;
    auto space=getContentSize().width*.05;
    auto duration=.7;
    auto fontSize=getContentSize().width*.05;
    
    std::vector<std::string>strings={
        DataManager::sharedManager()->getSystemMessage("description_tap"),
        DataManager::sharedManager()->getSystemMessage("description_doubletap"),
        DataManager::sharedManager()->getSystemMessage("description_hint")
    };
    
    std::vector<std::string>fileNames={"finger_0.png","finger_1.png","description_hint.png"};
    
    if (tutorial||!getUseMultiImage()) {
        //タッチによる切り替えなしパターン
        
        auto count=fileNames.size();
        if (tutorial) {
            //tutorialの場合はタップ、ダブルタップのみ説明する
            count=2;
        }
        else{
            //Topにヒント表示する際はタップ、ヒント、スワイプの説明を行う
            strings.erase(++strings.begin());
            strings.push_back(DataManager::sharedManager()->getSystemMessage("description_swipe"));
            fileNames.erase(++fileNames.begin());
            fileNames.push_back("finger_2.png");
        }
        
        for (int i=0; i<count; i++) {
            auto posY=getContentSize().height*(.7-.2*i);
            if (count==2) {
                posY=getContentSize().height*(.625-.25*i);
            }
            
            auto spr=Sprite::createWithSpriteFrameName(fileNames.at(i));
            spr->setScale(image_width/spr->getContentSize().width);
            spr->setPosition(space+image_width/2, posY);
            spr->setOpacity(0);
            addChild(spr);
            
            actions.pushBack(TargetedAction::create(spr, FadeIn::create(duration)));
            
            auto label=Label::createWithTTF(strings.at(i), Common::getUsableFontPath(MyFont), fontSize);
            label->setWidth(getContentSize().width-image_width-space*3);
            label->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
            label->setPosition(spr->getPosition().x+spr->getBoundingBox().size.width/2+label->getContentSize().width/2+space, spr->getPosition().y);
            label->setOpacity(0);
            addChild(label);
            
            actions.pushBack(TargetedAction::create(label, FadeIn::create(duration)));
        }
    }
    else
    {//タップによる切り替えありパターン
        image_width=getContentSize().width*.15;
        space=getContentSize().width*.05;
        auto imagePosY=getContentSize().height*.55;
        auto labelPosY=imagePosY-image_width-space;
        if (showIndex==0) {
            
            auto tapImage=Sprite::createWithSpriteFrameName("finger_0.png");
            tapImage->setPosition(getContentSize().width*.5,imagePosY);
            tapImage->setScale(image_width/tapImage->getContentSize().width);
            tapImage->setOpacity(0);
            addChild(tapImage);
            actions.pushBack(TargetedAction::create(tapImage, FadeIn::create(duration)));
            
            auto label=Label::createWithTTF(strings.at(0), Common::getUsableFontPath(MyFont), fontSize);
            label->setWidth(getContentSize().width);
            label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
            label->setPosition(tapImage->getPosition().x, labelPosY);
            label->setOpacity(0);
            addChild(label);
            
            actions.pushBack(TargetedAction::create(label, FadeIn::create(duration)));
            
            if (EscapeDataManager::getInstance()->isUsedShakeListenerOnPlayTitle()) {//シェイクのあるステージなら
                
                //タップイメージの位置をずらす
                auto deltaY = getContentSize().height*.15;
                tapImage->setPositionY(tapImage->getPositionY()+deltaY);
                label->setPositionY(labelPosY+deltaY);
                
                //シェイクイメージの生成
                auto shakeImage=Sprite::createWithSpriteFrameName("finger_shake.png");
                shakeImage->setPosition(getContentSize().width*.5,imagePosY-deltaY);
                shakeImage->setScale(image_width/shakeImage->getContentSize().width);
                shakeImage->setOpacity(0);
                addChild(shakeImage);
                actions.pushBack(TargetedAction::create(shakeImage, FadeIn::create(duration)));
                
                auto shakelabel=Label::createWithTTF(DataManager::sharedManager()->getSystemMessage("description_shake"), Common::getUsableFontPath(MyFont), fontSize);
                shakelabel->setWidth(getContentSize().width);
                shakelabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
                shakelabel->setPosition(shakeImage->getPosition().x, labelPosY-deltaY);
                shakelabel->setOpacity(0);
                addChild(shakelabel);
                
                actions.pushBack(TargetedAction::create(shakelabel, FadeIn::create(duration)));
            }
            
        }
        else if(showIndex==1){
            //ヒントの説明
            auto finger=Sprite::createWithSpriteFrameName("finger.png");
            auto distance=DataManager::sharedManager()->getItemBoxWidth()*.25;

            finger->setPosition(DataManager::sharedManager()->getItemBoxWidth()+image_width/2, DataManager::sharedManager()->getMainSpriteRect().origin.y+DataManager::sharedManager()->getMainSpriteRect().size.height-image_width/2-distance);
            finger->setOpacity(0);
            finger->setScale(image_width/finger->getContentSize().width);
            addChild(finger);
            
            actions.pushBack(TargetedAction::create(finger, Spawn::create(FadeIn::create(duration),Repeat::create(Sequence::create(EaseInOut::create(MoveBy::create(1, Vec2(-distance, distance)), 1.5),
                                                                                                                                   EaseInOut::create( MoveBy::create(1, Vec2(distance, -distance)),1.5), NULL), UINT_MAX), NULL)));

            auto label=Label::createWithTTF(strings.at(2), Common::getUsableFontPath(MyFont), fontSize);
            label->setWidth(getContentSize().width);
            label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
            label->setPosition(getContentSize().width/2,getContentSize().height/2);
            label->setOpacity(0);
            addChild(label);
            actions.pushBack(TargetedAction::create(label, FadeIn::create(duration)));
        }
        else if(showIndex==2){
            //ドラッグの説明
            auto finger=Sprite::createWithSpriteFrameName("finger.png");
            
            finger->setPosition(getContentSize().width-image_width*.25, getContentSize().height/2);
            finger->setOpacity(0);
            finger->setScale(-image_width/finger->getContentSize().width,image_width/finger->getContentSize().width);
            addChild(finger);
            
            auto distance=getContentSize().width*.1;
            auto originalPos=finger->getPosition();
            actions.pushBack(TargetedAction::create(finger, Spawn::create(FadeIn::create(duration),Repeat::create(Sequence::create(EaseInOut::create(MoveBy::create(1, Vec2(-distance, 0)), 1.5),
                                                                                                                                   FadeOut::create(.7),
                                                                                                                                   MoveTo::create(.1, originalPos),
                                                                                                                                   FadeIn::create(.7),
                                                                                                                                NULL), UINT_MAX), NULL)));
            
            auto label=Label::createWithTTF(DataManager::sharedManager()->getSystemMessage("description_swipe"), Common::getUsableFontPath(MyFont), fontSize);
            label->setWidth(getContentSize().width);
            label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
            label->setPosition(getContentSize().width/2,finger->getPosition().y-image_width/2-label->getContentSize().height/2);
            label->setOpacity(0);
            addChild(label);
            actions.pushBack(TargetedAction::create(label, FadeIn::create(duration)));
        }
    }
    
    //skipbutton 過去にクリアステージがある場合のみ表示
    if (DataManager::sharedManager()->getTotalStageRecord(RecordKey_ClearGame)>0) {
        auto space_skip=getContentSize().height*.025;
        auto skip_label=Label::createWithTTF("SKIP", HiraginoMaruFont, getContentSize().height*.033);
        skip_label->setTextColor(Color4B::WHITE);
        auto skip=/*TextMenuItem::create("SKIP", Size(getContentSize().width*.15, getContentSize().height*.04), Color3B::WHITE, [](){}])*/MenuItemLabel::create(skip_label, [this](Ref*ref){
            auto _ref=(MenuItemLabel*)ref;
            if (_ref->getOpacity()==255) {
                showIndex=endIndex;
                this->end();
            }
        });
        skip->setPosition(getContentSize().width-skip->getContentSize().width/2-space_skip, DataManager::sharedManager()->getMainSpriteRect().origin.y+DataManager::sharedManager()->getMainSpriteRect().size.height-skip->getContentSize().height/2-space_skip);
        auto skip_menu=Menu::create(skip, NULL);
        skip_menu->setPosition(Vec2::ZERO);
        skip_menu->setOpacity(0);
        addChild(skip_menu);
        actions.pushBack(TargetedAction::create(skip_menu, FadeIn::create(duration)));
    }
    
    //アニメーション
    auto fadein=FadeTo::create(1, mOpacity);
    auto call=CallFunc::create([this]{
        isShowed=true;
    });
    actions.pushBack(Sequence::create(fadein,call, NULL));
    
    runAction(Spawn::create(actions));
}

#pragma mark- タッチ
void DescriptionLayer::addTouchListener()
{
    auto listener=EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);//タッチ透過しない
    
    listener->onTouchBegan=[this](Touch*touch, Event*event)
    {
        this->end();
        
        return true;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

void DescriptionLayer::end()
{
    if (isShowed) {
        isShowed=false;
        Vector<FiniteTimeAction*>actions;
        this->setCascadeOpacityEnabled(true);
        
        auto end=(showIndex==endIndex);
        auto duration=(end?1.0:.7);
        auto fadeout = FadeOut::create(duration);
        actions.pushBack(fadeout);
        auto removeChildren=CallFunc::create([this]{
            this->removeAllChildren();
        });
        actions.pushBack(removeChildren);
        
        if (end) {
            auto callback = CallFunc::create([this](){
                if (mCallback) {
                    mCallback(this);
                }
            });
            actions.pushBack(callback);
            auto remove = RemoveSelf::create();
            actions.pushBack(remove);
        }
        else{
            auto call=CallFunc::create([this]{
                showIndex++;
                this->createMain();
            });
            actions.pushBack(call);
        }
        
        this->runAction(Sequence::create(actions));
    }
}

#pragma mark- 解放
DescriptionLayer::~DescriptionLayer()
{
    log("Descriptionレイヤーを解放する。");
}
