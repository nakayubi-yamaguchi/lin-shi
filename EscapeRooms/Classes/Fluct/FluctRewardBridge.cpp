//
//  FluctRewardBridge.cpp
//  FluctAdXxFluctSDK-mobile
//
//  Created by RyoKosuge on 2019/03/29.
//

#include "FluctRewardBridge.h"
#include "platform/android/jni/JniHelper.h"

#define FLUCT_REWARD_CLASS_NAME "org/cocos2dx/cpp/FluctRewardBridge"

using namespace cocos2d;

static FluctRewardBridgeDelegate* sFluctRewardCallback = NULL;

void FluctRewardBridge::setDelegate(FluctRewardBridgeDelegate* delegate) {
    sFluctRewardCallback = delegate;
}

void FluctRewardBridge::loadReward() {
    cocos2d::JniMethodInfo t;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, FLUCT_REWARD_CLASS_NAME, "loadFluctReward",
                                                "(Ljava/lang/String;Ljava/lang/String;)V")) {
        auto groupID = MOVIE_REWARD_FLUCT_GROUP_ID;
        auto unitID = MOVIE_REWARD_FLUCT_UNIT_ID;
        jstring utfGroupID = t.env->NewStringUTF(groupID);
        jstring utfUnitID = t.env->NewStringUTF(unitID);
        t.env->CallStaticVoidMethod(t.classID, t.methodID, utfGroupID, utfUnitID);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(utfGroupID);
        t.env->DeleteLocalRef(utfUnitID);
    }
}

bool FluctRewardBridge::isReady() {
    cocos2d::JniMethodInfo t;
    bool isPrepared = false;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, FLUCT_REWARD_CLASS_NAME, "isReady", "()Z")) {
        jboolean ready = (jboolean) t.env->CallStaticBooleanMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
        isPrepared = ready;
    }
    return isPrepared;
}

void FluctRewardBridge::show() {
    cocos2d::JniMethodInfo t;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, FLUCT_REWARD_CLASS_NAME, "show", "()V")) {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}

#ifdef __cplusplus
extern "C"
{
#endif

JNIEXPORT void Java_org_cocos2dx_cpp_FluctRewardBridge_didStart(JNIEnv* env, jobject object) {
    if (sFluctRewardCallback != nullptr) {
        sFluctRewardCallback->didStartFluctReward();
    }
}

JNIEXPORT void
Java_org_cocos2dx_cpp_FluctRewardBridge_didClose(JNIEnv* env, jobject object, jboolean completed) {
    if (sFluctRewardCallback != nullptr) {
        sFluctRewardCallback->didCloseFluctReward(completed);
    }
}

#ifdef __cplusplus
};
#endif
