//
//  FluctRewardBridge.m
//  FluctAdXxFluctSDK-mobile
//
//  Created by RyoKosuge on 2019/03/29.
//

#import "FluctRewardBridge.h"
#import <FluctSDK/FluctSDK.h>

@interface FluctRewardCallback: NSObject<FSSRewardedVideoDelegate>
@property (nonatomic) bool completeMovie;
@property (nonatomic) int loadingCount;
@property (nonatomic) FluctRewardBridgeDelegate *delegate_id;
@end

@implementation FluctRewardCallback

- (void)rewardedVideoDidLoadForGroupID:(NSString *)groupId unitId:(NSString *)unitId {
    FluctRewardBridge::logOfBridge("reward did load.");
}

- (void)rewardedVideoShouldRewardForGroupID:(NSString *)groupId unitId:(NSString *)unitId {
    FluctRewardBridge::logOfBridge("reward should reward.");
    self.completeMovie = true;
}

- (void)rewardedVideoWillAppearForGroupId:(NSString *)groupId unitId:(NSString *)unitId {
    FluctRewardBridge::logOfBridge("reward will appear.");
}

- (void)rewardedVideoDidAppearForGroupId:(NSString *)groupId unitId:(NSString *)unitId {
    FluctRewardBridge::logOfBridge("reward did appear.");
    if (self.delegate_id) {
        self.delegate_id->didStartFluctReward();
    }
}

- (void)rewardedVideoWillDisappearForGroupId:(NSString *)groupId unitId:(NSString *)unitId {
    FluctRewardBridge::logOfBridge("reward will disappear.");
}

- (void)rewardedVideoDidDisappearForGroupId:(NSString *)groupId unitId:(NSString *)unitId {
    FluctRewardBridge::logOfBridge("reward did disappear.");
    if (self.delegate_id) {
        self.delegate_id->didCloseFluctReward(self.completeMovie);
    }
}

- (void)rewardedVideoDidFailToLoadForGroupId:(NSString *)groupId unitId:(NSString *)unitId error:(NSError *)error {
    FluctRewardBridge::logOfBridge("reward did fail to load.");
    NSLog(@"error:  %@", error);
}

- (void)rewardedVideoDidFailToPlayForGroupId:(NSString *)groupId unitId:(NSString *)unitId error:(NSError *)error {
    FluctRewardBridge::logOfBridge("reward did fail to play.");
    NSLog(@"error:  %@", error);
}

@end

#pragma mark - Fluctリワードbridge
FluctRewardCallback *fluctRewardCallback = [FluctRewardCallback new];

void FluctRewardBridge::loadReward()
{
    FSSRewardedVideo.sharedInstance.delegate = fluctRewardCallback;
    NSString *groupID = [NSString stringWithUTF8String:MOVIE_REWARD_FLUCT_GROUP_ID];
    NSString *unitID = [NSString stringWithUTF8String:MOVIE_REWARD_FLUCT_UNIT_ID];
    [[FSSRewardedVideo sharedInstance] loadRewardedVideoWithGroupId:groupID
                                                             unitId:unitID];
}

bool FluctRewardBridge::isReady()
{
    NSString *groupID = [NSString stringWithUTF8String:MOVIE_REWARD_FLUCT_GROUP_ID];
    NSString *unitID = [NSString stringWithUTF8String:MOVIE_REWARD_FLUCT_UNIT_ID];
    return [[FSSRewardedVideo sharedInstance] hasAdAvailableForGroupId:groupID
                                                                unitId:unitID];
}

void FluctRewardBridge::show()
{
    if (isReady()) {
        UIViewController *viewController = UIApplication.sharedApplication.keyWindow.rootViewController;
        if (viewController) {
            NSString *groupID = [NSString stringWithUTF8String:MOVIE_REWARD_FLUCT_GROUP_ID];
            NSString *unitID = [NSString stringWithUTF8String:MOVIE_REWARD_FLUCT_UNIT_ID];
            [[FSSRewardedVideo sharedInstance] presentRewardedVideoAdForGroupId:groupID
                                                                         unitId:unitID
                                                             fromViewController:viewController];
        }
    }
}

void FluctRewardBridge::logOfBridge(std::string text)
{
    NSLog(@"[%@]%s", [NSString stringWithUTF8String:LOG_FLUCT_REWARD], text.c_str());
}

void FluctRewardBridge::setDelegate(FluctRewardBridgeDelegate *delegate)
{
    if (fluctRewardCallback) {
        if (fluctRewardCallback.delegate_id) {
            fluctRewardCallback.delegate_id = nil;
        }
        
        fluctRewardCallback.delegate_id = delegate;
    }
}
