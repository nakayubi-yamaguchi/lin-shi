//
//  FluctRewardBridge.h
//  FluctAdXxFluctSDK
//
//  Created by RyoKosuge on 2019/03/29.
//

#ifndef FluctRewardBridge_h
#define FluctRewardBridge_h

#include "../Utils/AdConstants.h"
#include "../Utils/Common.h"


#define LOG_FLUCT_REWARD "Fluctのリワード広告"

class FluctRewardBridgeDelegate
{
public:
    virtual void didStartFluctReward(){};
    virtual void didCloseFluctReward(bool completed){};
};

class FluctRewardBridge
{
public:
    
    static void loadReward();
    static bool isReady();
    static void show();
    
    static void setDelegate(FluctRewardBridgeDelegate* delegate);
    static void logOfBridge(std::string text);
};

#endif /* FluctRewardBridge_h */
