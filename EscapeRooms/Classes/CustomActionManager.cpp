//
//  CustomActionManager.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/03/12.
//
//

#include "CustomActionManager.h"
#include "Utils/Common.h"
#include "EscapeStageSprite.h"
#include "AnchorSprite.h"
#include "CustomActionKeys.h"
#include "SupplementsManager.h"
#include "DataManager.h"

using namespace cocos2d;

int CustomActionManager::getDefaultBackNum()
{
    return 1;
}

#pragma mark - カスタムアクション
void CustomActionManager::openingAction(cocos2d::Node *node, const onFinished &callback)
{
    callback(false);
}

void CustomActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    callback(true);
}


void CustomActionManager::clearParticleAction(cocos2d::Node *parent)
{//デフォルトはカラフルなパーティクル
    
    std::vector<Color4F>colors;
    colors.push_back(Color4F(Common::getColorFromHex("3681a8")));//濃い青
    colors.push_back(Color4F(Common::getColorFromHex("dd7672")));//濃いオレンジ
    colors.push_back(Color4F(Common::getColorFromHex("1da481")));//濃い緑
    colors.push_back(Color4F(Common::getColorFromHex("6db134")));//明るい緑
    colors.push_back(Color4F(Common::getColorFromHex("f0a85c")));//黄色
    colors.push_back(Color4F(Common::getColorFromHex("d3146c")));//赤色
    
    for (int i=0; i<colors.size(); i++) {
        auto particle=ParticleSystemQuad::create("particle_texture.plist");
        particle->setAutoRemoveOnFinish(true);
        particle->resetSystem();
        
        particle->setStartSize(parent->getContentSize().width*.035);
        particle->setStartSizeVar(parent->getContentSize().width*.005);
        particle->setEndSize(particle->getStartSize());
        particle->setSpeed(parent->getContentSize().height*.2);
        particle->setPosition(parent->getContentSize().width/2,parent->getContentSize().height+particle->getStartSize());
        particle->setPosVar(Vec2(parent->getContentSize().width*.8, 0));
        particle->setStartColor(colors[i]);
        particle->setEndColor(colors[i]);
        parent->addChild(particle);
    }
}

#pragma mark - addSprite
Sprite* CustomActionManager::createSpriteToCenter(std::string name, bool invisible, cocos2d::Node *parent)
{
    return createSpriteToCenter(name, invisible, parent, false);
}

Sprite* CustomActionManager::createSpriteToCenter(std::string name, bool invisible, cocos2d::Node *parent, bool addChild)
{
    return SupplementsManager::getInstance()->addSupplementToCenter(parent, name, invisible, addChild);
}

Node* CustomActionManager::createClippingNodeToCenter(std::string name, Node *parent)
{
    auto clip=ClippingNode::create(createSpriteToCenter(name, false, parent));
    clip->setAlphaThreshold(0);
    clip->setInverted(false);
    clip->setCascadeOpacityEnabled(true);
    clip->setName(name);
    parent->addChild(clip);
    
    return clip;
}

Sprite* CustomActionManager::createSpriteOnClip(std::string name, bool invisible, PopUpLayer *popup, bool addChild)
{
    auto clip=popup->clip;
    
    auto spr = EscapeStageSprite::createWithSpriteFileName(name);
    spr->setPosition(clip->getStencil()->getPosition());
    spr->setScale(popup->itemImage->getScale());
    spr->setCascadeOpacityEnabled(true);
    spr->setName(name);
    if (invisible) {
        spr->setOpacity(0);
    }
    if (addChild) {
        clip->addChild(spr);
    }
    
    return spr;
}

Sprite* CustomActionManager::createAnchorSprite(std::string name, float x, float y, bool invisible, cocos2d::Node *parent, bool addChild)
{
    auto spr = AnchorSprite::create(Vec2(x, y), parent->getContentSize(), name);
    spr->setCascadeOpacityEnabled(true);
    if (invisible) {
        spr->setOpacity(0);
    }
    if (addChild) {
        parent->addChild(spr);
    }
    
    return spr;
}

void CustomActionManager::transformToAnchorSprite(Node *parent, Node *targetNode, Vec2 anchorPoint)
{
    SupplementsManager::getInstance()->transformToAnchorSprite(parent, targetNode, anchorPoint);
}

Sprite* CustomActionManager::createSpriteOnStoryCutIn(std::string name, bool invisible, float width, StoryLayer *parent)
{
    auto item=EscapeStageSprite::createWithSpriteFileName(name);
    item->setScale(width/item->getContentSize().width);
    item->setPosition(parent->cutinSprite->getContentSize().width/2, parent->cutinSprite->getContentSize().height/2+parent->cutinSprite->getContentSize().height/2+item->getBoundingBox().size.height/2);
    if (invisible) {
        item->setOpacity(0);
    }
    parent->cutinSprite->addChild(item);
    
    return item;
}

#pragma mark - Change Action
Spawn* CustomActionManager::createChangeAction(float duration_fadeout,float duration_fadein, cocos2d::Node *before, cocos2d::Node *after)
{
    return createChangeAction(duration_fadeout, duration_fadein, before, after, "");
}

Spawn* CustomActionManager::createChangeAction(float duration_fadeout,float duration_fadein, cocos2d::Node *before, cocos2d::Node *after,std::string soundName)
{
    auto spawn=Spawn::create(TargetedAction::create(before, FadeOut::create(duration_fadeout)),
                             TargetedAction::create(after, FadeIn::create(duration_fadein)),
                             createSoundAction(soundName), NULL);
    
    return spawn;
}

Spawn* CustomActionManager::createChangeAction(float duration_fadeout, float duration_fadein, Node *before, Node *after, std::string soundName, ShowType type)
{
    auto spawn=Spawn::create(TargetedAction::create(before, FadeOut::create(duration_fadeout)),
                             TargetedAction::create(after, FadeIn::create(duration_fadein)),
                             createSoundAction(soundName, type), NULL);
    
    return spawn;
}

FiniteTimeAction* CustomActionManager::createChangeActionSequence(float duration_fadeout, float duration_fadein, Vector<Node *> nodes, std::vector<std::string> soundnames)
{
    auto counter=0;
    Vector<FiniteTimeAction*>actions;
    for(auto node : nodes){
        if (counter==0) {
            actions.pushBack(TargetedAction::create(node, FadeIn::create(duration_fadein)));
        }
        else{
            actions.pushBack(createChangeAction(duration_fadeout, duration_fadein, nodes.at(counter-1), node));
        }
        
        if (soundnames.size()>counter) {
            actions.pushBack(createSoundAction(soundnames.at(counter)));
        }
        actions.pushBack(DelayTime::create(duration_fadeout));
        
        counter++;
    }
    
    return Sequence::create(actions);
}

Spawn* CustomActionManager::createChangeAction(float duration_fadeout, float duration_fadein, Vector<Node *> befores, Vector<Node *> afters)
{
    return createChangeAction(duration_fadeout, duration_fadein, befores, afters, "");
}

Spawn* CustomActionManager::createChangeAction(float duration_fadeout, float duration_fadein, Vector<Node *> befores, Vector<Node *> afters, std::string soundName)
{
    Vector<FiniteTimeAction*> spawnActions;
    
    for (auto before : befores) {
        spawnActions.pushBack(TargetedAction::create(before,  FadeOut::create(duration_fadeout)));
    }
    
    for (auto after : afters) {
        spawnActions.pushBack(TargetedAction::create(after,  FadeIn::create(duration_fadein)));
    }
    
    if (soundName.size() > 0) {
        spawnActions.pushBack(createSoundAction(soundName));
    }
    
    auto spawn=Spawn::create(spawnActions);
    
    return spawn;
}

#pragma mark - Bounce
FiniteTimeAction* CustomActionManager::createBounceAction(cocos2d::Node *node, float duration)
{
    return createBounceAction(node, duration,.1);
}

FiniteTimeAction* CustomActionManager::createBounceAction(cocos2d::Node *node, float duration, float deflection)
{
    auto scale=node->getScale();
    return createBounceAction(node, duration, deflection, scale);
}

FiniteTimeAction* CustomActionManager::createBounceAction(cocos2d::Node *node, float duration, float deflection ,float original_scale)
{
    auto scale=original_scale;
    auto seq=Sequence::create(EaseIn::create(ScaleTo::create(duration, scale*(1+deflection), scale*(1-deflection/2)), 1.5) ,
                              EaseOut::create(ScaleTo::create(duration, scale*(1-deflection/2), scale*(1+deflection/2)), 1.5),
                              ScaleTo::create(duration/2, scale)
                              , NULL);
    
    return TargetedAction::create(node, seq);
}

TargetedAction* CustomActionManager::createBounceActionToTargetScale(cocos2d::Node *node, float duration, float deflection, float targetScale,float ease,float scale_original)
{
    auto scale_half=sqrt(targetScale/scale_original);//直線的に変化する場合の中点
    auto scale_0=Vec2(scale_half*(1-deflection)*scale_original, scale_half*(1+deflection)*scale_original);
    auto scale_1=Vec2(targetScale*(1+deflection), targetScale*(1-deflection));
    return TargetedAction::create(node, Sequence::create(EaseIn::create(ScaleTo::create(duration*.4, scale_0.x,scale_0.y), ease),
                                                         EaseOut::create(ScaleTo::create(duration*.4, scale_1.x,scale_1.y), ease),
                                                         EaseOut::create(ScaleTo::create(duration*.2, targetScale), ease),
                                                         NULL));
}

FiniteTimeAction* CustomActionManager::bounceJumpAction(Node *targetNode, Vec2 targetAnchorPoint, Node* parent, std::string jumpSoundName, int repeatCount)
{
    transformToAnchorSprite(parent, targetNode, targetAnchorPoint);

    return Repeat::create(bounceJumpToAction(targetNode, targetAnchorPoint, targetNode->getPosition(), parent, .24, parent->getContentSize().height*.1, jumpSoundName), repeatCount);
}

FiniteTimeAction* CustomActionManager::bounceJumpToAction(Node *targetNode, Vec2 targetAnchorPoint, Vec2 jumpToPoint, Node *parent, float jumpDuration, float jumpHeight, std::string jumpSoundName)
{
    float defaultScaleY = targetNode->getScaleY();
    transformToAnchorSprite(parent, targetNode, targetAnchorPoint);

    auto scaleSmallAct = ScaleTo::create(.2, targetNode->getScaleX(), defaultScaleY * 0.92);

    auto delayAct = DelayTime::create(0.15f);
    auto scaleBigAct = ScaleTo::create(0.1f, targetNode->getScaleX(), defaultScaleY);
    auto jumpAct = JumpTo::create(jumpDuration, jumpToPoint, jumpHeight, 1);
    auto spawn = Spawn::create(scaleBigAct, jumpAct, createSoundAction(jumpSoundName), NULL);
    
    return TargetedAction::create(targetNode, Sequence::create(scaleSmallAct, delayAct, spawn, NULL));
}

#pragma mark - jump
TargetedAction* CustomActionManager::jumpAction(float height,float duration, cocos2d::Node *node, float ease)
{
    return TargetedAction::create(node, Sequence::create(EaseOut::create(MoveBy::create(duration, Vec2(0, height)), ease),
                                                         EaseIn::create(MoveBy::create(duration, Vec2(0,-height)), ease), NULL));
}

TargetedAction* CustomActionManager::jumpAction(float height,float duration, cocos2d::Node *node, float ease,int repeat)
{
    return TargetedAction::create(node, Repeat::create(jumpAction(height, duration, node, ease), repeat));
}

TargetedAction* CustomActionManager::jumpAction(cocos2d::Vec2 vec, float duration, cocos2d::Node *node, float ease)
{
    return jumpAction(vec, duration, node, ease, 1);
}

TargetedAction* CustomActionManager::jumpAction(cocos2d::Vec2 vec, float duration, cocos2d::Node *node, float ease,int repeat)
{
    return TargetedAction::create(node, Repeat::create(Sequence::create(EaseOut::create(MoveBy::create(duration, Vec2(vec.x/2, vec.y)), ease),
                                                                        EaseIn::create(MoveBy::create(duration, Vec2(-vec.x/2,-vec.y)), ease), NULL), repeat));
}

#pragma mark - swing
TargetedAction* CustomActionManager::swingAction(float angle, float duration, cocos2d::Node *node, float ease)
{
    return swingAction(angle, duration, node, 1.5, false, 1);
}

TargetedAction* CustomActionManager::swingAction(float angle, float duration, cocos2d::Node *node, float ease,bool isPendulum)
{
    return swingAction(angle, duration, node, ease, isPendulum, 1);
}

TargetedAction* CustomActionManager::swingAction(float angle, float duration, cocos2d::Node *node, float ease,bool isPendulum,int repeat)
{
    return swingAction(angle, duration, node, ease, isPendulum, repeat, "");
}

TargetedAction* CustomActionManager::swingAction(float angle, float duration, Node *node, float ease, bool isPendulum, int repeat, std::string soundName)
{
    if (isPendulum) {
        return TargetedAction::create(node, Repeat::create(Sequence::create(EaseOut::create(RotateBy::create(duration, angle), ease),
                                                                            createSoundAction(soundName),
                                                                            EaseInOut::create(RotateBy::create(duration*2,-angle*2), ease),
                                                                            EaseInOut::create(RotateBy::create(duration, angle), ease), NULL), repeat));
    }
    else{
        return TargetedAction::create(node, Repeat::create(Sequence::create(EaseOut::create(RotateBy::create(duration, angle), ease),
                                                                            createSoundAction(soundName),
                                                                            EaseIn::create(RotateBy::create(duration,-angle), ease), NULL), repeat));
    }
}

TargetedAction* CustomActionManager::swingAction(cocos2d::Vec2 vec, float duration, cocos2d::Node *node, float ease, bool isPendulum)
{
    return swingAction(vec, duration, node, ease,isPendulum, 1);
}

TargetedAction* CustomActionManager::swingAction(cocos2d::Vec2 vec, float duration, cocos2d::Node *node, float ease, bool isPendulum,int repeat)
{
    if (isPendulum) {
        return TargetedAction::create(node, Repeat::create(Sequence::create(EaseInOut::create(MoveBy::create(duration, vec), ease),
                                                                            EaseInOut::create(MoveBy::create(duration*2,Vec2(-vec.x*2, -vec.y*2)), ease),
                                                                            EaseInOut::create(MoveBy::create(duration, vec), ease), NULL), repeat));
    }
    else{
        return TargetedAction::create(node, Repeat::create(Sequence::create(EaseInOut::create(MoveBy::create(duration, vec), ease),
                                                                            EaseInOut::create(MoveBy::create(duration,Vec2(-vec.x, -vec.y)), ease),
                                                                            NULL), repeat));
    }
}

TargetedAction* CustomActionManager::swingActionForever(float angle, float duration, cocos2d::Node *node, float ease)
{
    return TargetedAction::create(node, Sequence::create(EaseOut::create(RotateBy::create(duration, angle), ease),
                                                         Repeat::create(Sequence::create(EaseInOut::create(RotateBy::create(duration*2,-angle*2), ease),
                                                                                         EaseInOut::create(RotateBy::create(duration*2, angle*2), ease), NULL), UINT_MAX)
                                                         , NULL));
}

TargetedAction* CustomActionManager::swingActionForever(cocos2d::Vec2 vec, float duration, cocos2d::Node *node, float ease)
{
    return TargetedAction::create(node, Sequence::create(EaseInOut::create(MoveBy::create(duration, vec), ease),
                                                         Repeat::create(Sequence::create(EaseInOut::create(MoveBy::create(duration*2,Vec2(-vec.x*2, -vec.y*2)), ease),
                                                                                         EaseInOut::create(MoveBy::create(duration*2, Vec2(vec.x*2, vec.y*2)), ease), NULL),UINT_MAX),
                                                         NULL));
}

#pragma mark - Skew
TargetedAction* CustomActionManager::skewAction(cocos2d::Node *node, float duration, float angle, int repeat)
{
    auto skew=TargetedAction::create(node,Repeat::create(Sequence::create(EaseOut::create(SkewTo::create(duration, -angle, 0), 1.5),
                                                                          DelayTime::create(duration/2),
                                                                          EaseIn::create(SkewTo::create(duration, angle, 0),1.5),
                                                                          DelayTime::create(duration/2),
                                                                          NULL), repeat));
    
    return skew;
}

#pragma mark - Cloud
FiniteTimeAction* CustomActionManager::cloudAction(cocos2d::Node *parent, std::string name, cocos2d::Vec2 anchor)
{
    return cloudAction(parent, name, anchor, .5);
}

FiniteTimeAction* CustomActionManager::cloudAction(cocos2d::Node *parent, std::string name, cocos2d::Vec2 anchor,float duration)
{
    auto cloud=AnchorSprite::create(anchor, parent->getContentSize(), name);
    cloud->setOpacity(0);
    parent->addChild(cloud);
    auto original_scale=cloud->getScale();
    cloud->setScale(.1);
    
    auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(duration, original_scale),
                                                                                FadeIn::create(duration), NULL),
                                                                  createSoundAction("nyu.mp3"),
                                                                  DelayTime::create(duration),
                                                                  Spawn::create(ScaleTo::create(duration, .1),
                                                                                FadeOut::create(duration), NULL), NULL));
    
    return seq_cloud;
}

FiniteTimeAction* CustomActionManager::cloudBounceAction(Node *parent, std::vector<std::string> bounceCharaNames, Vec2 charaAnchorPoint, std::string cloudFileName, Vec2 cloudAnchorPoint, std::string charaVoiceName)
{
    auto createBounceActionLambda = [this, parent](std::string fileName, Vec2 anchorPoint)->FiniteTimeAction*{
        auto target = parent->getChildByName<Sprite*>(fileName.c_str());
        log("supは%s",fileName.c_str());
        transformToAnchorSprite(parent, target, anchorPoint);
        
        return createBounceAction(target, .3, .1);
    };
    
    Vector<FiniteTimeAction*>actions_spawn;
    
    if (bounceCharaNames.size()>0) {
        for (auto charaFileName : bounceCharaNames) {
            actions_spawn.pushBack(createBounceActionLambda(charaFileName, charaAnchorPoint));
        }
    }
    
    if (charaVoiceName.size()>0) {
        actions_spawn.pushBack(createSoundAction(charaVoiceName));
    }
    
    if (cloudFileName.size()>0) {
        actions_spawn.pushBack(cloudAction(parent, cloudFileName, cloudAnchorPoint));
    }
    
    return Spawn::create(actions_spawn);
}

FiniteTimeAction* CustomActionManager::cloudBounceAction(Node *parent, Node *character, std::string cloudFileName, Vec2 cloudAnchorPoint, std::string charaVoiceName)
{
    Vector<FiniteTimeAction*>actions_spawn;
    
    if (character!=NULL) {
        actions_spawn.pushBack(createBounceAction(character, .3,.1));
    }
    
    if (charaVoiceName.size()>0) {
        actions_spawn.pushBack(createSoundAction(charaVoiceName));
    }
    
    if (cloudFileName.size()>0) {
        actions_spawn.pushBack(cloudAction(parent, cloudFileName, cloudAnchorPoint));
    }
    
    return Spawn::create(actions_spawn);
}

#pragma mark - Sound
CallFunc* CustomActionManager::createSoundAction(std::string name)
{
    return createSoundAction(name, ShowType_Normal);
}

CallFunc* CustomActionManager::createSoundAction(std::string name,ShowType type)
{
    return CallFunc::create([name,type]{
        if (type==ShowType_Normal||
            type==ShowType_Answer_Big||
            type==ShowType_Hint_Big) {
            Common::playSE(name.c_str());
        }
    });
}

FiniteTimeAction* CustomActionManager::useItemSoundAction()
{
    auto pCall = CallFunc::create([](){
        Common::playSE("p.mp3");
    });

    return pCall;
}

FiniteTimeAction* CustomActionManager::correctSoundAction()
{
    auto pinponCall = CallFunc::create([](){
        Common::playSE("pinpon.mp3");
    });
    
    return pinponCall;
}

#pragma mark - フリップ
FiniteTimeAction* CustomActionManager::flipCardAction(Node *parent, std::string frontFileName, std::string backFileName, Vec2 anchorPoint, float flipDuration)
{
    auto backSp = parent->getChildByName<Sprite*>(backFileName.c_str());
    auto frontSp = parent->getChildByName<Sprite*>(frontFileName.c_str());
    
    if (backSp == NULL) {
        backSp = createAnchorSprite(backFileName, anchorPoint.x, anchorPoint.y, false, parent, true);
    }
    
    if (frontSp == NULL) {
        frontSp = createAnchorSprite(frontFileName, anchorPoint.x, anchorPoint.y, false, parent, true);
    }
    
    Vector<FiniteTimeAction*> actions;
    //frontspriteを表示回転・
    auto fadein = FadeIn::create(0);
    auto fadeout = FadeOut::create(0);
    auto camera = OrbitCamera::create(flipDuration/2, 1, 0, 0, 90, 0, 0);
    auto frontSeq = TargetedAction::create(frontSp, Sequence::create(fadein, camera, fadeout, NULL));
    actions.pushBack(frontSeq);
    
    auto camera_ = OrbitCamera::create(flipDuration/2, 1, 0, 270, 90, 0, 0);
    auto backSeq = TargetedAction::create(backSp, Sequence::create(fadein->clone(), camera_, NULL));
    actions.pushBack(backSeq);
    
    return Sequence::create(actions);
}

#pragma mark 旗
Sprite* CustomActionManager::createWaveSprite(Node *parent, std::string fileName, float duration,float delay, Size waveSize, int waveCount, float waveAmplitude, bool horizontal, bool vertical, int repeat)
{
    //parent基準になおす
    auto parent_size=parent->getContentSize();
    auto _amplitude=waveAmplitude*parent_size.width;
    auto _waveSize=Size(ceil(waveSize.width*parent_size.width), ceil(waveSize.height*parent_size.width));
    
    auto node=createSpriteToCenter(fileName, false, parent);
    
    auto waves=Waves::create(duration, _waveSize, waveCount, _amplitude, horizontal, vertical);
    
    auto nodeGrid = NodeGrid::create();
    nodeGrid->setName(fileName);
    nodeGrid->addChild(node);
    nodeGrid->setCascadeOpacityEnabled(true);
    
    nodeGrid->runAction(Sequence::create(DelayTime::create(delay),
                                         Repeat::create(waves, repeat),
                                         NULL));
    
    parent->addChild(nodeGrid);
    
    return node;
}

#pragma mark 水面
Sprite* CustomActionManager::createWave3DSprite(Node *parent, std::string fileName, float duration, Size waveSize, int waveCount, float waveAmplitude, int repeat)
{
    //parent基準になおす
    auto parent_size=parent->getContentSize();
    auto _amplitude=waveAmplitude*parent_size.width;
    auto _waveSize=Size(waveSize.width*parent_size.width, waveSize.height*parent_size.width);
    
    auto water=createSpriteToCenter(fileName, false, parent);
    // create a Waved3D action
    ActionInterval* waves = Waves3D::create(duration, Size(ceilf(_waveSize.width),ceilf(_waveSize.height)), waveCount, _amplitude);
    
    auto nodeGrid = NodeGrid::create();
    nodeGrid->setName(fileName);
    nodeGrid->addChild(water);
    nodeGrid->setCascadeOpacityEnabled(true);

    nodeGrid->runAction(Repeat::create(waves, repeat));
    
    parent->addChild(nodeGrid);
    
    return water;
}

Sprite* CustomActionManager::createRipple3DSprite(Node *parent, std::string fileName, float duration, Size waveSize, Vec2 position, float radius, int waveCount, float amplitude, int repeat)
{//addChildされていない時、正しいポジションが取れない...
    //screen基準になおす
    auto screen_size=Director::getInstance()->getRunningScene()->getContentSize();
    auto worldPos=parent->convertToWorldSpaceAR(Vec2::ZERO);
    auto _pos=Vec2(worldPos.x-parent->getBoundingBox().size.width/2+parent->getBoundingBox().size.width*position.x, worldPos.y-parent->getBoundingBox().size.height/2+parent->getBoundingBox().size.height*position.y);
    auto _radius=parent->getBoundingBox().size.width*radius;
    
    //log("worldpos x %f :y %f",worldPos.x,worldPos.y);
    
    //parent基準に直す
    auto _amplitude=parent->getContentSize().width*amplitude;
    auto _waveSize=Size(ceilf(waveSize.width*parent->getContentSize().width), ceilf(waveSize.height*parent->getContentSize().width));
    
    // スプライトを作成
    Sprite *water = SupplementsManager::getInstance()->addSupplementToCenter(parent, fileName, false, false);
    
    
    // 波紋を広げるアニメーション作成
    Ripple3D* action = Ripple3D::create(duration, _waveSize, _pos, _radius, waveCount, _amplitude);
    
    // NodeGrid作成
    NodeGrid *_nodeGrid = NodeGrid::create();
    _nodeGrid->setCascadeOpacityEnabled(true);
    _nodeGrid->setName(fileName);
    _nodeGrid->addChild(water);
    _nodeGrid->runAction(Repeat::create(action, -1));
    
    parent->addChild(_nodeGrid);
    
    return water;
}

#pragma mark - 汎用
FiniteTimeAction* CustomActionManager::keyAction(Node *parent,int backID)
{
    return keyAction(parent, backID,true);
}

FiniteTimeAction* CustomActionManager::keyAction(Node *parent,int backID,bool sound)
{
    if (sound) {
        Common::playSE("p.mp3");
    }
    
    Vector<FiniteTimeAction*>actions;
    auto count=2;
    for (int i=0; i<count; i++) {
        auto key=createSpriteToCenter(StringUtils::format("sup_%d_key_%d.png",backID,i), true, parent,true);
        if (key) {
            if (i>0) {
                auto key_before=parent->getChildByName(StringUtils::format("sup_%d_key_%d.png",backID,i-1));
                actions.pushBack(createChangeAction(1, .5, key_before, key));
                actions.pushBack(createSoundAction("key_0.mp3"));
            }
            else{
                actions.pushBack(TargetedAction::create(key, FadeIn::create(1)));
                actions.pushBack(createSoundAction("kacha.mp3"));
            }
        }
        actions.pushBack(DelayTime::create(1));
    }
    
    return Sequence::create(actions);
}

FiniteTimeAction* CustomActionManager::openDoorAction(cocos2d::Node *parent, std::string backname, int backID, std::string doorname, std::string supName, std::string soundName)
{
    return openDoorAction(parent, backname, backID, doorname, supName, soundName, false);
}

FiniteTimeAction* CustomActionManager::openDoorAction(cocos2d::Node *parent, std::string backname, int backID, std::string doorname, std::string supName, std::string soundName,bool removeDoor)
{
    Vector<FiniteTimeAction*>actions;
    
    
    Node* door_parent;
    if (backname.size()>0) {
        auto back=createSpriteToCenter(backname, true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, backID, true);
        
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        actions.pushBack(fadein_back);
        
        door_parent=back;
    }
    else{
        door_parent=parent;
    }
    
    auto delay=DelayTime::create(1);
    actions.pushBack(delay);
    
    if (!removeDoor) {
        auto door=createSpriteToCenter(doorname, true,door_parent ,true);
        if (supName.size()>0) {
            createSpriteToCenter(supName, false,door, true);
        }
        auto fadein_door=TargetedAction::create(door, Spawn::create(FadeIn::create(.5),
                                                                    createSoundAction(soundName), NULL));
        actions.pushBack(fadein_door);
    }
    else{
        Vector<FiniteTimeAction*>spawns;
        auto door=door_parent->getChildByName(doorname);
        auto fadeout_door=TargetedAction::create(door, Spawn::create(FadeOut::create(.5),
                                                                     createSoundAction(soundName), NULL));
        spawns.pushBack(fadeout_door);
        if (supName.size()>0) {
            spawns.pushBack(TargetedAction::create(createSpriteToCenter(supName, true,parent, true),FadeIn::create(.5)));
        }
        
        actions.pushBack(Spawn::create(spawns));
    }
    
    
    
    actions.pushBack(delay->clone());
    
    return Sequence::create(actions);
}

FiniteTimeAction* CustomActionManager::putAction(cocos2d::Node *node, std::string soundName, float duration_fadein,float delay)
{
    auto put=TargetedAction::create(node, Sequence::create(FadeIn::create(duration_fadein),
                                                           
                                                             createSoundAction(soundName),
                                                           DelayTime::create(delay),
                                                           NULL));
    return put;
}

Sequence* CustomActionManager::putAction(Node*parent,std::string fileName, std::string soundName,float duration, const onFinished &callback)
{
    Common::playSE(soundName.c_str());
    
    auto spr=createSpriteToCenter(fileName, true, parent,true);
    auto fadein=TargetedAction::create(spr, FadeIn::create(duration));
    auto delay=DelayTime::create(1);
    auto call=CallFunc::create([callback]{
        if (callback) {
            Common::playSE("pinpon.mp3");
            callback(true);
        }
    });
    
    return Sequence::create(fadein,delay,call, NULL);
}

TargetedAction* CustomActionManager::giveAction(cocos2d::Node *node, float duration, float scale,bool fadeout)
{
    return giveAction(node, duration, scale, fadeout,"");
}

TargetedAction* CustomActionManager::giveAction(Node *node, float duration, float scale, bool fadeout, std::string soundName)
{
    Vector<FiniteTimeAction*>actions;
    if (soundName.size()>0) {
        actions.pushBack(createSoundAction(soundName));
    }
    
    actions.pushBack(FadeIn::create(duration));
    actions.pushBack(ScaleBy::create(duration, scale));
    if (fadeout) {
        actions.pushBack(FadeOut::create(duration));
    }
    return TargetedAction::create(node, Sequence::create(actions));
}

FiniteTimeAction* CustomActionManager::changeAnchorAction(Node*parent,cocos2d::Node *node, cocos2d::Vec2 vec)
{
    auto call=CallFunc::create([node,parent,vec]{
        auto default_anchor=node->getAnchorPoint();
        node->setAnchorPoint(vec);
        node->setPosition(node->getPosition().x+(vec.x-default_anchor.x)*parent->getContentSize().width,
                          node->getPosition().y+(vec.y-default_anchor.y)*parent->getContentSize().height);
    });
    
    return call;
}

FiniteTimeAction* CustomActionManager::balloonAction(Node *parent, std::string balloonFileName, Vec2 balloonAnchorPoint, float duration)
{
    Vector<FiniteTimeAction*>actions;
    auto balloon=createAnchorSprite(balloonFileName.c_str(), balloonAnchorPoint.x, balloonAnchorPoint.y, true, parent, true);
    auto scale=balloon->getScale();
    balloon->setScale(scale*.01);
    balloon->setLocalZOrder(1);
    actions.pushBack(DelayTime::create(1));
    
    actions.pushBack(TargetedAction::create(balloon, Repeat::create(Sequence::create(createSoundAction("snoring.mp3"),
                                                                                     EaseInOut::create(Spawn::create(ScaleTo::create(duration, scale),
                                                                                                                     FadeTo::create(duration, 200), NULL),1.5),
                                                                                     EaseInOut::create(ScaleTo::create(duration, scale*.3),1.5),
                                                                                     NULL), -1)));
    return TargetedAction::create(balloon, Sequence::create(actions));
}



FiniteTimeAction* CustomActionManager::attentionAction(Node *parent, std::string charaName, Vec2 charaAnchorPoint, std::string attentionFileName)
{
    return attentionAction(parent, charaName, charaAnchorPoint, attentionFileName, .2, .1);
}

FiniteTimeAction* CustomActionManager::attentionAction(Node *parent, std::string charaName, Vec2 charaAnchorPoint, std::string attentionFileName, float duration, float defrection)
{
    Vector<FiniteTimeAction*> spawnActions;
    auto chara = parent->getChildByName(charaName);
    
    log("キャラネーム%s",charaName.c_str());
    chara->setAnchorPoint(charaAnchorPoint);
    chara->setPosition(charaAnchorPoint.x*parent->getContentSize().width, charaAnchorPoint.y*parent->getContentSize().height);
    
    //キャラをバウンス
    auto chara_bounce = createBounceAction(chara, duration, defrection);
    spawnActions.pushBack(chara_bounce);
    
    //驚き音
    spawnActions.pushBack(createSoundAction("surprise.mp3"));
    
    //びっくりマーク
    auto attention=createSpriteToCenter(attentionFileName, true, parent, true);
    auto attentionJump = TargetedAction::create(attention,
                                                Spawn::create(FadeIn::create(.1),
                                                              jumpAction(parent->getContentSize().height*.1, .2, attention, 1.5), NULL));
    spawnActions.pushBack(attentionJump);
    auto spawn = Spawn::create(spawnActions);
    
    Vector<FiniteTimeAction*> sequenceActions;
    sequenceActions.pushBack(spawn);
    
    //びっくりマークを消す。
    sequenceActions.pushBack(TargetedAction::create(attention, FadeOut::create(.5)));
    
    return Sequence::create(sequenceActions);
}

FiniteTimeAction* CustomActionManager::driverAction(int backNum, std::vector<Vec2> positions, Node* parent)
{
    return driverAction(backNum, positions, parent, 0);
}

FiniteTimeAction* CustomActionManager::driverAction(int backNum, std::vector<Vec2> positions, Node *parent, int zOrder)
{
    Vector<FiniteTimeAction*>actions;
    
    //初回位置配置&FadeIn
    auto vec = positions.at(0);
    auto item = createAnchorSprite(StringUtils::format("sup_%d_driver.png", backNum), .5, .5, true, parent, true);
    item->setLocalZOrder(zOrder);
    auto fadein_item = TargetedAction::create(item, FadeIn::create(1));
    actions.pushBack(fadein_item);
    
    //ネジとる
    for (int i=0; i<positions.size(); i++) {
        
        //移動
        if (i == 0) {//初回位置
            item->setPosition(parent->getContentSize().width*vec.x, parent->getContentSize().height*vec.y);
        }
        else {
            auto vec = positions.at(i);
            auto move=TargetedAction::create(item, Spawn::create(MoveTo::create(.7, Vec2(parent->getContentSize().width*vec.x, parent->getContentSize().height*vec.y)), NULL));
            actions.pushBack(move);
        }
        actions.pushBack(DelayTime::create(.3));
        
        
        //シェイク
        auto shake=Spawn::create(jumpAction(parent->getContentSize().height*.005, .1, item, 1.5, 3),
                                 createSoundAction("kacha_2.mp3"), NULL);
        actions.pushBack(shake);
        
        //ネジ排除
        auto remove_screw=TargetedAction::create(parent->getChildByName(StringUtils::format("sup_%d_screw_%d.png", backNum, i)),
                                                 Sequence::create(FadeOut::create(.3), RemoveSelf::create(), NULL));
        actions.pushBack(remove_screw);
        actions.pushBack(DelayTime::create(1));
        
    }
    
    //ドライバーをフェードアウト
    auto fadeout=TargetedAction::create(item, Spawn::create(MoveBy::create(1, Vec2(0, -parent->getContentSize().height*.1)), FadeOut::create(1), NULL));
    actions.pushBack(fadeout);
    actions.pushBack(DelayTime::create(.1));
    
    
    return Sequence::create(actions);
}

FiniteTimeAction* CustomActionManager::setItemAction(Node *parent, Sprite* itemSprite)
{
    Vector<FiniteTimeAction*> actions;
    
    //アイテム配置音
    actions.pushBack(createSoundAction("p.mp3"));
    
    //fadein
    actions.pushBack(TargetedAction::create(itemSprite, FadeIn::create(1)));
    actions.pushBack(DelayTime::create(1));
    
    return Sequence::create(actions);
}

void CustomActionManager::runSetItemAction(Node *parent, Sprite* itemSprite, const std::function<void (bool)> &callback)
{
    Vector<FiniteTimeAction*> actions;

    //アイテムをセット
    actions.pushBack(setItemAction(parent, itemSprite));
   
    //アイテム設置完了コールバック
    auto call=CallFunc::create([callback]{
        Common::playSE("pinpon.mp3");
        callback(true);
    });
    actions.pushBack(call);
    
    parent->runAction(Sequence::create(actions));
}

Spawn* CustomActionManager::getActionWithTargets(Vector<Sprite *> targets, FiniteTimeAction* action)
{
    Vector<FiniteTimeAction*> spawnActions;
    
    for (auto targetNode : targets) {
        spawnActions.pushBack(TargetedAction::create(targetNode, action->clone()));
    }
    
    return Spawn::create(spawnActions);
}

FiniteTimeAction* CustomActionManager::getActionsWithSound(FiniteTimeAction *action, std::string soundName)
{
    Vector<FiniteTimeAction*> spawnActions;
    
    spawnActions.pushBack(action);
    spawnActions.pushBack(createSoundAction(soundName));
    
    return Spawn::create(spawnActions);
}

FiniteTimeAction* CustomActionManager::matchAction(Node *parent, int backID, ParticleSystemQuad *fireParticle)
{
    auto match=createSpriteToCenter(StringUtils::format("sup_%d_match.png", backID), true, parent, true);
    auto seq_match=TargetedAction::create(match, Sequence::create(FadeIn::create(1),
                                                                  Repeat::create(swingAction(Vec2(0, parent->getContentSize().height*.02), .1, match, 1.51, false), 2),
                                                                  DelayTime::create(.3),
                                                                  CallFunc::create([this, fireParticle]{
        Common::playSE("huo.mp3");
        fireParticle->setTotalParticles(150);
    }),
                                                                  FadeOut::create(1),
                                                                  NULL));
    return seq_match;
}

FiniteTimeAction* CustomActionManager::bezierActions(Node* parent, std::vector<std::vector<Vec2>> posVecs, float duration)
{
    Vector<FiniteTimeAction*> bezierActions;
    
    float eachDuration = duration/posVecs.size();
    for (std::vector<Vec2> bezierVecs : posVecs) {
        ccBezierConfig config;
        if (!(bezierVecs.at(0).x == 0 && bezierVecs.at(0).y == 0)) {
            config.controlPoint_1 = Vec2(parent->getContentSize().width*bezierVecs.at(0).x, parent->getContentSize().height*bezierVecs.at(0).y);
        }
        
        if (!(bezierVecs.at(1).x == 0 && bezierVecs.at(1).y == 0)) {
            config.controlPoint_2 = Vec2(parent->getContentSize().width*bezierVecs.at(1).x, parent->getContentSize().height*bezierVecs.at(1).y);
        }

        config.endPosition = Vec2(parent->getContentSize().width*bezierVecs.at(2).x, parent->getContentSize().height*bezierVecs.at(2).y);
        
        bezierActions.pushBack(BezierTo::create(eachDuration, config));
    }
    
    auto bezierSec = Sequence::create(bezierActions);
    return bezierSec;
}

#pragma mark - 遅延実行
#pragma mark delay
void CustomActionManager::delayAndCallBack(Node*parent,float duration, bool touchEnable, const std::function<void ()> callback)
{
    auto layer=SupplementsManager::getInstance()->createDisableLayer();
    if (!touchEnable) {
        Director::getInstance()->getRunningScene()->addChild(layer);
    }
    
    auto wait=DelayTime::create(duration);
    auto call=CallFunc::create([callback,layer,touchEnable](){
        if (!touchEnable) {
            layer->removeFromParent();
        }
        
        if (callback) {
            callback();
        }
    });
    
    parent->runAction(Sequence::create(wait,call, NULL));
}
