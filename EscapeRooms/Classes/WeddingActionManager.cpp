//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "WeddingActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "PatternSprite.h"

using namespace cocos2d;

WeddingActionManager* WeddingActionManager::manager =NULL;

WeddingActionManager* WeddingActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new WeddingActionManager();
    }
    return manager;
}

#pragma mark - Back
void WeddingActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="light")
    {
        Vector<FiniteTimeAction*>actions;
        auto duration=.2;
        for (int i=0;i<4;i++) {
            auto light=createSpriteToCenter(StringUtils::format("sup_%d_light_%d.png",backID,i), true, parent);
            parent->addChild(light);
            
            auto flash=TargetedAction::create(light,Sequence::create(EaseOut::create(FadeIn::create(duration), 1.5) ,EaseIn::create(FadeOut::create(duration), 1.5), NULL));
            
            actions.pushBack(flash);
        }
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),actions.at(0),actions.at(2),actions.at(0)->clone(),actions.at(1),actions.at(3),actions.at(2)->clone(), NULL),UINT_MAX));
    }
    else if(key=="candle"&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(18)&&
            !DataManager::sharedManager()->isPlayingMinigame()){
        candleAction(parent);
    }
    else if(key=="balloon"&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(13)){
        std::vector<float>heights={.05,.025,.075,.1};
        std::vector<Vec2>anchors={Vec2(.383,.31),Vec2(.46,.31),Vec2(.533,.31),Vec2(.6,.31)};

        std::vector<Vec2>scales={Vec2(.9,1.4),Vec2(.95,1.2),Vec2(.85,1.6),Vec2(.8,1.8)};
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<4; i++) {
            auto rope=AnchorSprite::create(anchors.at(i), parent->getContentSize(), StringUtils::format("sup_%d_rope_%d.png",backID,i));
            parent->addChild(rope);
            auto scale_original=rope->getScale();
            auto heart=createSpriteToCenter(StringUtils::format("sup_%d_heart_%d.png",backID,i), false, parent);
            parent->addChild(heart);
            
            auto heart_action=swingAction(Vec2(0, parent->getContentSize().height*heights.at(i)), 4, heart, 2, false);
            auto rope_action=TargetedAction::create(rope, Sequence::create(EaseInOut::create(ScaleBy::create(4, scales.at(i).x, scales.at(i).y), 2),
                                                                           EaseInOut::create(ScaleTo::create(4, scale_original), 2), NULL));
            actions.pushBack(Spawn::create(heart_action,rope_action, NULL));
        }
        
        parent->runAction(Repeat::create(Spawn::create(actions), UINT_MAX));
    }
    else if(key=="clear"&&!DataManager::sharedManager()->isPlayingMinigame()){
        auto story=StoryLayer::create("ending1", [](Ref*ref){
            //Common::playBGM(DataManager::sharedManager()->getBgmName(true).c_str());
            
            auto story_1=StoryLayer::create("ending2", [](Ref*ref){
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story_1);
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
}

#pragma mark - Touch
void WeddingActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="eraser")
    {//黒板消し
        Common::playSE("p.mp3");
        
        auto stain=parent->getChildByName<Sprite*>("sup_33_stain.png");
        auto mark=createSpriteToCenter("sup_33_mark.png", true, parent);
        parent->addChild(mark);
        
        auto item=AnchorSprite::create(Vec2(.51, .4), parent->getContentSize(),"sup_33_eraser.png");
        item->setOpacity(0);
        parent->addChild(item);
        
        //fadein
        auto fadein=FadeIn::create(1);
        auto sound=CallFunc::create([this]{
            Common::playSE("goshigoshi.mp3");
        });
        //move
        auto distance=parent->getContentSize().width*.05;
        auto duration=.2;
        Vector<FiniteTimeAction*>moves;
        for (int i=1; i<6; i++) {
            auto move=MoveBy::create(duration+i*.025, Vec2(distance*i*pow(-1, i)+distance*.8*(i/2), distance*.75*i*pow(-1, i)-distance*.25*(i/2)));
            moves.pushBack(move);
        }
        
        auto seq=TargetedAction::create(item, Sequence::create(fadein,sound,Sequence::create(moves), NULL));
        auto change=Spawn::create(TargetedAction::create(item, FadeOut::create(.7)),
                                  createChangeAction(1, .7, stain, mark), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq,change,DelayTime::create(.5),call,NULL));
    }
    else if(key.compare(0,6,"monkey")==0){
        Common::playSE("handbell.mp3");
        
        auto index=atoi(key.substr(key.length()-1,1).c_str());
        AnswerManager::getInstance()->appendPasscode(StringUtils::format("%d",index));
        
        auto particle=ParticleSystemQuad::create("particle_music.plist");
        particle->setAutoRemoveOnFinish(true);
        particle->setPosition(parent->getContentSize().width*(.4+.2*index),parent->getContentSize().height*.35);
        particle->setStartSize(parent->getContentSize().width*.15);
        particle->setEndSize(particle->getStartSize());
        particle->setSpeed(parent->getContentSize().width*.3);
        particle->setAngle(150-120*index);
        particle->setLife(2);
        particle->setDuration(1);
        particle->setTotalParticles(7);
        particle->setStartSpin(-60*pow(-1, index));
        particle->setGravity(Vec2(parent->getContentSize().width*.15, parent->getContentSize().height*.02));
        if (index==1) {
            particle->setStartSpin(-60);

            particle->setTexture(Sprite::create("particle_music_1.png")->getTexture());
            particle->setStartColor(Color4F(255, 0, 0, 255));
            particle->setEndColor(Color4F(255, 0, 0, 0));
            particle->setGravity(Vec2(-parent->getContentSize().width*.15, parent->getContentSize().height*.02));
        }
        
        parent->addChild(particle);
        
        std::string armName="sup_24_bluearm.png";
        if (index==1) {
            armName="sup_24_redarm.png";
        }
        
        Vector<FiniteTimeAction*>actions;
        
        auto arm=parent->getChildByName<Sprite*>(armName);
        auto swing=swingAction(45*pow(-1, index+1), .2, arm, 1.5);
        actions.pushBack(swing);
        auto delay=DelayTime::create(.7);
        actions.pushBack(delay);
        
        auto correct=false;
        auto passcode=AnswerManager::getInstance()->getPasscode();
        log("入力中%s",passcode.c_str());
        if(passcode=="101101"&&
           !DataManager::sharedManager()->getEnableFlagWithFlagID(34)){
            correct=true;
            actions.pushBack(delay->clone());
        }
        else{
            if (passcode.size()==6) {
                AnswerManager::getInstance()->resetPasscode();
            }
        }
        
        auto call=CallFunc::create([callback,correct]{
            if (correct) {
                Common::playSE("pinpon.mp3");
            }
            callback(correct);
        });
        actions.pushBack(call);

        parent->runAction(Sequence::create(actions));
    }
    else if(key=="opencage"){
        //南京正解
        auto back=createSpriteToCenter("back_47.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        SupplementsManager::getInstance()->addSupplementToMain(back, 47);
        
        auto close=back->getChildByName<Sprite*>("sup_47_close.png");
        auto open=createSpriteToCenter("sup_47_open.png", true, parent);
        back->addChild(open);
        
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        auto change=Spawn::create(createChangeAction(.7, .7, close, open),
                                  createSoundAction("kacha.mp3"), NULL);
        auto delay=DelayTime::create(1);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,change,delay->clone(),call, NULL));
    }
    else if(key=="lens"){
        //レンズセット
        auto lens=createSpriteToCenter("sup_49_lens.png", true, parent);
        parent->addChild(lens);
        
        auto back=createSpriteToCenter("back_7.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        SupplementsManager::getInstance()->addSupplementToMain(back, 7);
        
        auto doll_0=back->getChildByName<Sprite*>("sup_7_doll_0.png");
        auto items=back->getChildByName<Sprite*>("sup_7_items.png");

        auto doll_1=createSpriteToCenter("sup_7_doll_1.png", true, parent);
        back->addChild(doll_1);
        auto lens_1=createSpriteToCenter("sup_7_lens.png", false, parent);
        back->addChild(lens_1);
        
        auto doll_2=createSpriteToCenter("sup_7_doll_2.png", true, parent);
        back->addChild(doll_2);
        auto doll_3=createSpriteToCenter("sup_7_doll_3.png", true, parent);
        back->addChild(doll_3);
        
        auto flash=createSpriteToCenter("sup_7_flash.png", true, parent);
        back->addChild(flash);
        
        auto fadein_lens=TargetedAction::create(lens, FadeIn::create(.7));
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        auto change=Spawn::create(createChangeAction(1, .7, doll_0, doll_1),
                                  createSoundAction("timer.mp3"), NULL);
        auto change_1=Spawn::create(createChangeAction(1, .7, doll_1, doll_2),
                                    TargetedAction::create(items, FadeOut::create(1)),
                                  createSoundAction("pop.mp3"), NULL);
        auto change_2=Spawn::create(createChangeAction(1, .7, doll_2, doll_3),
                                    createSoundAction("pop.mp3"), NULL);
        auto delay=DelayTime::create(.9);
        auto flash_action=Spawn::create(TargetedAction::create(flash, Sequence::create(FadeIn::create(.1),DelayTime::create(.1),FadeOut::create(.1), NULL)),
                                        createSoundAction("camera.mp3"), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_lens,delay,fadein,delay->clone(),change,delay->clone(),change_1,delay->clone(),change_2,delay->clone(),flash_action,delay->clone(),call, NULL));
    }
    else if(key=="pan"){
        auto pan=createSpriteToCenter("sup_47_pan.png", true, parent);
        parent->addChild(pan);
        auto bird=createSpriteToCenter("sup_47_bird.png", true, parent);
        parent->addChild(bird);
        
        auto fadein_pan=TargetedAction::create(pan, Spawn::create(FadeIn::create(1),
                                                                  createSoundAction("koto.mp3"), NULL));
        auto delay=DelayTime::create(1);
        auto fadein_bird=TargetedAction::create(bird,Spawn::create(FadeIn::create(1),
                                                                   createSoundAction("piyo.mp3"), NULL));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_pan,delay,fadein_bird,delay->clone(),call, NULL));
    }
    else if(key=="pushbutton"){
        auto on=createSpriteToCenter("sup_52_on.png", true, parent);
        parent->addChild(on);
        auto back=createSpriteToCenter("back_50.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        auto pig=createSpriteToCenter("sup_50_pig_0.png", false, parent);
        pig->setPositionY(parent->getContentSize().height);
        back->addChild(pig);
        
        auto pig_1=createSpriteToCenter("sup_50_pig_1.png", true, parent);
        back->addChild(pig_1);
        auto coin=createSpriteToCenter("sup_50_coin.png", true, parent);
        back->addChild(coin);
        
        SupplementsManager::getInstance()->addSupplementToMain(back, 50);
        
        auto fadein_on=TargetedAction::create(on, Spawn::create(FadeIn::create(1),
                                                                  createSoundAction("kacha.mp3"), NULL));
        auto delay=DelayTime::create(1);
        auto fadein_back=TargetedAction::create(back,Spawn::create(FadeIn::create(1), NULL));
        auto move_pig=TargetedAction::create(pig, Spawn::create(MoveTo::create(4, parent->getContentSize()/2),
                                                                createSoundAction("weeen.mp3"), NULL));
        auto change_pig=Spawn::create(createChangeAction(.7, .7, pig, pig_1),
                                      TargetedAction::create(coin, FadeIn::create(.7)),
                                      createSoundAction("chalin.mp3"),
                                      createSoundAction("pig.mp3"), NULL);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_on,delay,fadein_back,delay->clone(),move_pig,delay->clone(),change_pig,delay->clone(),call, NULL));
    }
    else if(key.compare(0,4,"coin")==0){
        Common::playSE("p.mp3");
        
        auto index=atoi(key.substr(5,1).c_str());
        
        std::string itemName="sup_26_pin.png";
        if (index==1) {
            itemName="sup_26_eraser.png";
        }
        
        auto coin=createSpriteToCenter("sup_26_coin.png", true, parent);
        parent->addChild(coin);

        auto item=createSpriteToCenter(itemName, false, parent);
        item->setPositionY(parent->getContentSize().height*.65);
        parent->addChild(item);
        
        auto seq_coin=TargetedAction::create(coin, Sequence::create(FadeIn::create(.7),
                                                                    DelayTime::create(.5),
                                                                    Common::createSoundAction("chalin.mp3"),
                                                                    FadeOut::create(.7),NULL));
        
        auto seq_item=TargetedAction::create(item, Sequence::create(Common::createSoundAction("koto.mp3"),
                                                                  EaseIn::create(MoveTo::create(.2, parent->getContentSize()/2), 2) ,
                                                                  JumpTo::create(.2, parent->getContentSize()/2, parent->getContentSize().height*.01, 2),
                                                                  DelayTime::create(.5),
                                                                  FadeOut::create(.7),NULL));
        
        auto call=CallFunc::create([callback](){
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_coin,DelayTime::create(1),seq_item,call, NULL));
    }
    else if(key=="baloon"){
        Common::playSE("p.mp3");
        
        auto baloon=parent->getChildByName<Sprite*>("sup_10_balloon.png");
        auto hearts=createSpriteToCenter("sup_10_hearts.png", true, parent);
        parent->addChild(hearts);
        auto hearts_1=createSpriteToCenter("sup_10_hearts_1.png", true, parent);
        parent->addChild(hearts_1);
        
        auto pin=createSpriteToCenter("sup_10_pin.png", true, parent);
        parent->addChild(pin);
        
        auto fadein=TargetedAction::create(pin, FadeIn::create(.7));
        auto scale=TargetedAction::create(pin, ScaleBy::create(1, .8));
        auto spawn=Spawn::create(createSoundAction("pan.mp3"),
                                 createSoundAction("pop.mp3"),
                                 createChangeAction(.5, .2, baloon, hearts),
                                 TargetedAction::create(pin, FadeOut::create(.5)), NULL);
        auto delay=DelayTime::create(1);
        auto change=Spawn::create(createChangeAction(1, .7, hearts, hearts_1),
                                   createSoundAction("pop.mp3"), NULL);
        auto fadeout=TargetedAction::create(hearts_1, FadeOut::create(1));
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,scale,spawn,delay,change,delay->clone(),fadeout,call, NULL));
    }
    else if(key=="female")
    {//
        Common::playSE("pig_1.mp3");
        auto num=DataManager::sharedManager()->getNowBack();
        auto cloud_anchor=Vec2(.55,.7);
        
        auto cloud=AnchorSprite::create(cloud_anchor, parent->getContentSize(), StringUtils::format("sup_%d_cloud.png",num));
        cloud->setOpacity(0);
        parent->addChild(cloud);
        auto original_scale=cloud->getScale();
        cloud->setScale(.1);
        
        auto seq_pig=createBounceAction(parent->getChildByName("sup_57_pig.png"), .3);
        
        auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(.5, original_scale),
                                                                                    FadeIn::create(.5), NULL),
                                                                      createSoundAction("nyu.mp3"),
                                                                      DelayTime::create(.5),
                                                                      Spawn::create(ScaleTo::create(.5, .1),
                                                                                    FadeOut::create(.5), NULL), NULL));
        
        
        auto spawn=Spawn::create(seq_cloud,seq_pig, NULL);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,call, NULL));
    }
    else if(key=="match"){
        Common::playSE("p.mp3");
        
        auto match=createSpriteToCenter("sup_15_match.png", true, parent);
        match->setPositionX(parent->getContentSize().width*.4);
        parent->addChild(match);
        
        //animation
        auto seq_match=TargetedAction::create(match, Sequence::create(FadeIn::create(.5),
                                                                      MoveTo::create(1, parent->getContentSize()/2),
                                                                      DelayTime::create(.5),
                                                                      Repeat::create(jumpAction(parent->getContentSize().height*.02, .1, match, 1.5), 2),
                                                                      createSoundAction("huo.mp3"),NULL));
        
        //fire
        auto fire=CallFunc::create([this,parent]{
            auto particle=SupplementsManager::getInstance()->getParticle(parent, DataManager::sharedManager()->getParticleVector(false, 15).at(0).asValueMap());
            
            parent->addChild(particle);
        });
        
        auto fadeout=TargetedAction::create(match, FadeOut::create(.5));
        
        auto call=CallFunc::create([callback,this,parent]{
            Common::playSE("pinpon.mp3");
            this->candleAction(parent);
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_match,fire,fadeout,DelayTime::create(1),call,NULL));
    }
    else if(key=="hammer"){
        Common::playSE("p.mp3");
        auto hammer=AnchorSprite::create(Vec2(.646, .52), parent->getContentSize(), "sup_19_hammer.png");
        hammer->setOpacity(0);
        hammer->setLocalZOrder(1);
        parent->addChild(hammer);
        
        auto lid=parent->getChildByName<Sprite*>("sup_19_lid.png");
        
        auto open=createSpriteToCenter("sup_19_lid_open.png", true, parent);
        parent->addChild(open);
        
        auto bottle=createSpriteToCenter("sup_19_bottle.png", true, parent);
        parent->addChild(bottle);
        
        auto openAction=Spawn::create(createSoundAction("paki.mp3"),
                                      createChangeAction(.2, .2, lid, open), NULL);
        
        auto seq_hammer=TargetedAction::create(hammer, Sequence::create(FadeIn::create(1),
                                                                        DelayTime::create(.5),
                                                                        MoveBy::create(.5, Vec2(-parent->getContentSize().width*.025, parent->getContentSize().height*.02)),
                                                                        RotateBy::create(.3, 10),
                                                                        DelayTime::create(1),
                                                                        Spawn::create(EaseIn::create(RotateBy::create(.2, -45), 1.3),
                                                                                      ScaleBy::create(.2, .8), NULL),
                                                                        openAction,
                                                                        Spawn::create(EaseOut::create(RotateBy::create(.2, 45), 1.3),
                                                                                      ScaleBy::create(.2, 1.0/.8), NULL),
                                                                        DelayTime::create(.5), NULL));
        
        auto fadeouts=Spawn::create(TargetedAction::create(open, FadeOut::create(.7)),
                                    TargetedAction::create(hammer, FadeOut::create(.7)),
                                    TargetedAction::create(bottle, FadeIn::create(.3))
                                    , NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(seq_hammer,fadeouts,DelayTime::create(.7),call, NULL));
    }
    else if(key=="spoon"){
        Common::playSE("p.mp3");
        
        auto pig=parent->getChildByName<Sprite*>("sup_57_pig.png");
        auto spoon=createSpriteToCenter("sup_57_spoon.png", true, parent);
        parent->addChild(spoon);
        auto pig_spoon=createSpriteToCenter("sup_57_pig_1.png", true, parent);
        parent->addChild(pig_spoon);
        auto pig_run=createSpriteToCenter("sup_57_pig_2.png", true, parent);
        parent->addChild(pig_run);
        
        auto back=createSpriteToCenter("back_21.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        SupplementsManager::getInstance()->addSupplementToMain(back, 21,true);
        
        auto male=back->getChildByName<Sprite*>("sup_21_pig_0.png");
        
        auto female_sit=createSpriteToCenter("sup_21_female_0.png", true, back);
        back->addChild(female_sit);
        auto female_scoop=createSpriteToCenter("sup_21_female_1.png", true, back);
        back->addChild(female_scoop);
        auto female_give=createSpriteToCenter("sup_21_female_2.png", true, back);
        back->addChild(female_give);
        auto female_heart=createSpriteToCenter("sup_21_female_3.png", true, back);
        back->addChild(female_heart);
        auto shortcake=createSpriteToCenter("sup_21_shortcake.png", true, back);
        back->addChild(shortcake);
        
        auto male_heart=createSpriteToCenter("sup_21_pig_1.png", true, back);
        back->addChild(male_heart);
        
        Vector<FiniteTimeAction*>actions;
        
        //give spooon
        auto give_spoon=TargetedAction::create(spoon, Sequence::create(FadeIn::create(.7),ScaleBy::create(1.5, .7),DelayTime::create(.7), NULL));
        actions.pushBack(give_spoon);
        
        //take spoon
        auto take_spoon=Spawn::create(createChangeAction(1, .7, pig, pig_spoon),
                                      TargetedAction::create(spoon, FadeOut::create(1)),
                                      createSoundAction("pig_1.mp3"), NULL);
        actions.pushBack(take_spoon);
        actions.pushBack(DelayTime::create(1));
        
        //run_pig
        auto run_pig=Sequence::create(createSoundAction("run.mp3"),
                                      createChangeAction(1, .7, pig_spoon, pig_run),
                                      DelayTime::create(1),
                                      TargetedAction::create(pig_run, FadeOut::create(1)),
                                      DelayTime::create(1), NULL);
        actions.pushBack(run_pig);
        
        //scene change
        auto fadein_back=TargetedAction::create(back, FadeIn::create(2));
        actions.pushBack(fadein_back);
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        //移動直後
        auto fadein_sit=Spawn::create(TargetedAction::create(female_sit, FadeIn::create(.5)),
                                      createSoundAction("pig_1.mp3"), NULL);
        actions.pushBack(fadein_sit);
        actions.pushBack(delay->clone());
        
        //ケーキをすくう
        Vector<FiniteTimeAction*>cakeActions;
        for (int i=3; i>=0; i--) {
            auto fadeout_cake=TargetedAction::create(back->getChildByName<Sprite*>(StringUtils::format("sup_21_%d.png",i)), FadeOut::create(.7));
            auto change_pig=Spawn::create(TargetedAction::create(female_sit, FadeOut::create(1)),
                                          TargetedAction::create(female_scoop, FadeIn::create(.7)),
                                          TargetedAction::create(female_give, FadeOut::create(1)), NULL);
            
            cakeActions.pushBack(Spawn::create(createSoundAction("po.mp3"),fadeout_cake,change_pig, NULL));
        }
        
        //ケーキを食わす
        auto give=Spawn::create(createChangeAction(1, .7, female_scoop, female_give),
                                TargetedAction::create(shortcake, FadeIn::create(.7)), NULL);
        auto move_cake=TargetedAction::create(shortcake, MoveBy::create(.5, Vec2(parent->getContentSize().width*.1, 0)));
        auto eat=Spawn::create(createBounceAction(male, .3),
                               TargetedAction::create(shortcake, Sequence::create(FadeOut::create(.7),MoveTo::create(.3, parent->getContentSize()/2), NULL)),
                               createSoundAction("paku.mp3"), NULL);
        auto seq_eat=Sequence::create(give,move_cake,eat, NULL);
        
        for (int i=0; i<4; i++) {
            actions.pushBack(cakeActions.at(i));
            actions.pushBack(seq_eat);
        }
        
        actions.pushBack(delay->clone());
        
        //わらう
        auto smile=Spawn::create(TargetedAction::create(female_give, FadeOut::create(.7)),
                                 TargetedAction::create(female_heart, FadeIn::create(.7)),
                                 TargetedAction::create(male_heart, FadeIn::create(.7)),
                                 createSoundAction("pig.mp3"),
                                 createSoundAction("pig_1.mp3"), NULL);
        actions.pushBack(smile);
        actions.pushBack(delay->clone());
        
        actions.pushBack(TargetedAction::create(back, FadeOut::create(2)));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="champagne")
    {
        Common::playSE("p.mp3");
        
        auto champagne=AnchorSprite::create(Vec2(.82, .8), parent->getContentSize(),true, "sup_59_champagne.png");
        parent->addChild(champagne);
        auto glasses=createSpriteToCenter("sup_59_glasses.png", true, parent);
        parent->addChild(glasses);
        
        auto coin=createSpriteToCenter("sup_59_coin.png", true, parent);
        parent->addChild(coin);
        
        auto bottle_action=TargetedAction::create(champagne, Sequence::create(FadeIn::create(1),
                                                                              Spawn::create(RotateBy::create(1, -10),
                                                                                            Sequence::create(DelayTime::create(.5),
                                                                                                             createSoundAction("jobojobo.mp3"),NULL), NULL),
                                                                              Spawn::create(TargetedAction::create(glasses, FadeIn::create(1)),
                                                                                            TargetedAction::create(coin, Sequence::create(FadeIn::create(.5),
                                                                                                                                          createSoundAction("chalin.mp3"),
                                                                                                                                           NULL)),
                                                                                            NULL),
                                                                              DelayTime::create(.3),
                                                                              Spawn::create(RotateBy::create(.7, -15),
                                                                                            FadeOut::create(.7),
                                                                                            MoveBy::create(.7, Vec2(0, parent->getContentSize().height*.1)), NULL),
                                                                              DelayTime::create(1),NULL));
        
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(bottle_action,call, NULL));
    }
    else if(key=="suzuri"){
        Common::playSE("p.mp3");
        
        auto brush=createSpriteToCenter("sup_13_brush.png", true, parent);
        parent->addChild(brush);
        
        auto ink=createSpriteToCenter("sup_13_ink.png", true, parent);
        brush->addChild(ink);
        
        auto seq_brush=TargetedAction::create(brush, Sequence::create(FadeIn::create(1),
                                                                      MoveBy::create(1, Vec2(0, -parent->getContentSize().height*.1)),
                                                                      DelayTime::create(.5),
                                                                      Repeat::create(jumpAction(parent->getContentSize().height*.05, .25, brush, 1.5), 3),
                                                                      DelayTime::create(.5),
                                                                      NULL));
        
        auto spawn=Spawn::create(seq_brush,
                                 TargetedAction::create(ink, Sequence::create(DelayTime::create(3),
                                                                              FadeIn::create(1), NULL)), NULL);
        auto fadeouts=Spawn::create(TargetedAction::create(brush, Spawn::create(MoveBy::create(1, Vec2(0, parent->getContentSize().height*.2)),
                                                                                FadeOut::create(1),NULL)),
                                    TargetedAction::create(ink, FadeOut::create(1)), NULL);
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(spawn,fadeouts,call, NULL));
    }
    else if(key=="brush"){
        Common::playSE("p.mp3");
        
        auto brush=createSpriteToCenter("sup_6_brush.png", true, parent);
        parent->addChild(brush);
        
        auto eye=createSpriteToCenter("sup_6_eye.png", true, parent);
        parent->addChild(eye);
        
        auto open=createSpriteToCenter("sup_6_open.png", true, parent);
        parent->addChild(open);
        
        auto seq_brush=TargetedAction::create(brush, Sequence::create(FadeIn::create(1),
                                                                      DelayTime::create(.5),
                                                                      Spawn::create(Repeat::create(jumpAction(parent->getContentSize().height*.01, .25, brush, 1.5), 3),
                                                                                    TargetedAction::create(eye, FadeIn::create(1)), NULL),
                                                                      DelayTime::create(.5),
                                                                      FadeOut::create(1),NULL));
        auto delay=DelayTime::create(.7);
        auto fadein_open=Spawn::create(createSoundAction("su.mp3"),
                                       TargetedAction::create(open, FadeIn::create(1)), NULL);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(seq_brush,delay,fadein_open,delay->clone(),call, NULL));
    }
    else if(key=="qrcode"){
        auto anim_0=createSpriteToCenter("anim_14_iphone_0.png", true, parent);
        parent->addChild(anim_0);
        auto anim_1=createSpriteToCenter("anim_14_iphone_1.png", true, parent);
        parent->addChild(anim_1);
        
        auto fadein_0=TargetedAction::create(anim_0, FadeIn::create(1));
        auto delay=DelayTime::create(.7);
        auto fadein_1=TargetedAction::create(anim_1, FadeIn::create(1));
        auto vibe=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            NativeBridge::vibration();
        });
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_0,delay,fadein_1,delay->clone(),vibe,delay->clone(),call, NULL));
    }
    else if(key=="disable_match"){
        auto pig=parent->getChildByName("sup_57_pig.png");
        Common::playSE("pig_1.mp3");
        auto pos_original=pig->getPosition();
        auto scale_original=pig->getScale();
        auto scale=1.3;//拡大率
        auto duration=.25;
        
        auto height=parent->getContentSize().height*.2;
        auto spawn=Spawn::create(JumpBy::create(duration, Vec2(parent->getContentSize().width*.2, -parent->getContentSize().height*.2), height, 1),
                                 ScaleBy::create(duration, scale), NULL);
        auto bounce=createBounceAction(pig, .2, .05,scale_original*scale);
        auto delay=DelayTime::create(duration);
        auto back=Spawn::create(JumpTo::create(duration, pos_original, height, 1),
                                ScaleTo::create(duration, scale_original), NULL);
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        pig->runAction(Sequence::create(spawn,bounce,delay,back,call, NULL));
    }
    else if(key=="putMonkey")
    {
        auto put=Spawn::create(putAction(parent, "sup_24_bluearm.png", "koto.mp3", .5, nullptr),
                               putAction(parent, "sup_24_bluemonkey.png", "", .5, nullptr), NULL);
        auto delay=DelayTime::create(.3);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(put,delay,call, NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark ドラッグ
void WeddingActionManager::dragAction(std::string key,cocos2d::Node *parent, Vec2 pos)
{
    
}

#pragma mark - Item
void WeddingActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造
    if(key=="message"){
        auto story=StoryLayer::create("ending",[callback]{
            callback(true);
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
    else if(key=="champagne")
    {//
        auto cork=parent->itemImage->getChildByName("champagne_cork.png");
        
        auto shake=swingAction(Vec2(0, parent->getContentSize().height*.005), .2, cork, 1, false,2);
        auto delay=DelayTime::create(1);
        auto fadeout_cork=TargetedAction::create(cork, Spawn::create(FadeOut::create(1),
                                                                     createSoundAction("po.mp3"),
                                                                     MoveBy::create(1, Vec2(0, parent->getContentSize().height*.15)), NULL));
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(shake,delay,fadeout_cork,delay->clone(),call, NULL));
    }
}

#pragma mark 特有
void WeddingActionManager::candleAction(cocos2d::Node *parent)
{
    Vector<FiniteTimeAction*>actions;
    
    actions.pushBack(DelayTime::create(1));
    auto duration=1.0;
    for (int i=0; i<4; i++) {
        auto fileName=StringUtils::format("sup_15_%d.png",i);
        auto candle=createSpriteToCenter(fileName, true, parent);
        parent->addChild(candle);
        
        actions.pushBack(createSoundAction("huo.mp3"));
        if (i==0) {
            actions.pushBack(TargetedAction::create(candle, FadeIn::create(duration)));
        }
        else{
            auto change=createChangeAction(duration*1.5, duration, parent->getChildByName<Sprite*>(StringUtils::format("sup_15_%d.png",i-1)), candle);
            
            if (i<3) {
                actions.pushBack(change);
            }
            else{
                actions.pushBack(Sequence::create(change,
                                                  DelayTime::create(1),
                                                  TargetedAction::create(candle, FadeOut::create(duration)) , NULL));
            }
        }
        
        actions.pushBack(DelayTime::create(1));
    }
    
    parent->runAction(Repeat::create(Sequence::create(actions), -1));
}
