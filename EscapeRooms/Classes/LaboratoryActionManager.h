//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__LaboratoryActionManager__
#define __EscapeContainer__LaboratoryActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class LaboratoryActionManager:public CustomActionManager
{
public:
    static LaboratoryActionManager* manager;
    static LaboratoryActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void dragAction(std::string key,Node*parent,Vec2 pos);
    
private:
    void runMouse(Node*parent,bool animate);
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
