//
//  SchoolFesActionManager.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2018/11/29.
//
//

#include "SchoolFesActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "GameScene.h"
#include "TouchManager.h"

using namespace cocos2d;

SchoolFesActionManager* SchoolFesActionManager::manager =NULL;

SchoolFesActionManager* SchoolFesActionManager::getInstance()
{
    if (manager==NULL) {
        manager = new SchoolFesActionManager();
    }
    
    return manager;
}

#pragma mark - 背景
int SchoolFesActionManager::getDefaultBackNum()
{
    /*
    if(!DataManager::sharedManager()->isShowedOpeningStory()) {
        return 0;
    }*/
    
    return 4;
}

#pragma mark - Back
void SchoolFesActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
#pragma mark モグラ叩き
    if (key == "mogura" && !DataManager::sharedManager()->getEnableFlagWithFlagID(23)) {
        moguraIndex = -1;
        moguraCorrectCount = 0;//もぐらを出現させる時に正解数をリセットする。
        //初期位置
        auto upperPigHeight = .12;
        auto bottomPigHeight = .15;
        for (int i=0; i<6; i++) {
            auto pigName = StringUtils::format("sup_20_pig_%d.png",i);
            auto pig = parent->getChildByName<Sprite*>(pigName.c_str());
            auto pigHeight = (i/3 == 1)? bottomPigHeight:upperPigHeight;
            pig->setPositionY(parent->getContentSize().height*(.5-pigHeight));
        }
        
        moguraAnswers = {3, 5, 1, 2, 4, 0};

        //アニメーション開始！
        Vector<FiniteTimeAction*> actions;
        
        //箱揺れまでの間隔
        actions.pushBack(DelayTime::create(1));
        
        //ボックスを揺らす
        std::vector<std::string> boxFiles = {"sup_20_box.png", "sup_20_cover_0.png", "sup_20_cover_1.png"};
        Vector<FiniteTimeAction*> boxActions;
        for (auto boxFile : boxFiles) {
            auto box = parent->getChildByName<Sprite*>(boxFile.c_str());
            auto seq = Sequence::create(MoveTo::create(.1, Vec2(box->getPositionX()-parent->getContentSize().width*.005,box->getPositionY())),
                                        MoveTo::create(.1, Vec2(box->getPositionX()+parent->getContentSize().width*.005,box->getPositionY())), NULL);
            boxActions.pushBack(TargetedAction::create(box, seq));
        }
       
        actions.pushBack(createSoundAction("bohu.mp3"));
        actions.pushBack(Repeat::create(Spawn::create(boxActions), 4));

        for (auto answer : moguraAnswers) {
            actions.pushBack(DelayTime::create(1));

            auto targetPigName = StringUtils::format("sup_20_pig_%d.png",answer);
            auto targetPig = parent->getChildByName<Sprite*>(targetPigName.c_str());
            
            auto hidePosY = targetPig->getPositionY();
            auto lambdaMoguraIndex = [this](int index){
                moguraIndex = index;
            };
            auto answerMoguraIndex = CallFunc::create([lambdaMoguraIndex, answer](){
                lambdaMoguraIndex(answer);
            });
            auto resetMoguraIndex = CallFunc::create([lambdaMoguraIndex](){
                lambdaMoguraIndex(-1);
            });
            
            auto pigSeq = Sequence::create(answerMoguraIndex,
                                           createSoundAction("nyu.mp3"),
                                           EaseOut::create(MoveTo::create(.6, Vec2(targetPig->getPositionX(), parent->getContentSize().height*.5)), 1.5) ,
                                           DelayTime::create(.8),
                                           EaseIn::create(MoveTo::create(.6, Vec2(targetPig->getPositionX(), hidePosY)), 2),
                                           resetMoguraIndex, NULL);
            actions.pushBack(TargetedAction::create(targetPig, pigSeq));
        }
        actions.pushBack(DelayTime::create(.5));
        
        int actionTag = 2;
        actions.pushBack(CallFunc::create([this, parent, actionTag](){
            if (moguraCorrectCount == moguraAnswers.size()) {//正解通りに叩いたよ。
                parent->stopActionByTag(actionTag);
            }
            log("正解通りに叩いている回数%d",moguraCorrectCount);

            moguraCorrectCount = 0;
        }));
        
        auto sequence = Sequence::create(actions);
        
        auto repeat = Repeat::create(sequence, -1);
        repeat->setTag(actionTag);
        
        parent->runAction(repeat);
    }
#pragma mark 火の玉
    else if(key == "hinotama" && DataManager::sharedManager()->getEnableFlagWithFlagID(44)){
        Vector<FiniteTimeAction*> actions;

        actions.pushBack(showHinotamaParticle(parent, backID));
        actions.pushBack(DelayTime::create(1));
        auto seq = Sequence::create(actions);
        
        parent->runAction(Repeat::create(seq, -1));
    }
#pragma mark ウサギジャンプ
    else if(key == "rabbit"){
        //ちょっと待機
        Vector<FiniteTimeAction*> actions_seq;
        actions_seq.pushBack(DelayTime::create(.5));
        
        std::vector<int> answer = {3, 2, 1, 4, 5};
        float duration = 1;
        Vector<FiniteTimeAction*> actions_josou;
        Vector<FiniteTimeAction*> actions;
        for (int i = 0; i < 5; i++) {
            auto anchorPoint = Vec2(.113*.195*i, .152);
            auto shadow = createSpriteToCenter(StringUtils::format("sup_92_shadow_%d.png",i), false, parent, true);
            auto rabbit = createSpriteToCenter(StringUtils::format("sup_92_rabbit_%d.png",i), false, parent, true);
            auto josouAction = bounceJumpAction(rabbit, anchorPoint, parent, "pop_1.mp3", 2);
            actions_josou.pushBack(josouAction);
            
            //影のアニメ
            shadow->setOpacity(160);
            auto distance_per = parent->getContentSize().height*.008;
            auto move = MoveBy::create(duration/2, Vec2(20, distance_per*answer[i]));
            auto scale = ScaleTo::create(duration/2, pow(.85, answer[i]));
            auto spawn = Spawn::create(move, scale, NULL);
            auto moveback = MoveBy::create(duration/2, Vec2(-20, -distance_per*answer[i]));
            auto scaleback = ScaleTo::create(duration/2, 1);
            auto spawnback = Spawn::create(moveback, scaleback, NULL);
            actions.pushBack(TargetedAction::create(shadow, Sequence::create(DelayTime::create(.3), spawn, spawnback, NULL)));
            
            //ウサギのアニメ
            actions.pushBack(bounceJumpToAction(rabbit, anchorPoint, Vec2(anchorPoint.x*parent->getContentSize().width, anchorPoint.y*parent->getContentSize().height), parent, duration, parent->getContentSize().height*.12*answer[i], "pop_1.mp3"));
        }
        actions_seq.pushBack(Spawn::create(actions_josou));
        actions_seq.pushBack(DelayTime::create(.5));
        actions_seq.pushBack(Spawn::create(actions));
        actions_seq.pushBack(DelayTime::create(.4));
        
        parent->runAction(Repeat::create(Sequence::create(actions_seq), -1));
    }
#pragma mark ろくろ首
    else if(key == "rokuro" && DataManager::sharedManager()->getEnableFlagWithFlagID(36)){
        
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(54) && !DataManager::sharedManager()->isPlayingMinigame()) {
            return;
        }
        
        std::vector<int> answers = {1, 0, 2, 1, 0};
        auto rokuroSeq = rokuroActions(parent, answers);
        auto repearRokuro = Sequence::create(DelayTime::create(1.5), rokuroSeq, NULL);
        parent->runAction(Repeat::create(repearRokuro, -1));
        
        for (int i = 0; i < 2; i++) {
            showFireParticle(parent, backID, i);
        }
    }
#pragma mark 金魚
    else if(key == "kingyo") {
        auto fish_1 = createSpriteToCenter("sup_6_fish_1.png", false, parent, true);
        auto fish_2 = createSpriteToCenter("sup_6_fish_2.png", true, parent, true);
        auto fish_3 = createSpriteToCenter("sup_6_fish_3.png", true, parent, true);

        Vector<Sprite*> fishes;
        fishes.pushBack(fish_1);
        fishes.pushBack(fish_2);
        fishes.pushBack(fish_3);
        
        Vector<FiniteTimeAction*> actions;
        for (int i=0; i<3; i++) {
            
            auto beforeFish = fishes.at(i);
            auto afterFish = fishes.at((i+1)%3);
            
            actions.pushBack(DelayTime::create(.5));

            actions.pushBack(createChangeAction(.5, .5, beforeFish, afterFish));
            actions.pushBack(createSoundAction("pochan.mp3"));
            
            actions.pushBack(DelayTime::create(1));
        }
        
        //後でアニメーション停止するためにparentにrunActionしていない。
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
}

#pragma mark - Touch
void SchoolFesActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    Vector<FiniteTimeAction*> actions;
    
#pragma mark マッチ→ろくろ首
    if (key == "candle") {
        actions.pushBack(useItemSoundAction());
        
        //マッチ
        auto match = createSpriteToCenter("sup_90_match.png", true, parent, true);
        transformToAnchorSprite(parent, match, Vec2(.167, .53));
        
        Vector<FiniteTimeAction*> matchActions;
        matchActions.pushBack(FadeIn::create(1));
        
        std::vector<Vec2> matchPoses = getRokuroRousokuPoses(90);
        
        int i = 0;
        for (auto matchPos : matchPoses) {
            //マッチ移動&フリフリ
            matchActions.pushBack(EaseInOut::create(MoveTo::create(i==1? 1.2 : .6, Vec2(matchPos.x*parent->getContentSize().width, matchPos.y*parent->getContentSize().height)), 2));
            matchActions.pushBack(Repeat::create(swingAction(Vec2(0, parent->getContentSize().height*.02), .1, match, 1.51, false), 2));
            matchActions.pushBack(DelayTime::create(.3));
            
            //火つける
            auto fire = showFireParticle(parent, 90, i);
            auto totalPar = fire->getTotalParticles();
            fire->setTotalParticles(0);
            
            matchActions.pushBack(createSoundAction("huo.mp3"));
            matchActions.pushBack(CallFunc::create([fire,totalPar](){
                fire->setTotalParticles(totalPar);
            }));
            
            i++;
        }
        
        //上に行きながら消える
        auto moveFadeout = Spawn::create(FadeOut::create(.5), MoveBy::create(.5, Vec2(0, parent->getContentSize().height*.05)), NULL);
        matchActions.pushBack(moveFadeout);
        
        actions.pushBack(TargetedAction::create(match, Sequence::create(matchActions)));
        actions.pushBack(DelayTime::create(1.5));
       
        //back_89を透明にして貼る
        auto back_89 = createSpriteToCenter("back_89.png", true, parent, true);
        back_89->setLocalZOrder(2);
        SupplementsManager::getInstance()->addSupplementToMain(back_89, 89);
        
        auto black=Sprite::create();
        black->setColor(Color3B::BLACK);
        black->setOpacity(0);
        black->setLocalZOrder(2);
        black->setPosition(parent->getContentSize()/2);
        black->setTextureRect(Rect(0, 0, parent->getContentSize().width, parent->getContentSize().height));
        parent->addChild(black);
        
        //ブラックアウト
        auto blackout = TargetedAction::create(black, FadeIn::create(.5));
        actions.pushBack(blackout);
        
        Vector<ParticleSystemQuad*> fires;
        for (int i = 0; i< 2; i++) {
            //火つける
            auto fire = showFireParticle(back_89, 89, i);
            auto totalPar = fire->getTotalParticles();
            fire->setTotalParticles(0);
            
            fires.pushBack(fire);
            actions.pushBack(CallFunc::create([fire,totalPar](){
                fire->setTotalParticles(totalPar);
            }));
        }
        
        //back_89可視化
        auto visible_89 = CallFunc::create([back_89](){
            back_89->setOpacity(255);
        });
        actions.pushBack(visible_89);
        actions.pushBack(DelayTime::create(1.5));
        
        //明転
        auto fadeout_black = TargetedAction::create(black, EaseOut::create(FadeOut::create(1.2), 1.5));
        actions.pushBack(fadeout_black);
        actions.pushBack(DelayTime::create(1));

        //だんだんこっち向く
        auto pig_0 = back_89->getChildByName<Sprite*>("sup_89_pig_0.png");
        auto pig_1 = createSpriteToCenter("sup_89_pig_1.png", true, back_89, true);
        auto pig_2 = createSpriteToCenter("sup_89_pig_2.png", true, back_89, true);
        auto pig_body = createSpriteToCenter("sup_89_pig_body.png", true, back_89, true);
        
        auto pig_head = createSpriteToCenter("sup_89_pig_head.png", true, back_89, true);
        float changeDuration = .75;
        actions.pushBack(createChangeAction(changeDuration, changeDuration, pig_0, pig_1, "su.mp3"));
        actions.pushBack(DelayTime::create(changeDuration*1.5));
        actions.pushBack(createChangeAction(changeDuration, changeDuration, pig_1, pig_2, "su.mp3"));
        actions.pushBack(DelayTime::create(changeDuration*1.8));
        actions.pushBack(createChangeAction(changeDuration, changeDuration, {pig_2}, {pig_body, pig_head}, "su.mp3"));
        actions.pushBack(DelayTime::create(changeDuration*1.8));
    
        //豚が鳴く
        actions.pushBack(createSoundAction("pig.mp3"));
        actions.pushBack(DelayTime::create(2.5));
        
        //首がちょいと伸びる演出
        std::vector<int> answers = {1, 0};
        actions.pushBack(rokuroActions(back_89, answers));
        
        actions.pushBack(DelayTime::create(1));

        //正解音
        actions.pushBack(correctSoundAction());
        
        //back_90に戻る
        float backDuration = .8;
        auto backSpawn = Spawn::create(TargetedAction::create(back_89, FadeOut::create(backDuration)),
                                   CallFunc::create([fires, backDuration](){
            for (auto fire : fires) {
                fire->setDuration(backDuration);
            }
        }), NULL);
        //actions.pushBack(backSpawn);
    }
#pragma mark 吹き出し
    else if(key.substr(0, 5) == "cloud") {
        int num = DataManager::sharedManager()->getNowBack();

        if (num == 36) {
            auto fileName = "sup_36_pig.png";
            auto charaAnchor = parent->getChildByName<Sprite*>(fileName)->getAnchorPoint();
            auto cloudBounce = cloudBounceAction(parent, {fileName}, charaAnchor, "sup_36_cloud.png", Vec2(.62, .72), "pig.mp3");
            actions.pushBack(cloudBounce);
        }
        else if (num == 43) {
            auto fileName = "sup_43_pig_0.png";
            auto charaAnchor = parent->getChildByName<Sprite*>(fileName)->getAnchorPoint();
            auto cloudBounce = cloudBounceAction(parent, {fileName}, charaAnchor, "sup_43_cloud.png", Vec2(.587, .505), "pig.mp3");
            actions.pushBack(cloudBounce);
        }
        else if (num == 48) {//コック豚にケチャップ
            auto fileName = "sup_48_pig.png";
            auto charaAnchor = parent->getChildByName<Sprite*>(fileName)->getAnchorPoint();
            auto cloudBounce = cloudBounceAction(parent, {fileName,"sup_48_hand.png"}, charaAnchor, "sup_48_cloud.png", Vec2(.648, .585), "pig.mp3");
            actions.pushBack(cloudBounce);
        }
        else if (num == 70) {//鉄砲&コルク
            auto fileName = "sup_70_monkey.png";
            auto cloudFileName = StringUtils::format("sup_70_cloud%s.png",key.substr(5).c_str());
            log("クラウドネームです。%s",cloudFileName.c_str());
            auto cloudBounce = cloudBounceAction(parent, {fileName}, Vec2(.692, .08), cloudFileName.c_str(), Vec2(.552, .573), "monkey.mp3");
            actions.pushBack(cloudBounce);
        }
        else if (num == 40) {//ボールが欲しい
            auto fileName = "sup_40_monkey.png";
            auto cloudBounce = cloudBounceAction(parent, {fileName}, Vec2(.345, .194), "sup_40_cloud.png", Vec2(.433, .624), "monkey.mp3");
            actions.pushBack(cloudBounce);
        }
        else if (num == 7) {//金魚
            auto cloudBounce = cloudBounceAction(parent, {/*fileName*/}, Vec2(.345, .194), "sup_7_cloud.png", Vec2(.62, .72), "pig.mp3");
            actions.pushBack(cloudBounce);
        }
        else if (num == 24) {//デコとハット
            log("3のフラグは%d",DataManager::sharedManager()->getEnableFlagWithFlagID(3));
            auto fileName = "sup_24_pig.png";
            auto cloudBounce = cloudBounceAction(parent, {fileName}, Vec2(.5, .03), StringUtils::format("sup_24_%s.png", key.c_str()), Vec2(.632, .647), "pig.mp3");
            actions.pushBack(cloudBounce);
        }
    }
#pragma mark お札をあげる
    else if(key == "givebill") {
        actions.pushBack(useItemSoundAction());
        
        //お札をあげる
        auto bill = createSpriteToCenter("sup_36_bill.png", true, parent, true);
        actions.pushBack(giveAction(bill, .7, .85, .7));
        
        //豚喜ぶ
        actions.pushBack(createSoundAction("pig.mp3"));
        auto pig = parent->getChildByName<Sprite*>("sup_36_pig.png");
        
        for (int i = 0; i < 3; i++) {
            actions.pushBack(createBounceAction(pig, .2, .15));
        }
        
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(correctSoundAction());
    }
#pragma mark 木箱に鍵使用
    else if(key == "usekey") {
        auto key_0 = createSpriteToCenter("sup_30_key_0.png", true, parent, true);
        auto key_1 = createSpriteToCenter("sup_30_key_1.png", true, parent, true);

        actions.pushBack(setItemAction(parent, key_0));
        actions.pushBack(DelayTime::create(.2));
        actions.pushBack(createChangeAction(.5, .5, key_0, key_1, "kacha.mp3"));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(TargetedAction::create(key_1, FadeOut::create(.6)));
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(correctSoundAction());
        
        actions.pushBack(DelayTime::create(.2));
        actions.pushBack(createSoundAction("gacha.mp3"));
    }
#pragma mark チケットをメイドにあげる
    else if(key == "giveticket") {
        
        actions.pushBack(useItemSoundAction());
        auto deskLocalZOrder = parent->getChildByName<Sprite*>("sup_43_desk.png")->getLocalZOrder();
        
        //チケットをあげる
        auto ticket = createSpriteToCenter("sup_43_ticket.png", true, parent, true);
        ticket->setLocalZOrder(deskLocalZOrder);
        actions.pushBack(giveAction(ticket, .8, .85, .9));
        actions.pushBack(DelayTime::create(.2));
        
        //豚喜ぶ
        actions.pushBack(createSoundAction("pig.mp3"));
        auto pig = parent->getChildByName<Sprite*>("sup_43_pig_0.png");
        actions.pushBack(createBounceAction(pig, .3, .15));
        actions.pushBack(DelayTime::create(.3));
        
        //豚切り替わる
        auto pig_1 = createAnchorSprite("sup_43_pig_1.png", pig->getAnchorPoint().x, pig->getAnchorPoint().y, true, parent, true);
        actions.pushBack(createChangeAction(.4, .4, pig, pig_1));
        actions.pushBack(DelayTime::create(.7));
        
        //豚が後ろにバックジャンプ
        auto underDeskPos = Vec2(.56*parent->getContentSize().width, .15*parent->getContentSize().height);
        actions.pushBack(bounceJumpToAction(pig_1, pig_1->getAnchorPoint(), underDeskPos, parent, .3, parent->getContentSize().height*.06, "pop_1.mp3"));
        actions.pushBack(DelayTime::create(1.5));
        
        //バナナを持つ
        auto banana_0 = createSpriteToCenter("sup_43_banana_0.png", true, pig_1, true);//createAnchorSprite("sup_43_banana_0.png", pig->getAnchorPoint().x, pig->getAnchorPoint().y, true, parent, true);
        actions.pushBack(TargetedAction::create(banana_0, FadeIn::create(.2)));
        actions.pushBack(DelayTime::create(.5));
        
        //バナナに向かってジャンプ
        actions.pushBack(bounceJumpToAction(pig_1, pig_1->getAnchorPoint(), Vec2(.367*parent->getContentSize().width, pig_1->getPositionY()), parent, .5, parent->getContentSize().height*.1, "pop_1.mp3"));
        
        //バナナを刺す
        auto banana_1 = createSpriteToCenter("sup_43_banana_1.png", true, parent, true);
        banana_1->setLocalZOrder(deskLocalZOrder);
        actions.pushBack(createChangeAction(.25, .25, banana_0, banana_1)/*TargetedAction::create(banana_1, FadeIn::create(.25))*/);
        actions.pushBack(DelayTime::create(.3));
        
        //元の位置に戻る
        actions.pushBack(bounceJumpToAction(pig_1, pig_1->getAnchorPoint(), pig_1->getPosition(), parent, .2, parent->getContentSize().height*.07, "pop_1.mp3"));
        actions.pushBack(DelayTime::create(.4));
        
        actions.pushBack(createChangeAction(.4, .4, pig_1, pig));
        actions.pushBack(correctSoundAction());
    }
#pragma mark 後ろの豚にあげる
    else if(key.substr(0, 5) == "give_") {
        
        actions.pushBack(useItemSoundAction());
        
        //装飾をあげる
        auto itemName = key.substr(5);
        auto fileName = StringUtils::format("sup_24_%s.png", itemName.c_str());
        auto deco = createSpriteToCenter(fileName.c_str(), true, parent, true);
        actions.pushBack(giveAction(deco, .8, .85, .9));
        actions.pushBack(DelayTime::create(.2));
        
        //サム喜ぶ
        auto pig_24 = parent->getChildByName<Sprite*>("sup_24_pig.png");
        transformToAnchorSprite(parent, pig_24, Vec2(.5, .03));
        actions.pushBack(createSoundAction("pig.mp3"));
        actions.pushBack(createBounceAction(pig_24, .3, .05));
        actions.pushBack(DelayTime::create(.3));
        
        if (itemName == "hat") {
            //back2に遷移
            auto back2 = createSpriteToCenter("back_2.png", true, parent, true);
            SupplementsManager::getInstance()->addSupplementToMain(back2, 2);
            actions.pushBack(TargetedAction::create(back2, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(.5));
            
            //back_24にいる豚を消しておく
            actions.pushBack(TargetedAction::create(pig_24, FadeOut::create(.01)));
            
            //豚アクション開始
            auto sitdownPos = Vec2(back2->getContentSize().width*.353, back2->getContentSize().height*.383);
            auto landingPos = Vec2(back2->getContentSize().width*.405, back2->getContentSize().height*.212);

            auto pig_sit = back2->getChildByName<Sprite*>("sup_2_sitpig.png");
            pig_sit->removeFromParent();
            auto pig_0 = createSpriteToCenter("sup_2_pig_0.png", false, back2, true);
            auto pig_1 = createAnchorSprite("sup_2_pig_1.png", .355, .385, true, back2, true);
            auto pig_2 = createAnchorSprite("sup_2_pig_2.png", .633, .07, true, back2, true);
            pig_2->setScale(.642);
            pig_2->setPosition(landingPos);
            
            //豚切り替え&着地
            pig_1->setPosition(sitdownPos);
            actions.pushBack(createChangeAction(.2, .2, pig_0, pig_1));
            actions.pushBack(DelayTime::create(.6));
            actions.pushBack(createSoundAction("pop_short.mp3"));
            actions.pushBack(TargetedAction::create(pig_1, JumpTo::create(1.5, landingPos, back2->getContentSize().height*.1, 1)));
            actions.pushBack(createChangeAction(.2, .2, pig_1, pig_2));
            actions.pushBack(DelayTime::create(1));
            
            //豚走っていく
            auto movetoPos = Vec2(back2->getContentSize().width, 0);
            auto pigSpawn = Spawn::create(createSoundAction("run.mp3"),
                                          ScaleTo::create(1, 1),
                                          JumpTo::create(1.5, movetoPos, back2->getContentSize().height*.05, 5),
                                          Sequence::create(DelayTime::create(1), FadeOut::create(.5), NULL), NULL);
            actions.pushBack(TargetedAction::create(pig_2, pigSpawn));
            actions.pushBack(DelayTime::create(.5));
            
            //back3に切り替え
            auto back3 = createSpriteToCenter("back_3.png", true, parent, true);
            SupplementsManager::getInstance()->addSupplementToMain(back3, 3);
            actions.pushBack(createChangeAction(.4, .4, back2, back3));
            
            //豚が移動する.ジャンプ、着地、切り替え
            auto pig_3 = createAnchorSprite("sup_3_pig_0.png", .693, .118, false, back3, true);
            auto pig_4 = createAnchorSprite("sup_3_pig_1.png", .692, .326, true, back3, true);
            auto scaleTo = .677;
            auto moveToPos_3 = Vec2(back3->getContentSize().width*.682, back3->getContentSize().height*.209);
            auto jumpToPos_3 = Vec2(back3->getContentSize().width*pig_4->getAnchorPoint().x, back3->getContentSize().height*pig_4->getAnchorPoint().y);

            pig_3->setPosition(back3->getContentSize().width*.4, -back3->getContentSize().height*.3);
            auto pig_3_seq = Sequence::create(JumpTo::create(1.5, moveToPos_3, back2->getContentSize().height*.05, 5),
                                              JumpTo::create(1, jumpToPos_3, back2->getContentSize().height*.15, 1), NULL);
            actions.pushBack(createSoundAction("run.mp3"));
            actions.pushBack(TargetedAction::create(pig_3, Spawn::create(pig_3_seq, ScaleTo::create(2.5, scaleTo), NULL)));
            actions.pushBack(createChangeAction(.2, .2, pig_3, pig_4));
            
            //豚グニョン
            auto bounce = bounceJumpToAction(pig_4, pig_4->getAnchorPoint(), pig_4->getPosition(), back3, .2, back3->getPositionY()*.05, "pop_1.mp3");
            actions.pushBack(Repeat::create(bounce, 2));
            actions.pushBack(DelayTime::create(.6));
            actions.pushBack(createSoundAction("pig.mp3"));

            //back3消す
            actions.pushBack(TargetedAction::create(back3, FadeOut::create(.4)));
            actions.pushBack(DelayTime::create(.5));
            
            actions.pushBack(correctSoundAction());
        }
        else if (itemName == "deco") {
            //back23に遷移
            auto back23 = createSpriteToCenter("back_23.png", true, parent, true);
            actions.pushBack(TargetedAction::create(back23, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(.5));
            
            auto pig_0 = createAnchorSprite("sup_23_decopig_1.png", .5, .367, false, back23, true);
            pig_0->setLocalZOrder(2);
            auto originalPosX = pig_0->getPositionX();
            auto originalPosY = pig_0->getPositionY();
        
            std::vector<Vec2> poses = {Vec2(.325, .491), Vec2(.44, .367), Vec2(.553, .491), Vec2(.673, .367)};
            for (int i = 0; i < poses.size(); i++) {
                auto pos = poses[i];
                //移動 & ジャンプして貼る
                auto move = JumpTo::create(1, Vec2(pos.x*back23->getContentSize().width, originalPosY), back23->getContentSize().height*.05, 3);
                
                auto deco = createSpriteToCenter(StringUtils::format("sup_23_deco_%d.png",i), true, back23, true);
                auto bounce = createBounceAction(pig_0, .3, .1);
                auto jumpTo = EaseOut::create(MoveTo::create(.6, Vec2(pos.x*back23->getContentSize().width, pos.y*back23->getContentSize().height)), 2);
                auto fadein_deco = TargetedAction::create(deco, FadeIn::create(.05));
                auto jumpBack = EaseIn::create(MoveTo::create(.4, Vec2(pos.x*back23->getContentSize().width, originalPosY)), 2);
                
                Vector<FiniteTimeAction*> decoActions;
                decoActions.pushBack(createSoundAction("pop_5.mp3"));
                decoActions.pushBack(move);
                decoActions.pushBack(DelayTime::create(.4));
                decoActions.pushBack(bounce);
                
                if (i%2 == 0) {
                    decoActions.pushBack(createSoundAction("jump.mp3"));
                }
                decoActions.pushBack(jumpTo);
                decoActions.pushBack(createSoundAction("basket.mp3"));
                decoActions.pushBack(fadein_deco);
                decoActions.pushBack(jumpBack);
                actions.pushBack(TargetedAction::create(pig_0, Sequence::create(decoActions)));
                actions.pushBack(DelayTime::create(.5));
            }
            
            //元の位置に戻ってくる
            actions.pushBack(TargetedAction::create(pig_0, JumpTo::create(1, Vec2(originalPosX, originalPosY), back23->getContentSize().height*.05, 3)));
            actions.pushBack(DelayTime::create(.1));
            
            //決めポーズと差し替え
            auto pig_1 = createSpriteToCenter("sup_23_decopig_2.png", true, back23, true);
            actions.pushBack(createSoundAction("pig.mp3"));
            actions.pushBack(createChangeAction(.4, .4, pig_0, pig_1));
            actions.pushBack(DelayTime::create(1.5));
            
            //back23を消す
            actions.pushBack(TargetedAction::create(back23, FadeOut::create(.5)));
            actions.pushBack(DelayTime::create(.5));
        }
        
        //正解
        actions.pushBack(correctSoundAction());
    }
#pragma mark お賽銭
    else if(key == "usecoin") {
        
        actions.pushBack(useItemSoundAction());
        
        //コイン投げる
        auto boxPos = Vec2(parent->getContentSize().width*.5, parent->getContentSize().height*.197);
        AnchorSprite*money = AnchorSprite::create(Vec2(.5, .191), parent->getContentSize(), true, "sup_81_coin.png");
        parent->addChild(money);
        
        auto duration = 1.5;
        auto angle = 15.0;
        auto move_back = MoveBy::create(1, Vec2(0, -parent->getContentSize().height*.05));
        auto spawn = Spawn::create(ScaleBy::create(duration, .5),
                                 createSoundAction("hyu.mp3"),
                                 Sequence::create(RotateBy::create(.1, angle/2),Repeat::create(Sequence::create(RotateBy::create(.1, -angle),RotateBy::create(.1, angle), NULL),5), NULL),
                                   JumpTo::create(duration, boxPos, parent->getContentSize().height*.4, 1),
                                 Sequence::create(DelayTime::create(duration*.7),FadeOut::create(duration*.3), NULL)
                                 ,NULL);
        auto sound = CallFunc::create([]{
            Common::playSE("chalin.mp3");
        });
        
        auto seq_0 = TargetedAction::create(money,Sequence::create(FadeIn::create(.3), DelayTime::create(.4), move_back,spawn,sound, NULL));
        
        actions.pushBack(seq_0);
        actions.pushBack(DelayTime::create(1));
        
        //火の玉を表示する。
        //actions.pushBack(showHinotamaParticle(parent));
       
        //正解
        actions.pushBack(correctSoundAction());
    }
#pragma mark 福引にハンドルセット
    else if(key == "hukubiki") {
        actions.pushBack(useItemSoundAction());
        
        auto hukubiki = parent->getChildByName<Sprite*>("sup_64_hukubiki.png");
        hukubiki->setLocalZOrder(2);
        transformToAnchorSprite(parent, hukubiki, Vec2(.718, .509));
        
        //ハンドル表示
        auto handle = createSpriteToCenter("sup_64_handle.png", true, parent, true);
        handle->setLocalZOrder(hukubiki->getLocalZOrder());
        transformToAnchorSprite(parent, handle, Vec2(.737, .506));
        actions.pushBack(putAction(handle, "", .5, .5));

        auto targets = {hukubiki, handle};
        //ぐるぐる回す
        std::vector<Vec2> ballAnchors = {Vec2(.32, .233), Vec2(.372, .238)};
        for (int i = 0; i < 2; i++) {
            
            //回転。+10度地点で玉放出
            auto backRotate = getActionWithTargets(targets, EaseInOut::create(RotateBy::create(.2, -15), 2));
            auto rotate = getActionWithTargets(targets, EaseInOut::create(RotateBy::create(1.5, 385), 2));
            auto backRotate_ = getActionWithTargets(targets, EaseOut::create(RotateBy::create(.5, -10), 2));
            
            //ボールのアクション
            auto ballAnchor = ballAnchors[i];
            auto launchPos = Vec2(.463, .55);
            auto ball = createAnchorSprite(StringUtils::format("sup_64_ball_%d.png",i), ballAnchor.x, ballAnchor.y, true, parent, true);
            ball->setLocalZOrder(hukubiki->getLocalZOrder()-1);
            ball->setPosition(Vec2(parent->getContentSize().width*launchPos.x, parent->getContentSize().height*launchPos.y));
            
            auto fadeinAction = FadeIn::create(.1);
            auto jumpAction = JumpTo::create(.7, Vec2((ballAnchor.x + launchPos.x)*.48*parent->getContentSize().width, ballAnchor.y*parent->getContentSize().height), parent->getContentSize().height*.15, 1);
            auto jumpAction2 = JumpTo::create(.4, Vec2(ballAnchor.x*parent->getContentSize().width, ballAnchor.y*parent->getContentSize().height), parent->getContentSize().height*.1, 1);
            auto jumpSeq = Sequence::create(jumpAction, jumpAction2, NULL);
            
            auto ballAction = TargetedAction::create(ball, Spawn::create(fadeinAction, jumpSeq, NULL));
            
            auto hukubikiSeq = Sequence::create(backRotate, DelayTime::create(.4), createSoundAction("bell_1.mp3"), rotate, ballAction, backRotate_, NULL);
            
            actions.pushBack(hukubikiSeq);
        }
        
        actions.pushBack(correctSoundAction());
    }
#pragma mark もぐら叩きでピコピコハンマー使用
    else if(key.substr(0, 10) == "hitmogura_") {

        auto hammer = createAnchorSprite("sup_20_hammer.png", .782, .589, true, parent, true);
        
        int index = atoi(key.substr(10).c_str());
        log("表示されているモグラインデックスは%d:::叩いているモグラインデックスは%d",moguraIndex,index);
        
        std::vector<Vec2> hammerPoses = {Vec2(.425, .58), Vec2(.645, .58), Vec2(.865, .58), Vec2(.382, .47), Vec2(.65, .47), Vec2(.92, .47)};
        Vec2 delta = Vec2(/*.16*/.2, 0);
        auto hammerPos = Vec2(hammerPoses[index].x*parent->getContentSize().width, hammerPoses[index].y*parent->getContentSize().height);

        auto star = CallFunc::create([parent, hammerPos, delta, this, index](){
            if (moguraIndex != index) return;//叩いている場所が違ったら
            
            Common::playSE("piko.mp3");
            
            //星を表示する
            auto particle=ParticleSystemQuad::create("particle_hit.plist");
            particle->setPosition(hammerPos-Vec2(delta.x*parent->getContentSize().width, delta.y*parent->getContentSize().height));
            particle->setSpeed(particle->getSpeed()/3);
            parent->addChild(particle);
            
            //正解カウント
            moguraCorrectCount++;
            
            //同じ場所で何度も叩けないように
            moguraIndex = -1;
        });
        
        auto checkCompleteMogura = CallFunc::create([this, parent](){
            if (moguraCorrectCount == moguraAnswers.size()) {
                auto layer=SupplementsManager::getInstance()->createDisableLayer();
                Director::getInstance()->getRunningScene()->addChild(layer);
                
                DataManager::sharedManager()->setEnableFlag(23);//フラグを立てる
                DataManager::sharedManager()->removeItemWithID(6);//ぴこぴこハンマーを捨てる。
                
                Vector<FiniteTimeAction*> completeActions;
                log("正解を叩いているよ");
                auto back21 = createSpriteToCenter("back_21.png", true, parent, true);
                auto pigAnchor = Vec2(.5, .076);
                auto pig = createAnchorSprite("sup_21_pig.png", pigAnchor.x, pigAnchor.y, true, back21, true);
                pig->setScale(.707);
                pig->setPositionY(back21->getContentSize().height*.242);
                createSpriteToCenter("sup_21_cover.png", false, back21, true);
                auto close = createSpriteToCenter("sup_21_close.png", false, back21, true);
                
                completeActions.pushBack(DelayTime::create(1.5));
                completeActions.pushBack(TargetedAction::create(back21, FadeIn::create(1)));
                completeActions.pushBack(TargetedAction::create(pig, FadeIn::create(.1)));//back21が表示された後に表示させる。
                
                //open
                completeActions.pushBack(DelayTime::create(.5));
                completeActions.pushBack(Spawn::create(createSoundAction("gacha.mp3"), TargetedAction::create(close, FadeOut::create(.3)), NULL));
                
                //ふらふら
                completeActions.pushBack(DelayTime::create(.5));
                auto hurahura = EaseOut::create(Sequence::create(RotateTo::create(.7, -15), createSoundAction("pop_short.mp3"), RotateTo::create(.7, 15), NULL), 2);
                auto hurahura_repeat = Repeat::create(hurahura, 3);
                auto rotate_normal = EaseOut::create(RotateTo::create(.5, 0), 2);

                completeActions.pushBack(TargetedAction::create(pig, Sequence::create(hurahura_repeat, rotate_normal, NULL)));
                
                //ジャンプしながら大きくなる。
                completeActions.pushBack(DelayTime::create(.2));
                completeActions.pushBack(createSoundAction("nyu.mp3"));
                auto jump_scale = Spawn::create(JumpTo::create(.6, Vec2(pigAnchor.x*back21->getContentSize().width, pigAnchor.y*back21->getContentSize().height), back21->getContentSize().height*.05, 1),
                                                ScaleTo::create(.6, 1), NULL);
                completeActions.pushBack(TargetedAction::create(pig, jump_scale));
                
                //地上でフラフラ
                auto pig_end = JumpTo::create(1.2, Vec2(parent->getContentSize().width*1.5, pigAnchor.y*parent->getContentSize().height*.9), parent->getContentSize().height*.05, 5);
                auto pig_end_spawn = getActionsWithSound(pig_end, "pig.mp3");
                
                completeActions.pushBack(TargetedAction::create(pig, Sequence::create(hurahura_repeat->clone(), pig_end_spawn, NULL)));
                completeActions.pushBack(DelayTime::create(.5));
                
                //back21を削除
                completeActions.pushBack(TargetedAction::create(back21, FadeOut::create(.5)));
                completeActions.pushBack(DelayTime::create(.4));
                
                completeActions.pushBack(correctSoundAction());
                
                //タッチ有効化させる
                auto touchEnableAction = CallFunc::create([layer](){
                    layer->removeFromParent();
                });
                completeActions.pushBack(touchEnableAction);
                
                parent->runAction(Sequence::create(completeActions));
            }
        });
        
        auto pig = parent->getChildByName<Sprite*>(StringUtils::format("sup_20_pig_%d.png",index));
        hammer->setPosition(hammerPos);
        auto seq_hammer=TargetedAction::create(hammer, Sequence::create(FadeIn::create(.05),
                                                                        RotateBy::create(.2, 10),
                                                                        DelayTime::create(.05),
                                                                        Spawn::create(EaseIn::create(RotateBy::create(.15, -45), 1.3),
                                                                                      ScaleBy::create(.15, .8), NULL),
                                                                        Spawn::create(EaseOut::create(RotateBy::create(.15, 45), 1.3),
                                                                                      ScaleBy::create(.15, 1.0/.8), NULL),
                                                                        star,
                                                                        FadeOut::create(.1),
                                                                        checkCompleteMogura, NULL));
        actions.pushBack(seq_hammer);
    }
#pragma mark 井戸覗き。お化け邪魔する
    else if(key == "goido") {
        
        bool isMinigame = DataManager::sharedManager()->isPlayingMinigame();
        
        auto ghostFileName = isMinigame? "sup_91_mistake.png" : "sup_91_ghost.png";
        auto anchorPoint = isMinigame? Vec2(.5, .52) : Vec2(.767, .52);
        
        auto ghost = createAnchorSprite(ghostFileName, anchorPoint.x, anchorPoint.y, true, parent, true);
        ghost->setPosition(0, parent->getContentSize().height/2);
        Vector<FiniteTimeAction*> ghostActions;//ループ使うとコールバック呼ばれなくなっちゃうため。
        //コンフィグ作成
        ccBezierConfig config;
        config.controlPoint_1 = Vec2(parent->getContentSize().width/6 , parent->getContentSize().height*.9);
        config.controlPoint_2 = Vec2(parent->getContentSize().width*2/6, parent->getContentSize().height*.1);
        config.endPosition = Vec2(parent->getContentSize().width*3/6, parent->getContentSize().height/2);
        
        float duration = 1.5;
        auto bezier = BezierTo::create(duration, config);
        auto fade = Sequence::create(FadeIn::create(duration/4), FadeTo::create(duration/4, 200), FadeIn::create(duration/4), FadeOut::create(duration/4), NULL);
        auto spawn = Spawn::create(bezier, createSoundAction("ghost.mp3"), fade, NULL);
        ghostActions.pushBack(spawn);
        actions.pushBack(DelayTime::create(duration));
        
        auto swing = swingAction(Vec2(0, parent->getContentSize().height*.05), .8, ghost, 2, false, -1);
        auto swing_spawn = Spawn::create(swing, FadeIn::create(duration/4), NULL);
        ghostActions.pushBack(swing_spawn);
        actions.pushBack(DelayTime::create(duration/4));
        //間違い探しタッチ検索可能にする。
        auto callfunc = CallFunc::create([](){
            DataManager::sharedManager()->setTemporaryFlag("sup_91_mistake", 1);
        });
        actions.pushBack(callfunc);
        
        ghost->runAction(Sequence::create(ghostActions));
    }
#pragma mark 御札使用.悪霊退散！！！
    else if(key == "ohuda") {
        auto ghost = parent->getChildByName<Sprite*>("sup_91_ghost.png");

        auto fire = showFireParticle(parent, 91, 0);
        int totalParticles = fire->getTotalParticles();
        fire->setTotalParticles(0);
        
        auto ohuda = createAnchorSprite("sup_91_huda.png", .777, .524, true, parent, true);
        ohuda->setPosition(parent->getContentSize()/2);

        Vector<FiniteTimeAction*> huruhuruActions;
        actions.pushBack(useItemSoundAction());
        actions.pushBack(giveAction(ohuda, .5, .8, false));
        actions.pushBack(createSoundAction("pisi.mp3"));
        actions.pushBack(DelayTime::create(.2));
        huruhuruActions.pushBack(DelayTime::create(1));
        
        //お札プルプル震える
        auto seq = Sequence::create(MoveTo::create(.06, Vec2(ohuda->getPositionX()-parent->getContentSize().width*.005,ohuda->getPositionY())),
                                    MoveTo::create(.06, Vec2(ohuda->getPositionX()+parent->getContentSize().width*.005,ohuda->getPositionY())), NULL);
        actions.pushBack(TargetedAction::create(ohuda, Repeat::create(seq, 5)));
        actions.pushBack(DelayTime::create(.4));
        huruhuruActions.pushBack(DelayTime::create(1));
        
        //お化け表示&札プルプル
        ghost->runAction(Repeat::create(seq->clone(), -1));//actionsとは別の次元でアクション。
        actions.pushBack(TargetedAction::create(ghost, getActionsWithSound(FadeIn::create(.3), "ghost.mp3")));
        huruhuruActions.pushBack(DelayTime::create(.3));
        huruhuruActions.pushBack(Repeat::create(swingAction(20, .5, ohuda, 2), -1));
        ohuda->runAction(Sequence::create(huruhuruActions));
        actions.pushBack(DelayTime::create(1.5));
        
        //マジカルパーティクル表示.
        auto showMagical = CallFunc::create([parent](){
            auto particle=ParticleSystemQuad::create("particle_galaxy.plist");
            particle->setPosition(parent->getContentSize()/2);
            particle->setStartSize(parent->getContentSize().width*.1);
            particle->setStartRadius(parent->getContentSize().width*.8);
            particle->setTotalParticles(50);
            particle->setRotatePerSecond(particle->getRotatePerSecond()*.8);
            parent->addChild(particle);
        });
        actions.pushBack(getActionsWithSound(showMagical, "shine.mp3"));
        actions.pushBack(DelayTime::create(2.5));
        
        //炎表示
        actions.pushBack(createSoundAction("huo.mp3"));
        actions.pushBack(CallFunc::create([parent, fire, totalParticles](){
            log("炎パーティクルを表示します。");
            fire->setTotalParticles(totalParticles);
        }));
        actions.pushBack(DelayTime::create(1));
        
        //お化け悲鳴
        actions.pushBack(createSoundAction("ghost.mp3"));
        actions.pushBack(DelayTime::create(2));
        
        //ボンッ
        auto showBomb = CallFunc::create([parent](){
            auto particle=ParticleSystemQuad::create("bomb.plist");
            particle->setPosition(parent->getContentSize()/2);
            parent->addChild(particle);
        });
        actions.pushBack(getActionsWithSound(showBomb, "gun-fire.mp3"));
        actions.pushBack(DelayTime::create(.2));
        
        //お化け&お札&火を消す。
        actions.pushBack(CallFunc::create([fire](){fire->setDuration(.2);}));
        actions.pushBack(getActionWithTargets({ohuda, ghost}, FadeOut::create(.3)));
        
        
        actions.pushBack(correctSoundAction());
    }
#pragma mark ケチャップ使用
    else if (key == "giveketchup") {
        auto ketchup_0 = createSpriteToCenter("sup_48_ketchup_0.png", true, parent, true);
        auto ketchup_1 = createSpriteToCenter("sup_48_ketchup_1.png", true, parent, true);

        //ケチャップ渡す
        actions.pushBack(useItemSoundAction());
        actions.pushBack(giveAction(ketchup_0, .5, .8, false));
        actions.pushBack(DelayTime::create(.5));
        
        //ケチャップ入れ替わり
        auto hand = parent->getChildByName<Sprite*>("sup_48_hand.png");
        actions.pushBack(createChangeAction(.5, .5, {ketchup_0, hand}, {ketchup_1}));
        actions.pushBack(DelayTime::create(1));
        
        //ケチャップを振る
        actions.pushBack(createSoundAction("pig.mp3"));
        actions.pushBack(swingAction(Vec2(0, parent->getContentSize().height*.03), .3, ketchup_1, 2, false, 3));
        
        //back49に遷移
        auto back49 = createSpriteToCenter("back_49.png", true, parent, true);
        actions.pushBack(TargetedAction::create(back49, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.6));
        
        //腕出現→ソースかける
        auto source = createSpriteToCenter("sup_49_source.png", true, back49, true);
        
        auto hand_1 = createAnchorSprite("sup_49_pighand_1.png", -.2, .5, true, back49, true);
        auto hand_2 = createAnchorSprite("sup_49_pighand_2.png", -.2, .5, true, back49, true);
        actions.pushBack(TargetedAction::create(hand_1, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.4));
        
        actions.pushBack(createSoundAction("pig.mp3"));
        Vector<FiniteTimeAction*> sourceActions;
        sourceActions.pushBack(createChangeAction(.2, .2, hand_1, hand_2));
        sourceActions.pushBack(DelayTime::create(.1));
        sourceActions.pushBack(createChangeAction(.2, .2, hand_2, hand_1));
        sourceActions.pushBack(DelayTime::create(.2));
        auto sourceSeq = Sequence::create(sourceActions);
        actions.pushBack(Repeat::create(sourceSeq, 3));
        
        //ソース出現
        actions.pushBack(createChangeAction(.6, .6, hand_1, source));
        actions.pushBack(DelayTime::create(1));
        
        //手あり状態に戻す。
        actions.pushBack(createChangeAction(.5, .5, ketchup_1, hand));
        
        //back戻る
        actions.pushBack(TargetedAction::create(back49, FadeOut::create(.5)));
        
        actions.pushBack(correctSoundAction());
    }
#pragma mark 鉄砲を渡す
    else if (key == "givegun") {
        actions.pushBack(useItemSoundAction());
        
        //鉄砲渡す
        auto gun = createSpriteToCenter("sup_70_gun_0.png", true, parent, true);
        actions.pushBack(giveAction(gun, .5, .9, false));
        actions.pushBack(DelayTime::create(.6));
    
        //構える
        auto gun_1 = createSpriteToCenter("sup_70_gun_1.png", true, parent, true);
        actions.pushBack(createChangeAction(.4, .4, gun, gun_1, "koto.mp3"));
        actions.pushBack(createSoundAction("monkey.mp3"));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(correctSoundAction());
        actions.pushBack(DelayTime::create(.3));
    }
#pragma mark コルク玉を渡す
    else if (key == "givecork") {
        actions.pushBack(useItemSoundAction());
        
        //コルク渡す
        auto gun = createSpriteToCenter("sup_70_cork.png", true, parent, true);
        actions.pushBack(giveAction(gun, .5, .9, true));
        
        //構える
        actions.pushBack(createSoundAction("monkey.mp3"));
        actions.pushBack(DelayTime::create(1.5));
        
        //back71に遷移
        auto back71 = createSpriteToCenter("back_71.png", true, parent, true);
        actions.pushBack(TargetedAction::create(back71, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(1.5));
        
        //暗転
        auto black=Sprite::create();
        black->setColor(Color3B::BLACK);
        black->setOpacity(0);
        black->setPosition(parent->getContentSize()/2);
        black->setTextureRect(Rect(0, 0, parent->getContentSize().width, parent->getContentSize().height));
        back71->addChild(black);
        auto blackoutAction = TargetedAction::create(black, FadeIn::create(.5));
        actions.pushBack(blackoutAction);
        actions.pushBack(DelayTime::create(.3));
        
        //back69に遷移
        auto stencil=createSpriteToCenter("sup_69_clip.png", false, parent);
        auto clip=ClippingNode::create(stencil);
        clip->setInverted(false);
        clip->setAlphaThreshold(0.0f);
        auto back69 = createSpriteToCenter("back_69.png", true, back71);
        SupplementsManager::getInstance()->addSupplementToMain(back69, 69);
        clip->addChild(back69);
        back71->addChild(clip);
        
        //ターゲットマークを表示
        auto targetMark = createSpriteToCenter("sup_69_target.png", true, back71, true);
        targetMark->setColor(Color3B::RED);
        actions.pushBack(getActionWithTargets({back69, targetMark},  FadeIn::create(.1)));
        
        //狙いを定める
        std::vector<Vec2> poses = {Vec2(.5, .82), Vec2(.408, .706), Vec2(.595, .706), Vec2(.27, .548), Vec2(.5, .548), Vec2(.727, .548)};
        std::vector<float> monkeyAnchorY = {.797, .671, .674, .497, .497, .497};
        
        int i = 0;
        for (auto pos : poses) {
            auto posVec = Vec2(pos.x*parent->getContentSize().width, pos.y*parent->getContentSize().height);
            actions.pushBack(getActionWithTargets({stencil, targetMark}, EaseInOut::create(MoveTo::create(1.5, posVec), 2) ));
            actions.pushBack(DelayTime::create(.3));
            actions.pushBack(createSoundAction("gun-fire_1.mp3"));
            
            ////発射&反動
            //発射
            auto cork = createSpriteToCenter("sup_69_cork.png", true, back69, true);
            cork->setPosition(posVec);
            auto corkSpawn = Spawn::create(Sequence::create(FadeIn::create(.05), DelayTime::create(.35), FadeOut::create(.1), NULL),
                                           RotateTo::create(.5, 340),
                                           jumpAction(parent->getContentSize().height*.02, .5, cork, 2),
                                           ScaleTo::create(.5, .4), NULL);
            
            //反動
            auto reactionAction = Spawn::create(jumpAction(parent->getContentSize().height*.025, .1, stencil, 1.5),
                                                jumpAction(parent->getContentSize().height*.025, .1, targetMark, 1.5), NULL);
            actions.pushBack(Spawn::create(TargetedAction::create(cork, corkSpawn),
                                           reactionAction,NULL));
            actions.pushBack(createSoundAction("pan.mp3"));
            ////
            
            //猿揺れる
            auto monkey = back69->getChildByName<Sprite*>(StringUtils::format("sup_69_monkey_%d.png",i));
            transformToAnchorSprite(back69, monkey, Vec2(pos.x, monkeyAnchorY[i]));
            auto monkeyCall = CallFunc::create([back69, monkey, this](){
                auto swingMonkeyAction= swingAction(10, .5, monkey, 2, true, 4);
                back69->runAction(swingMonkeyAction);
            });
            actions.pushBack(monkeyCall);
            
            actions.pushBack(DelayTime::create(.1));
            
            i++;
        }
        
        //戻る前にback_71で猿を倒しておく。
        auto lieMonkey = createSpriteToCenter("sup_71_lie.png", true, back71, true);
        actions.pushBack(TargetedAction::create(lieMonkey, FadeIn::create(.5)));
        
        //ターゲット状態を消す。
        //actions.pushBack(getActionWithTargets({back69, targetMark, black}, FadeOut::create(.4)));
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(createSoundAction("monkey.mp3"));
        actions.pushBack(DelayTime::create(2));
        
        //back71を消す
        //actions.pushBack(TargetedAction::create(back71, FadeOut::create(.4)));
        actions.pushBack(getActionWithTargets({back69, targetMark, black,back71}, FadeOut::create(.4)));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(correctSoundAction());
    }
#pragma mark 猿に玉を渡す
    else if (key == "giveball") {
        actions.pushBack(useItemSoundAction());
        
        //玉渡す
        auto ball_0 = createSpriteToCenter("sup_40_ball_0.png", true, parent, true);
        actions.pushBack(giveAction(ball_0, .5, .9, false));
        actions.pushBack(DelayTime::create(.6));
        
        //玉受け取る
        auto ball_1 = createSpriteToCenter("sup_40_ball_1.png", true, parent, true);
        actions.pushBack(createChangeAction(.4, .4, ball_0, ball_1));
        actions.pushBack(createSoundAction("monkey.mp3"));
        actions.pushBack(DelayTime::create(.5));
        
        //汚れをつける。
        auto dirty = createSpriteToCenter("sup_40_dirty.png", true, parent, true);
        
        //射的アングルに遷移
        auto back42 = createSpriteToCenter("back_42.png", true, parent, true);
        actions.pushBack(TargetedAction::create(back42, FadeIn::create(.5)));
        
        //汚れ表示
        actions.pushBack(TargetedAction::create(dirty, FadeIn::create(.01)));
        actions.pushBack(DelayTime::create(.4));
        
        //射的アングルに遷移したら、玉を非表示にしておく
        actions.pushBack(TargetedAction::create(ball_1, FadeOut::create(.01)));
        
        auto hand_0 = createSpriteToCenter("sup_42_hand_0.png", false, back42, true);
        hand_0->setLocalZOrder(2);
        auto hand_1 = createSpriteToCenter("sup_42_hand_1.png", true, back42, true);
        hand_1->setLocalZOrder(2);
        
        std::vector<std::string> ball_type = {"red", "blue", "green"};
        std::vector<int> colors = {0, 2, 0, 0, 1, 2, 2, 2, 0};
        std::vector<Vec2> targetPoses = {Vec2(.395, .709), Vec2(.533, .718), Vec2(.687, .724), Vec2(.357, .621), Vec2(.52, .621), Vec2(.692, .626), Vec2(.322, .511), Vec2(.503, .509), Vec2(.695, .506)};
        for (int i = 0; i < targetPoses.size(); i++) {
            auto ballName = StringUtils::format("sup_42_ball_%s.png",ball_type[colors[i]].c_str());
            auto ball = createAnchorSprite(ballName, .358, .421, true, back42, true);
            actions.pushBack(TargetedAction::create(ball, FadeIn::create(.05)));
            //手の切り替え
            actions.pushBack(createChangeAction(.2, .2, hand_0, hand_1, "hyu.mp3"));
            
            //ボールの動き
            auto dirtySp = createSpriteToCenter(StringUtils::format("sup_42_dirty_%d.png", i), true, back42, true);
            auto targetPos = Vec2(targetPoses[i].x*parent->getContentSize().width, targetPoses[i].y*parent->getContentSize().height);
            
            auto throughBall = EaseIn::create(Spawn::create(ScaleTo::create(.5, .75),
                                                            JumpTo::create(.5, targetPos, back42->getContentSize().height*.15, 1), NULL), 1.5);
            auto throughBallSeq = Sequence::create(throughBall,
                                                   createSoundAction("hit_1.mp3"),
                                                   createChangeAction(.2, .2, ball, dirtySp), NULL);
            
            actions.pushBack(TargetedAction::create(ball, throughBallSeq));
            
            //手の切り替え
            actions.pushBack(createChangeAction(.2, .2, hand_1, hand_0));
        }
        //back_42を消す
        actions.pushBack(DelayTime::create(.8));
        actions.pushBack(TargetedAction::create(back42, FadeOut::create(.5)));
        
        //ちょっと待って猿の声
        actions.pushBack(DelayTime::create(.4));
        actions.pushBack(createSoundAction("monkey.mp3"));
        
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(correctSoundAction());
        
    }
#pragma mark ぽい使用
    else if (key == "poi") {
        actions.pushBack(useItemSoundAction());
        
        auto poi = createSpriteToCenter("sup_6_poi.png", true, parent, true);
        auto kingyo = createSpriteToCenter("sup_6_kingyo.png", true, poi, true);
       
        actions.pushBack(setItemAction(parent, poi));
        
        //ぽいですくう
        float poiDuration = .7;
        auto jump = TargetedAction::create(poi, JumpTo::create(poiDuration, Vec2(parent->getContentSize().width*.05, parent->getContentSize().height*.45), -parent->getContentSize().height*.2, 1));
        
        auto kingyoSeq = Sequence::create(DelayTime::create(poiDuration/2),
                                          TargetedAction::create(kingyo, FadeIn::create(.2)), NULL);
        
        actions.pushBack(Spawn::create(createSoundAction("swing_0.mp3"), jump, kingyoSeq, NULL));
        actions.pushBack(DelayTime::create(.6));
        
        //ぽい上に移動しながらフェードアウト
        auto fadeoutSpawn = Spawn::create(MoveBy::create(.5, Vec2(0, parent->getContentSize().height*.1)), FadeOut::create(.5), NULL);
        actions.pushBack(TargetedAction::create(poi, fadeoutSpawn));
        actions.pushBack(DelayTime::create(.5));
        
        actions.pushBack(correctSoundAction());
    }
#pragma mark 金魚あげる
    else if (key == "givekingyo") {
        //金魚を配置
        actions.pushBack(useItemSoundAction());
        auto kingyo = createSpriteToCenter("sup_7_kingyo.png",true, parent, true);
        actions.pushBack(setItemAction(parent, kingyo));
        actions.pushBack(DelayTime::create(.5));
        
        //喜ぶ
        auto pig_0 = parent->getChildByName<Sprite*>("sup_7_pig_0.png");
        auto pig_1 = createSpriteToCenter("sup_7_pig_1.png", true, parent, true);
        //auto pig_2 = createSpriteToCenter("sup_7_pig_2.png", true, parent, true);
        actions.pushBack(TargetedAction::create(kingyo, FadeOut::create(.5)));
        actions.pushBack(createChangeAction(.2, .2, pig_0, pig_1));//
        actions.pushBack(DelayTime::create(.3));
        
        //手を振って喜ぶ。
        actions.pushBack(createSoundAction("pig.mp3"));
        /*
        float dura = .2;
        auto seq = Sequence::create(createChangeAction(dura, dura, pig_1, pig_2),
                                    createChangeAction(dura, dura, pig_2, pig_1), NULL);
        actions.pushBack(Repeat::create(seq, 7));
        actions.pushBack(DelayTime::create(.3));
         */
        
        Vector<SpriteFrame*>frames;
        for (int i=0; i<2; i++) {
            frames.pushBack(EscapeStageSprite::createWithSpriteFileName(StringUtils::format("sup_7_pig_%d.png",i+1))->getSpriteFrame());
        }
        auto animation=Animation::createWithSpriteFrames(frames);
        animation->setDelayPerUnit(5.0f/60.0f);//??フレームぶん表示
        animation->setRestoreOriginalFrame(true);
        animation->setLoops(10);
        actions.pushBack(TargetedAction::create(pig_1, Animate::create(animation)));
        actions.pushBack(DelayTime::create(.5));

        //懐中電灯に手をかける
        auto pig_3 = createSpriteToCenter("sup_7_pig_3.png", true, parent, true);
        actions.pushBack(createChangeAction(.2, .2, pig_1, pig_3));
        actions.pushBack(swingAction(Vec2(parent->getContentSize().width*.05, 0), .3, pig_3, 1.5, false, 5));
        actions.pushBack(DelayTime::create(.6));
        
        //懐中電灯くれる
        auto pig_4 = createSpriteToCenter("sup_7_pig_4.png", true, parent, true);
        actions.pushBack(createChangeAction(.2, .2, pig_3, pig_4, "poku.mp3"));
        actions.pushBack(DelayTime::create(.5));

        /*
        auto pig_5 = createSpriteToCenter("sup_7_pig_5.png", true, parent, true);
        actions.pushBack(createChangeAction(.3, .3, pig_4, pig_5));*/
    }
#pragma mark カメラパシャ
    else if (key == "camera") {
        actions.pushBack(useItemSoundAction());
        
        //back16に遷移
        auto back16 = createSpriteToCenter("back_16.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back16, 16);
        actions.pushBack(TargetedAction::create(back16, FadeIn::create(.7)));
        actions.pushBack(DelayTime::create(1));
        
        //カメラ表示
        auto camera = createSpriteToCenter("sup_16_camera_1.png", true, back16, true);
        actions.pushBack(giveAction(camera, .6, 1, false));
        actions.pushBack(DelayTime::create(.7));
        
        //カメラパシャり
        actions.pushBack(createSoundAction("camera.mp3"));
        auto upperFrame = Sprite::create();
        upperFrame->setTextureRect(Rect(0, 0, parent->getContentSize().width, parent->getContentSize().height/2));
        upperFrame->setColor(Color3B::BLACK);
        upperFrame->setAnchorPoint(Vec2(.5, 1));
        upperFrame->setScaleY(0);
        upperFrame->setPosition(Vec2(parent->getContentSize().width/2, parent->getContentSize().height));
        parent->addChild(upperFrame);
        
        auto bottomFrame = Sprite::create();
        bottomFrame->setTextureRect(Rect(0, 0, parent->getContentSize().width, parent->getContentSize().height/2));
        bottomFrame->setColor(Color3B::BLACK);
        bottomFrame->setAnchorPoint(Vec2(.5, 0));
        bottomFrame->setScaleY(0);
        bottomFrame->setPosition(Vec2(parent->getContentSize().width/2, 0));

        parent->addChild(bottomFrame);

        auto seq = Sequence::create(EaseIn::create(ScaleTo::create(.2, 1, 1), 2),
                                    EaseOut::create(ScaleTo::create(.2, 1, 0), 2), NULL);
        actions.pushBack(getActionWithTargets({upperFrame, bottomFrame}, seq));
        actions.pushBack(DelayTime::create(.7));
        
        //カメラ切り替え
        auto camera_2 = createSpriteToCenter("sup_16_camera_2.png", true, back16, true);
        camera_2->setLocalZOrder(2);
        actions.pushBack(createChangeAction(.5, .5, camera, camera_2));
        
        //現像
        auto cheki = createSpriteToCenter("sup_16_cheki.png", true, back16, true);
        transformToAnchorSprite(back16, cheki, Vec2(.5, .448));
        
        auto distance_1 = parent->getContentSize().height*.2;
        auto fadein = TargetedAction::create(cheki, FadeIn::create(2));//delay混み
        auto sound = CallFunc::create([]{Common::playSE("printer_1.mp3");});
        
        auto move_1=TargetedAction::create(cheki, EaseOut::create(MoveBy::create(1.5, Vec2(0, distance_1)),1.5));
        
        auto seq_ = Sequence::create(fadein , sound, move_1, NULL);
        actions.pushBack(seq_);
        actions.pushBack(DelayTime::create(1.5));
        
        //チェキアップに遷移&写真浮かび上がる。
        auto cheki_2 = createSpriteToCenter("sup_16_cheki_2.png", true, back16, true);
        auto cheki_3 = createSpriteToCenter("sup_16_cheki_3.png", true, back16, true);
        
        actions.pushBack(createChangeAction(.5, .5, {camera_2, cheki}, {cheki_2}));
        actions.pushBack(DelayTime::create(.8));
        
        actions.pushBack(TargetedAction::create(cheki_3, FadeIn::create(.7)));
        actions.pushBack(DelayTime::create(.7));
    }

    //コールバック
    auto call = CallFunc::create([callback]{
        callback(true);
    });
    actions.pushBack(call);
    
#pragma mark - アクション終わりにコールバックを返さない
#pragma mark カメラ獲得
    if (key == "getcamera") {
        //コールバック削除
        actions.eraseObject(call);
        
        //callback
        auto storycall = CallFunc::create([parent
                                      ,this, callback]{
            auto story=StoryLayer::create("ending1", [parent, this, callback]{
                
                //皆んな、集まれ〜
                Vector<FiniteTimeAction*> gatherActions;
                auto back_16 = createSpriteToCenter("back_16.png", true, parent, true);
                auto anim_1 = createSpriteToCenter("sup_16_anim_1.png", true, back_16, true);
                auto anim_2 = createSpriteToCenter("sup_16_anim_2.png", true, back_16, true);
                auto anim_3 = createSpriteToCenter("sup_16_anim_3.png", true, back_16, true);
                auto pig = createSpriteToCenter("sup_16_pig.png", false, back_16, true);//豚追加
                
                gatherActions.pushBack(TargetedAction::create(back_16, FadeIn::create(.5)));
                gatherActions.pushBack(DelayTime::create(.5));
                gatherActions.pushBack(TargetedAction::create(anim_1, FadeIn::create(.5)));
                gatherActions.pushBack(this->createSoundAction("pig.mp3"));
                gatherActions.pushBack(DelayTime::create(.6));
                gatherActions.pushBack(this->createSoundAction("monkey.mp3"));
                gatherActions.pushBack(DelayTime::create(1.5));
                gatherActions.pushBack(this->createChangeAction(.4, .4, anim_1, anim_2, "pig_1.mp3"));
                gatherActions.pushBack(DelayTime::create(1.5));
                gatherActions.pushBack(this->createChangeAction(.4, .4, anim_2, anim_3, "pig.mp3"));
                gatherActions.pushBack(DelayTime::create(1.5));
                gatherActions.pushBack(TargetedAction::create(back_16, FadeOut::create(1.5)));
                gatherActions.pushBack(DelayTime::create(.5));
                
                //コールバック
                auto callbackAction = CallFunc::create([callback](){
                    callback(true);
                });
                gatherActions.pushBack(callbackAction);
                
                parent->runAction(Sequence::create(gatherActions));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        actions.pushBack(storycall);
    }
#pragma mark チェキセット
    else if (key == "setcheki") {
        //コールバック削除
        actions.eraseObject(call);
        
        actions.pushBack(useItemSoundAction());
        
        //チェキ表示
        auto cheki = createSpriteToCenter("sup_72_picture.png", true, parent, true);
        actions.pushBack(TargetedAction::create(cheki, FadeIn::create(.7)));
        actions.pushBack(DelayTime::create(.5));
        
        //ストーリー表示
        auto showStoryAction = CallFunc::create([parent, this](){
            auto story=StoryLayer::create("ending2", [parent, this]{
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        actions.pushBack(showStoryAction);
        
        
    }
    
    parent->runAction(Sequence::create(actions));
}

#pragma mark - touch began
void SchoolFesActionManager::touchBeganAction(std::string key, Node *parent)
{
}


#pragma mark - Drag
void SchoolFesActionManager::dragAction(std::string key, Node *parent, Vec2 pos)
{
    
}

#pragma mark - Item
void SchoolFesActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造 addする際はcreateSpriteOnClipを使用するとよい
    Vector<FiniteTimeAction*>actions;
#pragma mark 電池
    if (key.compare(0,7,"battery")==0) {
        Common::playSE("p.mp3");
        
        auto index=atoi(key.substr(8,1).c_str());
        
        auto battery=createSpriteToCenter(StringUtils::format("flashlight_battery_%d.png",index), true, parent->itemImage, true);
        
        
        //move
        actions.pushBack(TargetedAction::create(battery, Spawn::create(FadeIn::create(1),
                                                                          createSoundAction("kacha.mp3"),
                                                                          NULL)));
        
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(17)) {//電池2個目セットなら
            actions.pushBack(DelayTime::create(1));
            
            auto switch_sup = createSpriteToCenter("flashlight_switch.png", true, parent->itemImage, true);
            auto on = createSpriteToCenter("flashlight_on.png", true, parent->itemImage, true);
            actions.pushBack(Spawn::create(TargetedAction::create(switch_sup, FadeIn::create(1)),
                                           TargetedAction::create(on, FadeIn::create(1)),
                                           createSoundAction("kacha.mp3"), NULL));
            actions.pushBack(DelayTime::create(.5));
        }
        
        actions.pushBack(DelayTime::create(1));
    }
#pragma mark 木箱open
    if (key == "box") {
        auto box_close = createSpriteToCenter("box.png", true, parent->backImage, true);
        auto box_open = createSpriteToCenter("box_open.png", true, parent->backImage, true);
        
        actions.pushBack(DelayTime::create(1.5));
        actions.pushBack(createChangeAction(.5, .5, parent->itemImage, box_close));
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(createChangeAction(.5, .5, box_close, box_open, "gacha_2.mp3"));
        actions.pushBack(DelayTime::create(1.5));
    }
    
    //call
    auto call=CallFunc::create([callback](){
        callback(true);
    });
    actions.pushBack(call);
    
    parent->runAction(Sequence::create(actions));
}


#pragma mark - Custom
void SchoolFesActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    
}

#pragma mark - Private

FiniteTimeAction* SchoolFesActionManager::showHinotamaParticle(Node *parent, int backID)
{
    Vector<FiniteTimeAction*> actions;
    auto boxPos = Vec2(parent->getContentSize().width*.5, parent->getContentSize().height*.197);

    //火の玉を表示する。
    auto particle=ParticleSystemQuad::create("fire.plist");
    particle->setLife(.45);
    particle->setAngleVar(0);
    particle->setGravity(Vec2(0, 40));
    particle->setPosVar(Vec2(-12, 15));
    particle->setTotalParticles(0);
    //particle->setAutoRemoveOnFinish(true);
    particle->setPosition(boxPos);
    particle->setSpeed(175);
    particle->setSpeedVar(5);
    particle->setStartColor(Color4F(.2, .4, 1, 1));
    particle->setStartColorVar(Color4F(0, 0, .2, .1));
    particle->setStartSize(parent->getContentSize().width*.08);
    particle->setStartSizeVar(particle->getStartSize()*.4);
    
    parent->addChild(particle);
    
    if (backID != 81) {
        particle->setStartSize(particle->getStartSize()*.8);
        particle->setStartSizeVar(particle->getStartSize()*.4);
        particle->setLife(particle->getLife()*.8);

    }
    
    //パーティクルを表示。
    auto showParticle = CallFunc::create([particle]{
        particle->setDuration(-1);
        particle->setTotalParticles(110);
    });
    actions.pushBack(showParticle);
    actions.pushBack(DelayTime::create(.5));
    
    //パーティクルを気持ち上に移動
    actions.pushBack(TargetedAction::create(particle, MoveBy::create(.5, Vec2(0, (backID == 81)? parent->getContentSize().height*.1 : parent->getContentSize().height*.08))));
    actions.pushBack(DelayTime::create(.6));
    
    std::vector<Vec2> jizouPoses = {Vec2(.3, .802), Vec2(.433, .802), Vec2(.563, .802), Vec2(.698, .802), Vec2(.27, .615), Vec2(.423, .615), Vec2(.567, .615), Vec2(.73, .615)};
    
    if (backID != 81) {
        jizouPoses = {Vec2(.375, .512), Vec2(.458, .512), Vec2(.531, .512), Vec2(.614, .512), Vec2(.357, .392), Vec2(.452, .392), Vec2(.547, .392), Vec2(.642, .392)};
    }
    
    
    float firstMoveDuration = 1;
    std::vector<int> answers = {0, 4, 5, 6, 7, 3, 2, 1};
    for (auto answer : answers) {
        //火の玉を地蔵のとこへ移動
        auto jizouPos = jizouPoses[answer];
        actions.pushBack(TargetedAction::create(particle, MoveTo::create(.5+firstMoveDuration, Vec2(parent->getContentSize().width*jizouPos.x, parent->getContentSize().height*jizouPos.y))));
        firstMoveDuration = 0;
        
        //風車を回す
        auto wind = parent->getChildByName<Sprite*>(StringUtils::format("sup_%d_wind_%d.png", backID, answer));
        auto windRotate = Spawn::create(createSoundAction("hyu.mp3"),
                                        EaseInOut::create(RotateBy::create(2, 360), 2) ,NULL);
        actions.pushBack(TargetedAction::create(wind, windRotate));
        
        actions.pushBack(DelayTime::create(.4));
    }
    
    //パーティクルを消す。
    float fadeoutDuration = 1;
    auto fadeoutParticle = CallFunc::create([particle, fadeoutDuration]{
        particle->setDuration(fadeoutDuration);
    });
    actions.pushBack(fadeoutParticle);
    actions.pushBack(DelayTime::create(fadeoutDuration));
   
    //賽銭箱の位置に戻す
    auto moveParticle = CallFunc::create([particle, boxPos]{
        particle->setPosition(boxPos);
    });
    actions.pushBack(moveParticle);

    return Sequence::create(actions);
}

std::vector<Vec2> SchoolFesActionManager::getRokuroRousokuPoses(int backID)
{
    std::vector<Vec2> matchPoses;
    
    if (backID == 89) {
        matchPoses.push_back(Vec2(.338, .245));
        matchPoses.push_back(Vec2(.66, .245));
    }
    else if (backID == 90) {
        matchPoses.push_back(Vec2(.157, .48));
        matchPoses.push_back(Vec2(.85, .483));
    }

    return matchPoses;
}

ParticleSystemQuad* SchoolFesActionManager::showFireParticle(cocos2d::Node *parent, int backID, int num)
{
    auto rokuroPoses = getRokuroRousokuPoses(backID);
    auto matchPos = rokuroPoses.size()>0? rokuroPoses[num] : Vec2(.5, .4);
    
    auto particle=ParticleSystemQuad::create("fire.plist");
    particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
    particle->setAutoRemoveOnFinish(true);
    particle->setPosition(matchPos.x*parent->getContentSize().width, matchPos.y*parent->getContentSize().height);
    particle->setTotalParticles(40);
    particle->setLife(1);
    particle->setStartColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 150));
    //particle->setPosVar(Vec2(parent->getContentSize().width*.075, parent->getContentSize().height*.05));
    particle->setStartSize(parent->getContentSize().width*.075);
    particle->setSpeed(parent->getContentSize().height*.08);
    
    if (backID == 89) {
        particle->setTotalParticles(35);
        particle->setStartSize(particle->getStartSize()/2);
        particle->setLife(.5);
    }
    else if (backID == 91) {//お化け
        particle->setTotalParticles(120);
        particle->setStartSize(parent->getContentSize().width*.1);
        particle->setPosVar(Vec2(parent->getContentSize().width*.12, particle->getPosVar().y));
        particle->setLife(3);

        particle->setStartColor(Color4F(255, particle->getStartColor().g, 255, 150));
    }
    
    parent->addChild(particle);
    
    return particle;
}


FiniteTimeAction* SchoolFesActionManager::rokuroActions(Node *parent, std::vector<int> answers)
{
    Node* stencil = nullptr;
    auto clip = parent->getChildByName<ClippingNode*>("clip");
    if (clip == NULL) {
        auto clipImageName = "sup_89_neck_clip.png";//首
        stencil = AnchorSprite::create(Vec2(.5, 0), parent->getContentSize(), clipImageName);
        stencil->setScaleY(.262);//首までは見えるように
        auto clip=ClippingNode::create(stencil);
        clip->setName("clip");
        clip->setInverted(false);
        clip->setAlphaThreshold(0.0f);
        auto neck = createSpriteToCenter("sup_89_pig_neck.png", false, parent);
        clip->addChild(neck);
        parent->addChild(clip);
    }
    else {
        stencil = clip->getStencil();
    }
    
    //豚の頭
    auto pig_head = parent->getChildByName<Sprite*>("sup_89_pig_head.png");
    
    if (pig_head == NULL) {
        pig_head = parent->getChildByName<Sprite*>("sup_89_pig_head_1.png");
    }
    
    pig_head->setLocalZOrder(2);
    transformToAnchorSprite(parent, pig_head, Vec2(.5, .318));
    
    //首が伸びていく
    Vector<FiniteTimeAction*> rokuroActions;
    
    //頭動かす
    auto originalHeadPosX = pig_head->getPositionX();
    auto originalHeadPosY = pig_head->getPositionY();
    
    float adjustRatio = .5-pig_head->getAnchorPoint().y;//首と顔のアンカーポイントの違い
    auto headPosY = originalHeadPosY+parent->getContentSize().height*.083;
    auto moveSpawn = Spawn::create(TargetedAction::create(pig_head, MoveTo::create(.5, Vec2(pig_head->getPositionX(), headPosY))),
                                   TargetedAction::create(stencil, ScaleTo::create(.5, 1, headPosY/parent->getContentSize().height+adjustRatio/2)), NULL);
    rokuroActions.pushBack(EaseIn::create(moveSpawn, 1));
    
    //首が伸びる演出
    float nowHeadPosY = headPosY;//アニメーション途中のy座標
    for (int i = 0; i < answers.size(); i++) {
        int answer = answers[i];
        
        int delta = i==0? answer: answer-answers[i-1];
        float duration = 1.5;
        int absoluteRepeat = abs(delta);
        
        //コンフィグ作成
        ccBezierConfig config;
        float xDelta = .1;
        float endDelta = .205;
        float yDelta = endDelta/4;
        
        Vector<FiniteTimeAction*> headNeckActions;
        for (int l = 0; l<absoluteRepeat; l++) {
            if (delta > 0) {//上に伸びる
                config.controlPoint_1 = Vec2(originalHeadPosX+xDelta*parent->getContentSize().width, nowHeadPosY+yDelta*parent->getContentSize().height);
                config.controlPoint_2 = Vec2(originalHeadPosX-xDelta*parent->getContentSize().width, nowHeadPosY+3*yDelta*parent->getContentSize().height);
                config.endPosition = Vec2(originalHeadPosX, nowHeadPosY+endDelta*parent->getContentSize().height);
            }
            else {//下に縮む
                config.controlPoint_1 = Vec2(originalHeadPosX-xDelta*parent->getContentSize().width, nowHeadPosY-yDelta*parent->getContentSize().height);
                config.controlPoint_2 = Vec2(originalHeadPosX+xDelta*parent->getContentSize().width, nowHeadPosY-3*yDelta*parent->getContentSize().height);
                config.endPosition = Vec2(originalHeadPosX, nowHeadPosY-endDelta*parent->getContentSize().height);
            }
            
            //首を回転
            float rotate = 10;
            float dur = duration/8;
            Vector<FiniteTimeAction*> headRotateActions;
            headRotateActions.pushBack(EaseOut::create(RotateTo::create(dur, rotate), 1.5));
            headRotateActions.pushBack(EaseOut::create(RotateTo::create(dur, 0), 1.5));
            headRotateActions.pushBack(EaseOut::create(RotateTo::create(dur*2, -rotate), 1.5));
            headRotateActions.pushBack(EaseOut::create(RotateTo::create(dur*2, 0), 1.5));
            headRotateActions.pushBack(EaseOut::create(RotateTo::create(dur, rotate), 1.5));
            headRotateActions.pushBack(EaseOut::create(RotateTo::create(dur, 0), 1.5));
            
            nowHeadPosY = config.endPosition.y;
            
            //アンカーポイントが異なる分、調整
            float neckScaleY = nowHeadPosY/parent->getContentSize().height*.93+adjustRatio;
            
            auto headSpawn = Spawn::create(BezierTo::create(duration, config), Sequence::create(headRotateActions), NULL);
            auto headAction = TargetedAction::create(pig_head, headSpawn);
            auto neckAction = TargetedAction::create(stencil, ScaleTo::create(duration, 1, neckScaleY));
            headNeckActions.pushBack(EaseInOut::create(Spawn::create(headAction, neckAction, NULL), 1.5));
        }
        
        auto headAction = Sequence::create(headNeckActions);
        rokuroActions.pushBack(headAction);
        
        //停止位置で顔を振る
        rokuroActions.pushBack(DelayTime::create(.3));
        
        auto shakeSeq = EaseOut::create(Sequence::create(RotateTo::create(.1, 5),
                                                         RotateTo::create(.1, -5),
                                                         RotateTo::create(.1, 0), NULL), 2);
        rokuroActions.pushBack(TargetedAction::create(pig_head, Repeat::create(shakeSeq, 3)));
        
        rokuroActions.pushBack(DelayTime::create(.3));
    }
    
    auto neckBack = TargetedAction::create(stencil, ScaleTo::create(.5, 1, originalHeadPosY/parent->getContentSize().height));
    auto headBack = TargetedAction::create(pig_head, MoveTo::create(.5, Vec2(pig_head->getPositionX(), originalHeadPosY)));
    rokuroActions.pushBack(EaseInOut::create(Spawn::create(neckBack, headBack, NULL), 2));
    rokuroActions.pushBack(CallFunc::create([stencil](){stencil->setScaleY(0.2);}));//なぜかstencil残る
    auto rokuroSeq = Sequence::create(rokuroActions);
    
    return rokuroSeq;
}
