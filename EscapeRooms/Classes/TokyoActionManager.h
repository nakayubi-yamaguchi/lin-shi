//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__TokyoActionManager__
#define __EscapeContainer__TokyoActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class TokyoActionManager :public CustomActionManager
{
public:
    static TokyoActionManager* manager;
    static TokyoActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void startWheel(bool willStart,Node*parent,const onFinished& callback);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    
    void blindAction(Node*parent,const onFinished& callback);
    
    void customAction(std::string key,Node*parent);

private:
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
