//
//  RotateSprite.h
//  Yashiki
//
//  Created by 成田凌平 on 2017/04/13.
//
//

#ifndef __Yashiki__RotateSprite__
#define __Yashiki__RotateSprite__

#include "cocos2d.h"
#include "AnchorSprite.h"

USING_NS_CC;
class RotateSprite:public AnchorSprite
{
public:
    static RotateSprite*create(std::string name,Size parentSize,ValueMap map,Value answerV,int number,std::string temporaryKey);
    bool init(std::string name,Size parentSize,ValueMap map,Value answerV,int number,std::string temporaryKey);
        
    ValueMap autoReverseMap;
    CC_SYNTHESIZE(int, number, Number)//何番目のspriteか
    CC_SYNTHESIZE(float, duration,Duration);//duration
    CC_SYNTHESIZE(std::string, se,SE);//効果音
    CC_SYNTHESIZE(float, angle, Angle);
    CC_SYNTHESIZE(int, index, Index);
    CC_SYNTHESIZE(int, maxIndex, MaxIndex);
    CC_SYNTHESIZE(bool, memory, Memory);
    CC_SYNTHESIZE(std::string, filename, Filename);//保存用キー
    CC_SYNTHESIZE(std::string, flashFileName, FlashFileName);//点滅させるスプライト名
    CC_SYNTHESIZE(std::string, CustomFlagKey, CustumFlagKey);//フラグを読み込むキーを指定。(Optional)
    CC_SYNTHESIZE(bool, disable, Disable);//必要フラグ不足による非表示
    
    void rotate(ccMenuCallback callback);
    
    std::string getTemporaryKey(std::string name);
    void save();
private:

};

#endif /* defined(__Yashiki__RotateSprite__) */
