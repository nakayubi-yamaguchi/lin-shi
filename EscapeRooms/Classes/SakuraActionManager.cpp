//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "SakuraActionManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"

using namespace cocos2d;

SakuraActionManager* SakuraActionManager::manager =NULL;

SakuraActionManager* SakuraActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new SakuraActionManager();
    }
    return manager;
}

#pragma mark - Back
void SakuraActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="back_66")
    {//もちつき
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(4)) {
            auto rabbit=parent->getChildByName("sup_66_rabbit_0.png");
            auto rope=parent->getChildByName("sup_66_rope.png");
            auto rope_1=parent->getChildByName("sup_66_rope_1.png");

            auto distance=parent->getContentSize().width*.005;
            auto duration=.1;
            auto rate=1.5;
            
            auto shake=Sequence::create(EaseInOut::create(MoveBy::create(duration, Vec2(distance, 0)), rate),
                                        EaseInOut::create(MoveBy::create(duration*2, Vec2(-distance*2, 0)), rate),
                                        EaseInOut::create(MoveBy::create(duration, Vec2(distance, 0)), rate),
                                        DelayTime::create(1), NULL);
            auto spawn=Spawn::create(
                                     TargetedAction::create(rope, shake),
                                     TargetedAction::create(rope_1, shake->clone()), NULL);
            parent->runAction(Repeat::create(spawn, UINT_MAX));
            rabbit->runAction(Repeat::create(shake->clone(),UINT_MAX));
        }
    }
    else if(key=="back_18"&&DataManager::sharedManager()->getEnableFlagWithFlagID(10))
    {//コルク
        auto doll_0=parent->getChildByName("sup_18_0.png");
        auto doll_1=parent->getChildByName("sup_18_1.png");
        
        auto distance=parent->getContentSize().width*.025;
        auto height=parent->getContentSize().height*.025;
        auto duration=.2;
        auto angle=3;
        
        auto swing_0=Repeat::create(swingAction(angle, .1, doll_0, 1.5), 2);
        auto move_0=TargetedAction::create(doll_0, Sequence::create(swing_0,
                                                                    createSoundAction("pi_2.mp3"),
                                                                    JumpBy::create(duration, Vec2(distance, 0), height, 1),
                                                                    DelayTime::create(.5), NULL));
        auto swing_1=Repeat::create(swingAction(angle, .1, doll_1, 1.5), 2);
        auto move_1=TargetedAction::create(doll_1, Sequence::create(swing_1,
                                                                    createSoundAction("pi_2.mp3"),
                                                                    JumpBy::create(duration, Vec2(-distance, 0), height, 1),
                                                                    DelayTime::create(.5), NULL));
        auto move_last=TargetedAction::create(doll_1, Sequence::create(swing_1->clone(),
                                                                       createSoundAction("pi_2.mp3"),
                                                                    JumpBy::create(duration, Vec2(-distance*2, 0), height, 1),
                                                                       createSoundAction("hit.mp3"),
                                                                     NULL));
        auto lose=TargetedAction::create(doll_0, Spawn::create(RotateBy::create(.2, -90),
                                                                  JumpBy::create(duration, Vec2(-distance*5, 0), height*3, 1),
                                                               DelayTime::create(.5), NULL));
        parent->runAction(Sequence::create(DelayTime::create(1),move_0,move_0->clone(),move_1,move_1->clone(),move_0->clone(),DelayTime::create(.5),move_last,lose, NULL));
    }
    else if(key=="windmill"){
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(26))
        {//風車
            Vector<FiniteTimeAction*>actions;
            for (int i=0; i<2; i++) {
                auto mill=parent->getChildByName(StringUtils::format("sup_%d_wind_%d.png",backID,i));
                
                actions.pushBack(TargetedAction::create(mill, Sequence::create(EaseInOut::create(RotateBy::create(3, 720), 2),
                                                                               DelayTime::create(1), NULL)));
            }
            
            parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),actions.at(1),actions.at(0),Spawn::create(actions),actions.at(1)->clone(),actions.at(0)->clone(),DelayTime::create(1), NULL), UINT_MAX));
        }
    }
    else if(key=="back_21"){
        //かえる
        auto action_flog=[this,parent](int index){
            
            auto flog=parent->getChildByName<Sprite*>(StringUtils::format("sup_21_%d.png",index));
            
            auto call=CallFunc::create([flog,index,parent]{
                Common::playSE("flog.mp3");

                auto speed=parent->getContentSize().height/3;
                auto startSize=parent->getContentSize().width*.1;
                
                auto particle=ParticleSystemQuad::create("particle_music.plist");
                particle->setAutoRemoveOnFinish(true);
                particle->setPosition(flog->getPosition().x,parent->getContentSize().height*.65);
                particle->setSpeed(speed);
                particle->setStartSize(startSize);
                particle->setAngle(120);
                particle->setGravity(Vec2(0, -parent->getContentSize().height*.3));
                particle->setStartColor(Color4F(0, 1, .2, 1));
                particle->setEndColor(Color4F(0, 1, .2, 0));
                
                parent->addChild(particle);
            });
            
            
            auto bounce=createBounceAction(flog, .3);
            
            return Sequence::create(Spawn::create(call,bounce, NULL),DelayTime::create(1), NULL);
        };
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),action_flog(0),action_flog(1),action_flog(2),action_flog(1)->clone(),action_flog(0)->clone(),DelayTime::create(1), NULL), UINT_MAX));
    }
    else if(key=="back_76")
    {//囲炉裏
        if(DataManager::sharedManager()->getEnableFlagWithFlagID(34)){
            auto particle=createFire(parent, parent->getContentSize().width*.05, Vec2(parent->getContentSize().width*.55, parent->getContentSize().height*.45));
            
            parent->addChild(particle);
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(35)&&
                !DataManager::sharedManager()->getEnableFlagWithFlagID(36)) {
                auto size=parent->getContentSize().width*.04;
                auto smoke=ParticleSystemQuad::create("smoke.plist");
                smoke->resetSystem();
                smoke->setTotalParticles(30);
                smoke->setPosVar(Vec2(0, 0));
                smoke->setAutoRemoveOnFinish(true);
                smoke->setPosition(parent->getContentSize().width*.675, parent->getContentSize().height*.575);
                smoke->setLocalZOrder(2);
                smoke->setStartSize(size);
                smoke->setEndSize(size*4);
                parent->addChild(smoke);
            }
        }
    }
    else if(key=="back_70"&&!DataManager::sharedManager()->getEnableFlagWithFlagID(43)){
        auto baloon=AnchorSprite::create(Vec2(.458, .70), parent->getContentSize(), "sup_70_baloon.png");
        baloon->setScale(.5);
        parent->addChild(baloon);
    }
    else if(key=="back_59"){
        //月夜桜
        auto sakura=[parent](int index){
            auto call=CallFunc::create([parent,index]{
                auto particle=ParticleSystemQuad::create("particle_texture.plist");
                particle->setAutoRemoveOnFinish(true);
                particle->resetSystem();
                particle->setDuration(1.5);
                particle->setTotalParticles(70);
                particle->setTexture(Sprite::create("sakura_shade.png")->getTexture());
                particle->setStartSize(parent->getContentSize().width*.05);
                particle->setStartSizeVar(parent->getContentSize().width*.005);
                particle->setEndSize(particle->getStartSize());
                particle->setSpeed(parent->getContentSize().height*.25);
                particle->setSpeedVar(particle->getStartSize());
                particle->setPosition(parent->getContentSize().width*index,parent->getContentSize().height*.85);
                particle->setAngle(-30-120*index);
                particle->setGravity(Vec2(0,-parent->getContentSize().height*.05));
                particle->setAngleVar(0);
                particle->setPosVar(Vec2(parent->getContentSize().width*.2, parent->getContentSize().height*.35));
                parent->addChild(particle);
            });
            
            return Sequence::create(call,DelayTime::create(6), NULL);
        };
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),sakura(0),sakura(0),sakura(1),sakura(0),sakura(1),sakura(1),DelayTime::create(2), NULL), UINT_MAX));
    }
    else if(key=="back_19"&&DataManager::sharedManager()->isPlayingMinigame()){
        auto rope=createSpriteToCenter("sup_19_rope_0.png",false, parent);
        parent->addChild(rope);
    }
    else if(key=="back_26"&&DataManager::sharedManager()->isPlayingMinigame()){
        auto rope=createSpriteToCenter("sup_26_rope_0.png",false, parent);
        parent->addChild(rope);
        auto rope_1=createSpriteToCenter("sup_26_rope_1.png",true, parent);
        parent->addChild(rope_1);
        
        auto mistake=parent->getChildByName("sup_26_mistake.png");
        mistake->setOpacity(0);
        
        auto delay=DelayTime::create(1);
        auto sound=createSoundAction("weeen.mp3");
        auto delay_1=DelayTime::create(1);
        auto change=createChangeAction(1.5, 1, rope, rope_1);
        auto sound_1=createSoundAction("nyu.mp3");
        auto fadein=TargetedAction::create(mistake, FadeIn::create(.5));
        
        auto call=CallFunc::create([]{
            DataManager::sharedManager()->setTemporaryFlag("mistake1", 1);
        });
        mistake->runAction(Sequence::create(delay,sound,delay_1,change,sound_1,fadein,call, NULL));
    }
}

#pragma mark - Touch
void SakuraActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="sword"){
        Common::playSE("p.mp3");
        
        auto sword=AnchorSprite::create(Vec2(.17, .825), parent->getContentSize(), "sup_66_sword.png");
        sword->setOpacity(0);
        sword->setLocalZOrder(1);
        parent->addChild(sword);
        
        auto rabbit_0=parent->getChildByName<Sprite*>("sup_66_rabbit_0.png");
        auto rabbit_1=createSpriteToCenter("sup_66_rabbit_1.png", true, parent);
        rabbit_1->setLocalZOrder(3);
        parent->addChild(rabbit_1);
        
        auto rope=parent->getChildByName("sup_66_rope.png");
        auto rope_1=parent->getChildByName("sup_66_rope_1.png");
        
        auto rice=parent->getChildByName<Sprite*>("sup_66_rice.png");
        auto rice_cake=createSpriteToCenter("sup_66_ricecake.png", true, parent);
        rice_cake->setLocalZOrder(rice->getLocalZOrder());
        parent->addChild(rice_cake);
        
        auto fadein=FadeIn::create(1);
        auto delay=DelayTime::create(.7);
        auto rotate_0=RotateBy::create(1.2, -15);
        auto move_0=MoveBy::create(1.2, Vec2(0, parent->getContentSize().height*.1));
        auto rotate_1=RotateBy::create(.2, 45);
        auto move_1=MoveBy::create(.2, Vec2(0, -parent->getContentSize().height*.5));
        
        auto seq_sword=TargetedAction::create(sword, Sequence::create(fadein,
                                                                      delay,
                                                                      Spawn::create(rotate_0,move_0, NULL),
                                                                      delay->clone(),
                                                                      createSoundAction("sword.mp3"),
                                                                      EaseIn::create(Spawn::create(rotate_1,move_1,NULL), 1.5) ,
                                                                      NULL));
        auto stop_rabbit=Sequence::create(CallFunc::create([rabbit_0]{rabbit_0->stopAllActions();}),
                                          TargetedAction::create(rabbit_0, MoveTo::create(.1, parent->getContentSize()/2)), NULL);
        auto fadeouts_rope=Spawn::create(TargetedAction::create(rope, FadeOut::create(.3)),
                                         TargetedAction::create(rope_1, FadeOut::create(.3)), NULL);
        auto delay_1=DelayTime::create(.5);
        auto fadeout_sword=TargetedAction::create(sword, FadeOut::create(.5));
        
        auto anim=Repeat::create(Sequence::create(createSoundAction("hit_1.mp3"),
                                                  createChangeAction(.3, .15, rabbit_0, rabbit_1),
                                                  createChangeAction(.3, .15, rabbit_1, rabbit_0),
                                                  DelayTime::create(.2),NULL), 5);
        auto anim_rice=Sequence::create(DelayTime::create(2.5),createChangeAction(3, 2, rice, rice_cake), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_sword,stop_rabbit,fadeouts_rope,delay_1,fadeout_sword,delay_1->clone(),Spawn::create(anim,anim_rice, NULL),call, NULL));
    }
    else if(key=="hammer"){
        Common::playSE("p.mp3");
        
        auto item=createSpriteToCenter("sup_61_hammer.png", true, parent);
        parent->addChild(item);
        
        auto mouse_0=parent->getChildByName<Sprite*>("sup_61_mouse_0.png");
        auto mouse_1=createSpriteToCenter("sup_61_mouse_1.png", true, parent);
        parent->addChild(mouse_1);
        auto mouse_2=createSpriteToCenter("sup_61_mouse_2.png", true, parent);
        parent->addChild(mouse_2);
        auto mouse_3=createSpriteToCenter("sup_61_mouse_3.png", true, parent);
        parent->addChild(mouse_3);
        
        auto distance_jump=parent->getContentSize().width*.025;
        auto height_jump=parent->getContentSize().height*.05;
        
        auto seq_hammer=TargetedAction::create(item,Sequence::create(Spawn::create(FadeIn::create(1),ScaleBy::create(2, .9), NULL),
                                                                     FadeOut::create(.5), NULL));
        auto change_mouse=createChangeAction(1, .7, mouse_0, mouse_1);
        auto delay=DelayTime::create(.7);
        auto change_mouse_1=createChangeAction(1, .7, mouse_1, mouse_2);
        
        auto shot_mouse=TargetedAction::create(mouse_2, Sequence::create(createSoundAction("nyu.mp3"),
                                                                         MoveBy::create(.15, Vec2(-distance_jump, 0)),
                                                                         DelayTime::create(.3),
                                                                         JumpBy::create(.2, Vec2(distance_jump, 0), height_jump, 1),
                                                                         createChangeAction(.2, .1, mouse_2, mouse_3),NULL));
        auto shot_block=[this,parent,mouse_2,mouse_3](int index){
            
            auto block=parent->getChildByName(StringUtils::format("sup_61_%d.png",index));
            auto move=TargetedAction::create(block, MoveBy::create(.2, Vec2(parent->getContentSize().width, 0)));
            auto sound=createSoundAction("hit.mp3");
            
            Vector<FiniteTimeAction*>actions;
            auto fall_distance=parent->getContentSize().height*.08;
            for (int i=index+1; i<6; i++) {
                auto block_fall=parent->getChildByName(StringUtils::format("sup_61_%d.png",i));
                auto fall=TargetedAction::create(block_fall, Sequence::create(EaseIn::create(MoveBy::create(.1, Vec2(0, -fall_distance)), 1.5),
                                                                              this->jumpAction(fall_distance*.1, .1, block_fall, 1.5), NULL));
                actions.pushBack(fall);
            }
            
            if (actions.size()>0) {
                return Sequence::create(sound,move,Spawn::create(actions), NULL);
            }
            else{
                return Sequence::create(sound,move, NULL);
            }
        };
        
        auto replace=createChangeAction(.3, .2, mouse_3, mouse_2);
        
        Vector<FiniteTimeAction*>shots;
        for (int i=0; i<6; i++) {
            auto shot=Sequence::create(shot_mouse,shot_block(i),replace, NULL);
            shots.pushBack(shot);
        }
        
        auto change_final=createChangeAction(1, .7, mouse_2, mouse_0);

        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_hammer,change_mouse,delay,change_mouse_1,Sequence::create(shots),change_final,delay->clone(),call, NULL));
    }
    else if(key=="bamboo"){
        Common::playSE("p.mp3");
        
        auto sword=AnchorSprite::create(Vec2(.2, .5), parent->getContentSize(), "sup_22_sword.png");
        sword->setOpacity(0);
        parent->addChild(sword);
        
        auto bamboo_0=parent->getChildByName<Sprite*>("sup_22_0_0.png");
        auto bamboo_1=parent->getChildByName<Sprite*>("sup_22_1_0.png");

        auto water_0=createSpriteToCenter("sup_22_water_0.png", true, parent);
        parent->addChild(water_0);
        auto water_1=createSpriteToCenter("sup_22_water_1.png", true, parent);
        parent->addChild(water_1);
        
        auto fadein=FadeIn::create(1);
        auto delay=DelayTime::create(.7);
        auto rotate_0=RotateBy::create(1.2, -15);
        auto move_0=MoveBy::create(1.2, Vec2(0, parent->getContentSize().height*.1));
        auto rotate_1=RotateBy::create(.1, 45);
        auto move_1=MoveBy::create(.1, Vec2(0, -parent->getContentSize().height*.35));
        
        auto seq_sword=TargetedAction::create(sword, Sequence::create(fadein,
                                                                      delay,
                                                                      Spawn::create(rotate_0,move_0, NULL),
                                                                      delay->clone(),
                                                                      createSoundAction("sword.mp3"),
                                                                      EaseIn::create(Spawn::create(rotate_1,move_1,NULL), 1.5) ,
                                                                      NULL));
        auto delay_1=DelayTime::create(.7);
        auto remove_bamboo=TargetedAction::create(bamboo_0, Spawn::create(MoveBy::create(.5, Vec2(parent->getContentSize().width*.05, -parent->getContentSize().height*.05)),
                                                                          FadeOut::create(.5), NULL));
        auto remove_sword=TargetedAction::create(sword, FadeOut::create(.7));
        
        //水0
        auto fadein_water=Spawn::create(TargetedAction::create(water_0, FadeIn::create(1)),
                                        createSoundAction("jaa.mp3"), NULL);
        auto delay_2=DelayTime::create(2);
        auto fadein_water_1=Spawn::create(createSoundAction("jaa.mp3"),
                                          TargetedAction::create(water_0, FadeIn::create(1)),
                                         createChangeAction(1, .7, water_0, water_1),
                                         TargetedAction::create(bamboo_1, FadeOut::create(1)), NULL);
        auto fadeout_kettles=Spawn::create(createSoundAction("bamboo.mp3"),
                                          createChangeAction(1, .7, water_1, bamboo_1), NULL);
        
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_sword,delay_1,remove_bamboo,remove_sword,fadein_water,delay_2,fadein_water_1,delay_2->clone(),fadeout_kettles,delay_1->clone(),call, NULL));
    }
    else if(key=="firewood")
    {
        Common::playSE("p.mp3");
        
        auto item=createSpriteToCenter("sup_76_firewood.png", true, parent);
        parent->addChild(item);
        
        //fadein
        auto fadein=TargetedAction::create(item, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,call,NULL));
    }
    else if(key=="plug")
    {
        Common::playSE("p.mp3");
        
        auto item=createSpriteToCenter("sup_61_plug.png", true, parent);
        parent->addChild(item);
        
        //fadein
        auto fadein=TargetedAction::create(item, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,call,NULL));
    }
    else if(key.compare(0,7,"omikuji")==0){
        Common::playSE("p.mp3");
        
        auto index=atoi(key.substr(8,1).c_str());
        
        auto coin=createSpriteToCenter("anim_63_coin.png", true, parent);
        parent->addChild(coin);
        
        auto back=createSpriteToCenter("anim_63_back.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        auto item=createSpriteToCenter(StringUtils::format("anim_63_%d.png",index), true, parent);
        item->setPositionY(parent->getContentSize().height*.6);
        back->addChild(item);
        
        auto box=createSpriteToCenter("anim_63_box.png", false, parent);
        back->addChild(box);
        
        //animation
        auto fadein_coin=TargetedAction::create(coin, FadeIn::create(.5));
        auto delay=DelayTime::create(.7);
        
        auto vec_coin=Vec2(-parent->getContentSize().width*.02, parent->getContentSize().height*.075);
        auto seq_coin=TargetedAction::create(coin, Sequence::create(MoveBy::create(.5, vec_coin),
                                                                    Spawn::create(createSoundAction("chalin.mp3"),FadeOut::create(.5), NULL), NULL));
        
        //scene change
        auto delay_1=DelayTime::create(1);
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        
        auto shake_height=parent->getContentSize().height*.02;
        auto shake=Sequence::create(createSoundAction("sa.mp3"),
                                    Repeat::create(jumpAction(shake_height, .1, box, 1.2), 3),
                                    DelayTime::create(.5), NULL);
        auto seq_shake=TargetedAction::create(box, Repeat::create(shake, 2));
        
        auto getItem=TargetedAction::create(item, Spawn::create(MoveTo::create(.2, parent->getContentSize()/2),
                                                                FadeIn::create(0),
                                                                createSoundAction("kata.mp3"), NULL));
        
        //scene change
        auto fadeout=TargetedAction::create(back, FadeOut::create(1));
        
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_coin,delay,seq_coin,delay_1,fadein_back,seq_shake,getItem,delay_1->clone(),fadeout,call,NULL));
    }
    else if(key=="chasen")
    {//茶筅
        Common::playSE("p.mp3");
        
        auto item=createSpriteToCenter("anim_47_chasen_0.png", true, parent);
        parent->addChild(item);
        
        auto tea=createSpriteToCenter("sup_47_tea.png", true, parent);
        parent->addChild(tea);
        
        auto pig=parent->getChildByName<Sprite*>("sup_47_pig.png");
        pig->setLocalZOrder(1);
        
        Vector<SpriteFrame*>frames;
        for(int i=1;i<3;i++){
            auto s=Sprite::createWithSpriteFrameName(StringUtils::format("anim_47_chasen_%d.png",i));
            frames.pushBack(s->getSpriteFrame());
        }
        
        auto animation=Animation::createWithSpriteFrames(frames);
        animation->setDelayPerUnit(6.0f/60.0f);//??フレームぶん表示
        animation->setRestoreOriginalFrame(true);
        animation->setLoops(20);
        
        
        auto fadein=TargetedAction::create(item, FadeIn::create(1));
        auto delay=DelayTime::create(.7);
        auto fadeout=TargetedAction::create(item, FadeOut::create(.7));
        //auto change_0=createChangeAction(1, .5, pig, anim_1);
        auto sound=createSoundAction("pig.mp3");
        auto shake=TargetedAction::create(pig, Animate::create(animation));
        auto change_1=Spawn::create(//TargetedAction::create(anim_1, FadeOut::create(1)),
                                    //TargetedAction::create(anim_2, FadeOut::create(1)),
                                    TargetedAction::create(tea, FadeIn::create(1)),
                                    TargetedAction::create(pig, FadeIn::create(.5)), NULL);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,fadeout,delay->clone(),sound,shake,change_1,delay->clone(),call, NULL));
    }
#pragma mark マッチ
    else if(key=="match")
    {
        Common::playSE("p.mp3");
        
        auto match=createSpriteToCenter("sup_76_match.png", true, parent);
        parent->addChild(match);
        
        //animation
        auto seq_match=TargetedAction::create(match, Sequence::create(FadeIn::create(.5),
                                                                      MoveBy::create(1, Vec2(parent->getContentSize().width*.1, 0)),
                                                                    DelayTime::create(.5),
                                                                      Repeat::create(jumpAction(parent->getContentSize().height*.02, .1, match, 1.5), 2),
                                                                      createSoundAction("huo.mp3"),NULL));
        
        //fire
        auto fire=CallFunc::create([this,parent]{
            auto particle=createFire(parent, parent->getContentSize().width*.05, Vec2(parent->getContentSize().width*.55, parent->getContentSize().height*.45));
            
            parent->addChild(particle);
        });
        
        auto fadeout=TargetedAction::create(match, FadeOut::create(.5));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_match,fire,fadeout,DelayTime::create(1),call,NULL));
    }
    else if(key=="well")
    {//井戸
        auto back=createSpriteToCenter("back_26.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        std::vector<std::string>supnames={"sup_26_rope_0.png"};
        if(!DataManager::sharedManager()->getEnableFlagWithFlagID(17)){
            supnames.push_back("sup_26_firewood.png");
        }
        
        for (auto name : supnames) {
            auto sup=createSpriteToCenter(name, false, parent);
            back->addChild(sup);
        }
        
        auto fadein=TargetedAction::create(back, FadeIn::create(1.5));
        
        Vector<FiniteTimeAction*>actions;
        auto duration_anim=1;
        for (int i=0; i<4; i++) {
            auto frame=createSpriteToCenter(StringUtils::format("anim_26_%d.png",i), true, parent);
            frame->setLocalZOrder(1);
            back->addChild(frame);
            
            if (i==0) {
                auto rope=createSpriteToCenter("sup_26_rope_1.png", true, parent);
                back->addChild(rope);
                
                actions.pushBack(createSoundAction("weeen.mp3"));
                actions.pushBack(Spawn::create(TargetedAction::create(frame, FadeIn::create(duration_anim)),
                                               TargetedAction::create(rope, FadeIn::create(duration_anim)), NULL));
            }
            else{
                auto frame_before=back->getChildByName<Sprite*>(StringUtils::format("anim_26_%d.png",i-1));
                auto change=TargetedAction::create(frame, createChangeAction(duration_anim*1.5, duration_anim, frame_before, frame));
                if (i==1) {
                    actions.pushBack(Spawn::create(createSoundAction("jump.mp3"),
                                                   change, NULL));
                }
                else if (i==2) {
                    actions.pushBack(Spawn::create(createSoundAction("pig.mp3"),
                                                   change, NULL));
                }
                else if (i==3) {
                    actions.pushBack(Spawn::create(createSoundAction("horce_run.mp3"),
                                                   change, NULL));
                }
            }
        }
        
        auto seq_frame=Sequence::create(actions);
        auto fadeout=TargetedAction::create(back->getChildByName("anim_26_3.png"), FadeOut::create(1));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,DelayTime::create(.6),seq_frame,fadeout,call,NULL));
    }
    else if(key=="handle"){
        Common::playSE("p.mp3");
        
        auto handle=AnchorSprite::create(Vec2(.5, .2727), parent->getContentSize(), "sup_71_handle.png");
        handle->setOpacity(0);
        parent->addChild(handle);
        
        auto sano_0=parent->getChildByName<Sprite*>("sup_71_sano_0.png");
        auto sano_1=createSpriteToCenter("sup_71_sano_1.png", true, parent);
        parent->addChild(sano_1);
        auto sano_2=createSpriteToCenter("sup_71_sano_2.png", true, parent);
        parent->addChild(sano_2);
        
        auto back=createSpriteToCenter("back_70.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        std::vector<std::string>names={"sup_70_rope.png","sup_70_handle.png","sup_70_sano_0.png"};
        for (int i=0; i<5; i++) {
            names.push_back(StringUtils::format("sup_70_monkey_%d_0.png",i));
            
            for (int j=1; j<4; j++) {
                auto monkey_pause=createSpriteToCenter(StringUtils::format("sup_70_monkey_%d_%d.png",i,j), true, parent);
                back->addChild(monkey_pause);
            }
        }
        
        for (auto name : names) {
            auto sup=createSpriteToCenter(name, false, parent);
            back->addChild(sup);
        }
        
        Vector<FiniteTimeAction*>monkey_actions;
        auto duration_in=.2;
        auto duration_out=.375;
        for (int i=0; i<5; i++) {
            auto monkey_0=back->getChildByName<Sprite*>(StringUtils::format("sup_70_monkey_%d_0.png",i));
            auto monkey_1=back->getChildByName<Sprite*>(StringUtils::format("sup_70_monkey_%d_1.png",i));
            auto monkey_2=back->getChildByName<Sprite*>(StringUtils::format("sup_70_monkey_%d_2.png",i));
            auto monkey_3=back->getChildByName<Sprite*>(StringUtils::format("sup_70_monkey_%d_3.png",i));

            auto change_0=createChangeAction(duration_out, duration_in, monkey_0, monkey_1);
            auto change_1=createChangeAction(duration_out, duration_in, monkey_1, monkey_2);
            auto change_2=createChangeAction(duration_out, duration_in, monkey_2, monkey_3);
            auto change_3=createChangeAction(duration_out, duration_in, monkey_3, monkey_0);
            auto change_last=createChangeAction(duration_out, duration_in, monkey_0, monkey_3);
            monkey_actions.pushBack(Sequence::create(Repeat::create(Sequence::create(change_0,change_1,change_2,change_3, NULL), 5),
                                                     change_last, NULL));
        }
        
        for (int i=1; i<3; i++) {
            auto sano=createSpriteToCenter(StringUtils::format("sup_70_sano_%d.png",i), true, parent);
            back->addChild(sano);
        }
        auto sano_1_0=back->getChildByName<Sprite*>("sup_70_sano_0.png");
        auto sano_1_1=back->getChildByName<Sprite*>("sup_70_sano_1.png");
        auto sano_1_2=back->getChildByName<Sprite*>("sup_70_sano_2.png");

        auto change_sano_0=createChangeAction(duration_out, duration_in, sano_1_0, sano_1_1);
        auto change_sano_1=createChangeAction(duration_out, duration_in, sano_1_1, sano_1_2);
        auto change_sano_2=createChangeAction(duration_out, duration_in, sano_1_2, sano_1_1);
        auto change_sano_3=createChangeAction(duration_out, duration_in, sano_1_1, sano_1_0);
        
        auto sano_action=Repeat::create(Sequence::create(change_sano_0,change_sano_1,change_sano_2,change_sano_3, NULL), 4);
        auto sakura=createSpriteToCenter("sup_70_sakura.png", true, back);
        back->addChild(sakura);
        
        auto baloon=AnchorSprite::create(Vec2(.458, .705), parent->getContentSize(), "sup_70_baloon.png");
        back->addChild(baloon);
        auto scale_original=baloon->getScale();
        baloon->setScale(.5);
        
        auto fadein=TargetedAction::create(handle, FadeIn::create(.7));
        auto seq_handle=TargetedAction::create(handle,Repeat::create(Sequence::create(createSoundAction("gigigi.mp3"),
                                                                                      EaseInOut::create(RotateBy::create(2, 720), 1.5), NULL), 2));
        auto delay=DelayTime::create(.7);
        
        duration_out=.3;
        duration_in=.15;
        auto anim_0=createChangeAction(duration_out, duration_in, sano_0, sano_1);
        auto anim_1=createChangeAction(duration_out, duration_in, sano_1, sano_2);
        auto anim_2=createChangeAction(duration_out, duration_in, sano_2, sano_0);

        auto seq_sano=Repeat::create(Sequence::create(createSoundAction("kacha.mp3"),anim_0,anim_1,anim_2, NULL), 4);//up
        auto fadein_back=TargetedAction::create(back, Sequence::create(DelayTime::create(duration_out*6),FadeIn::create(duration_out*4), NULL));
        auto spawn=Spawn::create(seq_sano,fadein_back, NULL);
        
        auto scale=TargetedAction::create(baloon, ScaleTo::create(7, scale_original*1.2));
        auto scale_spawn=Spawn::create(scale,
                                       sano_action,
                                       createSoundAction("pop.mp3"),
                                       Sequence::create(Spawn::create(monkey_actions),
                                                        createSoundAction("monkey.mp3"), NULL), NULL);
        
        auto break_baloon=Sequence::create(createSoundAction("pan.mp3"),
                                           Spawn::create(TargetedAction::create(baloon, FadeOut::create(.1)),
                                                         TargetedAction::create(back->getChildByName("sup_70_rope.png"), FadeOut::create(.1))
                                                         , NULL),
                                           CallFunc::create([back]{
            auto particle=ParticleSystemQuad::create("particle_texture.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->resetSystem();
            particle->setDuration(2);
            particle->setTotalParticles(150);
            particle->setTexture(Sprite::create("sakura.png")->getTexture());
            particle->setStartSize(back->getContentSize().width*.05);
            particle->setStartSizeVar(back->getContentSize().width*.005);
            particle->setEndSize(particle->getStartSize());
            particle->setSpeed(back->getContentSize().height*.2);
            particle->setPosition(back->getContentSize().width/2,back->getContentSize().height+particle->getStartSize());
            particle->setPosVar(Vec2(back->getContentSize().width*.8, 0));
            back->addChild(particle);
        }),
                                           DelayTime::create(3),
                                           Spawn::create(TargetedAction::create(sakura, FadeIn::create(.7)),
                                                         NULL), NULL);

        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein,seq_handle,delay,spawn,scale_spawn,break_baloon,DelayTime::create(1.5),TargetedAction::create(back,FadeOut::create(1.5)),call, NULL));
    }
    else if(key=="driver")
    {
        Common::playSE("p.mp3");
        auto driver=Sprite::createWithSpriteFrameName("anim_46_driver.png");
        driver->setPosition(parent->getContentSize()/2);
        driver->setOpacity(0);
        parent->addChild(driver);
        
        auto screw_0=parent->getChildByName("sup_46_screw_0.png");
        auto screw_1=parent->getChildByName("sup_46_screw_1.png");
        auto cover=parent->getChildByName("sup_46_cover.png");
        
        auto distance=parent->getContentSize().width*.01;
        auto fadein= FadeIn::create(.7);
        auto shake=Repeat::create(Sequence::create(MoveBy::create(.1,Vec2(distance, distance)),
                                                   CallFunc::create([]{Common::playSE("kacha_1.mp3");}),
                                                   MoveBy::create(.1,Vec2(-distance, -distance)), NULL),3);
        
        auto seq_0=TargetedAction::create(driver, Sequence::create(fadein,shake, NULL));
        auto remove_screw_0=TargetedAction::create(screw_1, FadeOut::create(0));
        
        auto move=MoveBy::create(2, Vec2(0, parent->getContentSize().height*.59));
        auto seq_1=TargetedAction::create(driver, Sequence::create(move,shake->clone(), NULL));
        auto remove_screw_1=TargetedAction::create(screw_0, FadeOut::create(0));
        
        auto spawn=Spawn::create(
                                 TargetedAction::create(cover, FadeOut::create(.5)),
                                 TargetedAction::create(driver, FadeOut::create(.5)), NULL);
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_0,remove_screw_0,seq_1,remove_screw_1,DelayTime::create(.5),spawn,DelayTime::create(.3),call, NULL));
    }
    else if(key=="openwindow")
    {
        auto back=createSpriteToCenter("back_77.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        std::vector<std::string>names={"sup_77_0.png","sup_77_1.png","sup_77_wind_0.png","sup_77_wind_1.png"};
        for (auto name : names) {
            auto sup=createSpriteToCenter(name, false, parent);
            sup->setLocalZOrder(1);
            back->addChild(sup);
        }
        
        auto open=createSpriteToCenter("sup_77_open.png", true, parent);
        back->addChild(open);
        
        auto cover=createSpriteToCenter("sup_77_cover.png", true, parent);
        back->addChild(cover);
        
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        
        auto seq_open=TargetedAction::create(open, Sequence::create(createSoundAction("gacha.mp3"),Spawn::create(FadeIn::create(.7),
                                                                                                                 TargetedAction::create(cover, FadeIn::create(.7)), NULL),createSoundAction("wind.mp3"),DelayTime::create(1.5), NULL));
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,DelayTime::create(1),seq_open,call, NULL));
    }
    else if(key=="sakura_cake"){
        auto call_story_0=CallFunc::create([this,parent,callback](){
            auto story=StoryLayer::create("ending1", [this,callback,parent](Ref*ref){
                Common::playSE("kapa.png");
                auto oju=parent->getChildByName<Sprite*>("sup_15_oju.png");
                auto oju_1=createSpriteToCenter("sup_15_oju_1.png", true, parent);
                parent->addChild(oju_1);
                
                auto delay=DelayTime::create(.5);
                auto change=createChangeAction(1, .7, oju, oju_1);
                auto call_story_1=CallFunc::create([this,parent,callback,oju_1](){
                    auto story=StoryLayer::create("ending2", [this,callback,parent,oju_1](Ref*ref){
                        
                        auto fadeout=TargetedAction::create(oju_1, FadeOut::create(.7));
                        auto call=CallFunc::create([callback]{
                            callback(true);
                        });
                        
                        parent->runAction(Sequence::create(fadeout,call, NULL));
                    });
                    Director::getInstance()->getRunningScene()->addChild(story);
                });
                
                parent->runAction(Sequence::create(delay,change,call_story_1, NULL));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });

        parent->runAction(Sequence::create(call_story_0, NULL));
    }
    else if(key=="cloud"){
        Common::playSE("pig_1.mp3");
        auto num=DataManager::sharedManager()->getNowBack();
        auto cloud_anchor=Vec2(.625,.74);
        
        Vector<FiniteTimeAction*>actions;
        auto cloud=AnchorSprite::create(cloud_anchor, parent->getContentSize(), StringUtils::format("sup_%d_cloud.png",num));
        cloud->setOpacity(0);
        parent->addChild(cloud);
        auto original_scale=cloud->getScale();
        cloud->setScale(.1);
        
        auto seq_pig=createBounceAction(parent->getChildByName("sup_47_pig.png"), .3);
        actions.pushBack(seq_pig);
        
        auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(.5, original_scale),
                                                                                    FadeIn::create(.5), NULL),
                                                                      createSoundAction("nyu.mp3"),
                                                                      DelayTime::create(.5),
                                                                      Spawn::create(ScaleTo::create(.5, .1),
                                                                                    FadeOut::create(.5), NULL), NULL));
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(37)) {
            actions.pushBack(seq_cloud);
        }
        
        auto spawn=Spawn::create(actions);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,call, NULL));
    }
    else if(key=="pigvoice"){
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(createSoundAction("pig.mp3"),DelayTime::create(1),call, NULL));
    }
    else if(key=="clear"){
        Common::playSE("dogara.mp3");
        
        auto open=createSpriteToCenter("sup_30_open.png", true, parent);
        parent->addChild(open);
        
        auto fadein=TargetedAction::create(open, FadeIn::create(.2));
        
        auto call=CallFunc::create([]{

            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

            auto story=StoryLayer::create("ending3", [](Ref*ref){
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        parent->runAction(Sequence::create(fadein,DelayTime::create(.5),call, NULL));
    }
    else if(key=="dontopen"){
        auto story=StoryLayer::create("dontopen", [callback,parent](Ref*ref){
            callback(true);
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
    else{
        callback(true);
    }
}

#pragma mark - Private
ParticleSystemQuad* SakuraActionManager::createFire(Node*parent,float size,Vec2 pos)
{
    auto particle=ParticleSystemQuad::create("fire.plist");
    particle->setPosition(pos);
    particle->setAutoRemoveOnFinish(true);
    particle->setStartSize(size);
    particle->setEndSize(size*2);
    particle->setSpeed(size);
    particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
    particle->setTotalParticles(200);
    particle->setPosVar(Vec2(parent->getContentSize().width*.1, parent->getContentSize().height*.02));
    return particle;
}

#pragma mark - Item
void SakuraActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{

}

#pragma mark - Custom
void SakuraActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    if (key == CustomAction_Title){
        clearParticleAction(parent);
    }
}

void SakuraActionManager::clearParticleAction(cocos2d::Node *parent)
{
    auto particle=ParticleSystemQuad::create("particle_texture.plist");
    particle->setAutoRemoveOnFinish(true);
    particle->resetSystem();
    particle->setDuration(-1);
    particle->setTotalParticles(50);
    particle->setTexture(Sprite::create("sakura.png")->getTexture());
    particle->setStartSize(parent->getContentSize().width*.05);
    particle->setStartSizeVar(parent->getContentSize().width*.005);
    particle->setEndSize(particle->getStartSize());
    particle->setSpeed(parent->getContentSize().height*.2);
    particle->setPosition(parent->getContentSize().width/2,parent->getContentSize().height+particle->getStartSize());
    particle->setPosVar(Vec2(parent->getContentSize().width*.8, 0));
    parent->addChild(particle);
}
