
#include "AppDelegate.h"
#include "CorporationScene.h"
#include "Utils/FireBaseBridge.h"
#include "Utils/InterstitialBridge.h"
#include "NotificationKeys.h"
#include "Utils/NativeBridge.h"
#include "Utils/AdmobRewardBridge.h"
#include "Utils/Common.h"
#include "Utils/NativeAdManager.h"
#include "Utils/ItemStore.h"
#include "Utils/NotificationBridge.h"
#include "Utils/BannerBridge.h"
#include "EscapeStageSprite.h"
#include "AnalyticsManager.h"
#include <unistd.h>//sleep使うよう
#include "Utils/ShakeGestureManager.h"
#include "Adjust/Adjust2dx.h"

USING_NS_CC;

AppDelegate::AppDelegate()
{
}

AppDelegate::~AppDelegate() 
{
}

// if you want a different context, modify the value of glContextAttrs
// it will affect all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};
    
    GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages,
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLViewImpl::createWithRect("EscapeContainer", Rect(0, 0, director->getWinSize().width, director->getWinSize().height));
        director->setOpenGLView(glview);
    }

    director->getOpenGLView()->setDesignResolutionSize(director->getWinSize().width, director->getWinSize().height, ResolutionPolicy::SHOW_ALL);
    
    //テクスチャアトラスの読み込み.ゲーム全体で使用する画像
    for (int i=0; i<3; i++) {
        auto altas = StringUtils::format("gameAtlas_p%d",i);
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile(altas + ".plist", altas + ".png");
    }
    for (int i=0; i<1; i++) {
        auto altas = StringUtils::format("gameAtlas_j%d",i);
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile(altas + ".plist", altas + ".jpg");
    }
    
    //ローカルテスト用に画像をあらかじめキャッシュもしくは画像パスを検索しやすくする。
    EscapeStageSprite::prepareLocalTest();
    
   
    //turn on display FPS
    Common::performProcessForDebug([director]{
        //director->setDisplayStats(true);
    }, nullptr);
    
    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);
    
    //FileUtils::getInstance()->addSearchPath("res");
    
    //登録されている通知キャンセル
    NotificationBridge::cancelAllLocalNotification();
    
    Common::performProcessForDebug([](){
        //インターステイシャル
        //InterstitialBridge::loadingIS(INTERSTITIAL_APPID);
        //InterstitialBridge::loadingIS(INTERSTITIAL_LAUNCH_APPID);
        
        //動画パネル広告
        //NativeAdManager::getInstance()->configureNativeAd();
        //NativeAdManager::getInstance()->loadingNativeAd();
        
        //アイテムデータを読み込み.価格とか読み込める。
        //ItemStore_plugin::ItemStore::loadingStoreInformation();
        
        
    }, [](){            
        //Admobで初期化
        BannerBridge::configure();
        
        //インターステイシャル
        InterstitialBridge::loadingIS(INTERSTITIAL_APPID);
        InterstitialBridge::loadingIS(INTERSTITIAL_LAUNCH_APPID);
        
        //動画パネル広告
        NativeAdManager::getInstance()->configureNativeAd();
        NativeAdManager::getInstance()->loadingNativeAd();
        
        //アイテムデータを読み込み.価格とか読み込める。
        ItemStore_plugin::ItemStore::loadingStoreInformation();
    });

    //Firebase初期化。データ読み込み
    FireBaseBridge::getInstance()->setUp();
    FireBaseBridge::getInstance()->initRemoteConfig();//ABtesting
    
    FireBaseBridge::getInstance()->checkConnected();
    FireBaseBridge::getInstance()->loadingTimestamp(false);
    
    //分析用nodeの初期化
    AnalyticsManager::getInstance();
    
    
    //Adjust:とりあえずiOSだけ処理する
    if (!Common::isAndroid()) {
        std::string appToken = "azyfe4v4rsow";
        std::string environment = AdjustEnvironmentProduction2dx;
        
        Common::performProcessForDebug([&environment](){
            environment = AdjustEnvironmentSandbox2dx;
            log("テスト環境です");
        });

        AdjustConfig2dx adjustConfig = AdjustConfig2dx(appToken, environment);
        Adjust2dx::start(adjustConfig);
    }
    
    //create a scene. it's an autorelease object
    auto scene = CorporationScene::createScene();

    // run
    director->runWithScene(scene);
    
    return true;
}

// This function will be called when the app is inactive. Note, when receiving a phone call it is invoked.
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be paused
    
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyDidEnterBackGround);
    
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        //Adjust2dx::onPause();
#endif
   
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    
    Director::getInstance()->startAnimation();
    // if you use SimpleAudioEngine, it must resume here

    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyWillEnterForeGround);
    
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    //Adjust2dx::onResume();
#endif
}
