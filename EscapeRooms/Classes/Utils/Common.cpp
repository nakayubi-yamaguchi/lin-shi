//
//  Common.cpp
//  Nekotsumi
//
//  Created by 成田凌平 on 2016/01/19.
//
//

#include "Common.h"
#include "SettingDataManager.h"
#include "BannerBridge.h"
#include "AudioManager.h"
#include "ProjectConstants.h"

#pragma mark- デバイス
float Common::getDensity()
{
    float dpi=(float)Device::getDPI();
    
    return  floor(dpi/160+.5);
}

bool Common::isIPad()
{
    float win_height=Director::getInstance()->getWinSize().height;
    //log("%f",win_height);
    
    if (win_height==1024||win_height==768||win_height==1536||win_height==2048) {
        return true;
    }
    
    return false;
}

bool Common::isAndroid()
{
    if(SystemOS=="android")
        return true;
    
    return false;
}

#pragma mark- ローカライズ
std::string Common::localize(std::string en, std::string ja)
{//設定言語を読み込み判別
    auto lang = Application::getInstance()->getCurrentLanguage();
    
    switch(lang) {
        case LanguageType::JAPANESE:
            return ja;
        default:
            return en;
    }
}

std::string Common::localize(string en, string ja, string ch_simple)
{
    auto lang = Application::getInstance()->getCurrentLanguage();
    
    switch(lang) {
        case LanguageType::JAPANESE:
            return ja;
        case LanguageType::CHINESE:
            return ch_simple;
        default:
            return en;
    }
}

std::string Common::localize(string en, string ja, string ch_simple,string ch_traditional,string korean)
{
    auto lang = NativeBridge::getCurrentLanguage();
    //log("設定言語 %s",lang.c_str());
    
    auto isSame = [lang](std::string lCode, std::string lCode2)->bool{
        return (lang == lCode) || (lang == lCode2);
    };
    
    if (hasPrefix(lang, "ja")) {
        return ja;
    }
    else if(hasPrefix(lang, "zh-Hans") || hasPrefix(lang, "zh_CN")) {
        return ch_simple;
    }
    else if(hasPrefix(lang, "zh-Hant") || hasPrefix(lang, "zh_TW")) {
        return ch_traditional;
    }
    else if(hasPrefix(lang, "ko")) {
        return korean;
    }
    else {
        return en;
    }
}

std::string Common::getLocalizeText(ValueMap map)
{
    auto text=map[getLocalizeCode()].asString();
    return text;
}

std::string Common::localizeURL(string url)
{
    auto suffix = Common::localize("-en", "", "-en","-en","-ko");
    
    if (suffix.size() > 0) {//日本以外なら
        url.append(suffix);
    }
    
    log("リターンするURLは%s",url.c_str());
    
    return url;
}

std::string Common::getLocalizeCode()
{
    return localize("en", "ja", "ch", "tw", "ko");
}

std::string Common::getLocalizeKey(string apendString)
{
    std::string string;
    auto lang = Application::getInstance()->getCurrentLanguage();

    switch(lang) {
        case LanguageType::JAPANESE:
            string="";
            break;
        case LanguageType::CHINESE:
            string="_ch";
            break;
        case LanguageType::KOREAN:
            string="_ko";
            break;
        default:
            string="_en";
            break;
    }
    
    return apendString+string;
}

std::string Common::getUsableFontPath(string fontPath)
{
    auto lang = Application::getInstance()->getCurrentLanguage();
    
    switch(lang) {
        case LanguageType::JAPANESE:
            return fontPath;
        case LanguageType::ENGLISH:
            return fontPath;
        default:
            break;
    }
    
    return LocalizeFont;
}

std::string Common::transformToPlural(int num, string singular, string plural)
{
    return num > 1 ? plural : singular;
}

#pragma mark- サウンド

void Common::playBGM(const char *fileName)
{
    Common::playBGM(fileName,true);
}

void Common::playBGM(const char *fileName, bool roop)
{
    playBGM(fileName, roop, 1);
}

void Common::playBGM(const char *fileName, bool roop, float volume)
{
    AudioManager::getInstance()->playBGM(fileName, roop, volume);
}

void Common::stopBgm()
{
    AudioManager::getInstance()->stopBgm();
}

void Common::resumeBGM()
{
    AudioManager::getInstance()->resumeBGM();
}

int Common::playSE(const char *fileName)
{
    return AudioManager::getInstance()->playSE(fileName);
}

void Common::stopAllSE()
{
    AudioManager::getInstance()->stopAllSE();
}

void Common::playClick()
{
    AudioManager::getInstance()->playSE(ClickSoundName);
}

void Common::playClickSmallButton()
{
    AudioManager::getInstance()->playSE(ClickSmallSoundName);
}

void Common::allFreeze()
{
    //再生開始時に、必ず他のアニメーションを停止して下さい。(一部のアドネットワークでエラーが起きます)
    //再生完了（もしくは失敗）後、アニメーションを再開して下さい。
    Director::getInstance()->pause();
    Director::getInstance()->stopAnimation();
    
    AudioManager::getInstance()->allFreeze();
}

#pragma mark- 色
Color3B Common::getColorFromHex(const string hex)
{
    return Color3B(
                   getNumberFromHex(hex, 0),
                   getNumberFromHex(hex, 2),
                   getNumberFromHex(hex, 4));
}

int Common::getNumberFromHex(const string hex, int rangeFrom)
{
    string hexString=hex.substr(rangeFrom,2);
    
    istringstream is(hexString);
    int value;
    is >> std::hex >> value;
    
    return value;
}

#pragma mark- シェア
void Common::shareWithImage(std::string shareText, ShareType shareType)
{
    log("今からシェアします");
    
    utils::captureScreen([&,shareText,shareType](bool succeed, const std::string &filePath){
        if (succeed) {
            log("スクショ保存成功");

            //キャッシュを削除
            Director::getInstance()->getTextureCache()->removeTextureForKey(filePath);

            NativeBridge::openShareMenu(shareType, shareText.c_str(), filePath.c_str());
        }
        else{
            log("スクショ保存失敗");

        }
    }, "screenshot.png");
}

#pragma mark- String関連
bool Common::hasPrefix(const std::string& s, const char* v) {
    
    if (s.find(v) == 0) {
        //log("文字列`%s`で始まる文字列です",v);
        return true;
    }

    return false;
}

std::vector<std::string> Common::splitString(std::string str, std::string del)
{
    int first = 0;
    int last = str.find_first_of(del.c_str());
    
    std::vector<std::string> result;
    
    while (first < str.size()) {
        std::string subStr(str, first, last - first);
        
        result.push_back(subStr);
        
        first = last + 1;
        last = str.find_first_of(del.c_str(), first);
        
        if (last == std::string::npos) {
            last = str.size();
        }
    }
    
    return result;
}
#pragma mark - ラベルサイズ
void Common::sizeFit(Label *label, float maxWidth)
{
    //自動size調整
    while (maxWidth<label->getContentSize().width) {
        //log("調整");
        //log("%f<%f",size.width,butLabel->getContentSize().width);
        TTFConfig ttfConfig(label->getTTFConfig().fontFilePath,
                            label->getTTFConfig().fontSize*.9,
                            GlyphCollection::DYNAMIC);
        label->setTTFConfig(ttfConfig);
    }
}

#pragma mark- レビューURL取得
std::string Common::getReviewUrl()
{
    auto reviewUrl = ReviewURL;
    auto appId = AppID;
    
    return StringUtils::format(reviewUrl,appId);
}

#pragma mark - アクション
CallFunc* Common::createSoundAction(std::string fileName)
{
    auto call=CallFunc::create([fileName]{Common::playSE(fileName.c_str());});
    return call;
}

#pragma mark - 日付
long long Common::transformTimeToDate(time_t tm1970)
{
    struct tm* tm = std::localtime(&tm1970);

    auto tm_St = StringUtils::format("%.3d%.2d%.2d",tm->tm_year, tm->tm_mon, tm->tm_mday);
    
    //tm_St = StringUtils::format("%04d%02d%02d%02d%02d",tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday, tm->tm_hour, tm->tm_min);
    
    auto tm_long = atoll(tm_St.c_str());
    return tm_long;
}

long long Common::dateWithTimeStamp(TimeUnit validTimeUnit, time_t tm)
{
    return dateWithTimeInterval(validTimeUnit, tm, 0);
}

long long Common::dateWithTimeInterval(TimeUnit validTimeUnit, time_t tm, int intervalTime)
{
    struct tm *time = localtime(&tm);
    
    if (intervalTime!=0) {
        time->tm_sec = time->tm_sec + intervalTime;
        time_t transedTime = mktime(time);
        time = localtime(&transedTime);
    }
    
    auto dateSt = StringUtils::format("%04d%02d%02d%02d%02d%02d", time->tm_year+1900, time->tm_mon+1, time->tm_mday, time->tm_hour, time->tm_min, time->tm_sec);
    
    log("時間を変換いします%s",dateSt.c_str());
    long long longDate = atoll(dateSt.c_str());
    log("時間をlongに変換いします%lld",longDate);

    
    int exponentiationCount = 0;
    switch (validTimeUnit) {
        case TimeUnit_Year:
            exponentiationCount = 10;
            break;
        case TimeUnit_Month:
            exponentiationCount = 8;
            break;
        case TimeUnit_Day:
            exponentiationCount = 6;
            break;
        case TimeUnit_Hour:
            exponentiationCount = 4;
            break;
        case TimeUnit_Min:
            exponentiationCount = 2;
            break;
        default:
            break;
    }
    
    long long finalLongDate = longDate/pow(10, exponentiationCount);
        
    return finalLongDate;
}

long long Common::dateWithDateDetail(TimeUnit validTimeUnit, int year, int month, int day, int hour, int minutes, int seconds)
{
    return dateWithTimeStamp(validTimeUnit, timeStampWithDate(year, month, day, hour, minutes, seconds));
}

time_t Common::timeStampWithDate(int year, int month, int day)
{
    return timeStampWithDate(year, month, day, 0);
}

time_t Common::timeStampWithDate(int year, int month, int day, int hour)
{
    return timeStampWithDate(year, month, day, hour, 0);
}

time_t Common::timeStampWithDate(int year, int month, int day, int hour, int minutes)
{
    return timeStampWithDate(year, month, day, hour, minutes, 0);
}

time_t Common::timeStampWithDate(int year, int month, int day, int hour, int minutes, int seconds)
{
    time_t t = time(NULL);
    struct tm *sampleDate = localtime(&t);
    sampleDate->tm_mday  = day;
    sampleDate->tm_mon   = month - 1;
    sampleDate->tm_year  = year - 1900;
    sampleDate->tm_hour  = hour;
    sampleDate->tm_min   = minutes;
    sampleDate->tm_sec   = seconds;
    
    time_t time = mktime(sampleDate);
    
    return time;
}

long long Common::differenceTime(long long beforeDate, long long afterDate)
{
    auto getIntTime = [](std::string dateSt, int startPos, int range)->int{
        int time = 0;
        if ((startPos + range) <= dateSt.size()) {
            time = atoi(dateSt.substr(startPos, range).c_str());
        }
        log("%s時間の抽出{%d,%d}時間は%d",dateSt.c_str(), startPos, range, time);
        
        return time;
    };
    
    auto transToTime_t = [getIntTime](std::string dateSt)->time_t{//年月日時分秒
        time_t time = timeStampWithDate(getIntTime(dateSt, 0, 4),//年
                                        getIntTime(dateSt, 4, 2),//月
                                        getIntTime(dateSt, 6, 2),//日
                                        getIntTime(dateSt, 8, 2),//時
                                        getIntTime(dateSt, 10, 2),//分
                                        getIntTime(dateSt, 12, 2));//秒
        
        return time;
    };
    
    //
    auto afterSt = StringUtils::format("%lld",afterDate);
    
    time_t after_time = transToTime_t(afterSt);
    
    auto beforeSt = StringUtils::format("%lld",beforeDate);
    time_t before_time = transToTime_t(beforeSt);
    
    auto diff = difftime(after_time, before_time);
    
    return diff;
}

std::string Common::getMonthDateSt(int intdate)
{
    auto dateSt = StringUtils::format("%d", intdate);
    return StringUtils::format("%s/%s", dateSt.substr(4, 2).c_str(), dateSt.substr(6, 2).c_str());
}

void Common::performAsyncTask(const std::function<void ()> &asyncTask, const std::function<void ()> &mainThreadTask)
{
    auto loading_thread = std::thread([asyncTask, mainThreadTask](){

        if (asyncTask) {
            asyncTask();
        }
        
        Director::getInstance()->getScheduler()->performFunctionInCocosThread([mainThreadTask](){
            if (mainThreadTask) {
                mainThreadTask();
            }
        });
    });
    
    loading_thread.detach();
}

#pragma mark - デバッグ用
std::string Common::getLocalTestDir()
{
    auto path = StringUtils::format(LocalResourcesDir, LocalTestTitle);
    return path;
}

void Common::performProcessForDebug(const onDebugAction &debugCallback, const onSemiDebugAction &releaseCallback)
{
    if (DebugMode == 2) {
        if (debugCallback) {
            debugCallback();
        }
    }
    else {
        if (releaseCallback) {
            releaseCallback();
        }
    }
}

void Common::performProcessForDebug(const onDebugAction &debugCallback)
{
    if (DebugMode != 0) {
        if (debugCallback) {
            debugCallback();
        }
    }
}

void Common::performProcessForDebug_1(const onDebugAction &debugCallback)
{
    if (DebugMode == 1) {
        if (debugCallback) {
            debugCallback();
        }
    }
}

void Common::performProcessOnlyRelease(const onReleaseAction &releaseCallback)
{
    if (DebugMode)
        return;
    
    if (releaseCallback) {
        releaseCallback();
    }
}

//実行速度計測用のmap.開始時間を格納。
static std::map<const char*, double> startPerformanceTimeMap = {};

void Common::startTimeForDebug()
{
    startTimeForDebug(PerformanceKey);
}

void Common::startTimeForDebug(const char* debugKey)
{
    performProcessForDebug([debugKey](){
        clock_t startTime = clock();
        startPerformanceTimeMap[debugKey] = startTime;
        
        log("[%s]を実行します%lu", debugKey,startTime);
        
    }, nullptr);
}


void Common::stopTimeForDebug()
{
    stopTimeForDebug(PerformanceKey);
}

void Common::stopTimeForDebug(const char *debugKey)
{
    Common::performProcessForDebug([debugKey](){
        auto startTime = startPerformanceTimeMap[debugKey];
        auto nowTime = clock();
        float delta = (double)(nowTime - startTime) / CLOCKS_PER_SEC;
        log("[%s]の実行に[%.3f秒]かかりました。", debugKey, delta);
    }, nullptr);
}
