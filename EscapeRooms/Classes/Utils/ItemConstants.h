//
//  BannerConstants.h
//  Anaguma
//
//  Created by yamaguchinarita on 2016/06/15.
//
//


#ifndef ItemConstants_h
#define ItemConstants_h

#include "cocos2d.h"

USING_NS_CC;
//注 PurchaseID 必要に応じて、編集すること.cppも
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#define ItemID_30Coin "com.ryohei.haruki.EscapeRooms.30Coin"
#define ItemID_90Coin "com.ryohei.haruki.EscapeRooms.90Coin"
#define ItemID_126Coin "com.ryohei.haruki.EscapeRooms.126Coin"
#define ItemID_165Coin "com.ryohei.haruki.EscapeRooms.165Coin"
#define ItemID_330Coin "com.ryohei.haruki.EscapeRooms.330Coin"
#define ItemID_690Coin "com.ryohei.haruki.EscapeRooms.690Coin"
#define ItemID_1035Coin "com.ryohei.haruki.EscapeRooms.1035Coin"
#define ItemID_1800Coin "com.ryohei.haruki.EscapeRooms.1800Coin"
#define ItemID_3540Coin "com.ryohei.haruki.EscapeRooms.3540Coin"
#define ItemID_PresentPackSmall "com.ryohei.haruki.EscapeRooms.PresentPack.Small"
#define ItemID_PresentPackBig "com.ryohei.haruki.EscapeRooms.PresentPack.Big"
#define ItemID_Subscription ""


#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#define ItemID_30Coin "com.ryohei.haruki.escaperooms.30coin"
#define ItemID_90Coin "com.ryohei.haruki.escaperooms.90coin"
#define ItemID_126Coin "com.ryohei.haruki.escaperooms.126coin"
#define ItemID_165Coin "com.ryohei.haruki.escaperooms.165coin"
#define ItemID_330Coin "com.ryohei.haruki.escaperooms.330coin"
#define ItemID_690Coin "com.ryohei.haruki.escaperooms.690coin"
#define ItemID_1035Coin "com.ryohei.haruki.escaperooms.1035coin"
#define ItemID_1800Coin "com.ryohei.haruki.escaperooms.1800coin"
#define ItemID_3540Coin "com.ryohei.haruki.escaperooms.3540coin"
#define ItemID_PresentPackSmall "com.ryohei.haruki.escaperooms.presentpack.small"
#define ItemID_PresentPackBig "com.ryohei.haruki.escaperooms.presentpack.big"
#define ItemID_Subscription ""
#else

#endif


class ItemConstants {
public:
    static std::vector<std::string> getItemIds();
    static std::vector<std::string> getItemIds(std::vector<const char*> searchTexts);
};


#endif /* ItemConstants_h */
