//
//  CommonNotificationKeys.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/09/09.
//

#ifndef CommonNotificationKeys_h
#define CommonNotificationKeys_h

#define NotificationKeyWillEnterForeGround "willenterforeground"
#define NotificationKeyDidEnterBackGround "didenterforeground"


#endif /* CommonNotificationKeys_h */
