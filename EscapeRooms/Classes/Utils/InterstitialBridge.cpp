//
//  InterstitialBridge.cpp
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/03/27.
//
//

#include "InterstitialBridge.h"
#include <jni.h>
#include "platform/android/jni/JniHelper.h"

#define CLASS_NAME "org/cocos2dx/cpp/InterstitialBridge"
using namespace cocos2d;

static std::function<void()> m_Callback = NULL;

void InterstitialBridge::loadingIS(std::string appID){
    //jniでローディングを呼ぶ
    cocos2d::JniMethodInfo t;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "loadingInterstitial", "(Ljava/lang/String;)V"))
    {
        jstring utfAppId = t.env->NewStringUTF(appID.c_str());
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID, utfAppId);
        
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(utfAppId);//★jstringは解放が必要です。
    }
}

bool InterstitialBridge::isReady(std::string appID)
{
    cocos2d::JniMethodInfo t;
    bool isPrepare;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "isReady", "(Ljava/lang/String;)Z"))
    {
        jstring utfAppId = t.env->NewStringUTF(appID.c_str());

        jboolean ready = (jboolean)t.env->CallStaticBooleanMethod(t.classID, t.methodID, utfAppId);
        
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(utfAppId);//★jstringは解放が必要です。
        isPrepare = ready;
    }
    
    log("インターステイシャルの準備状況%d",isPrepare);
    return isPrepare;
}

void InterstitialBridge::showIS(std::string appID)
{
    showIS(appID, nullptr);
}

void InterstitialBridge::showIS(std::string appID, const std::function<void()> &callback)
{
    m_Callback = callback;
    
    cocos2d::JniMethodInfo t;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "showIS", "(Ljava/lang/String;)V"))
    {
        jstring utfAppId = t.env->NewStringUTF(appID.c_str());
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID, utfAppId);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(utfAppId);//★jstringは解放が必要です。
    }
}

#ifdef __cplusplus
extern "C"
{
#endif
    JNIEXPORT void Java_org_cocos2dx_cpp_InterstitialBridge_didClose(JNIEnv *env,jobject thiz)
    {
        if(m_Callback){
            m_Callback();
        }
        return;
    }
    
#ifdef __cplusplus
}
#endif



