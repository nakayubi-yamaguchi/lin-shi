//
//  NotificationBridge.cpp
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/10/11.
//
//

#include "NotificationBridge.h"
#include <jni.h>
#include "platform/android/jni/JniHelper.h"

//パッケージ名を指定
#define CLASS_NAME "org/cocos2dx/cpp/NotificationBridge"

using namespace cocos2d;

#pragma mark- ローカル通知
void NotificationBridge::registerLocalNotificationSetting()
{
    
}

void NotificationBridge::fireLocalNotification(int time, const char *message, int identifier)
{
    log("%d秒後に通知発行",time);
    
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "fireLocalNotification", "(Ljava/lang/String;II)V")) {
        return;
    }
    
    // 文字列をJava Stringに変換する
    jstring stringArg = methodInfo.env->NewStringUTF(message);
    
    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, stringArg, time, identifier);
    
    //リソースを解放
    methodInfo.env->DeleteLocalRef(stringArg);
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void NotificationBridge::cancelLocalNotification(int identifier)
{
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "cancelLocalNotification", "(I)V")) {
        return;
    }
    
    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID,identifier);
    
    //リソースを解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void NotificationBridge::cancelAllLocalNotification()
{
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "cancelAllLocalNotification", "(II)V")) {
        return;
    }
    
    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID,NotificationTag_Login,NotificationTag_Max);
    
    //リソースを解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

#pragma mark- リモート通知
void NotificationBridge::registerRemoteNotificationSetting(std::vector<std::string>topics)
{
    int len_topics=(int)topics.size();

    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "subscribeToTopic", "([Ljava/lang/String;)V")) {
        return;
    }
    
    jclass _class=methodInfo.env->FindClass("java/lang/String");
    if (_class==NULL) {
        log("そんなのねえよばか");
    }
    
    jobjectArray jvalues;
    jvalues=methodInfo.env->NewObjectArray(len_topics,_class,NULL);
    for (int i=0; i<len_topics; i++) {
        methodInfo.env->SetObjectArrayElement(jvalues, i, methodInfo.env->NewStringUTF(topics[i].c_str()));
        
        log("追加するトピックは%s",topics[i].c_str());
    }
    
    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, jvalues);
    
    //リソースを解放
    methodInfo.env->DeleteLocalRef(jvalues);
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void NotificationBridge::subscribeToTopic(const char* topic)
{
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "subscribeToTopic", "(Ljava/lang/String;)V")) {
        return;
    }
    
    // 文字列をJava Stringに変換する
    jstring stringArg = methodInfo.env->NewStringUTF(topic);
    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, stringArg);
    
    //リソースを解放
    methodInfo.env->DeleteLocalRef(stringArg);
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}
