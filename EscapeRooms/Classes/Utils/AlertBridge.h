//
//  GFScene.h
//  Nekotsumi
//
//  Created by 成田凌平 on 2016/01/18.
//
//

#ifndef GFScene_h
#define GFScene_h

#include "cocos2d.h"

USING_NS_CC;

namespace native_plugin {
    class AlertBridgeDelegate
    {
    public:
        virtual void alertDismissed(int tag, int buttonIndex){};
        ~AlertBridgeDelegate(){log("アラートを解放します");};
    };
    
    class AlertBridge:public Ref
    {
    public:
        static void showAlert(AlertBridgeDelegate*delegate,int tag, const char *title, const char *msg, const char* firstBtn, ...);
    };
}

#endif /* GFScene_h */
