#include "FireBaseBridge.h"
#include "Firebase.h"
#import "Common.h"
#include "NativeBridge.h"
#include "ProjectConstants.h"

USING_NS_CC;

@interface FirebaseHelper : NSObject
{
}

@end

@implementation FirebaseHelper
+(Value)transformSnapshot:(id)snapValue
{
    Value value;

    if (snapValue == [NSNull null]) {
        value = Value();
    }
    else if ([snapValue isKindOfClass:[NSNumber class]]){
        value = Value([snapValue doubleValue]);
    }
    else if ([snapValue isKindOfClass:[NSString class]]) {
        value = Value([snapValue UTF8String]);
    }
    else if ([snapValue isKindOfClass:[NSArray class]]) {
        ValueVector vector;
        for (id a : snapValue) {
            vector.push_back([FirebaseHelper transformSnapshot:a]);
        }
        value = Value(vector);
    }
    else if ([snapValue isKindOfClass:[NSDictionary class]]) {
        ValueMap valueMap;
    
        for (NSString *key :[snapValue allKeys]) {
            id dicIdValue = snapValue[key];
            auto dicValue = [FirebaseHelper transformSnapshot:dicIdValue];
            
            valueMap[[key UTF8String]] = dicValue;
        }
        
        value = Value(valueMap);
    }
    return value;
}

@end


@interface LoadingCallback : NSObject <UIAlertViewDelegate>
{
}

@property NSString *url;

@end

@implementation LoadingCallback

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSLog(@"アラートが消えました。");
    
    LogReferenceCount(self);
    
    if (buttonIndex==0) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.url]];
    }
}

-(void)dealloc
{
    NSLog(@"新着情報クラスが解放されました。");
}

@end


FireBaseBridge* manager=NULL;
FIRDatabaseReference*_ref=NULL;
FIRStorageReference*storageRef=NULL;
FIRRemoteConfig*remoteConfig=NULL;

LoadingCallback *loadingCallback=NULL;
bool m_isConnected = false;
time_t m_server_date = 0;
time_t m_time_offset;//タイムスタンプ読み込み完了時点から計測開始


#pragma mark- イニシャライズ
FireBaseBridge* FireBaseBridge::getInstance()
{
    //If the singleton has no instance yet, create one
    if(NULL == manager)
    {
        //Create an instance to the singleton
        manager = new FireBaseBridge();
        manager->initialize();
    }

    //Return the singleton object
    return manager;
}

void FireBaseBridge::initialize()
{
    
}

#pragma mark - セットアップ
void FireBaseBridge::setUp()
{
    // Use Firebase library to configure APIs
    [FIRApp configure];
    
    //データベースのリファレンス
    _ref=[[FIRDatabase database] reference];
    
    //ストレージのリファレンス
    FIRStorage* storage = [FIRStorage storage];
    storageRef=[storage referenceForURL:[NSString stringWithUTF8String:StrageUrl]];
    
    //remote config
    remoteConfig=[FIRRemoteConfig remoteConfig];
    FIRRemoteConfigSettings *remoteConfigSettings = [[FIRRemoteConfigSettings alloc] initWithDeveloperModeEnabled:(DebugMode!=0)];
    remoteConfig.configSettings = remoteConfigSettings;
    [remoteConfig setDefaultsFromPlistFileName:@"remoteConfigDefaults"];
}

#pragma mark- 自社アラート
void FireBaseBridge::loadingAlertHouseAd(bool doCallback)
{//データベースから取得
    //JsonからAlertsネストを参照する。
    FIRDatabaseQuery*query=[[_ref child:@"Alerts"]queryOrderedByKey];
    
    [query observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot)
    {//一度しか通らないようにSigleEventOfTypeを使用
       // NSLog(@"取得情報%@",snapshot.value);
        NSDictionary *postDict = snapshot.value;
        if([postDict isKindOfClass:[NSDictionary class]]&&postDict.allKeys.count>0)
        {//結果がNULLの時は通らない
            for (int i=0; i<postDict.allValues.count; i++)
            {//アプリごとの情報を1つずつ参照
                NSDictionary*dic=postDict.allValues[i];
                //NSLog(@"%@",dic);
                
                NSString*messageKey=[NSString stringWithUTF8String:Common::localize("message_en", "message").c_str()];
                
                if ([dic objectForKey:@"iosUrl"]&&[dic objectForKey:messageKey]&&![[dic objectForKey:@"bundleID"]isEqualToString:[[NSBundle mainBundle]bundleIdentifier]] &&![[dic objectForKey:@"type"]isEqualToString:@"util"])// type指定(util以外を指定中)
                {//格納
                    HouseAlert object;
                    object.isNew=[[dic objectForKey:@"isNew"]boolValue];
                    object.message=[[dic objectForKey:messageKey]UTF8String];
                    object.url=[[dic objectForKey:@"iosUrl"]UTF8String];
                    
                    availableAlerts.push_back(object);
                }
            }
            
            //NSLog(@"格納数は%lu",availableAlerts.size());
            //読み取り情報をシャッフル
            shuffleAlerts();
            
            //即時表示
            if (doCallback) {
                showHouseAlert();
            }
        }
    }];
}

void FireBaseBridge::shuffleAlerts()
{//シャッフル
    std::vector<HouseAlert>retainAlerts;
    retainAlerts=availableAlerts;
    
    availableAlerts.clear();
    
    while (retainAlerts.size()>0) {
        
        int size=(int)retainAlerts.size();
        
        auto random=RandomHelper::random_int(0, size-1);//抽選
        auto obj=retainAlerts[random];
        
        // log("追加するメッセージは%s",obj.message);
        
        if (obj.isNew==true) {
            //log("Newがあったので、先頭に持ってくる%d",obj.isNew);
            availableAlerts.insert(availableAlerts.begin(),obj);//先頭に挿入
        }
        else{
            availableAlerts.push_back(obj);//末尾に追加
        }
        retainAlerts.erase(retainAlerts.begin() + random);//保持アラートから削除
    }
}

void FireBaseBridge::cancelShowAlertHouseAd()
{//新作情報のアラート表示をキャンセルする。ローディング完了する前に、ゲームとか開始されちゃった時。
    cancel=true;
}

void FireBaseBridge::showHouseAlert()
{
    if ((int)availableAlerts.size()>0 && cancel==false) {
        
        if (loadingCallback==NULL) {
            loadingCallback=[[LoadingCallback alloc]init];
        }
        
        auto object=availableAlerts[0];
        
        loadingCallback.url=[NSString stringWithUTF8String:object.url.c_str()];
        
        LogReferenceCount(loadingCallback);
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil
                                                     message:[NSString stringWithUTF8String:object.message.c_str()]
                                                    delegate:loadingCallback
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"YES",@"NO", nil];
        [alert show];
        
        showCount++;
    }
}

int FireBaseBridge::getShowCount()
{
    return showCount;
}

#pragma mark- Icon
void FireBaseBridge::loadingHouseIcons()
{//アイコン情報を取得し保持する
    NSLog(@"アイコンロード開始");
    //JsonからIconsネストを参照する。
    [[_ref child:@"Icons"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot)
     {//一度しか通らないようにSigleEventOfTypeを使用
         NSDictionary *postDict = snapshot.value;
         if([postDict isKindOfClass:[NSDictionary class]]&&postDict.allKeys.count>0)
         {//結果がNULLの時は通らない
             savedIconCount=(int)postDict.allKeys.count;
             
             for (NSDictionary*dic in postDict.allValues)
             {//アプリごとの情報を1つずつ参照する
                 //NSDictionary*dic=postDict.allValues[i];
                 NSString*fileName=[dic objectForKey:@"fileName"];
                 NSNumber*num=(NSNumber*)[dic objectForKey:@"japanOnly"];
                 if ([num boolValue]==YES&&Common::localize("eng", "ja")=="eng") {
                     //言語対応していないので除外
                     //NSLog(@"%@は言語み対応",fileName);
                     savedIconCount--;
                 }
                 else if ([dic objectForKey:@"iosUrl"]&&![[dic objectForKey:@"bundleID"]isEqualToString:[[NSBundle mainBundle]bundleIdentifier]] &&![[dic objectForKey:@"type"]isEqualToString:@"util"])// type指定(util以外を指定中)
                 {//取得した情報からファイルデータを取得する
                     // Create a reference with an initial file path and name
                     FIRStorageReference *imageRef = [storageRef child:fileName];
                     
                     // Download in memory with a maximum allowed size of 1MB (1 * 1024 * 1024 bytes)
                     [imageRef dataWithMaxSize:1 * 1024 * 1024 completion:^(NSData* data, NSError* error){
                         if (error != nil) {
                             // Uh-oh, an error occurred!
                             NSLog(@"アイコン取得時にエラー発生");
                         } else {
                             // Data for "images/island.jpg" is returned
                             // ... UIImage *islandImage = [UIImage imageWithData:data];
                             NSLog(@"アイコン取得成功 %@",fileName);
                             //uiimage->pngにして一時保存
                             UIImage*saveimage=[UIImage imageWithData:data];
                             NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                             NSString *documentsDirectory = [paths objectAtIndex:0];
                             NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:fileName];
                             
                             if ([UIImagePNGRepresentation(saveimage) writeToFile:savedImagePath atomically:YES]) {
                                 log("画像保存成功");
                                 std::string imagPath = FileUtils::getInstance()->getWritablePath()+[fileName UTF8String];
                                 
                                 Texture2D* objTexture2D = new Texture2D();
                                 Image* image = new Image();
                                 
                                 image->initWithImageFile(imagPath);
                                 objTexture2D->initWithImage(image);
                                 
                                 //cache削除
                                 Director::getInstance()->getTextureCache()->removeTextureForKey(imagPath);
                                 
                                 auto spr=Sprite::createWithTexture(objTexture2D);
                                 spr->retain();//保持
                                 HouseIcon icon;
                                 icon.sprite=spr;
                                 icon.url=[[dic objectForKey:@"iosUrl"] UTF8String];
                                 icon.isNew=[(NSNumber*)[dic objectForKey:@"isNew"] boolValue];
                                 log("%s",icon.url.c_str());

                                 icons.push_back(icon);
                                 
                                 CC_SAFE_RELEASE(objTexture2D);
                                 CC_SAFE_RELEASE(image);
                             }
                             else{
                                 log("画像を保存失敗");
                             }
                             
                             
                             if (icons.size()==savedIconCount)
                             {//通信終了
                                 log("Iconの取得が全て終了しました。");
                                 
                                 if (iconBanner) {
                                     //すでにバナーが存在する場合、そこにiconを追加する
                                     addIconOnBanner();
                                 }
                             }
                         }
                     }];
                 }
                 else{
                     savedIconCount--;
                 }
             }
         }
    }];
}

Sprite* FireBaseBridge::getIconBanner(cocos2d::Size size, int iconCount)
{
    _bannerSize=size;
    _iconCount=iconCount;
    
    if (iconBanner) {
        deleteIconBanner();
    }
    
    iconBanner=Sprite::create();
    iconBanner->setColor(Color3B::WHITE);
    iconBanner->setOpacity(0);
    iconBanner->setCascadeOpacityEnabled(false);
    iconBanner->setTextureRect(cocos2d::Rect(0, 0, size.width,size.height));
    
    //アイコンをセット
    addIconOnBanner();
    
    return iconBanner;
}

void FireBaseBridge::addIconOnBanner()
{
    int count=MIN((int)icons.size(),_iconCount);//表示できる個数(在庫)、指定個数の少ない方
    
    //表示画像をランダムでシャッフル
    std::vector<HouseIcon>retainIcons;
    retainIcons=icons;
    
    icons.clear();
    
    while (icons.size()<count) {
        int size=(int)retainIcons.size();
        
        auto random=RandomHelper::random_int(0, size-1);//抽選
        auto icon=retainIcons[random];
        
        if (icon.isNew==true) {
            //log("Newがあったので、先頭に持ってくる%d",obj.isNew);
            icons.insert(icons.begin(),icon);//先頭に挿入
        }
        else{
            icons.push_back(icon);//末尾に追加
        }
        retainIcons.erase(retainIcons.begin() + random);//保持アラートから削除
    }
    
    //サイズとスペース計算
    float iconSize=MIN(_bannerSize.width/count, _bannerSize.height)*.9;
    float space=(_bannerSize.width-iconSize*count)/(count+1);
    
    for (int i=0; i<count; i++) {
        auto icon=icons[i];
        
        auto menuItem=MenuItemSprite::create(icon.sprite, icon.sprite,[this,icon](Ref*sender){
            NativeBridge::openUrl(icon.url.c_str());
        });
        menuItem->setScale(iconSize/menuItem->getContentSize().width);
        menuItem->setAnchorPoint(Vec2(0, .5));
        menuItem->setPosition(space+(iconSize+space)*i, iconBanner->getContentSize().height/2);
        
        auto menu=Menu::create(menuItem, nil);
        menu->setPosition(0,0);
        iconBanner->addChild(menu);
    }
}

void FireBaseBridge::deleteIconBanner()
{
    iconBanner=NULL;
}

void FireBaseBridge::createSprite(unsigned char *array, int length, const char *url,bool isNew){}


#pragma mark- Analytics
void FireBaseBridge::analyticsWithName(const char *eventName)
{
    log("[%s]のイベントを飛ばします。",eventName);
    [FIRAnalytics logEventWithName:[NSString stringWithUTF8String:eventName] parameters:nil];
}

void FireBaseBridge::analyticsWithParameters(const char *eventName, std::vector<const char *> keys, std::vector<const char *> values)
{
    NSMutableDictionary*dic=[NSMutableDictionary dictionary];
    for (int i=0; i<keys.size(); i++) {
        [dic setObject:[NSString stringWithUTF8String:values[i]] forKey:[NSString stringWithUTF8String:keys[i]]];
    }
    
    NSLog(@"送信パラメータ%@",dic.copy);
    [FIRAnalytics logEventWithName:[NSString stringWithUTF8String:eventName] parameters:dic.copy];
}

void FireBaseBridge::setUserID(const char *userId)
{
    [FIRAnalytics setUserID:[NSString stringWithUTF8String:userId]];
}

void FireBaseBridge::setUserProperty(const char *propertyName, const char *value)
{
    [FIRAnalytics setUserPropertyString:[NSString stringWithUTF8String:propertyName] forName:[NSString stringWithUTF8String:value]];
}


#pragma mark - Remote Config
void FireBaseBridge::initRemoteConfig()
{
    NSLog(@"分析トークン %@", [[FIRInstanceID instanceID] token]);
}

void FireBaseBridge::fetchRemoteConfig(std::string key,const onFinishedLoadValue& callback)
{
    auto duration=remoteConfig.configSettings.isDeveloperModeEnabled?60:60*60*24;
    log("分析 fetch開始 duration : %d",duration);
    
    //値として保持
    __block auto f_callback=callback;

    [remoteConfig fetchWithExpirationDuration:duration completionHandler:^(FIRRemoteConfigFetchStatus status, NSError * _Nullable error) {
        if (status == FIRRemoteConfigFetchStatusSuccess) {
            [remoteConfig activateFetched];
            
            auto value=[[remoteConfig[[NSString stringWithUTF8String:key.c_str()]]stringValue]intValue];
            log("分析 fetch成功 : %d",value);
            f_callback(true,Value(value));
        }
        else{
            log("分析 fetch失敗");
            NSLog(@"Error %@", error.localizedDescription);
        }
    }];
}

#pragma mark- 接続状態
bool FireBaseBridge::getIsConnected()
{    
    log("接続状態は%d",m_isConnected);
    return m_isConnected;
}

void FireBaseBridge::checkConnected()
{
    FIRDatabaseReference *connectedRef = [[FIRDatabase database] referenceWithPath:@".info/connected"];
    [connectedRef observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot) {
        if([snapshot.value boolValue]) {
            NSLog(@"connected");
        } else {
            NSLog(@"not connected");
        }
        
        m_isConnected = [snapshot.value boolValue];
    }];
}

#pragma mark - Database
#pragma mark データ取得
void FireBaseBridge::loadingValue(const char *keyName, const onFinishedLoadValue &callback)
{
    /*
    NSDictionary
    NSArray
    NSNumber (also includes booleans)
    NSString*/

    FIRDatabaseQuery*query = [[_ref child:[NSString stringWithUTF8String:keyName]] queryOrderedByKey];
    //NSLog(@"読み取りvalueを取得開始%s",keyName);
    __block auto f_callback = callback;
    
    [query observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSLog(@"読み取り完了発生！！！%@",snapshot);
        
        auto value = [FirebaseHelper transformSnapshot:snapshot.value];
        
        if (value.isNull()) {
            if (f_callback) {
                f_callback(false, value);
            }
        }
        else {
            if (f_callback) {
                f_callback(true, value);
            }
        }
    } withCancelBlock:^(NSError * _Nonnull error) {
        if (error) {
            NSLog(@"読み取り完了発生！！！%@",error);

            if (f_callback) {
                f_callback(false, Value());
            }
        }
    }];
}
#pragma mark データセット
void FireBaseBridge::setValue(std::string childKey, std::map<std::string, int> value, const onFIRFinished &callback)
{
    __block auto f_callback = callback;
    
    //セットするDictionaryを作成します。
    NSMutableDictionary *postDic = @{}.mutableCopy;
    for (auto itr = value.begin(); itr != value.end(); ++itr){
        
        NSString *iKeySt = [NSString stringWithUTF8String:itr->first.c_str()];
        int postNum = itr->second;

        postDic[iKeySt] = @(postNum);
    }
    
    //値をセット
    NSString *childKeySt = [NSString stringWithUTF8String:childKey.c_str()];
    [[_ref child:childKeySt] setValue:postDic withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (f_callback) {
            f_callback((!error));
        }
    }];
}

void FireBaseBridge::transactionPost(std::string childKey, std::vector<std::string> incrementKeys, std::vector<std::string> decrementKeys, const onFIRFinished &callback)
{
    std::map<std::string, int>transactionMap;
    
    for (auto incrementKey : incrementKeys) {
        transactionMap[incrementKey] = 1;
    }
    
    for (auto decrementKey : decrementKeys) {
        transactionMap[decrementKey] = -1;
    }
    
    transactionPost(childKey, transactionMap, callback);
}

void FireBaseBridge::transactionPost(std::string childKey, std::map<std::string, int> transactionMap, const onFIRFinished &callback)
{
    __block auto f_callback = callback;
    
    __block auto f_transactionMap = transactionMap;
    
    NSString *childKeySt = [NSString stringWithUTF8String:childKey.c_str()];
    [[_ref child:childKeySt] runTransactionBlock:^FIRTransactionResult * _Nonnull(FIRMutableData * _Nonnull currentData) {
        
        NSMutableDictionary *post = currentData.value;
        NSLog(@"ポストデータは%@::%d", post, [post isEqual:[NSNull null]]);
        
        if (!post || [post isEqual:[NSNull null]]) {
            //return [FIRTransactionResult successWithValue:currentData];
            post = @{}.mutableCopy;
        }
        
        for (auto itr = f_transactionMap.begin(); itr != f_transactionMap.end(); ++itr){
            
            NSString *iKeySt = [NSString stringWithUTF8String:itr->first.c_str()];
            NSLog(@"サイズは%d「%@」[%@]", f_transactionMap.size(), iKeySt,post);

            //サーバー上の数値
            NSNumber *num = post[iKeySt];
            
            int serverNum = (!num)? 0: [num intValue];
            
            int incrementNum = itr->second;
            NSLog(@"「%@」に「%d」増分", iKeySt, incrementNum);
            serverNum+=incrementNum;
            
            post[iKeySt] = @(serverNum);
        }
        
        currentData.value = post;
        return [FIRTransactionResult successWithValue:currentData];
    } andCompletionBlock:^(NSError * _Nullable error, BOOL committed, FIRDataSnapshot * _Nullable snapshot) {
        
        if (f_callback) {
            f_callback(!error);
        }
    }];
}


#pragma mark- ストレージ
void FireBaseBridge::getURL(const char *downloadFile, const onGetUrl&callback)
{
    __block auto f_callback = callback;
    
    // Create a reference to the file you want to download
    FIRStorageReference *starsRef = [storageRef child:[NSString stringWithUTF8String:downloadFile]];
    
    log("URLをこれから取得します。%s",downloadFile);
    // Fetch the download URL
    [starsRef downloadURLWithCompletion:^(NSURL *URL, NSError *error){
        
        if (error != nil) {
            // Handle any errors
            NSLog(@"URLを取得段階でのエラー内容[%@]",error.description);
        }
        
        auto fileUrl = [URL.absoluteString UTF8String];
        NSLog(@"firebaseから取得したURLは%@",URL.absoluteString);

        if (f_callback) {
            f_callback((error == nil), fileUrl);
       }
    }];
}

//ファイルダウンロード
void FireBaseBridge::downloadFile(const char *downloadFile, const onProgressLoadFile &progressCallback, const onFinishLoadFile &callback)
{
    __block auto f_progressCallback = progressCallback;
    __block auto f_callback = callback;

    log("iOSでは呼ばないよ%s", downloadFile);
    FIRStorageReference *fileRef = [storageRef child:[NSString stringWithUTF8String:downloadFile]];
    
    long ONE_MEGABYTE = 1024 * 1024;
    long maxSize = 50 * ONE_MEGABYTE;
    
    // Download in memory with a maximum allowed size of 1MB (1 * 1024 * 1024 bytes)
    [fileRef dataWithMaxSize:maxSize completion:^(NSData *data, NSError *error){
        NSLog(@"エラーの内容は%@",error.localizedDescription);
        if (error != nil) {
            // Uh-oh, an error occurred!
            NSLog(@"%ldダウンロードエラーの内容は%@", (long)error.code, error.localizedFailureReason);
            
            if (f_callback) {
                unsigned char*bytes = new unsigned char[0];
                std::vector<unsigned char> bytesVec(bytes, bytes + 0);
                
                f_callback(false, bytesVec, (STRAGE_ERRORCODE)error.code, [error.localizedDescription UTF8String]);
            }
        } else {
            //NSLog(@"取得したデータは%@",data);
            auto len = [data length];

            unsigned char*bytes = new unsigned char[len];
            bytes = (unsigned char*)[data bytes];
            
            std::vector<unsigned char> bytesVec(bytes, bytes + len);
            
            if (f_callback) {
                log("DL完了！！！！%lu",bytesVec.size());
                f_callback((error == nil), bytesVec, STRAGE_ERRORCODE_NONE, "");
            }
        }
    }];
    
    FIRStorageDownloadTask *task = [fileRef dataWithMaxSize:maxSize completion:^(NSData * _Nullable data, NSError * _Nullable error) {
        NSLog(@"タスクが完了しました。%@",error);
        
        if (f_progressCallback) {//進捗100%を見せる
            f_progressCallback(1);
        }
    }];
    
    FIRStorageHandle observer = [task observeStatus:FIRStorageTaskStatusProgress
                                            handler:^(FIRStorageTaskSnapshot * _Nonnull snapshot) {
                                                auto progress = [snapshot.progress fractionCompleted];
                                                
                                                log("ダウンロードの進捗は%f",progress);
                                                if (f_progressCallback) {
                                                    f_progressCallback(progress);
                                                }
                                            }
                                 ];
}

void FireBaseBridge::removeAllObserver(const char* keyName)
{
    FIRDatabaseQuery*query = [[_ref child:[NSString stringWithUTF8String:keyName]] queryOrderedByKey];
    [query removeAllObservers];
}

#pragma mark- タイムスタンプ
void FireBaseBridge::loadingTimestamp(bool keepSync)
{
    //NSLog(@"タイムスタンプは%@",[FIRServerValue timestamp]);
    FIRDatabaseReference *offsetRef = [[FIRDatabase database] referenceWithPath:@".info/serverTimeOffset"];
    
    __block bool f_keepSync = keepSync;

    [offsetRef observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot) {
        NSTimeInterval offset = [(NSNumber *)snapshot.value doubleValue];
        
        NSTimeInterval estimatedServerTimeMs = [[NSDate date] timeIntervalSince1970] * 1000.0 + offset;
        time_t now = estimatedServerTimeMs/1000;
        
        m_server_date = now;
        
        if (f_keepSync) {
            m_time_offset = time(NULL);
        }
        log("サーバー時刻の読み取り完了%ld",m_server_date);
    }];
}

time_t FireBaseBridge::getServerTimestamp()
{
    double diff = 0;
    if (m_time_offset > 0) {//同期対象
        time_t now = time(NULL);
        diff = difftime(now, m_time_offset);//タイムスタンプ読み取り完了からの経過時間
    }
    
    time_t server_date = m_server_date + diff;
    
    Common::performProcessForDebug([this, &server_date](){
        //server_date = Common::timeStampWithDate(2019, 3, 10, 12, 59, 20);
    }, nullptr);
    
    log("サーデーは%ld", server_date);
    //log("サーバーデートひづけは%ld", Common::dateWithTimeStamp(TimeUnit_Sec, server_date));
    //Common::dateWithTimeStamp(TimeUnit_Sec, server_date)
    return server_date;
}
