//
//  AutoRenewableVerification.cpp
//  EscapeRooms
//
//  Created by yamaguchinarita on 2017/11/23.
//
//

#include "AutoRenewableVerification.h"
#include "ItemStore.h"

void AutoRenewableVerification::verifySubscription(const char *subscriptionId)
{
    bool isExistExpireData = (UserDefault::getInstance()->getStringForKey(ExpiredDateKey,"") != "");
    log("定期購読の有効期限を検証をします。%s",UserDefault::getInstance()->getStringForKey(ExpiredDateKey,"").c_str());

    if (!isExistExpireData) return;//期日のデータがない場合は何もしない。

    bool isInTime = checkIsInTimeSubscription();
    
    log("定期購読の有効期限。%d",isInTime);

    if (!isInTime) {//期限外なら
        ItemStore_plugin::ItemStore::checkAppStoreExpired(subscriptionId);
    }
}

bool AutoRenewableVerification::checkIsInTimeSubscription()
{
    bool isExistExpireData = (UserDefault::getInstance()->getStringForKey(ExpiredDateKey,"") != "");

    if (!isExistExpireData) return false;
    
    auto expiredDate = atoll(UserDefault::getInstance()->getStringForKey(ExpiredDateKey).c_str());
    auto nowUnix = utils::getTimeInMilliseconds();
    
    log("購入期限日は%s::%lld\n現在日は%lld", UserDefault::getInstance()->getStringForKey(ExpiredDateKey).c_str(), expiredDate, nowUnix);
    
    bool isInTime = nowUnix <= expiredDate;
    log("定期購入の期限内であるかどうか%d", isInTime);
    
    return isInTime;
};
