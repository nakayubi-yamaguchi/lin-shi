//
//  NotificationBridge.h
//  Rocket
//
//  Created by yamaguchinarita on 2016/06/17.
//
//

#ifndef __EscapeContainer__NotificationBridge__
#define __EscapeContainer__NotificationBridge__

#include "cocos2d.h"

typedef enum {
    NotificationTag_Login=1,//1-3までの使用を想定
    NotificationTag_Stage=4,//4-6までの使用を想定
    NotificationTag_Max=7//androidにてcancelAllするさいにこのrequestCodeのnotificationまで削除する
}NotificationTag;

class NotificationBridge
{
public:
    //ローカル通知
    static void registerLocalNotificationSetting();
    static void fireLocalNotification(int time, const char *message, int identifier);
    /**idを指定して通知を削除*/
    static void cancelLocalNotification(int identifier);
    /**通知を全削除*/
    static void cancelAllLocalNotification();
    
    //リモート通知
    static void registerRemoteNotificationSetting(std::vector<std::string>topics);
    static void subscribeToTopic(const char* topic);

private:

};

#endif /* defined(__EscapeContainer__NotificationBridge__) */
