//
//  AutoRenewableVerificationM.cpp
//  EscapeRooms-mobile
//
//  Created by yamaguchinarita on 2017/11/23.
//

//検証時のステータスコード
typedef enum{
    VerifyCode_NoError=0,
    VerifyCode_InvalidReceipt,
    VerifyCode_NoBase64String,
    VerifyCode_21007,//本番環境にsandboxレシート投げた時の
    VerifyCode_ConnectionFailed
}VerifyCode;


#include "AutoRenewableVerification.h"
#include "ItemConstants.h"

//共有シークレット。iTunesConnectから引っ張ってくる。注：iTunesConnectから取得する。下記どちらを利用しても良いはず。
#define ShareSecretMasterKey "c62ddd047dab40aba3d5a336a94a84c4"//アプリ毎に変更する必要ない。
#define ShareSecretKey "d1f84668a23c44d59f607cda2b1e1618"

//サンドボックスURLと本番URL
#define SandboxUrlString "https://sandbox.itunes.apple.com/verifyReceipt"
#define UrlString "https://buy.itunes.apple.com/verifyReceipt"


void AutoRenewableVerification::verifySubscription()
{
    log("定期購読の有効期限を検証をします。");
    
}

void AutoRenewableVerification::getBase64Receipt()
{
    /**AppStoreにレシートの整合性を問い合わせるラムダ式*/
    auto getVerifyReceipt = [](const char* storeUrlString, std::function<void(VerifyCode verifyCode, NSDictionary * receiptJson)> callback){
        //ローカルからチェック
        NSString* base64String = nil;
        
        NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
        NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
        base64String = [receiptData base64EncodedStringWithOptions:0];
        
        if (base64String == nil) {//ローカルにbase64のデータがない。リストアしてー
            if (callback) {
                callback(VerifyCode_NoBase64String, nil);
            }
        }
        NSLog(@"レシートの検証します。%@",base64String);

        NSError *error;
        NSDictionary *requestContents = @{
                                          @"receipt-data" : base64String,
                                          @"password" : [NSString stringWithUTF8String:ShareSecretMasterKey]
                                          };
        
        
        NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents
                                                              options:0
                                                                error:&error];
        
        NSURL *storeURL = [NSURL URLWithString:[NSString stringWithUTF8String:storeUrlString]];
        NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
        [storeRequest setHTTPMethod:@"POST"];
        [storeRequest setHTTPBody:requestData];
        
        NSURLSessionConfiguration*configration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession*session = [NSURLSession sessionWithConfiguration:configration delegate:nil delegateQueue:[NSOperationQueue mainQueue]];
        NSLog(@"セッションをはじめるよー。");

        [[session dataTaskWithRequest:storeRequest completionHandler:^(NSData*data, NSURLResponse*responce, NSError*error)
         {
             NSLog(@"レシートの検証。");

             if (error) {
                 NSLog(@"取得エラー%@",error);
                 
                 if (callback) {
                     callback(VerifyCode_ConnectionFailed, nil);
                 }
             }
             else{
                 NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                                      options:NSJSONReadingMutableContainers
                                                                        error:nil];
                 NSLog(@"検証データ取得%@",json);
                 
                 int status = [json[@"status"] intValue];
                 NSString *error_text = json[@"error-text"];
                 
                 if (status==0 && error_text.length==0)
                 {//レシートは有効です
                     NSLog(@"レシートは有効です。");
                     if (callback) {
                         callback(VerifyCode_NoError, json);
                     }
                 }
                 else if (status==21007)
                 {//本番にサンドボックスリクエスト
                     NSLog(@"本番URLにサンドボックスの問い合わせしてるで。");
                     if (callback) {
                         callback(VerifyCode_21007, json);
                     }
                 }
                 else
                 {//不正の可能性ありレシート
                     if (callback) {
                         callback(VerifyCode_InvalidReceipt, json);
                     }
                 }
             }
             
             [session invalidateAndCancel];
         }] resume];
    };
    
    /**期限切れか確認するラムダ式*/
    auto checkExpired = [](NSDictionary* jsonDic){
     
        NSString *bundleID = jsonDic[@"receipt"][@"bundle_id"];
        NSArray* latest_receipt_info = jsonDic[@"latest_receipt_info"];

        NSLog(@"バンドルIDは%@",bundleID);
        NSLog(@"jsonの全て%dのキーは%@",[[jsonDic allKeys] count],[jsonDic allKeys]);
        
        //NSLog(@"ステータス：%d、、最新のレシート%@,レシートの数は%lu個", status, latest_receipt_info, (unsigned long)[latest_receipt_info count]);
        //NSLog(@"レシートの数は%lu個", (unsigned long)[latest_receipt_info count]);

        bool isValid = false;
        
        for (NSDictionary* receipt in latest_receipt_info) {
            
            NSString* productID = receipt[@"product_id"];
            NSString* purchaseDate = receipt[@"purchase_date_ms"];
            long long expireDate = [receipt[@"expires_date_ms"] longLongValue];
            
            auto nowTime = cocos2d::utils::getTimeInMilliseconds();

            NSLog(@"製品名::%@\n購入日::%@\n期限日::%lld\n現時間::%lld",productID, purchaseDate, expireDate,nowTime);
            
            if ([bundleID isEqualToString:[NSBundle mainBundle].bundleIdentifier] && //バンドルIDが一致
                [productID isEqualToString:[NSString stringWithUTF8String:ItemID_Subscription]] && //プロダクトIDが一致
                nowTime <= expireDate //期限内
                ) {//期限
                NSLog(@"レシート情報をゲットしています。%@",receipt);

                //ここで、期限日をユーザーデフォルトに保存する。
                
                
                isValid = true;
                break;
            }
        }
        
        NSLog(@"このレシートが有効かどうか%d",isValid);
    };
    
    /**検証結果ラムダ式*/
    auto callbackResultVerify = [checkExpired](VerifyCode code, NSDictionary* jsonDic){
        NSLog(@"コールバックを送信完了しました。");

        switch (code) {
            case VerifyCode_21007://テスト時に本番環境に問い合わせしている
                NSLog(@"21007のエラー");
                //getVerifyReceiptとcallbackResultVerifyで循環参照になりそうだから、ここでは何もしなくて良い。
                
                break;
            case VerifyCode_NoError:
                NSLog(@"エラーなし");
                checkExpired(jsonDic);

                break;
            case VerifyCode_InvalidReceipt:
                NSLog(@"無効なレシート");
                
                break;
            case VerifyCode_ConnectionFailed:
                NSLog(@"通信失敗");
                
                break;
            case VerifyCode_NoBase64String:
                NSLog(@"base64がない");
                
                break;
            default:
                //検証失敗。リトライ。
                NSLog(@"検証失敗。");
                break;
        }
    };
    
    //検証開始！
    getVerifyReceipt(UrlString, [callbackResultVerify, getVerifyReceipt](VerifyCode code, NSDictionary *jsonDic){
        
        switch (code) {
            case VerifyCode_21007://テスト時に本番環境に問い合わせしている.もう一回やり直し
                getVerifyReceipt(SandboxUrlString, callbackResultVerify);

                break;
                
            default://21007以外のコールバック
                NSLog(@"21007以外のコールバック");

                callbackResultVerify(code, jsonDic);
                
                break;
        }
    });
}
