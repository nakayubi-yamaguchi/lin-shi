//
//  ValueHelper.cpp
//  Anaguma
//
//  Created by yamaguchinarita on 2016/06/25.
//
//

#include "ValueHelper.h"
#include "Common.h"

using namespace cocos2d;

#pragma mark - ValueMap
bool ValueHelper::isExistsKey(const cocos2d::ValueMap &map,
                              const std::string &key)
{
    return (map.find(key) != map.end());
}

bool ValueHelper::isExistsKey(const cocos2d::ValueMapIntKey &map,
                              const int key)
{
    return (map.find(key) != map.end());
}

bool ValueHelper::isEqualMap(const cocos2d::ValueMap &map1, const cocos2d::ValueMap &map2)
{
    // 要素数が違えばもう違う
    if (map1.size() != map2.size()) return false;
    
    /**** 以下、map1とmap2の要素数が等しいことは保証される *****/
    
    // 要素数が等しいことは保証されているので、value1の全ての要素について
    //   - 同じキーが有る
    //   - そのキーに対する値が等しい
    // なら同じ。
    for (std::pair<std::string, Value> pair : map1) {
        const std::string key        = pair.first;
        const Value       childValue = pair.second;
        // 一つでも違えば違う。
        if (!ValueHelper::isExistsKey(map2, key)
            ||
            !ValueHelper::isEqual(childValue, map2.at(key))) return false;
    }
    
    return true;
}

bool ValueHelper::isEqualIntKeyMap(const cocos2d::ValueMapIntKey &map1, const cocos2d::ValueMapIntKey &map2)
{
    // 要素数が違えばもう違う
    if (map1.size() != map2.size()) return false;
    
    /**** 以下、map1とmap2の要素数が等しいことは保証される *****/
    
    // 要素数が等しいことは保証されているので、value1の全ての要素について
    //   - 同じキーが有る
    //   - そのキーに対する値が等しい
    // なら同じ。
    for (std::pair<int, Value> pair : map1) {
        const int key          = pair.first;
        const Value childValue = pair.second;
        // 一つでも違えば違う。
        if (!ValueHelper::isExistsKey(map2, key)
            ||
            !ValueHelper::isEqual(childValue, map2.at(key))) return false;
    }
    
    return true;
}

#pragma mark - Value
void ValueHelper::dumpValue(const cocos2d::Value &value)
{
    Common::performProcessForDebug([value]{
        log("%s", ValueHelper::getDumpString(value,1).c_str());
    }, nullptr);
}

std::string ValueHelper::getDumpString(const cocos2d::Value &value,
                                       unsigned int level /* = 1 */)
{
    std::string ret;
    
    switch (value.getType()) {
        case Value::Type::NONE:    ret = "NONE,\n";                                                                 break;
        case Value::Type::BYTE:    ret = StringUtils::format("BYTE(%c),\n",    value.asByte());                     break;
        case Value::Type::INTEGER: ret = StringUtils::format("INTEGER(%d),\n", value.asInt());                      break;
        case Value::Type::FLOAT:   ret = StringUtils::format("FLOAT(%f),\n",   value.asFloat());                    break;
        case Value::Type::DOUBLE:  ret = StringUtils::format("DOUBLE(%f),\n",  value.asDouble());                   break;
        case Value::Type::BOOLEAN: ret = StringUtils::format("BOOLEAN(%s),\n", value.asBool() ? "true" : "false");  break;
        case Value::Type::STRING:  ret = StringUtils::format("STRING(%s),\n",  value.asString().c_str());           break;
            
        case Value::Type::VECTOR:
            ret += "VECTOR(" + StringUtils::toString((int)value.asValueVector().size()) + ")(\n";
            for (Value child : value.asValueVector()) {
                // 階層によるインデント
                for (int i=0; i<level; i++) ret += "    ";
                ret += ValueHelper::getDumpString(child, level+1);
            }
            // 階層によるインデント
            for (int i=0; i<level-1; i++) ret += "    ";
            ret += "),\n";
            break;
            
        case Value::Type::MAP:
            ret += "MAP(" + StringUtils::toString((int)value.asValueMap().size()) + "){\n";
            for (ValueMap::const_iterator itr = value.asValueMap().begin(); itr != value.asValueMap().end(); itr++) {
                // 階層によるインデント
                for (int i=0; i<level; i++) ret += "    ";
                ret += StringUtils::format("\"%s\": ", (*itr).first.c_str()) + ValueHelper::getDumpString((*itr).second, level+1);
            }
            // 階層によるインデント
            for (int i=0; i<level-1; i++) ret += "    ";
            ret += "},\n";
            break;
            
        case Value::Type::INT_KEY_MAP:
            ret += "INT_KEY_MAP(" + StringUtils::toString((int)value.asIntKeyMap().size()) + "){\n";
            for (ValueMap::const_iterator itr = value.asValueMap().begin(); itr != value.asValueMap().end(); itr++) {
                // 階層によるインデント
                for (int i=0; i<level; i++) ret += "    ";
                ret += StringUtils::format("\"%s\": ", (*itr).first.c_str()) + ValueHelper::getDumpString((*itr).second, level+1);
            }
            // 階層によるインデント
            for (int i=0; i<level-1; i++) ret += "    ";
            ret += "},\n";
            break;
            
        default:
            //NERROR("引数 value の getType() の値が不正です。: %d", value.getType());
            break;
    }
    
    return ret;
}

bool ValueHelper::isEqual(const cocos2d::Value &value1, const cocos2d::Value &value2)
{
    // 型が違えばもう違う
    if (value1.getType() != value2.getType()) return false;
    
    /***** 以下、value1とvalue2の型が等しいことは保証される *****/
    
    switch (value1.getType()) {
        case Value::Type::NONE:        return true;     // 中身が無いので比較する必要無し
        case Value::Type::BYTE:        return (value1.asByte()   == value2.asByte());
        case Value::Type::INTEGER:     return (value1.asInt()    == value2.asInt());
        case Value::Type::FLOAT:       return (value1.asFloat()  == value2.asFloat());
        case Value::Type::DOUBLE:      return (value1.asDouble() == value2.asDouble());
        case Value::Type::BOOLEAN:     return (value1.asBool()   == value2.asBool());
        case Value::Type::STRING:      return (value1.asString() == value2.asString());
        case Value::Type::VECTOR:      return ValueHelper::isEqualVector(value1.asValueVector(), value2.asValueVector(), false);
        case Value::Type::MAP:         return ValueHelper::isEqualMap(value1.asValueMap(), value2.asValueMap());
        case Value::Type::INT_KEY_MAP: return ValueHelper::isEqualIntKeyMap(value1.asIntKeyMap(), value2.asIntKeyMap());
    }
    
    return false;
}

#pragma mark - ValueVector
bool ValueHelper::isEqualVector(const cocos2d::ValueVector vector1,
                                const cocos2d::ValueVector vector2,
                                const bool considerOrder /* = false */)
{
    // 要素数が違えばもう違う
    if (vector1.size() != vector2.size()) return false;
    
    /***** 以下、vector1とvector2の要素数が等しいことは保証される *****/
    
    if (considerOrder) {
        // 順序を考慮する場合
        // vector1, vector2の同じ要素番号に格納されている値が、全て同じなら同じ。
        for (unsigned int i=0; i<vector1.size(); i++)
            // 一つでも違えば違う。
            if (!ValueHelper::isEqual(vector1.at(i), vector2.at(i))) return false;
    }
    else {
        // 順序を考慮しない場合
        // 要素数が等しいことは保証されているので、value1の要素が全てvalue2に入っていれば同じ。
        for (Value childValue1 : vector1)
            // 一つでも違えば違う。
            if (!ValueHelper::isContainsValue(vector2, childValue1)) return false;
    }
    
    return true;
}

bool ValueHelper::isContainsValue(const cocos2d::ValueVector &vector, const cocos2d::Value &value)
{
    for (Value comparison : vector)
        // 同値が見つかった時点でtrueを返却
        if (ValueHelper::isEqual(value, comparison)) return true;
    
    return false;
}

#pragma mark - std::vector
std::vector<float> ValueHelper::sort(std::vector<float> nums,bool ascend)
{
    //挿入ソートを使用
    for (auto itr=nums.begin()+1; itr!=nums.end(); ++itr) {
        auto num=*itr;
        auto itr_insert=itr;
        for (auto itr_compare=itr-1; itr_compare>=nums.begin(); --itr_compare) {
            auto num_compare=*itr_compare;
            //log("@@@%.1fと%.1fを比較",num,num_compare);
            if (num_compare>num) {
                //log("***%.1fは%.1fより大きいのでinsertIndexを入れ替え",num_compare,num);
                itr_insert=itr_compare;
            }
            
             //ソートを実行
            if (itr_compare==nums.begin()) {
                nums.erase(itr);
                nums.insert(itr_insert, num);
            }
        }
    }
    
    if (!ascend) {
        std::reverse(nums.begin(), nums.end());
    }
    
    return nums;
}

bool ValueHelper::isContainsValue(std::vector<std::string> vector, std::string string)
{
    if (vector.size()==0) {
        return false;
    }

    auto itr=std::find(vector.begin(), vector.end(), string);
    auto index=std::distance(vector.begin(), itr);
    if (index==vector.size()) {
        //見つからなかった
        return false;
    }
    return true;
}


bool ValueHelper::isContainsValue(std::vector<int> vector, int intValue)
{
    if (vector.size()==0) {
        return false;
    }
    
    auto itr=std::find(vector.begin(), vector.end(), intValue);
    auto index=std::distance(vector.begin(), itr);
    if (index==vector.size()) {
        //見つからなかった
        return false;
    }
    return true;
}

#pragma mark std::string
std::string ValueHelper::substr(std::string str, int location)
{
    int pos;
    unsigned char lead;//unsigned char 0〜255(符号無し)
    int char_size;
    int charNum = 0;//文字数
    
    std::string string="";
    
    auto counter=0;
    for (pos = 0; pos < str.length(); pos += char_size) {
        charNum++;
        lead = str[pos];
        
        //        if (lead < 0x80) {
        //            char_size = 1;
        //        } else if (lead < 0xE0) {
        //            char_size = 2;
        //        } else if (lead < 0xF0) {
        //            char_size = 3;
        //        } else {
        //            char_size = 4;
        //        }
        
        //16進数と10進数
        //        9=9
        //        a=10
        //        b=11
        //        c=12
        //        d=13
        //        e=14
        //        f = 15
        if (lead < 128) {
            char_size = 1;
        } else if (lead < 224) {
            char_size = 2;
        } else if (lead < 240) {
            char_size = 3;
        } else {
            char_size = 4;
        }
        
        //log("%s",str.substr(pos, char_size).c_str());
        //log("lead:%d",lead);
        //log("pos:%d char_size:%d", pos, char_size);
        
        if (counter==location) {
            string=str.substr(pos, char_size);
            break;
        }
        
        counter++;
    }
    
    return string;
}
