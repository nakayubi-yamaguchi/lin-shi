//
//  AutoRenewableVerification.h
//  EscapeRooms
//
//  Created by yamaguchinarita on 2017/11/23.
//
//

#include "cocos2d.h"

USING_NS_CC;

class AutoRenewableVerification
{
public:
    /**定期購読の有効期限を確認する。期限外なら確認後に検証する。起動時に呼ぶ。*/
    static void verifySubscription(const char* subscriptionId);
    
    /**定期購読の期限日内かどうか確認。プレミアム機能の利用可否判定で使う。falseの時は、[購入]、[復元]ボタンへの導線を貼るのがおすすめ。*/
    static bool checkIsInTimeSubscription();

    static void getBase64Receipt();
};

