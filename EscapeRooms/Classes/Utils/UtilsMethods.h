//
//  UtilsMethods.hpp
//  EscapeRooms-mobile
//
//  Created by yamaguchinarita on 2018/02/06.
//

#ifndef UtilsMethods_h
#define UtilsMethods_h

#include "cocos2d.h"

USING_NS_CC;
using namespace std;

using onBecomeFriendCallback = std::function<void()>;

//LINE@もカカオトークも同じキーで保存。LINE@とカカオ同時登録は無いため。
#define Past_Promote_Key_LINEAt "PromoteLine@"//iOS3.2 android3.4まで使用。
#define Promote_Key_LINEAt "PromoteLine@Modified"//
#define Promote_Key_IsLINEAtFriend "IsFriendPromoteLine@"

/*typedef enum{
    BeginnersPackType_Recommend,
    BeginnersPackType_Big,
    BeginnersPackType_Small
}BeginnersPackType;*/

#define UserKey_PromoteBeginnersPack "PromoteBeginers"//

/**他のクラスを用いたUI関連便利系メソッドたち.Commonとの違い→他のクラスをincludeしまくっていい。循環参照を避ける用。*/
class UtilsMethods
{
public:

    /**LINE@への友達促進。表示頻度とかも管理。友達になった際のコールバック付き*/
    static void showPromoteBecomingFriends(int addBonus, const onBecomeFriendCallback& becomeFriendCallback);
    
    /**LINE@への促進アラート。*/
    static void showPromoteBecomingFriendsPopup();
    
    /**LINE@への促進アラート。コールバック付き*/
    static void showPromoteBecomingFriendsPopup(int addBonus, const onBecomeFriendCallback& becomeFriendCallback);
    
    /**初心者パック催促*/
    static void showBeginnersPackRecommendAlert(const ccMenuCallback&callback);
    
    /**お礼アラート*/
    static void showThanksAlert(const char* message, const ccMenuCallback& callback);

};

#endif /* UtilsMethods_hpp */
