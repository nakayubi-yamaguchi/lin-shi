//
//  ValueHelper.h
//  Anaguma
//
//  Created by yamaguchinarita on 2016/06/25.
//
//

#ifndef __Anaguma__ValueHelper__
#define __Anaguma__ValueHelper__

#include "cocos2d.h"

class ValueHelper : public cocos2d::Node
{
public:
#pragma mark - ValueMap
    static bool isExistsKey(const cocos2d::ValueMap &map, const std::string &key);
    static bool isExistsKey(const cocos2d::ValueMapIntKey &map, const int key);
    static bool isEqualMap(const cocos2d::ValueMap &map1, const cocos2d::ValueMap &map2);
    static bool isEqualIntKeyMap(const cocos2d::ValueMapIntKey &map1, const cocos2d::ValueMapIntKey &map2);

#pragma mark - Value
    static void dumpValue(const cocos2d::Value &value);
    static std::string getDumpString(const cocos2d::Value &value, unsigned int level /* = 1 */);
    static bool isEqual(const cocos2d::Value &value1, const cocos2d::Value &value2);
    
#pragma mark - ValueVector
    static bool isEqualVector(const cocos2d::ValueVector vector1, const cocos2d::ValueVector vector2, const bool considerOrder /* = false */);
    static bool isContainsValue(const cocos2d::ValueVector &vector, const cocos2d::Value &value);

#pragma mark - std::vector
    /**要素を並び替える*/
    static std::vector<float> sort(std::vector<float>nums,bool ascend);
    /**要素を含んでいるかチェック*/
    static bool isContainsValue(std::vector<std::string> vector, std::string string);
    static bool isContainsValue(std::vector<int> vector, int intValue);

#pragma mark - std::string
    /**マルチバイト対応の切り抜き 1文字のみ*/
    static std::string substr(std::string str,int location);
};

#endif /* defined(__Anaguma__ValueHelper__) */
