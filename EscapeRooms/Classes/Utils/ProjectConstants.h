//
//  ProjectConstants.h
//  EscapeRooms
//
//  Created by yamaguchi on 2019/02/20.
//

#ifndef ProjectConstants_h
#define ProjectConstants_h


#pragma mark - Firebase
#define StrageUrl "gs://escapecontainer-c7579.appspot.com"//　コンテナ

#pragma mark - Common
#define DebugMode 2//注 本番→0。アナリティクス,Firebaseディレクトリ以外本番環境→1。完全デバッグ環境→2。広告表示 event送信管理。
#define LocalMode 0//注 本番→0。アトラス使用時→1。TexturePackerフォルダ使用→2。
//*映画館Remakeより以前のものは2に対応していない可能性あり。
#define LocalVersionMode 0//注 本番→0。ローカルのversion.plistを使用→1。
#define ReviewVersion 120//Version.plistのバージョン指定

#define LocalResourcesDir "LocalResources/%s_resource/"
#define LocalTitlesResourceDir "Titles/%s_resource/"//本番用のローカルリソース置き場
#define LocalVersionDir "LocalVersion/"

#define LocalTestTitle "farm"
#define PerformanceKey "nakayubiperformance"

#define MyFont "fonts/TanukiMagic.ttf"
#define MyLightFont "fonts/TanukiMagic.ttf"
#define MyTitleFont "fonts/TanukiMagic.ttf"
#define LocalizeFont "fonts/NotoSansCJKkr-DemiLight.otf"
#define HiraginoMaruFont "fonts/HGRSMP.TTF"

#define MyColor "36ade0"//"a6d9de"
#define BlackColor "383838"
#define DarkGrayColor "444444"
#define GrayColor "919191"

#define LightBlackColor "595757"
#define WhiteColor "e7e7e7"
#define WhiteColor2 "fafafa"
#define BlueColor "3497f8"
#define RedColor "f04024"
#define RedColor2 "cd2b26"
#define MagendaColor "ef5af7"
#define OrangeColor "f59736"
#define OrangeColor2 "eb8334"
#define GreenColor "5ad542"
#define GreenColor2 "009193"
#define YellowColor "fffb00"
#define YellowColor2 "eed348"


#define ClickSoundName "click.mp3"
#define ClickSmallSoundName ""

#define AutoPromoteHint 60*5 //5分で自動ヒント

#define TwitterConsumeKey "xNlkwBOGHfQIIZb9cyAiIcvhJ"
#define TwitterSecretKey "mWpK2MrtO3EqGcgQARn68TIbVZgU6VZfCUYv2y5eCMh4iZw1qb"

#define MyURL "http://nakayubi-corp.jp/escaperooms.html"// コンテナ
#define MyHintURL "http://nakayubi-corp.jp/2017/04/17/yashiki-hints-1/"// 忍者

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#define ReviewURL "https://play.google.com/store/apps/details?id=%s"
#define DeveloperURL "https://play.google.com/store/apps/developer?id=アプリ開発まん"
#define AppID "com.ryohei.haruki.EscapeRooms" //　コンテナ

#else
#define ReviewURL "itms-apps://itunes.apple.com/app/id/%s?action=write-review"
#define DeveloperURL "https://itunes.apple.com/jp/developer/ryohei-narita/id826300169"
#define AppID "1296686566"//コンテナ

#endif


#endif /* ProjectConstants_h */
