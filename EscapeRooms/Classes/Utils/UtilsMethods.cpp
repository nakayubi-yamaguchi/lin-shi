//
//  UtilsMethods.cpp
//  EscapeRooms-mobile
//
//  Created by yamaguchinarita on 2018/02/06.

#include "UtilsMethods.h"
#include "ContentsAlert.h"
#include "NativeBridge.h"
#include "Common.h"
#include "DataManager.h"
#include "../ShopMenuLayer.h"
#include "../AnalyticsManager.h"

#define Promote_Interval_BeginnersPack 4


#pragma mark- 友達になる促進

//(iOS:~3.2 android:~3.4で利用)リリース時は15と3
#define Past_Promote_Interval_LINEAt 15
#define Past_Promote_MaxCount_LINEAt 3

#if (DebugMode)==0//本番用
//(iOS:3.3~ android:3.5~で利用)リリース時は8と4
#define Promote_Interval_LINEAt 8
#define Promote_MaxCount_LINEAt 4

#else//デバッグ用
//(iOS:3.3~ android:3.5~で利用)
#define Promote_Interval_LINEAt 1
#define Promote_MaxCount_LINEAt 8
#endif

void UtilsMethods::showPromoteBecomingFriends(int addBonus, const onBecomeFriendCallback &becomeFriendCallback)
{
    //既に友達なら、以後何もしない。
    auto isFriend = UserDefault::getInstance()->getBoolForKey(Promote_Key_IsLINEAtFriend);
    if (isFriend)
        return;
    
    //現バージョン（iOS:3.3~ android:3.5~）で使用しているLINE@へ促進するための呼び出し回数
    int promoteCount = UserDefault::getInstance()->getIntegerForKey(Promote_Key_LINEAt);
    
    //過去バージョン(iOS:~3.2 android:~3.4)で使用していたLINE@へ促進するための呼び出し回数
    int pastPromoteCount = UserDefault::getInstance()->getIntegerForKey(Past_Promote_Key_LINEAt);

    //初回呼び出しは必ず促進アラートが表示されるので、-1する。
    int maxPromoteCount = Promote_Interval_LINEAt*(Promote_MaxCount_LINEAt-1);
    
    //前バージョンまでで既に規定の回数促進した、または前バージョンで既に友達。
    if (pastPromoteCount == Past_Promote_Interval_LINEAt * Past_Promote_MaxCount_LINEAt+1)
        return;
    
    //interval回に1回line@でのサポートを促進する。
    if (promoteCount%Promote_Interval_LINEAt == 0 && promoteCount <= maxPromoteCount) {
        //log("プロモートカウ%d",promoteCount);
        showPromoteBecomingFriendsPopup(addBonus, becomeFriendCallback);
    }
    
    promoteCount++;
    UserDefault::getInstance()->setIntegerForKey(Promote_Key_LINEAt, promoteCount);
}

void UtilsMethods::showPromoteBecomingFriendsPopup()
{
    showPromoteBecomingFriendsPopup(0, nullptr);
}

void UtilsMethods::showPromoteBecomingFriendsPopup(int addBonus, const onBecomeFriendCallback &becomeFriendCallback)
{
    log("友達追加でライン友達");
    std::string message = DataManager::sharedManager()->getSystemMessage("promoteFriendsMessage");
    
    if (addBonus > 0) {
        auto addMessage = DataManager::sharedManager()->getSystemMessage("promoteFriendsMessageWithBonus");
        message = StringUtils::format(addMessage.c_str() ,message.c_str(), addBonus);
    }
    
    auto title = DataManager::sharedManager()->getSystemMessage("promoteFriendsTitle");
    auto noText = DataManager::sharedManager()->getSystemMessage("promoteFriendsButton_No");
    auto yesText = DataManager::sharedManager()->getSystemMessage("promoteFriendsButton_Yes");

    auto contentsAlert = ContentsAlert::create("line@.png", title.c_str(), message.c_str(), {noText,yesText}, [becomeFriendCallback](Ref* sender){
        auto alert = (ContentsAlert*)sender;
        
        int selectedNum = alert->getSelectedButtonNum();
        
        if (selectedNum == 1) {//友達になる
            
            //日本と韓国以外はここ通らないはず。
            auto url = Common::localize(
                                        "",
                                        "https://line.me/R/ti/p/%40dpy0194n",
                                        "",
                                        "",
                                        "https://pf.kakao.com/_atCpC");
            
            NativeBridge::openUrl(url.c_str());
            
            //友達になったら、データ保存
            Common::performProcessOnlyRelease([](){
                UserDefault::getInstance()->setBoolForKey(Promote_Key_IsLINEAtFriend, true);
            });
            
            if (becomeFriendCallback) {
                becomeFriendCallback();
            }
        }
        log("%d番目のボタンがおされました。",selectedNum);
    });
    log("友達追加でライン友達２");

    
    contentsAlert->showAlert();
}
#pragma mark - 初心者パック
void UtilsMethods::showBeginnersPackRecommendAlert(const cocos2d::ccMenuCallback &callback)
{
    int promoteCount = UserDefault::getInstance()->getIntegerForKey(UserKey_PromoteBeginnersPack,0);
    
    if (promoteCount%Promote_Interval_BeginnersPack>0) {
        return;
    }
    
    UserDefault::getInstance()->setIntegerForKey(UserKey_PromoteBeginnersPack, promoteCount+1);
    
    auto title=DataManager::sharedManager()->getSystemMessage("beginnersPackTitle");
    auto message=DataManager::sharedManager()->getSystemMessage("beginnersPackRecommend");
    
    auto spr=Sprite::createWithSpriteFrameName("beginners_alert.png");
    auto alert=ContentsAlert::create(spr,title, message, {"NO","YES"}, callback);
    
    alert->showAlert();
    
    /*auto present=Sprite::createWithSpriteFrameName("present.png");
    present->setScale(spr->getContentSize().height*.4/present->getContentSize().height);
    //present->setPosition(spr->getContentSize().width*.665,spr->getContentSize().height*.65);
    spr->addChild(present);
    
    auto coin_image=Sprite::createWithSpriteFrameName("coin_image.png");
    coin_image->setScale(coin_image->getContentSize().width*.4/coin_image->getContentSize().height);
    spr->addChild(coin_image);
    
    
    auto shop=ShopMenuLayer::create(nullptr);
    auto coin_count=shop->getTotalCoinsByBeginnersPack(true);
    auto coin_label=Label::createWithTTF(StringUtils::format("x %d",coin_count), HiraginoMaruFont, coin_image->getBoundingBox().size.height*.6);
    coin_label->setTextColor(Color4B::BLACK);
    spr->addChild(coin_label);
    
    auto width_coin_container=coin_image->getBoundingBox().size.width+coin_label->getContentSize().width;
    auto coin_container_pos=Vec2(spr->getContentSize().width*.665, spr->getContentSize().height*.65);
    coin_image->setPosition(coin_container_pos.x-width_coin_container/2+coin_image->getBoundingBox().size.width/2,coin_container_pos.y);
    coin_label->setPosition(coin_container_pos.x+width_coin_container/2-coin_label->getContentSize().width/2,coin_container_pos.y-coin_image->getBoundingBox().size.height*.4+coin_label->getContentSize().height/2);
    
    auto discount=shop->getDiscountRateByBeginnersPack(true);
    auto discount_label=Label::createWithTTF(StringUtils::format("SAVE %.0f%%",discount), MyFont, coin_image->getBoundingBox().size.height*.4);
    discount_label->setTextColor(Color4B::RED);
    discount_label->setPosition(coin_container_pos.x+width_coin_container/2-discount_label->getContentSize().width/2, coin_container_pos.y+coin_image->getBoundingBox().size.height/2);
    spr->addChild(discount_label);*/

    
}

#pragma mark -
void UtilsMethods::showThanksAlert(const char *message, const ccMenuCallback &callback)
{
    auto alert = ContentsAlert::create("purchase.png", "INFORMATION", message, {"OK"}, callback);
    alert->showAlert();
}
