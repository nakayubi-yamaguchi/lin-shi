//
//  BannerBridge.cpp
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/04/22.
//
//

#include "BannerBridge.h"
#include <jni.h>
#include "platform/android/jni/JniHelper.h"
#include "Utils/Common.h"


//パッケージ名を指定
#define CLASS_NAME "org/cocos2dx/cpp/BannerBridge"

using namespace cocos2d;

void BannerBridge::configure() {
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "configure",
                                        "(Ljava/lang/String;)V")) {
        assert(false);
        return;
    }
    jstring utfAppId = methodInfo.env->NewStringUTF(CONFIGURE_APPID);

    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, utfAppId);

    //解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    methodInfo.env->DeleteLocalRef(utfAppId);//★jstringは解放が必要です。

}

#pragma mark- 動画パネル広告

void BannerBridge::configureMovieNative() {}

void BannerBridge::loadingMovieNativeAd() {}

void BannerBridge::showMovieNativeAd(cocos2d::Vec2 point, cocos2d::Size nativeSize) {}

void BannerBridge::hideMovieNativeAd() {}

#pragma mark- パネル広告

void
BannerBridge::createNativeAd(const char *bannerID, cocos2d::Vec2 point, cocos2d::Size nativeSize) {
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "createNativeAd",
                                        "(Ljava/lang/String;FFFF)V")) {
        assert(false);
        return;
    }

    jstring utfAppId = methodInfo.env->NewStringUTF(bannerID);

    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, utfAppId, point.x,
                                         point.y, nativeSize.width, nativeSize.height);

    //解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    methodInfo.env->DeleteLocalRef(utfAppId);//★jstringは解放が必要です。
}

void BannerBridge::showPanelAd(const char *bannerID) {
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "showPanelAd",
                                        "(Ljava/lang/String;)V")) {
        assert(false);
        return;
    }

    jstring utfAppId = methodInfo.env->NewStringUTF(bannerID);


    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, utfAppId);

    //解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    methodInfo.env->DeleteLocalRef(utfAppId);//★jstringは解放が必要です。
}

void BannerBridge::hidePanelAd(const char *bannerID) {
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "hidePanelAd",
                                        "(Ljava/lang/String;)V")) {
        assert(false);
        return;
    }

    jstring utfAppId = methodInfo.env->NewStringUTF(bannerID);


    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, utfAppId);

    //解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    methodInfo.env->DeleteLocalRef(utfAppId);//★jstringは解放が必要です。
}

void BannerBridge::hideAllPanelAd() {
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "hideAllPanelAd", "()V")) {
        assert(false);
        return;
    }


    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);

    //解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void BannerBridge::removeAllPanelAd() {
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "removeAllPanelAd", "()V")) {
        assert(false);
        return;
    }

    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);

    //解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

#pragma mark- バナー広告

float BannerBridge::getBannerWidth() {
    cocos2d::JniMethodInfo t;
    float bannerWidth;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "getBannerWidth", "()F")) {
        jfloat width = (jfloat) t.env->CallStaticFloatMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);

        bannerWidth = width;
    }

    return bannerWidth;
}

float BannerBridge::getBannerHeight() {
    cocos2d::JniMethodInfo t;
    float bannerHeight;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "getBannerHeight", "()F")) {

        jfloat height = (jfloat) t.env->CallStaticFloatMethod(t.classID, t.methodID);

        t.env->DeleteLocalRef(t.classID);

        bannerHeight = height;
    }

    return bannerHeight;
}

float BannerBridge::getRectangleHeight() {
    cocos2d::JniMethodInfo t;
    float rectangleHeight;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "getRectangleHeight", "()F")) {

        jfloat height = (jfloat) t.env->CallStaticFloatMethod(t.classID, t.methodID);

        t.env->DeleteLocalRef(t.classID);

        rectangleHeight = height;
    }

    return rectangleHeight;
}

void BannerBridge::createAdxBanner(const char *adUnitID, bool isBottom, int tag) {
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "createAdxBanner",
                                        "(Ljava/lang/String;ZI)V")) {
        assert(false);
        return;
    }

    jstring utfAdUnitID = methodInfo.env->NewStringUTF(adUnitID);

    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, utfAdUnitID,
                                         isBottom, tag);
    //解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    methodInfo.env->DeleteLocalRef(utfAdUnitID);//★jstringは解放が必要です。
}

void BannerBridge::createBanner(const char *bannerID, Vec2 position, int tag) {
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "createBanner",
                                        "(Ljava/lang/String;FFI)V")) {
        assert(false);
        return;
    }

    jstring utfAppId = methodInfo.env->NewStringUTF(bannerID);

    //positionをandroid仕様（左上アンカーポイント）に変換する。
    position.x = position.x - getBannerWidth() / 2;
    position.y = Director::getInstance()->getRunningScene()->getContentSize().height -
                 (position.y + getBannerHeight() / 2);

    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, utfAppId,
                                         position.x, position.y, tag);

    //解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    methodInfo.env->DeleteLocalRef(utfAppId);//★jstringは解放が必要です。
}

void BannerBridge::createRectangle(const char *bannerID, Vec2 position) {
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "createRectangle",
                                        "(Ljava/lang/String;FF)V")) {
        assert(false);
        return;
    }

    jstring utfAppId = methodInfo.env->NewStringUTF(bannerID);

    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, utfAppId,
                                         position.x, position.y/*0, 0*/);
    //解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    methodInfo.env->DeleteLocalRef(utfAppId);//★jstringは解放が必要です。
}

void BannerBridge::hideBanner(const char *bannerID) {
    log("バナー広告を隠します__Test_Banner");

    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "hideBanner",
                                        "(Ljava/lang/String;)V")) {
        assert(false);
        return;
    }

    jstring utfAppId = methodInfo.env->NewStringUTF(bannerID);

    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, utfAppId);

    //解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    methodInfo.env->DeleteLocalRef(utfAppId);//★jstringは解放が必要です。
}

void BannerBridge::removeBanner(const char *bannerID) {
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "removeBanner",
                                        "(Ljava/lang/String;)V")) {
        assert(false);
        return;
    }

    jstring utfAppId = methodInfo.env->NewStringUTF(bannerID);


    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, utfAppId);

    //解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    methodInfo.env->DeleteLocalRef(utfAppId);//★jstringは解放が必要です。
}

void BannerBridge::removeBanner() {
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "removeBanner", "()V")) {
        assert(false);
        return;
    }

    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);

    //解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

#pragma mark - Admobネイティブアドバンス広告

void
BannerBridge::loadingNativeAdvanceAd(std::map<const char *, const char *> nativeAdvanceIDsMap) {
    /*
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "loadingNativeAdvanceAd", "([Ljava/lang/String;[Ljava/lang/String;)V")){
        assert(false);
        return;
    }
    
    jobjectArray jUnitIds;
    jobjectArray jvalues;
    jkeys=t.env->NewObjectArray(len_key,_class,NULL);
    jvalues=t.env->NewObjectArray(len_key,_class,NULL);
    for (int i=0; i<len_key; i++) {
        t.env->SetObjectArrayElement(jkeys,i,t.env->NewStringUTF(keys[i]));
        t.env->SetObjectArrayElement(jvalues,i,t.env->NewStringUTF(values[i]));
    }
    
    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, utfAppId);
    
    //解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    methodInfo.env->DeleteLocalRef(utfAppId);//★jstringは解放が必要です。
     */
}

void BannerBridge::showNativeAdvanceAd(const char *adUnitID, cocos2d::Vec2 point,
                                       cocos2d::Size nativeSize) {

}

void BannerBridge::hideNativeAdvanceAd(const char *adUnitID) {

}

void BannerBridge::hideAllNativeAdvanceAd() {

}
