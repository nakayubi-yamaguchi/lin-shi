//
//  BannerBridge.h
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/04/22.
//
//

#ifndef __Regitaro2__BannerBridge__
#define __Regitaro2__BannerBridge__


#include "cocos2d.h"
#include "AdConstants.h"

USING_NS_CC;

typedef enum {
    BannerType_Admob = 0,
    BannerType_DFP
} BannerType;


typedef enum {
    BannerPosition_Bottom = 0,
    BannerPosition_Top
} BannerPosition;

class BannerBridge {
public:
    static void configure();

#pragma mark - 動画パネル広告

    static void configureMovieNative();

    static void loadingMovieNativeAd();

    static void showMovieNativeAd(cocos2d::Vec2 point, cocos2d::Size nativeSize);

    static void hideMovieNativeAd();

#pragma mark - パネル広告

    static void createNativeAd(const char *bannerID, cocos2d::Vec2 point, cocos2d::Size nativeSize);

    static void showPanelAd(const char *bannerID);

    static void hidePanelAd(const char *bannerID);

    static void hideAllPanelAd();

    static void removeAllPanelAd();

#pragma mark - バナー広告

    /**スマートバナーの幅を取得*/
    static float getBannerWidth();

    /**スマートバナーの高さを取得*/
    static float getBannerHeight();

    /**レクタングルの高さを取得*/
    static float getRectangleHeight();

    static void createBanner(const char *bannerID, BannerPosition bannerPosition) {
        createBanner(bannerID, bannerPosition, 0);
    };

    /**バナーをTopかBottomか指定して表示する。*/
    static void createBanner(const char *bannerID, BannerPosition bannerPosition, int tag) {
        Vec2 pos;
        auto contentSize = Director::getInstance()->getRunningScene()->getContentSize();
        switch (bannerPosition) {
            case BannerPosition_Top:
                pos = Vec2(contentSize.width / 2, contentSize.height - getBannerHeight() / 2);
                break;

            case BannerPosition_Bottom:
                pos = Vec2(contentSize.width / 2, getBannerHeight() / 2);
                break;

            default:
                log("TOPとBOTTOM以外を指定したので、強制クラッシュさせる。");
                assert(false);
                break;
        };
        BannerBridge::createBanner(bannerID, pos, tag);
    };

    static void createBanner(const char *bannerID, Vec2 position) {
        createBanner(bannerID, position, 0);
    };

    static void createAdxBanner(const char *adUnitID, BannerPosition bannerPosition) {
        createAdxBanner(adUnitID, bannerPosition, 0);
    }

    static void createAdxBanner(const char *adUnitID, BannerPosition bannerPosition, int tag) {
        bool isBottom = false;
        switch (bannerPosition) {
            case BannerPosition_Top:
                isBottom = false;
                break;
            case BannerPosition_Bottom:
                isBottom = true;
                break;
            default:
                log("TOPとBOTTOM以外を指定したので、強制クラッシュさせる。");
                assert(false);
                break;
        };
        BannerBridge::createAdxBanner(adUnitID, isBottom, tag);
    }

    /**バナーを座標指定で表示する。アンカーポイントはバナーの中心。Cocos仕様。*/
    static void createBanner(const char *bannerID, Vec2 position, int tag);

    static void createRectangle(const char *bannerID, Vec2 position);

    static void createAdxBanner(const char *adUnitID, bool isBottom, int tag);

    static void moveTo(cocos2d::Vec2 point);

    static void hideBanner(const char *bannerID);

    static void removeBanner(const char *bannerID);

    static void removeBanner();

#pragma mark - Admobネイティブアドバンス広告

    /**利用予定のネイティブアドバンス広告を全て読み込み.*/
    static void loadingAllNativeAdvanceAd() {
        loadingNativeAdvanceAd(AdConstants::getNativeUnitIdAndNibNameMap());
    };

    /**引数は{KEY:広告ユニットID,VALUE:nibファイル名}のmap。利用予定のネイティブアドバンス広告を読み込み.*/
    static void loadingNativeAdvanceAd(std::map<const char *, const char *> nativeAdvanceIDsMap);

    /**ネイティブ広告の読み込み完了していたら、フレームを設定して表示する。*/
    static void
    showNativeAdvanceAd(const char *adUnitID, cocos2d::Vec2 point, cocos2d::Size nativeSize);

    /**該当広告枠を非表示にする。*/
    static void hideNativeAdvanceAd(const char *adUnitID);

    /**全てのネイティブアドバンス広告枠を非表示にする。*/
    static void hideAllNativeAdvanceAd();

};

#endif /* defined(__Regitaro2__BannerBridge__) */
