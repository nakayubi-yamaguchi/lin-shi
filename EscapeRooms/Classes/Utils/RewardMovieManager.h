//
//  RewardMovieManager.h
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/03/27.
//
//

#ifndef __Regitaro2__RewardMovieManager__
#define __Regitaro2__RewardMovieManager__

#include "cocos2d.h"

#define SWALLOW_TOUCH "swallowtouchlayer"

#include "../RewardPlayer/RewardPlayer.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "Utils/AdmobRewardBridge.h"
#include "Adfurikun/ADFMovieReward.h"
#include "Adfurikun/RewardCocosConstants.h"

#else
#include "AdmobRewardBridge.h"
#include "ADFMovieReward.h"
#include "RewardCocosConstants.h"

#endif

USING_NS_CC;
typedef std::function<void(bool completed)> onFinished;

class RewardMovieManagerDelegate
{
public:
    virtual void didCloseRewardMovie(bool complete) = 0;
};

class RewardMovieManager:public Adfurikun::ADFMovieRewardDelegate, RewardPlayerDelegate
{
public:
    static RewardMovieManager* manager;
    
    //デリゲートを受け取りたいクラスが解放される際にデリゲートにNullを代入すること
    CC_SYNTHESIZE(RewardMovieManagerDelegate*, delegate, Delegate);
    
    //メソッド
    static RewardMovieManager* sharedManager();
    
    void loading();
    bool isPrepared();
    void play();
    void play(const onFinished& callback);
    
    //動画広告コールバック
    virtual void prepareSuccess(const char * appID);
    virtual void startPlaying(const char * appID, const char * adnetworkKey);
    virtual void finishedPlaying(const char * appID);
    virtual void failedPlaying(const char * appID);
    virtual void adClose(const char * appID);
    
    //広告出し分けプレイヤーコールバック
    virtual void didStartReward();
    virtual void didCloseReward(bool completed);

private:
    bool m_complete;
    onFinished m_callback;
    
    void addTouchLayer();
    
    void removeTouchLayer();
    
    void init();
    
    /**リワード動画が終了した時に、各アドネットワークのコールバックから呼び出す。*/
    void didCloseReward();

    ~RewardMovieManager();
};

#endif /* defined(__Regitaro2__RewardMovieManager__) */
