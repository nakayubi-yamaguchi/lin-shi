//
//  AdmobRewardBridge.cpp
//  Yashiki
//
//  Created by yamaguchinarita on 2017/04/12.
//
//

#include "Utils/AdmobRewardBridge.h"
#include <jni.h>
#include "platform/android/jni/JniHelper.h"

#define CLASS_NAME "org/cocos2dx/cpp/AdmobRewardBridge"

using namespace cocos2d;

static AdmobRewardBridgeDelegate* sRewardCallback = NULL;

void AdmobRewardBridge::setDelegate(AdmobRewardBridgeDelegate *var)
{
    sRewardCallback = var;
}

void AdmobRewardBridge::loadingAdmobReward(){
    //jniでローディングを呼ぶ
    cocos2d::JniMethodInfo t;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "loadingAdmobReward", "(Ljava/lang/String;)V"))
    {
        auto rewardId = DebugMode == 0? MOVIE_REWARD_ADMOB_APPID : MOVIE_REWARD_ADMOB_APPID_MEDIATION_TEST;
        
        jstring utfAppId = t.env->NewStringUTF(rewardId);
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID, utfAppId);
        
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(utfAppId);//★jstringは解放が必要です。
    }
}

bool AdmobRewardBridge::isReady()
{
    cocos2d::JniMethodInfo t;
    bool isPrepare;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "isReady", "()Z"))
    {
        jboolean ready = (jboolean)t.env->CallStaticBooleanMethod(t.classID, t.methodID);
        
        t.env->DeleteLocalRef(t.classID);

        isPrepare = ready;
    }
    
    log("動画広告の準備状況%d",isPrepare);
    return isPrepare;
}

void AdmobRewardBridge::show()
{
    cocos2d::JniMethodInfo t;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "show", "()V"))
    {
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}

#ifdef __cplusplus
extern "C"
{
#endif
    JNIEXPORT void Java_org_cocos2dx_cpp_AdmobRewardBridge_didStart(JNIEnv *env,jobject thiz)
    {
        if(sRewardCallback != NULL)
        {
            sRewardCallback->didStart();
        }
        return;
    }
   
    JNIEXPORT void Java_org_cocos2dx_cpp_AdmobRewardBridge_didClose(JNIEnv *env,jobject thiz, jboolean completed)
    {
        if(sRewardCallback != NULL)
        {
            sRewardCallback->didClose(completed);
        }
        
        //広告を見終わったら自動でローディング。
        AdmobRewardBridge::loadingAdmobReward();
        
        return;
    }

#ifdef __cplusplus
}
#endif

