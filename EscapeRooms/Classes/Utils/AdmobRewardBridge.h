//
//  AdmobRewardBridge.h
//  Yashiki
//
//  Created by yamaguchinarita on 2017/04/12.
//
//

#ifndef __Yashiki__AdmobRewardBridge__
#define __Yashiki__AdmobRewardBridge__

#include "cocos2d.h"
#include "AdConstants.h"
#include "Common.h"

#define LOG_ADMOB_REWARD "アドモブのリワード広告"

class AdmobRewardBridgeDelegate
{
public:
    
    virtual void didStart(){};
    virtual void didClose(bool completed){};
};

class AdmobRewardBridge
{
public:
    static void loadingAdmobReward();
    static bool isReady();
    static void show();
    
    static void setDelegate(AdmobRewardBridgeDelegate* delegate);
    static void logOfBridge(std::string text);
    
};

#endif /* defined(__Yashiki__AdmobRewardBridge__) */
