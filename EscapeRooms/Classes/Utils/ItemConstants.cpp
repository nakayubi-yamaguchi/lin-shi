//
//  ItemConstants.cpp
//  EscapeRooms-mobile
//
//  Created by yamaguchinarita on 2018/02/21.
//

#include "ItemConstants.h"


std::vector<std::string> ItemConstants::getItemIds()
{
    static std::vector<std::string> itemIds =
    {
        ItemID_PresentPackBig,
        ItemID_PresentPackSmall,
        ItemID_126Coin,
        ItemID_165Coin,
        ItemID_330Coin,
        ItemID_690Coin,
        ItemID_1035Coin,
        ItemID_1800Coin,
        ItemID_3540Coin,
        ItemID_30Coin,
        ItemID_90Coin
    };
    
    
    return itemIds;
}

std::vector<std::string> ItemConstants::getItemIds(std::vector<const char*> searchTexts)
{
    std::vector<std::string> searchVec;
    
    for (auto itemId : getItemIds()) {
        for (auto searchText : searchTexts) {
            log("%s検索:%s",searchText,itemId.c_str());

            if (itemId.find(searchText) != std::string::npos) {
                log("あった%s検索:%s",searchText,itemId.c_str());
                searchVec.push_back(itemId);
                break;//検索ワードで重複防止のため
            }
        }
    }
    
    return searchVec;
}

