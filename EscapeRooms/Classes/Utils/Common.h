//
//  Common.h
//  Nekotsumi
//
//  Created by 成田凌平 on 2016/01/16.
//

#ifndef __Rocket__Common__
#define __Rocket__Common__

#include "cocos2d.h"
#include "NativeBridge.h"
#include "ProjectConstants.h"


#define PerformanceKey "nakayubiperformance"


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#define SystemOS "android"

#else
#define SystemOS "iOS"

#endif



USING_NS_CC;
using namespace std;

//デバッグ時の挙動:DebugMode=2
using onDebugAction = std::function<void()>;

//セミデバッグ時の挙動:DebugMode=0または1
using onSemiDebugAction = std::function<void()>;

//リリース時の挙動:DebugMode=0
using onReleaseAction = std::function<void()>;

typedef enum {
    TimeUnit_Year = 0,
    TimeUnit_Month,
    TimeUnit_Day,
    TimeUnit_Hour,
    TimeUnit_Min,
    TimeUnit_Sec
}TimeUnit;

class Common
{
public:
    static string localize(string en,string ja);
    static string localize(string en,string ja,string ch_simple);//簡体字つき
    static string localize(string en,string ja,string ch_simple,string ch_traditional,string korean);//簡体字、繁体字ハングルつき
    static string getLocalizeText(ValueMap map);

    static string localizeURL(string url);

    static string getLocalizeCode();//localeに応じて ja en ch ko twを返す
    static string getLocalizeKey(string apendString);//言語に応じて_en,_ch,_koをつけて返す
    static string getUsableFontPath(string fontPath);//使いたいfontを渡す ko chiなら対応してるフォントを返す
    /**英語を複数形に変換*/
    static string transformToPlural(int num , string singular, string plural);
    
    static float getDensity();
    static bool isIPad();
    static bool isAndroid();
    
    //サウンド
    static void playBGM(const char *fileName);
    static void playBGM(const char *fileName , bool roop);
    static void playBGM(const char *fileName , bool roop,float volume);

    static void resumeBGM();
    static void stopBgm();

    static int playSE(const char*fileName);
    static void stopAllSE();

    static void playClick();
    static void playClickSmallButton();

    static void allFreeze();
    
    //色
    static Color3B getColorFromHex(const string hex);
    static int  getNumberFromHex(const string hex, int rangeFrom);

    //シェア
    static void shareWithImage(std::string shareText,ShareType shareType);
 
    //ストリング関連
    static bool hasPrefix(const std::string& s, const char* v);
    static std::string getReviewUrl();
    static std::vector<std::string> splitString(std::string str, std::string del);

    /**ラベルサイズ調整*/
    static void sizeFit(Label*label,float maxWidth);
    
    //アクション
    static CallFunc* createSoundAction(std::string fileName);
    
    /**時間を日付までのlonglong型に変換.変換後の値は[年-1900][月-1][日]*/
    static long long transformTimeToDate(time_t tm);
    
    /**引数はタイムスタンプ。yyyyMMDD~の形に変換*/
    static long long dateWithTimeStamp(TimeUnit validTimeUnit, time_t tm);
    
    /**
     引数タイムスタンプからの経過時間を返す。
     @param validTimeUnit 必要な時間単位
     @param tm タイムスタンプ
     @param intervalTime 経過秒
     @return 経過後のタイムスタンプ
     */
    static long long dateWithTimeInterval(TimeUnit validTimeUnit, time_t tm, int intervalTime);
    
    /**
     日付の詳細から、検証して正しい日付（long）を返す。取得した方で不要な桁は割ってやる。
     @param year 年
     @param month 月
     @param day 日
     @param hour 時
     @param minutes 分
     @param seconds 秒
     @return YYYYMMDDHHSS
     */
    static long long dateWithDateDetail(TimeUnit validTimeUnit, int year, int month, int day, int hour, int minutes, int seconds);
    
    /**タイムスタンプを取得.年月日*/
    static time_t timeStampWithDate(int year, int month, int day);
    
    /**タイムスタンプを取得.年月日時*/
    static time_t timeStampWithDate(int year, int month, int day, int hour);
    
    /**タイムスタンプを取得.年月日時分*/
    static time_t timeStampWithDate(int year, int month, int day, int hour, int minutes);
   
    /**タイムスタンプを取得.年月日時分秒*/
    static time_t timeStampWithDate(int year, int month, int day, int hour, int minutes, int seconds);
    
    /**beforeDateからafterDateまでの時間を計算*/
    static long long differenceTime(long long beforeDate, long long afterDate);
    
    /**20180919の形式の日付から、月日Stringを取得*/
    static string getMonthDateSt(int intdate);
    
    /**簡易的な非同期処理*/
    static void performAsyncTask(const std::function<void()>& asyncTask, const std::function<void()>& mainThreadTask);
    
#pragma mark - デバッグ用
    /**デバッグ用の処理とリリース用の処理を判別.onDebugAction→DebugMode=2,onSemiDebugAction→DebugMode=2以外の処理*/
    static void performProcessForDebug(const onDebugAction& debugCallback, const onSemiDebugAction& releaseCallback);
    /**デバッグ用の処理とリリース用の処理を判別 1または2の場合処理する*/
    static void performProcessForDebug(const onDebugAction& debugCallback);
    /**デバッグ用の処理とリリース用の処理を判別 1の場合処理する*/
    static void performProcessForDebug_1(const onDebugAction& debugCallback);
    /**リリース時のみ処理を行う。アナリティクス、ディレクトリとかで使う。DebugMode=0の時のみ行う処理*/
    static void performProcessOnlyRelease(const onReleaseAction& releaseCallback);

    static std::string getLocalTestDir();
    
    /**実行速度計測用。開始時間を記録。*/
    static void startTimeForDebug();
    /**複数箇所の実行速度計測用。開始時間を記録。*/
    static void startTimeForDebug(const char* debugKey);
    
    /**実行速度計測用。終了時間を記録。*/
    static void stopTimeForDebug();
    /**複数箇所の実行速度計測用。終了時間を記録。*/
    static void stopTimeForDebug(const char* debugKey);
private:
    
};

#endif /* defined(__Nekotsumi__Common__) */
