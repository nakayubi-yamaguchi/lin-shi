//
//  AlertBridge.cpp
//  Nekotsumi
//
//  Created by 成田凌平 on 2016/01/18.
//
//

#include "AlertBridge.h"
#include <jni.h>
#include "platform/android/jni/JniHelper.h"

//パッケージ名を指定
#define CLASS_NAME "org/cocos2dx/cpp/AlertBridge"

namespace native_plugin {
    
    //コールバッククラスを一時的に保持するクラス変数を用意しとくよ
    static AlertBridgeDelegate* sMessageBoxCallback = NULL;
    
    void AlertBridge::showAlert(AlertBridgeDelegate*delegate,int tag, const char* title, const char* msg, const char* firstBtn, ...) {
        
        log("アラートを作るデーー");
        //delegateをセット
        if(sMessageBoxCallback != delegate){
           
            /*if(sMessageBoxCallback != NULL){
                delete sMessageBoxCallback;
            }*/
            sMessageBoxCallback = delegate;
        }
        
        //ぼたんをセット
        va_list ap;
        va_start(ap, firstBtn);
        
        const char* button1;
        const char* button2;
        
        button1 = va_arg(ap, const char*);
        if (button1 == NULL) {
            button2 = NULL;
        } else {
            button2 = va_arg(ap, const char*);
        }
        va_end(ap);
        
        //jni
        JniMethodInfo t;
        
        if (!JniHelper::getStaticMethodInfo(t, CLASS_NAME, "showAlert", "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V")) {
            assert(false);
            return;
        }
        
        jstring titleArg = t.env->NewStringUTF(title);
        jstring msgArg = t.env->NewStringUTF(msg);
        jstring firstArg=t.env->NewStringUTF(firstBtn);
        jstring but1Arg=t.env->NewStringUTF(button1);
        jstring but2Arg=t.env->NewStringUTF(button2);
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID,tag,titleArg,msgArg,firstArg,but1Arg,but2Arg);
        t.env->DeleteLocalRef(titleArg);// ★jstringは解放が必要です。
        t.env->DeleteLocalRef(msgArg);// ★jstringは解放が必要です。
        t.env->DeleteLocalRef(firstArg);// ★jstringは解放が必要です。
        t.env->DeleteLocalRef(but1Arg);// ★jstringは解放が必要です。
        t.env->DeleteLocalRef(but2Arg);// ★jstringは解放が必要です。
        
        
        t.env->DeleteLocalRef(t.classID);
        
        log("アラートを表示デーー");
    }
    
    #ifdef __cplusplus
    extern "C"
    {
    #endif
            JNIEXPORT void Java_org_cocos2dx_cpp_AlertBridge_alertClosedCPP(JNIEnv *env,jobject thiz,jint tag,jint num)
            {
                int num_ = num;
                int tag_=tag;
                if(sMessageBoxCallback != NULL)
                {
                    sMessageBoxCallback->alertDismissed(tag_,num_);
                   
                    /*delete sMessageBoxCallback;
                    sMessageBoxCallback = NULL;*/
                }
                return;
            }
        
    #ifdef __cplusplus
    }
    #endif
}



