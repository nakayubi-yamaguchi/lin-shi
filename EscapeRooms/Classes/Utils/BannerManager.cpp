//
//  BannerManager.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2019/04/08.
//
//

#include "BannerManager.h"
#include "NativeBridge.h"
#include "Common.h"

using namespace cocos2d;

BannerManager* BannerManager::manager =NULL;

#pragma mark- イニシャライズ
BannerManager* BannerManager::getInstance()
{
    //If the singleton has no instance yet, create one
    if(NULL == manager)
    {
        //Create an instance to the singleton
        manager = new BannerManager();

    }
    
    //Return the singleton object
    return manager;
}


void BannerManager::createBanner(BannerPosition bannerPosition)
{
    Common::performProcessForDebug([this](){
        bannerType = BannerType_DFP;
        log("強制的にAdxにする");
    }, nullptr);
    
    switch (bannerPosition) {
        case BannerPosition_Top:{
            if (bannerType == BannerType_DFP) {
                log("AdXヘッダーを表示します。");
                BannerBridge::createAdxBanner(BANNER_ADX_HEADER_AD_UNIT_ID, BannerPosition_Top);
            }
            else {
                log("Admobヘッダーを表示します。");
                BannerBridge::createBanner(NativeBridge::judgeID(BANNER_APPID_HEADER, BANNER_APPID_HEADER_TABLET), BannerPosition_Top);
            }
            break;
        }
        
        case BannerPosition_Bottom:{
            if (bannerType == BannerType_DFP) {
                log("AdXフッターを表示します");
                BannerBridge::createAdxBanner(BANNER_ADX_FOOTER_AD_UNIT_ID, BannerPosition_Bottom);
            }
            else {
                log("Admobフッターを表示します。");
                BannerBridge::createBanner(NativeBridge::judgeID(BANNER_APPID_FOOTER, BANNER_APPID_FOOTER_TABLET), BannerPosition_Bottom);
            }
            break;
        }
        default:
            break;
    }
}


void BannerManager::removeBanner(BannerPosition bannerPosition)
{
    switch (bannerPosition) {
        case BannerPosition_Top:{
            if (bannerType == BannerType_DFP) {
                log("AdXヘッダーを表示します。");
                BannerBridge::removeBanner(BANNER_ADX_HEADER_AD_UNIT_ID);
            }
            else {
                log("Admobヘッダーを表示します。");
               
                auto bannerID = NativeBridge::judgeID(BANNER_APPID_HEADER, BANNER_APPID_HEADER_TABLET);
                BannerBridge::removeBanner(bannerID);
            }
            break;
        }
            
        case BannerPosition_Bottom:{
            if (bannerType == BannerType_DFP) {
                log("AdXフッターを表示します");
                BannerBridge::removeBanner(BANNER_ADX_FOOTER_AD_UNIT_ID);
            }
            else {
                log("Admobフッターを表示します。");
                BannerBridge::removeBanner(NativeBridge::judgeID(BANNER_APPID_FOOTER, BANNER_APPID_FOOTER_TABLET));
            }
            break;
        }
        default:
            break;
    }
}

BannerType BannerManager::getBannerType()
{
    return bannerType;
}

void BannerManager::setBannerType(BannerType type)
{
    //タブレットなら強制Admob
    bannerType = NativeBridge::isTablet()? BannerType_Admob : type;
    
}
