//
//  AdConstants.cpp
//  EscapeRooms-mobile
//
//  Created by yamaguchinarita on 2017/10/16.
//

#include "AdConstants.h"

#include "Common.h"

std::string AdConstants::getPanelID(const char *key)
{
    static std::map<const std::string, const std::string> panelConstants=
    {//タイトル名_plistのキーの形式になっているので一致することはまずない。
        //タイトル名より後ろ→panelAd.plistのIDKeyと一致していることを確認すること。
        //共通
        {"a_small",PANEL_A_SMALL},
        {"a_big",PANEL_A_BIG},
        {"b_small",PANEL_B_SMALL},
        {"b_big",PANEL_B_BIG},
        {"c_small",PANEL_C_SMALL},
        {"c_big",PANEL_C_BIG},
        
        //寺
        {"temple_toilet",PANEL_A_SMALL},
        {"temple_jizo",PANEL_A_BIG},
        {"temple_shoesbox",PANEL_B_BIG},
        
        //ハロウィン
        {"halloween_pumpkin",PANEL_A_SMALL},
        {"halloween_pumpkinup",PANEL_A_BIG},
        {"halloween_game",PANEL_B_SMALL},
        {"halloween_gameup",PANEL_B_BIG},
        
        //子供部屋
        {"kidsroom_bed",PANEL_A_SMALL},
        {"kidsroom_bedup",PANEL_A_BIG},
        {"kidsroom_blue",PANEL_B_SMALL},
        {"kidsroom_blueup",PANEL_B_BIG},
        
        //東京
        {"tokyo_shelf",PANEL_A_SMALL},
        {"tokyo_shelfup",PANEL_A_BIG},
        {"tokyo_drawer",PANEL_B_BIG},
        {"tokyo_pc",PANEL_C_BIG},
        
        //ゲストハウス
        {"guesthouse_shelf",PANEL_A_SMALL},
        {"guesthouse_shelfup",PANEL_A_BIG},
        {"guesthouse_drawer",PANEL_B_BIG}
    };
    
    return panelConstants[key];
}

std::map<const char*, const char*> AdConstants::getNativeUnitIdAndNibNameMap()
{
    static std::map<const char*, const char*> nativeAdvanceConstants=
    {//nibファイル名は好きなものにしていい。例：nibファイル名がSmallTV_ContentAdView→SmallTVをセット。_以降はBannerBridgeでセット。
        {NATIVEADVANCE_SMALL, "SmallTV"},
        {NATIVEADVANCE_BIG,"BigTV"}
    };
    
    return nativeAdvanceConstants;
}

std::string AdConstants::getNativeAdvanceUnitId(const char *panelUnitID)
{
    std::string nativeUnitId;
    
    log("パネル広告のIDは%s",panelUnitID);
    if (!strcmp(panelUnitID, PANEL_A_SMALL) || !strcmp(panelUnitID, PANEL_B_SMALL) ||  !strcmp(panelUnitID, PANEL_C_SMALL)) {
        nativeUnitId = NATIVEADVANCE_SMALL;
    }
    else {//小さいやつ以外
        nativeUnitId = NATIVEADVANCE_BIG;
    }
    
    return nativeUnitId;
}

