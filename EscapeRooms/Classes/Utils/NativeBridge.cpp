//
//  NativeBridge.cpp
//  Rocket
//
//  Created by 成田凌平 on 2016/02/21.
//
//

#include "NativeBridge.h"
#include <jni.h>
#include "platform/android/jni/JniHelper.h"

//パッケージ名を指定
#define CLASS_NAME "org/cocos2dx/cpp/NativeBridge"

#pragma mark- openURL
void NativeBridge::openUrl(const char*url)
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "openUrl", "(Ljava/lang/String;)V")) {
     // ★パラメータがStringの場合はjstringに変換する
     // java側は「String」にする。UTF8変換
     jstring stringArg1 = t.env->NewStringUTF(url);
     
     t.env->CallStaticVoidMethod(t.classID, t.methodID,stringArg1);
     t.env->DeleteLocalRef(stringArg1);// ★jstringは解放が必要です。
     
     t.env->DeleteLocalRef(t.classID);
     }
}

#pragma mark- シェア
void NativeBridge::openShareMenu(const char *tweet)
{//java
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "postTweet", "(Ljava/lang/String;)V")) {
        // ★パラメータがStringの場合はjstringに変換する
        // java側は「String」にする。UTF8変換
        jstring stringArg1 = t.env->NewStringUTF(tweet);
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID,stringArg1);
        t.env->DeleteLocalRef(stringArg1);// ★jstringは解放が必要です。
        
        t.env->DeleteLocalRef(t.classID);
    }
}

void NativeBridge::openShareMenu(const char *tweet,const char* path)
{//java
    // ネイティブコードのツイート関数を取得する
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "postTweet", "(Ljava/lang/String;Ljava/lang/String;)V"))
    {
        assert(false);
        return;
    }
    
    // 文字列をJava Stringに変換する
    jstring message_jstr = methodInfo.env->NewStringUTF(tweet);
    jstring imagepath_jstr = methodInfo.env->NewStringUTF(path);
    
    // 関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID , message_jstr, imagepath_jstr);
    
    // リソースを解放する
    methodInfo.env->DeleteLocalRef(message_jstr);
    methodInfo.env->DeleteLocalRef(imagepath_jstr);
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void NativeBridge::openShareMenu(ShareType shareType, const char *tweet, const char *path)
{
    const char* packageName;
    
    switch (shareType) {
        case ShareType_Activity:
            NativeBridge::openShareMenu(tweet,path);
            return;
            break;
            
        case ShareType_Twitter:
            packageName="com.twitter.android";
            break;
            
        case ShareType_FaceBook:
            packageName="com.facebook.katana";
            break;
            
        case ShareType_LINE:
            packageName="jp.naver.line.android";
            break;
            
        default:
            break;
    }
    
    // ネイティブコードのシェア関数を取得する
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "share", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V"))
    {
        assert(false);
        return;
    }
    
    // 文字列をJava Stringに変換する
    jstring package_jstr=methodInfo.env->NewStringUTF(packageName);
    jstring message_jstr = methodInfo.env->NewStringUTF(tweet);
    jstring imagepath_jstr = methodInfo.env->NewStringUTF(path);
    
    // 関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID , package_jstr,message_jstr, imagepath_jstr);
    
    // リソースを解放する
    methodInfo.env->DeleteLocalRef(message_jstr);
    methodInfo.env->DeleteLocalRef(imagepath_jstr);
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void NativeBridge::openWithTwitter(const char *tweet, const char *path)
{
}

void NativeBridge::openWithFaceBook(const char *tweet, const char *path)
{
}

void NativeBridge::openWithLINE(const char *path)
{
}


std::string url_encode(const std::string &value) {
    std::ostringstream escaped;
    escaped.fill('0');
    escaped << std::hex;
    
    for (auto i = value.begin(); i != value.end(); ++i) {
        unsigned char c = (*i);
        
        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
            escaped << c;
            continue;
        }
        
        escaped << '%' << std::setw(2) << int(c);
    }
    
    return escaped.str();
}


void NativeBridge::openWithLINEText(const std::string& message)
{
    std::string encodedMessage = url_encode(message);
    std::string urlString = cocos2d::StringUtils::format(LINE_URL_SCHEME, CONTENT_TEXT, encodedMessage.c_str());
    cocos2d::Application::getInstance()->openURL(urlString);
}

#pragma mark - 設定言語
std::string NativeBridge::getCurrentLanguage()
{
    std::string ret;
    
    cocos2d::JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "getCurrentLanguage", "()Ljava/lang/String;")){
        return "error";
    }
    
    jobject objResult = methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
    ret = cocos2d::JniHelper::jstring2string((jstring)objResult); // jstringをstd::stringに変換
    methodInfo.env->DeleteLocalRef(objResult);
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    
    return ret;
}

#pragma mark- ローカル通知

void NativeBridge::registerNotificationSetteing()
{
    
}

void NativeBridge::fireLocalNotification(int time ,const char* message)
{
    log("%d秒後に通知発行",time);
    
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "showLocalNotification", "(Ljava/lang/String;II)V")) {
        return;
    }
    
    // 文字列をJava Stringに変換する
    jstring stringArg = methodInfo.env->NewStringUTF(message);
    
    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, stringArg, time, 1);

    //リソースを解放
    methodInfo.env->DeleteLocalRef(stringArg);
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void NativeBridge::cancelAllLocalNotification()
{
    log("通知をキャンセル");
    
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "cancelAllLocalNotification", "()V")) {
        return;
    }
    
    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
    
    //リソースを解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

#pragma mark- +1ボタン
void NativeBridge::createPlusOneButton(float x, float y, float width, float height)
{
    //log("プラスワンボタンを生成します。{%f,%f,%f,%f}",x,y,width,height);
    
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "createPlusOneButton", "(FFFF)V")) {
        return;
    }
    
    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID,x,y,width,height);
    
    //リソースを解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void NativeBridge::removePlusOneButton()
{
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "removePlusOneButton", "()V")) {
        return;
    }
    
    //関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
    
    //リソースを解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

#pragma mark- 端末判定
bool NativeBridge::isTablet()
{
    JniMethodInfo methodInfo;
    bool isTablet;
    
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "isTablet", "()Z"))
    {
        assert(false);
        return "";
    }
    
    // 関数を呼び出す
    jboolean jIsTablet = (jboolean)methodInfo.env->CallStaticBooleanMethod(methodInfo.classID, methodInfo.methodID);
    // 解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    
    isTablet = jIsTablet;
    
    return isTablet;
}

const char* NativeBridge::judgeID(const char* smartPhoneID, const char* tabletID)
{
    if (!isTablet())
        return smartPhoneID;
    
    return tabletID;
}

std::string NativeBridge::getOSVersion()
{
    JniMethodInfo methodInfo;
    std::string osVersion;

    
    log("これからストレージの容量");
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "getOSVersion", "()Ljava/lang/String;")){
        assert(false);
        return 0;
    }
    
    // 関数を呼び出す
    jobject objResult = methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
    osVersion = cocos2d::JniHelper::jstring2string((jstring)objResult); // jstringをstd::stringに変換
   
    methodInfo.env->DeleteLocalRef(objResult);
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    
    return osVersion;
}

#pragma mark- ストレージ
unsigned long long NativeBridge::storageAvailableMb()
{
    JniMethodInfo methodInfo;
    int jFreeSpace;
    
    log("これからストレージの容量");
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "availableStorageMB", "()I")){
        assert(false);
        return 0;
    }
    
    // 関数を呼び出す
    jint jFree = (jint)methodInfo.env->CallStaticIntMethod(methodInfo.classID, methodInfo.methodID);
    // 解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    
    jFreeSpace = jFree;
    
    return jFreeSpace;
}
#pragma mark- オンライン確認
bool NativeBridge::isOnline()
{
    JniMethodInfo methodInfo;
    bool isConnect;
    
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "isOnline", "()Z")){
        assert(false);
        return "";
    }
    
    // 関数を呼び出す
    jboolean jIsOnline = (jboolean)methodInfo.env->CallStaticBooleanMethod(methodInfo.classID, methodInfo.methodID);
    // 解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    
    isConnect = jIsOnline;
    
    return isConnect;
}

#pragma mark- レビュー求む
bool NativeBridge::requestReview()
{
    return false;
}

#pragma mark- 3点カンマ
std::string NativeBridge::transformThreeComma(float num)
{
    JniMethodInfo methodInfo;
    std::string commaSt;
    
    log("これから3点カンマをつける");
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "transformThreeComma", "(F)Ljava/lang/String;")){
        log("これないぜ");

        assert(false);
        
        return 0;
    }
    
    // 関数を呼び出す
    jobject objResult = methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID, num);
    commaSt = cocos2d::JniHelper::jstring2string((jstring)objResult); // jstringをstd::stringに変換
    
    methodInfo.env->DeleteLocalRef(objResult);
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    
    return commaSt;
}

#pragma mark- バイブレーション
void NativeBridge::vibration()
{
    if (!SettingDataManager::sharedManager()->getStopVibe()) {
        JniMethodInfo methodInfo;
        if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "vibration", "()V")) {
            return;
        }
        
        //関数を呼び出す
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
        
        //リソースを解放
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
        
    }
    
}

void NativeBridge::vibration3D()
{
    if (!SettingDataManager::sharedManager()->getStopVibe()) {
    }
}

#pragma mark- UUID生成
std::string NativeBridge::createUUID()
{
    std::string str;
    
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "createUUID", "()Ljava/lang/String;"))
    {
        assert(false);
        return NULL;
    }
    
    // 関数を呼び出す
    // メソッド呼び出し。今回はStatic関数かつ、String型が戻り値なので CallStaticObjectMethod を使う
    jstring jpath  = (jstring)methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
    
    //文字列変換
    const char* npath = methodInfo.env->GetStringUTFChars(jpath, NULL);
    str = npath;
    
    // 解放
    methodInfo.env->ReleaseStringUTFChars(jpath, npath);
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
    
    return str;
}
