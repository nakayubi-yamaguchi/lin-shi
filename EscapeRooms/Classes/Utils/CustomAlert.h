//
//  CustomAlert.h
//  Wedding
//
//  Created by yamaguchinarita on 2017/04/25.
//
//

#ifndef __Wedding__CustomAlert__
#define __Wedding__CustomAlert__

#include "cocos2d.h"

USING_NS_CC;

//よく使うアラートのタイトル。よく使うものはここにまとめる。
#define AlertTitle_INFORMATION "INFORMATION"

using onTappedClose = std::function<void()>;


class CustomAlert : public cocos2d::LayerColor
{
public:
    CREATE_FUNC(CustomAlert);
    static CustomAlert* create(std::string title, std::string message, std::vector<std::string> buttons, const ccMenuCallback &callback);
    /**ボタンを押下した瞬間のcallback付き*/
    static CustomAlert* create(std::string title, std::string message, std::vector<std::string> buttons, const ccMenuCallback &callback, const ccMenuCallback &callback_pushButton);
    /**ボタン縦配置*/
    static CustomAlert* create(std::string title, std::string message, std::vector<std::string> buttons,bool isVertical, const ccMenuCallback &callback, const ccMenuCallback &callback_pushButton);
    
    virtual bool init();
    virtual bool init(std::string title, std::string message, std::vector<std::string> buttons, const ccMenuCallback &callback);
    virtual bool init(std::string title, std::string message, std::vector<std::string> buttons,bool isVertical, const ccMenuCallback &callback, const ccMenuCallback &callback_pushButton);
    
    CC_SYNTHESIZE(int, selectedButtonNum, SelectedButtonNum);
    CC_SYNTHESIZE(bool, disableButton, DisableButton);
    CC_SYNTHESIZE(bool, isVertical, IsVertical);
    
    ~CustomAlert();
    
    float getMessageLineHeight();
    void setMessageLineHeight(float lineHeight);
    
    /**ボタンが選択されて、コールバックが呼ばれるまでの時間*/
    void setCloseDuration(float duration);
    
    /**ボタンが選択された時の音声ファイル*/
    void setSoundName(std::string fileName);
    
    /**アラートのフォントを設定します。*/
    virtual void setAlertFont(std::string fontName);
    
    /**ボタン色*/
    virtual void setButtonColor(Color3B color,int index);
    
    /**
     @brief 右上にクローズボタンをつける
     @param const char* (fileName) クローズボタンにする任意のファイル名
     */
    void displayCloseButton(const char* fileName);
    
    /**右上にクローズボタンをつける*/
    void displayCloseButton();
    
    /**右上にクローズボタンをつける.閉じるボタンが押された場合のコールバックもあるよ*/
    void displayCloseButton(const onTappedClose& callback);

    /**タイトルのテキストカラーを変更する*/
    void setTitleColor(const char* colorCode);
    
    /**メッセージのテキストカラーを変更する*/
    void setMessageColor(const char* colorCode);
    
    /**指定したノードにアラートを貼り付けて、アニメーションさせる*/
    void showAlert(Node* parent);
    void showAlert(Node* parent,const ccMenuCallback& callback);
    
    /**表示しているシーンにアラートを貼りつけて、アニメーション表示*/
    void showAlert();
    /**表示しているシーンにアラートを貼りつけて、アニメーション表示*/
    void showAlert(const ccMenuCallback& callback);
    
    /**閉じた時のコールバック*/
    void setCallback(const ccMenuCallback& callback);
    
    /**ボタン選択時のコールバック*/
    void setSelectedButtonCallback(const ccMenuCallback& callback);

protected:
    
    Sprite* mainSprite;
    Label* titleLabel;
    Label* messageLabel;
    Vector<MenuItem*> m_menuItems;

    std::string mTitle;
    std::string mMessage;
    std::vector<std::string> mButtons;
    ccMenuCallback mCallback;
    ccMenuCallback mCallback_pushButton;
    onTappedClose m_closeCallback;

    float m_closeDuration;
    std::string m_soundName;
    int m_backOpacity;
    Size buttonSize;
    
    //最小の色付きレイヤーの高さ
    cocos2d::Size minSize;
    
    /**タイトルラベル中心からてっぺんまで。メニューの中心から底辺まで。*/
    float space;
    float buttonSpace;
    
    //ラベルのフォントを変更する。
    void setLabelConfig(std::string fontName, Label* label);
    
    //UI
    virtual void createMain();
    
    //各パーツを配置する。
    virtual void arrange();
    
    //タッチ
    void addTouchListener();
    
    //表示アニメーション
    void showAnimation(const ccMenuCallback&callback);
    virtual void closeAnimation(bool withButtonCallback);
};

#endif /* defined(__Wedding__CustomAlert__) */
