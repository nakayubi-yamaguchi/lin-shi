//
//  RewardMovieManager.cpp
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/03/27.
//

#include "ShakeGestureManager.h"
#include "CommonNotificationKeys.h"
#include "AccelerometerFilter.h"
#include "NativeBridge.h"
#include "Common.h"

#define NotificationKeyShakeListener "notificationkeyshakelistener"

ShakeGestureManager* ShakeGestureManager::manager=NULL;

#pragma mark- 初期化

ShakeGestureManager* ShakeGestureManager::getInstance()
{
    //If the singleton has no instance yet, create one
    if(NULL == manager){
        //Create an instance to the singleton
        manager = new ShakeGestureManager();
        
        manager->init();
    }
    
    //Return the singleton object
    return manager;
}


void ShakeGestureManager::init()
{
    log("ShakeGestureManagerを初期化");

    addNotification();
}

void ShakeGestureManager::addNotification()
{
    auto listener = EventListenerCustom::create(NotificationKeyWillEnterForeGround, [this](Event*event)
                                              {//バックグラウンド復帰
                                                 
                                                  isBackground = false;
                                                  log("バックグラウンドから復帰した。ShakeGestureManager");
                                              });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 1);
    
    auto lis = EventListenerCustom::create(NotificationKeyDidEnterBackGround, [this](Event*event)
                                           {//バックグラウンド
                                               log("バックグラウンドに入った。ShakeGestureManager");
                                               isBackground = true;
                                               //this->pauseShakeListener();
                                           });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(lis, 1);
    
    //アクセル
    auto accelerationListener = EventListenerAcceleration::create([this](Acceleration *accel, Event *event){
        if (isBackground) {
            //log("バックグラウンドにいるので、通知を飛ばしません。");
            return;
        }
        
        
        auto lowFilter = LowpassFilter::create(10, 5);
        lowFilter->addAcceleration(accel);
        
        //log("シェイク検知しました。{%f, %f, %f}", lowFilter->x, lowFilter->y, lowFilter->z);

        if (this->analyzeShake(lowFilter->x, lowFilter->y, lowFilter->z)) {
            
            log("シェイク機能を検知しました。");
            Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyShakeListener);
        }
    });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(accelerationListener, 1);
}

#pragma mark- パブリック
void ShakeGestureManager::startShakeGesture()
{
    Device::setAccelerometerEnabled(true);
}

void ShakeGestureManager::addShakeGesture(Node* parent, const std::function<void ()> &callback)
{
    auto lis = EventListenerCustom::create(NotificationKeyShakeListener, [callback](Event*event)
                                           {
                                               if (callback) {
                                                   callback();
                                               }
                                           });
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(lis, parent);
}

void ShakeGestureManager::stopShakeGesture()
{
    Device::setAccelerometerEnabled(false);
}
    
bool ShakeGestureManager::analyzeShake(double x, double y, double z)
{
    //「山」の閾値
    double hiThreshold = NativeBridge::isTablet()? .62 : 1.0;
    //「谷」の閾値
    double lowThreshold = NativeBridge::isTablet()? .52 : 0.9;
    
    if (Common::isAndroid() && NativeBridge::isTablet()) {//androidタブレットは益々特別扱い。
        hiThreshold = .55;
        lowThreshold = .45;
    }
    
    //合成加速度を算出
    double composite;
    composite = sqrt(pow(x,2)+pow(y,2)+pow(z,2));
    
    if (composite > .4) {
        //log("シェイクの最高地点%f:",composite);
    }
    //「山」の後に「谷」を検知すると1歩進んだと認識
    if ( stepFlag == true )
    {
        if ( composite < lowThreshold )
        {
            stepFlag = false;
            //log("下回った%f",composite);

            return true;
        }
    }
    else
    {
        if ( composite > hiThreshold )
        {
            //log("てっぺんこえた%f",composite);

            stepFlag = true;
        }
    }
    return false;
}

#pragma mark- 解放
ShakeGestureManager::~ShakeGestureManager()
{//解放されないと思うけど一応書いておく
    log("ShakeGestureManagerの解放。呼ばれたらおかしい。");
}

