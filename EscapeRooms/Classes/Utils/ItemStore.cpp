//
//  ItemStore.cpp
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/04/16.

#include "ItemStore.h"
#include <jni.h>
#include "platform/android/jni/JniHelper.h"
#include "CustomAlert.h"
#include "Common.h"

//パッケージ名を指定 注
#define CLASS_NAME "org/cocos2dx/cpp/ItemStoreBridge"

namespace ItemStore_plugin {
    
    static ItemStoreDelegate* sCallback = NULL;
    
    void ItemStore::loadingStoreInformation()
    {
        cocos2d::JniMethodInfo t;
        if (!cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "loadingStoreInformation", "([Ljava/lang/String;)V"))
        {
            log("メソッドなしです");
            assert(false);
            return;
        }
        
    
        jclass clsj= t.env->FindClass("java/lang/String");
        if (clsj==NULL) {
            log("そんなのねえよばか");
        }
    
        auto itemVector = ItemConstants::getItemIds();
        jobjectArray jkeys = t.env->NewObjectArray(itemVector.size(), clsj, NULL);
        
        int i = 0;
        for(auto key : itemVector) {
            t.env->SetObjectArrayElement(jkeys, i, t.env->NewStringUTF(key.c_str()));
            i++;
        }
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID, jkeys);
        t.env->DeleteLocalRef(t.classID);
    }
    
    void ItemStore::startPurchase(ItemStore_plugin::ItemStoreDelegate *delegate, std::string itemID, bool isRestore, PurchaseType purchaseType)
    {
        cocos2d::JniMethodInfo t;

        if(sCallback != delegate) {//delegateをセット
            sCallback = delegate;
        }
        
        bool jIsConsumable = false;
        
        if (purchaseType == PurchaseType_Consumable) {
            jIsConsumable = true;
        }
        
        if (purchaseType == PurchaseType_Subscription) {//定期購読用
            if (!cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "startSubscriptionPurchase", "(Ljava/lang/String;Z)V")){
                log("定期購読用のメソッドがない");
                assert(false);
                return;
            }
            jstring utfAppId = t.env->NewStringUTF(itemID.c_str());

            log("定期購読用のメソッドを呼ぶ");
            t.env->CallStaticVoidMethod(t.classID, t.methodID, utfAppId, isRestore);
            t.env->DeleteLocalRef(utfAppId);
        }
        else {//消費、非消費型
            if (!cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "startPurchase", "(Ljava/lang/String;ZZ)V")){
                log("消費、非消費型用のメソッドがない");
                
                assert(false);
                return;
            }
            jstring utfAppId = t.env->NewStringUTF(itemID.c_str());

            t.env->CallStaticVoidMethod(t.classID, t.methodID, utfAppId, isRestore, jIsConsumable);
            t.env->DeleteLocalRef(utfAppId);
        }
        
        t.env->DeleteLocalRef(t.classID);
    }
    
    std::string ItemStore::getDescription(std::string itemID)
    {
        std::string ret;
        
        cocos2d::JniMethodInfo methodInfo;
        if (cocos2d::JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "getDescription", "(Ljava/lang/String;)Ljava/lang/String;"))
        {
            jstring itemId = methodInfo.env->NewStringUTF(itemID.c_str());

            jobject objResult = methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID, itemId);
            ret = cocos2d::JniHelper::jstring2string((jstring)objResult); // jstringをstd::stringに変換
            methodInfo.env->DeleteLocalRef(objResult);
            methodInfo.env->DeleteLocalRef(methodInfo.classID);
        }
        
        log("取得したアイテム情報は%s",ret.c_str());
        
        return ret;
    }

    std::string ItemStore::getLocalPrice(std::string itemID)
    {
        std::string ret;
        
        cocos2d::JniMethodInfo methodInfo;
        if (cocos2d::JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "getLocalPrice", "(Ljava/lang/String;)Ljava/lang/String;"))
        {
            jstring itemId = methodInfo.env->NewStringUTF(itemID.c_str());
            
            jobject objResult = methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID, itemId);
            ret = cocos2d::JniHelper::jstring2string((jstring)objResult); // jstringをstd::stringに変換
            methodInfo.env->DeleteLocalRef(objResult);
            methodInfo.env->DeleteLocalRef(methodInfo.classID);
        }
        
        log("取得したアイテム情報は%s",ret.c_str());
        
        return ret;
    }
    
    std::string ItemStore::getPrice(std::string itemID)
    {
        std::string ret;
        
        cocos2d::JniMethodInfo methodInfo;
        if (cocos2d::JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "getPrice", "(Ljava/lang/String;)Ljava/lang/String;"))
        {
            jstring itemId = methodInfo.env->NewStringUTF(itemID.c_str());
            
            jobject objResult = methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID, itemId);
            ret = cocos2d::JniHelper::jstring2string((jstring)objResult); // jstringをstd::stringに変換
            methodInfo.env->DeleteLocalRef(objResult);
            methodInfo.env->DeleteLocalRef(methodInfo.classID);
        }
        
        log("取得したアイテム情報は%s",ret.c_str());
        
        return ret;
    }
    
    void ItemStore::checkAppStoreExpired(const char* itemId)
    {
        cocos2d::JniMethodInfo t;
        
        if (!cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "verifyReceipt", "(Ljava/lang/String;)V")){
            
            assert(false);
            return;
        }
        
        jstring utfAppId = t.env->NewStringUTF(itemId);
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID, utfAppId);
        
        t.env->DeleteLocalRef(utfAppId);
        t.env->DeleteLocalRef(t.classID);
    }
    
    
    bool ItemStore::isUntreatedTransaction(std::string itemID)
    {
        //androidのトランザクション関連は未対応
        return false;
    }
    
    void ItemStore::treatTransaction(std::string itemID)
    {
        //androidのトランザクション関連は未対応
    }
    
    void ItemStore::showErrorAlert(std::string itemID, bool restore, ErrorType errorType)
    {
        std::string message;
        
        bool isSuccess = false;
        log("課金のエラーコード%d",errorType);
        if (errorType == ErrorType_Android_IsPurchaseHistory) {//購入履歴があるのに再度購入しようとしてたら
            isSuccess = true;
            
            message = Common::localize("Since you have purchase history, restore the state for free.", "ご購入履歴があるので、状態を無料で復元します。", "由于您有购买历史记录，请免费恢复状态。", "由於您有購買歷史記錄，請免費恢復狀態。", "구매 이력이 있으므로 무료로 상태를 복원하니다.");
        }
        else if(errorType == ErrorType_NoPurchaseHistory){//購入履歴なし
            message = Common::localize("There was no purchase history. Please purchase from the \"purchase\" button.", "ご購入履歴がございませんでした。「購入」ボタンよりご購入ください。", "没有购买历史。 请从\"购买\"按钮购买。", "沒有購買歷史。請從\"購買\"按鈕購買。", "구매 이력이 없습니다. \"구매\"버튼으로 구입하십시오.");
        }
        else {
            message = Common::localize("Purchasing process failed because an error occurred.", "エラーが発生したため、購入処理に失敗しました。", "采购过程失败，因为发生错误。","採購過程失敗，因為發生錯誤。",  "오류가 발생했기 때문에 구매 처리에 실패했습니다.");
        }
        
        auto alert = CustomAlert::create(AlertTitle_INFORMATION, message, {"OK"}, [itemID,isSuccess,restore,errorType](Ref* sender){
            if (isSuccess) {
                if(sCallback != NULL){//リストアした扱い.リストア成功のコールバックを呼ぶ
                    sCallback->didFinishPayment(itemID, true);
                    sCallback = NULL;
                }
            }
            else {
                if(sCallback != NULL){//リストアした扱い.リストア成功のコールバックを呼ぶ
                    sCallback->happenedError(itemID, restore, errorType);
                    sCallback = NULL;
                }
            }
        });
        log("エラーアラートを表示する");
        alert->showAlert();
    }

    
#ifdef __cplusplus
    extern "C"
    {
#endif
        JNIEXPORT void Java_org_cocos2dx_cpp_ItemStoreBridge_didFinishPayment(JNIEnv *env,jobject thiz,jstring itemID,jboolean isRestore)
        {
            const char* id = env->GetStringUTFChars(itemID, 0);
            bool restore = isRestore;

            if(sCallback != NULL){
                sCallback->didFinishPayment(id, restore);
                sCallback = NULL;
            }
            
            log("コールバックさんが呼ばれました%s",id);

            return;
        }
        
        JNIEXPORT void Java_org_cocos2dx_cpp_ItemStoreBridge_didHappenedError(JNIEnv *env,jobject thiz,jstring itemID,jboolean isRestore, jint errorCode)
        {
            auto id = StringUtils::format("%s",env->GetStringUTFChars(itemID, 0));
            bool restore = isRestore;
            ErrorType errorType = (ErrorType)errorCode;
            
            if(sCallback != NULL){//リストアした扱い.リストア成功のコールバックを呼ぶ
                sCallback->happenedError(id, restore, errorType);
                sCallback = NULL;
            }
            log("エラーが発生した際に呼ばれます。%s",id.c_str());

            return;
        }
        
        JNIEXPORT void Java_org_cocos2dx_cpp_ItemStoreBridge_didFinishSubscriptionPayment(JNIEnv *env,jobject thiz,jstring itemID,jboolean isRestore, jstring expiredTime)
        {
            const char* id = env->GetStringUTFChars(itemID, 0);
            bool restore = isRestore;
            auto expiredSt = env->GetStringUTFChars(expiredTime, 0);
            
            log("サブスクリプションのコールバック");

            if(sCallback != NULL){
                log("サブスクリプションのコールバックがあるので、呼ぶ。");
                sCallback->didFinishPayment(id, restore);
                sCallback = NULL;
            }
            
            //期限日のString
            UserDefault::getInstance()->setStringForKey(ExpiredDateKey, expiredSt);
            log("購入コールました%s",  expiredSt);
            
            return;
        }
        
        JNIEXPORT void Java_org_cocos2dx_cpp_ItemStoreBridge_didHappenedSubscriptionError(JNIEnv *env,jobject thiz,jstring itemID,jboolean isRestore, jint errorCode)
        {
            auto id = StringUtils::format("%s",env->GetStringUTFChars(itemID, 0));
            bool restore = isRestore;
            ErrorType errorType = (ErrorType)errorCode;
            
            bool isExistExpireData = (UserDefault::getInstance()->getStringForKey(ExpiredDateKey,"") != "");
            if (isExistExpireData) {
                //期限日データがあるので、削除します。
                UserDefault::getInstance()->setStringForKey(ExpiredDateKey, "");
            }
            
            if(sCallback != NULL){//リストアした扱い.リストア成功のコールバックを呼ぶ
                sCallback->happenedError(id, restore, errorType);
                sCallback = NULL;
            }
            log("課金のエラーコード%d",errorCode);
            
            return;
        }
#ifdef __cplusplus
    }
#endif

}
