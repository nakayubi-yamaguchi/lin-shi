//
//  TextFieldAlert.h
//  EscapeRooms
//
//  Created by yamaguchinarita on 2018/05/21.
//
//

#ifndef _____EscapeRooms________TextFieldAlert_____
#define _____EscapeRooms________TextFieldAlert_____

#include "cocos2d.h"
#include "ContentsAlert.h"

#include "../UIParts/NCEditBox.h"

USING_NS_CC;
using namespace cocos2d::ui;


class TextFieldAlert : public ContentsAlert, NCEditBoxDelegate
{
public:
    static TextFieldAlert* create(std::string title, std::string contentsMessage, std::vector<std::string> buttons, std::string placefolder, const ccMenuCallback &callback);
    
    bool init(std::string title, std::string contentsMessage, std::vector<std::string> buttons, std::string placefolder, const ccMenuCallback &callback);
    
    
    /**テキストフィールドに入力されている文字列を取得*/
    std::string getText();
    
    /**キーボードの入力形式を変更*/
    void setInputMode(EditBox::InputMode inputMode);
    
    /**アラートをshakeビジョン*/
    void shakeAlert();
    
    /**テキストアラートは手動で閉じる*/
    void close(const ccMenuCallback &callback);
    
private:
    NCEditBox *m_editBox;

    Sprite* createTextFieldSprite(std::string placefolder);
    
    /**ボタン選択時に呼ばれるメソッド。ボタンが押された時はこのメソッドが呼ばれる。*/
    virtual void closeAnimation(bool withButtonCallback) override;

    //editboxのデリゲート
    virtual void customEditBoxEditingDidBegin() override;
    virtual void customEditBoxDidReturn() override;
    virtual void customEditBoxTextChanged(bool textHeightChanged,float textDifference) override;
};

#endif /* defined(_____EscapeRooms________TextFieldAlert_____) */
