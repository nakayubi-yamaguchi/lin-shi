//
//  NotificationBridge.m
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/10/11.
//
//

#include "NotificationBridge.h"
#import <UserNotifications/UserNotifications.h>
#import "Firebase.h"
#include "Common.h"
#include "../AnalyticsManager.h"

NSString*userInfoKey=@"id";

@interface NotificationDelegate : NSObject <UIApplicationDelegate,UNUserNotificationCenterDelegate, FIRMessagingDelegate>
{
    NSMutableArray *topicsArray;
}

@end

@implementation NotificationDelegate

-(void)setTopicsArray:(NSMutableArray*)array
{
    topicsArray = array;
}
#pragma mark リモートdelegate
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{//foreground時呼ばれる
    NSLog(@"%s",__func__);

    // アプリがフォアグランドにいた場合の通知動作を指定する。
    completionHandler(UNNotificationPresentationOptionBadge |
                      UNNotificationPresentationOptionSound |
                      UNNotificationPresentationOptionAlert);
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler
{//通知をタップ後に呼ばれる
    NSLog(@"%s",__func__);

    NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    NSLog(@"%@", response.notification.request.content.userInfo);
}

#pragma mark FIRMessagingDelegate
-(void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
    NSLog(@"デバイストークンを取得成功::%@",fcmToken);
    if (fcmToken) {
        for (int i = 0; i < topicsArray.count; i++) {
            NSString * topic = topicsArray[i];
            NSLog(@"トピックは%@",topic);
            NotificationBridge::subscribeToTopic([topic UTF8String]);
        }
    }
    
    NSLog(@"%s",__func__);
}

-(void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage
{
    NSLog(@"デバイストークンを取得成功・・・メッセージの取得成功");
}

#pragma mark 解放
-(void)dealloc
{
    NSLog(@"%s",__func__);
}

@end

NotificationDelegate *notificationDelegate = NULL;
#pragma mark- ローカル通知
void NotificationBridge::registerLocalNotificationSetting()
{
    UIApplication*application=[UIApplication sharedApplication];
    //local通知
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:
                                                UIUserNotificationTypeBadge|
                                                UIUserNotificationTypeAlert|
                                                UIUserNotificationTypeSound categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    }
}

void NotificationBridge::fireLocalNotification(int time, const char *message, int identifier)
{
    // 通知を作成する
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    
    notification.fireDate = [[NSDate date] dateByAddingTimeInterval:time];
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.alertBody = [NSString stringWithUTF8String:message];
    notification.alertAction = @"Open";
    notification.soundName = UILocalNotificationDefaultSoundName;
    notification.userInfo=@{userInfoKey:[NSString stringWithFormat:@"%d",identifier]};
    
    NSLog(@"通知セットid:%d %@",identifier,notification.alertBody);

    // 通知を登録する
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

void NotificationBridge::cancelLocalNotification(int identifier)
{
    NSArray*notifications=[[UIApplication sharedApplication]scheduledLocalNotifications];
    for (UILocalNotification*n in notifications) {
        int targetedID=[n.userInfo[userInfoKey]intValue];
        if (targetedID==identifier) {
            NSLog(@"指定ID%dの通知を削除しました",identifier);
            [[UIApplication sharedApplication]cancelLocalNotification:n];
        }
    }
}

void NotificationBridge::cancelAllLocalNotification()
{//全キャンセル
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

#pragma mark リモート通知

void NotificationBridge::registerRemoteNotificationSetting(std::vector<std::string> topics)
{
    if (!notificationDelegate) {
        notificationDelegate = [[NotificationDelegate alloc] init];
    }
    
    //トピックをセット。デバイストークンが取得できたタイミングで、トピックをセットする。「
    NSMutableArray* array = @[].mutableCopy;
    for (auto topic : topics) {
        NSString *topicSt = [NSString stringWithUTF8String:topic.c_str()];
        [array addObject:topicSt];
    }
    [notificationDelegate setTopicsArray:array];
    
    // [START set_messaging_delegate]
    [FIRMessaging messaging].delegate = notificationDelegate;
    // [END set_messaging_delegate]
    
    NSLog(@"リモート登録%@",[FIRMessaging messaging].delegate);
    // iOS 8 or later
    // [START register_for_notifications]
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
       
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = notificationDelegate;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if (!error&&granted) {
                AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_RegisterNotification);
            }
        }];
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

void NotificationBridge::subscribeToTopic(const char* topic)
{
    log("トピックは%s",topic);
    [[FIRMessaging messaging] subscribeToTopic:[NSString stringWithUTF8String:topic]];
}
