//
//  RewardMovieManager.cpp
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/03/27.
//

#include "RewardMovieManager.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "Utils/Common.h"
#include "UIParts/NCLoadingScene.h"

#else
#include "Common.h"
#include "NCLoadingScene.h"
#endif

RewardMovieManager* RewardMovieManager::manager=NULL;

#pragma mark - Adfurikun movie reward SDK callback
//**< 広告の表示準備が終わった時のイベント
void RewardMovieManager::prepareSuccess(const char * appID)
{
    log("adfurikun::prepareSuccess %s", appID);
}

//**< 広告の表示が開始した時のイベント
void RewardMovieManager::startPlaying(const char * appID, const char * adnetworkKey)
{
    log("adfurikun::startPlaying %s %s", appID, adnetworkKey);
    
    std::string buf = "再生開始(" + StringUtils::format("%s", adnetworkKey) + ")";
    
    Common::allFreeze();
}

//**< 広告の表示が最後まで終わった時のイベント
void RewardMovieManager::finishedPlaying(const char * appID)
{
    log("adfurikun::finishedPlaying %s", appID);
    
    m_complete = true;
}

//**< 動画広告再生エラー時のイベント
void RewardMovieManager::failedPlaying(const char * appID)
{
    m_complete = false;

    log("adfurikun::failedPlaying %s", appID);
}

//**< 広告を閉じた時のイベント *
void RewardMovieManager::adClose(const char * appID)
{
    log("adfurikun::adClose %s", appID);
    
    didCloseReward();
}

#pragma mark - 広告出し分けリワード広告
void RewardMovieManager::didStartReward()
{
    Common::allFreeze();
}

void RewardMovieManager::didCloseReward(bool completed)
{
    log("admobの動画広告を閉じました。%d",completed);
    
    m_complete = completed;
    
    //リワードが終了したタイミングで呼び出す。
    didCloseReward();
}

#pragma mark- 初期化
RewardMovieManager* RewardMovieManager::sharedManager()
{
    //If the singleton has no instance yet, create one
    if(NULL == manager){
        //Create an instance to the singleton
        manager = new RewardMovieManager();
        
        manager->init();
    }
    
    //Return the singleton object
    return manager;
}

void RewardMovieManager::init()
{
    log("RewardMovieManagerを初期化");
}

#pragma mark- パブリック
void RewardMovieManager::loading()
{
    Adfurikun::ADFMovieReward::initializeWithAppID(MOVIE_REWARD_APPID);

    RewardPlayer::getInstance()->load();
    RewardPlayer::getInstance()->setDelegate(this);
}

bool RewardMovieManager::isPrepared()
{
    Common::performProcessForDebug([](){
        log("アドふりの準備%d,アドモブの準備%d",Adfurikun::ADFMovieReward::isPrepared(MOVIE_REWARD_APPID), RewardPlayer::getInstance()->isReady());
    });
    
    return Adfurikun::ADFMovieReward::isPrepared(MOVIE_REWARD_APPID) || RewardPlayer::getInstance()->isReady();
}

void RewardMovieManager::play()
{//アドフリとアドモブどちらを表示するか抽選します。
    //動画再生と同時に画面タッチを効かなくする。
    addTouchLayer();
    
    Common::performProcessForDebug([](){
        log("再生時のアドふりの準備%d,アドモブの準備%d",Adfurikun::ADFMovieReward::isPrepared(MOVIE_REWARD_APPID), RewardPlayer::getInstance()->isReady());
    }, nullptr);

    if (RewardPlayer::getInstance()->isReady()){//Admob最優先
        log("admobを再生します。");
        RewardPlayer::getInstance()->show();
    }
    else if(!Common::isAndroid() && Adfurikun::ADFMovieReward::isPrepared(MOVIE_REWARD_APPID)){//最後にアドふり
        log("アドフリを再生します。");
        Adfurikun::ADFMovieReward::setDelegate(MOVIE_REWARD_APPID, manager);
        Adfurikun::ADFMovieReward::play(MOVIE_REWARD_APPID);
    }
}

void RewardMovieManager::play(const onFinished &callback)
{
    m_callback = callback;
    
    play();
}

#pragma mark- プライベート
void RewardMovieManager::addTouchLayer()
{
    auto runningScene = Director::getInstance()->getRunningScene();
    auto swallowTouchLayer = NCLoadingScene::create(runningScene->getContentSize(), "");
    swallowTouchLayer->setOpacity(50);
    swallowTouchLayer->setName(SWALLOW_TOUCH);
    runningScene->addChild(swallowTouchLayer);
    
    auto listener=EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);//タッチ透過しない
    
    listener->onTouchBegan=[this](Touch*touch, Event*event){
        return true;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, swallowTouchLayer);
}

void RewardMovieManager::removeTouchLayer()
{
    //タッチを無効にさせていたレイヤーを削除する。
    Director::getInstance()->getRunningScene()->removeChildByName(SWALLOW_TOUCH);
}

void RewardMovieManager::didCloseReward()
{
    Director::getInstance()->resume();
    Director::getInstance()->startAnimation();
    
    Director::getInstance()->getScheduler()->performFunctionInCocosThread([this](){
        //BGMを再開
        Common::resumeBGM();
        
        //タッチを有効にする。
        removeTouchLayer();
        
        if (m_callback) {
            m_callback(m_complete);
            m_callback = nullptr;
        }
        else if (delegate!=NULL) {
            delegate->didCloseRewardMovie(m_complete);
        }
        else{
            log("リワードマネージャーにデリゲートがセットされていません。");
        }
        m_complete=false;
    });
}

#pragma mark- 解放
RewardMovieManager::~RewardMovieManager()
{//解放されないと思うけど一応書いておく
    log("呼ばれたらおかしい。");
}

