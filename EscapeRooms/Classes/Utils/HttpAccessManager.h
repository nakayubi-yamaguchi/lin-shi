//
//  HttpAccessManager.h
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/09/27.
//
//

#ifndef __EscapeContainer__HttpAccessManager__
#define __EscapeContainer__HttpAccessManager__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "network/HttpClient.h"

#define NotificationKey_Download "httpsundownload"
#define NotificationKey_Progress "downloadProgress"
#define NotificationKey_Unzip "httpsunzip"
#define NotificationKey_Progress_AllDownload "downloadProgressForAllDownload"
#define NotificationKey_Complete_AllDownload "completeAllDownload"

#define ProgressKey_RECEIVE "receivesize"
#define ProgressKey_TOTAL "totalsize"

USING_NS_CC;

using namespace network;

using onFinishedDownloadZip = std::function<void(bool succeeded)>;

class HttpAccessManager
{
public:
    /**zipファイルをダウンロードして、展開した後保存する。*/
    static void downloadZip(const std::string& url, const std::string& outdir, onFinishedDownloadZip onFinished);
    
    /**zipファイルをダウンロードして、展開した後保存する。*/
    static void downloadZipp(const std::string& url, const std::string& outdir, onFinishedDownloadZip onFinished);
    
    static void postNotification(const char* notificationKey);
    static void postNotification(const char* notificationKey, Value progressValue);
    
    /**ダウンローダー,firebaseを使用した場合のzip解凍*/
    static void pushToUnzip(onFinishedDownloadZip onFinished, const std::string& outdir, std::vector<unsigned char>& data);

    static void pushToWriteFile(onFinishedDownloadZip onFinished, const std::string& path, unsigned char* data, ssize_t size);

private:
    static void createHttpRequest(const std::string& url, const std::string& outdir, onFinishedDownloadZip onFinished);
    static void createDownloader(const std::string& url, const std::string& outdir, onFinishedDownloadZip onFinished);
    

    /**HttpRequestを使用した場合のzip解凍*/
    static void pushToUnzip(onFinishedDownloadZip onFinished, const std::string& outdir, HttpResponse* response);
};

#endif /* defined(__EscapeContainer__HttpAccessManager__) */
