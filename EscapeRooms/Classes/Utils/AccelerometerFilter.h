
#include "cocos2d.h"

USING_NS_CC;

// Basic filter object.
class AccelerometerFilter : public Node
{
public:

    double x, y, z;

    virtual void addAcceleration(Acceleration* accel);
    
private:
    
protected:
    bool adaptive;
    
    double norm(double x, double y, double z);
    double clamp(double v, double min, double max);
};



class LowpassFilter : public AccelerometerFilter
{
public:
    
    static LowpassFilter* create(double rate, double cutoffFrequency);
    virtual bool initWithSampleRate(double rate, double cutoffFrequency);
    virtual void addAcceleration(Acceleration* accel) override;
    
private:
    double filterConstant;
    double lastX, lastY, lastZ;
    
};


class HighpassFilter : public AccelerometerFilter
{
public:
    
    static HighpassFilter* create(double rate, double cutoffFrequency);
    virtual bool initWithSampleRate(double rate, double cutoffFrequency);
    virtual void addAcceleration(Acceleration* accel) override;
    
    
private:
    double filterConstant;
    double lastX, lastY, lastZ;
    
};
