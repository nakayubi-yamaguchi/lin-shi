//
//  FireBaseBridge.h
//  PuzzleWord2
//
//  Created by 成田凌平 on 2016/05/28.
//

#ifndef __EscapeRooms__FireBaseBridge__
#define __EscapeRooms__FireBaseBridge__

#include "cocos2d.h"

#define LogReferenceCount(obj) NSLog(@"[%@] reference count = %ld", [obj class], CFGetRetainCount((__bridge void*)obj))

USING_NS_CC;

//alert構造体
struct HouseAlert{
    std::string url;
    std::string message;
    bool isNew;
};

//icon構造体
struct HouseIcon{
    Sprite*sprite;
    bool isNew;
    std::string url;
};

typedef enum {//firbaseで追加,変更される可能性あり。
    STRAGE_ERRORCODE_NONE = 0,
    STRAGE_ERRORCODE_UNKNOWN = -13000,
    STRAGE_ERRORCODE_ObjectNotFound = -13010,
    STRAGE_ERRORCODE_BucketNotFound = -13011,
    STRAGE_ERRORCODE_ProjectNotFound = -13012,
    STRAGE_ERRORCODE_QuotaExceeded = -13013,
    STRAGE_ERRORCODE_Unauthenticated = -13020,
    STRAGE_ERRORCODE_Unauthorized = -13021,
    STRAGE_ERRORCODE_RetryLimitExceeded = -13030,
    STRAGE_ERRORCODE_NonMatchingChecksum = -13031,
    STRAGE_ERRORCODE_Cancelled = -13040,
    STRAGE_ERRORCODE_DownloadSizeExceeded = -13032
}STRAGE_ERRORCODE;

//コールバック.androidで保持して使うかもしれないので、同じ引数でも別名で作成しておく。
typedef std::function<void(bool successed, const char*url)> onGetUrl;
using onFIRFinished = std::function<void(bool successed)>;
using onFinishLoadFile = std::function<void(bool successed, std::vector<unsigned char> bytes, STRAGE_ERRORCODE errorcode, std::string errorDescription)>;
using onProgressLoadFile = std::function<void(float progress)>;
using onFinishedLoadValue = std::function<void(bool successed, Value value)>;


USING_NS_CC;

class FireBaseBridge
{
private:    
    void initialize();
    
public:
    static FireBaseBridge* getInstance();
    void setUp();
    
#pragma mark - 自社アラート
    int showCount;
    bool cancel;
    std::vector<HouseAlert>availableAlerts;
    void loadingAlertHouseAd(bool doCallback);
    void shuffleAlerts();
    void cancelShowAlertHouseAd();
    void addAlertInfos(const char*message,const char*url,bool isNew);
    void clickedHouseAlert();
    /** loadingAlertHouseAd(false)の時、任意の場所で呼ぶ。*/
    void showHouseAlert();
    int getShowCount();
    
#pragma mark - 自社アイコン
    std::vector<HouseIcon> icons;
    Sprite*iconBanner;
    cocos2d::Size _bannerSize;
    int _iconCount;//アイコン表示指定個数
    int savedIconCount;//サーバからのアイコン取得個数
    
    void loadingHouseIcons();
    Sprite* getIconBanner(cocos2d::Size size,int iconCount);
    void deleteIconBanner();
    void addIconOnBanner();

    void createSprite(unsigned char* array,int length,const char*url,bool isNew);//cppのみ使用

#pragma mark - Alalytics
    void analyticsWithName(const char*eventName);
    void analyticsWithParameters(const char*eventName,std::vector<const char*>keys,std::vector<const char*>values);
    void setUserID(const char* userId);
    void setUserProperty(const char* propertyName, const char* value);

#pragma mark - Remote Config
    void initRemoteConfig();
    void fetchRemoteConfig(std::string key,const onFinishedLoadValue& callback);

#pragma mark - Database
#pragma mark 値読み込み
    //通信状態を取得します。
    void checkConnected();
    bool getIsConnected();
    
    /**値をValue型で取得。*/
    void loadingValue(const char *keyName, const onFinishedLoadValue& callback);
    
    /**タイムスタンプを取得.*/
    
    /**
     起動時などに、タイムスタンプを読み込み。
     @param keepSync 最新の状態で保持し続けるかどうか。
     */
    void loadingTimestamp(bool keepSync);
    
    /**サーバー時刻を取得.値が0の場合は読み込めていない。*/
    time_t getServerTimestamp();
    
#pragma mark 値セット
    /**値セット。今後Value型で渡せるようにする。*/
    void setValue(std::string childKey, std::map<std::string, int> value, const onFIRFinished& callback);
    /**データベースが複数ユーザーによって同時更新される時に使用。例：いいねとかレビュー。 インクリメント限定*/
    void transactionPost(std::string childKey, std::vector<std::string> incrementKeys, std::vector<std::string> decrementKeys, const onFIRFinished& callback);
    /**同時に複数箇所更新。{"key":intValue(増分)}を渡す*/
    void transactionPost(std::string childKey, std::map<std::string, int> transactionMap, const onFIRFinished& callback);

#pragma mark - Storage
    //URLを生成
    void getURL(const char *downloadFile, const onGetUrl& callback);
    
    //ファイルダウンロード
    void downloadFile(const char *downloadFile, const onProgressLoadFile& progressCallback, const onFinishLoadFile& callback);
        
    //オブサーバー停止
    void removeAllObserver(const char* keyName);
    
};

#endif /* defined(__PuzzleWord2__FireBaseBridge__) */
