//
//  BannerManager.h
//  EscapeRooms
//
//  Created by yamaguchi on 2019/04/08.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________BannerManager_____
#define _____PROJECTNAMEASIDENTIFIER________BannerManager_____

#include "cocos2d.h"
#include "BannerBridge.h"

class BannerManager
{
public:
    static BannerManager* manager;
    static BannerManager* getInstance();
    
    void createBanner(BannerPosition bannerPosition);
    
    BannerType getBannerType();
    void setBannerType(BannerType type);
    
    
    void removeBanner(BannerPosition bannerPosition);

private:
    BannerType bannerType;
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
