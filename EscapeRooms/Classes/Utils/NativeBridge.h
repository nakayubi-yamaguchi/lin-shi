//
//  NativeBridge.h
//  Rocket
//
//  Created by 成田凌平 on 2016/02/21.
//
//

#ifndef __Rocket__NativeBridge__
#define __Rocket__NativeBridge__

#include "cocos2d.h"
#include <iomanip>
#include "SettingDataManager.h"

USING_NS_CC;

//Buildが通らない場合、Social frameworkを入れること。
typedef enum{
    ShareType_Activity,
    ShareType_Twitter,
    ShareType_FaceBook,
    ShareType_LINE
}ShareType;

class NativeBridge
{
public:
    static void openUrl(const char*url);
    
    //シェア
    static void openShareMenu(const char *tweet);
    static void openShareMenu(const char *tweet,const char* path);///アクティビティ
    static void openShareMenu(ShareType shareType, const char *tweet, const char* path);
    
    //設定言語判定
    static std::string getCurrentLanguage();

    //ローカル通知
    static void registerNotificationSetteing();
    static void fireLocalNotification(int time, const char *message);
    static void cancelAllLocalNotification();
    
    //+1ボタン
    static void createPlusOneButton(float x, float y, float width, float height);
    static void removePlusOneButton();
    
    static void openWithLINEText(const std::string& message);

    //端末判定
    static bool isTablet();
    static const char* judgeID(const char* smartPhoneID, const char* tabletID);
    static std::string getOSVersion();
    
    //ストレージ
    static unsigned long long storageAvailableMb();
    
    //オンライン確認
    static bool isOnline();
    
    //レビューを求む
    static bool requestReview();
    
    /**3点カンマ*/
    static std::string transformThreeComma(float num);
    
    /**バイブ*/
    static void vibration();
    static void vibration3D();
    
    /**独立したIDを生成*/
    static std::string createUUID();


private:
    //シェア
    static void openWithTwitter(const char* tweet,const char*path);
    static void openWithFaceBook(const char* tweet,const char*path);
    static void openWithLINE(const char*path);
    
    //ライン用
    static constexpr auto LINE_URL_SCHEME = "line://msg/%s/%s";
    static constexpr auto CONTENT_IMAGE = "image";
    static constexpr auto CONTENT_TEXT = "text";
};

#endif /* defined(__Rocket__NativeBridge__) */
