//
//  AdmobRewardBridge.m
//
//  Created by yamaguchinarita on 2017/04/12.

#import "AdmobRewardBridge.h"
#import "RootViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#include <AppLovinAdapter/GADMAdapterAppLovinExtras.h>
#include <FBAudienceNetwork/FBNativeAd.h>
#include <AdColony/AdColony.h>

@interface RewardCallback : NSObject<GADRewardBasedVideoAdDelegate>{
    
}

@property bool completedMovie;
@property int loadingCount;
@property AdmobRewardBridgeDelegate *delegate_id;

@end

@implementation RewardCallback : NSObject

#pragma mark- リワードデリゲート
- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd didRewardUserWithReward:(GADAdReward *)reward
{
    NSString *rewardMessage =
    [NSString stringWithFormat:@"Reward received with currency %@ , amount %lf",
     reward.type,
     [reward.amount doubleValue]];
    
    _completedMovie = true;
    
    AdmobRewardBridge::logOfBridge([[NSString stringWithFormat:@"Reward based video ad%@", rewardMessage] UTF8String]);
}

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    AdmobRewardBridge::logOfBridge("Reward based video ad is received.");
    
    _loadingCount = 0;
}

- (void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    AdmobRewardBridge::logOfBridge("Opened reward based video ad.");
}

- (void)rewardBasedVideoAdDidStartPlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    AdmobRewardBridge::logOfBridge("Reward based video ad started playing.");
    
    if (_delegate_id!=NULL){
        _delegate_id->didStart();
    }
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    AdmobRewardBridge::logOfBridge("Reward based video ad is closed.");
    
    //動画が閉じられたら自動で再読み込み。
    AdmobRewardBridge::loadingAdmobReward();
    
    if (_delegate_id!=NULL){
        _delegate_id->didClose(_completedMovie);
    }
    
    _completedMovie = false;
}

- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    AdmobRewardBridge::logOfBridge("Reward based video ad will leave application.");
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error {
    
    if (_loadingCount < 10) {//読み込み失敗時、10回は連続再読み込み
        AdmobRewardBridge::loadingAdmobReward();
        _loadingCount += 1;
    }
    else {//超えた時.1分後に動画を再読み込みする。
        [self performSelector:@selector(reloadAdmob) withObject:NULL afterDelay:60];
    }
    NSLog(@"アドモブのローディングに失敗しました。%@",error);
    //AdmobRewardBridge::logOfBridge( "Reward based video ad failed to load.%@",error);
}

#pragma mark- 再読み込み用
-(void)reloadAdmob
{
    AdmobRewardBridge::logOfBridge("アドモブのリワードを再読み込みします。");
    _loadingCount = 0;
    
    AdmobRewardBridge::loadingAdmobReward();
}

#pragma mark- 解放
-(void)dealloc{
    NSString *log = [NSString stringWithFormat:@"%s", __func__];
    AdmobRewardBridge::logOfBridge([log UTF8String]);
}

@end

#pragma mark- Admobリワードブリッジ
RewardCallback *rewardCallback = [[RewardCallback alloc] init];

void AdmobRewardBridge::loadingAdmobReward()
{
    
    /*
    [AdColony configureWithAppID:@"app9d9262429bc64632b9"
                         zoneIDs:@[@"vza170ccc5b25e4609bc",
                                   @"vzbcc521aa631d4389b5",
                                   @"vzca2aac50eb79438cbc"]
                         options:nil
                      completion:nil];*/
    logOfBridge("動画広告をローディング");
    GADRequest *request = [GADRequest request];
    //Applovinの設定
    GADMAdapterAppLovinExtras * extras = [[GADMAdapterAppLovinExtras alloc] init];
    extras.muteAudio = NO;
    [request registerAdNetworkExtras:extras];

    auto rewardId = DebugMode == 0? MOVIE_REWARD_ADMOB_APPID : MOVIE_REWARD_ADMOB_APPID_MEDIATION_TEST;
    
    [GADRewardBasedVideoAd sharedInstance].delegate = rewardCallback;
    [[GADRewardBasedVideoAd sharedInstance] loadRequest:request
                                           withAdUnitID:[NSString stringWithUTF8String:rewardId]];
    
}

bool AdmobRewardBridge::isReady()
{
    return [[GADRewardBasedVideoAd sharedInstance] isReady];
}

void AdmobRewardBridge::show()
{
    if (isReady()) {
        UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        
        NSLog(@"[%@]の広告を表示します",[[GADRewardBasedVideoAd sharedInstance] adNetworkClassName]);
        
        if (rootViewController)//ルートビューコントローラーがあれば。
            [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:rootViewController];
    }
    else{
        logOfBridge("ローディングが完了していない。");
    }
}

void AdmobRewardBridge::logOfBridge(std::string text)
{
    NSLog(@"[%@]%s", [NSString stringWithUTF8String:LOG_ADMOB_REWARD], text.c_str());
}

void AdmobRewardBridge::setDelegate(AdmobRewardBridgeDelegate *var)
{
    if (rewardCallback)
        rewardCallback.delegate_id = var;
}
