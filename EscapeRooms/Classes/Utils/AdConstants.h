//
//  BannerConstants.h
//  Anaguma
//
//  Created by yamaguchinarita on 2016/06/15.
//
//

#ifndef AdConstants_h
#define AdConstants_h

#include "cocos2d.h"
#include <string.h>


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

// iOS - AdXBanner
// headerとfooterのAdUnitID
#define BANNER_ADX_HEADER_AD_UNIT_ID "/62532913/a_escaperooms.ios_320x50_header_24579"
#define BANNER_ADX_FOOTER_AD_UNIT_ID "/62532913/a_escaperooms.ios_320x50_footer_24579"

// iOS - AdX
// AdXの動画リワードのAdUnitID
#define MOVIE_REWARD_ADX_AD_UNIT_ID "/62532913/a_escaperooms.ios_1024x768_reward_24579"
// AdXの動画リワードのサンプルID
#define MOVIE_REWARD_ADX_SAMPLE_AD_UNIT_ID "/6499/example/rewarded-video"

// iOS - Fluct
// FluctSDKの動画リワードの必要なID
#define MOVIE_REWARD_FLUCT_GROUP_ID "1000105670"
#define MOVIE_REWARD_FLUCT_UNIT_ID "1000164180"

//iOSの広告枠ID
#define CONFIGURE_APPID "ca-app-pub-3145084262749532~9820989946"//コンテナ ios
#define BANNER_APPID_HEADER "ca-app-pub-3145084262749532/4568663263"//コンテナ admob ios
#define BANNER_APPID_HEADER_TABLET BANNER_APPID_HEADER

#define BANNER_APPID_FOOTER "ca-app-pub-3145084262749532/3391312961" //コンテナ admob ios
#define BANNER_APPID_FOOTER_TABLET BANNER_APPID_FOOTER
#define BANNER_APPID_RECTANGLE "ca-app-pub-3145084262749532/9176980357"//コンテナ admob ios

#define INTERSTITIAL_APPID "ca-app-pub-3145084262749532/6935806304"//コンテナ 復帰時 ios
#define INTERSTITIAL_LAUNCH_APPID "ca-app-pub-3145084262749532/9978926135"//コンテナ 起動時 ios

#define MOVIE_REWARD_ADMOB_APPID "ca-app-pub-3145084262749532/1367784853"//コンテナ ios
#define MOVIE_REWARD_ADMOB_APPID_MEDIATION_TEST "ca-app-pub-3145084262749532/1772101072"
#define PANEL_APPID_Setting ""//"59e42d0b0d34958208000211" //コンテナ ios
#define PANEL_APPID_Loading "" //コンテナ ios

#define MOVIE_Native_APPID "59e42d440d34957308000228" //コンテナ 海外配信用 ios

//パネル
#define PANEL_A_SMALL "59e40b2c2d34951f6f0007df" //コンテナ ios
#define PANEL_A_BIG "59e40b682e3495321e000956" //コンテナ ios
#define PANEL_B_SMALL "59e40cdb2e3495391e000992" //コンテナ ios
#define PANEL_B_BIG "59e40bb42d3495296f000807" //コンテナ ios
#define PANEL_C_SMALL "59e40c642e3495271e0009db" //コンテナ ios
#define PANEL_C_BIG "59e40c8d2e34952f1e0009c5" //コンテナ ios

#define NATIVEADVANCE_SMALL "ca-app-pub-3145084262749532/8396600665" //コンテナ ios
#define NATIVEADVANCE_BIG "ca-app-pub-3145084262749532/3397867920" //コンテナ ios



#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

// Android - AdXBanner
// headerとfooterのAdUnitID
#define BANNER_ADX_HEADER_AD_UNIT_ID "/62532913/a_escaperooms.and_320x50_header_24580"
#define BANNER_ADX_FOOTER_AD_UNIT_ID "/62532913/a_escaperooms.and_320x50_footer_24580"

// Android - AdX
// AdXの動画リワードのAdUnitID
#define MOVIE_REWARD_ADX_AD_UNIT_ID "/62532913/a_escaperooms.and_1024x768_reward_24580"
// AdXの動画リワードのサンプルID
#define MOVIE_REWARD_ADX_SAMPLE_AD_UNIT_ID "/6499/example/rewarded-video"

// Android - Fluct
// FluctSDKの動画リワードの必要なID
#define MOVIE_REWARD_FLUCT_GROUP_ID "1000105671"
#define MOVIE_REWARD_FLUCT_UNIT_ID "1000164181"

//Androidの広告枠ID
#define CONFIGURE_APPID "ca-app-pub-3145084262749532~2515369085"//コンテナandroid
#define BANNER_APPID_HEADER "ca-app-pub-3145084262749532/6263042401"//コンテナ admob
#define BANNER_APPID_HEADER_TABLET BANNER_APPID_HEADER
#define BANNER_APPID_FOOTER "ca-app-pub-3145084262749532/2846062641"//コンテナ admob android
#define BANNER_APPID_RECTANGLE "ca-app-pub-3145084262749532/5479856037"

#define INTERSTITIAL_APPID "ca-app-pub-3145084262749532/2435833257"//コンテナ 復帰時 android
#define INTERSTITIAL_LAUNCH_APPID "ca-app-pub-3145084262749532/4697489922"//コンテナ 起動時 android
#define MOVIE_REWARD_ADMOB_APPID "ca-app-pub-3145084262749532/5284523723"//コンテナ
#define MOVIE_REWARD_ADMOB_APPID_MEDIATION_TEST "ca-app-pub-3145084262749532/7705463344"


#define BANNER_APPID_FOOTER_TABLET BANNER_APPID_FOOTER

#define PANEL_APPID_Setting "59e42d250e3495e54e000735" //コンテナ android
#define PANEL_APPID_Loading "59f078810e3495e32900040d" //コンテナ android
#define MOVIE_Native_APPID "59e42d562e3495a433000045"//コンテナ　海外配信用 android

//パネル
#define PANEL_A_SMALL "59e40d4c2d3495246f000889" //コンテナ android
#define PANEL_A_BIG "59e40d652e3495361e000978" //コンテナ android
#define PANEL_B_SMALL "59e40e582e3495321e0009a6" //コンテナ android
#define PANEL_B_BIG "59e40ded0e3495ee4e0004f5" //コンテナ android
#define PANEL_C_SMALL "59e40e0b2d3495166f00081d" //コンテナ android
#define PANEL_C_BIG "59e40e322e3495161e0009b6" //コンテナ android

#define NATIVEADVANCE_SMALL "" //コンテナ android
#define NATIVEADVANCE_BIG "" //コンテナ android

#else

#endif

class AdConstants {
public:
    static std::string getPanelID(const char *key);
    
    /**KEY:広告ユニットID,VALUE:nibファイル名のmapを取得.BannerBridgeのネイティブ広告ローディングで使用*/
    static std::map<const char*, const char*> getNativeUnitIdAndNibNameMap();
    
    /**引数のパネル広告のIDから、ネイティブアドバンス広告IDを決定する*/
    static std::string getNativeAdvanceUnitId(const char *panelUnitID);
};

#endif /* AdConstants_h */
