//
//  HttpAccessManager.cpp
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/09/27.
//
#include "HttpAccessManager.h"
#include "NativeBridge.h"
#include "DownloadManager.h"
#include "FireBaseBridge.h"
#include "Common.h"

void HttpAccessManager::downloadZip(const std::string& url, const std::string& outdir, onFinishedDownloadZip onFinished)
{
    //通知発行
    log("ダウンロードしているので、ダウンロード中の通知を発行");
    
    //httpリクエスト
    createHttpRequest(url, outdir, onFinished);
}

void HttpAccessManager::downloadZipp(const std::string& url, const std::string& outdir, onFinishedDownloadZip onFinished)
{
    //通知発行
    log("ダウンロードしているので、ダウンロード中の通知を発行");
    
    //進捗を表示するために、ダウンローダーを使用する。
    createDownloader(url, outdir, onFinished);
}

#pragma mark- プライベート
void HttpAccessManager::createHttpRequest(const std::string& url, const std::string& outdir, onFinishedDownloadZip onFinished)
{
    //http通信
    log("出力先%s url%s",outdir.c_str(),url.c_str());
    auto req = new (std::nothrow) cocos2d::network::HttpRequest();
    req->setRequestType(cocos2d::network::HttpRequest::Type::GET);
    req->setUrl(url);
    req->setResponseCallback([onFinished, outdir](cocos2d::network::HttpClient* client, cocos2d::network::HttpResponse* response){
        log("ダウンロードの成功有無を確認%d",response->isSucceed());
        int dataSize = (int)response->getResponseData()->size();
        
        if(response->isSucceed() && dataSize > 0){
            //ダウンロードしたzipファイルを展開スレッドへ送る
            pushToUnzip(onFinished, outdir, response);
        }else{
            onFinished(false);
        }
    });
    
    log("ダウンロードサイズを表示します%zd",req->getRequestDataSize());
    auto client = cocos2d::network::HttpClient::getInstance();
    
    client->sendImmediate(req);
    req->release();
}

void HttpAccessManager::createDownloader(const std::string &url, const std::string &outdir, onFinishedDownloadZip onFinished)
{
    auto onDataTaskSuccess = [outdir,onFinished](const cocos2d::network::DownloadTask& task,
                                                 std::vector<unsigned char>& data) {
        log("downloaded success[%s]",outdir.c_str());
        
        pushToUnzip(onFinished, outdir, data);
        DownloadManager::getInstance()->cancelDownload();
    };
    
    auto onTaskProgress = [](const network::DownloadTask& task, int64_t bytesReceived, int64_t totalBytesReceived, int64_t totalBytesExpected) {
        log("download progress %.0f%%   %lld/%lld", (float) totalBytesReceived * 100.0 / totalBytesExpected,totalBytesReceived,totalBytesExpected);
        
        auto data = Value(float(totalBytesReceived * 100) / totalBytesExpected);
        postNotification(NotificationKey_Progress, data);
    };
    
    auto onTaskError = [onFinished](const network::DownloadTask& task, int errorCode, int errorCodeInternal, const std::string& errorStr) {
        log("downloading error %s,エラーコード%d", errorStr.c_str(), errorCode);
        
        //DownloadManager::getInstance()->cancelDownload();
        
        if (onFinished)
            onFinished(false);
    };
    
    DownloadManager::getInstance()->startDownload(url, onDataTaskSuccess, onTaskProgress, onTaskError);
}

void HttpAccessManager::pushToUnzip(onFinishedDownloadZip onFinished, const std::string& outdir, cocos2d::network::HttpResponse* response){
    //通知発行
    log("ダウンロードしているので、展開の通知を発行");
    postNotification(NotificationKey_Unzip);
    
    //出力先のディレクトリがなければ、生成しておかないと保存する際にクラッシュする。
    bool isExist = FileUtils::getInstance()->isDirectoryExist(outdir);
    if (!isExist) {
        log("出力先がないので、生成しておきます。%s",outdir.c_str());
        FileUtils::getInstance()->createDirectory(outdir);
    }
    
    //別スレッドで実行されるタスク
    auto task = [onFinished, outdir, response](){
        // zipに含まれるファイル情報リストを取得
        cocos2d::ZipFile* zipfile = cocos2d::ZipFile::createWithBuffer(response->getResponseData()->data(),
                                                                       response->getResponseData()->size());

        for(std::string filename = zipfile->getFirstFilename(); !filename.empty(); filename = zipfile->getNextFilename()){
            log("ダウンロードしたzipファイルは%s",filename.c_str());
            
            if( *filename.rbegin() == '/' ) {
                log("ディレクトリを作成する");
                // It's a directory.
                cocos2d::FileUtils::getInstance()->createDirectory( outdir + filename );
            }
            else {
                // It's a file.
                ssize_t filesize;
                unsigned char* filedata = zipfile->getFileData( filename, &filesize );
                // ファイルデータが取得できたので、書き込みを行うスレッドへ送る
                pushToWriteFile(nullptr, outdir + filename, filedata, filesize);
            }
        }
        delete zipfile;
        
        // 終了判定用に空データを送る
        pushToWriteFile(onFinished, "", nullptr, 0);
    };
    
    // 最後にUIスレッドで実行されるタスク
    auto finished = [response](void*){
        response->release();
    };
    
    // unzipタスク実行中に破棄されないよう保護する
    response->retain();
    
    // 非同期タスクの開始 (TASK_OTHERのスレッドキューへ積まれる)
    cocos2d::AsyncTaskPool::getInstance()->enqueue(cocos2d::AsyncTaskPool::TaskType::TASK_OTHER, finished, nullptr, task);
}


void HttpAccessManager::pushToUnzip(onFinishedDownloadZip onFinished, const std::string &outdir, std::vector<unsigned char> &data){
    log("zip解凍はじめます。HttpAccessManager");
    //通知発行
    postNotification(NotificationKey_Unzip);
    
    //出力先のディレクトリがなければ、生成しておかないと保存する際にクラッシュする。
    bool isExist = FileUtils::getInstance()->isDirectoryExist(outdir);
    if (!isExist) {
        log("DLは出力先がないので、生成しておきます。%s",outdir.c_str());
        FileUtils::getInstance()->createDirectory(outdir);
    }
    
    auto write = [](const std::string& path, unsigned char* data, ssize_t size){
        if( data ){
            FILE* file = fopen( path.c_str(), "wb" );
            fwrite( data, size, 1, file );
            fclose( file );
            CCLOG("endOfWriteFile: %s", path.c_str());
            
            // zipfile->getFileData で確保されたメモリを開放する
            free( data );
        }
    };
    
    // スレッド処理を記述
    auto loading_thread = std::thread([=](){
        //zipに含まれるファイル情報リストを取得
        cocos2d::ZipFile* zipfile = cocos2d::ZipFile::createWithBuffer(data.data(), data.size());
        log("DLはzipを展開して、その中のファイルを順番に処理するよ。データ%s,,%zd", data.data(), data.size());
        
        for(std::string filename = zipfile->getFirstFilename(); !filename.empty(); filename = zipfile->getNextFilename()){
            auto key = "書き込み終わり";
            Common::startTimeForDebug(key);
            log("DLはダウンロードしたzipファイルは%s",filename.c_str());
            
            if( *filename.rbegin() == '/' ) {
                log("DLはディレクトリを作成する");
                // It's a directory.
                cocos2d::FileUtils::getInstance()->createDirectory( outdir + filename );
            }
            else {
                // It's a file.
                ssize_t filesize;
                unsigned char* filedata = zipfile->getFileData( filename, &filesize );
                // ファイルデータが取得できたので、書き込みを行うスレッドへ送る
                write(outdir + filename, filedata, filesize);
            }
            
            Common::stopTimeForDebug(key);
        }
        delete zipfile;
        
        Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
            if (onFinished) {
                onFinished(true);
            }
        });
    });
    loading_thread.detach();
    
    /*
    //別スレッドで実行されるタスク
    auto task = [onFinished, outdir, data](){
        
        //zipに含まれるファイル情報リストを取得
        cocos2d::ZipFile* zipfile = cocos2d::ZipFile::createWithBuffer(data.data(), data.size());
        log("DLはzipを展開して、その中のファイルを順番に処理するよ。データ%s,,%zd", data.data(), data.size());

        for(std::string filename = zipfile->getFirstFilename(); !filename.empty(); filename = zipfile->getNextFilename()){
            log("DLはダウンロードしたzipファイルは%s",filename.c_str());
            
            if( *filename.rbegin() == '/' ) {
                log("DLはディレクトリを作成する");
                // It's a directory.
                cocos2d::FileUtils::getInstance()->createDirectory( outdir + filename );
            }
            else {
                // It's a file.
                ssize_t filesize;
                unsigned char* filedata = zipfile->getFileData( filename, &filesize );
                // ファイルデータが取得できたので、書き込みを行うスレッドへ送る
                pushToWriteFile(nullptr, outdir + filename, filedata, filesize);
            }
        }
        delete zipfile;
        
        // 終了判定用に空データを送る
        pushToWriteFile(onFinished, "", nullptr, 0);
    };
    
    // 最後にUIスレッドで実行されるタスク
    auto finished = [](void*){
        log("DLは読み込みが完了しました");
    };
    
    // 非同期タスクの開始 (TASK_OTHERのスレッドキューへ積まれる)
    cocos2d::AsyncTaskPool::getInstance()->enqueue(cocos2d::AsyncTaskPool::TaskType::TASK_IO, finished, nullptr, task);
     */
}

void HttpAccessManager::pushToWriteFile(onFinishedDownloadZip onFinished, const std::string& path, unsigned char* data, ssize_t size){
    log("pushToWriteFile: %s", path.c_str());
    
    // 別スレッドで実行されるタスク
    auto task = [path, data, size](){
        if( data ){
            FILE* file = fopen( path.c_str(), "wb" );
            fwrite( data, size, 1, file );
            fclose( file );
            CCLOG("endOfWriteFile: %s", path.c_str());
            
            // zipfile->getFileData で確保されたメモリを開放する
            free( data );
        }
    };
    // 最後にUIスレッドで実行されるタスク
    auto finished = [onFinished](void*){
        if(onFinished){ onFinished(true); }
    };
    // 非同期タスクの開始 (TASK_IOのスレッドキューへ積まれる)
    cocos2d::AsyncTaskPool::getInstance()->enqueue(cocos2d::AsyncTaskPool::TaskType::TASK_IO, finished, nullptr, task);
}

void HttpAccessManager::postNotification(const char *notificationKey)
{
    postNotification(notificationKey, Value(nullptr));
}

void HttpAccessManager::postNotification(const char *notificationKey, cocos2d::Value progressValue)
{
    auto evt = EventCustom(notificationKey);
    
    if (!progressValue.isNull()) {
        evt.setUserData(&progressValue);
    }
    Director::getInstance()->getEventDispatcher()->dispatchEvent(&evt);
}

