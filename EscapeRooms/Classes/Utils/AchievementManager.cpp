//
//  SettingDataManager.cpp
//  Kebab
//
//  Created by 成田凌平 on 2016/12/17.
//
//

#include "AchievementManager.h"

AchievementManager* AchievementManager::manager =NULL;

AchievementManager* AchievementManager::getInstance()
{
    if (manager==NULL) {
        manager=new AchievementManager();
        manager->init();
    }
    
    return manager;
}

void AchievementManager::init()
{//初期値をセット
    
}

ValueVector AchievementManager::loadingAchievementPlist()
{
    static auto achievementData = FileUtils::getInstance()->getValueVectorFromFile("achievement.plist");
    
    return achievementData;
}

ValueMap AchievementManager::loadingAchievementDataPlist()
{
    ValueMap valueMap;
    
    return valueMap;
}

bool AchievementManager::recordAchievement(const char *achievementID)
{
    
    
    return true;
}
