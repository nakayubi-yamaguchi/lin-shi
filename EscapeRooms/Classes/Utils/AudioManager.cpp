//
//  AudioManager.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/06/13.
//
//

#include "AudioManager.h"
#include "../SettingDataManager.h"

using namespace cocos2d;

//All static variables need to be defined in the .cpp file
//I've added this following line to fix the problem
AudioManager*AudioManager::manager=NULL;

AudioManager* AudioManager::getInstance()
{
    if (manager==NULL) {
        manager=new AudioManager();
    }
    
    return manager;
}

#pragma mark - BGM
void AudioManager::playBGM(const char *fileName)
{
    playBGM(fileName, true, 1);
}

void AudioManager::playBGM(const char *fileName, bool loop)
{
    playBGM(fileName, loop, 1);
}

void AudioManager::playBGM(const char *fileName, bool loop, float volume)
{
    if (SettingDataManager::sharedManager()->getStopBgm()) {
        return;
    }
    
    stopBgm();
    bgmID=AudioEngine::play2d(fileName,loop,volume,nullptr);
    log("BGM:%dを再生",bgmID);
}

void AudioManager::stopBgm()
{
    AudioEngine::stop(bgmID);
}

void AudioManager::resumeBGM()
{
    if (SettingDataManager::sharedManager()->getStopBgm()) {
        return;
    }
    AudioEngine::resume(bgmID);
}

int AudioManager::playSE(const char *fileName)
{
    return playSE(fileName,false);
}

int AudioManager::playSE(const char *fileName, bool checkSameSound)
{
    if (SettingDataManager::sharedManager()->getStopSe()) {
        return 0;
    }
    
    if (checkSameSound) {
        if (seNameMap[fileName].asBool()) {
            log("同じサウンドが再生中");
            return 0;
        }
    }
    
    //再生処理
    auto seID=AudioEngine::play2d(fileName);
    seIDs.push_back(seID);
    seNameMap[fileName]=Value(true);

    AudioEngine::setFinishCallback(seID, [this,fileName](int audioID,const std::string filePath){
        //終了時コールバック stopSEした際には呼ばれない
        auto itr=std::find(seIDs.begin(), seIDs.end(), audioID);
        if (itr!=seIDs.end()) {
            seIDs.erase(itr);
            seNameMap[filePath]=Value(false);
        }
    });
    
    return seID;
}

void AudioManager::stopAllSE()
{
    for (auto seID : seIDs) {
        AudioEngine::stop(seID);
    }
    seIDs.clear();
    seNameMap.clear();
}

#pragma mark All
void AudioManager::preload(std::string filepath)
{
    AudioEngine::preload(filepath);
}

void AudioManager::allFreeze()
{
    stopAllSE();
    AudioEngine::pause(bgmID);
}
