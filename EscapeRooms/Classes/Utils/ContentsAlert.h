//
//  EscapeRooms
//
//  Created by yamaguchinarita on 2017/11/22.
//
//

#ifndef _____EscapeContainer________ContentsAlert_____
#define _____EscapeContainer________ContentsAlert_____

#include "cocos2d.h"
#include "CustomAlert.h"

class ContentsAlert : public CustomAlert
{
public:
    static ContentsAlert* create(std::string subContentsSpriteName,std::string title, std::string contentsMessage, std::vector<std::string> buttons, const ccMenuCallback &callback);
    
    static ContentsAlert* create(Sprite* subContentsSprite, std::string title, std::string contentsMessage, std::vector<std::string> buttons, const ccMenuCallback &callback);
    static ContentsAlert* create(Sprite* subContentsSprite, std::string title, std::string contentsMessage, std::vector<std::string> buttons, const ccMenuCallback &callback,const ccMenuCallback &callback_pushClose);
    /**Spriteの幅基準でsetScaleしてるっぽい*/
    static ContentsAlert* create(Sprite* subContentsSprite, std::string title, std::string contentsMessage, std::vector<std::string> buttons,bool isVertical, const ccMenuCallback &callback,const ccMenuCallback &callback_pushClose);
    /**指定サイズにclip (spriteのサイズ基準)*/
    static ContentsAlert* create(Sprite* subContentsSprite, std::string title, std::string contentsMessage, std::vector<std::string> buttons,bool isVertical,Size clipSize, const ccMenuCallback &callback,const ccMenuCallback &callback_pushClose);
#pragma mark init
    virtual bool init(std::string subContentsSpriteName, std::string title, std::string contentsMessage, std::vector<std::string> buttons, const ccMenuCallback &callback);
    virtual bool init(std::string subContentsSpriteName, std::string title, std::string contentsMessage, std::vector<std::string> buttons, const ccMenuCallback &callback,const ccMenuCallback &callback_pushClose);
    virtual bool init(Sprite* subContentsSprite, std::string title, std::string contentsMessage, std::vector<std::string> buttons, const ccMenuCallback &callback);
    virtual bool init(Sprite* subContentsSprite, std::string title, std::string contentsMessage, std::vector<std::string> buttons,bool isVertical,Size clipSize, const ccMenuCallback &callback,const ccMenuCallback &callback_pushClose);
#pragma mark
    virtual void setAlertFont(std::string fontName) override;
    
    virtual void setSubContentsSprite(Sprite* subContentsSprite,Size clipSize);

    virtual void setContentsMessage(std::string message);
    ~ContentsAlert(){log("ContentsAlertが解放されました。");};
protected:
    //std::string m_constensSpriteName;
    std::string m_constensMessage;
    Sprite *m_contentsNode;
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
