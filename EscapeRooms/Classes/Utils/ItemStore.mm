//
//  StoreView.m
//  Otsuri
//
//  Created by 成田凌平 on 2015/10/15.
//  Copyright 2015年 yamaguchinarita. All rights reserved.
//

#import "ItemStore.h"
#import <StoreKit/StoreKit.h>
#include "Common.h"

//検証時のステータスコード
typedef enum{
    VerifyCode_NoError=0,
    VerifyCode_InvalidReceipt,
    VerifyCode_NoBase64String,
    VerifyCode_21007,//本番環境にsandboxレシート投げた時の
    VerifyCode_ConnectionFailed
}VerifyCode;

//共有シークレット。iTunesConnectから引っ張ってくる。：iTunesConnectから取得する。下記どちらを利用しても良いはず。
#define ShareSecretMasterKey "c62ddd047dab40aba3d5a336a94a84c4"//アプリ毎に変更する必要ない。
#define ShareSecretKey "d1f84668a23c44d59f607cda2b1e1618"

//サンドボックスURLと本番URL
#define SandboxUrlString "https://sandbox.itunes.apple.com/verifyReceipt"
#define UrlString "https://buy.itunes.apple.com/verifyReceipt"

#pragma mark- ストアのアイテム情報取得オブジェクト
@interface ItemInfo : NSObject<SKProductsRequestDelegate>

@property NSMutableDictionary *localePriceDic;//ストアの価格情報が詰まっている。
@property NSMutableDictionary *priceDic;//ストアの価格情報が詰まっている。
@property NSMutableDictionary *descriptionDic;//ストアの説明情報が詰まっている。

@end

@implementation ItemInfo

-(void)startLoadingInfo
{
    NSMutableSet *set = [NSMutableSet set];

    for (auto itemId: ItemConstants::getItemIds()) {
        NSLog(@"アイテムIDは%@",[NSString stringWithUTF8String:itemId.c_str()]);
        [set addObject:[NSString stringWithUTF8String:itemId.c_str()]];
    }
    
    SKProductsRequest* skProductsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:set];
    skProductsRequest.delegate = self;
    [skProductsRequest start];
}

// リクエスト取得完了時のdelegate
-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    if (response == nil) {
        NSLog(@"no response");
        return;
    }
    
    // 無効なアイテムがないかチェック
    if ([response.invalidProductIdentifiers count] > 0) {
        NSLog(@"PRODUCTION_ID error. count:%lu", (unsigned long)[response.invalidProductIdentifiers count]);
        // インジケータを止める
        return;
    }
    // 価格取得処理開始
    self.localePriceDic=@{}.mutableCopy;
    self.priceDic=@{}.mutableCopy;
    self.descriptionDic=@{}.mutableCopy;
    

    for (SKProduct *product in response.products) {
        // フォーマット
        NSNumberFormatter *numberFmt = [[NSNumberFormatter alloc] init];
        [numberFmt setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [numberFmt setNumberStyle:NSNumberFormatterCurrencyStyle];
        // PriceLocalを設定(ユーザーの環境に合わせた金額を取得する)
        [numberFmt setLocale:product.priceLocale];
        NSString *str = [numberFmt stringFromNumber:product.price];
        NSString *fmtStr = [NSString stringWithFormat:@"「購入金額は %@ です", str];
        NSLog(@"%@",fmtStr);
        
        NSLog(@"価格のみの読み込み%@",product.price);

        self.localePriceDic[product.productIdentifier]=str;
        self.priceDic[product.productIdentifier]=product.price.stringValue;
        self.descriptionDic[product.productIdentifier]=product.localizedDescription;
        
        // 他の情報
        NSLog(@"   id:%@",[product productIdentifier]);
        NSLog(@"title:%@",product.localizedTitle);
        NSLog(@" desc:%@",product.localizedDescription);
        NSLog(@"price:%@",product.priceLocale.localeIdentifier);
    }
}

-(void)dealloc
{
    NSLog(@"これ解放されたら都合悪い");
}

@end

#pragma mark- 購入処理コールバック取得オブジェクト
@interface ItemStoreCallback : NSObject <SKProductsRequestDelegate,SKPaymentTransactionObserver>
{
    void *delegate_id;
    NSString *itemID;
    BOOL isRestore;
    PurchaseType m_PurchaseType;
}

@end

@implementation ItemStoreCallback

-(id)initWithDelegate:(void*)delegate purchaseType:(PurchaseType)purchaseType purchaseItemID:(NSString*)purchaseItemID restore:(BOOL)restore
{
    self=[super init];
    if (self)
    {
        delegate_id=delegate;
        itemID=purchaseItemID;
        isRestore=restore;
        m_PurchaseType = purchaseType;
    }
    return self;
}

-(void)successPurchase:(SKPaymentTransaction*)transaction
{
    log("購入完了！！！");
    //通知をキャンセルしないと落ちる
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];

    if (m_PurchaseType == PurchaseType_Subscription) {//サブスクリプションなら検証
        ItemStore_plugin::ItemStore::checkAppStoreExpired([itemID UTF8String]);
    }
    else {//それ以外はコールバック
        [self callbackFinishPayment];
    }
}

-(void)callbackFinishPayment
{
    ItemStore_plugin::ItemStoreDelegate *delegate=(ItemStore_plugin::ItemStoreDelegate*)delegate_id;
    if (delegate!=NULL){
        log("購入成功！！！デリゲート先に通知します。%d",isRestore);
        delegate->didFinishPayment([itemID UTF8String], isRestore);
    }
}

-(void)happenedError:(ErrorType)errorType
{
    //通知をキャンセルしないと落ちる
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    
    ItemStore_plugin::ItemStoreDelegate *delegate=(ItemStore_plugin::ItemStoreDelegate*)delegate_id;
    if (delegate!=NULL)
    {
        log("エラーが発生したので、デリゲート先に通知します。");
        delegate->happenedError([itemID UTF8String], isRestore, errorType);
    }
}

-(void)dealloc
{
    log("アイテムストアのデリゲートを解放します。");
}

#pragma mark StoreKit Delegate
-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSLog(@"%s",__func__);
    //無効なアイテムチェック
    if ([response.invalidProductIdentifiers count] > 0)
    {
        [self happenedError:ErrorType_InvalidItemID];
        
        return;
    }
    
    //
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    if (isRestore) {
        NSLog(@"リストア処理開始");
        [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    }
    else{
        NSLog(@"購入処理開始");
        
        for (SKProduct *product in response.products)
        {
            
            NSLog(@"購入処理中のプロダクトは%@",product);

            SKPayment *payment = [SKPayment paymentWithProduct:product];
            [[SKPaymentQueue defaultQueue] addPayment:payment];
            payment=nil;
            
        }
    }
}

-(void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"そそそそ%s %@",__func__,error);
    
    [self happenedError:ErrorType_ConnectionFailed];
    
}

-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    NSLog(@"%s",__func__);
    
    for (SKPaymentTransaction *transaction in transactions)
    {
        if (transaction.transactionState == SKPaymentTransactionStatePurchasing)
        {
            // 購入処理中
            NSLog(@"購入処理中%@",transaction.error);
        }
        else if (transaction.transactionState == SKPaymentTransactionStatePurchased)
        {// 購入処理成功
            NSLog(@"購入処理成功");
            //購入完了デリゲートを発行
            [self successPurchase:transaction];
            
            [queue finishTransaction:transaction];
        }
        else if (transaction.transactionState == SKPaymentTransactionStateFailed)
        {
            NSLog(@"エラーの内容[%@]",transaction.error.description);

            // 購入処理エラー。ユーザが購入処理をキャンセルした場合もここにくる
            [queue finishTransaction:transaction];
            
            
            [self happenedError:ErrorType_Canceled];
            //エラー発生。
            // エラーが発生したことをユーザに知らせる
            
        }
        else if (transaction.transactionState ==SKPaymentTransactionStateRestored)
        {//リストア
            NSLog(@"restore処理");
            [queue finishTransaction:transaction];
        }
        else {
            NSLog(@"その他の場合です。エラーの内容[%@]",transaction.error.description);
        }
    }
}

-(void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions
{
    NSLog(@"うううううううううう%s",__func__);
    
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

-(void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{//リストア処理結果
    NSLog(@"っっっっっっっっk%s %@",__func__,error);
    
    [self happenedError:ErrorType_NoPurchaseHistory];
    
}


-(void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"リストア処理完了");
    SKPaymentTransaction *successTransaction;
    for (SKPaymentTransaction*transaction in queue.transactions) {
        if ([transaction.payment.productIdentifier isEqual:itemID]) {
            //一致。アイテムの再生
            successTransaction=transaction;
            break;
        }
    }
    
    if (successTransaction) {
        //リストア完了デリゲート発行
        [self successPurchase:successTransaction];
    }
    else{
        //履歴なし
        NSLog(@"履歴なし！！！");
        
        [self happenedError:ErrorType_NoPurchaseHistory];
    }
}
@end

#pragma mark- ItemStore_plugin
namespace ItemStore_plugin {
    ItemStoreCallback *itemCallback = NULL;
    static ItemInfo *itemInfo = NULL;
    
#pragma mark アイテム情報取得
    void ItemStore::loadingStoreInformation()
    {
        itemInfo=[[ItemInfo alloc]init];
        [itemInfo startLoadingInfo];
    }
    
    std::string ItemStore::getPrice(std::string itemID)
    {
        log("プライスを取得します%s",itemID.c_str());
        if (!itemInfo || !itemInfo.priceDic) {
            return "";
        }
        log("プライスを取得します%s",[itemInfo.priceDic[[NSString stringWithUTF8String:itemID.c_str()]] UTF8String]);
        
        return [itemInfo.priceDic[[NSString stringWithUTF8String:itemID.c_str()]] UTF8String];
    }
    
    std::string ItemStore::getLocalPrice(std::string itemID)
    {
        //log("プライスを取得します%s",itemID.c_str());
        if (!itemInfo || !itemInfo.localePriceDic) {
            return "";
        }
        //log("ffffプライスを取得します%s",[itemInfo.localePriceDic[[NSString stringWithUTF8String:itemID.c_str()]] UTF8String]);
        
        return [itemInfo.localePriceDic[[NSString stringWithUTF8String:itemID.c_str()]] UTF8String];
    }
    
    
    std::string ItemStore::getDescription(std::string itemID)
    {
        if (!itemInfo || !itemInfo.descriptionDic) {
            return "";
        }
        
        return [itemInfo.descriptionDic[[NSString stringWithUTF8String:itemID.c_str()]] UTF8String];
    }

    void ItemStore::checkAppStoreExpired(const char* itemId)
    {
        /**AppStoreにレシートの整合性を問い合わせるラムダ式*/
        auto getVerifyReceipt = [](const char* storeUrlString, std::function<void(VerifyCode verifyCode, NSDictionary * receiptJson)> callback){
            //ローカルからチェック
            NSString* base64String = nil;
            
            NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
            NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
            base64String = [receiptData base64EncodedStringWithOptions:0];
            
            NSLog(@"これからレシートの検証します");

            if (base64String == nil) {//ローカルにbase64のデータがない。リストアしてー
                if (callback) {
                    callback(VerifyCode_NoBase64String, nil);
                }
                return;
            }
            NSLog(@"レシートの検証します。%@",base64String);
            
            NSError *error;
            NSDictionary *requestContents = @{
                                              @"receipt-data" : base64String,
                                              @"password" : [NSString stringWithUTF8String:ShareSecretMasterKey]
                                              };
            
            NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents
                                                                  options:0
                                                                    error:&error];
            
            NSURL *storeURL = [NSURL URLWithString:[NSString stringWithUTF8String:storeUrlString]];
            NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
            [storeRequest setHTTPMethod:@"POST"];
            [storeRequest setHTTPBody:requestData];
            
            NSURLSessionConfiguration*configration = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession*session = [NSURLSession sessionWithConfiguration:configration delegate:nil delegateQueue:[NSOperationQueue mainQueue]];
            NSLog(@"セッションをはじめるよー。");
            
            [[session dataTaskWithRequest:storeRequest completionHandler:^(NSData*data, NSURLResponse*responce, NSError*error)
              {
                  NSLog(@"レシートの検証。");
                  
                  if (error) {
                      NSLog(@"取得エラー%@",error);
                      
                      if (callback) {
                          callback(VerifyCode_ConnectionFailed, nil);
                      }
                  }
                  else{
                      NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:NSJSONReadingMutableContainers
                                                                             error:nil];
                      NSLog(@"検証データ取得%@",json);
                      
                      int status = [json[@"status"] intValue];
                      NSString *error_text = json[@"error-text"];
                      
                      if (status==0 && error_text.length==0){//レシートは有効です
                          NSLog(@"レシートは有効です。");
                          if (callback) {
                              callback(VerifyCode_NoError, json);
                          }
                      }
                      else if (status==21007){//本番にサンドボックスリクエスト
                          NSLog(@"本番URLにサンドボックスの問い合わせしてるで。");
                          if (callback) {
                              callback(VerifyCode_21007, json);
                          }
                      }
                      else{//不正の可能性ありレシート
                          if (callback) {
                              callback(VerifyCode_InvalidReceipt, json);
                          }
                      }
                  }
                  
                  [session invalidateAndCancel];
              }] resume];
        };
        
        /**検証に失敗した時の処理記載のラムダ式*/
        auto failedVerify = [](){
            
            if (itemCallback) {//購入、リストア時なら検証失敗を返す。
                [itemCallback happenedError:ErrorType_FailedVerify];
            }
            
            bool isExistExpireData = (UserDefault::getInstance()->getStringForKey(ExpiredDateKey,"") != "");
            if (isExistExpireData) {
                log("定期購読のデータが更新されていなかったので、削除します。");
                //期限日データがあるので、削除します。
                UserDefault::getInstance()->setStringForKey(ExpiredDateKey, "");
            }
        };
        
        /**期限切れか確認するラムダ式*/
        auto checkExpired = [failedVerify](NSDictionary* jsonDic){
            
            NSString *bundleID = jsonDic[@"receipt"][@"bundle_id"];
            NSArray* latest_receipt_info = jsonDic[@"latest_receipt_info"];
            
            NSLog(@"バンドルIDは%@",bundleID);
            NSLog(@"jsonの全て%dのキーは%@",[[jsonDic allKeys] count],[jsonDic allKeys]);
            
            //NSLog(@"ステータス：%d、、最新のレシート%@,レシートの数は%lu個", status, latest_receipt_info, (unsigned long)[latest_receipt_info count]);
            //NSLog(@"レシートの数は%lu個", (unsigned long)[latest_receipt_info count]);
            
            bool isValid = false;
            
            for (NSDictionary* receipt in latest_receipt_info) {
                
                NSString* productID = receipt[@"product_id"];
                NSString* purchaseDate = receipt[@"purchase_date_ms"];
                long long expireDate = [receipt[@"expires_date_ms"] longLongValue];
                
                auto nowTime = cocos2d::utils::getTimeInMilliseconds();
                
                NSLog(@"製品名::%@\n購入日::%@\n期限日::%lld\n現時間::%lld",productID, purchaseDate, expireDate,nowTime);
                NSLog(@"期限日::%@",receipt[@"expires_date"]);

                if ([bundleID isEqualToString:[NSBundle mainBundle].bundleIdentifier] && //バンドルIDが一致
                    [productID isEqualToString:[NSString stringWithUTF8String:ItemID_Subscription]] && //プロダクトIDが一致
                    nowTime <= expireDate //期限内
                    ) {//期限
                    NSLog(@"レシート情報をゲットしています。%@",receipt);
                    
                    //ここで、期限日をユーザーデフォルトに保存する。
                    UserDefault::getInstance()->setStringForKey(ExpiredDateKey, StringUtils::format("%lld",expireDate));
                    
                    if (itemCallback) {//コールバックがある場合は購入、リストア時。
                        log("アイテムストアのコールバックがある場合は購入、リストア時");
                        [itemCallback callbackFinishPayment];
                    }
                    
                    isValid = true;
                    break;
                }
            }
            
            if (!isValid) {
                failedVerify();
            }
            NSLog(@"このレシートが有効かどうか%d",isValid);
        };
        
        /**検証結果ラムダ式*/
        auto callbackResultVerify = [checkExpired,failedVerify](VerifyCode code, NSDictionary* jsonDic){
            NSLog(@"コールバックを送信完了しました。");
            
            switch (code) {
                case VerifyCode_21007://テスト時に本番環境に問い合わせしている
                    NSLog(@"21007のエラー");
                    //getVerifyReceiptとcallbackResultVerifyで循環参照になりそうだから、ここでは何もしなくて良い。
                    
                    break;
                case VerifyCode_NoError:
                    NSLog(@"エラーなし");
                    checkExpired(jsonDic);
                    
                    break;
                case VerifyCode_InvalidReceipt:
                    NSLog(@"無効なレシート");
                    failedVerify();
                    
                    break;
                case VerifyCode_ConnectionFailed:
                    NSLog(@"通信失敗");
                    failedVerify();
                    
                    break;
                case VerifyCode_NoBase64String:
                    NSLog(@"base64がない");
                    failedVerify();
                    
                    break;
                default:
                    //検証失敗。
                    NSLog(@"検証失敗。");
                    failedVerify();

                    break;
            }
        };
        
        //検証開始！
        getVerifyReceipt(UrlString, [callbackResultVerify, getVerifyReceipt](VerifyCode code, NSDictionary *jsonDic){
            
            switch (code) {
                case VerifyCode_21007://テスト時に本番環境に問い合わせしている.もう一回やり直し
                    getVerifyReceipt(SandboxUrlString, callbackResultVerify);
                    
                    break;
                    
                default://21007以外のコールバック
                    NSLog(@"21007以外のコールバック");
                    callbackResultVerify(code, jsonDic);
                    
                    break;
            }
        });
    }
    
#pragma mark 購入処理
    void ItemStore::startPurchase(ItemStoreDelegate *delegate, std::string itemID, bool isRestore, PurchaseType purchaseType)
    {
        NSLog(@"リストア%d,,,,%d", isRestore, purchaseType);
        
        if (![SKPaymentQueue canMakePayments]) {//購入制限がかかっている
            
            if (delegate!=NULL)
            {
                log("エラーが発生したので、デリゲート先に通知します。");
                delegate->happenedError(itemID, isRestore, ErrorType_Limited);
            }
        }
        else{
            log("アイテムIDは%s",itemID.c_str());
            //プロダクトIDを指定
            NSString *itemIDSt=[NSString stringWithUTF8String:itemID.c_str()];
            
            NSSet *set = [NSSet setWithObjects:itemIDSt, nil];
            
            SKProductsRequest* request = [[SKProductsRequest alloc] initWithProductIdentifiers:set];
            
            itemCallback = [[ItemStoreCallback alloc]initWithDelegate:delegate
                                                         purchaseType:purchaseType
                                                 purchaseItemID:itemIDSt
                                                        restore:isRestore];
            
            
            request.delegate = itemCallback;
            [request start];
            
            set=nil;
        }
    }
    
    void ItemStore::showErrorAlert(std::string itemID, bool restore, ErrorType errorType)
    {
        
    }
    
#pragma mark 未処理トランザクション確認
    bool ItemStore::isUntreatedTransaction(std::string itemID)
    {
        bool isUntreated = false;
        if([[[SKPaymentQueue defaultQueue] transactions] count] >0){
            for(SKPaymentTransaction *skpt in [[SKPaymentQueue defaultQueue] transactions]){
                
                if(skpt.transactionState != SKPaymentTransactionStatePurchasing){
                    
                    switch (skpt.transactionState) {
                        case SKPaymentTransactionStatePurchased:
                        case SKPaymentTransactionStateRestored:{
                            auto untreatedItemID = StringUtils::format("%s",[skpt.payment.productIdentifier UTF8String]);
                            
                            if (untreatedItemID == itemID) {
                                isUntreated = true;
                                log("指定アイテムIDが該当しました。[%s]",itemID.c_str());
                            }
                            break;
                        }
                        default: {
                            log("それ以外のやつらを処理");
                            [[SKPaymentQueue defaultQueue] finishTransaction:skpt];
                            break;
                        }
                    }
                }
                
            }
        }
        
        return isUntreated;
    }
    
    void ItemStore::treatTransaction(std::string itemID)
    {
        if([[[SKPaymentQueue defaultQueue] transactions] count] >0){
            for(SKPaymentTransaction *skpt in [[SKPaymentQueue defaultQueue] transactions]){
                
                if(skpt.transactionState != SKPaymentTransactionStatePurchasing){
                    switch (skpt.transactionState) {
                        case SKPaymentTransactionStatePurchased:
                        case SKPaymentTransactionStateRestored:{
                            auto untreatedItemID = StringUtils::format("%s",[skpt.payment.productIdentifier UTF8String]);
                            if (untreatedItemID == itemID) {
                                log("[%s]トランザクションの終了処理を完了しました",itemID.c_str());
                                [[SKPaymentQueue defaultQueue] finishTransaction:skpt];
                                
                                log("トランザクションを確認します。%d",isUntreatedTransaction(itemID));
                            }
                            break;
                        }
                        default: {
                            log("それ以外のやつらきっとないはず");
                            break;
                        }
                    }
                }
            }
        }
    }
}


