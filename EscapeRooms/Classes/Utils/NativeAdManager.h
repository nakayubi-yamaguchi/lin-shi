//
//  AdfurikunNativeManager.h
//  PublicBath
//
//  Created by yamaguchinarita on 2017/05/16.
//
//

#ifndef __PublicBath__AdfurikunNativeManager__
#define __PublicBath__AdfurikunNativeManager__

#include "cocos2d.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "ADFMovieReward.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "Adfurikun/ADFMovieReward.h"

#endif


class NativeAdManager:public Adfurikun::ADFMovieNativeDelegate
{
public:
    static NativeAdManager* manager;
    
    static NativeAdManager* getInstance();
    
    void configureNativeAd();
    void loadingNativeAd();
    void showNativeAd(const char* bannerID, cocos2d::Vec2 point, cocos2d::Size nativeSize);
    void hideNativeAd();
    
    /**
     広告の読み込みが成功したときに呼ばれるコールバック
     @param const char* (appID) 広告枠ID
     */
    virtual void onNativeMovieAdViewLoadFinish(const char* appId);
    /**
     広告の読み込みが失敗したときに呼ばれるコールバック
     @param const char* (appID) 広告枠ID
     */
    virtual void onNativeMovieAdViewLoadError(const char* appId, int errorCode);
    /**
     広告の再生が開始したときに呼ばれるコールバック
     @param const char* (appID) 広告枠ID
     */
    virtual void onNativeMovieAdViewPlayStart(const char* appId);
    /**
     広告の再生が完了したときに呼ばれるコールバック
     @param const char* (appID) 広告枠ID
     @param bool (isVideoAd) 動画 : true , 静止画 : false
     */
    virtual void onNativeMovieAdViewPlayFinish(const char* appId, bool isVideoAd);
    /**
     広告の再生が失敗したときに呼ばれるコールバック
     @param const char* (appID) 広告枠ID
     @param int (errorCode) エラーコード
     */
    virtual void onNativeMovieAdViewPlayFail(const char* appId, int errorCode);
    
private:
    void initialize();
    
    
    //virtual void onNativeMovieAdViewLoadFinish (const char *appId);
    //virtual void onNativeMovieAdViewLoadError (const char *appId, int errorCode);
   
    /**広告の再生が開始したときに呼ばれるコールバック
     @param const char* (appID) 広告枠ID
     */
    //virtual void onNativeMovieAdViewPlayStart(const char* appId);
   
    /**広告の再生が完了したときに呼ばれるコールバック
     @param const char* (appID) 広告枠ID
     @param bool (isVideoAd) 動画 : true , 静止画 : false
     */
    //virtual void onNativeMovieAdViewPlayFinish(const char* appId, bool isVideoAd);
    
    /**広告の再生が失敗したときに呼ばれるコールバック
     @param const char* (appID) 広告枠ID
     @param int (errorCode) エラーコード
     */
    //virtual void onNativeMovieAdViewPlayFail(const char* appId, int errorCode);
};

#endif /* defined(__PublicBath__AdfurikunNativeManager__) */
