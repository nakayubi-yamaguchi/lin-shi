//
//  RewardMovieManager.h
//  EscapeRooms
//
//  Created by yamaguchinarita on 2018/09/09.
//

#ifndef __EscapeRooms__ShakeGestureManager__
#define __EscapeRooms__ShakeGestureManager__

#include "cocos2d.h"

USING_NS_CC;

/**バックグラウンドで自動でリスナー停止。シェイクジェスチャの複数箇所取得は想定していない。需要があれば今後実装します。*/
class ShakeGestureManager
{
public:
    static ShakeGestureManager* manager;
    static ShakeGestureManager* getInstance();
    
    void init();
    
    /**シェイクを検知開始。不要になったら、停止すること*/
    void startShakeGesture();

    /**シェイクを受け取る場所で呼ぶ。*/
    void addShakeGesture(Node* parent, const std::function<void()>& callback);
    /**シェイクを検知終了。デストラクタで絶対に呼ぶ*/
    void stopShakeGesture();

private:
    bool stepFlag;
    bool isBackground;
   
    void addNotification();
    
    /**シェイクジェスチャーを一時停止します。バックグラウンドに入った時に呼び出す。*/
    //void pauseShakeListener();
    
    /**シェイクジェスチャーを再開します。バックグラウンド復帰時*/
    //void resumeShakeListener();
   
    /**シェイク判定*/
    bool analyzeShake(double x, double y, double z);
   
    ~ShakeGestureManager();
};

#endif /* defined(__EscapeRooms__ShakeGestureManager__) */
