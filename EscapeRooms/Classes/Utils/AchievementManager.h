
#ifndef __EscapeRooms__AchievementManager__
#define __EscapeRooms__AchievementManager__

#include "cocos2d.h"

#define MapKey_ResultPoint "resultPoint"
#define MapKey_LocalizedName "localizedName"
#define MapKey_ID "id"

USING_NS_CC;

class AchievementManager
{
public:
    static AchievementManager* manager;
    static AchievementManager* getInstance();

    ValueVector loadingAchievementPlist();
    ValueMap loadingAchievementDataPlist();
    bool recordAchievement(const char* achievementID);
    
private:
    void init();

};

#endif /* defined(__EscapeRooms__AchievementManager__) */
