//
//  CustomAlert.cpp
//  Wedding
//
//  Created by yamaguchinarita on 2017/04/25.
//

#include "CustomAlert.h"
#include "Common.h"
#include "../UIParts/TextMenuItem.h"
#include "../UIParts/MenuItemScale.h"

CustomAlert* CustomAlert::create(std::string title, std::string message, std::vector<std::string> buttons, const ccMenuCallback &callback)
{
    return create(title, message, buttons, callback, nullptr);
}

CustomAlert* CustomAlert::create(std::string title, std::string message, std::vector<std::string> buttons, const cocos2d::ccMenuCallback &callback, const cocos2d::ccMenuCallback &callback_pushButton)
{
    return create(title, message, buttons, false, callback, callback_pushButton);
}

CustomAlert* CustomAlert::create(std::string title, std::string message, std::vector<std::string> buttons, bool isVertical, const cocos2d::ccMenuCallback &callback, const cocos2d::ccMenuCallback &callback_pushButton)
{
    auto node =new CustomAlert();
    if (node && node->init(title, message, buttons,isVertical, callback,callback_pushButton)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool CustomAlert::init() {
    
    return init("", "", {""},false, nullptr,nullptr);
}

bool CustomAlert::init(std::string title, std::string message, std::vector<std::string> buttons, const cocos2d::ccMenuCallback &callback)
{
    return init(title, message, buttons, false, callback, nullptr);
}

bool CustomAlert::init(std::string title, std::string message, std::vector<std::string> buttons, bool isVertical, const cocos2d::ccMenuCallback &callback, const cocos2d::ccMenuCallback &callback_pushButton)
{
    if (!LayerColor::initWithColor(Color4B(0, 0, 0, 0))) {
        return false;
    }
    
    // 初期化
    mTitle = title;
    mMessage = message;
    mButtons = buttons;
    mCallback = callback;
    mCallback_pushButton=callback_pushButton;
    m_closeDuration = 0;
    m_soundName = "";
    m_backOpacity = 80;
    setDisableButton(false);//なぜかtrueのときある
    setIsVertical(isVertical);

    space = getContentSize().height*.04;//タイトルラベル中心からてっぺんまで。メニューの中心から底辺まで。
    buttonSpace = space*.75;

    if (mButtons.size()==3) {//3個ボタンがあれば、スペースを小さくしていく
        buttonSpace = space*.2;
    }
    else if(mButtons.size() > 3) {//ボタン4個以上は作成できません。
        log("アラートのボタンを4個以上作成することはできません。他のUIを検討し給へ。");
        assert(false);
    }
    
    minSize = Size(getContentSize().width*.85, getContentSize().height*.3);

    if (isVertical) {
        buttonSize=Size(minSize.width*.8, getContentSize().height*.052);
    }
    else{
        buttonSize=Size((minSize.width-buttonSpace*(mButtons.size()+1))/mButtons.size(), getContentSize().height*.052);
    }
    
    addTouchListener();
    
    createMain();
    
    return true;
}

#pragma mark- protected
void CustomAlert::setLabelConfig(std::string fontName, cocos2d::Label *label)
{
    TTFConfig ttfConfig(Common::getUsableFontPath(fontName),
                        label->getTTFConfig().fontSize,
                        GlyphCollection::DYNAMIC);
    label->setTTFConfig(ttfConfig);
}

#pragma mark UI
void CustomAlert::createMain()
{
    mainSprite=Sprite::create();
    mainSprite->setColor(Color3B(230, 230, 230));
    mainSprite->setPosition(getContentSize().width/2, getContentSize().height/2);
    addChild(mainSprite);
    
    //タイトルラベル配置
    float titleFontSize = getContentSize().height*.032;
    
    titleLabel=Label::createWithTTF(mTitle, Common::getUsableFontPath(HiraginoMaruFont), titleFontSize);
    
    titleLabel->enableBold();
    titleLabel->setTextColor(Color4B::BLACK);
    titleLabel->setHeight(getContentSize().height*.048);
    mainSprite->addChild(titleLabel);
        
    //messageラベル。
    messageLabel = Label::createWithTTF(mMessage, Common::getUsableFontPath(HiraginoMaruFont), titleFontSize*.8);
    messageLabel->setWidth(minSize.width*.8);
    messageLabel->setColor(Color3B::BLUE);
    messageLabel->setTextColor(Color4B::BLACK);
    messageLabel->setLineHeight(messageLabel->getLineHeight()*1.05);
    mainSprite->addChild(messageLabel);
    
    //ボタン名の配列に""を追加すると、ボタンのサイズを小さくできる。
    std::vector<std::string> buttonNames;
    for (auto buttonName : mButtons) {
        if (buttonName != "") {
            buttonNames.push_back(buttonName);
        }
    }
    
    for (int i = 0; i < buttonNames.size(); i++) {
        auto menuText = buttonNames.at(i);
        
        auto item = TextMenuItem::create(menuText, buttonSize, Common::getColorFromHex(MyColor),[this](Ref*sender){
            if (this->getDisableButton()) {
                log("Disable");
                return ;
            }
            //連打無効処理
            auto menuItem = (MenuItemScale*)sender;
            menuItem->setEnabled(false);
            
            if (m_soundName.size() == 0) {
                Common::playClick();
            }
            else {
                 Common::playSE(m_soundName.c_str());
            }
            
            auto selectedItem = (MenuItemScale*) sender;
            //何番目のボタンが選択されたか。
            selectedButtonNum = selectedItem->getTag();
            
            if (mCallback_pushButton) {
                mCallback_pushButton(this);
            }
            
            this->closeAnimation(true);
        });
        item->setTag(i);
        m_menuItems.pushBack(item);
    }
    
    auto doneMenu = Menu::createWithArray(m_menuItems);
    doneMenu->setPosition(0, 0);
    mainSprite->addChild(doneMenu);
    
    //各々のUIを配置していきます。
    arrange();
}

void CustomAlert::arrange()
{
    //spaceはボタン半分の高さを含んでる?
    float titleAndButtonHeight = space * 4+(space+buttonSize.height/2)*(m_menuItems.size()-1)*getIsVertical();//タイトルとボタンの表示高さ verticalの時は随時+
    float contentsHeight = MAX(minSize.height , messageLabel->getContentSize().height + titleAndButtonHeight);
    
    log("%f::::カスタムアラート::::%f", minSize.height , messageLabel->getContentSize().height + titleAndButtonHeight);
    mainSprite->setTextureRect(Rect(0, 0, minSize.width, contentsHeight));
    
    titleLabel->setPosition(mainSprite->getContentSize().width/2, mainSprite->getContentSize().height-space);
    if (getIsVertical()) {
        messageLabel->setPosition(mainSprite->getContentSize().width/2, titleLabel->getPositionY()-space-messageLabel->getContentSize().height/2);
    }
    else{
        messageLabel->setPosition(mainSprite->getContentSize().width/2, mainSprite->getContentSize().height/2+space/4);//メッセージラベルを気持ち上に配置する。
    }
    
    //各ラベルの表示位置確認用
    Common::performProcessForDebug([this](){
        /*
        auto sp = Sprite::create();
        sp->setTextureRect(Rect(0, 0, titleLabel->getContentSize().width, titleLabel->getContentSize().height));
        sp->setColor(Color3B::ORANGE);
        sp->setOpacity(255/2);
        sp->setPosition(titleLabel->getPosition());
        mainSprite->addChild(sp, 2);
        
        auto sp1 = Sprite::create();
        sp1->setTextureRect(Rect(0, 0, messageLabel->getContentSize().width, messageLabel->getContentSize().height));
        sp1->setColor(Color3B::BLUE);
        sp1->setOpacity(255/2);
        sp1->setPosition(messageLabel->getPosition());
        mainSprite->addChild(sp1, 2);
        
        mainSprite->setColor(Color3B::GREEN);*/
    }, nullptr);
   

    for (int i = 0; i<m_menuItems.size(); i++) {
        auto item = m_menuItems.at(i);
        if (getIsVertical()) {
            item->setPosition(
                              mainSprite->getContentSize().width/2, space+(space+buttonSize.height/2)*(m_menuItems.size()-i-1));
        }
        else{
            item->setPosition(
                              mainSprite->getContentSize().width/2 - (item->getContentSize().width + buttonSpace)/2*(m_menuItems.size()-1) + (item->getContentSize().width+buttonSpace)*i, space);
        }
    }    
}

void CustomAlert::showAnimation(const cocos2d::ccMenuCallback &callback)
{
    //表示完了までボタンのタッチ効かなくする
    for (auto buttonItem : m_menuItems) {
        buttonItem->setEnabled(false);
    }
    
    //アニメーション
    mainSprite->setScale(.8);
    auto ease = TargetedAction::create(mainSprite, EaseOut::create(ScaleTo::create(.15, 1.15), 2));
    auto ease2 = TargetedAction::create(mainSprite, EaseOut::create(ScaleTo::create(.1, 1), 2));
    auto selfFadein = FadeTo::create(.15, m_backOpacity);
    auto spawn = Spawn::create(selfFadein, ease2, NULL);//背景がフェードインしながら、元のサイズに戻る
    auto call = CallFunc::create([callback,this]{
        
        //表示完了したら、タッチを有効にする
        for (auto buttonItem : m_menuItems) {
            buttonItem->setEnabled(true);
        }
        
        //表示完了のコールバック
        if (callback) {
            callback(this);
        }
    });
    runAction(Sequence::create(ease, spawn,call, NULL));
}

void CustomAlert::closeAnimation(bool withButtonCallback)
{
    //フェードアウトしながら、小さくなる。
    float duration = .2;
    mainSprite->setCascadeOpacityEnabled(true);//mainspriteの透明度を子供にも反映
    auto fadeout = FadeOut::create(duration) ;
    auto mainFadeout = TargetedAction::create(mainSprite, Spawn::create(FadeOut::create(duration/2), ScaleTo::create(duration, .5), NULL));
    auto spawnFadeout = Spawn::create(fadeout, mainFadeout, NULL);
    auto closeDelay = DelayTime::create(m_closeDuration);//コールバックが呼ばれるまでの時間。
    auto callback = CallFunc::create([this, withButtonCallback](){//フェードアウトが完了してから、コールバックを返す。
        if (mCallback && withButtonCallback) {//コールバック
            mCallback(this);
        }
        log("カスタムアラートのコールバックを返すアニメーション");
        if (m_closeCallback && !withButtonCallback) {//閉じるボタンが押された時のコールバック
            m_closeCallback();
        }
    });
    auto remove = RemoveSelf::create();
    this->runAction(Sequence::create(spawnFadeout, closeDelay, callback, remove, NULL));
}
#pragma mark- タッチ
void CustomAlert::addTouchListener()
{
    auto listener=EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);//タッチ透過しない
    
    listener->onTouchBegan=[this](Touch*touch, Event*event){
        return true;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

#pragma mark- Public
float CustomAlert::getMessageLineHeight()
{
    //log("メッセージの行間は%f",messageLabel->getLineHeight());
    return messageLabel->getLineHeight();
}

void CustomAlert::setMessageLineHeight(float lineHeight)
{
    messageLabel->setLineHeight(lineHeight);

    //高さを変えたら、再整列
    arrange();
}

void CustomAlert::setCloseDuration(float duration)
{
    m_closeDuration = duration;
}

void CustomAlert::setAlertFont(std::string fontName)
{
    setLabelConfig(fontName, titleLabel);
    setLabelConfig(fontName, messageLabel);
    
    //ボタンのフォントを変更
    for (auto menuItem : m_menuItems) {
        auto scaleMenuItem = (TextMenuItem*)menuItem;
        auto normalImage = scaleMenuItem->getNormalImage();
        auto buttonTextLabel = (Label*)normalImage->getChildByTag(ButtonLabelTag);
        setLabelConfig(fontName, buttonTextLabel);
    }
}

void CustomAlert::displayCloseButton(const char* fileName)
{
    auto spr=Sprite::createWithSpriteFrameName(fileName);
    spr->setColor(Common::getColorFromHex(LightBlackColor));
    auto select=Sprite::createWithSpriteFrameName(fileName);
    select->setColor(Common::getColorFromHex(BlackColor));
    
    auto menuitem=MenuItemSprite::create(spr, select, [this](Ref*ref){
        Common::playClick();
        this->closeAnimation(false);
    });
    menuitem->setScale(titleLabel->getContentSize().height*.9/menuitem->getContentSize().width);
    menuitem->setPosition(mainSprite->getContentSize()-menuitem->getBoundingBox().size*1.5/2);
    
    auto closeMenu=Menu::create(menuitem, NULL);
    closeMenu->setPosition(0, 0);
    mainSprite->addChild(closeMenu);
}

void CustomAlert::displayCloseButton()
{
    onTappedClose callback = []{};
    displayCloseButton(callback);
}

void CustomAlert::displayCloseButton(const onTappedClose& callback)
{
    m_closeCallback = callback;
    
    auto createLabel = [this]()->Label*{
        
        auto closeLabel = Label::createWithTTF("×", HiraginoMaruFont, titleLabel->getContentSize().height*.9);
        closeLabel->setTextColor(Color4B(Common::getColorFromHex(BlackColor)));
        closeLabel->enableBold();
        
        return closeLabel;
    };
    
    auto normalL = createLabel();
    auto selectedL = createLabel();
    selectedL->setOpacity(255/2);
    
    auto menuitem = MenuItemLabel::create(normalL, [this](Ref*ref){
        if (!getDisableButton()) {
            Common::playClick();
            this->closeAnimation(false);
        }
    });
    //menuitem->setScale(titleLabel->getContentSize().height*.9/menuitem->getContentSize().width);
    menuitem->setPosition(mainSprite->getContentSize()-menuitem->getBoundingBox().size*1.5/2);
    
    auto closeMenu=Menu::create(menuitem, NULL);
    closeMenu->setPosition(0, 0);
    mainSprite->addChild(closeMenu);
}

void CustomAlert::setTitleColor(const char *colorCode)
{
    titleLabel->setTextColor(Color4B(Common::getColorFromHex(colorCode)));
}

void CustomAlert::setMessageColor(const char *colorCode)
{
    messageLabel->setTextColor(Color4B(Common::getColorFromHex(colorCode)));
}

void CustomAlert::showAlert(cocos2d::Node *parent)
{
    showAlert(parent, nullptr);
}

void CustomAlert::showAlert(cocos2d::Node *parent, const cocos2d::ccMenuCallback &callback)
{
    parent->addChild(this);
    showAnimation(callback);
}

void CustomAlert::showAlert()
{
    auto runningScene = Director::getInstance()->getRunningScene();
    showAlert(runningScene,nullptr);
}

void CustomAlert::showAlert(const cocos2d::ccMenuCallback &callback)
{
    auto runningScene = Director::getInstance()->getRunningScene();
    showAlert(runningScene,callback);
}

void CustomAlert::setCallback(const cocos2d::ccMenuCallback &callback)
{
    mCallback = callback;
}

void CustomAlert::setSelectedButtonCallback(const cocos2d::ccMenuCallback &callback)
{
    mCallback_pushButton = callback;
}

void CustomAlert::setButtonColor(cocos2d::Color3B color, int index)
{
    auto item=(TextMenuItem*)m_menuItems.at(index);
    item->setButtonColor(color);
}

#pragma mark- 解放
CustomAlert::~CustomAlert()
{
    log("カスタムレイヤーを解放する。");
}


