//
//  NativeBridge.m
//  Rocket
//
//  Created by 成田凌平 on 2016/02/21.

#import "NativeBridge.h"
#import <Social/Social.h>
#import "AppController.h"
#import <StoreKit/StoreKit.h>
#include "FireBaseBridge.h"
#import <AudioToolbox/AudioToolbox.h>

//Twitter外し
//#import <TwitterKit/TwitterKit.h>
#import <TwitterKit/TWTRKit.h>


@interface NativeBridgeCallback : NSObject <TWTRComposerViewControllerDelegate>
{
}

@end

@implementation NativeBridgeCallback

#pragma mark TwitterDelegate（iOS10以上）
-(void)composerDidCancel:(TWTRComposerViewController *)controller
{
    NSLog(@"コンポーザーをキャンセルしました。");
}

-(void)composerDidFail:(TWTRComposerViewController *)controller withError:(NSError *)error
{
    NSLog(@"コンポーザーでエラー発生%@",error.userInfo);

    if (error.domain == TWTRAPIErrorDomain) {//端末にはログイン状態が保持されているが、twitter側では連携が解除されている。端末上でログアウトだけしておく。
        NSLog(@"エラードメイン");
        
        TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
        NSString *userID = store.session.userID;
        [store logOutUserID:userID];
    }
}

-(void)composerDidSucceed:(TWTRComposerViewController *)controller withTweet:(TWTRTweet *)tweet
{
    NSLog(@"コンポーザー成功しました。");
}

#pragma mark 解放
-(void)dealloc{
    NSLog(@"%s",__func__);
}

@end

NativeBridgeCallback *nativeCallback = [[NativeBridgeCallback alloc] init];


#pragma mark- openURL
void NativeBridge::openUrl(const char *url)
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithCString:url encoding:NSUTF8StringEncoding]]];
}

#pragma mark- シェア

void NativeBridge::openShareMenu(const char *text)
{
    openShareMenu(text, nil);
}

void NativeBridge::openShareMenu(const char *text,const char *filePath)
{
    NSMutableArray *shareItems=@[].mutableCopy;
    
    if (text) {
        [shareItems addObject:[NSString stringWithCString:text encoding:NSUTF8StringEncoding]];
    }
    
    if (filePath) {

        NSData *imageData = [NSData dataWithContentsOfFile:[NSString stringWithCString:filePath encoding:NSUTF8StringEncoding]];
        [shareItems addObject:imageData];
    }
    
    UIViewController*root=[UIApplication sharedApplication].keyWindow.rootViewController;
    
    UIActivityViewController*activity=[[UIActivityViewController alloc]initWithActivityItems:shareItems applicationActivities:nil];
    
    [activity setCompletionHandler:^(NSString*act,BOOL success){
        NSLog(@"アクティビティ終了");
        //通知飛ばすっちゃ
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent("DidEndActivity");
    }];
    
    if ([[[UIDevice currentDevice]systemVersion]floatValue]>=8.0) {
        activity.popoverPresentationController.sourceView=root.view;
    }
    
    [root presentViewController:activity animated:YES completion:nil];
    activity=nil;
}

void NativeBridge::openShareMenu(ShareType shareType, const char *tweet, const char *path)
{
    
    switch (shareType) {
        case ShareType_Activity:
            NativeBridge::openShareMenu(tweet,path);
            break;
            
        case ShareType_Twitter:
            NativeBridge::openWithTwitter(tweet,path);
            break;
            
        case ShareType_FaceBook:
            NativeBridge::openWithFaceBook(tweet,path);
            break;
            
        case ShareType_LINE:
            NativeBridge::openWithLINE(path);
            break;
            
        default:
            break;
    }
}

void NativeBridge::openWithTwitter(const char *tweet, const char *path)
{
    log("Twitterの投稿準備を行います。");
    NSString *text = [NSString stringWithCString:tweet encoding:NSUTF8StringEncoding];
    
    UIImage *image;
    if (path != nullptr) {
        NSString*p = [NSString stringWithCString:path encoding:NSUTF8StringEncoding];
        image = [UIImage imageWithContentsOfFile:p];
        if (image != nil) {
        }
    }
    
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    if (!rootViewController) {
        rootViewController = [UIApplication sharedApplication].windows.firstObject.rootViewController;
    }
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_9_x_Max) {//ios10以降
        log("Twitterの投稿準備を行います。1");
        
        /*
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Twitter can't be available." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        
        [controller presentViewController:alert animated:YES completion:nil];*/
        
        /*
        TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
        NSString *userID = store.session.userID;
        [store logOutUserID:userID];
         */
        
        //Twitter外し.代わりに上のエラーアラート追加
        if ([[Twitter sharedInstance].sessionStore hasLoggedInUsers]) {
            NSLog(@"Twitterの投稿準備を行います。2");
            
            TWTRComposerViewController *composer = [[TWTRComposerViewController emptyComposer] initWithInitialText:text image:image videoURL:nil];
            composer.delegate = nativeCallback;


            [rootViewController presentViewController:composer animated:YES completion:^{
                log("Twitterの投稿準備完了");
            }];
            
        } else {
            [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
            
                if (session) {
                    TWTRComposerViewController *composer = [[TWTRComposerViewController alloc] initWithInitialText:text image:image videoURL:nil];;
                    [rootViewController presentViewController:composer animated:YES completion:nil];
                    log("Twitterの投稿準備を行います。3");

                } else {
                    log("Twitterの投稿準備を行います。4");

                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No Twitter Accounts Available" message:@"You must log in before presenting a composer." preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];

                    [rootViewController presentViewController:alert animated:YES completion:nil];
                }
            }];
        }
    }
    else {
        NSLog(@"iOS9以下なので、socia");
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            SLComposeViewController *composeController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            [rootViewController presentViewController:composeController
                                     animated:YES
                                   completion:^{
                                       [composeController setCompletionHandler:^(SLComposeViewControllerResult result) {
                                           if (result == SLComposeViewControllerResultDone) {
                                               [rootViewController dismissViewControllerAnimated:YES completion:nil];
                                               
                                           } else if (result == SLComposeViewControllerResultCancelled) {
                                               
                                           }
                                       }];
                                   }];
        }
    }
}

void NativeBridge::openWithFaceBook(const char *tweet, const char *path)
{
    float iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];

    if (iOSVersion >= 11) {//ios11以上の端末
        UIWindow* window = [[UIApplication sharedApplication] keyWindow];
        UIViewController* controller = window.rootViewController;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"For ios 11 and later, share using facebook can not be used."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        
        [controller presentViewController:alert animated:YES completion:nil];
    }
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *composeController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        NSString *text = [NSString stringWithCString:tweet encoding:NSUTF8StringEncoding];
        [composeController setInitialText:text];
        
        if (path != nullptr) {
            NSString*p = [NSString stringWithCString:path encoding:NSUTF8StringEncoding];
            UIImage *image = [UIImage imageWithContentsOfFile:p];
            if (image != nil) {
                [composeController addImage:image];
            }
        }
        
        UIWindow* window = [[UIApplication sharedApplication] keyWindow];
        UIViewController* controller = window.rootViewController;
        [controller presentViewController:composeController
                                 animated:YES
                               completion:^{
                                   [composeController setCompletionHandler:^(SLComposeViewControllerResult result) {
                                       if (result == SLComposeViewControllerResultDone) {
                                           [controller dismissViewControllerAnimated:YES completion:nil];
                                           
                                       } else if (result == SLComposeViewControllerResultCancelled) {
                                           
                                       }
                                   }];
                               }];
    }
}


std::string url_encode(const std::string &value) {
    std::ostringstream escaped;
    escaped.fill('0');
    escaped << std::hex;
    
    for (auto i = value.begin(); i != value.end(); ++i) {
        unsigned char c = (*i);
        
        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
            escaped << c;
            continue;
        }
        
        escaped << '%' << std::setw(2) << int(c);
    }
    
    return escaped.str();
}

void NativeBridge::openWithLINEText(const std::string& message)
{
    std::string encodedMessage = url_encode(message);
    std::string urlString = cocos2d::StringUtils::format(LINE_URL_SCHEME, CONTENT_TEXT, encodedMessage.c_str());
    cocos2d::Application::getInstance()->openURL(urlString);
    
    log("ユーアールエル%s",urlString.c_str());
}

void NativeBridge::openWithLINE(const char *path)
{
    if (path != nullptr) {
        NSString*p = [NSString stringWithCString:path encoding:NSUTF8StringEncoding];
        UIImage *image = [UIImage imageWithContentsOfFile:p];

        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        
        [pasteboard setData:UIImagePNGRepresentation(image) forPasteboardType:@"public.png"];
        
        //NSString *LineUrlString = [NSString stringWithFormat:@"line://msg/image/%@", pasteboard.name];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"line://"]];
        
        //投稿に成功した場合
        NSLog(@"投稿しました");
    }
}

#pragma mark - 設定言語
std::string NativeBridge::getCurrentLanguage()
{
    NSString * language = [[NSLocale preferredLanguages] firstObject];
    
    return [language UTF8String];
}

#pragma mark - ローカル通知
void NativeBridge::registerNotificationSetteing()
{
    UIApplication*application=[UIApplication sharedApplication];
    //local通知
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:
                                                UIUserNotificationTypeBadge|
                                                UIUserNotificationTypeAlert|
                                                UIUserNotificationTypeSound categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    }
}

void NativeBridge::fireLocalNotification(int time, const char *message)
{
    // 通知を作成する
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    
    notification.fireDate = [[NSDate date] dateByAddingTimeInterval:time];
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.alertBody =[NSString stringWithCString:message encoding:NSUTF8StringEncoding];
    notification.alertAction = @"Open";
    notification.soundName = UILocalNotificationDefaultSoundName;
    
    NSLog(@"通知セット %@",notification.fireDate);

    // 通知を登録する
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

void NativeBridge::cancelAllLocalNotification()
{//全キャンセル
    [[UIApplication sharedApplication]cancelAllLocalNotifications];
}
#pragma mark- +1ボタン
void NativeBridge::createPlusOneButton(float x, float y, float width, float height)
{
    NSLog(@"+1ボタンはないのでつけません。");
}

void NativeBridge::removePlusOneButton()
{
    NSLog(@"+1ボタンはないので削除しません。");   
}

#pragma mark- 端末判定
bool NativeBridge::isTablet()
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        //NSLog(@"スマートフォンなので、スマフォ用IDを返します。");
        return false;
    }
    
    //NSLog(@"タブレットなので、タブレット用IDを返します。");
    return true;
}

const char* NativeBridge::judgeID(const char* smartPhoneID, const char* tabletID)
{
    return (!isTablet())? smartPhoneID: tabletID;
}

std::string NativeBridge::getOSVersion()
{
    return StringUtils::format("%f",[[[UIDevice currentDevice] systemVersion] floatValue]);
}

#pragma mark- ストレージ
unsigned long long NativeBridge::storageAvailableMb()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSDictionary *dict = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error:nil];
    
    if (dict) {
        int Mb = 1024 * 1024;
        unsigned long long free = [[dict objectForKey: NSFileSystemFreeSize] unsignedLongLongValue] / Mb;
        return (int)free;
    }
    
    return 0;
}

#pragma mark- オンライン確認
bool NativeBridge::isOnline()
{
    BOOL isOnline = NO;
    /*
    BOOL isOnline = [FBNetworkReachability sharedInstance].reachable;
    //[FBNetworkReachability sharedInstance].reachable;
        NSLog(@"オンライン状態は%d",isOnline);
    */
    return isOnline;
}

#pragma mark- レビュー求む
bool NativeBridge::requestReview()
{
    // iOS 10.3以上の処理
    float iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (iOSVersion > 10.2f) {
        if([SKStoreReviewController class]){
            [SKStoreReviewController requestReview];
        }
        
        return true;
    }
    else {
        /*
        NSString *url = [NSString stringWithFormat: @"%@%d%@", @"itms-apps://itunes.apple.com/app/id/", AppID_iOS, @"?action=write-review"];
        // iOS 10以上
        if (iOSVersion > 9.9f) {
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]
                                               options: @{}
                                     completionHandler: nil];
        }
        // iOS 10未満
        else {
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
        }*/
    }
    return false;
}

std::string NativeBridge::transformThreeComma(float num)
{
    NSNumber *number = [NSNumber numberWithFloat:num];
    // NSNumberFormatterを生成
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    
    // NSNumberを3桁区切りに変換
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    NSString *formattedNumber = [formatter stringFromNumber:number];
    
    // コンソールに出力
    //NSLog(@"result: %@", formattedNumber); // 「result: 123,456,789」が出力される
    
    return StringUtils::format("%s",[formattedNumber UTF8String]);
    
}

#pragma mark- バイブレーション
void NativeBridge::vibration()
{
    if (!SettingDataManager::sharedManager()->getStopVibe()) {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
}

void NativeBridge::vibration3D()
{
    if (!SettingDataManager::sharedManager()->getStopVibe()) {
        AudioServicesPlaySystemSound(1520);
    }
}

#pragma mark- UUID生成
std::string NativeBridge::createUUID()
{
    NSString *UUIDSt=[[NSUUID UUID] UUIDString];
    
    return [UUIDSt UTF8String];
}
