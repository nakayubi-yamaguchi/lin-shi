//
//  FireBaseBridge.cpp
//  PuzzleWord2
//
//  Created by 成田凌平 on 2016/05/28.
//
//

#include "FireBaseBridge.h"
#include "NativeBridge.h"
#include "ValueHelper.h"
#include "Common.h"
#include "ProjectConstants.h"

using namespace cocos2d;

//パッケージ名を指定
#define CLASS_NAME "org/cocos2dx/cpp/FireBaseBridge"
class FirebaseHelper
{
public:
    static Value transformToValue(JNIEnv* env, jobject obj)
    {
        log("トランスフォームします");
        Value value;
        
        jclass longClsj = env->FindClass("java/lang/Long");
        jclass doubleClsj = env->FindClass("java/lang/Double");
        jclass booleanClsj = env->FindClass("java/lang/Boolean");
        jclass stringClsj = env->FindClass("java/lang/String");
        jclass arrayClsj = env->FindClass("java/lang/reflect/Array");
        jclass mapClsj = env->FindClass("java/util/HashMap");

        /*
        if (longClsj==NULL) {
            log("Integerクラスは存在しない");
            return ;}
        if (stringClsj==NULL) {
            log("Stringクラスは存在しない");
            return;}
        if (arrayClsj==NULL) {
            log("Arrayクラスは存在しない");
            return;}
        if (mapClsj==NULL) {
            log("Mapクラスは存在しない");
            return;}*/
        
        if (obj!=NULL && env->IsInstanceOf(obj, longClsj)) {//LONG
            
            jmethodID longValueMID   = env->GetMethodID(longClsj, "longValue", "()J");
            long longValue           = (long) env->CallLongMethod(obj, longValueMID);
            
            value = Value((double)longValue);
            log("Integerクラスです%ld",longValue);
        }else if (obj!=NULL && env->IsInstanceOf(obj, doubleClsj)) {//Double
            jmethodID doubleValueMID   = env->GetMethodID(doubleClsj, "doubleValue", "()D");
            double doubleValue           = (double) env->CallDoubleMethod(obj, doubleValueMID);
            
            value = Value((double)doubleValue);
            log("Doubleクラスです%f",doubleValue);
        }else if (obj!=NULL && env->IsInstanceOf(obj, booleanClsj)) {//Boolean
            jmethodID booleanValueMID   = env->GetMethodID(booleanClsj, "booleanValue", "()Z");
            bool boolValue           = (bool) env->CallBooleanMethod(obj, booleanValueMID);
            
            value = Value(boolValue);
            log("Booleanクラスです%d",boolValue);
        }else if (obj!=NULL && env->IsInstanceOf(obj, stringClsj)) {//STRING
            std::string ret = cocos2d::JniHelper::jstring2string((jstring)obj); // jstringをstd::stringに変換
            value = Value(ret);
            
            log("Stringクラスです%s",ret.c_str());
        }else if (obj!=NULL && env->IsInstanceOf(obj, arrayClsj)) {//ARRAY
            log("Arrayクラスです");
        }
        else if (obj!=NULL && env->IsInstanceOf(obj, mapClsj)) {//MAP
            
            ValueMap valueMap;
            jmethodID map_put_mid = 0;
            jmethodID size_method = 0;
            jmethodID map_get_mid = 0;
            jmethodID map_keySet_mid = 0;
            
            jclass jsetclass = env->FindClass("java/util/Set");
            jmethodID jtoArraymid = env->GetMethodID(jsetclass, "toArray", "()[Ljava/lang/Object;");

            size_method = env->GetMethodID(mapClsj, "size", "()I");
            map_get_mid = env->GetMethodID(mapClsj, "get",
                                           "(Ljava/lang/Object;)Ljava/lang/Object;");
            map_keySet_mid = env->GetMethodID(mapClsj, "keySet", "()Ljava/util/Set;");

            int size = (int) env->CallIntMethod(obj, size_method);
            //log("Mapクラスです%d",size);
            
            jobject jkeys = env->CallObjectMethod(obj, map_keySet_mid);
            jobjectArray jobjArray = (jobjectArray)env->CallObjectMethod(jkeys, jtoArraymid);

            for (int i=0; i<size; i++) {
                //url
                jstring jstr=(jstring)env->GetObjectArrayElement(jobjArray, i);
                std::string key = cocos2d::JniHelper::jstring2string(jstr); // jstringをstd::stringに変換
                
                if (key == "Users") {
                    log("Usersだったから拒否");
                    break;
                }
                //const char * cstring = env->GetStringUTFChars(jstr, 0);
                log("Mapクラスです%s",key.c_str());
                
                jobject map_value = (jobject)env->CallObjectMethod(obj, map_get_mid, jstr);
                
                valueMap[key] = transformToValue(env, map_value);
                
                env->DeleteLocalRef(jstr);// ★jstringは解放が必要です。
                env->DeleteLocalRef(map_value);
            }
            value = Value(valueMap);
            
            env->DeleteLocalRef(jkeys);
            env->DeleteLocalRef(jobjArray);
        }
        else {
            jclass cls = env->GetObjectClass(obj);

            // First get the class object
            jmethodID mid = env->GetMethodID(cls, "getClass", "()Ljava/lang/Class;");
            jobject clsObj = env->CallObjectMethod(obj, mid);
            
            // Now get the class object's class descriptor
            cls = env->GetObjectClass(clsObj);
            
            // Find the getName() method on the class object
            mid = env->GetMethodID(cls, "getName", "()Ljava/lang/String;");
            
            // Call the getName() to get a jstring object back
            jstring strObj = (jstring)env->CallObjectMethod(clsObj, mid);
            
            std::string key = cocos2d::JniHelper::jstring2string(strObj); // jstringをstd::stringに変換

            log("不明のクラスです。これが呼ばれたらおかしい。%s",key.c_str());
            env->DeleteLocalRef(strObj);// ★jstringは解放が必要です。
        }
        
        env->DeleteLocalRef(longClsj);
        env->DeleteLocalRef(doubleClsj);
        env->DeleteLocalRef(booleanClsj);
        env->DeleteLocalRef(stringClsj);
        env->DeleteLocalRef(arrayClsj);
        env->DeleteLocalRef(mapClsj);
        
        return value;
    }
};

FireBaseBridge* manager=NULL;

bool _doCallback=false;
int alertCount=0;
std::vector<HouseAlert>alertObjects;
int iconCount=0;
std::map<std::string, onFIRFinished> finishedCallbackMap;
std::map<std::string, onGetUrl> urlCallbackMap;
std::map<std::string, onFinishLoadFile> dlFileCallbackMap;
std::map<std::string, onProgressLoadFile> prFileCallbackMap;

std::map<std::string, onFinishedLoadValue> valueCallbackMap;

#pragma mark- イニシャライズ
FireBaseBridge* FireBaseBridge::getInstance()
{
    //If the singleton has no instance yet, create one
    if(NULL == manager)
    {
        //Create an instance to the singleton
        manager = new FireBaseBridge();
        manager->initialize();
    }
    
    //Return the singleton object
    return manager;
}

void FireBaseBridge::initialize()
{
    
}

#pragma mark - セットアップ
void FireBaseBridge::setUp()
{
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "setUpFirebase", "(Ljava/lang/String;)V")){
        assert(false);
        return;
    }
    
    jstring strURLArg = methodInfo.env->NewStringUTF(StrageUrl);
    
    // 関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, strURLArg);
    
    // 解放
    methodInfo.env->DeleteLocalRef(strURLArg);// ★jstringは解放が必要です。
    
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

#pragma mark- 自社アラート
void FireBaseBridge::loadingAlertHouseAd(bool doCallback)
{
    _doCallback=doCallback;
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "loadingAlertHouseAd", "()V"))
    {
        assert(false);
        return;
    }
    
    // 関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID);
    
    // 解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void FireBaseBridge::shuffleAlerts()
{//シャッフル
    std::vector<HouseAlert>retainAlerts;
    retainAlerts=alertObjects;
    
    alertObjects.clear();
    
    while (retainAlerts.size()>0) {
        
        int size=retainAlerts.size();
        
        //log("保持アラートの数%d",size);
        
        auto random=RandomHelper::random_int(0, size-1);//抽選
        
        auto obj=retainAlerts[random];
        
        // log("追加するメッセージは%s",obj.message);
        
        if (obj.isNew==true) {
            //log("Newがあったので、先頭に持ってくる%d",obj.isNew);
            alertObjects.insert(alertObjects.begin(),obj);//先頭に挿入
        }
        else{
            alertObjects.push_back(obj);//末尾に追加
        }
        retainAlerts.erase(retainAlerts.begin() + random);//保持アラートから削除
    }
}

void FireBaseBridge::cancelShowAlertHouseAd()
{//新作情報のアラート表示をキャンセルする。ローディング完了する前に、ゲームとか開始されちゃった時。
    cancel=true;
}

void FireBaseBridge::showHouseAlert()
{
    if (alertObjects.size()>0 && cancel==false) {
        //jni
        
        auto obj=alertObjects[0];
        auto msg=obj.message;
        
        JniMethodInfo t;
        if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "showHouseAlert", "(Ljava/lang/String;)V")) {
            // ★パラメータがStringの場合はjstringに変換する
            // java側は「String」にする。UTF8変換
            
            jstring msgArg = t.env->NewStringUTF(msg.c_str());
            
            t.env->CallStaticVoidMethod(t.classID, t.methodID,msgArg);
            t.env->DeleteLocalRef(msgArg);// ★jstringは解放が必要です。
            
            t.env->DeleteLocalRef(t.classID);
        }
    }
}

int FireBaseBridge::getShowCount()
{
    return showCount;
}

#pragma mark- Icon
void FireBaseBridge::loadingHouseIcons()
{//アイコン情報を取得し保持する
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "loadingIconHouseAd", "()V"))
    {
        assert(false);
        return;
    }
    
    // 関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID);
    
    // 解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

Sprite* FireBaseBridge::getIconBanner(cocos2d::Size size, int iconCount)
{
    _bannerSize=size;
    _iconCount=iconCount;
    
    if (iconBanner) {
        deleteIconBanner();
    }
    
    iconBanner=Sprite::create();
    iconBanner->setOpacity(0);
    iconBanner->setCascadeOpacityEnabled(false);
    iconBanner->setTextureRect(cocos2d::Rect(0, 0, size.width,size.height));
    
    //アイコンをセット
    addIconOnBanner();
    
    return iconBanner;
}

void FireBaseBridge::addIconOnBanner()
{
    int count=MIN((int)icons.size(),_iconCount);//表示できる個数(在庫)、指定個数の少ない方
    
    //表示画像をランダムでシャッフル
    std::vector<HouseIcon>retainIcons;
    retainIcons=icons;
    
    icons.clear();
    
    while (icons.size()<count) {
        int size=(int)retainIcons.size();
        
        auto random=RandomHelper::random_int(0, size-1);//抽選
        auto icon=retainIcons[random];
        
        if (icon.isNew==true) {
            //log("Newがあったので、先頭に持ってくる%d",obj.isNew);
            icons.insert(icons.begin(),icon);//先頭に挿入
        }
        else{
            icons.push_back(icon);//末尾に追加
        }
        retainIcons.erase(retainIcons.begin() + random);//保持アラートから削除
    }
    
    //サイズとスペース計算
    float iconSize=MIN(_bannerSize.width/count, _bannerSize.height)*.9;
    float space=(_bannerSize.width-iconSize*count)/(count+1);
    
    for (int i=0; i<count; i++) {
        auto icon=icons[i];
        
        auto menuItem=MenuItemSprite::create(icon.sprite, icon.sprite,[this,icon](Ref*sender){
            NativeBridge::openUrl(icon.url.c_str());
        });
        menuItem->setScale(iconSize/menuItem->getContentSize().width);
        menuItem->setAnchorPoint(Vec2(0, .5));
        menuItem->setPosition(space+(iconSize+space)*i, iconBanner->getContentSize().height/2);
        
        auto menu=Menu::create(menuItem, NULL);
        menu->setPosition(0,0);
        iconBanner->addChild(menu);
    }
}

void FireBaseBridge::deleteIconBanner()
{
    iconBanner=NULL;
}

#pragma mark- Analytics
void FireBaseBridge::analyticsWithName(const char *eventName)
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "analyticsWithName", "(Ljava/lang/String;)V")) {
        // ★パラメータがStringの場合はjstringに変換する
        // java側は「String」にする。UTF8変換
        jstring stringArg1 = t.env->NewStringUTF(eventName);
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
        t.env->DeleteLocalRef(stringArg1);// ★jstringは解放が必要です。
        
        t.env->DeleteLocalRef(t.classID);
    }
}

void FireBaseBridge::analyticsWithParameters(const char *eventName, std::vector<const char *> keys, std::vector<const char *> values)
{
    int len_key=(int)keys.size();
    
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "analyticsWithParameters", "(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V")) {
        // ★パラメータがStringの場合はjstringに変換する
        jstring stringArg1 = t.env->NewStringUTF(eventName);//イベント名
        
        //jstringはプリミティブ型でない？ので面倒すぎる処理をほんとうに嫌々する
        jclass _class=t.env->FindClass("java/lang/String");
        if (_class==NULL) {
            log("そんなのねえよばか");
        }
        
        jobjectArray jkeys;
        jobjectArray jvalues;
        jkeys=t.env->NewObjectArray(len_key,_class,NULL);
        jvalues=t.env->NewObjectArray(len_key,_class,NULL);
        for (int i=0; i<len_key; i++) {
            t.env->SetObjectArrayElement(jkeys,i,t.env->NewStringUTF(keys[i]));
            t.env->SetObjectArrayElement(jvalues,i,t.env->NewStringUTF(values[i]));
        }
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID,stringArg1,jkeys,jvalues);
        
        t.env->DeleteLocalRef(stringArg1);// ★jstringは解放が必要です。
        t.env->DeleteLocalRef(t.classID);
    }
}

void FireBaseBridge::setUserID(const char *userId)
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "setUserID", "(Ljava/lang/String;)V")) {
        // ★パラメータがStringの場合はjstringに変換する
        // java側は「String」にする。UTF8変換
        jstring stringArg1 = t.env->NewStringUTF(userId);
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
        t.env->DeleteLocalRef(stringArg1);// ★jstringは解放が必要です。
        
        t.env->DeleteLocalRef(t.classID);
    }
}

void FireBaseBridge::setUserProperty(const char *propertyName, const char *value)
{    
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "setUserProperty", "(Ljava/lang/String;Ljava/lang/String;)V")) {
        // ★パラメータがStringの場合はjstringに変換する
        // java側は「String」にする。UTF8変換
        jstring stringArg1 = t.env->NewStringUTF(propertyName);
        jstring stringArg2 = t.env->NewStringUTF(value);

        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1, stringArg2);
        t.env->DeleteLocalRef(stringArg1);// ★jstringは解放が必要です。
        t.env->DeleteLocalRef(stringArg2);// ★jstringは解放が必要です。

        t.env->DeleteLocalRef(t.classID);
    }
}

#pragma mark - Remote Config
void FireBaseBridge::initRemoteConfig()
{
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "initRemoteConfig", "(Z)V"))//引数boolean
    {
        assert(false);
        return;
    }
    
    // 関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID,(DebugMode>0));//debugならtrueを
    
    // 解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void FireBaseBridge::fetchRemoteConfig(std::string key,const onFinishedLoadValue& callback)
{
    JniMethodInfo methodInfo;

    if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "fetchRemoteConfig", "(Ljava/lang/String;)V"))//引数jstring
    {
        jstring jKey = methodInfo.env->NewStringUTF(key.c_str());
        valueCallbackMap[key] = callback;
        
        // 関数を呼び出す
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID,jKey);
        
        // 解放
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
        methodInfo.env->DeleteLocalRef(jKey);
    }
}

#pragma mark- 接続状態を確認
bool FireBaseBridge::getIsConnected()
{
    cocos2d::JniMethodInfo t;
    bool isConnected;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "getIsConnected", "()Z"))
    {
        jboolean ready = (jboolean)t.env->CallStaticBooleanMethod(t.classID, t.methodID);
        
        t.env->DeleteLocalRef(t.classID);
        
        isConnected = ready;
    }
    
    log("firebaseの接続状況%d",isConnected);
    return isConnected;
}

void FireBaseBridge::checkConnected()
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "checkConnected", "()V")) {
        // ★パラメータがStringの場合はjstringに変換する
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        
        t.env->DeleteLocalRef(t.classID);
    }
}

#pragma mark - Database
#pragma mark データ取得
void FireBaseBridge::loadingValue(const char *keyName, const onFinishedLoadValue& callback)
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "loadingValue", "(Ljava/lang/String;)V")) {
        // ★パラメータがStringの場合はjstringに変換する
        // java側は「String」にする。UTF8変換
        
        log("読み込みを開始します。");
        valueCallbackMap[keyName] = callback;
        
        jstring stringArg1 = t.env->NewStringUTF(keyName);
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
        t.env->DeleteLocalRef(stringArg1);// ★jstringは解放が必要です。
        
        t.env->DeleteLocalRef(t.classID);
    }
}

#pragma mark データセット
void FireBaseBridge::setValue(std::string childKey, std::map<std::string, int> value, const onFIRFinished &callback)
{
    int random = cocos2d::random(0, RAND_MAX);
    auto randomSt = StringUtils::format("%d",random);
    
    log("ランダムなマップコールバック用のキーは%s",randomSt.c_str());
    JniMethodInfo t;
    if (!JniHelper::getStaticMethodInfo(t, CLASS_NAME, "setValue", "(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V")){
        assert(false);
        return;
    }
    
    finishedCallbackMap[randomSt] = callback;
    
    // ★パラメータがStringの場合はjstringに変換する
    jstring jcallbackKey = t.env->NewStringUTF(randomSt.c_str());//コールバック用のキー
    jstring jchildKey = t.env->NewStringUTF(childKey.c_str());//子階層のキー
    
    //jstringはプリミティブ型でない？ので面倒すぎる処理をほんとうに嫌々する
    jclass _class=t.env->FindClass("java/lang/String");
    if (_class==NULL) {
        log("そんなのねえよばか");
    }
    
    jobjectArray jKeys = t.env->NewObjectArray( value.size(), _class, NULL);
    jobjectArray jValues = t.env->NewObjectArray( value.size(), _class, NULL);
    
    int i = 0;
    for (auto itr = value.begin(); itr != value.end(); ++itr){
        
        t.env->SetObjectArrayElement(jKeys, i, t.env->NewStringUTF(itr->first.c_str()));
        t.env->SetObjectArrayElement(jValues, i, t.env->NewStringUTF(StringUtils::format("%d",itr->second).c_str()));
        i++;
    }
    
    t.env->CallStaticVoidMethod(t.classID, t.methodID, jchildKey, jKeys, jValues, jcallbackKey);
    t.env->DeleteLocalRef(jcallbackKey);// ★jstringは解放が必要です。
    t.env->DeleteLocalRef(jchildKey);// ★jstringは解放が必要です。
    t.env->DeleteLocalRef(t.classID);
}

void FireBaseBridge::transactionPost(std::string childKey, std::vector<std::string> incrementKeys, std::vector<std::string> decrementKeys, const onFIRFinished &callback)
{
    std::map<std::string, int>transactionMap;
    
    for (auto incrementKey : incrementKeys) {
        transactionMap[incrementKey] = 1;
    }
    
    for (auto decrementKey : decrementKeys) {
        transactionMap[decrementKey] = -1;
    }
    
    transactionPost(childKey, transactionMap, callback);
}

void FireBaseBridge::transactionPost(std::string childKey, std::map<std::string, int> transactionMap, const onFIRFinished &callback)
{
    int random = cocos2d::random(0, RAND_MAX);
    auto randomSt = StringUtils::format("%d",random);
    
    log("ランダムなマップコールバック用のキーは%s",randomSt.c_str());
    JniMethodInfo t;
    if (!JniHelper::getStaticMethodInfo(t, CLASS_NAME, "transactionPost", "(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V")){
        assert(false);
        return;
    }
    
    finishedCallbackMap[randomSt] = callback;
    
    // ★パラメータがStringの場合はjstringに変換する
    jstring jcallbackKey = t.env->NewStringUTF(randomSt.c_str());//コールバック用のキー
    jstring jchildKey = t.env->NewStringUTF(childKey.c_str());//子階層のキー
    
    //jstringはプリミティブ型でない？ので面倒すぎる処理をほんとうに嫌々する
    jclass _class=t.env->FindClass("java/lang/String");
    if (_class==NULL) {
        log("そんなのねえよばか");
    }
    
    jobjectArray jincreKeys = t.env->NewObjectArray( transactionMap.size(), _class, NULL);
    jobjectArray jincreValues = t.env->NewObjectArray( transactionMap.size(), _class, NULL);

    int i = 0;
    for (auto itr = transactionMap.begin(); itr != transactionMap.end(); ++itr){
        
        t.env->SetObjectArrayElement(jincreKeys, i, t.env->NewStringUTF(itr->first.c_str()));
        t.env->SetObjectArrayElement(jincreValues, i, t.env->NewStringUTF(StringUtils::format("%d",itr->second).c_str()));
        i++;
    }

    t.env->CallStaticVoidMethod(t.classID, t.methodID, jchildKey, jincreKeys, jincreValues, jcallbackKey);
    t.env->DeleteLocalRef(jcallbackKey);// ★jstringは解放が必要です。
    t.env->DeleteLocalRef(jchildKey);// ★jstringは解放が必要です。
    t.env->DeleteLocalRef(t.classID);
}

#pragma mark - ストレージ
void FireBaseBridge::getURL(const char *downloadFile, const onGetUrl&callback)
{
    log("URLを取得します。");
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "getURL", "(Ljava/lang/String;)V")) {
        // ★パラメータがStringの場合はjstringに変換する
        // java側は「String」にする。UTF8変換
        urlCallbackMap[downloadFile] = callback;
        
        jstring stringArg1 = t.env->NewStringUTF(downloadFile);
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
        t.env->DeleteLocalRef(stringArg1);// ★jstringは解放が必要です。
        
        t.env->DeleteLocalRef(t.classID);
    }
}

void FireBaseBridge::downloadFile(const char *downloadFile, const onProgressLoadFile &progressCallback, const onFinishLoadFile &callback)
{
    auto key = "downloadFile";
    /*
    if (progressCallback == nullptr && callback == nullptr) {
        key = "downloadFile_test";
    }*/
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, key, "(Ljava/lang/String;)V")) {
        // ★パラメータがStringの場合はjstringに変換する
        // java側は「String」にする。UTF8変換
        dlFileCallbackMap[downloadFile] = callback;
        prFileCallbackMap[downloadFile] = progressCallback;

        jstring stringArg1 = t.env->NewStringUTF(downloadFile);
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
        t.env->DeleteLocalRef(stringArg1);// ★jstringは解放が必要です。
        
        t.env->DeleteLocalRef(t.classID);
    }
}

#pragma mark - オブサーバー
void FireBaseBridge::removeAllObserver(const char* keyName)
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "removeAllObserver", "(Ljava/lang/String;)V")) {
        // ★パラメータがStringの場合はjstringに変換する
        // java側は「String」にする。UTF8変換
        
        jstring stringArg1 = t.env->NewStringUTF(keyName);
        
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
        t.env->DeleteLocalRef(stringArg1);// ★jstringは解放が必要です。
        
        t.env->DeleteLocalRef(t.classID);
    }
}

#pragma mark- タイムスタンプ
void FireBaseBridge::loadingTimestamp(bool keepSync)
{
    JniMethodInfo methodInfo;
    if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "loadingTimestamp", "(Z)V"))
    {
        assert(false);
        return;
    }
    
    // 関数を呼び出す
    methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, keepSync);
    
    // 解放
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

time_t FireBaseBridge::getServerTimestamp()
{
    cocos2d::JniMethodInfo t;
    time_t serverTimestamp;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, CLASS_NAME, "getServerTimestamp", "()D"))
    {
        jdouble jserverTimestamp = (jdouble)t.env->CallStaticDoubleMethod(t.classID, t.methodID);
        
        t.env->DeleteLocalRef(t.classID);
        
        serverTimestamp = (time_t)jserverTimestamp;
    }
    
    log("サーバー時刻%ld", serverTimestamp);
    log("サーバーデートは%ld", Common::dateWithTimeStamp(TimeUnit_Sec, serverTimestamp));

    return serverTimestamp;
}

#pragma mark- C to C++
#pragma mark アラート
void FireBaseBridge::addAlertInfos(const char*message,const char*url,bool isNew)
{//isNew判定はjava側でやってる
    HouseAlert obj;
    obj.url=url;
    obj.message=message;
    obj.isNew=isNew;
    
    alertObjects.push_back(obj);
    
    
    if (alertCount==alertObjects.size()) {
        log("Alert格納終了");
        //シャッフル
        shuffleAlerts();
        
        if (_doCallback) {
            //即座に表示する
            showHouseAlert();
        }
    }
}

void FireBaseBridge::clickedHouseAlert()
{
    auto obj=alertObjects[0];
    NativeBridge::openUrl(obj.url.c_str());
}


#pragma mark アイコン
void FireBaseBridge::createSprite(unsigned char* array,int length,const char*url,bool isNew)
{
    auto image=new Image();
    image->initWithImageData(array, length);
    
    auto texture=new Texture2D();
    texture->initWithImage(image);
    
    auto sprite=Sprite::createWithTexture(texture);
    sprite->retain();//保持
    
    HouseIcon icon;
    icon.sprite=sprite;
    icon.url=url;
    icon.isNew=isNew;
    
    icons.push_back(icon);
    
    if (icons.size()==iconCount) {
        log("IconSprite格納終了 %d",icons.size());
        if (iconBanner) {
            //すでにバナーが存在する場合、そこにiconを追加する
            addIconOnBanner();
        }
    }
    
    CC_SAFE_RELEASE(texture);
    CC_SAFE_RELEASE(image);
}


#pragma mark- callback
#ifdef __cplusplus
extern "C"{
#endif
#pragma mark アラート
    JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_FireBaseBridge_gotAlerts(JNIEnv* env , jobject thiz , jobjectArray urls,jobjectArray messages,jbooleanArray isNews){
        log("アラートロード終了");
        jobjectArray _urls=urls;
        jobjectArray _messages=messages;
        jbooleanArray _isNews=isNews;
        
        //要素数を取得
        alertCount = env->GetArrayLength(_urls);
        log("アラート%d個取得しました。",alertCount);
        for (int i=0; i<alertCount; i++) {
            //url
            jstring jstr_u=(jstring)env->GetObjectArrayElement(_urls, i);
            const char * cstring_u = env->GetStringUTFChars(jstr_u, 0);
            
            //message
            jstring jstr_m=(jstring)env->GetObjectArrayElement(_messages, i);
            const char * cstring_m = env->GetStringUTFChars(jstr_m, 0);
            
            //isnew
            jboolean *getbool = env->GetBooleanArrayElements(_isNews, NULL);
            bool isnew=(bool)getbool[i];
            
            if (manager) {
                manager->addAlertInfos(cstring_m,cstring_u,isnew);
            }
        }
    }
    
    JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_FireBaseBridge_clickHouseAlertUrl(JNIEnv* env,jobject thiz,jboolean selected){
        if (selected==true) {
            if (manager) {
                manager->clickedHouseAlert();
            }
        }
    }
    
#pragma mark アイコン
    JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_FireBaseBridge_gotIcons(JNIEnv* env , jobject thiz , jobjectArray urls,jobjectArray bytes,jbooleanArray isNews){
        log("アイコンロード終了");
        
        jobjectArray _urls=urls;
        jobjectArray _bytes=bytes;
        jbooleanArray _isNews=isNews;
        
        //要素数を取得
        iconCount = env->GetArrayLength(_urls);
        
        for (int i=0; i<iconCount; i++) {
            //url
            jstring jstr=(jstring)env->GetObjectArrayElement(_urls, i);
            const char * cstring = env->GetStringUTFChars(jstr, 0);
            
            //isnew
            jboolean *getbool = env->GetBooleanArrayElements(_isNews, NULL);
            bool isnew=(bool)getbool[i];
            
            //画像データ
            jbyteArray bytesArray = (jbyteArray)env->GetObjectArrayElement(_bytes,i);
            int len=env->GetArrayLength(bytesArray);
            
            unsigned char*bytes=new unsigned char[len];
            env->GetByteArrayRegion (bytesArray, 0, len, reinterpret_cast<jbyte*>(bytes));
            
            if (manager) {
                manager->createSprite(bytes,len,cstring,isnew);
            }
            
            env->ReleaseStringUTFChars( jstr, cstring );
            delete[] bytes;
        }
        
        return;
    }
    
    
#pragma mark 接続状態確認
    JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_FireBaseBridge_gotConnected(JNIEnv* env,jobject thiz, jboolean gotConnected){
        bool connectedCondition = gotConnected;
        
        //manager->setIsConnected(connectedCondition);

        log("firebaseの接続状態を確認 %d",connectedCondition);
    }
    
#pragma mark 値取得
    
    JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_FireBaseBridge_gotValue(JNIEnv* env, jobject thiz, jobject obj, jstring key, jboolean successed){
        
        auto value = FirebaseHelper::transformToValue(env, obj);
        
        const char * cstring_key = env->GetStringUTFChars(key, 0);
        
        bool cboolSuccessed = successed;
        
        auto callback = valueCallbackMap[cstring_key];
        
        if (callback) {
            callback(cboolSuccessed, value);
            valueCallbackMap.erase(cstring_key);
        }
        
        env->ReleaseStringUTFChars(key, cstring_key);
    }
    
#pragma mark RemoteConfig Callback
    JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_FireBaseBridge_completeFetch(JNIEnv* env, jobject thiz, jint type, jstring key){
        const char * cstring_key = env->GetStringUTFChars(key, 0);
        int ctype=type;
        
        log("分析 jni %d",ctype);

        auto callback = valueCallbackMap[cstring_key];
        
        if (callback) {
            
            callback(true, Value(ctype));
            valueCallbackMap.erase(cstring_key);
        }
        
        env->ReleaseStringUTFChars(key, cstring_key);
    }
    
#pragma mark 同時更新
    JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_FireBaseBridge_finishedUpdateChildren(JNIEnv* env, jobject thiz, jstring key, jboolean successed){
        const char * cstring_key = env->GetStringUTFChars(key, 0);
        bool cboolSuccessed = successed;
        
        auto callback = finishedCallbackMap[cstring_key];
        
        if (callback) {
            callback(cboolSuccessed);
            
            finishedCallbackMap.erase(cstring_key);
        }
    }
    
#pragma mark トランザクション
    JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_FireBaseBridge_finishedTransaction(JNIEnv* env, jobject thiz, jstring key, jboolean successed){
        const char * cstring_key = env->GetStringUTFChars(key, 0);
        bool cboolSuccessed = successed;
        
        auto callback = finishedCallbackMap[cstring_key];
        
        if (callback) {
            callback(cboolSuccessed);
            
            finishedCallbackMap.erase(cstring_key);
        }
    }
#pragma mark ストレージ取得
    JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_FireBaseBridge_gotURL(JNIEnv* env,jobject thiz,jstring gotUrlString, jstring fileName, jboolean successed){
        
        const char * cstring_key = env->GetStringUTFChars(fileName, 0);
        const char * cstring_url = env->GetStringUTFChars(gotUrlString, 0);
        bool cboolSuccessed = successed;
        
        log("callback::urlを取得 %s %s", cstring_url,cstring_key);
        
        auto callback = urlCallbackMap[cstring_key];
        
        if (callback) {
            callback(cboolSuccessed, cstring_url);
            
            urlCallbackMap.erase(cstring_key);
        }
    }

    JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_FireBaseBridge_gotFile(JNIEnv* env,jobject thiz, jbyteArray bytesArray, jstring fileName, jboolean successed, jint errorcode, jstring errorDescription)
    {
        //画像データ
        ssize_t len = env->GetArrayLength(bytesArray);
        unsigned char*bytes = new unsigned char[len];
        env->GetByteArrayRegion (bytesArray, 0, len, reinterpret_cast<jbyte*>(bytes));
        
        //std::vector<unsigned char> bytesVec;
        
        //コールバックのキー
        const char * cstring_key = env->GetStringUTFChars(fileName, 0);
        bool cboolSuccessed = successed;
        const char * cerrro_description = env->GetStringUTFChars(errorDescription, 0);
        int cerrorcode = errorcode;
        
        /*for文で中身の詰め替えを行う。
        for (int i = 0; i < len; i++) {
            bytesVec.push_back(bytes[i]);
        }*/
        
        auto callback = dlFileCallbackMap[cstring_key];
        
        dlFileCallbackMap.erase(cstring_key);
        prFileCallbackMap.erase(cstring_key);//成功失敗にしろ進捗のコールバックは削除する。
        
        auto loading_thread = std::thread([=](){
            //この作業が時間かかる。
            std::vector<unsigned char> bytesVec(bytes, bytes + len);
            
            if (callback) {
                callback(cboolSuccessed, bytesVec, (STRAGE_ERRORCODE)cerrorcode, cerrro_description);
                //callback(cboolSuccessed, bytes, len);
            }
            
            delete[] bytes;
        });
        loading_thread.detach();
    }
    
    JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_FireBaseBridge_gotProgress(JNIEnv* env,jobject thiz, jstring fileName, jfloat jprogress)
    {//未設定。これは呼ばれていない。
        //コールバックのキー
        const char * cstring_key = env->GetStringUTFChars(fileName, 0);
        auto prCallback = prFileCallbackMap[cstring_key];
        
        //プログレス
        float progress = jprogress;
        
        if (prCallback) {
            prCallback(progress);
        }
    }
    
#ifdef __cplusplus
}
#endif

