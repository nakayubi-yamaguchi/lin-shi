//
//  BannerBridge.m
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/04/22.
//
//
#include "BannerBridge.h"
#import "RootViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <ADFMovieReward/ADFmyMovieNativeAdView.h>
#include "Common.h"

#pragma mark バナーコールバック
@interface BannerCallback : NSObject <GADBannerViewDelegate,GADNativeExpressAdViewDelegate, ADFmyMovieNativeAdViewDelegate,GADVideoControllerDelegate, GADUnifiedNativeAdDelegate, GADUnifiedNativeAdLoaderDelegate>
{
    
    
}

//KEY:広告ID,VALUE:(AdLoader*).ネイティブ広告のGADAdLoader保持用。解放されないように保持しているだけなので、参照する事はないと思う。
@property(nonatomic) NSMutableDictionary *adLoadersDic;

//KEY:広告ID,VALUE:(NSString*)ストーリーボード用ファイルネーム
@property(nonatomic) NSMutableDictionary *nibFileNamesDic;

//KEY:広告ID,VALUE:{KEY:@(kGADAdLoaderAdTypeNativeContentまたはkGADAdLoaderAdTypeNativeAppInstall),VALUE:ネイティブアドバンス広告View}
@property(nonatomic) NSMutableDictionary *nativeAdvancesDic;

@end

@implementation BannerCallback

-(id)init
{
    self = [super init];
    if (self) {
        self.adLoadersDic = @{}.mutableCopy;
        self.nibFileNamesDic = @{}.mutableCopy;
        self.nativeAdvancesDic = @{}.mutableCopy;
    }
    return self;
}

#pragma mark バナーデリゲート
-(void)adViewDidReceiveAd:(UIView *)bannerView
{
    NSLog(@"バナーの読み込みに成功しました。{%f,%f}{%f,%f}", bannerView.frame.size.width, bannerView.frame.size.height,bannerView.center.x,bannerView.center.y);
}

-(void)adView:(UIView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"バナーの読み込みに失敗しました%@",error);
}

#pragma mark アドフリくん動画パネル広告デリゲート
-(void)onNativeMovieAdViewLoadFinish:(NSString *)appID
{
    NSLog(@"ネイティブ動画広告のロードが完了しました。%@",appID);
}

-(void)onNativeMovieAdViewLoadError:(ADFMovieError *)error appID:(NSString *)appID
{
    if (error.errorCode == ADFMovieError_InvalidAppId) {
        NSLog(@"無効なIDです。");
    }
    
    if (error.errorCode == ADFMovieError_ApiRequestFailure) {
        NSLog(@"APIリクエストに失敗です。");
    }
    
    NSLog(@"ネイティブ動画広告のローディングエラー%lu",(unsigned long)error.errorCode);
    NSLog(@"ネイティブ動画広告のロードに失敗しました。%@→%@",error,appID);
}

#pragma mark Admobネイティブアドバンスデリゲート
-(void)adLoader:(nonnull GADAdLoader *)adLoader didFailToReceiveAdWithError:(nonnull GADRequestError *)error {

    NSLog(@"Admobネイティブの読み込みに失敗しました。%@\n%@」", adLoader.adUnitID, error);
}

#pragma mark GADUnifiedNativeAdLoaderDelegate implementation
- (void)adLoader:(GADAdLoader *)adLoader didReceiveUnifiedNativeAd:(GADUnifiedNativeAd *)nativeAd {
    NSLog(@"GADUnifiedNativeAdのAdmobネイティブのインストール読み込み完了%@",adLoader.adUnitID);

    GADUnifiedNativeAdView *nativeAdView = self.nativeAdvancesDic[adLoader.adUnitID];
    nativeAdView.nativeAd = nativeAd;
    
    // Set ourselves as the ad delegate to be notified of native ad events.
    nativeAd.delegate = self;
    NSLog(@"GADUnifiedNativeAdのAdmobネイティブのインストール読み込み完了%@",nativeAd);

    // Populate the native ad view with the native ad assets.
    // The headline and mediaContent are guaranteed to be presezt in every native ad.
    ((UILabel *)nativeAdView.headlineView).text = nativeAd.headline;
    //nativeAdView.mediaView.mediaContent = nativeAd.mediaContent;
    
    if ([adLoader.adUnitID isEqualToString:[NSString stringWithUTF8String:NATIVEADVANCE_SMALL]] && !nativeAd.icon.image) {//アイコン画像がないなら、普通のイメージを入れる
        UIImageView *icon = (UIImageView *)nativeAdView.iconView;
        CGFloat centerX = icon.center.x;

        GADNativeAdImage *loadedImageView = (GADNativeAdImage *)[nativeAd.images firstObject];

        float widthOnHeight = loadedImageView.image.size.width/loadedImageView.image.size.height;
        icon.frame = CGRectMake(icon.frame.origin.x, icon.frame.origin.y, icon.frame.size.height*widthOnHeight, icon.frame.size.height);
        icon.image = loadedImageView.image;
        icon.center = CGPointMake(centerX, icon.center.y);
    }
    else {
        ((UIImageView *)nativeAdView.iconView).image = nativeAd.icon.image;
    }
    ((UILabel *)nativeAdView.bodyView).text = nativeAd.body;
    ((UIImageView *)nativeAdView.imageView).image =
    ((GADNativeAdImage *)[nativeAd.images firstObject]).image;
    [((UIButton *)nativeAdView.callToActionView) setTitle:nativeAd.callToAction
                                                 forState:UIControlStateNormal];
    
    if (nativeAd.store) {
        ((UILabel *)nativeAdView.storeView).text = nativeAd.store;
        nativeAdView.storeView.hidden = NO;
    } else {
        nativeAdView.storeView.hidden = YES;
    }
    
    if (nativeAd.price) {
        ((UILabel *)nativeAdView.priceView).text = nativeAd.price;
        nativeAdView.priceView.hidden = NO;
    } else {
        nativeAdView.priceView.hidden = YES;
    }
    
    nativeAdView.callToActionView.userInteractionEnabled = NO;
    
    // This app uses a fixed width for the GADMediaView and changes its height
    // to match the aspect ratio of the media content it displays.
    /*
    if (nativeAd.mediaContent.aspectRatio > 0) {
        self.heightConstraint =
        [NSLayoutConstraint constraintWithItem:nativeAdView.mediaView
                                     attribute:NSLayoutAttributeHeight
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:nativeAdView.mediaView
                                     attribute:NSLayoutAttributeWidth
                                    multiplier:(1 / nativeAd.mediaContent.aspectRatio)
                                      constant:0];
        self.heightConstraint.active = YES;
    }*/
    
    if (nativeAd.videoController.hasVideoContent) {
        nativeAd.videoController.delegate = self;
    }
    NSLog(@"Admobネイティブのインストール読み込み完了%@",adLoader.adUnitID);
    
}

#pragma mark GADVideoControllerDelegate implementation

- (void)videoControllerDidEndVideoPlayback:(GADVideoController *)videoController {
}

#pragma mark GADUnifiedNativeAdDelegate

- (void)nativeAdDidRecordClick:(GADUnifiedNativeAd *)nativeAd {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)nativeAdDidRecordImpression:(GADUnifiedNativeAd *)nativeAd {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)nativeAdWillPresentScreen:(GADUnifiedNativeAd *)nativeAd {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)nativeAdWillDismissScreen:(GADUnifiedNativeAd *)nativeAd {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)nativeAdDidDismissScreen:(GADUnifiedNativeAd *)nativeAd {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)nativeAdWillLeaveApplication:(GADUnifiedNativeAd *)nativeAd {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

#pragma mark 解放
-(void)dealloc{
    NSLog(@"%s",__func__);
}

@end

#pragma mark- バナーブリッジ
BannerCallback *bannerCallback = [[BannerCallback alloc] init];
NSMutableDictionary *bannersDic = @{}.mutableCopy;
NSMutableDictionary *panelsDic = @{}.mutableCopy;
ADFmyMovieNativeAdView *nativeAdView = [[ADFmyMovieNativeAdView alloc]
                                        initWithAppID:[NSString stringWithUTF8String:MOVIE_Native_APPID]
                                        layoutPattern:ADFMovieNativeAdLayoutPattern_Default];

#pragma mark 動画パネル
void BannerBridge::configureMovieNative()
{
    //アドフリくんの動画ネイティブ広告は枠を一つしか作れないため、引数いらん。
    
    NSLog(@"ネイティブ動画広告のローディングIDは「%@」",[NSString stringWithUTF8String:MOVIE_Native_APPID]);
    if ([ADFmyMovieNativeAdView isSupportedOSVersion]) {
        
        NSLog(@"サポートしている「%@」",[NSString stringWithUTF8String:MOVIE_Native_APPID]);

        [ADFmyMovieNativeAdView configureWithAppID:[NSString stringWithUTF8String:MOVIE_Native_APPID]];
        
    }
    
    NSLog(@"ネイティブ動画広告は「%@」", nativeAdView);
}

void BannerBridge::loadingMovieNativeAd()
{//rootViewControllerがある時じゃないと呼べないから、AppDelegateクラスでは呼ばない。
    static bool subViewed = false;
    
    if (!subViewed) {//一度しか通らない。
        [nativeAdView loadAndNotifyTo:bannerCallback];
        
        //configureはRootViewControllerが作成される前に表示されるから、貼り付けは初回表示時に
        UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        NSLog(@"2回以上呼ばれてはいけない。ネイティブ広告のルート%@",rootViewController);
        
        if (DebugMode) {
            [nativeAdView getAdView].backgroundColor = [UIColor orangeColor];
        }
        
        [rootViewController.view addSubview:[nativeAdView getAdView]];
        
        subViewed = true;
    }
}

void BannerBridge::showMovieNativeAd(cocos2d::Vec2 point, cocos2d::Size nativeSize)
{
    if (nativeSize.width/16*9 != nativeSize.height) {
        NSLog(@"動画ネイティブ広告の比率が16:9ではない。！！！！！");
    }
    
    float density = Common::getDensity();
    
    Vec2 densityPoint = point / density;
    cocos2d::Size densitySize = nativeSize / density;
    [nativeAdView getAdView].frame = CGRectMake(densityPoint.x, densityPoint.y, densitySize.width, densitySize.height);

    [nativeAdView getAdView].alpha = 1;
    
    NSLog(@"2ネイティブ動画広告は「%@」", nativeAdView);
}

void BannerBridge::hideMovieNativeAd()
{
    NSLog(@"ネイティブ動画広告を隠します。");
    [nativeAdView getAdView].alpha = 0;
}

#pragma mark パネル広告
void BannerBridge::createNativeAd(const char* bannerID, cocos2d::Vec2 point, cocos2d::Size nativeSize)
{
}

void BannerBridge::showPanelAd(const char* bannerID)
{
    
}

void BannerBridge::hidePanelAd(const char* bannerID)
{
}

void BannerBridge::hideAllPanelAd()
{//全てのパネルIDを隠す。
}

void BannerBridge::removeAllPanelAd()
{
}

#pragma mark バナー
void BannerBridge::configure()
{//admobを早く読み込むやつ
    [GADMobileAds configureWithApplicationID:[NSString stringWithUTF8String:CONFIGURE_APPID]];
}

float BannerBridge::getBannerWidth()
{
    float width = CGSizeFromGADAdSize(kGADAdSizeSmartBannerPortrait).width;
    log("スマートバナーの幅は%f",width);
    
    return width * Common::getDensity();
}

float BannerBridge::getBannerHeight()
{
    float height = CGSizeFromGADAdSize(kGADAdSizeSmartBannerPortrait).height;
    
    log("バナーの高さは%f",height);
    
    return height * Common::getDensity();
}

float BannerBridge::getRectangleHeight()
{
    float height = CGSizeFromGADAdSize(kGADAdSizeMediumRectangle).height;
    
    log("レクタングルの高さは%f",height);
    
    return height * Common::getDensity();
}

void BannerBridge::createAdxBanner(const char *adUnitID, bool isBottom, int tag)
{
    NSString *bannerID_St = [NSString stringWithUTF8String:adUnitID];
    
    if ([[bannersDic allKeys] containsObject:bannerID_St]) {
        NSLog(@"すでにこのバナー貼っているよ");
        return;
    }
    
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    if (!rootViewController) {
        rootViewController = [UIApplication sharedApplication].windows.firstObject.rootViewController;
    }

    auto size = Director::getInstance()->getRunningScene()->getContentSize();
    log("DPIは%f比較します{%f, %f}{%f, %f}", Common::getDensity(),size.width, size.height, rootViewController.view.frame.size.width,rootViewController.view.frame.size.height);

    //DFPバナー
    DFPBannerView *dfpBannerView = [[DFPBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
    dfpBannerView.adUnitID = bannerID_St;
    dfpBannerView.rootViewController = rootViewController;
    dfpBannerView.delegate = bannerCallback;
    dfpBannerView.tag = tag;
    
    //ポジションの基点をios仕様に変更
    CGPoint point = CGPointZero;
    float bannerHeight = getBannerHeight() / Common::getDensity();
    if (isBottom) {
        point = CGPointMake(rootViewController.view.center.x,
                            rootViewController.view.frame.size.height - bannerHeight / 2);
    } else {
        point = CGPointMake(rootViewController.view.center.x,
                            bannerHeight / 2);
    }
    log("バナーのpos{%f, %f}" , point.x, point.y);
    dfpBannerView.center = point;
    
    //DFPリクエストを使用
    DFPRequest*request=[DFPRequest request];
    [dfpBannerView loadRequest:request];
    request.testDevices = @[@"2e0b6b87ff66f02f2a230a7fcd12fd6c"];
    
    [rootViewController.view addSubview:dfpBannerView];
    
    bannersDic[bannerID_St] = dfpBannerView;
}

void BannerBridge::createBanner(const char *bannerID, Vec2 position, int tag)
{
    NSString *bannerID_St = [NSString stringWithUTF8String:bannerID];
    
    if ([[bannersDic allKeys] containsObject:bannerID_St]) {
        NSLog(@"すでにこのバナー貼っているよ");
        return;
    }
    
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    if (!rootViewController) {
        rootViewController = [UIApplication sharedApplication].windows.firstObject.rootViewController;
    }
    
    auto size = Director::getInstance()->getRunningScene()->getContentSize();
    log("DPIは%f比較します{%f, %f}{%f, %f}", Common::getDensity(),size.width, size.height, rootViewController.view.frame.size.width,rootViewController.view.frame.size.height);
    log("バナーのpos{%f, %f}" ,rootViewController.view.center.x,rootViewController.view.center.y-getBannerHeight()/2);
    
    //DFPバナー
    DFPBannerView *dfpBannerView = [[DFPBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
    dfpBannerView.adUnitID = bannerID_St;
    dfpBannerView.rootViewController = rootViewController;
    dfpBannerView.delegate = bannerCallback;
    dfpBannerView.tag = tag;
    
    //ポジションの基点をios仕様に変更
    dfpBannerView.center = CGPointMake(position.x/Common::getDensity(), (size.height-position.y)/Common::getDensity());
    
    //DFPリクエストを使用
    DFPRequest*request=[DFPRequest request];
    [dfpBannerView loadRequest:request];
    request.testDevices = @[@"2e0b6b87ff66f02f2a230a7fcd12fd6c"];
    
    [rootViewController.view addSubview:dfpBannerView];
    
    bannersDic[bannerID_St] = dfpBannerView;
}

void BannerBridge::createRectangle(const char *bannerID, cocos2d::Vec2 position)
{
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    if (!rootViewController) {
        rootViewController = [UIApplication sharedApplication].windows.firstObject.rootViewController;
    }
    
    NSString *bannerID_st = [NSString stringWithUTF8String:bannerID];

    GADBannerView *admobView;
    if ([[bannersDic allKeys] containsObject:bannerID_st]) {
        admobView = bannersDic[bannerID_st];
        admobView.hidden = NO;
    }
    else {
        NSLog(@"レクタングル広告を新規作成します。");
        //admobバナー
        admobView = [[GADBannerView alloc]initWithAdSize:kGADAdSizeMediumRectangle];
        
        admobView.delegate = bannerCallback;
        admobView.adUnitID = [NSString stringWithUTF8String:bannerID];
        admobView.rootViewController = rootViewController;
        
        [rootViewController.view addSubview:admobView];
        
        //リクエスト
        GADRequest *admobRequest = [GADRequest request];
        
        // テストデバイス登録
        admobRequest.testDevices = @[kGADSimulatorID,@"6eb369eb140922d07a08b1bd8823c2a6"];
        [admobView loadRequest:admobRequest];
        
        bannersDic[bannerID_st] = admobView;
    }
    
    admobView.frame = CGRectMake(0, 0, admobView.frame.size.width, admobView.frame.size.height);
    
    if (DebugMode) {
        admobView.backgroundColor = [UIColor orangeColor];
    }
    
    float density = Common::getDensity();
    admobView.center = CGPointMake(position.x/density, position.y/density);
}

void BannerBridge::hideBanner(const char *bannerID)
{
    NSString *bannerID_st = [NSString stringWithUTF8String:bannerID];
    if ([[bannersDic allKeys] containsObject:bannerID_st]) {
        UIView *banner = bannersDic[bannerID_st];
        banner.hidden = YES;
        
    }
}

void BannerBridge::removeBanner(const char* bannerID)
{
    NSString *bannerID_st = [NSString stringWithUTF8String:bannerID];
    if ([[bannersDic allKeys] containsObject:bannerID_st]) {
        UIView *banner = bannersDic[bannerID_st];
        [banner removeFromSuperview];
        
        [bannersDic removeObjectForKey:bannerID_st];
    }
    else {
        log("指定されたバナーは表示されていません。");
    }
}

void BannerBridge::removeBanner()
{
    for (id key in bannersDic) {
        UIView *object = (UIView *)bannersDic[key];
        [object removeFromSuperview];
    }

    [bannersDic removeAllObjects];
}

#pragma mark Admobネイティブアドバンス広告

void BannerBridge::loadingNativeAdvanceAd(std::map<const char *, const char *> nativeAdvanceIDsMap)
{
    for (auto keyValue : nativeAdvanceIDsMap) {
        
        auto key = keyValue.first;
        auto value = keyValue.second;
        
        NSString *keyString = [NSString stringWithUTF8String:key];
        NSString *valueString = [NSString stringWithUTF8String:value];
        
        //広告IDとnibファイル名のDictionaryを作成。
        bannerCallback.nibFileNamesDic[keyString] = valueString;
        
        UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        if (!rootViewController) {
            rootViewController = [UIApplication sharedApplication].windows.firstObject.rootViewController;
        }
        
        //広告タイプとnib名の対応関係を定義.
        NSString *nibNativeAdFileName = [NSString stringWithFormat:@"%@_NativeAdView", valueString];
        
        NSLog(@"広告IDは%@ニブファイルネーム[%@]", keyString, nibNativeAdFileName);
      
        //広告の読み込み開始
        GADAdLoader* adLoader = [[GADAdLoader alloc] initWithAdUnitID:keyString
                                                   rootViewController:rootViewController
                                                              adTypes:@[kGADAdLoaderAdTypeUnifiedNative]
                                                              options:@[/*multipleAdsOptions*/]];
        adLoader.delegate = bannerCallback;
        GADRequest *request = [GADRequest request];
        
        Common::performProcessForDebug([request](){
            request.testDevices = @[ @"605f964af34cc71b76298c131f613593" ];
        }, nullptr);
        
        [adLoader loadRequest:request];
        
        //AdViewを事前に生成。Dictionaryに保持する。
        UIView *nativeAdView = [[[NSBundle mainBundle] loadNibNamed:nibNativeAdFileName
                                                                   owner:nil
                                                                 options:nil] firstObject];
        nativeAdView.backgroundColor=[UIColor whiteColor];

        Common::performProcessForDebug([nativeAdView](){
            //nativeAdView.backgroundColor=[UIColor orangeColor];
        }, nullptr);
        nativeAdView.hidden = YES;
        [rootViewController.view addSubview:nativeAdView];

        bannerCallback.nativeAdvancesDic[keyString] = nativeAdView;
        
        //AdLoaderが解放されないようにDictionaryに保持。
        bannerCallback.adLoadersDic[keyString] = adLoader;
        
        NSLog(@"ネイティブアドバンス広告を表示します。%@:::%@::%@",rootViewController,adLoader, bannerCallback);
        
       
    }
}


void BannerBridge::showNativeAdvanceAd(const char *adUnitID, cocos2d::Vec2 point, cocos2d::Size nativeSize)
{
    log("Admobネイティブ広告を表示します。%s",adUnitID);
    float density = Common::getDensity();
    
    Vec2 densityPoint = point / density;
    cocos2d::Size densitySize = nativeSize / density;

    NSString *keyString = [NSString stringWithUTF8String:adUnitID];
    UIView *advanceAdView = bannerCallback.nativeAdvancesDic[keyString];
    advanceAdView.frame = CGRectMake(densityPoint.x, densityPoint.y, densitySize.width, densitySize.height);
    advanceAdView.hidden = NO;
    
    NSLog(@"ネイティブ高校%@",NSStringFromCGRect(advanceAdView.frame));
}

void BannerBridge::hideNativeAdvanceAd(const char *adUnitID)
{
    NSString *keyString = [NSString stringWithUTF8String:adUnitID];
    UIView *advanceAdView = bannerCallback.nativeAdvancesDic[keyString];
    advanceAdView.hidden = YES;
}

void BannerBridge::hideAllNativeAdvanceAd()
{
    for (NSString *unitId : [bannerCallback.nativeAdvancesDic allKeys]) {
        hideNativeAdvanceAd(unitId.UTF8String);
    }
}

