//
//  InterstitialManager.h
//
//
//  Created by yamaguchinarita on 2017/07/07.
//
//

#ifndef __Regitaro2__InterstitialManager__
#define __Regitaro2__InterstitialManager__

#include "cocos2d.h"


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "Utils/AdmobRewardBridge.h"

#else
#include "AdmobRewardBridge.h"

#endif

USING_NS_CC;

class InterstitialManagerDelegate
{
public:
    virtual void didCloseInterstitial(bool complete) = 0;
};

class InterstitialManager{
public:
    static InterstitialManager* manager;
    
    //:デリゲートを受け取りたいクラスが解放される際にデリゲートにNullを代入すること
    //CC_SYNTHESIZE(InterstitialManager*, delegate, Delegate);
    
    //メソッド
    static InterstitialManager* sharedManager();
    
private:
    

};

#endif /* defined(__Regitaro2__RewardMovieManager__) */
