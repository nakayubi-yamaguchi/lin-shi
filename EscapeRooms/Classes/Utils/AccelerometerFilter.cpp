
#include "AccelerometerFilter.h"

// Implementation of the basic filter. All it does is mirror input to output.

#define kAccelerometerMinStep                0.02
#define kAccelerometerNoiseAttenuation        3.0

void AccelerometerFilter::addAcceleration(Acceleration* accel)
{
    x = accel->x;
    y = accel->y;
    z = accel->z;
}

double AccelerometerFilter::norm(double x, double y, double z)
{
	return sqrt(x * x + y * y + z * z);
}

double AccelerometerFilter::clamp(double v, double min, double max)
{
	if(v > max)
		return max;
	else if(v < min)
		return min;
	else
		return v;
}

LowpassFilter* LowpassFilter::create(double rate, double cutoffFrequency)
{
    auto node=new LowpassFilter();
    if (node&&node->initWithSampleRate(rate, cutoffFrequency)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool LowpassFilter::initWithSampleRate(double rate, double cutoffFrequency)
{
    if (!AccelerometerFilter::init()) {
        return false;
    }
    
    double dt = 1.0 / rate;
    double RC = 1.0 / cutoffFrequency;
    filterConstant = dt / (dt + RC);
    
    return true;
    
}

void LowpassFilter::addAcceleration(Acceleration* accel)
{
    double alpha = filterConstant;
    
    if(adaptive)
    {
        double d = clamp(fabs(norm(x, y, z) - norm(accel->x, accel->y, accel->z)) / kAccelerometerMinStep - 1.0, 0.0, 1.0);
        alpha = (1.0 - d) * filterConstant / kAccelerometerNoiseAttenuation + d * filterConstant;
    }
    
    x = accel->x * alpha + x * (1.0 - alpha);
    y = accel->y * alpha + y * (1.0 - alpha);
    z = accel->z * alpha + z * (1.0 - alpha);
}

// See http://en.wikipedia.org/wiki/Low-pass_filter for details low pass filtering
HighpassFilter* HighpassFilter::create(double rate, double cutoffFrequency)
{
    auto node=new HighpassFilter();
    if (node&&node->initWithSampleRate(rate, cutoffFrequency)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool HighpassFilter::initWithSampleRate(double rate, double cutoffFrequency)
{
    if (!AccelerometerFilter::init()) {
        return false;
    }
    
    double dt = 1.0 / rate;
    double RC = 1.0 / cutoffFrequency;
    filterConstant = RC / (dt + RC);
    
    return true;
}

void HighpassFilter::addAcceleration(Acceleration* accel)
{
    double alpha = filterConstant;
    
    if(adaptive)
    {
        double d = clamp(fabs(norm(x, y, z) - norm(accel->x, accel->y, accel->z)) / kAccelerometerMinStep - 1.0, 0.0, 1.0);
        alpha = d * filterConstant / kAccelerometerNoiseAttenuation + (1.0 - d) * filterConstant;
    }
    
    x = alpha * (x + accel->x - lastX);
    y = alpha * (y + accel->y - lastY);
    z = alpha * (z + accel->z - lastZ);
    
    lastX = accel->x;
    lastY = accel->y;
    lastZ = accel->z;
}



