//
//  StoreView.h
//  Otsuri
//
//  Created by 成田凌平 on 2015/10/15.
//  Copyright 2015年 yamaguchinarita. All rights reserved.
//
#include "ItemConstants.h"

typedef enum
{
    ErrorType_None = 0,
    ErrorType_ConnectionFailed,
    ErrorType_InvalidItemID,
    ErrorType_NoPurchaseHistory,
    ErrorType_Canceled,
    ErrorType_Limited,
    ErrorType_Android,
    ErrorType_Android_IsPurchaseHistory,//androidで購入履歴があるのに、再度購入しようとした場合。非消費型
    ErrorType_FailedVerify,//定期購読のレシート検証失敗

}ErrorType;

typedef enum//月額課金が追加されることを考えて、typedefにしておく。
{
    PurchaseType_Consumable = 0,
    PurchaseType_NonConsumable,
    PurchaseType_Subscription
}PurchaseType;

#include "cocos2d.h"

#define ExpiredDateKey "expiredDate"
USING_NS_CC;

using onIsUntreatedTransactionCallback = std::function<void(std::vector<std::string> purchasedItemIdVector, std::vector<std::string> restoredItemIdVector)>;

namespace ItemStore_plugin{
    class ItemStoreDelegate
    {

    public:
        virtual void didFinishPayment(std::string itemID, bool isRestore)=0;
        virtual void happenedError(std::string itemID, bool restore, ErrorType happenedError)=0;
        ~ItemStoreDelegate(){log("アイテムストアデリゲートを解放します");};
    };
    
    class ItemStore
    {
    public:
        
        /**購入可能なアイテム情報の取得*/
        static void loadingStoreInformation();
        
        /**金額のみ*/
        static std::string getPrice(std::string itemID);

        /**ローカライズ記号付きの金額*/
        static std::string getLocalPrice(std::string itemID);
        static std::string getDescription(std::string itemID);
        
        /**
         プラットフォーム側から期限日取得→チェックする
         */
        static void checkAppStoreExpired(const char* itemId);
        

        /**
         *  @param delegate 購入完了、エラー情報を受け取るクラス
         *  @param itemID 購入対象のアイテムID
         *  @param isRestore リストアかどうか
         
         */
        static void startPurchase(ItemStoreDelegate *delegate ,std::string itemID, bool isRestore, PurchaseType purchaseType);
        
    
        static void showErrorAlert(std::string itemID, bool restore, ErrorType errorType);
        
        
        /**
         引数のアイテムIDが購入済みorリストア済みの未処理トランザクションかどうか

         @param itemID 購入済みかどうか確認するメソッド
         @return 購入済み or リストア済み
         */
        static bool isUntreatedTransaction(std::string itemID);
        
        
        /**
         引数アイテムIDが購入済みorリストア済みの未処理トランザクションなら、完了処理を行う。

         @param itemID 完了済みにするアイテムID
         */
        static void treatTransaction(std::string itemID);

    };
}
