//
//  ContentsAlert.cpp
//  EscapeRooms
//
//  Created by yamaguchinarita on 2017/11/22.
//

#include "ContentsAlert.h"
#include "Common.h"

#define MessageLabelTag 1

using namespace cocos2d;

ContentsAlert* ContentsAlert::create(std::string subContentsSpriteName, std::string title, std::string contentsMessage, std::vector<std::string> buttons, const ccMenuCallback &callback)
{
    auto node =new ContentsAlert();
    if (node && node->init(subContentsSpriteName, title, contentsMessage, buttons, callback,nullptr)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

ContentsAlert* ContentsAlert::create(Sprite* subContentsSprite, std::string title, std::string contentsMessage, std::vector<std::string> buttons, const ccMenuCallback &callback)
{
    return create(subContentsSprite, title, contentsMessage, buttons, callback, nullptr);
}

ContentsAlert* ContentsAlert::create(cocos2d::Sprite *subContentsSprite, std::string title, std::string contentsMessage, std::vector<std::string> buttons, const cocos2d::ccMenuCallback &callback, const cocos2d::ccMenuCallback &callback_pushClose)
{
    return create(subContentsSprite, title, contentsMessage, buttons, false, callback, callback_pushClose);
}

ContentsAlert* ContentsAlert::create(cocos2d::Sprite *subContentsSprite, std::string title, std::string contentsMessage, std::vector<std::string> buttons, bool isVertical, const cocos2d::ccMenuCallback &callback, const cocos2d::ccMenuCallback &callback_pushClose)
{
    return create(subContentsSprite, title, contentsMessage, buttons, isVertical, Size::ZERO, callback, callback_pushClose);
}

ContentsAlert* ContentsAlert::create(cocos2d::Sprite *subContentsSprite, std::string title, std::string contentsMessage, std::vector<std::string> buttons, bool isVertical,Size clipSize, const cocos2d::ccMenuCallback &callback, const cocos2d::ccMenuCallback &callback_pushClose)
{
    auto node =new ContentsAlert();
    if (node && node->init(subContentsSprite, title, contentsMessage, buttons, isVertical,clipSize, callback, callback_pushClose)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

#pragma mark init
bool ContentsAlert::init(std::string subContentsSpriteName, std::string title, std::string contentsMessage, std::vector<std::string> buttons, const cocos2d::ccMenuCallback &callback) {

    return init(subContentsSpriteName, title, contentsMessage, buttons, callback,nullptr);
}

bool ContentsAlert::init(std::string subContentsSpriteName, std::string title, std::string contentsMessage, std::vector<std::string> buttons, const cocos2d::ccMenuCallback &callback, const cocos2d::ccMenuCallback &callback_pushClose) {
   
    Sprite *promoteSprite;
    if (subContentsSpriteName.size() == 0) {//ファイル名指定なし
        promoteSprite = Sprite::create();
    }
    else {
        promoteSprite = Sprite::createWithSpriteFrameName(subContentsSpriteName);
    }
    
    return init(promoteSprite, title, contentsMessage, buttons,false,Size::ZERO, callback,callback_pushClose);
}

bool ContentsAlert::init(cocos2d::Sprite *subContentsSprite, std::string title, std::string contentsMessage, std::vector<std::string> buttons, const cocos2d::ccMenuCallback &callback){
    
    return init(subContentsSprite, title, contentsMessage, buttons,false,Size::ZERO, callback, nullptr);
}

bool ContentsAlert::init(cocos2d::Sprite *subContentsSprite, std::string title, std::string contentsMessage, std::vector<std::string> buttons,bool isVertical,Size clipSize, const cocos2d::ccMenuCallback &callback, const cocos2d::ccMenuCallback &callback_pushClose){
    
    if (!CustomAlert::init(title, "", buttons,isVertical, callback,callback_pushClose)) {
        return false;
    }
    m_constensMessage = contentsMessage;
    
    //タイトルラベルの
    titleLabel->setTextColor(Color4B(Common::getColorFromHex(LightBlackColor)));

    setSubContentsSprite(subContentsSprite,clipSize);
    
    return true;
}

#pragma mark- パブリック
void ContentsAlert::setContentsMessage(std::string message)
{
    m_constensMessage = message;
    auto messageLabel = (Label*)m_contentsNode->getChildByTag(MessageLabelTag);
    messageLabel->setString(message);
    
}

#pragma mark- Override
void ContentsAlert::setAlertFont(std::string fontName)
{
    CustomAlert::setAlertFont(fontName);

    auto messageLabel = (Label*)m_contentsNode->getChildByTag(MessageLabelTag);
    setLabelConfig(fontName, messageLabel);
}

void ContentsAlert::setSubContentsSprite(Sprite *subContentsSprite, Size clipSize)
{
    //m_contentsNodeはコンテナ
    if (m_contentsNode == NULL) {
        m_contentsNode = Sprite::create();
    }
    else {//既にあるなら、子供を削除
        m_contentsNode->removeAllChildrenWithCleanup(true);
    }
    
    m_contentsNode->setTextureRect(Rect(0, 0, mainSprite->getContentSize().width, messageLabel->getContentSize().height));
    m_contentsNode->setCascadeOpacityEnabled(true);
    m_contentsNode->setColor(mainSprite->getColor());
    
    //プロモーションしたい画像
    float width = m_contentsNode->getContentSize().width*.9;
    if (!subContentsSprite) {//ファイルなし
        subContentsSprite = Sprite::create();
        subContentsSprite->setTextureRect(Rect(0, 0, 0, 0));
    }
    else {
        subContentsSprite->setScale(width/subContentsSprite->getContentSize().width);
    }
    
    //
    float contents_height=subContentsSprite->getBoundingBox().size.height;
    //clip指定がある
    if (clipSize.width>0) {
        //clip比率
        auto ratio=Vec2(clipSize.width/subContentsSprite->getContentSize().width,clipSize.height/subContentsSprite->getContentSize().height);
        auto stencil_size=Size(width*ratio.x, subContentsSprite->getBoundingBox().size.height*ratio.y);
        
        auto stencil=Sprite::create();
        stencil->setTextureRect(Rect((subContentsSprite->getBoundingBox().size.width-stencil_size.width)/2, (subContentsSprite->getBoundingBox().size.height-stencil_size.height)/2, stencil_size.width, stencil_size.height));
        
        auto clip=ClippingNode::create(stencil);
        clip->setAlphaThreshold(0);
        clip->setInverted(false);
        clip->setName("clip");
        clip->setCascadeOpacityEnabled(true);
        clip->addChild(subContentsSprite);
        
        m_contentsNode->addChild(clip);
        
        contents_height=stencil_size.height;
    }
    else{
        m_contentsNode->addChild(subContentsSprite);
    }
    
    
    //ラベル
    auto label = Label::createWithTTF(m_constensMessage, messageLabel->getTTFConfig().fontFilePath, messageLabel->getTTFConfig().fontSize);
    label->setTextColor(Color4B(Common::getColorFromHex(LightBlackColor)));
    label->setWidth(mainSprite->getContentSize().width*.9);
    label->setTag(MessageLabelTag);
    m_contentsNode->addChild(label);
    
    
    
    //size 自動計算
    m_contentsNode->setTextureRect(Rect(0, 0, m_contentsNode->getContentSize().width, contents_height + label->getContentSize().height + space));
    
    if (clipSize.width>0) {
        auto clip=m_contentsNode->getChildByName<ClippingNode*>("clip");
        auto stencil=clip->getStencil();
        stencil->setPosition(m_contentsNode->getContentSize().width/2, m_contentsNode->getContentSize().height-contents_height/2);
    }
    
    subContentsSprite->setPosition(m_contentsNode->getContentSize().width/2, m_contentsNode->getContentSize().height-contents_height/2);
    label->setPosition(m_contentsNode->getContentSize().width/2, subContentsSprite->getPositionY()-contents_height/2-label->getContentSize().height/2-space/2);
    
    
    mainSprite->addChild(m_contentsNode);
    
    //なぜかmessageLabel->getContentSize().width以外の幅を設定すると、高さが正常に取れない
    messageLabel->setContentSize(Size(messageLabel->getContentSize().width, m_contentsNode->getContentSize().height));
    arrange();
    m_contentsNode->setPosition(messageLabel->getPosition());
}
