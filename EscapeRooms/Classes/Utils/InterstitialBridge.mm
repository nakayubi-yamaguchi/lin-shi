//
//  InterstitialBridge.m
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/03/27.
//
//

#include "RootViewController.h"
#include "InterstitialBridge.h"
#import <GoogleMobileAds/GoogleMobileAds.h>


@interface InterstitialCallback : NSObject <GADInterstitialDelegate>
{
}
@property std::function<void()> m_delegateCallback;


@end

@implementation InterstitialCallback

#pragma mark-　インターステイシャルデリゲート
-(void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    NSLog(@"[%@]インターステイシャルの読み込み完了しました。",ad.adUnitID);
}

-(void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"[%@]インターステイシャルの読み込みに失敗しました。%@", ad.adUnitID,error);
}

-(void)interstitialDidDismissScreen:(GADInterstitial *)ad
{
    NSLog(@"[%@]インターステイシャルを閉じました。",ad.adUnitID);
    
    if (self.m_delegateCallback) {
        self.m_delegateCallback();
    }
    
    InterstitialBridge::loadingIS([ad.adUnitID UTF8String]);
}

#pragma mark- 解放
-(void)dealloc
{
    NSLog(@"%s",__func__);
}

@end

InterstitialCallback *inter_callback = NULL;
NSMutableDictionary *interstitialDic =@{}.mutableCopy;

void InterstitialBridge::loadingIS(std::string appID)
{
    NSString *unitID = [NSString stringWithUTF8String:appID.c_str()];
    
    DFPInterstitial *interstitial;

    interstitial = [[DFPInterstitial alloc] initWithAdUnitID:unitID];
    
    if (!inter_callback) {
        inter_callback = [[InterstitialCallback alloc] init];
    }
    
    interstitial.delegate = inter_callback;
    
    DFPRequest *request = [DFPRequest request];
    
    /*
    request.testDevices = @[@"2e0b6b87ff66f02f2a230a7fcd12fd6c",
                            @"1e27765de8a389f831320d97ecb8fe10"];*/
    
    [interstitial loadRequest:request];
    
    //辞書に入れておく
    interstitialDic[unitID] = interstitial;
}

bool InterstitialBridge::isReady(std::string appID)
{
    NSString *unitID = [NSString stringWithUTF8String:appID.c_str()];

    if (!interstitialDic[unitID]) {
        return false;
    }
    
    DFPInterstitial *checkIS = interstitialDic[unitID];
    
    return checkIS.isReady;
}

void InterstitialBridge::showIS(std::string appID)
{
    InterstitialBridge::showIS(appID, nullptr);
}

void InterstitialBridge::showIS(std::string appID, const std::function<void ()> &callback)
{
    //デリゲート先でコールバックを呼び出す
    inter_callback.m_delegateCallback = callback;

    //インステ表示
    NSString *unitID = [NSString stringWithUTF8String:appID.c_str()];
    
    DFPInterstitial *showIS = interstitialDic[unitID];
    
    if (!showIS || !showIS.isReady) {
        NSLog(@"インターステイシャルの準備がまだできていないので、表示できません。");
    }
    else{
        UIViewController *rootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
        
        if (!rootVC.presentedViewController) {
            [showIS presentFromRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
        }
        else{
            NSLog(@"何かがプレゼントされているので、インターステイシャルを表示できません。");
        }
    }
}
