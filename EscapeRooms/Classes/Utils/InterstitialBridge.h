//
//  InterstitialBridge.h
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/03/27.
//
//

#ifndef __Regitaro2__InterstitialBridge__
#define __Regitaro2__InterstitialBridge__

#include "cocos2d.h"
#include "AdConstants.h"

USING_NS_CC;

class InterstitialBridge
{
public:
    static void loadingIS(std::string appID);

    static bool isReady(std::string appID);
    static void showIS(std::string appID);
    
    static void showIS(std::string appID, const std::function<void()> &callback);

    
private:
    static std::function<void()> m_callback;


};

#endif /* defined(__Regitaro2__InterstitialBridge__) */
