//
//  PurchaseLayer.h
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/04/16.
//
//

#ifndef __Regitaro2__PurchaseLayer__
#define __Regitaro2__PurchaseLayer__

#include "cocos2d.h"
#include "ItemStore.h"

USING_NS_CC;

using onFinishedPurchase = std::function<void(std::string itemID, bool isRestore, ErrorType happenedError)>;


class PurchaseLayerDelegate
{
public:
    virtual void didFinishPurchase(std::string itemID, bool isRestore)=0;
    virtual void didHappenedError(std::string itemID, bool isRestore, ErrorType happenedError)=0;
};

class PurchaseLayer : public LayerColor,public ItemStore_plugin::ItemStoreDelegate
{
public:
    
    virtual bool init();
    CREATE_FUNC(PurchaseLayer);
    
    void startPurchase(std::string itemID, bool isRestore, PurchaseType purchaseType);
    void startPurchase(std::string itemID, bool isRestore, PurchaseType purchaseType, const onFinishedPurchase& finishedCallback);
    CC_SYNTHESIZE(PurchaseLayerDelegate* , delegate, Delegate);
    
    //購入デリゲート
    virtual void didFinishPayment(std::string itemID, bool isRestore);
    virtual void happenedError(std::string itemID, bool restore, ErrorType happenedError);
    
private:
    onFinishedPurchase m_orgCallback;
    
    void createPurchaseData();
    void createListener();
    void createIndicator();
    void rotateIndicator(float delta);
    
    CC_SYNTHESIZE(Sprite*, indicatorSprite, IndicatorSprite);
    ~PurchaseLayer(){log("購入レイヤーを解放します。");};
};

#endif /* defined(__Regitaro2__PurchaseLayer__) */
