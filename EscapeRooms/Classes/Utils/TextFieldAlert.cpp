//
//  TextFieldAlert.cpp
//  EscapeRooms
//
//  Created by yamaguchinarita on 2018/05/21.
//
//

#include "TextFieldAlert.h"
#include "Common.h"

#include "ValueHelper.h"

TextFieldAlert* TextFieldAlert::create(std::string title, std::string contentsMessage, std::vector<std::string> buttons, std::string placefolder, const ccMenuCallback &callback)
{
    auto node =new TextFieldAlert();
    if (node && node->init(title, contentsMessage, buttons, placefolder, callback)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool TextFieldAlert::init(std::string title, std::string contentsMessage, std::vector<std::string> buttons, std::string placefolder, const ccMenuCallback &callback) {
    
    if (!ContentsAlert::init(NULL, title, contentsMessage, buttons, callback)) {
        return false;
    }
    
    //テキストフィールド付きスプライトをコンテンツスプライトとしてセット
    auto sp = createTextFieldSprite(placefolder);
    setSubContentsSprite(sp,Size::ZERO);
    
    //キーボードの高さを簡単には取得できないので、十分高い位置に設置
    float yRate = (Common::isAndroid())? .69:.65;
    mainSprite->setPosition(mainSprite->getPositionX(),  getContentSize().height*yRate);
    
    return true;
}

Sprite* TextFieldAlert::createTextFieldSprite(std::string placefolder)
{
    auto width = mainSprite->getContentSize().width*.6;
    auto size=Size(width, width/7);

    auto textFieldSp = Sprite::create();
    textFieldSp->setCascadeOpacityEnabled(true);
    textFieldSp->setTextureRect(Rect(0, 0, size.width, size.height));
    auto img= ui::Scale9Sprite::create("image.png");
    
    m_editBox=NCEditBox::create(size, img);
    m_editBox->setCascadeOpacityEnabled(true);
    m_editBox->setInputMode(EditBox::InputMode::PHONE_NUMBER);
    m_editBox->setFontColor(Color4B::BLACK);
    m_editBox->setColor(Color3B::WHITE);
    m_editBox->setReturnType(ui::EditBox::KeyboardReturnType::SEND);
    
    
    m_editBox->setPlaceHolder(placefolder.c_str());
    m_editBox->setCustomDelegate(this);
    m_editBox->setFont(HiraginoMaruFont, size.height*.8);
    m_editBox->openKeyBoard();
    m_editBox->setPosition(textFieldSp->getContentSize()/2);
    
    textFieldSp->addChild(m_editBox);
    
    log("テキストフィールドです%f",textFieldSp->getContentSize().height);

    return textFieldSp;
}

#pragma mark - Public
void TextFieldAlert::setInputMode(EditBox::InputMode inputMode)
{
    m_editBox->setInputMode(inputMode);
}

std::string TextFieldAlert::getText()
{
    if (m_editBox == NULL) {
        log("EditBoxが無いです。");
        return "";
    }
    
    return m_editBox->getText();
}

void TextFieldAlert::shakeAlert()
{
    float swing = getContentSize().width/20;
    
    for (auto button : m_menuItems) {//シェイク中は全てのボタンの機能停止。
        button->setEnabled(false);
    }
    
    auto move1=MoveBy::create(.04, Vec2(swing,0));
    auto ease1=EaseInOut::create(move1, 1);
    auto move2=MoveBy::create(.04, Vec2(-swing*2,0));
    auto ease2=EaseInOut::create(move2, 1);
    auto move3=MoveBy::create(.04, Vec2(swing,0));
    auto ease3=EaseInOut::create(move3, 1);
    
    auto repeat=Repeat::create(Sequence::create(ease1, ease2, ease3, NULL), 2);
    
    auto delay=DelayTime::create(.2);
    
    auto callback = CallFunc::create([this](){
        for (auto button : m_menuItems) {//シェイク後に全てのボタンの機能再開
            button->setEnabled(true);
        }
    });
    
    mainSprite->runAction(Sequence::create(repeat, delay, callback, NULL));
}

void TextFieldAlert::close(const cocos2d::ccMenuCallback &callback)
{
    CustomAlert::closeAnimation(false);

    if (callback) {
        callback(this);
    }
}

#pragma mark - Override
void TextFieldAlert::closeAnimation(bool withButtonCallback)
{//親クラスではcloseAnimationでアラートを閉じるアニメーション後にコールバックを呼んでいる。TextFieldAlertではまずコールバックを呼んで入力文字などの判定はコールバック先に任せる。
    if (withButtonCallback) {
        if (mCallback) {//コールバックがあるなら
            mCallback(this);
        }
    }
}

#pragma mark - NCTextFieldDelegate
void TextFieldAlert::customEditBoxEditingDidBegin()
{
    log("textfieldの編集スタート");
}

void TextFieldAlert::customEditBoxDidReturn()
{
    log("textfieldのReturnが押されました。");
}

void TextFieldAlert::customEditBoxTextChanged(bool textHeightChanged,float textDifference)
{
    log("textfieldのテキストが変更されました。");
}
