//
//  DownloadManager.cpp
//  EscapeRooms-mobile
//
//  Created by yamaguchinarita on 2017/10/23.

#include "DownloadManager.h"
#include "NativeBridge.h"

DownloadManager* DownloadManager::manager =NULL;

DownloadManager* DownloadManager::getInstance()
{
    if (manager==NULL) {
        manager=new DownloadManager();
        manager->init();
    }
    
    return manager;
}

void DownloadManager::init()
{
    _downloadRunning = false;
    
    _downloader = nullptr;
    _task = nullptr;
}

void DownloadManager::startDownload(const std::string &url, onDataTaskSuccessCallback onDataTaskSuccess, onTaskProgressCallback onTaskProgress, onTaskErrorCallback onTaskError)
{
    _downloader = new (std::nothrow) network::Downloader();
    
    _downloader->onTaskProgress = onTaskProgress;
    
    _downloader->onDataTaskSuccess = onDataTaskSuccess;
    
    _downloader->onTaskError = onTaskError;
    
    _task = _downloader->createDownloadDataTask(url);
    
    _downloadRunning = true;
    
    log("download started");
}

void DownloadManager::cancelDownload() {
    
    if (_downloadRunning) {
        log("canceling download...");
        
        std::string path = _task->storagePath;
        delete _downloader;
        _downloader = nullptr;
        _task = nullptr;
        _downloadRunning = false;
        
        log("キャンセルした時のストレージパス[%s]",path.c_str());
        
        /*
        if (FileUtils::getInstance()->removeFile(path + ".tmp"))
            CCLOG("download canceled");*/
    }
}
