//
//  PurchaseLayer.cpp
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/04/16.
//

#include "PurchaseLayer.h"

bool PurchaseLayer::init() {
    if (!LayerColor::initWithColor(Color4B(0, 0, 0, 255/2))) {
        return false;
    }
    log("購入レイヤーの初期化");
    
    
    //初期化
    createListener();
    log("購入レイヤーのリスナー");

    createIndicator();
    log("購入レイヤーのインヂケーター");
    
    schedule(schedule_selector(PurchaseLayer::rotateIndicator), .1);
    log("購入レイヤーのスケジューラー");
    
    return true;
}

#pragma mark- パブリック
void PurchaseLayer::startPurchase(std::string itemID, bool isRestore ,PurchaseType purchaseType)
{
    log("サブスクリプションの購入処理を");
    ItemStore_plugin::ItemStore::startPurchase(this, itemID, isRestore, purchaseType);
}

void PurchaseLayer::startPurchase(std::string itemID, bool isRestore, PurchaseType purchaseType, const onFinishedPurchase &finishedCallback)
{
    m_orgCallback = finishedCallback;
    
    ItemStore_plugin::ItemStore::startPurchase(this, itemID, isRestore, purchaseType);
}

#pragma mark- プライベート
void PurchaseLayer::createListener()
{//タッチ透過させない
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [this](Touch *touch,Event*event)->bool{
        return true;
    };
    
    auto dip = Director::getInstance()->getEventDispatcher();
    dip->addEventListenerWithSceneGraphPriority(listener, this);
}

void PurchaseLayer::createIndicator()
{//インヂケーターを作成
    indicatorSprite = Sprite::createWithSpriteFrameName("loading.png");

    indicatorSprite->setPosition(getContentSize()/2);//ど真ん中に表示
    addChild(indicatorSprite);
}

void PurchaseLayer::rotateIndicator(float delta)
{
    //log("アップデート");
    
    indicatorSprite->setRotation(indicatorSprite->getRotation()+360.0/12.0);
}

#pragma mark- ItemStoreDelegate
void PurchaseLayer::didFinishPayment(std::string itemID, bool isRestore)
{
    log("購入でエラー発生%s,;lllllllllll",itemID.c_str());

    if (m_orgCallback) {
        m_orgCallback(itemID, isRestore, ErrorType_None);
    }
    else if (delegate != NULL) {//
        delegate->didFinishPurchase(itemID, isRestore);
    }    
    removeFromParent();
}

void PurchaseLayer::happenedError(std::string itemID, bool restore, ErrorType happenedError)
{
    log("購入でエラー発生%s,リストア%d,エラー番号%d",itemID.c_str(), restore, happenedError);
    if (m_orgCallback) {
        m_orgCallback(itemID, restore, happenedError);
    }
    else if (delegate != NULL) {//
        delegate->didHappenedError(itemID, restore, happenedError);
    }
    
    removeFromParent();
}


