//
//  AudioManager.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/06/13.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________AudioManager_____
#define _____PROJECTNAMEASIDENTIFIER________AudioManager_____

#include "cocos2d.h"
#include <AudioEngine.h>

using namespace cocos2d::experimental;
USING_NS_CC;
class AudioManager
{
public:
    static AudioManager*manager;
    static AudioManager*getInstance();

#pragma mark BGM
    int bgmID;
    void playBGM(const char *fileName);
    void playBGM(const char *fileName , bool roop);
    void playBGM(const char *fileName , bool roop,float volume);
    void resumeBGM();
    void stopBgm();
#pragma mark SE
    std::vector<int>seIDs;
    /**fileNameをkeyに(bool)1を格納*/
    ValueMap seNameMap;
    int playSE(const char*fileName);
    /**checkSameSoundがtrueのとき同じ効果音が鳴っているときは再生しない*/
    int playSE(const char*fileName,bool checkSameSound);

    void stopAllSE();
#pragma mark All
    void preload(std::string filepath);
    void allFreeze();
private:

};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
