//
//  AdfurikunNativeManager.cpp
//  PublicBath
//
//  Created by yamaguchinarita on 2017/05/16.
//

#include "NativeAdManager.h"
#include "BannerBridge.h"
#include "Common.h"

using namespace cocos2d;

NativeAdManager* NativeAdManager::manager =NULL;

#pragma mark- イニシャライズ
NativeAdManager* NativeAdManager::getInstance()
{
    //If the singleton has no instance yet, create one
    if(NULL == manager)
    {
        //Create an instance to the singleton
        manager = new NativeAdManager();
        manager->initialize();
    }
    
    //Return the singleton object
    return manager;
}

void NativeAdManager::initialize()
{
    log("ネイティブ広告のデリゲートをセットします。%s",MOVIE_Native_APPID);
}

#pragma mark- パブリック
void NativeAdManager::configureNativeAd()
{
    //if (Common::localize("en", "ja") == "en") //海外なら。
    
    //BannerBridge::configureMovieNative();
   
    Adfurikun::ADFMovieNative::initializeWithAppID(MOVIE_Native_APPID, 1 , 0, 0, 0, 0);
    
    
    if (Adfurikun::ADFMovieNative::getDelegate(MOVIE_Native_APPID) == NULL) {
        Adfurikun::ADFMovieNative::setDelegate(MOVIE_Native_APPID, this);
        log("ネイティブ広告のデリゲートをセット");
    }
}

void NativeAdManager::loadingNativeAd()
{
    //if (Common::localize("en", "ja") == "en") //海外なら。
        //BannerBridge::loadingMovieNativeAd();
    log("ネイティブ広告のローディングを開始します。");

    //ローディング
    Adfurikun::ADFMovieNative::load(MOVIE_Native_APPID);
}

void NativeAdManager::showNativeAd(const char* bannerID, cocos2d::Vec2 point, cocos2d::Size nativeSize)
{
    if (DebugMode==2) {
        //return;
    }
    
    if (!Common::isAndroid()) {//iOSならネイティブアドバンス広告を表示する。
        //パネル広告のIDからネイティブアドバンス広告のIDを決定する。IDがからでも大丈夫
        auto nativeUnitID = AdConstants::getNativeAdvanceUnitId(bannerID);
        log("iOSなので、国内外問わずネイティブアドバンス広告を表示します。%s",nativeUnitID.c_str());

        BannerBridge::showNativeAdvanceAd(nativeUnitID.c_str(), point, nativeSize);
    }
    else if (strlen(bannerID) > 0 && Common::localize("", "ja") == "ja") {//国内androidなら
        log("国内androidなので、アドフリくん静止画パネル広告を表示します。");
        BannerBridge::createNativeAd(bannerID, point, nativeSize);
        BannerBridge::showPanelAd(bannerID);
    }
    else {//海外androidなら
        log("海外androidなので、アドフリくん動画パネル広告を表示します。");

        log("ネイティブ広告を表示します。%f,%f,%f,%f",point.x, point.y, nativeSize.width, nativeSize.height);
        //BannerBridge::showMovieNativeAd(point, nativeSize);
        
        float density = Common::getDensity();
        
        if(CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID){
            density=1;
        }
        
        Adfurikun::ADFMovieNative::setFrame(MOVIE_Native_APPID, point.x/density, point.y/density, nativeSize.width/density, nativeSize.height/density);
        
        Adfurikun::ADFMovieNative::showVideo(MOVIE_Native_APPID);
        Adfurikun::ADFMovieNative::playVideo(MOVIE_Native_APPID);
    }
}

void NativeAdManager::hideNativeAd()
{
    //動画も静止画も両方止める。
    BannerBridge::hideAllPanelAd();

    //BannerBridge::hideMovieNativeAd();
    Adfurikun::ADFMovieNative::hideVideo(MOVIE_Native_APPID);
    
    //ネイティブアドバンス広告を非表示にする。
    BannerBridge::hideAllNativeAdvanceAd();
}

#pragma mark- デリゲート
void NativeAdManager::onNativeMovieAdViewLoadFinish(const char *appId)
{
    log("ネイティブ広告のローディングに成功しました。%s",appId);
}

void NativeAdManager::onNativeMovieAdViewLoadError(const char *appId, int errorCode)
{
    log("ネイティブ広告のローディングに失敗しました。%s",appId);
}

void NativeAdManager::onNativeMovieAdViewPlayStart(const char* appId)
{
    log("ネイティブ広告を再生開始しました。%s",appId);
}

void NativeAdManager::onNativeMovieAdViewPlayFinish(const char* appId, bool isVideoAd)

{
    log("ネイティブ広告を再生完了しました。%s",appId);
}

void NativeAdManager::onNativeMovieAdViewPlayFail(const char* appId, int errorCode)
{
    log("ネイティブ広告を再生失敗しました。%s",appId);
}
