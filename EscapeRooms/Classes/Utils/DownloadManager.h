//
//  DownloadManager.hpp
//  EscapeRooms-mobile
//
//  Created by yamaguchinarita on 2017/10/23.
//


#ifndef DownloadManager_h
#define DownloadManager_h


#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
using namespace network;

using onDataTaskSuccessCallback = std::function<void(const DownloadTask& task,
                                             std::vector<unsigned char>& data)>;

using onTaskProgressCallback = std::function<void(const DownloadTask& task,
                                                 int64_t bytesReceived,
                                                 int64_t totalBytesReceived,
                                                 int64_t totalBytesExpected)>;

using onTaskErrorCallback = std::function<void(const DownloadTask& task,
                                               int errorCode,
                                               int errorCodeInternal,
                                               const std::string& errorStr)>;

class DownloadManager
{
public:
    static DownloadManager* manager;
    static DownloadManager* getInstance();
    
    /**
     ダウンロード開始。同時にダウンロードできるのは一つと想定。
     */    
    void startDownload(const std::string &url, onDataTaskSuccessCallback onDataTaskSuccess, onTaskProgressCallback onTaskProgress, onTaskErrorCallback onTaskError);

    void cancelDownload();
    
private:
    void init();
    
    bool _downloadRunning;
    cocos2d::network::Downloader *_downloader;
    std::shared_ptr<const cocos2d::network::DownloadTask> _task;
};



#endif /* DownloadManager_hpp */
