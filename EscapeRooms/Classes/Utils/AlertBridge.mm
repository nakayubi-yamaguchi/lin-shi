//
//  GFScene.m
//  Nekotsumi
//
//  Created by 成田凌平 on 2016/01/18.

#include "AlertBridge.h"

@interface UIAlertViewCallback : NSObject <UIAlertViewDelegate>
{
    void *delegate_id;
}

@end

@implementation UIAlertViewCallback

-(id)initWithDelegate:(void*)delegate_
{
    self=[super init];
    if (self)
    {
        delegate_id=delegate_;
    }
    return self;
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    native_plugin::AlertBridgeDelegate *delegate=(native_plugin::AlertBridgeDelegate*)delegate_id;
    if (delegate!=NULL)
    {
        delegate->alertDismissed((int)alertView.tag, (int)buttonIndex);
    }
}

-(void)dealloc
{
    NSLog(@"%s",__func__);
}

@end

namespace native_plugin {
    
    UIAlertViewCallback * callback = NULL;
    
    void AlertBridge::showAlert(AlertBridgeDelegate*delegate,int tag, const char* title, const char* msg, const char* firstBtn, ...) {
        UIAlertView* alert = [[UIAlertView alloc] init];
        [alert setTitle:[NSString stringWithUTF8String:title]];
        [alert setMessage:[NSString stringWithUTF8String:msg]];
        
        va_list ap;
        va_start(ap, firstBtn);
        for (const char* string=firstBtn; string!=NULL; string=va_arg(ap, const char*)) {
            [alert addButtonWithTitle:[NSString stringWithCString:string encoding:NSUTF8StringEncoding]];
        }
        va_end(ap);
        
        callback=[[UIAlertViewCallback alloc] initWithDelegate:(void*)delegate];
        [alert setDelegate:callback];
        alert.tag = tag;
        [alert show];

        /*
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithUTF8String:title]
                                                                                 message:[NSString stringWithUTF8String:msg]
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        int buttonIndex = 0;
        va_list buttons;
        va_start(buttons, firstBtn);
        for (const char* string=firstBtn; string!=NULL; string=va_arg(ap, const char*)) {
            
            [alertController addAction:[UIAlertAction actionWithTitle:[NSString stringWithUTF8String:string]
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                  
                                                                  if (delegate != NULL) {
                                                                      delegate->alertDismissed(tag, buttonIndex);
                                                                  }
                                                                  
                                                                  buttonIndex++;
                                                                  
                                                              }]];
            [alert addButtonWithTitle:[NSString stringWithCString:string encoding:NSUTF8StringEncoding]];
        }
        va_end(ap);*/
    }
}
