//
//  WhiteDayActionManager.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/03/05.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________WhiteDayActionManager_____
#define _____PROJECTNAMEASIDENTIFIER________WhiteDayActionManager_____

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class WhiteDayActionManager: public CustomActionManager
{
public:
    static WhiteDayActionManager* manager;
    static WhiteDayActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
        
private:
    Sequence* gumAction(Node*parent,Node*gum,bool isLeft);
    void dollAction(Node*parent,int back);
    void candyAction(Node*parent,int back);
    ParticleSystemQuad* createFire(Vec2 pos,float size);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
