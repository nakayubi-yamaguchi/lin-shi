//
//  OrderSprite.h
//  Wedding
//
//  Created by 成田凌平 on 2017/04/27.
//
// 2/20 animate パラメータを追加

#ifndef __Wedding__OrderSprite__
#define __Wedding__OrderSprite__

#include "cocos2d.h"
#include "EscapeStageSprite.h"
USING_NS_CC;

typedef std::function<void(bool success)> onFinished;

typedef enum{
    OrderAnimateType_None=0,
    OrderAnimateType_Move,
    OrderAnimateType_Scale
}OrderAnimateType;

class OrderSprite:public EscapeStageSprite
{
public:
    static OrderSprite*create(ValueMap map,Value answerV,std::string name,std::string temporaryKey,int number,Size parentSize,ValueVector setItemMap);
    bool init(ValueMap map,Value answerV,std::string name,std::string temporaryKey,int number,Size parentSize,ValueVector setItemMap);
    
    CC_SYNTHESIZE(int, number, Number)//何番目のspriteか
    CC_SYNTHESIZE(int, index, Index);//表示中の画像インデックス
    CC_SYNTHESIZE(bool, isSelected, IsSelected);//選択状態の
    CC_SYNTHESIZE(Vec2, defaultPos, DefaultPos);

    CC_SYNTHESIZE(Vec2, distance, Distance);//選択中の変位
    CC_SYNTHESIZE(std::string, playSE, PlaySE);//選択時の効果音
    CC_SYNTHESIZE(std::string, CustomFlagKey, CustumFlagKey);//フラグを読み込むキーを指定。(Optional)
    CC_SYNTHESIZE(OrderAnimateType, animateType, OrderAnimateType);
    
    void up(const onFinished& callback);
    void down();
    void change(int imageIndex,Size parentSize,bool save,bool sound);
    void saveIndex();
    
    void useItem(Value setItemVec,Size parentSize,const onFinished& callback);//アイテムを使用したらtrueを返す
    
    std::string getTemporaryKey(std::string name);
private:

};

#endif /* defined(__Wedding__OrderSprite__) */
