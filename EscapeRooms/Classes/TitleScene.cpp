//
//  TitleScene.cpp
//  Kebab
//
//  Created by yamaguchinarita on 2016/12/21.
//
//

#include "TitleScene.h"
#include "MainSprite.h"
#include "Utils/Common.h"
#include "GameScene.h"
#include "ClearScene.h"
#include "UIParts/MenuItemScale.h"
#include "DataManager.h"
#include "ConfirmAlertLayer.h"
#include "NotificationKeys.h"
#include "Utils/InterstitialBridge.h"
#include "Utils/NativeAdManager.h"
#include "Utils/FireBaseBridge.h"
#include "EscapeDataManager.h"
#include "EscapeLoadingScene.h"
#include "SettingLayer.h"
#include "AnalyticsManager.h"

#include "EscapeCollectionScene.h"

#include "Utils/AudioManager.h"
#include "EscapeStageSprite.h"
#include "SupplementsManager.h"

using namespace cocos2d;

Scene* TitleScene::createScene() {
	auto scene = Scene::create();
	auto layer = TitleScene::create();
	scene->addChild(layer);
	
    return scene;
}

bool TitleScene::init() {
    if (!LayerColor::initWithColor(Color4B::WHITE)) {
        return false;
    }
    
    log("タイトルシーンを生成します。");

    setColor(Color3B::WHITE);
    //初期化
    createMenus();
    
    addNotification();
    
    return true;
}

void TitleScene::onEnterTransitionDidFinish()
{
    recordTitleData();
    
    //特有のse
    auto titleSE = DataManager::sharedManager()->getTitleSE();
    EscapeDataManager::getInstance()->playSound(titleSE.c_str(), false);

    //カスタムアクション呼び出し。
    auto customManager=SupplementsManager::getInstance()->getCustomActionManager();
    customManager->customAction(CustomAction_Title, this);
}

#pragma mark- プライベート
void TitleScene::createMenus()
{
    auto back = EscapeStageSprite::createWithSpriteFileName("room_image.png");
    back->setScale(getContentSize().width/back->getContentSize().width);//幅基準でめいっぱい表示
    back->setPosition(getContentSize()/2);
    back->setOpacity(0);
    addChild(back);
    
    auto title_height=getContentSize().height*.15;
    
    auto title_back=Sprite::create();
    title_back->setTextureRect(Rect(0, 0, getContentSize().width, title_height));
    title_back->setPosition(getContentSize().width/2,getContentSize().height*.8);
    title_back->setColor(Color3B::BLACK);
    title_back->setLocalZOrder(1);
    title_back->setOpacity(0);
    addChild(title_back);
    
    auto titleFileName = EscapeDataManager::getInstance()->getPlayTitleResourceDirWithFile("title.png");
    
    auto title = EscapeStageSprite::createWithSpriteFileName("title.png");
    title->setLocalZOrder(1);
    title->setScale(title_height/title->getContentSize().height);//title_backの高さ基準
    title->setPosition(title_back->getPosition());
    title->setOpacity(0);
    addChild(title);
    
    //
    std::vector<std::string> titleMenuString = {"START"};
    
    auto type = EscapeDataManager::getInstance()->getTypePlayTitleCondition();
    log("ストーリー表示履歴 %d",DataManager::sharedManager()->isShowedOpeningStory());
    auto alreadyPlaying=(DataManager::sharedManager()->isExistItemData() || DataManager::sharedManager()->isExistFlagData());
    if (alreadyPlaying)
    {//データがある場合
        titleMenuString.push_back("CONTINUE");
    }
    
    float space = getContentSize().height*.025;
    auto buttonSize=Size(getContentSize().height*.35, getContentSize().height*.065);
    auto font_size=buttonSize.height*.6;
    Vector<MenuItem*> menuItems;
    Vector<Label*>menuLabels;
    
    //バックボタン
    auto backButtonSize = getContentSize().width*.2;
    auto backSprite = Sprite::createWithSpriteFrameName("collection.png");
    backSprite->setColor(Color3B::BLACK);
    backSprite->setOpacity(150);
    
    auto collectionMenuSprite = Sprite::createWithSpriteFrameName("collectionMenu.png");
    collectionMenuSprite->setPosition(backSprite->getContentSize()/2);
    collectionMenuSprite->setOpacity(0);
    backSprite->addChild(collectionMenuSprite);
    
    auto backMenuItem = MenuItemScale::create(backSprite,backSprite, [this,collectionMenuSprite](Ref *sender){
        auto _sender=(Menu*)sender;
        if (_sender->getOpacity() != 255) return;
        Common::playClick();
        auto message = DataManager::sharedManager()->getSystemMessage("return_to_selection");
        auto alert = ConfirmAlertLayer::create(ConfirmAlertType_Twice, message, [this](Ref* sender){
            auto alert = (ConfirmAlertLayer*)sender;
            auto selectedButton = alert->getSelectedButtonNum();
            
            if (selectedButton == 1) {
                Director::getInstance()->replaceScene(TransitionFade::create(.5,EscapeLoadingScene::createScene(LoadingType_RemoveCathe),Color3B::WHITE));
            }
        });
        alert->showAlert(this);
    });
    backMenuItem->setEnabled(false);
    backMenuItem->setScale(backButtonSize/backMenuItem->getContentSize().width);
    backMenuItem->setPosition(backButtonSize/2, title->getPositionY()-title_height/2-backButtonSize/2);
    auto backMenu=Menu::create(backMenuItem, NULL);
    backMenu->setPosition(Vec2::ZERO);
    backMenu->setOpacity(0);
    addChild(backMenu);
            
    for (int i = 0; i < titleMenuString.size(); i++) {
        auto sprite=Sprite::create();
        sprite->setTextureRect(Rect(0, 0, buttonSize.width, buttonSize.height));
        sprite->setColor(Color3B::BLACK);
        sprite->setOpacity(150);
        
        auto label = Label::createWithTTF(titleMenuString.at(i), HiraginoMaruFont, font_size);
        label->enableBold();
        label->enableItalics();
        label->setTextColor(Color4B::WHITE);
        label->setPosition(sprite->getContentSize()/2);
        label->setOpacity(0);
        sprite->addChild(label);
        
        menuLabels.pushBack(label);
        
        auto menuItem = MenuItemScale::create(sprite,sprite, [this,label,type,alreadyPlaying](Ref *sender){
            
            if (label->getOpacity()!=255||
                isTransition) {
                log("アニメーション中です");
                return;
            }
            Common::playClick();
            
            auto senderItem = (MenuItemLabel*)sender;
            switch (senderItem->getTag()) {
                case 0://ニューゲーム
                {
                    if (type == TitleCondition_TUTORIAL) {
                        DataManager::sharedManager()->resetAllData(true);//データ削除
                        replaceToGameScene();
                    }
                    else if (alreadyPlaying) {//アイテムデータか、フラグデータがあれば、プレイ中
                        auto message = DataManager::sharedManager()->getSystemMessage("alert_delete_data");
                        auto confirmLayer = ConfirmAlertLayer::create(ConfirmAlertType_Twice, message,[this](Ref*sender){
                            auto confirm = (ConfirmAlertLayer*)sender;
                            auto selectedIndex = confirm->getSelectedButtonNum();
                            
                            if (selectedIndex == 0) {//キャンセル
                            }
                            else{//OK
                                DataManager::sharedManager()->resetAllData(true);//データ削除
                                replaceToGameScene();
                            }
                        });
                        confirmLayer->showAlert(this);
                    }
                    else{
                        replaceToGameScene();
                    }
                }
                    break;
                case 1://コンティニュー
                    //コンティニューの時は、セーブデータを読み込みする
                    DataManager::sharedManager()->loadAllData();
                    replaceToGameScene();
                    
                    break;
                default:
                    break;
            }
        });
        
        menuItem->setEnabled(false);
        menuItem->setTag(i);
        menuItems.pushBack(menuItem);
    }
    
    auto titleMenu = Menu::createWithArray(menuItems);
    titleMenu->setPosition(getContentSize().width/2,getContentSize().height*.25);
    titleMenu->setOpacity(0);
    titleMenu->alignItemsVerticallyWithPadding(space);
    addChild(titleMenu);
    
    //animation
    float duration = 1;
    
    Common::performProcessForDebug([&duration](){
        duration = .1;
    }, nullptr);

    
    Vector<FiniteTimeAction*>actions;
    auto fadein_back=TargetedAction::create(back, FadeIn::create(2*duration));
    actions.pushBack(fadein_back);
    auto fadein_title_back=TargetedAction::create(title_back, FadeTo::create(1*duration, 150));
    actions.pushBack(fadein_title_back);
    auto fadein_title=TargetedAction::create(title, FadeIn::create(1*duration));
    actions.pushBack(fadein_title);
    auto fadein_menu=Spawn::create(TargetedAction::create(titleMenu,FadeIn::create(.5)),
                                   TargetedAction::create(backMenu,FadeIn::create(.5)), NULL);
    
    Vector<FiniteTimeAction*>actions_menu;
    for (auto label :menuLabels) {
        actions_menu.pushBack(TargetedAction::create(label, FadeIn::create(.5)));
    }
    actions_menu.pushBack(fadein_menu);
    actions_menu.pushBack(TargetedAction::create(collectionMenuSprite, FadeIn::create(.5)));
    
    actions.pushBack(Spawn::create(actions_menu));
    
    //ボタンをタッチを有効にする
    auto enableCallback = CallFunc::create([backMenuItem,menuItems](){
        backMenuItem->setEnabled(true);
        
        for (auto menuItem : menuItems) {
            menuItem->setEnabled(true);
        }
    });
    actions.pushBack(enableCallback);

    runAction(Sequence::create(actions));
}

void TitleScene::showSettingLayer()
{
    auto layer = SettingLayer::create(SettingType_OnTitleScene);
    layer->showAlert(this);
}

void TitleScene::replaceToGameScene()
{
    isTransition=true;
    DataManager::sharedManager()->setEnFlagTime(time(NULL));
    
    //送信履歴がない&&新規ユーザーのみ送信されます。
    if (EscapeDataManager::getInstance()->getTypePlayTitleCondition() != TitleCondition_TUTORIAL) {
        AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_StartFirstStage);
    }
    
    //flagがセットされてなければセットする
    if (DataManager::sharedManager()->flagIDforHint==0) {
        DataManager::sharedManager()->setFlagIDForHint();
    }
    
    //gamesceneに遷移する
    Common::performProcessForDebug([](){
        Director::getInstance()->replaceScene(TransitionFade::create(.5, GameScene::createScene(),Color3B::BLACK));
        
    }, [](){
        Director::getInstance()->replaceScene(TransitionFade::create(.5, GameScene::createScene(),Color3B::BLACK));
        
    });
}

void TitleScene::recordTitleData()
{//タイトル表示完了時に、プレイカウントとラストプレイバージョンをセーブする
    //プレイカウントを保存(インクリメント)
    auto playCount = DataManager::sharedManager()->getEternalRecordDataOnPlay(RecordKey_PlayCount).asInt();
    playCount += 1;
    
    //プレイバージョンを保存(string)
    auto titleVersionSt = EscapeDataManager::getInstance()->getPlayTitleVersion();
    
    log("タイトルのバージョンは%s",titleVersionSt.c_str());
    
    //保存するMapを作成
    ValueMap savedMap;
    savedMap[RecordKey_PlayCount] = Value(playCount);
    savedMap[RecordKey_LastPlayVersion] = Value(titleVersionSt);
    
    DataManager::sharedManager()->recordEternalDataMapOnPlay(savedMap);
}

#pragma mark- 通知 広告
void TitleScene::addNotification()
{
    auto listener=EventListenerCustom::create(NotificationKeyWillEnterForeGround, [this](Event*event){
        Director::getInstance()->getScheduler()->schedule(schedule_selector(TitleScene::showIS), this, 0, 1, .5, false);
        log("タイトルシーンにおけるバックグラウンドでの通知");

    });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener,this);
}

void TitleScene::showIS(float delay)
{
    InterstitialBridge::showIS(INTERSTITIAL_APPID);
}

#pragma mark- 解放
TitleScene::~TitleScene()
{
    log("タイトルシーンを解放します。");
}


