//
//  PopUpSprite.h
//  Kebab
//
//  Created by 成田凌平 on 2016/12/19.
//
// アイテムのズームアップ管理

#ifndef __Kebab__PopUpLayer__
#define __Kebab__PopUpLayer__

#include "cocos2d.h"
#include "CountUpLabel.h"

USING_NS_CC;
typedef std::function<void()> popupCloseCallback;
class PopUpLayer:public LayerColor
{
public:
    static PopUpLayer*create(const popupCloseCallback& callback);
    bool init(const popupCloseCallback& callback);
    FiniteTimeAction* fadeOutAll(float duration);

    Sprite*itemImage;//アイテム画像
    Sprite*backImage;//白背景部分
    ClippingNode*clip;//アニメーション描画に使う clipに描画されたものはreload時に全て消去される
    
private:
    popupCloseCallback m_callback;
    ValueMap itemMap;//アイテムデータ
    //ValueMap touchMap;
    Menu*closeMenu;
    
    void loadData();
    void addSupplements();
    
    void addEventListner();
    void changeAngle();
    void showItemImage();
    
    void manageCustomAction(std::string key,ccMenuCallback callback);//カスタムアクションがあれば実行する。終了後コールバック
    /**touchMapをもとに処理を開始*/
    void manageTouchMap();
    
    void showAnimateLayer(Value animation,ccMenuCallback callback);
    void manageMysteryAction(cocos2d::Value mystery, ccMenuCallback callback);
    void manageDataByTouch(ValueMap map);//タッチによるフラグ、アイテム周りのデータ処理
    //void dragAction(bool isInBegan,Vec2 pos);

    CC_SYNTHESIZE(Rect, touchThroughRect, TouchThroughRect);
    CC_SYNTHESIZE(Rect, mainRect, MainRect);
    CC_SYNTHESIZE(bool, enable,Enable);//タッチ可能か
    
    /**表示するitemImageの名前を返す*/
    std::string getShowingFileName();
    
    ~PopUpLayer();
};

#endif /* defined(__Kebab__PopUpSprite__) */
