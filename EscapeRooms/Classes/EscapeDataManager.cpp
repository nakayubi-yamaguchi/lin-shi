//
//  EscapeDataManager.cpp
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/09/28.
//
//

#include "EscapeDataManager.h"
#include "Utils/Common.h"
#include "Utils/FireBaseBridge.h"
#include "AnalyticsManager.h"
#include "Utils/Common.h"
#include "SettingDataManager.h"
#include "Utils/HttpAccessManager.h"
#include "Utils/AudioManager.h"

//未使用なら消していい
#include "Utils/ValueHelper.h"
#include "Utils/NativeBridge.h"
#include "Utils/DownloadManager.h"

#define MapKey_NeedAppVersion "NeedAppVersion"
#define MapKey_AppInfo "AppInfo"
#define MapKey_VersionCode "VersionCode"
#define MapKey_TitleInfo "TitleInfo"
#define MapKey_VoteInfo "VoteInfo"

//タイトル情報内のキー。プライベートでのみ使用
#define MapKey_Atlas "atlas"
#define MapKey_Atlas_png "atlas_p"
#define MapKey_Atlas_jpg "atlas_j"
#define MapKey_Type "type"
#define MapKey_SupportedVersion "playable_version"
#define MapKey_Url "url"
#define MapKey_IsRepairing "isRepairing"//(BOOL)修理中のステージ

//VoteInfo内のキー。全部じゃない
#define MapKey_Candidates "candidates"//(Array)
#define MapKey_ImplementedVersion "implemented_version"//(Dictionary)

//プライベートで使用するディレクトリ名を定義
#define VersionDir "Version/"
#define VersionForReviewDir "VersionForReview/"
#define AchievementDir "Achievement/"

//プライベートで使用する拡張子を定義
#define ZipExt ".zip"
#define PlistExt ".plist"

//プライベートで使用するファイル名を定義
#define FilePath_Version "version.plist"
#define FilePath_Resource "resource"
#define FilePath_Data "Datas"
#define FilePath_Achievement "achievement.plist"

EscapeDataManager* EscapeDataManager::manager =NULL;

EscapeDataManager* EscapeDataManager::getInstance()
{
    if (manager==NULL) {
        manager=new EscapeDataManager();
    }
    return manager;
}

#pragma mark- セッター
void EscapeDataManager::setPlayTitleNum(int num)
{
    m_playTitleNum = num;
}

void EscapeDataManager::setIsPlayPre(bool isPre)
{
    m_isPlayPre = isPre;
}

#pragma mark- ゲッター
bool EscapeDataManager::getIsPlayPre()
{
    return m_isPlayPre;
}

long long EscapeDataManager::getApologyCode()
{
    auto mapValue = getFB_AppDataMap()[MapKey_AppInfo].asValueMap();

    return mapValue.at("ApologyCode").asInt();
}

#pragma mark- マップ管理関連
ValueMap EscapeDataManager::getVersionValueMap()
{
    auto versionDir = StringUtils::format("%s%s", VersionDir, FilePath_Version);

    auto versionPlistDir =  getWritableFileDir(versionDir.c_str());
    
    if (LocalVersionMode) {
        versionPlistDir = StringUtils::format("%s%s", LocalVersionDir, FilePath_Version);
    }
    
    log("version.plistを[%s]から引っ張ってくる",versionPlistDir.c_str());
    
    auto infodata = FileUtils::getInstance()->getValueMapFromFile(versionPlistDir);
    
    return infodata;
}

#pragma mark AppInfo関連
ValueMap EscapeDataManager::getAppDataMap()
{
    //staticつけたらダメです。
    auto infodata = getVersionValueMap();
    auto map = infodata[MapKey_AppInfo];
    
    if (map.isNull()){
        ValueMap nullMap;
        log("AppInfoはありません。");
        return nullMap;
    }
    
    log("version.plistをよもこむぜ");
    ValueHelper::dumpValue(map);
    
    return map.asValueMap();
}

#pragma mark TitleInfo関連
ValueVector EscapeDataManager::getTitleDataVector()
{
    static auto infodata = getVersionValueMap();
    static auto vector = infodata[MapKey_TitleInfo];
    
    if (vector.isNull()){
        ValueVector nullVector;
        log("タイトルデータはありません。");
        return nullVector;
    }

    Common::performProcessForDebug([](){
        //ValueHelper::dumpValue(vector);
    }, nullptr);

    static auto valueVector = vector.asValueVector();
    
    return valueVector;
}

ValueVector EscapeDataManager::getTitleDataSortedVector()
{
    //ソートされたデータがなければソートする。
    if (m_sortedTitleVector.size() == 0) {
        log("ソートしたデータがないので生成しておきます。");
        sortTitleDataVector();
    }
    
    return m_sortedTitleVector;
}

void EscapeDataManager::sortTitleDataVector()
{
    auto key = "タイトルデータをソート";
    Common::startTimeForDebug(key);
    
    //指定並び替え、順序に応じてソートする。
    ValueVector titleVector;
    std::vector<int> orderVector;
    
    switch (SettingDataManager::sharedManager()->getSortType()) {
        case SortType_Date:{
            if (SettingDataManager::sharedManager()->getIsAscend()) {//昇順なら
                //日付順なら、広告、チュートリアル以外を除く順番を保持
                for (int i=0; i<getTitleDataVector().size(); i++) {
                    auto titleType = getTypeTitleCondition(i, false);
                    if (!conditionOfFixedTitle(titleType)) {
                        orderVector.push_back(i);
                    }
                }
                
                //version.plistは日付降順で並んでいるため、昇順設定の場合は中身を反転させる必要あり。
                titleVector = sortedValueVector(orderVector, true);
            }
            else {//日付順で降順なら、version.plistのまま。
                titleVector = getTitleDataVector();
            }
            
            break;
        }
        case SortType_Difficulty:{
            
            auto isAscend = SettingDataManager::sharedManager()->getIsAscend();
            std::map<float, int> difficultyMap_;
            for (int i=0; i<getTitleDataVector().size(); i++) {
                auto titleType = getTypeTitleCondition(i, false);
                
                if (!conditionOfFixedTitle(titleType)) {
                    auto findDifficulty = getSortItemValueWithIndex(i, ClearData, false);
                    while (difficultyMap_.count(findDifficulty) > 0) {//同一のキー上書き防止
                        
                        //値が同じ時に最新順に並べる用の処理。
                        findDifficulty+=(isAscend)? .001: -.001;
                    }
                    log("難易度%f,順番%dをmapに追加します。", findDifficulty, i);
                    difficultyMap_[findDifficulty] = i;
                }
            }
            
            //難易度順の配列を作成
            std::vector<int> orderVector;
            for (auto map: difficultyMap_) {
                //log("難易度のマップです。{%f,,%d}",map.first, map.second);
                orderVector.push_back(map.second);
            }
            
            //std::mapのキーは昇順で並べてくれる、昇順設定の場合は中身反転させる必要なし。降順設定の場合は中身反転させる必要あり。
            titleVector = sortedValueVector(orderVector, !isAscend);
            
            break;
        }
            
        case SortType_Star:{
            
            auto isAscend = SettingDataManager::sharedManager()->getIsAscend();
            std::map<float, int> difficultyMap_;
            for (int i=0; i<getTitleDataVector().size(); i++) {
                auto titleType = getTypeTitleCondition(i, false);
                
                if (!conditionOfFixedTitle(titleType)) {
                    auto findDifficulty = getSortItemValueWithIndex(i, Stars, false);
                    while (difficultyMap_.count(findDifficulty) > 0) {//同一のキー上書き防止
                        
                        //値が同じ時に最新順に並べる用の処理。
                        findDifficulty+=(isAscend)? .001: -.001;
                    }
                    log("難易度%f,順番%dをmapに追加します。", findDifficulty, i);
                    difficultyMap_[findDifficulty] = i;
                }
            }
            
            //難易度順の配列を作成
            std::vector<int> orderVector;
            for (auto map: difficultyMap_) {
                //log("難易度のマップです。{%f,,%d}",map.first, map.second);
                orderVector.push_back(map.second);
            }
            
            //std::mapのキーは昇順で並べてくれる、昇順設定の場合は中身反転させる必要なし。降順設定の場合は中身反転させる必要あり。
            titleVector = sortedValueVector(orderVector, !isAscend);
            
            break;
        }
        default:
            break;
    }
    
    //対応外の国では配信しない。
    auto itr = titleVector.begin();
    int i = 0;
    while (itr != titleVector.end())
    {        
        auto val = *itr;
        auto map = val.asValueMap();
        auto language = Common::localize("en", "jp", "ch", "tw", "ko");
        auto appnameMap = map[MapKey_TitleName].asValueMap();
        auto localizedAppName = appnameMap[language].asString();
        
        //タイトルのローカライズ名の記載がなかったら、その地域では配信しない。
        auto localizeName = getTitleLocalNameWithIndex(i, false);//ソート前のオリジナルデータから名前を取得
        
        
        if(localizedAppName == ""){
            log("%d番目の要素を削除します",i);
            
            itr = titleVector.erase(itr);
        }
        else{
            itr++;
        }
        i++;
    }
    
    //log("バージョンplistの中身は%d", titleVector.size());
    
    m_sortedTitleVector = titleVector;
    
    Common::stopTimeForDebug(key);
}

ValueMap EscapeDataManager::getTitleMapWithIndex(int index)
{
    return getTitleMapWithIndex(index, true);
}

ValueMap EscapeDataManager::getTitleMapWithIndex(int index, bool isSorted)
{
    return (isSorted)? getTitleDataSortedVector().at(index).asValueMap(): getTitleDataVector().at(index).asValueMap();
}

std::string EscapeDataManager::getTitleNameWithIndex(int index)
{
    return getTitleNameWithIndex(index, true);
}

std::string EscapeDataManager::getTitleNameWithIndex(int index, bool isSorted)
{
    return getTitleMapWithIndex(index, isSorted)[MapKey_FileName].asString();
}

std::string EscapeDataManager::getPlayTitleName()
{
    if (LocalMode) {
        return LocalTestTitle;
    }
    return getTitleNameWithIndex(m_playTitleNum);
}

std::string EscapeDataManager::getTitleLocalNameWithIndex(int index)
{
    return getTitleLocalNameWithIndex(index, true);
}

std::string EscapeDataManager::getTitleLocalNameWithIndex(int index, bool isSorted)
{
    auto language = Common::localize("en", "jp", "ch", "tw", "ko");
    auto appnameMap = getTitleMapWithIndex(index, isSorted)[MapKey_TitleName].asValueMap();
    auto localizedAppName = appnameMap[language].asString();
    
    return localizedAppName;
}

std::string EscapeDataManager::getPlayTitleVersion()
{    
    return getTitleMapWithIndex(m_playTitleNum)[MapKey_TitleVersion].asString();
}

bool EscapeDataManager::isExistTitleResource(int num)
{
    auto titleResourceDir = getTitleResourceDir(num);
    
    bool isExit = FileUtils::getInstance()->isDirectoryExist(titleResourceDir);
    
    log("リソースディレクトリ[%s]--存在しているかどうか%d", titleResourceDir.c_str(), isExit);

    return isExit;
}

bool EscapeDataManager::isExistPlayTitleResource()
{
    return isExistTitleResource(m_playTitleNum);
}

TitleCondition EscapeDataManager::getTypeTitleCondition(int index)
{
    return getTypeTitleCondition(index, true);
}

TitleCondition EscapeDataManager::getTypeTitleCondition(int index, bool isSorted)
{
    //plistの時間を読み込み
    auto titleMap = getTitleMapWithIndex(index, isSorted);
    
    auto typeValue = titleMap[MapKey_Type];
    
    auto condition = TitleCondition_None;
    
    std::string typeSt;
    if (!typeValue.isNull()) {
        typeSt = typeValue.asString();
        if (typeSt == "ad") {
            condition = TitleCondition_AD;
        }
        else if (typeSt == "remake") {
            condition = TitleCondition_REMAKE;
        }
        else if (typeSt == "tutorial") {
            condition = TitleCondition_TUTORIAL;
        }
        else if(typeSt == "collabo") {
            condition = TitleCondition_COLLABORATION;
        }
        else if(typeSt == "vote") {
            condition = TitleCondition_VOTE;

        }
    }
    
    //log("インデックス%d：：タイトル%s", index, typeSt.c_str());
    
    return condition;
}

TitleCondition EscapeDataManager::getTypePlayTitleCondition()
{
    return getTypeTitleCondition(m_playTitleNum);
}

TitleCondition EscapeDataManager::getReleaseTitleCondition(int index)
{
    TitleCondition condition = getTypeTitleCondition(index);
    if (conditionOfFixedTitle(condition)) {
        return condition;
    }
    
    //plistの時間を読み込み
    auto titleMap = getTitleMapWithIndex(index);
    
    //起動完了後にversion.plistをいじることは無いからstatic保持。
    static auto appInfoMap = getAppDataMap();
    static auto versionCode = appInfoMap[MapKey_VersionCode].asInt();
    
    int preTime = getIntDateFromVersionPlist(index, MapKey_VideoDate, 0);
    int releaseTime = getIntDateFromVersionPlist(index, MapKey_ReleaseDate, 0);
    int passReleaseTime = getIntDateFromVersionPlist(index, MapKey_ReleaseDate, Period_New);//新作公開日から5日で新作のバンドは消える。
    
    //デバイスの時間
    time_t serverTime = FireBaseBridge::getInstance()->getServerTimestamp();
        
    int deviceTime = (int)Common::dateWithTimeStamp(TimeUnit_Day, serverTime);//Common::dateWithTimeStamp(serverTime);
    
    log("タイトル名は%sデバイスの時間は%d,動画公開の時間は%d,リリースの時間は%d,新作終わりの時間は%d", getTitleNameWithIndex(index).c_str(), deviceTime, preTime, releaseTime, passReleaseTime);
    
    if (deviceTime < preTime) {
        return TitleCondition_Waiting;
    }
    else if (preTime <= deviceTime && deviceTime <releaseTime) {
        return TitleCondition_Pre;
    }
    else if (releaseTime <= deviceTime && deviceTime < passReleaseTime) {
        return TitleCondition_New;
    }
    
    if (versionCode >= 14) {//バージョン14から追加
        
        int lockedTime = getIntDateFromVersionPlist(index, MapKey_LockedDate, 0);
        
        //log("タイトル名は%sデバイスの時間は%d,動画公開の時間は%d,リリースの時間は%d,新作終わりの時間は%d,ロックする日にちは%d", getTitleNameWithIndex(index).c_str(), deviceTime, preTime, releaseTime, passReleaseTime, lockedTime);

        if (lockedTime <= deviceTime) {
            return TitleCondition_LOCKED;
        }
        
    }
    
    return TitleCondition_None;
}

bool EscapeDataManager::canPlayTitleOnAppVersion(int index)
{
    auto titleMap = getTitleMapWithIndex(index);
    
    if (titleMap[MapKey_SupportedVersion].isNull()) {
        return false;
    }
    auto supportedVersion = titleMap[MapKey_SupportedVersion].asValueMap();
    
    //タイトルが対応しているバージョン
    auto supportedOSVersion = supportedVersion[SystemOS].asString();
    
    return isSupportedVersion(Application::getInstance()->getVersion(), supportedOSVersion) /*supportedOS <= appVersion*/;
}

std::string EscapeDataManager::getAdUrl(int index)
{
    if (getTypeTitleCondition(index) != TitleCondition_AD) {
        log("広告ではないので、URLはありません。");
        return nullptr;
    }
    
    auto titleMap = getTitleMapWithIndex(index);
    auto urlValue = titleMap[MapKey_Url];
    
    if (urlValue.isNull()) {
        log("広告であるが、URLキーがありません。");
        return nullptr;
    }

    auto urlSt = urlValue.asValueMap()[SystemOS].asString();
    
    return urlSt;
}

int EscapeDataManager::getIntDateFromVersionPlist(int index, const char *conditionMapKey, int intervalDay)
{
    auto titleMap = getTitleMapWithIndex(index);
    
    auto dateMapValue = titleMap[conditionMapKey];
    auto dateMap = dateMapValue.asValueMap();
    
    int year = atoi(dateMap[MapKey_Year].asString().c_str());
    int month = atoi(dateMap[MapKey_Month].asString().c_str());
    int day = atoi(dateMap[MapKey_Day].asString().c_str());
    
    if (!strcmp(conditionMapKey, MapKey_LockedDate)) {//プレイできるのはロック日の前の日まで
        intervalDay-=1;
    }
    
    time_t dateDetailToTimestamp = Common::timeStampWithDate(year, month, day);
    auto date = (int)Common::dateWithTimeInterval(TimeUnit_Day, dateDetailToTimestamp, 60*60*24*intervalDay);
  
    return date;
}

std::string EscapeDataManager::getDateString(int index, const char *conditionMapKey)
{
    auto intdate = getIntDateFromVersionPlist(index, conditionMapKey, 0);
    auto dateSt = StringUtils::format("%d",intdate);
    auto month =  dateSt.substr(4,2);
    auto day =  dateSt.substr(6,2);
    
    auto transedDateSt = StringUtils::format("%s/%s", month.c_str(), day.c_str());
    
    return transedDateSt;
}

float EscapeDataManager::getSortItemValueWithIndex(int index, const char *sortItemKey)
{
    return getSortItemValueWithIndex(index, sortItemKey, true);
}

float EscapeDataManager::getSortItemValueWithIndex(int index, const char *sortItemKey, bool isSorted)
{
    //auto sortItemValue = getTitleMapWithIndex(index, isSorted)[sortItemKey];
    
    //Stars or ClearData
    auto sortItemValue = getFB_AppDataMap()[sortItemKey];
    auto sortItemMap = sortItemValue.asValueMap();
    auto titleName = getTitleNameWithIndex(index, isSorted);
    transform(titleName.begin(), titleName.begin()+1, titleName.begin(), ::toupper);
    auto readValue = sortItemMap[titleName];
    if (readValue.isNull()) {//まだレビューが入って
        return 0;
    }
    auto readMap = readValue.asValueMap();

    if (!strcmp(sortItemKey, Stars)) {
        int starPhase = 5;//五段階評価
        int userCount = 0;//評価された数
        int starCount = 0;//獲得星数
        
        for (int i = 0; i < starPhase; i++) {
            auto key = StringUtils::format("%d%s",i+1,Stars);
            auto user = readMap[key.c_str()].asInt();
            userCount += user;
            
            starCount += (i+1)*user;
            //log("ユーザー%d人が星%dつを付けました",user, i+1);
            if (i == starPhase-1) {//ラスト
                if (userCount == 0)
                    userCount = 1;
                float averageRate = (float)starCount/(float)userCount;
                
                log("[%s]の平均は%f点",titleName.c_str(),averageRate);
                
                return averageRate;
            }
        }
    }
    else if (!strcmp(sortItemKey, ClearData)) {
        auto hintCount = readMap["hint"].asFloat();
        auto playersCount = readMap["players"].asFloat();
        auto difficulty = hintCount/playersCount;
        
        log("[%s]の難易度は%f",titleName.c_str(), difficulty);
        
        return difficulty;
    }
    else {// 意図しないタイプ。
        
    }
    
    
    return 0;
}

bool EscapeDataManager::getPlayTitleIsCollectionDisplayForHint()
{
    //return true;
    auto value = getTitleMapWithIndex(m_playTitleNum)[MapKey_HintCollection];
    
    if (value.isNull()) {
        return false;
    }
    
    return value.asBool();
}

bool EscapeDataManager::isRepairing(int index)
{
    auto titleMap = getTitleMapWithIndex(index);
    return titleMap[MapKey_IsRepairing].asBool();
}

#pragma mark VoteInfo関連
ValueVector EscapeDataManager::getVoteDataVector()
{
    static auto infodata = getVersionValueMap();
    static auto vector = infodata[MapKey_VoteInfo];
    
    if (vector.isNull()){
        ValueVector nullVector;
        log("ボートデータはありません。");
        return nullVector;
    }
    
    Common::performProcessForDebug([](){
        //ValueHelper::dumpValue(vector);
    }, nullptr);
    
    static auto valueVector = vector.asValueVector();
    
    return valueVector;
}

bool EscapeDataManager::isPassedVoteDate(ValueMap voteValueMap, const char* mapKey)
{
    return getDaysToVoteDate(voteValueMap, mapKey)<=0;
}

int EscapeDataManager::getDaysToVoteDate(ValueMap voteValueMap, const char *mapKey)
{
    auto dateMapValue = voteValueMap[mapKey];
    auto dateMap = dateMapValue.asValueMap();
    
    int year = atoi(dateMap[MapKey_Year].asString().c_str());
    int month = atoi(dateMap[MapKey_Month].asString().c_str());
    int day = atoi(dateMap[MapKey_Day].asString().c_str());
    
    long long date = Common::dateWithDateDetail(TimeUnit_Day, year, month, day, 0, 0, 0);
    
    long long nowTime = Common::dateWithTimeStamp(TimeUnit_Day, FireBaseBridge::getInstance()->getServerTimestamp());
    
    long long restTime = Common::differenceTime(nowTime, date);
    int restDay = (int)(restTime/60/60);
    
    log("締切日は%d：：現在時刻は%d::残り日%d",date, nowTime, restDay);
    
    return restDay;
}

std::string EscapeDataManager::getLocalizeStringForVoteWithKey(ValueMap voteValueMap, const char *key)
{
    auto textValue = voteValueMap[key];
    if (textValue.isNull()) {
        return "";
    }
    
    auto textMapValue = textValue.asValueMap();
    auto text = textMapValue[Common::localize("en", "jp", "ch", "tw", "ko")].asString();
    
    log("取得下テキストは%s",text.c_str());
    return text;
}

VoteTerm EscapeDataManager::getVoteTerm(ValueMap voteValueMap)
{
    bool hidePassed = isPassedVoteDate(voteValueMap, MapKey_HideDate);
    bool deadlinePassed = isPassedVoteDate(voteValueMap, MapKey_DeadlineDate);
    bool interimPassed = isPassedVoteDate(voteValueMap, MapKey_InterimDate);
    
    VoteTerm term = VoteTerm_NONE;
    
    if (hidePassed) {
        term = VoteTerm_Hide;
    }
    else if (deadlinePassed) {
        term = VoteTerm_Result;
    }
    else if (interimPassed) {
        term = VoteTerm_Interim;
    }
    
    return term;
}

std::string EscapeDataManager::getDateStringForVote(ValueMap voteValueMap, const char *mapkey)
{
    auto dateMapValue = voteValueMap[mapkey];
    auto dateMap = dateMapValue.asValueMap();
    
    //int year = atoi(dateMap[MapKey_Year].asString().c_str());
    int month = atoi(dateMap[MapKey_Month].asString().c_str());
    int day = atoi(dateMap[MapKey_Day].asString().c_str());
    
    auto transedDateSt = StringUtils::format("%d/%d", month, day);
    
    return transedDateSt;
}

#pragma mark- ディレクトリ関連
std::string EscapeDataManager::getWritableFileDir(const std::string fileDir)
{
    auto writableDir = FileUtils::getInstance()->getWritablePath();
    auto writableFileDir = writableDir +fileDir;
    
    return writableFileDir;
}

std::string EscapeDataManager::getIconDirWithFile(const std::string noExtFileName)
{
    return getWritableFileDir(IconsDir + noExtFileName + JpgExt);
}

std::string EscapeDataManager::getTitleDirWithIndex(int index)
{
    auto titleMap = getTitleMapWithIndex(index);
    
    auto versionString = titleMap[MapKey_TitleVersion].asString();
    auto fileName = titleMap[MapKey_FileName].asString();
    auto directoryName = transformFileToDirectory(fileName);
    
    auto titleDir = StringUtils::format("%s%s",TitlesDir , directoryName.c_str());
    
    auto type = getTypeTitleCondition(index);//チュートリアルなら
    
    return (type == TitleCondition_TUTORIAL)?  titleDir:getWritableFileDir(titleDir);
}

std::string EscapeDataManager::getPlayTitleDir()
{
    return getTitleDirWithIndex(m_playTitleNum);
}

std::string EscapeDataManager::getTitleResourceDir(int num){
    auto selectedTitleMap = getTitleMapWithIndex(num);
    
    auto versionString = selectedTitleMap[MapKey_TitleVersion].asString();
    auto fileName = selectedTitleMap[MapKey_FileName].asString();
    
    /*auto value = getTitleDataVector().at(num);
    ValueHelper::dumpValue(value);*/
    
    return getTitleDirWithIndex(num) + fileName + "_" + FilePath_Resource + "_" + versionString + "/";
}

std::string EscapeDataManager::getPlayTitleResourceDir()
{
    return (LocalMode) ?  Common::getLocalTestDir() : getTitleResourceDir(m_playTitleNum);
}

std::string EscapeDataManager::getPlayTitleResourceDirWithFile(const char *fileName)
{
    return getPlayTitleResourceDir() + fileName;
}

std::string EscapeDataManager::getPlayTitleSoundResourceDirWithSoundFile(const char *soundfileName)
{
    return getPlayTitleResourceDir() + "sounds/" + soundfileName;
}

std::string EscapeDataManager::getPlayTitleSoundResourceDirWithExistSoundFile(const char *soundfileName)
{
    auto localSoundDir = getPlayTitleSoundResourceDirWithSoundFile(soundfileName);
    if (FileUtils::getInstance()->isFileExist(localSoundDir)) {//ダウンロードしたリソースの中に入っている。
        log("音源[%s]は,リソースの中にある",soundfileName);
        return localSoundDir;
    }
    else if (FileUtils::getInstance()->isFileExist(soundfileName)) {//元から積んである
        log("音源[%s]は,元から積んである",soundfileName);

        return soundfileName;
    }
    
    return "";
}

std::string EscapeDataManager::getTitleDataDirWithIndex(int index)
{
    auto dir = getTitleDirWithIndex(index) + FilePath_Data + "/";
    if (getTypeTitleCondition(index) == TitleCondition_TUTORIAL) {//チュートリアルなら
        //リソースはローカルにあっても、データのディレクトリはWritablePathにあるのだ
        dir = getWritableFileDir(dir);
    }
    
    //log("ディレクトリ-----は%s",dir.c_str());
    
    return dir;
}

std::string EscapeDataManager::getTitleDataDirWithIndexAndFile(int index, const char *fileName)
{
    return  getTitleDataDirWithIndex(index) + fileName;
}

std::string EscapeDataManager::getPlayTitleDataDirWithFile(const char *fileName)
{
    return getTitleDataDirWithIndexAndFile(m_playTitleNum, fileName);
}

std::string EscapeDataManager::getTitleDataDirForSaveWithIndexAndFile(int index, const char *fileName)
{
    auto dataDir = getTitleDataDirWithIndex(index);
    
    auto indexDataDir = getTitleDataDirWithIndexAndFile(index, fileName);
    //log("データ%s,プレイデータは%s", dataDir.c_str(), playdataDir.c_str());
    
    bool isExistDir = FileUtils::getInstance()->isDirectoryExist(dataDir);
    if (!isExistDir) {
        FileUtils::getInstance()->createDirectory(dataDir);
        log("ディレクトリの存在確認Dir%d", FileUtils::getInstance()->isDirectoryExist(dataDir));
    }
    
    return indexDataDir;
}

std::string EscapeDataManager::getPlayTitleDataDirForSaveWithFile(const char *fileName)
{
    return getTitleDataDirForSaveWithIndexAndFile(m_playTitleNum, fileName);
}

std::string EscapeDataManager::getAchievementDataDir()
{
    auto titleDir = StringUtils::format("%s%s",AchievementDir , FilePath_Achievement);
    auto achievementDir = getWritableFileDir(titleDir);
    
    log("アチーブメントのディレクトリ%s",achievementDir.c_str());
    return achievementDir;
}

bool EscapeDataManager::isUsedShakeListenerOnPlayTitle()
{
    //シェイクが含まれるステージはここに追加して行く
    std::vector<std::string> usedShakeTitles = {"christmas","circus","twothousandnineteen"};
    
    auto titleName = getPlayTitleName();
    for (auto shakeTitleName : usedShakeTitles) {
        if (shakeTitleName == titleName) {
            return true;
        }
    }
    return false;
}

void EscapeDataManager::saveData(int index, const char *recordFilePath, Value savedValue)
{
    auto file = EscapeDataManager::getInstance()->getTitleDataDirForSaveWithIndexAndFile(index, recordFilePath);
    
    if (savedValue.getType() == Value::Type::VECTOR) {
        FileUtils::getInstance()->writeValueVectorToFile(savedValue.asValueVector(), file, [file](bool success){
            //log("[%s]にValueVectorを書き出しました。%d", file.c_str(), success);
        });
    }
    else if(savedValue.getType() == Value::Type::MAP){
        FileUtils::getInstance()->writeValueMapToFile(savedValue.asValueMap(), file, [file](bool success){
            //log("[%s]にValueMapを書き出しました。%d", file.c_str(), success);
        });
    }
    else {
        log("VectorとMap以外のデータを保存しようとしている。");
        assert(false);
    }
}

void EscapeDataManager::saveDataOnPlay(const char *recordFilePath, Value savedValue)
{
    saveData(m_playTitleNum, recordFilePath, savedValue);
}

#pragma mark- ダウンロード関連

void EscapeDataManager::loadingAppData(const std::function<void(bool success)>& callback)
{
    //[Debug or Release]の中を全て取得する。
    FireBaseBridge::getInstance()->loadingValue(FirebaseDownloadDir, [this, callback](bool success, Value value){
        
        if (success) {
            this->setFB_AppDataMap(value.asValueMap());
        }
        
        if (callback) {
            callback(success);
        }
    });
}


bool EscapeDataManager::checkNeedAppVersion()
{
    auto mapValue = getFB_AppDataMap()[MapKey_AppInfo].asValueMap();

    auto needAppVersion = mapValue[MapKey_NeedAppVersion].asValueMap();
    auto needVersionSt = needAppVersion[SystemOS].asString();
    
    auto nowAppVersionSt = Application::getInstance()->getVersion();
    log("現在のアプリバージョンは%sです。必要なアプリバージョンは%s", nowAppVersionSt.c_str(), needVersionSt.c_str());
    

    return !isSupportedVersion(nowAppVersionSt, needVersionSt);
}

void EscapeDataManager::checkVersion(const onFinishedLoadingFile &callback)
{
    //firebaseから読み取ったデータ
    auto mapValue = getFB_AppDataMap()[MapKey_AppInfo].asValueMap();
    auto reviewMode = mapValue.at("ReviewMode").asBool();
    auto versionNum = mapValue.at("Version").asInt();
    
    //ローカルに保存されているデータ
    auto localAppInfoMap = getAppDataMap();
    auto localVersionCode = localAppInfoMap[MapKey_VersionCode];
    
    if (!localVersionCode.isNull() && localVersionCode.asInt() == versionNum) {//新規起動ではなくて、バージョンPlistの更新なし
        log("新規の情報ローディングなし。");
        if (callback) {
            callback(false, "Firebaseからのローディングに失敗");
        }
        return;
    }
    
    //レビューモードでなおかつ、ローカル指定バージョンがサーバーバージョンより大きければ、レビューディレクトリからversion.plistを取得。
    auto isReviewMode = reviewMode && ReviewVersion > versionNum;
    auto versionName = "version";
    
    //ダウンロードするバージョン番号。
    auto downloadVersionNum = (isReviewMode)? ReviewVersion : versionNum;
    auto versionFileName = StringUtils::format("%s_%d", versionName, downloadVersionNum);
    
    auto downloadUrl = StringUtils::format("%s%s%s",(isReviewMode)? VersionForReviewDir : VersionDir, versionFileName.c_str(), ZipExt);
    
    auto outDir = getWritableFileDir(VersionDir);
    
    log("ダウンロードURLは%s::%d::%d::%s",downloadUrl.c_str(),reviewMode, versionNum, outDir.c_str());
    
    this->getUrlAndDownload(downloadUrl, outDir, [outDir, versionName, versionFileName, callback,downloadUrl](bool successed, std::string errorDescription){
        
        if (successed) {
            auto appVersionFileDir = outDir + versionName + PlistExt;
            
            //既存のバージョンplistを削除します。
            if (FileUtils::getInstance()->isFileExist(appVersionFileDir)) {
                FileUtils::getInstance()->removeFile(appVersionFileDir);
            }
            //保存したplistをversion.plistにリネームする。→version.plistは一つでいい。
            auto savedVersionFileDir = StringUtils::format("%s%s", outDir.c_str(), versionFileName.c_str()) + PlistExt;
            FileUtils::getInstance()->renameFile(savedVersionFileDir, appVersionFileDir);
            
            log("リネームしました。%s:::%syyyy%s",savedVersionFileDir.c_str(),appVersionFileDir.c_str(),downloadUrl.c_str());
        }
        
        if (callback) {
            callback(successed, errorDescription);
        }
    });
}

void EscapeDataManager::loadingEachIconData(const onFinishedLoadingEachIcon &callback)
{
    auto appInfoMap = getAppDataMap();
    log("バージョンコードは%d",appInfoMap["VersionCode"].asInt());

    auto titleInfoVector = getTitleDataVector();
    
    for (auto map : titleInfoVector) {
        auto titleMap = map.asValueMap();
        auto titleIconName = titleMap[MapKey_FileName];
        
        if (!titleIconName.isNull()) {
            auto iconName = titleIconName.asString();
            auto fileDir = EscapeDataManager::getInstance()->getIconDirWithFile(iconName);
           
            if (!FileUtils::getInstance()->isFileExist(fileDir)) {//該当のアイコンが端末になければ。
                auto iconJpgName = iconName + "_jpg";//zip状態でpngとjpgを区別するための添字

                auto serverDir = IconsDir + iconJpgName + ZipExt;
                auto outIconDir = getWritableFileDir(IconsDir);//保存可能ディレクトリ/Icons

                getUrlAndDownload(serverDir, outIconDir, [this, outIconDir, callback, iconName, fileDir, iconJpgName](bool successed, std::string errorDescription){
                    log("アイコンのダウンロードに成功%d:::", successed);
                    
                    if (!successed) return;
                    
                    FileUtils::getInstance()->renameFile(outIconDir, iconJpgName+JpgExt, iconName+JpgExt, [this, callback, iconName](bool success){
                        log("リネーム完了しました！！%d", success);

                        //過去にpngでダウンロード経験があれば、削除する。
                        auto old_png_dir = getWritableFileDir(IconsDir + iconName + PngExt);
                        if (FileUtils::getInstance()->isFileExist(old_png_dir)) {
                            FileUtils::getInstance()->removeFile(old_png_dir, [](bool completed){
                                log("過去にダウンロードしたpngファイルを削除します。");
                            });
                        }
                        
                        if (success) {
                            if (callback) {
                                callback(success, iconName);
                            }
                        }
                    });
                });
            }
        }
    }
}

void EscapeDataManager::loadingAllTitleData()
{
    auto key = "全てのダウンロードを完了しました。";
    Common::startTimeForDebug(key);

    if (getAllTitleDataDownloadStatus() == AllTitleDataDownloadStatus_DOWNLOADING) {
        return;
    }
    
    //ステータスをダウンロード中に変更。
    setAllTitleDataDownloadStatus(AllTitleDataDownloadStatus_DOWNLOADING);
    
    loadingNeededTitleData(0, [this,key](){
        Common::stopTimeForDebug(key);
        
        HttpAccessManager::postNotification(NotificationKey_Complete_AllDownload);
        
        //ダウンロードステータスをリセット。
        setAllTitleDataDownloadStatus(AllTitleDataDownloadStatus_NONE);
        //インデックス配列をクリア。
        m_needTitleIndexVector = Value(ValueVector());
    });
}

ValueVector EscapeDataManager::getNeedDownloadTitleIndexVector()
{
    if (!m_needTitleIndexVector.isNull()) {
        log("AllDL一度読み込みすみ。");
        return m_needTitleIndexVector.asValueVector();
    }
    
    log("AllDLはじめて。");

    ValueVector needTitleIndexVector;
    for (int i = 0; i < getTitleDataSortedVector().size(); i++ ) {
        
        auto titleType = getTypeTitleCondition(i);
        if (!conditionOfFixedTitle(titleType) && !isExistTitleResource(i)) {//リソースデータがなくてチュートリアルでもない
            needTitleIndexVector.push_back(Value(i));
        }
        
        if (/*Common::isAndroid() &&*/ needTitleIndexVector.size() == 6) {//androidの場合はダウンロードが必要なトップ6個のデータのみ読み込みを行う。→Ver5.0~ iOSのダウンロード数も6個に制限
            break;
        }
    }
    
    m_needTitleIndexVector = Value(needTitleIndexVector);
    
    return needTitleIndexVector;
}

int EscapeDataManager::getAboutAllTitleSize()
{
    auto noTitleVector = getNeedDownloadTitleIndexVector();
    auto singelSize = 20;//大体一つあたり20メガバイト見ておけばOK.(jpgに対応してから、ファイルサイズが大幅に小さくなりました。)
    
    return (int)noTitleVector.size()*singelSize;
}

void EscapeDataManager::loadingTitleData(int index, const onFinishedLoadingFile &callback)
{
    auto playTitleMap = getTitleMapWithIndex(index);
    
    auto versionString = playTitleMap[MapKey_TitleVersion].asString();
    auto fileName = playTitleMap[MapKey_FileName].asString();
    auto directoryName = transformFileToDirectory(fileName);
    
    auto resourceXDir = TitlesDir + directoryName + fileName + "_" + FilePath_Resource + "_%d";
    auto serverDir = StringUtils::format(resourceXDir.c_str(), atoi(versionString.c_str())) + ZipExt; //Titles/Airplane/airplane_resource_1.zip
    auto outDir = getWritableFileDir(TitlesDir + directoryName);
    
    log("サーバーディレクトリ::%s\n出力先ディレクトリ::%s", serverDir.c_str(), outDir.c_str());
    getUrlAndDownload(serverDir, outDir, [this, callback, versionString, resourceXDir](bool successed, std::string errorDescription){
        if (successed) {//最新バージョンのアップデートに成功したら、過去のバージョンリソースは全て削除
            for (int i = atoi(versionString.c_str())-1; i > 0; i--) {
                auto oldVersionResourceName = StringUtils::format(resourceXDir.c_str(), i);
                auto oldVersionResourceDir = getWritableFileDir(oldVersionResourceName);
                
                bool isExist = FileUtils::getInstance()->isDirectoryExist(oldVersionResourceDir);
                if (isExist) {//過去のバージョンのリソースデータが存在しているなら。
                    FileUtils::getInstance()->removeDirectory(oldVersionResourceDir);
                }
            }
        }
        
        if (callback) {
            callback(successed, errorDescription);
        }
    });
}

void EscapeDataManager::loadingPlayTitleData(const onFinishedLoadingFile &callback)
{
    loadingTitleData(m_playTitleNum, callback);
}

#pragma mark- キャッシュ関連
void EscapeDataManager::manageCatheAtlas(bool isAdd, const onFinishedAddCathe &callback)
{
    //進捗は0パーセントからスタート
    HttpAccessManager::postNotification(NotificationKey_Progress, Value(0));

    if (LocalMode) {//ローカルモード時はキャッシュを削除しない。
        if (callback) {
            callback();
        }
    }
    else {//キャッシュを操作する。
        operateCathe(0, isAdd, callback);
    }
}

void EscapeDataManager::deleteResourceDataOfPlayTitle()
{
    log("%sリソースが存在しているかどうか%d",getPlayTitleName().c_str(),isExistPlayTitleResource());

    if (isExistPlayTitleResource()) {
        FileUtils::getInstance()->removeDirectory(getPlayTitleResourceDir());
    }
    
    m_needTitleIndexVector = Value();
    log("%sリソースが存在しているかどうか%d",getPlayTitleName().c_str(),isExistPlayTitleResource());
}

void EscapeDataManager::deleteAllResourceData()
{
    for (int i = 0; i < getTitleDataSortedVector().size(); i++) {
        log("%sリソースが存在しているかどうか%d", getTitleNameWithIndex(i).c_str(), isExistTitleResource(i));

        if (isExistTitleResource(i)) {
            FileUtils::getInstance()->removeDirectory(getTitleResourceDir(i));
        }
        log("%sリソースが存在しているかどうか%d", getTitleNameWithIndex(i).c_str(), isExistTitleResource(i));
    }
    
    //デバックモードが2の時はアイコンも消す。
    Common::performProcessForDebug([this](){
        auto titleInfoVector = getTitleDataVector();

        for (auto map : titleInfoVector) {
            auto titleMap = map.asValueMap();
            auto titleIconName = titleMap[MapKey_FileName];
            
            if (!titleIconName.isNull()) {
                auto iconName = titleIconName.asString();
                auto fileDir = EscapeDataManager::getInstance()->getIconDirWithFile(iconName);
                if (FileUtils::getInstance()->isFileExist(fileDir)) {//該当のアイコンが端末にあれば。
                    FileUtils::getInstance()->removeFile(fileDir);
                }
            }
        }
    }, nullptr);
    
    
    m_needTitleIndexVector = Value();
}

#pragma mark- Firebaseデータ関連
Value EscapeDataManager::getAnnounceValue()
{
    static auto announceValue = getFB_AppDataMap()["Announce"];
    return  announceValue;
}

AnnounceDetail EscapeDataManager::getAnnounceDetail()
{
    AnnounceDetail announceDetail;

    auto annouceValue = getAnnounceValue();
    if (annouceValue.isNull()) return announceDetail;
    auto announceMap = getAnnounceValue().asValueMap();
    
    //配信可否
    auto canDelivery = announceMap["delivery"].asBool();
    announceDetail.canDelivery = canDelivery;
    
    //表示間隔
    auto ratio = announceMap["ratio"].asInt();
    announceDetail.ratio = ratio;
    
    //url
    announceDetail.url = announceMap["url"].asString();
        
    //テキスト
    auto textMap = announceMap["text"].asValueMap();
    auto localizedTextValue = textMap[Common::localize("en", "ja", "ch", "tw", "ko").c_str()];
    auto localizedText = localizedTextValue.isNull()? textMap["en"].asString(): localizedTextValue.asString();
    announceDetail.text = localizedText;
    
    return announceDetail;
}

#pragma mark- サウンド関連
void EscapeDataManager::preloadSound(const char *fileName)
{
    auto soundDir = getPlayTitleSoundResourceDirWithExistSoundFile(fileName);
    if (soundDir.size()>0) {
        AudioManager::getInstance()->preload(soundDir.c_str());
    }
    else {
        auto titleName = getPlayTitleName();
        log("プリロードしようとしたけど、[%s]の音声[%s]がどこにもない。", titleName.c_str(), fileName);
    }
}

void EscapeDataManager::playSound(const char *fileName, bool isBGM)
{
    if (strlen(fileName) == 0) return;
    
    auto soundDir = getPlayTitleSoundResourceDirWithExistSoundFile(fileName);

    if (soundDir.size()>0) {//指定したサウンドはどっかしらにある。
        if (isBGM) {//BGM
            Common::playBGM(soundDir.c_str());
        }
        else {//SE
            Common::playSE(soundDir.c_str());
        }
    }
    else {
        auto titleName = getPlayTitleName();
        log("[%s]の音声[%s]がどこにもない。", titleName.c_str(), fileName);
        
        //Common::performProcessForDebug([](){assert(false);}, nullptr);
    }
}

#pragma mark- プライベート
bool EscapeDataManager::conditionOfFixedTitle(TitleCondition condition)
{
    return (condition == TitleCondition_TUTORIAL || !hasClearDataTitleCondition(condition));
}

bool EscapeDataManager::hasClearDataTitleCondition(TitleCondition condition)
{
     return !(condition == TitleCondition_AD || condition == TitleCondition_VOTE);
}

ValueVector EscapeDataManager::sortedValueVector(std::vector<int> orderVector, bool needReverse)
{
    std::vector<int> nonReverseVector;
    
    //広告とチュートリアルは一旦除外
    for (int i=0; i<getTitleDataVector().size(); i++) {
        auto titleType = getTypeTitleCondition(i, false);
        if (conditionOfFixedTitle(titleType)) {
            nonReverseVector.push_back(i);
        }
    }
    
    //ひっくり返す。
    if (needReverse)
        std::reverse(orderVector.begin(), orderVector.end());
    
    //ADとかチュートリアルを固定位置に挿入
    for (auto orderNum : nonReverseVector) {
        auto it = orderVector.begin() + orderNum;
        orderVector.insert(it, orderNum);
    }
    
    //ValueVectorに変換
    ValueVector transedValueVector;
    for (auto num : orderVector) {
        auto titleData = getTitleDataVector()[num];//version.plistの中身が減ることは想定していない。
        transedValueVector.push_back(titleData);
    }
    
    return transedValueVector;
}

std::string EscapeDataManager::transformFileToDirectory(const std::string fileName)
{
    std::string directoryName = fileName + "/";
    transform(fileName.begin(), fileName.begin()+1, directoryName.begin(), ::toupper);

    return directoryName;
}

void EscapeDataManager::getUrlAndDownload(const std::string serverDir, const std::string outDir, const onFinishedLoadingFile &callback)
{
    log("ダウンロードしたファイルの出力先は%s",outDir.c_str());
    HttpAccessManager::postNotification(NotificationKey_Download);
    
    //進捗コールバック
    auto progressCallback = [](float progress){
        log("進捗コールバックは%f",progress);
        HttpAccessManager::postNotification(NotificationKey_Progress, Value(progress*100));
    };
    
    //ローディング完了時コールバック
    auto finishCallback = [serverDir,outDir,callback](bool successed, std::vector<unsigned char> bytes, STRAGE_ERRORCODE errorcode, std::string errorDescription){
        log("zip解凍はじめます。EscapeDataManager");

        if (!successed) {//ファイルのダウンロードに失敗
            auto analyticsName = StringUtils::format("DL_ERROR_%s_%d", serverDir.c_str(), errorcode);

            //ダウンロード失敗の情報を送る。
            AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName(analyticsName.c_str());
            
            if (callback) {
                callback(false, errorDescription);
            }
            return;
        }
        
        HttpAccessManager::pushToUnzip([callback](bool succeededUnzip){
            if (succeededUnzip) {//ファイルのダウンロードに失敗
                if (callback) {
                    callback(true, "");
                }
                return;
            }
        }, outDir, bytes);
    };
    
    //ダウンロード開始！
    FireBaseBridge::getInstance()->downloadFile(serverDir.c_str(), progressCallback, finishCallback);
}

void EscapeDataManager::operateCathe(int i, bool isAdd, const onFinishedAddCathe &callback)
{
    auto playTitleMap = getTitleMapWithIndex(m_playTitleNum);
    
    auto titleResourceDir = getPlayTitleResourceDir();
    auto atlasCountValue = playTitleMap[MapKey_Atlas];
    int index = i;//キャッシュに追加している番号
    int atlasCount = 0;
    std::string atlasName = "atlas";
    std::string ext = ".png";
    if (atlasCountValue.isNull()) {//最新版のpngとjpg分かれているパターン
        int pngCount = playTitleMap[MapKey_Atlas_png].asInt();
        int jpgCount = playTitleMap[MapKey_Atlas_jpg].asInt();
        atlasCount = pngCount + jpgCount;
        
        if (i < pngCount) {
            atlasName = MapKey_Atlas_png;
        }
        else {
            atlasName = MapKey_Atlas_jpg;
            ext = ".jpg";
            index = i - pngCount;
        }
    }
    else {//旧バージョン。pngのみ
        atlasCount = atlasCountValue.asInt();
        
    }
   
    auto atlas = StringUtils::format("%s%s%d", titleResourceDir.c_str(), atlasName.c_str(), index);
    auto plistName = atlas + ".plist";
    auto fileName = atlas + ext;

    if (isAdd) {
        log("[%s][%s]をキャッシュします。", plistName.c_str(), fileName.c_str());
        
        Director::getInstance()->getTextureCache()->addImageAsync(fileName, [plistName, atlasCount, callback, i, this,isAdd](Texture2D*texture){
            SpriteFrameCache::getInstance()->addSpriteFramesWithFile(plistName, texture);
            
            //進捗を表示
            HttpAccessManager::postNotification(NotificationKey_Progress, Value(float((i + 1) * 100) / (atlasCount)));
            
            if (i == atlasCount-1) {//ループを終了します。
                if (callback) {
                    callback();
                }
            }
            else {
                int next = i + 1;
                //log("ネクストは%d",next);
                this->operateCathe(next, isAdd, callback);
            }
            //log("キャッシュしたデータ情報は「\n%s」", Director::getInstance()->getTextureCache()->getCachedTextureInfo().c_str());

            //log("キャッシュするまでに%d秒かかりました。",nowTime-startTime);
        });
    }
    else {
        log("[%s][%s]をキャッシュを削除。", plistName.c_str(), fileName.c_str());
        
        if (i == 0) {
            Director::getInstance()->getTextureCache()->removeAllTextures();
        }
        
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile(plistName);
        
        if (i == atlasCount-1) {//ループを終了します。
            //log("キャッシュした削除後データ情報は「\n%s」", Director::getInstance()->getTextureCache()->getCachedTextureInfo().c_str());
            
            if (callback) {
                callback();
            }
        }
        else {
            int next = i + 1;
            log("キャッシュをネクストは%d",next);
            operateCathe(next, isAdd, callback);
        }
    }
    log("キャッシュを操作しました");
}

void EscapeDataManager::loadingNeededTitleData(int i, const std::function<void ()> &callback)
{
    auto postProgressNotification = [this, i](bool isFirst){
        //通知発行。
        auto progressString = StringUtils::format("%d/%lu",(isFirst)? i:i+1,getNeedDownloadTitleIndexVector().size());
        
        log("全てのダウンロードを完了しました。::%s完了",progressString.c_str());
        
        HttpAccessManager::postNotification(NotificationKey_Progress_AllDownload, Value(progressString));
    };
    
    //タイトルデータのチェック完了時に呼ぶラムダ式
    auto finishCheckTitleData = [i,this,postProgressNotification,callback](){
        //通知発行
        postProgressNotification(false);
        if (i == this->getNeedDownloadTitleIndexVector().size()-1) {//ローディング処理終わりです。
            
            if (callback) {
                callback();
            }
        } else {//次のタイトルをローディングする。
            this->loadingNeededTitleData(i+1, callback);
        }
    };
    
    if (i==0) {
        postProgressNotification(true);
    }
    
    //ダウンロードが必要なインデックスを取得して、ダウンロードを開始。
    auto indexVector = getNeedDownloadTitleIndexVector();
    
    int index = indexVector[i].asInt();
    loadingTitleData(index, [finishCheckTitleData](bool successed, std::string errorDescription){
        //ダウンロードが成功しようがしまいが、次の処理に移る
        finishCheckTitleData();
    });
}

bool EscapeDataManager::isSupportedVersion(std::string versionSt, std::string needVersionSt)
{
    auto versionVector = Common::splitString(versionSt, ".");
    auto needVersionVector = Common::splitString(needVersionSt, ".");
    
    bool isSupported = false;
    for (int i = 0; i<2; i++) {//整数部分と小数部分に分割。
        int versionNum = atoi(versionVector.at(i).c_str());
        int needVersionNum = atoi(needVersionVector.at(i).c_str());

        if (i == 0) {//メジャーバージョン比較
            if (versionNum < needVersionNum) {
                //メジャーバージョンが小さかったらもうアウト。
                break;
            }
            else if(versionNum > needVersionNum){
                isSupported = true;
                break;
            }
        }
        else {//マイナーバージョン比較
            if (versionNum < needVersionNum) {
                break;
            }
            
            isSupported = true;
        }
    }
    log("%sと%sを比較,結果%d", versionSt.c_str(), needVersionSt.c_str(), isSupported);
    
    return isSupported;
}
