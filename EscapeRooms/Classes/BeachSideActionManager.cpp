//
//  IzakayaActionManager.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2018/10/17.
//
//

#include "BeachSideActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "GameScene.h"
#include "TouchManager.h"

using namespace cocos2d;

BeachSideActionManager* BeachSideActionManager::manager =NULL;


BeachSideActionManager* BeachSideActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new BeachSideActionManager();
    }
    
    return manager;
}

int pine_counter=0;

#pragma mark - Back
void BeachSideActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
#pragma mark サメ
    if(key=="shark")
    {
        Vector<FiniteTimeAction*>actions;
        Vector<FiniteTimeAction*>spawns;
        auto counter=0;
        std::vector<int>indexs={0,1,2,2,1,0,2};
        for (auto index : indexs) {
            auto shark=AnchorSprite::create(Vec2(.5, .535), parent->getContentSize(), StringUtils::format("sup_%d_shark.png",backID));
            shark->setPositionX(parent->getContentSize().width*1.33);
            auto original_pos=shark->getPosition();
            parent->addChild(shark);
            if (index==1) {
                shark->setScale(shark->getScale()*.7);
            }
            else if (index==2) {
                shark->setScale(shark->getScale()*.4);
            }
            
            auto duration=5.0;
            auto posX=-parent->getContentSize().width*.33;
            auto angle=3;
            auto swing_count=3;
            auto move=TargetedAction::create(shark, Spawn::create(EaseInOut::create(MoveTo::create(duration, Vec2(posX, original_pos.y)), 1.2),
                                                                  swingAction(angle, duration/swing_count/4.0, shark, 1.2, true, swing_count), NULL));
            
            spawns.pushBack(Sequence::create(DelayTime::create(counter*1.5+1),
                                             move,
                                             TargetedAction::create(shark, MoveTo::create(0, original_pos)), NULL));
            counter++;
        }
        actions.pushBack(Spawn::create(spawns));
        createSpriteToCenter(StringUtils::format("sup_%d_cover.png",backID), false, parent, true);
        
        actions.pushBack(DelayTime::create(2));
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
#pragma mark 海賊船+大砲
    else if (key == "ships") {
        
        Vector<FiniteTimeAction*>actions;
        
        for (int i=0; i<3; i++) {
            auto ship=createAnchorSprite(StringUtils::format("sup_%d_ship_%d.png",backID,i), .25+i*.25, .3, false, parent, true);
            if (DataManager::sharedManager()->isPlayingMinigame()&&i==1) {
                auto mistake=parent->getChildByName("sup_31_mistake.png");
                mistake->retain();
                mistake->removeFromParent();
                ship->addChild(mistake);
            }
            
            
            auto flow=swingAction(Vec2(0, parent->getContentSize().height*.02), 1, ship, 1.2, false);
            if (i==1) {
                std::vector<int>indexs={0,1,3,1,0,2};//左下start 0:右 1:上　2:した 3:左
                Vector<FiniteTimeAction*>sequences;
                auto duration=1.5;
                for (auto index:indexs) {
                    sequences.pushBack(flow);
                    
                    auto cannon=createAnchorSprite(StringUtils::format("sup_31_cannon_%d.png",index), .466, .455, true, parent, true);
                    cannon->setScale(cannon->getScale()*.5);
                    cannon->setLocalZOrder(1);
                    
                    auto scale_original=cannon->getScale();
                    auto pos_original=cannon->getPosition();
                    
                    auto fly_cannon=TargetedAction::create(cannon, EaseIn::create(Spawn::create(RotateBy::create(duration, 360),
                                                                                                FadeIn::create(.2),
                                                                                              JumpBy::create(duration, Vec2(-parent->getContentSize().width*.2, -parent->getContentSize().height*.5), parent->getContentSize().height*.4, 1),
                                                                                              ScaleBy::create(duration, 3)
                                                                                              , NULL), 2.0));
                    
                    auto swing_ship=Sequence::create(swingAction(Vec2(0, -parent->getContentSize().height*.03), duration/8, ship, 1.2, false,1),
                                                     flow->clone(), NULL);
                    
                    //smoke
                    auto smoke=CallFunc::create([parent,pos_original]{
                        
                        BlendFunc func;
                        func.dst=771;
                        func.src=770;
                        
                        auto particle=ParticleSystemQuad::create("smoke.plist");
                        particle->setAutoRemoveOnFinish(true);
                        particle->setDuration(.2);
                        particle->setPosition(pos_original);
                        particle->setBlendFunc(func);
                        particle->setStartColor(Color4F::BLACK);
                        particle->setEndColor(Color4F(0, 0, 0, 0));
                        particle->setSpeed(parent->getContentSize().height*.1);
                        particle->setGravity(Vec2(0, -parent->getContentSize().height*.025));
                        particle->setTotalParticles(20);
                        particle->setAngle(90);
                        particle->setAngleVar(10);
                        particle->setLife(2);
                        particle->setLifeVar(.5);
                        particle->setStartSize(parent->getContentSize().width*.1);
                        particle->setEndSize(parent->getContentSize().width*.2);
                        parent->addChild(particle);
                    });
                    
                    //water
                    auto water=CallFunc::create([parent]{
                        Common::playSE("bashaa.mp3");
                        
                        BlendFunc func;
                        func.dst=1;
                        func.src=770;
                        
                        auto particle=ParticleSystemQuad::create("particle_shower.plist");
                        particle->setTexture(Sprite::create("particle_fire.png")->getTexture());
                        particle->setStartColor(Color4F(.5, .5, 1, 1));
                        particle->setEndColor(Color4F(.5,.5,1,0));
                        particle->setBlendFunc(func);
                        particle->setAutoRemoveOnFinish(true);
                        particle->setDuration(.75);
                        particle->setPosition(parent->getContentSize().width*.25,0);
                        particle->setSpeed(parent->getContentSize().height*2);
                        particle->setSpeedVar(particle->getSpeed()*.1);
                        particle->setGravity(Vec2(0, -parent->getContentSize().height*3));
                        particle->setTotalParticles(300);
                        particle->setAngle(90);
                        particle->setAngleVar(10);
                        particle->setLife(2);
                        particle->setLifeVar(.25);
                        particle->setStartSize(parent->getContentSize().width*.04);
                        particle->setStartSizeVar(parent->getContentSize().width*.02);
                        particle->setEndSize(parent->getContentSize().width*.08);
                        particle->setEndSizeVar(parent->getContentSize().width*.02);

                        parent->addChild(particle);
                    });
                    
                    auto spawn_launch=Spawn::create(createSoundAction("cannon.mp3"),
                                                    swing_ship,
                                                    smoke,
                                                    Sequence::create(fly_cannon,water, NULL),
                                                    NULL);
                    sequences.pushBack(spawn_launch);
                    
                    //reset
                    auto reset=TargetedAction::create(cannon, Spawn::create(FadeOut::create(0),
                                                                            ScaleTo::create(0, scale_original),
                                                                            MoveTo::create(0, pos_original),
                                                                            NULL));
                    sequences.pushBack(reset);
                    
                }
                sequences.pushBack(flow);

                actions.pushBack(Repeat::create(Sequence::create(sequences), -1));
            }
            else{
                actions.pushBack(Repeat::create(flow, -1));
            }
        }
        
        createSpriteToCenter("sup_31_cover.png", false, parent,true);
        auto cover_bottom=createSpriteToCenter("sup_31_cover_1.png", false, parent,true);
        cover_bottom->setLocalZOrder(2);
        
        parent->runAction(Spawn::create(actions));
    }
#pragma mark 海賊船アップ
    else if (key == "ship") {
        Vector<FiniteTimeAction*> actions;
        
        auto ship=createSpriteToCenter(StringUtils::format("sup_%d_ship.png",backID), false, parent, true);
        createSpriteToCenter("sup_32_cover.png", false, parent, true);
        
        if (DataManager::sharedManager()->isPlayingMinigame()&&
            backID==32) {
            auto mistake=parent->getChildByName("sup_32_mistake.png");
            actions.pushBack(Spawn::create(swingAction(Vec2(0, parent->getContentSize().height*.02), 1.5, ship, 1.2, false, -1),
                                           swingAction(Vec2(0, parent->getContentSize().height*.02), 1.5, mistake, 1.2, false, -1), NULL));

        }
        else{
            actions.pushBack(swingAction(Vec2(0, parent->getContentSize().height*.02), 1.5, ship, 1.2, false, -1));
        }
        
        parent->runAction(Repeat::create(Sequence::create(Sequence::create(actions), NULL), -1));
    }
#pragma mark パイナップル
    else if (key == "pineapple"&&DataManager::sharedManager()->getEnableFlagWithFlagID(12)) {
        pine_counter=0;
        std::vector<int>indexs={0,1,0,1,1,0,1};
        auto pineapple=parent->getChildByName<spine::SkeletonAnimation*>("pineapple");
        pineapple->setTimeScale(.8);
        pineapple->setAnimation(0, "left", false);
        
        //終了検知
        pineapple->setCompleteListener([pineapple,indexs,this,parent](spTrackEntry*entry){
            pine_counter++;
            Vector<FiniteTimeAction*>actions;

            if (pine_counter%indexs.size()==0) {
                //最終
                actions.pushBack(DelayTime::create(1));
            }
            
            auto call=CallFunc::create([pineapple,indexs]{
                auto index=indexs.at(pine_counter%indexs.size());
                if (index==0) {
                    pineapple->setAnimation(pine_counter, "left", false);
                }
                else{
                    pineapple->setAnimation(pine_counter, "right", false);
                }
            });
            actions.pushBack(call);
            
            parent->runAction(Sequence::create(actions));
        });
        
        //event検知
        pineapple->setEventListener([backID,parent,indexs](spTrackEntry*entry,spEvent*event){
            auto eventName=StringUtils::format("%s",event->data->name);
            if (eventName=="jump"&&backID==19) {
                Common::playSE("pop_short.mp3");
                auto isLeft=(StringUtils::format("%s",entry->animation->name)=="left");
                
                auto pos=Vec2(parent->getContentSize().width*.5, parent->getContentSize().height*.75);
                auto size=parent->getContentSize().width*.1;
                
                auto particle=ParticleSystemQuad::create("particle_music.plist");
                particle->setAutoRemoveOnFinish(true);
                particle->setPosition(pos);
                particle->setLife(3);
                particle->setLifeVar(0);
                particle->setStartSize(size);
                particle->setEndSize(size);
                particle->setGravity(Vec2(size*0, -size*4));
                particle->setStartColor(Color4F::BLACK);
                particle->setEndColor(Color4F(0, 0, 0, 0));
                
                particle->setAngle(45+90*isLeft);
                particle->setSpeed(size*4);
                parent->addChild(particle);
            }
        });
    }
#pragma mark ラッコ
    else if (key == "racco") {
        
        Vector<FiniteTimeAction*> actions;
        auto leg=createSpriteToCenter("sup_8_leg.png", false, parent,true);
        createSpriteToCenter("sup_8_cover_leg.png", false, parent, true);
        auto tail=createSpriteToCenter("sup_8_tail.png", false, parent,true);
        createSpriteToCenter("sup_8_cover_tail.png", false, parent, true);
        auto racco=createSpriteToCenter("sup_8_racco.png", false, parent,true);
        auto arm_1=createAnchorSprite("sup_8_arm_1.png", .33, .5, false, racco, true);
        createSpriteToCenter("sup_8_racco_cover.png", false, racco, true);
        auto arm_0=createAnchorSprite("sup_8_arm_0.png", .375, .4, false, racco, true);
        arm_0->setLocalZOrder(1);
        //
        auto distance=Vec2(0, -parent->getContentSize().height*.01);
        actions.pushBack(swingAction(distance, 1, racco, 1.2, false, -1));
        actions.pushBack(swingAction(distance, 1, leg, 1.2, false, -1));
        actions.pushBack(swingAction(distance, 1, tail, 1.2, false, -1));

        //cover
        createSpriteToCenter("sup_8_cover.png", false, parent,true);
        
        //particle
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(20)) {
            createSpriteToCenter("sup_8_shell_open.png", false, racco,true);
            arm_0->setRotation(-30);
            arm_1->setRotation(-30);

            if (!DataManager::sharedManager()->getEnableFlagWithFlagID(21)) {
                addShellParticle(racco);
            }
        }
        
        parent->runAction(Spawn::create(actions));
    }
#pragma mark カニのドア
    else if(key=="crub"&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(25)&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(26))
    {
        Vector<FiniteTimeAction*>actions;
        auto open=createSpriteToCenter("sup_91_open.png", true, parent,true);
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(Spawn::create(TargetedAction::create(open, FadeIn::create(.5)),
                                       createSoundAction("gacha.mp3"), NULL));
        auto crub_0=createSpriteToCenter("sup_91_crub_0.png", true, parent,true);
        crub_0->setPositionX(parent->getContentSize().width*.3);
        
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(TargetedAction::create(crub_0, FadeIn::create(.5)));
        //run
        actions.pushBack(createSoundAction("run_small.mp3"));
        actions.pushBack(TargetedAction::create(crub_0, JumpTo::create(.7, parent->getContentSize()/2, parent->getContentSize().height*.02, 4)));
        actions.pushBack(DelayTime::create(.5));
        
        //cloud
        actions.pushBack(CallFunc::create([]{
            DataManager::sharedManager()->setTemporaryFlag("crub", 1);
        }));
        actions.pushBack(cloudAction(parent, "sup_91_cloud.png", Vec2(.51, .51),.7));
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark モアイ
    else if(key=="moai"&&DataManager::sharedManager()->getEnableFlagWithFlagID(13)){
        moveMoai(parent, false);
    }
#pragma mark ペンギン
    else if(key=="penguins"&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(44)){
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<3; i++) {
            auto penguin=parent->getChildByName(StringUtils::format("sup_21_penguin_%d.png",i));
            actions.pushBack(createBounceAction(penguin, .5, .05));
        }
        
        parent->runAction(Repeat::create(Sequence::create(Spawn::create(actions),DelayTime::create(1), NULL), -1));
    }
#pragma mark 車のライト
    else if(key=="cars"){
        std::vector<int>lights;//点灯箇所の配列
        if (backID==92) {
            lights={2,3};
        }
        else{
            lights={1,2};
        }
        
        Vector<FiniteTimeAction*>spawns;
        for (auto index : lights) {
            auto light=createSpriteToCenter(StringUtils::format("sup_%d_%d.png",backID,index), true, parent, true);
            spawns.pushBack(TargetedAction::create(light, Sequence::create(DelayTime::create(1)
                                                                           ,EaseIn::create(FadeIn::create(1), 1.5),
                                                                           DelayTime::create(.5),
                                                                           EaseOut::create(FadeOut::create(1), 2),
                                                                           DelayTime::create(1), NULL)));
        }
        parent->runAction(Repeat::create(Spawn::create(spawns), -1));
    }
#pragma mark ドア
    else if(key=="door")
    {//init
        DataManager::sharedManager()->setTemporaryFlag("door", 0);
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            auto mistake=parent->getChildByName("sup_9_mistake.png");
            mistake->runAction(Sequence::create(DelayTime::create(.5),
                                                FadeIn::create(.5), NULL));
        }
    }
#pragma mark カッパまちがい
    else if(key=="kappa"&&DataManager::sharedManager()->isPlayingMinigame()){
        DataManager::sharedManager()->setTemporaryFlag("mistake_7", 0);

        auto kappa=parent->getChildByName("sup_63_mistake.png");
        kappa->setPositionY(parent->getContentSize().height*.35);
        kappa->retain();
        kappa->removeFromParent();
        auto clip=createClippingNodeToCenter("sup_63_clip.png", parent);
        clip->addChild(kappa);
        
        auto delay=DelayTime::create(.8);
        
        auto shower=CallFunc::create([parent,clip]{
            Common::playSE("bashaa.mp3");
            
            BlendFunc func;
            func.dst=1;
            func.src=770;
            
            auto particle=ParticleSystemQuad::create("particle_shower.plist");
            particle->setTexture(Sprite::create("particle_fire.png")->getTexture());
            particle->setStartColor(Color4F(.5, .5, 1, 1));
            particle->setEndColor(Color4F(.5,.5,1,0));
            particle->setBlendFunc(func);
            particle->setAutoRemoveOnFinish(true);
            particle->setDuration(.3);
            particle->setPosition(parent->getContentSize().width*.5,parent->getContentSize().height*.394);
            particle->setAngle(90);
            particle->setAngleVar(5);
            particle->setSpeed(parent->getContentSize().height*1.25);
            particle->setSpeedVar(particle->getSpeed()*.1);
            particle->setGravity(Vec2(0, -parent->getContentSize().height*3));
            particle->setTotalParticles(100);
            particle->setAngle(90);
            particle->setAngleVar(15);
            particle->setLife(2);
            particle->setLifeVar(.25);
            particle->setStartSize(parent->getContentSize().width*.03);
            particle->setStartSizeVar(parent->getContentSize().width*.01);
            particle->setEndSize(parent->getContentSize().width*.04);
            particle->setEndSizeVar(parent->getContentSize().width*.02);
            
            clip->addChild(particle);
        });
        auto move=TargetedAction::create(kappa, MoveTo::create(.3, parent->getContentSize()/2));
        
        auto call=CallFunc::create([this,parent,kappa]{
            kappa->retain();
            parent->addChild(kappa);
            
            DataManager::sharedManager()->setTemporaryFlag("mistake_7", 1);
        });
        
        parent->runAction(Sequence::create(delay,shower,move,call, NULL));
    }
}

#pragma mark - Touch
void BeachSideActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
        Vector<FiniteTimeAction*> actions;
#pragma mark クラウド。欲しがる。
    if (key == "cloud") {
        int num = DataManager::sharedManager()->getNowBack();
        if (num == 35) {//chef
            auto cloudBounce = cloudBounceAction(parent, {"sup_35_chef.png"}, Vec2(.42, .2), "sup_35_cloud.png", Vec2(.54, .5), "pig.mp3");
            actions.pushBack(cloudBounce);
        }
        else if (num == 68) {//rabbit
            auto cloud = cloudAction(parent, "sup_68_cloud.png", Vec2(.375, .415), .4);
            actions.pushBack(cloud);
        }
        
        //コールバック
        auto call = CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark ラッコ+貝
        else if(key=="racco"){
            Common::playSE("p.mp3");
            auto racco=parent->getChildByName("sup_8_racco.png");
            auto shell=createAnchorSprite("sup_8_shell.png", .4, .45, true, racco, true);
            
            
            //put shell
            actions.pushBack(TargetedAction::create(shell, FadeIn::create(1)));
            actions.pushBack(DelayTime::create(1));
            
            //rotate arms
            auto arm_0=racco->getChildByName("sup_8_arm_0.png");
            auto arm_1=racco->getChildByName("sup_8_arm_1.png");
            auto angle=30;
            auto rotates=Spawn::create(TargetedAction::create(arm_0, RotateBy::create(1, -angle)),
                                       TargetedAction::create(arm_1, RotateBy::create(1, -angle)),
                                       createSoundAction("kyuu.mp3"), NULL);
            actions.pushBack(rotates);
            actions.pushBack(DelayTime::create(1));
            
            //punch lambda
            auto punch=[arm_0,arm_1,shell,angle,this](bool isLeft){
                Vector<FiniteTimeAction*>seq;
                Node*node;
                if (isLeft) {
                    node=arm_1;
                }
                else{
                    node=arm_0;
                }
                
                auto duration=.1;
                seq.pushBack(Sequence::create(TargetedAction::create(node, EaseIn::create(RotateBy::create(duration, angle), 1.5)),
                                              Spawn::create(this->createSoundAction("hit.mp3"),
                                                            TargetedAction::create(node, EaseOut::create(RotateBy::create(duration, -angle), 1.5)),
                                                            this->swingAction(Vec2(shell->getContentSize().width*.01, 0), .05, shell, 1, false),NULL)
                                              , NULL));

                return Sequence::create(seq);
            };
            
            for (int i=0; i<14; i++) {
                auto isLeft=i%2;
                actions.pushBack(punch(isLeft));
                if (i<4) {
                    actions.pushBack(DelayTime::create(.2));
                }
            }
            
            //open shell & addParticle
            actions.pushBack(DelayTime::create(1));
            auto shell_open=createSpriteToCenter("sup_8_shell_open.png", true, racco, true);
            actions.pushBack(Spawn::create(createChangeAction(1, .5, shell, shell_open, "pop.mp3"),
                                           CallFunc::create([racco,this]{this->addShellParticle(racco);}), NULL));
            actions.pushBack(DelayTime::create(1));
            
            //コールバック
            auto call=CallFunc::create([callback]{
                Common::playSE("pinpon.mp3");
                callback(true);
            });
            actions.pushBack(call);
            
            parent->runAction(Sequence::create(actions));
        }
#pragma mark かに+おにぎり
        else if (key == "riceball") {
            Common::playSE("p.mp3");
            
            auto anchor=Vec2(.33, .4);
            auto crub_0=parent->getChildByName("sup_91_crub_0.png");
            auto crub_1=createAnchorSprite("sup_91_crub_1.png", anchor.x, anchor.y, true, parent, true);
            auto arm=createSpriteToCenter("sup_91_crub_1_arm.png", false, crub_1, true);
            crub_1->setScale(crub_1->getScale()*.7);
            auto riceball=createAnchorSprite("sup_91_riceball.png", anchor.x, anchor.y, true, parent, true);
            auto crub_2_arm_0=createAnchorSprite("sup_91_crub_2_arm_0.png", .375, .32, true, parent, true);
            crub_2_arm_0->setLocalZOrder(1);
            auto crub_2_arm_1=createAnchorSprite("sup_91_crub_2_arm_1.png", .625, .32, true, parent, true);
            crub_2_arm_1->setLocalZOrder(1);

            auto crub_2=createSpriteToCenter("sup_91_crub_2.png", true, parent, true);
            crub_2->setLocalZOrder(1);
            
            //fadein rice
            actions.pushBack(TargetedAction::create(riceball, Spawn::create(jumpAction(Vec2(0, parent->getContentSize().height*.05), .5, riceball, 1.3),
                                                                            createSoundAction("pop.mp3"),
                                                                            FadeIn::create(1), NULL)));
            
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(createChangeAction(.7, .5, crub_0, crub_1));
            actions.pushBack(TargetedAction::create(crub_1, Spawn::create(jumpAction(parent->getContentSize().height*.01, .1, crub_1, 1.3, 4),
                                                                          createSoundAction("run_small.mp3"),
                                                                          ScaleTo::create(.8, crub_0->getScale()),NULL)));
            actions.pushBack(TargetedAction::create(arm, Spawn::create(FadeOut::create(.3),
                                                                       createSoundAction("pop.mp3"), NULL)));
            actions.pushBack(DelayTime::create(.5));
            
            //run out
            auto duration=.8;
            auto scale=.6;
            auto move=MoveBy::create(duration, Vec2(-parent->getContentSize().width*.15, 0));
            
            actions.pushBack(Spawn::create(TargetedAction::create(crub_1, Spawn::create(jumpAction(parent->getContentSize().height*.01, duration/8, crub_1, 1.1, 4),
                                                                                        ScaleBy::create(duration, scale),
                                                                                        move,
                                                                                        NULL)),
                                           TargetedAction::create(riceball,Spawn::create(ScaleBy::create(duration, scale),
                                                                                         move->clone(),
                                                                  NULL)),
                                           createSoundAction("run_small.mp3"),
                                           NULL));
            
            actions.pushBack(DelayTime::create(.3));
            
            //moveout
            auto moveout=MoveBy::create(.5, Vec2(-parent->getContentSize().width*.15, 0));
            actions.pushBack(Spawn::create(TargetedAction::create(crub_1, moveout),
                                           TargetedAction::create(riceball, Sequence::create(moveout->clone(),
                                                                                             RemoveSelf::create(), NULL)),
                                           NULL));
            actions.pushBack(DelayTime::create(1));
            
            //re appear
            actions.pushBack(TargetedAction::create(crub_0, Spawn::create(FadeIn::create(0),
                                                                          MoveBy::create(0, Vec2(-parent->getContentSize().width*.2, 0))
                                                                          , NULL)));
            actions.pushBack(TargetedAction::create(crub_0, Spawn::create(JumpTo::create(.7, parent->getContentSize()/2, parent->getContentSize().height*.02, 4),
                                                                          createSoundAction("run_small.mp3"), NULL)));

            //infront
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(createChangeAction(1, .7, {crub_0}, {crub_2,crub_2_arm_0,crub_2_arm_1},"pop.mp3"));
            actions.pushBack(DelayTime::create(1));
            
            //dance
            actions.pushBack(Spawn::create(swingAction(8, .4, crub_2_arm_0, 1.3, false, 1),
                                           swingAction(-8, .4, crub_2_arm_1, 1.6,false,1),
                                           createSoundAction("run_small.mp3"),
                                           NULL));
            
            actions.pushBack(DelayTime::create(1));
            //actions.pushBack(TargetedAction::create(crub_2, FadeOut::create(.5)));
            
            //コールバック
            auto call = CallFunc::create([callback]{
                callback(true);
            });
            actions.pushBack(call);
            
            parent->runAction(Sequence::create(actions));
        }
#pragma mark かに+糸
        else if (key == "cutyarn") {
            Common::playSE("p.mp3");
            
            auto arm = createAnchorSprite("sup_6_arm.png", .45, .4, true, parent, true);
            auto crub = createSpriteToCenter("sup_6_crub.png", true, parent, true);
            auto yarn_0=parent->getChildByName("sup_6_yarn_0.png");
            auto yarn_1=parent->getChildByName("sup_6_yarn_1.png");
            auto yarn_2=parent->getChildByName("sup_6_yarn_2.png");

            //fadein crub
            auto fadein_crub=Spawn::create(TargetedAction::create(arm, FadeIn::create(1)),
                                           TargetedAction::create(crub, FadeIn::create(1)), NULL);
            
            actions.pushBack(fadein_crub);
            actions.pushBack(DelayTime::create(.5));
            
            //rotate arm & cut yarn
            auto rotate=Sequence::create(createSoundAction("run_small.mp3"),
                                         Spawn::create(EaseInOut::create(RotateBy::create(2, -90), 1.4),
                                                       swingAction(Vec2(parent->getContentSize().width*.005,0), .1, arm, 1.2, false, 10)
                                                       , NULL),
                                         NULL);

            auto rotate_back=Sequence::create(createSoundAction("sword_2.mp3"),
                                              Spawn::create(EaseInOut::create(RotateBy::create(.2, 100), 1.4),
                                                            Sequence::create(DelayTime::create(.05),
                                                                             TargetedAction::create(yarn_0, FadeOut::create(.05)),
                                                                             TargetedAction::create(yarn_1, FadeOut::create(.05)),
                                                                             TargetedAction::create(yarn_2, FadeOut::create(.05)),
                                                                             NULL),
                                                            NULL),
                                              NULL);;
            
            actions.pushBack(Spawn::create(TargetedAction::create(arm, rotate), NULL));
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(Spawn::create(TargetedAction::create(arm, rotate_back), NULL));

            actions.pushBack(DelayTime::create(1));
            actions.pushBack(Spawn::create(TargetedAction::create(crub, FadeOut::create(.7)),
                                           TargetedAction::create(arm, FadeOut::create(.7)),
                                           NULL));
            actions.pushBack(DelayTime::create(.5));
            //コールバック
            auto call=CallFunc::create([callback]{
                Common::playSE("pinpon.mp3");
                callback(true);
            });
            actions.pushBack(call);
            
            parent->runAction(Sequence::create(actions));
        }
#pragma mark 汚れ+ふきん
        else if(key == "stain") {
            Common::playSE("p.mp3");
            
            auto item = createSpriteToCenter("sup_18_towel.png", true, parent, true);
            item->setLocalZOrder(1);
            auto fadein_item = TargetedAction::create(item, FadeIn::create(.4));
            actions.pushBack(fadein_item);
            actions.pushBack(DelayTime::create(.7));
            
            //ゴシゴシ&黒板切り替え
            float duration = .4;
            Vector<FiniteTimeAction* >spawnActions;
            Vector<FiniteTimeAction* >goshigoshiActions;
            std::vector<Vec2> positions = {Vec2(.2, .4), Vec2(.32, .6), Vec2(.44, .4), Vec2(.56, .6), Vec2(.68, .4), Vec2(.8, .7)};
            for (int i = 0; i < positions.size(); i++) {
                auto pos = positions[i];
                if (i == 0) {
                    item->setPosition(parent->getContentSize().width*pos.x, parent->getContentSize().height*pos.y);
                    goshigoshiActions.pushBack(createSoundAction("goshigoshi.mp3"));
                }
                else {
                    auto easeIn = EaseIn::create(MoveTo::create(.35, Vec2(parent->getContentSize().width*pos.x, parent->getContentSize().height*pos.y)), 1);
                    goshigoshiActions.pushBack(TargetedAction::create(item, easeIn));
                }
            }
            goshigoshiActions.pushBack(DelayTime::create(.5));
            goshigoshiActions.pushBack(TargetedAction::create(item, FadeOut::create(.3)));
            spawnActions.pushBack(Sequence::create(goshigoshiActions));
            
            Vector<FiniteTimeAction* >boardChangeActions;
            auto board_0 = parent->getChildByName("sup_18_stain.png");
            auto board_1 = createSpriteToCenter("sup_18_hint.png", true, parent, true);
            boardChangeActions.pushBack(DelayTime::create(duration*4));
            boardChangeActions.pushBack(createChangeAction(duration, duration, board_0, board_1));
            spawnActions.pushBack(Sequence::create(boardChangeActions));
            actions.pushBack(Spawn::create(spawnActions));
            
            auto call=CallFunc::create([callback](){
                Common::playSE("pinpon.mp3");
                callback(true);
            });
            actions.pushBack(call);
            
            parent->runAction(Sequence::create(actions));
        }
#pragma mark 水鉄砲あげる
        else if (key=="gun") {
            Common::playSE("p.mp3");
            
            auto rabbit_0 = parent->getChildByName(StringUtils::format("sup_68_rabbit_0.png"));
            rabbit_0->setCascadeOpacityEnabled(true);
            auto gun=createAnchorSprite("sup_68_gun.png", .25, .3, true, parent, true);
            auto gun_1 = createSpriteToCenter("sup_68_gun_onrabbit.png", true, rabbit_0,true);
            auto rabbit_1 = createAnchorSprite("sup_68_rabbit_1.png", rabbit_0->getAnchorPoint().x, rabbit_0->getAnchorPoint().y, true, parent, true);
            
            auto stain=parent->getChildByName("sup_68_stain.png");
            auto hint=createSpriteToCenter("sup_68_hint.png", true, parent, true);
            
            //アニメーション
            auto give_gun=TargetedAction::create(gun, Sequence::create(FadeIn::create(1),
                                                                       ScaleBy::create(1, .7), NULL));
            actions.pushBack(give_gun);
            actions.pushBack(DelayTime::create(1));
            auto change_rabbit_0=createChangeAction(1, .5, gun, gun_1, "pop.mp3");
            actions.pushBack(change_rabbit_0);
            auto bounce=createBounceAction(rabbit_0, .5, .05);
            actions.pushBack(bounce);
            
            //change 銃を構える
            auto change_rabbit_1=createChangeAction(1, .5, rabbit_0, rabbit_1, "pop.mp3");
            actions.pushBack(change_rabbit_1);
            actions.pushBack(DelayTime::create(.5));
            
            //水を発射
            auto shake_rabbit=TargetedAction::create(rabbit_1, jumpAction(Vec2(-parent->getContentSize().width*.02, 0), .1, rabbit_1, 1, 20));
            
            auto water=CallFunc::create([parent,rabbit_1]{
                Common::playSE("jaa.mp3");
                BlendFunc func;
                func.dst=1;
                func.src=770;
                
                auto particle=ParticleSystemQuad::create("particle_shower.plist");
                particle->setTexture(Sprite::create("particle_fire.png")->getTexture());
                particle->setStartColor(Color4F(.5, .5, 1, 1));
                particle->setEndColor(Color4F(.5,.5,1,0));
                particle->setBlendFunc(func);
                particle->setAutoRemoveOnFinish(true);
                particle->setDuration(3.0);
                particle->setPosition(parent->getContentSize().width*.42,parent->getContentSize().height*.33);
                particle->setPosVar(Vec2::ZERO);
                particle->setSpeed(parent->getContentSize().height*1.5);
                particle->setSpeedVar(particle->getSpeed()*.02);
                particle->setGravity(Vec2(0, -parent->getContentSize().height*3));
                particle->setTotalParticles(300);
                particle->setAngle(75);
                particle->setAngleVar(4);
                particle->setLife(.5);
                particle->setLifeVar(0);
                particle->setStartSize(parent->getContentSize().width*.04);
                particle->setStartSizeVar(parent->getContentSize().width*.02);
                particle->setEndSize(parent->getContentSize().width*.05);
                particle->setEndSizeVar(parent->getContentSize().width*.02);
                
                parent->addChild(particle);
            });
            
            auto change_stain=Sequence::create(DelayTime::create(2),
                                               createChangeAction(1, 1, stain, hint), NULL);
            
            actions.pushBack(Spawn::create(shake_rabbit,water,change_stain, NULL));
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(createChangeAction(.5, .3, rabbit_1, rabbit_0));
            auto call=CallFunc::create([callback](){
                Common::playSE("pinpon.mp3");
                callback(true);
            });
            actions.pushBack(call);
            
            parent->runAction(Sequence::create(actions));
        }
#pragma mark シェフ+コイン
        else if (key == "coins") {
            Common::playSE("p.mp3");
            auto chef_0=parent->getChildByName("sup_35_chef.png");
            auto chef_1=createAnchorSprite("sup_35_chef_1.png", .42, .2, true, parent,true);
            auto chef_2=createAnchorSprite("sup_35_chef_2.png", .42, .2, true, parent,true);
            auto pos_chef_2=chef_2->getPosition();
            chef_2->setPositionY(-parent->getContentSize().height*.4);

            //give
            auto coins = createSpriteToCenter("sup_35_coins.png", true, parent, true);
            coins->setLocalZOrder(1);
            actions.pushBack(TargetedAction::create(coins, FadeIn::create(1)));
            actions.pushBack(TargetedAction::create(coins, Spawn::create(MoveBy::create(1, Vec2(0, parent->getContentSize().height*.1)),
                                                                         ScaleBy::create(1, .9), NULL)));
            actions.pushBack(DelayTime::create(.5));
            
            //change chef
            actions.pushBack(createChangeAction(1, .7, {chef_0,coins}, {chef_1}, "pop.mp3"));
            actions.pushBack(DelayTime::create(1));
            
            //jump
            actions.pushBack(createSoundAction("pig.mp3"));
            actions.pushBack(Spawn::create(createBounceAction(chef_1, .3, .05),
                                           jumpAction(Vec2(0, parent->getContentSize().height*.1), .15, chef_1, 1.5,2),
                                           NULL));
            actions.pushBack(DelayTime::create(1));
            
            //sink
            actions.pushBack(TargetedAction::create(chef_1, MoveBy::create(2, Vec2(0, -parent->getContentSize().height*.5))));
            actions.pushBack(createSoundAction("goshigoshi_short.mp3"));
            //re appear
            actions.pushBack(createChangeAction(1, 1, chef_1, chef_2));
            actions.pushBack(TargetedAction::create(chef_2, EaseOut::create(MoveTo::create(2, pos_chef_2), 1.5)));
            actions.pushBack(createSoundAction("pig.mp3"));
            actions.pushBack(DelayTime::create(2));
            
            //コールバック
            auto call=CallFunc::create([callback](){
                //Common::playSE("pinpon.mp3");
                callback(true);
            });
            actions.pushBack(call);
            
            parent->runAction(Sequence::create(actions));
        }
#pragma mark くじら
        else if (key.compare(0,5,"whale")==0) {
            auto index=atoi(key.substr(6,1).c_str());
            //木札配置
            auto whale=parent->getChildByName(StringUtils::format("sup_83_whale_%d.png",index));
            static auto scale=whale->getScale();
            
            actions.pushBack(TargetedAction::create(whale, Sequence::create(ScaleTo::create(.1, scale*.95, scale*1.05),
                                                                            ScaleTo::create(.1, scale),
                                                                            NULL)));
            //コールバック
            auto call=CallFunc::create([callback](){
                //Common::playSE("pinpon.mp3");
                callback(true);
            });
            actions.pushBack(call);
            
            parent->runAction(Spawn::create(actions));
        }
#pragma mark かぎ
        else if (key == "key") {
            Common::playSE("p.mp3");
            auto key_0=createSpriteToCenter("sup_46_key_0.png", true, parent,true);
            auto key_1=createSpriteToCenter("sup_46_key_1.png", true, parent,true);

            auto action=createChangeActionSequence(1, .7, {key_0,key_1}, {"kacha.mp3","key_1.mp3"});
            actions.pushBack(action);
            
            auto call=CallFunc::create([callback]{
                Common::playSE("pinpon.mp3");
                callback(true);
            });
            actions.pushBack(call);
            parent->runAction(Sequence::create(actions));
        }
#pragma mark かき氷
        else if (key == "iceHandle") {
            Common::playSE("p.mp3");
            
            //handle
            Vector<FiniteTimeAction*>handleActions;
            for (int i=0; i<9; i++) {
                auto fileName=StringUtils::format("sup_62_handle_%d.png",i%4);
                Node*handle=parent->getChildByName(fileName);
                if (!handle) {
                    handle=createSpriteToCenter(fileName, true, parent, true);
                }
                
                if (i==0) {
                    handleActions.pushBack(TargetedAction::create(handle, FadeIn::create(1)));
                    handleActions.pushBack(createSoundAction("kacha.mp3"));
                }
                else{
                    auto beforeNode=parent->getChildByName(StringUtils::format("sup_62_handle_%d.png",(i-1)%4));
                    handleActions.pushBack(createChangeAction(.5, .3, beforeNode, handle));
                    if (i%4==1) {
                        handleActions.pushBack(createSoundAction("gigigi.mp3"));
                    }
                }
                handleActions.pushBack(DelayTime::create(.3));
            }
            
            //ice
            auto clip=(ClippingNode*)createClippingNodeToCenter("sup_62_clip.png", parent);
            auto ice=createSpriteToCenter("sup_62_ice.png", false, parent);
            clip->addChild(ice);
            auto stencil=clip->getStencil();
            stencil->setPositionY(parent->getContentSize().height*.3);
            auto ice_seq=TargetedAction::create(stencil, Sequence::create(DelayTime::create(1),
                                                                          EaseIn::create(MoveTo::create(6, Vec2(parent->getContentSize().width*.5, parent->getContentSize().height*.5)), 1.5), NULL));
            
            
            //spawn
            auto handle_seq=Sequence::create(handleActions);
            actions.pushBack(Spawn::create(handle_seq,ice_seq, NULL));
            
            
            //gather penguins
            actions.pushBack(DelayTime::create(1));
            auto back_61=createSpriteToCenter("back_61.png", true, parent, true);
            createSpriteToCenter("sup_61_handle.png", false, back_61, true);
            createSpriteToCenter("sup_61_ice.png", false, back_61, true);

            actions.pushBack(TargetedAction::create(back_61, FadeIn::create(1)));
            actions.pushBack(DelayTime::create(1));
            
            Vector<FiniteTimeAction*>penguinsAction_0;
            Vector<FiniteTimeAction*>penguinsAction_1;

            for (int i=0; i<3; i++) {
                auto penguin=createSpriteToCenter(StringUtils::format("sup_61_penguin_%d_0.png",i), true, parent, true);
                auto penguin_1=createSpriteToCenter(StringUtils::format("sup_61_penguin_%d_1.png",i), true, parent, true);

                penguinsAction_0.pushBack(TargetedAction::create(penguin, FadeIn::create(.5)));
                if (i==0) {
                    penguinsAction_0.pushBack(createSoundAction("pop.mp3"));
                    penguinsAction_1.pushBack(createSoundAction("pop.mp3"));
                }
                
                penguinsAction_1.pushBack(createChangeAction(.7, .5, penguin, penguin_1));
            }
            
            actions.pushBack(Spawn::create(penguinsAction_0));
            actions.pushBack(DelayTime::create(.7));
            actions.pushBack(Spawn::create(penguinsAction_1));
            actions.pushBack(DelayTime::create(1));

            //コールバック
            auto call=CallFunc::create([callback]{
                Common::playSE("pinpon.mp3");
                callback(true);
            });
            actions.pushBack(call);
            
            parent->runAction(Sequence::create(actions));
        }
#pragma mark ドライバー
    else if (key == "driver") {
        auto lid=parent->getChildByName("sup_83_lid.png");
        //ドライバーカタカタ
        std::vector<Vec2> positions = {Vec2(.07, .5), Vec2(.92, .5)};
        auto driverAct = driverAction(DataManager::sharedManager()->getNowBack(), positions, parent, 2);
        actions.pushBack(driverAct);
        
        //remove lid
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(createSoundAction("kacha.mp3"));
        actions.pushBack(TargetedAction::create(lid, FadeOut::create(1)));
        actions.pushBack(DelayTime::create(1));
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark モアイ
    else if (key == "moai") {
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(moveMoai(parent, true));
        actions.pushBack(DelayTime::create(.5));

        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);

        parent->runAction(Sequence::create(actions));
    }
#pragma mark スパナ
    else if (key == "wrench") {
        Common::playSE("p.mp3");
        
        auto wrench=createAnchorSprite("sup_12_wrench.png", .75, .727, true, parent, true);
        wrench->setPositionX(parent->getContentSize().width*.5);
        
        auto nut=parent->getChildByName("sup_12_nut.png");
        
        //fadein_wrench
        actions.pushBack(TargetedAction::create(wrench, FadeIn::create(1)));
        
        //rotate
        auto rotate=Sequence::create(createSoundAction("twist.mp3"),
                                     EaseInOut::create(RotateBy::create(1, -40), 1.5),
                                     DelayTime::create(.5), NULL);
        auto rep=Repeat::create(rotate, 2);
        actions.pushBack(Spawn::create(TargetedAction::create(wrench, rep),
                                       TargetedAction::create(nut, rep->clone()),
                                       NULL));
        
        //fadeouts
        actions.pushBack(TargetedAction::create(wrench, FadeOut::create(.5)));
        actions.pushBack(TargetedAction::create(nut, Spawn::create(createSoundAction("kacha_1.mp3"),
                                                                   FadeOut::create(.5), NULL)));
        
        actions.pushBack(DelayTime::create(1));
        
        //change
        auto poster_0=parent->getChildByName("sup_12_poster_0.png");
        auto poster_1=createSpriteToCenter("sup_12_poster_1.png", true, parent,true);
        actions.pushBack(createChangeAction(1, .5, poster_0, poster_1,"page.mp3"));
        actions.pushBack(DelayTime::create(1));

        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark パイナップル装置ハンドル
    else if (key == "setPineHandle") {
        Common::playSE("p.mp3");
        
        //set handle
        auto item = createSpriteToCenter("sup_19_handle.png", true, parent, true);
        actions.pushBack(TargetedAction::create(item, FadeIn::create(1)));
        actions.pushBack(createSoundAction("kacha.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        //open
        auto open = createSpriteToCenter("sup_19_0_1.png", true, parent, true);
        actions.pushBack(createSoundAction("gacha.mp3"));
        actions.pushBack(TargetedAction::create(open, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark パイナップル始動
    else if (key == "startPine") {
        //replace
        auto back = createSpriteToCenter("back_19.png", true, parent, true);
        back->setLocalZOrder(1);
        SupplementsManager::getInstance()->addSupplementToMain(back, 19, true, ShowType_Normal);
        actions.pushBack(TargetedAction::create(back, FadeIn::create(1)));
        
        actions.pushBack(DelayTime::create(1));
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark パターン解除
    else if (key == "unlock") {
        //
        auto unlock = createSpriteToCenter("sup_52_unlock.png", true, parent, true);
        actions.pushBack(TargetedAction::create(unlock, Repeat::create(Sequence::create(FadeIn::create(.5),
                                                                                        FadeOut::create(.5), NULL), 2)));
        
        actions.pushBack(DelayTime::create(1));
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark ヒトデはめ
    else if (key == "starfish") {
        Common::playSE("p.mp3");
        //put
        auto index=DataManager::sharedManager()->getEnableFlagWithFlagID(53);
        auto item = createSpriteToCenter(StringUtils::format("sup_84_starfish_%d.png",index), true, parent, true);
        actions.pushBack(TargetedAction::create(item, FadeIn::create(1)));
        actions.pushBack(createSoundAction("kacha.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 出口ドラッグ終了
    else if (key == "door") {
        if (DataManager::sharedManager()->getTemporaryFlag("door").asBool()&&
            !DataManager::sharedManager()->isPlayingMinigame()) {
            //clear
            auto story=StoryLayer::create("ending1", []{
                EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

                auto story_1=StoryLayer::create("ending2", []{
                    Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
                });
                Director::getInstance()->getRunningScene()->addChild(story_1);
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        }
        else{
            callback(false);
        }
    }

}

#pragma mark - touch began
void BeachSideActionManager::touchBeganAction(std::string key, Node *parent)
{
    if (key=="door") {
        Common::stopAllSE();
        Common::playSE("gatagata.mp3");
        auto door=parent->getChildByName("sup_9_door.png");
        auto scale_original=parent->getContentSize().width/door->getContentSize().width;
        
        door->runAction(Sequence::create(ScaleTo::create(.1, scale_original*.995),
                                         ScaleTo::create(.1, scale_original)
                                         , NULL));
    }
}

#pragma mark - Drag
void BeachSideActionManager::dragAction(std::string key, Node *parent, Vec2 pos)
{
    if (key=="door"&&
        DataManager::sharedManager()->getEnableFlagWithFlagID(55)) {

        auto distance_x=TouchManager::getInstance()->touchStartPos.x-pos.x;
        
        if (!DataManager::sharedManager()->getTemporaryFlag("door").asBool()) {
            auto success=(distance_x>parent->getContentSize().width*.25);
            if (success) {
                DataManager::sharedManager()->setTemporaryFlag("door", 1);
                auto door=parent->getChildByName("sup_9_door.png");
                door->runAction(Spawn::create(createSoundAction("su.mp3"),
                                              EaseOut::create(MoveBy::create(.5, Vec2(-parent->getContentSize().width*.5, 0)), 1.5), NULL));
            }
        }
    }
}

#pragma mark - Item
void BeachSideActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造 addする際はcreateSpriteOnClipを使用するとよい
    Vector<FiniteTimeAction*>actions;
#pragma mark 水鉄砲
    if (key=="gun") {
        Common::playSE("p.mp3");
        
        auto tunk=createSpriteToCenter("gun_anim.png", true, parent->backImage, true);
        tunk->setPosition(parent->backImage->getContentSize().width*.32,parent->backImage->getContentSize().height*.44);
        createSpriteToCenter("gun_cover.png", false, parent->backImage,true);
        actions.pushBack(TargetedAction::create(tunk, FadeIn::create(1)));
        
        //move
        actions.pushBack(DelayTime::create(.7));
        actions.pushBack(TargetedAction::create(tunk, EaseInOut::create(MoveTo::create(1, parent->backImage->getContentSize()/2), 1.5)));
        actions.pushBack(createSoundAction("kacha.mp3"));
        
        //change
        auto gun=createSpriteToCenter("gun.png", true, parent->backImage,true);
        actions.pushBack(createChangeAction(1, .5, {parent->itemImage,tunk}, {gun}));
        //actions.pushBack(DelayTime::create(.7));
        
        //call
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 懸賞金box
    else if(key=="open_box"){
        auto box=createSpriteToCenter("box.png", true, parent->backImage,true);
        auto open=createSpriteToCenter("box_open.png", true, parent->backImage,true);

        //change
        actions.pushBack(createChangeAction(1, .7, parent->itemImage, box));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createChangeAction(1, .5, box, open,"kacha.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        //call
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
}


#pragma mark - Custom
void BeachSideActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    
}

#pragma mark - Private
void BeachSideActionManager::addShellParticle(Node *parent)
{
    auto size=parent->getContentSize().width*.08;
    auto particle = ParticleSystemQuad::create("kirakira.plist");
    particle->setAutoRemoveOnFinish(true);
    particle->setPosition(parent->getContentSize().width*.4,parent->getContentSize().height*.49);
    particle->setTotalParticles(12);
    particle->setStartSpinVar(10);
    particle->setLife(.5);
    particle->setLifeVar(.2);
    particle->setAngleVar(20);
    particle->setStartSize(size);
    particle->setStartSizeVar(0);
    particle->setEndColor(Color4F(0, 0, 0, .5));
    particle->setEndSize(particle->getStartSize()*1.2);
    particle->setEndSpin(15);
    particle->setEndSpinVar(15);
    particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
    particle->setLocalZOrder(1);
    parent->addChild(particle);
}

FiniteTimeAction* BeachSideActionManager::moveMoai(Node *parent, bool animate)
{
    auto moai=parent->getChildByName("sup_89_moai.png");
    auto distance=-parent->getContentSize().width*.25;
    if (animate) {
        return TargetedAction::create(moai, Spawn::create(createSoundAction("zuzuzu.mp3"),
                                                          EaseInOut::create(MoveBy::create(1, Vec2(distance, 0)), 1.5), NULL));
    }
    else{
        moai->setPositionX(moai->getPositionX()+distance);
        return NULL;
    }
}
