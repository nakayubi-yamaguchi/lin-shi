//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "TutorialActionManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "EscapeCollectionScene.h"
#include "Utils/FireBaseBridge.h"
#include "AnalyticsManager.h"
#include "GameScene.h"
#include "SupplementsManager.h"

using namespace cocos2d;

TutorialActionManager* TutorialActionManager::manager =NULL;

TutorialActionManager* TutorialActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new TutorialActionManager();
    }
    return manager;
}

#pragma mark - Back
void TutorialActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="tutorial_2")
    {
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(1))
        {
            if (DataManager::sharedManager()->getSelectedItemID()!=1) {
                //itemboxのいちに
                auto scene=Director::getInstance()->getRunningScene();
                createFinger(Vec2(DataManager::sharedManager()->getItemBoxWidth()*.4/scene->getContentSize().width, .87), parent,FingerDirection_Top);                }
            else{
                createFinger(Vec2(.35, .35), parent,FingerDirection_Top);
            }
        }
    }
    else if(key=="tutorial_3"||key=="tutorial_5")
    {
        if (key=="tutorial_5") {
            //移動用矢印を示す
            if (!DataManager::sharedManager()->getEnableFlagWithFlagID(4)&&
                DataManager::sharedManager()->getTemporaryFlag(key).asInt()==1) {
                createFinger(Vec2(.75, .15), parent, FingerDirection_Right);
            }
        }
        
        if(DataManager::sharedManager()->getTemporaryFlag(key).asInt()!=1){
            auto story=StoryLayer::create(key,[key,this,backID,parent](Ref*sender){
                DataManager::sharedManager()->setTemporaryFlag(key, 1);
                this->backAction(key, backID, parent);
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        }
    }
    else if(key=="back_1")
    {
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(1)&&
            DataManager::sharedManager()->isShowedOpeningStory()) {
            createFinger(Vec2(.7, .45), parent,FingerDirection_Right);
        }
    }
    else if(key=="back_8")
    {
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(4)&&
            DataManager::sharedManager()->getTemporaryFlag(key).asInt()==0) {
            createFinger(Vec2(.2, .15), parent,FingerDirection_Left);
            DataManager::sharedManager()->setTemporaryFlag(key, 1);
        }
    }
    else if(key=="changeSlectedItemID")
    {
        if (backID==2) {
            auto finger=parent->getChildByName("sup_finger.png");
            if (finger) {
                finger->removeFromParent();
            }
            backAction("tutorial_2",backID, parent);
        }
    }
}

#pragma mark - Touch
void TutorialActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
#pragma mark マーカー取得
    if(key=="getMarker")
    {
        callback(true);
    }
#pragma mark マーカー使用
    else if(key=="marker")
    {
        Common::playSE("p.mp3");
        //give
        auto stirrer=createSpriteToCenter("anim_7_marker.png", true, parent);
        stirrer->setLocalZOrder(1);
        parent->addChild(stirrer);
        
        //mix
        auto pig=createSpriteToCenter("anim_7_0.png", true, parent);
        pig->setLocalZOrder(1);
        parent->addChild(pig);
        
        Vector<SpriteFrame*>frames;
        for(int i=0;i<2;i++){
            auto s=Sprite::createWithSpriteFrameName(StringUtils::format("anim_7_%d.png",i));
            frames.pushBack(s->getSpriteFrame());
        }
        
        auto animation=Animation::createWithSpriteFrames(frames);
        animation->setDelayPerUnit(4.0f/60.0f);//??フレームぶん表示
        animation->setRestoreOriginalFrame(true);
        animation->setLoops(20);
        
        //finish
        auto pig_1=createSpriteToCenter("sup_7_pig_1.png", true, parent);
        pig_1->setLocalZOrder(2);
        parent->addChild(pig_1);
        
        //animation
        auto seq_stirrer=TargetedAction::create(stirrer, Sequence::create(Spawn::create(FadeIn::create(.5),ScaleBy::create(1, .9), NULL),DelayTime::create(.5),FadeOut::create(.5),
                                                                          DelayTime::create(.2),NULL));
        auto fadeout=TargetedAction::create(parent->getChildByName("sup_7_pig.png"), FadeOut::create(.7));
        auto mix=Spawn::create(fadeout,
                               TargetedAction::create(pig, FadeIn::create(.5)),
                               TargetedAction::create(pig, Animate::create(animation)),
                               createSoundAction("pen.mp3"),
                               NULL);
        
        auto change=Spawn::create(TargetedAction::create(pig, Sequence::create(FadeOut::create(.7), NULL)),
                                  TargetedAction::create(pig_1, FadeIn::create(.5)),
                                  createSoundAction("pinpon.mp3"), NULL);
        
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_stirrer,mix,change,call,NULL));
    }
    else if(key=="pig"){
        Common::playSE("pig.mp3");
        auto pig=parent->getChildByName("sup_7_pig.png");
        
        auto cloud=AnchorSprite::create(Vec2(.625, .715), parent->getContentSize(), StringUtils::format("anim_7_cloud.png"));
        cloud->setOpacity(0);
        cloud->setLocalZOrder(1);
        parent->addChild(cloud);
        
        auto original_scale=cloud->getScale();
        cloud->setScale(.1);
        
        auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(.5, original_scale),
                                                                                    FadeIn::create(.5), NULL),
                                                                      createSoundAction("nyu.mp3"),
                                                                      DelayTime::create(.5),
                                                                      Spawn::create(ScaleTo::create(.5, .1),
                                                                                    FadeOut::create(.5), NULL), NULL));
        
        auto seq_pig=TargetedAction::create(pig, Sequence::create(ScaleTo::create(.3, 1.05, .95),
                                                                  ScaleTo::create(.3, .975, 1.025),
                                                                  ScaleTo::create(.2, original_scale), NULL));
        
        auto spawn=Spawn::create(seq_cloud,seq_pig, NULL);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,call, NULL));
    }
    else if(key=="clear")
    {
        auto back_5=createSpriteToCenter("back_5.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back_5, 5, true);
        SupplementsManager::getInstance()->customBackAction(back_5, 5, DataManager::sharedManager()->getBackData(5)["customActionKey"].asString(),ShowType_Normal);
        
        auto door=createSpriteToCenter("sup_5_open.png", true, back_5,true);
        auto fadein_back=TargetedAction::create(back_5, FadeIn::create(1));
        auto open_door=TargetedAction::create(door, Spawn::create(createSoundAction("gacha.mp3"),
                                                                  FadeIn::create(.5), NULL));
        
        auto call=CallFunc::create([]{
            auto story=StoryLayer::create("ending",[](Ref*sender){
                Common::stopBgm();
                Common::playBGM(DataManager::sharedManager()->getBgmName(true).c_str());
                Director::getInstance()->replaceScene(TransitionFade::create(3, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        parent->runAction(Sequence::create(DelayTime::create(.5),fadein_back,DelayTime::create(.5),open_door,call, NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark - Arrow
void TutorialActionManager::arrowAction(std::string key, Node *parent, const onFinished &callback)
{
    if (key=="pushArrow") {
        auto finger=parent->getChildByName("sup_finger.png");
        if (finger) {
            finger->runAction(FadeOut::create(.05));
        }
    }
    
    callback(true);
}

#pragma mark - Item
void TutorialActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{
    
}


#pragma mark - Private
void TutorialActionManager::createFinger(Vec2 position, Node *parent, FingerDirection direction)
{
    auto finger=createSpriteToCenter("sup_finger.png", true, parent);
    finger->setPosition(parent->getContentSize().width*position.x, parent->getContentSize().height*position.y);
    parent->addChild(finger);
    
    auto distanceX=parent->getContentSize().height*.05;
    auto distanceY=distanceX;
    
    if (direction==FingerDirection_Top) {
        finger->setScaleY(-finger->getScale());
        distanceY*=-1;
    }
    else if (direction==FingerDirection_Left) {
        finger->setScaleX(-finger->getScale());
        distanceX*=-1;
    }
    
    
    
    auto fadein=FadeIn::create(.2);
    auto move=EaseInOut::create(MoveBy::create(1, Vec2(distanceX, -distanceY)), 1.5);
    auto move_1=EaseInOut::create(MoveBy::create(1, Vec2(-distanceX, distanceY)), 1.5);

    finger->runAction(Repeat::create(Sequence::create(fadein,move,move_1, NULL), UINT_MAX));
}
