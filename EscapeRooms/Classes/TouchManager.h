//
//  TouchManager.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/07/12.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________TouchManager_____
#define _____PROJECTNAMEASIDENTIFIER________TouchManager_____

#include "cocos2d.h"
#include "SideBarLayer.h"

typedef std::function<void(bool success)>onFinished;

typedef enum{
    TouchType_False,
    TouchType_True,
    TouchType_Continue
}TouchType;

USING_NS_CC;
class TouchManager
{
public:
    static TouchManager* manager;
    static TouchManager* getInstance();
    
    void customAction(std::string key,Node*parent,bool onItem,const onFinished&callback);
    
    Vec2 touchStartPos;
    
    /**タッチ不可判定*/
    bool checkTouchEnable(ValueMap map);
    /**タッチ座標が含まれているか判定*/
    bool isContained(Node*parent,ValueMap map,Vec2 pos);
    /**処理対象のmap touchbeganで保持*/
    ValueMap touchMap;
    /**処理対象のmap touchbeganで保持*/
    ValueMap priorityWarpMap;
    /**タッチmapをリセット 新しいタッチ情報処理前に呼び出す*/
    void resetTouchInfo();
    /**タッチ、shake後に呼び出し touchが可能ならtouchMapを保持する.endで処理*/
    TouchType manageTouchStart(ValueMap map,Touch*touch,Node*parent,bool onItem);
    /**drag処理 began or moved  タッチ開始からの呼び出しかどうかで分岐する*/
    void dragAction(bool isInBegan,Touch*touch,Node*parent,bool onItem);
    
    //touchEndでの処理中判定 shakeとの衝突回避
    CC_SYNTHESIZE(bool, isManagingTouch, IsManagingTouch);
private:
    LayerColor* createDisableLayer();
    void disableAction(ValueMap map);//タッチ不能時のSE
    void showSideBar(SideBarType type);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
