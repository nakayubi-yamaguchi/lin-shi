//
//  EscapeLoadingScene.h
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/09/30.
//
//

#ifndef _____EscapeContainer________EscapeLoadingScene_____
#define _____EscapeContainer________EscapeLoadingScene_____

#include "cocos2d.h"
#include "UIParts/NCIndicator.h"

typedef enum {
    LoadingType_Icons,//アイコン(起動->コレクション)をローディングする
    LoadingType_Titles,//タイトル(コレクション->タイトル)をローディングする
    LoadingType_RemoveCathe,//キャッシュを削除する。/コレクションシーンに戻るとき。
    LoadingType_AllLoadTitle//全てのタイトルデータをローディングする。/起動時。

}LoadingType;

USING_NS_CC;

class EscapeLoadingScene : public cocos2d::LayerColor
{
public:
    static cocos2d::Scene *createScene(std::vector<LoadingType> typeVector);
    static cocos2d::Scene *createScene(LoadingType type);
    static EscapeLoadingScene* create(std::vector<LoadingType> typeVector);
    virtual bool init(std::vector<LoadingType> typeVector);
    void onEnterTransitionDidFinish();

    std::vector<LoadingType> getLoadingTypes();
    LoadingType getLoadingType();

private:
    std::vector<LoadingType> m_loadingTypeVector;
    int m_performNum;//loadingTypeの何番目の処理をしているか。

    //LoadingType m_loadingType;
    
    NCIndicator *indicator;
    Label* conditionLabel;
    Label* progressLabel;
    Label* talkLabel;

    void createMain();
    void showThumbImages();
    void showPanelAd();
    void startLoadingTask();
    void addNotification();
    
    /**Remote config*/
    void fetchABTests();
    
    /**userID生成*/
    void createUserID();
    
    /**全てタイトルダウンロード時のみ使用*/
    void transitionOnAllTitleDL();
    
    /**ローディングタイプが最後のやつなら、引数コールバックの処理をする。*/
    void checkLoadingTypePerform(const std::function<void()> &onLastLoadingTypeCallback);
    
    ~EscapeLoadingScene();
};

#endif /* defined(_____EscapeContainer________EscapeLoadingScene_____) */
