//
//  StarReviewLayer.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/08.
//
//

#include "StarReviewLayer.h"
#include "Utils/Common.h"
#include "EscapeDataManager.h"
#include "Utils/FireBaseBridge.h"
#include "DataManager.h"

#include <string>

using namespace cocos2d;

bool StarReviewLayer::init() {
    if (!LayerColor::initWithColor(Color4B(0, 0, 0, 0))) {
        return false;
    }
    
    popup_size=Size(getContentSize().width*.8, getContentSize().width*.6);
    title_height=popup_size.height*.4;
    buttons_height=popup_size.height*.2;
    setIsAnimating(true);
    
    createMain();
    createListener();
    
    return true;
}

#pragma mark- タッチ
void StarReviewLayer::createListener()
{//タッチ透過させない
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [this](Touch *touch,Event*event)->bool{
        //タッチ座標を計算
        //thisを基準にしたtouchpointを取得
        auto pos=Vec2(touch->getLocation().x, touch->getLocation().y);
        Rect rect=Rect((getContentSize().width-popup_size.width)/2,(getContentSize().height-popup_size.height)/2+buttons_height, popup_size.width, popup_size.height-buttons_height-title_height);
        
        if (rect.containsPoint(pos)&&!getIsAnimating())
        {//星の領域をタップ
            //posによってタップされた星を判定
            auto distance=pos.x-(getContentSize().width/2-(starsSprite->starWidth*2.5+starsSprite->starSpace*2));
            auto rate=distance/(starsSprite->getContentSize().width/5);
            rate=ceil(rate);
            rate=MAX(rate, 1);
            rate=MIN(5, rate);
            starsSprite->setRate(rate,true);
        }
        
        return true;
    };
    auto dip = Director::getInstance()->getEventDispatcher();
    dip->addEventListenerWithSceneGraphPriority(listener, this);
}

#pragma mark - UI
void StarReviewLayer::createMain()
{
    popup=Sprite::create();
    popup->setColor(Color3B(230, 230, 230));
    popup->setTextureRect(Rect(0, 0, popup_size.width, popup_size.height));
    popup->setPosition(getContentSize()/2);
    popup->setCascadeOpacityEnabled(true);
    addChild(popup);
    
    //title
    titleLabel=Label::createWithTTF(Common::localize("Congraturations!\nPlease rate this stage!", "おめでとうございます！\nよろしければ評価を入力してください！"), MyFont,title_height*.2);
    titleLabel->setHeight(title_height);
    titleLabel ->setColor(Color3B::BLACK);
    titleLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    titleLabel->setWidth(popup_size.width*.8);
    titleLabel->setPosition(popup_size.width*.5,popup_size.height-title_height/2);
    popup->addChild(titleLabel);
    
    //star
    starsSprite = StarsSprite::create(popup_size.width*.8);
    starsSprite->setPosition(popup->getContentSize().width*.5,popup->getContentSize().height-title_height-starsSprite->getContentSize().height/2);
    
    //過去の自分の評価を表示。
    auto pastReview = DataManager::sharedManager()->getEternalRecordDataOnPlay(RecordKey_Review).asInt();
    starsSprite->setRate(pastReview);
    
    popup->addChild(starsSprite);
    
    //buttons
    std::vector<std::string>texts={"Cancel","Submit"};
    Vector<MenuItem*>menuItems;
    int counter=0;
    for (auto text : texts) {
        auto label=Label::createWithTTF(text, MyFont, buttons_height*.5);
        label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        label->setWidth(popup_size.width*.5);
        label->setTextColor(Color4B::BLACK);

        auto menuitem=MenuItemLabel::create(label, [this](Ref*ref){
            auto _ref=(MenuItem*)ref;
            this->pushMenuButton(_ref->getTag());
        });
        menuitem->setTag(counter);
        menuItems.pushBack(menuitem);
    
        counter++;
    }
    auto menu=Menu::createWithArray(menuItems);
    menu->alignItemsHorizontallyWithPadding(0);
    menu->setPosition(popup->getContentSize().width/2,buttons_height/2);
    popup->addChild(menu);
    
    //アニメーション
    popup->setScale(.8);
    auto ease = TargetedAction::create(popup, EaseOut::create(ScaleTo::create(.15, 1.15), 2));
    auto ease2 = TargetedAction::create(popup, EaseOut::create(ScaleTo::create(.1, 1), 2));
    auto selfFadein = FadeTo::create(.15, 80);
    auto spawn = Spawn::create(selfFadein, ease2, NULL);//背景がフェードインしながら、元のサイズに戻る
    auto call = CallFunc::create([this]{
        this->setIsAnimating(false);
    });
    runAction(Sequence::create(ease, spawn, call, NULL));
}

#pragma mark - ボタンプッシュ
void StarReviewLayer::pushMenuButton(int index)
{
    if (getIsAnimating()) {
        return;
    }
    
    if (index==1)
    {//submitをプッシュ
        if (starsSprite->getRate() == 0) {//レビューしていない。
            setIsAnimating(true);
            auto originalColor=titleLabel->getColor();
            titleLabel->setColor(Color3B::RED);
            auto distance=popup->getContentSize().width*.05;
            auto shake=TargetedAction::create(popup, Sequence::create(MoveBy::create(.1, Vec2(distance, 0)),
                                                                      MoveBy::create(.1, Vec2(-distance*1.5, 0)),
                                                                      MoveBy::create(.1, Vec2(distance*.5, 0)), NULL));
            auto color=CallFunc::create([this,originalColor]{
                titleLabel->setColor(originalColor);
            });
            auto color_1=CallFunc::create([this]{
                titleLabel->setColor(Color3B::RED);
            });
            auto label_action=TargetedAction::create(titleLabel, Sequence::create(DelayTime::create(.3),color,DelayTime::create(.3),color_1,DelayTime::create(.3),color->clone(), NULL));
            
            auto spawn=Spawn::create(shake,label_action ,NULL);
            auto call=CallFunc::create([this]{
                this->setIsAnimating(false);
            });
            runAction(Sequence::create(spawn,call, NULL));
        }
        else {//レビューを受け付ける。
            auto pastReview = DataManager::sharedManager()->getEternalRecordDataOnPlay(RecordKey_Review).asInt();
            
            auto titleName = EscapeDataManager::getInstance()->getPlayTitleName();
            std::transform(titleName.begin(), titleName.begin()+1, titleName.begin(), ::toupper);
            
            int nowReview = (int)starsSprite->getRate();
            
            log("かつてのレビューは%d,,,最新のレビューは%d", pastReview, nowReview);
            if (pastReview != nowReview) {//レビューを更新していたら
                auto fbDir = StringUtils::format("%s%s/%s/",FirebaseDownloadDir, Stars, titleName.c_str());
                
                auto pastStars = StringUtils::format("%d%s",pastReview, Stars);
                auto nowStars = StringUtils::format("%d%s",nowReview, Stars);
                
                FireBaseBridge::getInstance()->transactionPost(fbDir, {nowStars}, {pastStars}, [nowReview](bool successed){
                    log("トランザクションは終わりです。%d",successed);
                    DataManager::sharedManager()->recordEternalDataOnPlay(RecordKey_Review, Value(nowReview));
                });
            }
            close();
        }
    }
    else{
        close();
    }
}

void StarReviewLayer::close()
{
    setIsAnimating(true);
    
    //フェードアウトしながら、小さくなる。
    float duration = .15;
    auto fadeout = FadeOut::create(duration) ;
    auto mainFadeout = TargetedAction::create(popup, Spawn::create(FadeOut::create(duration), ScaleTo::create(duration, .85), NULL));
    
    auto starsFadeoudAction = starsSprite->getFadeoutAction(duration);
        
    auto spawnFadeout = Spawn::create(fadeout, mainFadeout, starsFadeoudAction, NULL);
    auto delay = DelayTime::create(.1);//少し空いていた方が自然に見える。
    auto callback = CallFunc::create([this](){//フェードアウトが完了してから、コールバックを返す。
        if (mDelegate) {
            mDelegate->didClosedStarsReviewLayer();
        }
    });
    auto remove = RemoveSelf::create();
    runAction(Sequence::create(spawnFadeout, callback, delay, remove, NULL));
}
