//
//  ClearScene.h
//  Kebab
//
//  Created by yamaguchinarita on 2016/12/22.
//
//

#ifndef __Kebab__ClearScene__
#define __Kebab__ClearScene__

#include "cocos2d.h"
#include "StarReviewLayer.h"

USING_NS_CC;

class ClearScene : public cocos2d::LayerColor,StarsReviewLayerDelegate
{
public:
    virtual bool init();
    void onEnterTransitionDidFinish();
    static Scene *createScene();
    CREATE_FUNC(ClearScene);
    
    ~ClearScene();
    
private:
    /**firebaseにクリアデータを送信*/
    void sendClearData();
    
    void createSuccessObject();
    void createParticle();
    void createNextButton();
    void createShareMenu();
    
    void showAlertBeforeReviewLayer();
    void showStarReviewLayer();
    void displayReviewLayer();
    
    void didClosedStarsReviewLayer();
};

#endif /* defined(__Kebab__ClearScene__) */
