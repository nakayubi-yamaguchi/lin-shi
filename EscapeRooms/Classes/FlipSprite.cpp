//
//  FlipSprite.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2019/02/01.
//
//

#include "FlipSprite.h"
#include "DataManager.h"

std::string FlipSprite::getSerchName(std::string name)
{
    return "flip_" + name;
}

FlipSprite* FlipSprite::create(std::string fileName, Vec2 anchorPoint, Node* parent)
{
    auto node=new FlipSprite();
    if (node&&node->init(fileName, anchorPoint, parent)) {
        node->autorelease();
        return node;
    }
    CC_SAFE_RELEASE(node);
    
    return node;
}

bool FlipSprite::init(std::string fileName, Vec2 anchorPoint, Node* parent)
{
    if (!Sprite::init()) {
        return false;
    }
    
    setCascadeOpacityEnabled(true);
    
    setName(getSerchName(fileName));//この名前でparentから検索。temporarykeyもこれ。
    Common::performProcessForDebug([this](){
        log("フリップアクションのテンポラリキー%s:Value:%d", getName().c_str(), DataManager::sharedManager()->getTemporaryFlag(getName().c_str()).asBool());
    }, nullptr);
    setBaseFileName(fileName);//例：sup_79_0 画像ファイル名作成用

    bool temporaryFlipFlag = DataManager::sharedManager()->getTemporaryFlag(getName()).asBool();
    m_isFlipped = temporaryFlipFlag;
    
    setFileAnchorPoint(anchorPoint);
   
    createCard(parent);
    
    return true;
}

#pragma mark ゲッター
bool FlipSprite::getIsFlipped()
{
    return m_isFlipped;
}

#pragma mark プライベート
void FlipSprite::createCard(Node *parent)
{
    //0が手前の画像
    auto frontName = getBaseFileName() + "_0";
    auto frontFileName = frontName+ ".png";
    auto backName = getBaseFileName() + "_1";
    auto backFileName = backName + ".png";
    log(":::::::フロント名%s::バック名:%s", frontName.c_str(), backName.c_str());

    auto createAnchorSprite = [parent, this](std::string fileName, bool invisible)->AnchorSprite*{
        auto spr = AnchorSprite::create(getFileAnchorPoint(), parent->getContentSize(), fileName);
        spr->setCascadeOpacityEnabled(true);
        
        log("%sインビジフリッション:Value:%d",fileName.c_str(), invisible);

        if (invisible) {
            spr->setOpacity(0);
        }
        
        return spr;
    };
    
    auto frontSp = createAnchorSprite(frontFileName, getIsFlipped());
    frontSp->setName(frontName);
    addChild(frontSp);
    flipSps.pushBack(frontSp);
    
    auto backSp = createAnchorSprite(backFileName, !getIsFlipped());
    backSp->setName(backName);
    addChild(backSp);
    flipSps.pushBack(backSp);
}

#pragma mark パブリック
FiniteTimeAction* FlipSprite::getFlipAction(float flipDuration)
{
    Vector<FiniteTimeAction*> actions;
    //frontspriteを表示回転・
    auto fadein = FadeIn::create(0);
    auto fadeout = FadeOut::create(0);
    auto camera = OrbitCamera::create(flipDuration/2, 1, 0, 0, 90, 0, 0);
    
    auto nowFrontSp = flipSps.at(m_isFlipped);
    auto frontSeq = TargetedAction::create(nowFrontSp, Sequence::create(camera, fadeout, NULL));
    actions.pushBack(frontSeq);
    
    auto camera_ = OrbitCamera::create(flipDuration/2, 1, 0, 270, 90, 0, 0);
    auto nowBackSp = flipSps.at(!m_isFlipped);
    auto backSeq = TargetedAction::create(nowBackSp, Sequence::create(fadein, camera_, NULL));
    actions.pushBack(backSeq);
    
    m_isFlipped = !m_isFlipped;
    DataManager::sharedManager()->setTemporaryFlag(getName(), m_isFlipped);
    
    return Sequence::create(actions);
}

FiniteTimeAction* FlipSprite::getActionToFlips(FiniteTimeAction *action)
{
    Vector<FiniteTimeAction*> spawnActions;
    
    for (auto flipSp : flipSps) {
        spawnActions.pushBack(TargetedAction::create(flipSp, action));
    }
    
    return Spawn::create(spawnActions);
}
