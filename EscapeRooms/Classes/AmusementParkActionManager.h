//
//  ChocolateShopActionManager.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/11/29.
//
//

#ifndef __EscapeContainer__AmusementParkActionManager__
#define __EscapeContainer__AmusementParkActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

typedef enum{
    WheelLightType_Up=0,
    WheelLightType_Down,
    WheelLightType_None
}WheelLightType;

class AmusementParkActionManager:public CustomActionManager
{    
public:
    static AmusementParkActionManager* manager;
    static AmusementParkActionManager* getInstance();
   
    //over ride
    int getDefaultBackNum();
    void backAction(std::string key,int backID,Node*parent,ShowType type);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void touchBeganAction(std::string key,Node*parent);
    
    void dragAction(std::string key,Node*parent,Vec2 pos);
    
    void itemBackAction(std::string key,PopUpLayer*parent);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    
    void customAction(std::string key,Node*parent);
    
    void showAnswerAction(std::string key, Node *parent, ShowType showType);
    void cameraShotAction(int backID);
    
private:
    FiniteTimeAction* wheelAction(Node*parent,bool isPink,bool stopAnimate,WheelLightType lightType,int index);
    
    FiniteTimeAction* bananaAction(Node *parent, int index, ShowType type);
    
    /**10秒ごとにジェットコースター。リピート*/
    FiniteTimeAction* jetcoasterActionForBack(FiniteTimeAction* jetcoasterAction);
    FiniteTimeAction* jetcoasterActionForExit(int backID, Node *parent);
    FiniteTimeAction* jetcoasterActionForBalloon(int backID, Node *parent, bool fromBackAction);
    FiniteTimeAction* jetcoasterActionForCircus(int backID, Node *parent, bool fromBackAction);
    FiniteTimeAction* jetcoasterActionForPlatform(int backID, Node *parent, bool fromBackAction);
    FiniteTimeAction* createJetSpawn(Sprite* targetSprite, Node* parent, float jetDuration, float scaleS, Vec2 jetPos, float easeRate);
    
    /**花火を表示する。*/
    void showHanabi(Node* clip, Node* parent ,int index, Color4F color, Vec2 startPos, Vec2 endPos);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
