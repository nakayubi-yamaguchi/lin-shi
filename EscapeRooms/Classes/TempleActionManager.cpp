//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "TempleActionManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"

using namespace cocos2d;

TempleActionManager* TempleActionManager::manager =NULL;

TempleActionManager* TempleActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new TempleActionManager();
    }
    return manager;
}

#pragma mark - Back
void TempleActionManager::backAction(std::string key,int backID, Node *parent)
{
#pragma mark 猿
    if (key=="monkey") {
        auto isReady=DataManager::sharedManager()->getEnableFlagWithFlagID(13);
        
        
        auto arm_green=AnchorSprite::create(Vec2(.64, .58), parent->getContentSize(), "sup_33_green_arm.png");
        parent->addChild(arm_green);
        
        auto body_green=Sprite::createWithSpriteFrameName("sup_33_green_body.png");
        body_green->setPosition(parent->getContentSize()/2);
        parent->addChild(body_green);
        
        auto wood_green=parent->getChildByName<AnchorSprite*>("sup_33_green_wood.png");
        auto wood_red=parent->getChildByName<AnchorSprite*>("sup_33_red_wood.png");

        //
        auto angle=60;
        Vector<FiniteTimeAction*>actions;
        
        //midori action
        auto rotate_green_0=TargetedAction::create(arm_green, EaseIn::create(RotateBy::create(.15, -angle),1.5));
        auto sound=CallFunc::create([]{
            Common::playSE("poku.mp3");
        });
        auto shake_green_wood=TargetedAction::create(wood_green, Sequence::create(ScaleBy::create(.1, 1.05,.95),ScaleBy::create(.1,1/1.05, 1/.95), NULL));
        auto rotate_green_1=TargetedAction::create(arm_green, EaseOut::create(RotateBy::create(.15, angle),1.5));
        auto action_green=Sequence::create(rotate_green_0,sound,Spawn::create(shake_green_wood,rotate_green_1, NULL),DelayTime::create(1), NULL);
        
        //分岐
        if (isReady) {
            auto arm_red=AnchorSprite::create(Vec2(.7, .45), parent->getContentSize(), "sup_33_red_arm.png");
            parent->addChild(arm_red);
            
            //red action
            auto rotate_red_0=TargetedAction::create(arm_red, EaseIn::create(RotateBy::create(.15, -angle),1.5));
            auto shake_red_wood=TargetedAction::create(wood_red, Sequence::create(ScaleBy::create(.1, 1.05,.95),ScaleBy::create(.1,1/1.05, 1/.95), NULL));
            auto rotate_red_1=TargetedAction::create(arm_red, EaseOut::create(RotateBy::create(.15, angle),1.5));
            auto action_red=Sequence::create(rotate_red_0,sound->clone(),Spawn::create(shake_red_wood,rotate_red_1, NULL),DelayTime::create(1), NULL);
            
            actions.pushBack(action_green);
            actions.pushBack(action_red);
            actions.pushBack(action_green);
            actions.pushBack(action_red->clone());
            actions.pushBack(action_red->clone());
            actions.pushBack(action_red->clone());
            actions.pushBack(DelayTime::create(1));

        }
        else{
            auto arm_red=Sprite::createWithSpriteFrameName("sup_33_red_arm_none.png");
            arm_red->setPosition(parent->getContentSize()/2);
            parent->addChild(arm_red);
            
            actions.pushBack(action_green);
        }
        
        auto body_red=Sprite::createWithSpriteFrameName("sup_33_red_body.png");
        body_red->setPosition(parent->getContentSize()/2);
        parent->addChild(body_red);
        
        if(isReady){
            parent->runAction(Repeat::create(Sequence::create(actions),UINT_MAX));
        }
    }
    else if (key=="light"){
        Vector<Sprite*>lights;
        for (int i=0; i<4; i++) {
            auto spr=Sprite::createWithSpriteFrameName(StringUtils::format("sup_37_%d.png",i));
            spr->setPosition(parent->getContentSize()/2);
            spr->setOpacity(0);
            parent->addChild(spr);
            
            lights.pushBack(spr);
        }
        
        auto flash=Sequence::create(EaseOut::create(FadeIn::create(.2), 2),FadeOut::create(.2), NULL);
        auto flash_0=TargetedAction::create(lights.at(0), flash);
        auto flash_1=TargetedAction::create(lights.at(1), flash->clone());
        auto flash_2=TargetedAction::create(lights.at(2), flash->clone());
        auto flash_3=TargetedAction::create(lights.at(3), flash->clone());

        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),flash_1,flash_2,flash_0,flash_3,flash_3->clone(),flash_0->clone(), NULL), UINT_MAX));
    }
    else if(key=="mouse")
    {
        Vector<FiniteTimeAction*>actions;
        Vector<FiniteTimeAction*>actions_1;

        std::vector<int>nums={0,1,2,0,2,1};
        float count=0;
        float duration=3;
        float jump_count=10;
        float jump_height=parent->getContentSize().height*.04;
        for (auto num:nums) {
            auto spr=AnchorSprite::create(Vec2(.5, .33), parent->getContentSize(), StringUtils::format("sup_27_%d.png",num));
            spr->setPosition(parent->getContentSize().width,spr->getPosition().y);
            parent->addChild(spr);
            
            auto delay=DelayTime::create(1.5+.25*count);
            auto sound=CallFunc::create([count]{
                if (count==2) {
                    Common::playSE("nyu.mp3");
                }
            });
            auto move=JumpTo::create(duration, Vec2(0, spr->getPosition().y),jump_height, jump_count);
            auto scale_0=Repeat::create(Sequence::create(ScaleBy::create(duration/jump_count/2,1.1,.9),
                                                         ScaleBy::create(duration/jump_count/2,1/1.1,1/.9),
                                                         NULL), jump_count);
            auto seq=TargetedAction::create(spr, Sequence::create(delay,sound,Spawn::create(scale_0,move ,NULL), NULL));
            actions.pushBack(seq);
            
            auto scale=ScaleBy::create(0, -1, 1);
            auto move_1=JumpTo::create(duration, Vec2(parent->getContentSize().width, spr->getPosition().y),jump_height, jump_count);
            auto seq_1=TargetedAction::create(spr, Sequence::create(delay->clone(),sound->clone(),scale,Spawn::create(move_1,scale_0->clone(), NULL),scale->clone(), NULL));
            actions_1.pushBack(seq_1);
            
            count++;
        }
        
        auto rep=Repeat::create(Sequence::create(Spawn::create(actions),Spawn::create(actions_1), NULL), UINT_MAX);
        
        parent->runAction(rep);
        
        //cover
        auto cover=Sprite::createWithSpriteFrameName("sup_27_cover.png");
        cover->setPosition(parent->getContentSize()/2);
        parent->addChild(cover);
    }
    else if(key=="tanuki"){
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(33)) {
            auto tanuki=parent->getChildByName<AnchorSprite*>("sup_5_tanuki.png");
            tanuki->setScale(.5);
        }
    }
    else if(key=="clear"){
        auto storyLayer = StoryLayer::create(StoryKey_Ending, [this](Ref* sen){
            
            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

            auto story1=StoryLayer::create(StoryKey_Ending2, [this](Ref*ref){
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story1);
        });
        Director::getInstance()->getRunningScene()->addChild(storyLayer);
    }
    else if(key=="back_1"){
        //バグ修正
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(6)&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(7)) {
            //ぞうきんを持っていなければ
            auto find_itr=find(DataManager::sharedManager()->items.begin(), DataManager::sharedManager()->items.end(), 6);
            bool found= find_itr!=DataManager::sharedManager()->items.end();
            
            if (!found) {
                log("フラグ6を消去します");
                DataManager::sharedManager()->flagMap["6"]=false;
            }
        }
    }
}

#pragma mark - Touch
void TempleActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="match")
    {
        Common::playSE("huo.mp3");
        
        auto particle=ParticleSystemQuad::create("fire.plist");
        particle->setAutoRemoveOnFinish(true);
        particle->setPosition(parent->getContentSize().width/2,parent->getContentSize().height*.85);
        particle->setStartSize(parent->getContentSize().width*.04);
        parent->addChild(particle);
        
        auto shelf=SupplementsManager::getInstance()->switchSprites.at(0);
        
        auto delay=DelayTime::create(2);
        auto call=CallFunc::create([callback,shelf]{
            Common::playSE("pinpon.mp3");

            shelf->switching([](Ref*ref){});
            callback(true);
        });
        
        parent->runAction(Sequence::create(delay,call, NULL));
    }
    else if(key=="towel"){
        Common::playSE("p.mp3");
        
        auto towel=Sprite::createWithSpriteFrameName("anim_23_0.png");
        towel->setPosition(parent->getContentSize()/2);
        towel->setOpacity(0);
        parent->addChild(towel);
        
        auto fadein=FadeIn::create(.7
                                   );
        auto sound=CallFunc::create([]{
            Common::playSE("goshigoshi.mp3");
        });
        auto distance_y=parent->getContentSize().height*.1;
        auto moves=Spawn::create(MoveBy::create(2, Vec2(parent->getContentSize().width*.1, 0)),
                                 Sequence::create(MoveBy::create(.25, Vec2(0,distance_y/2)),
                                                  MoveBy::create(.25, Vec2(0, -distance_y)),
                                                  Repeat::create(Sequence::create(MoveBy::create(.25, Vec2(0, distance_y)),
                                                                                  MoveBy::create(.25, Vec2(0, -distance_y)), NULL), 3)
                                                  
                                                  , NULL)
                                 , NULL);
        
        auto seq_towel=TargetedAction::create(towel, Sequence::create(fadein,sound,moves,FadeOut::create(.3), NULL));
        
        //hint appear
        auto stain=parent->getChildByName<Sprite*>("sup_23_stain.png");
        auto hint=Sprite::createWithSpriteFrameName("sup_23_hint.png");
        hint->setPosition(parent->getContentSize()/2);
        hint->setOpacity(0);
        parent->addChild(hint);
        auto duration=.5;
        
        auto spawn=Spawn::create(TargetedAction::create(stain, FadeOut::create(duration)),
                                 TargetedAction::create(hint, FadeIn::create(duration)),
                                 TargetedAction::create(towel, FadeOut::create(duration)),
                                 CallFunc::create([]{
            Common::playSE("pinpon.mp3");
        })
                                 ,NULL);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_towel,spawn,call, NULL));
    }
    else if(key.compare(0, 5, "money")==0)
    {
        Common::playSE("p.mp3");
        
        Vector<FiniteTimeAction*>actions;
        
        int index=atoi(key.substr(6,1).c_str());
        
        AnchorSprite*money;
        
        if (index==0) {
            money=AnchorSprite::create(Vec2(.5, .4), parent->getContentSize(), "sup_29_5yen.png");
        }
        else if(index==1){
            money=AnchorSprite::create(Vec2(.5, .4), parent->getContentSize(), "sup_29_100yen.png");
        }
        else{
            money=AnchorSprite::create(Vec2(.5, .4), parent->getContentSize(), "sup_29_500yen.png");
        }
        parent->addChild(money);
        
        //animation
        auto duration=1.5;
        auto angle=15.0;
        auto move_back=MoveBy::create(1, Vec2(0, -parent->getContentSize().height*.05));
        auto spawn=Spawn::create(ScaleBy::create(duration, .5),CallFunc::create([]{
            Common::playSE("hyu.mp3");
        }),
                                 Sequence::create(RotateBy::create(.1, angle/2),Repeat::create(Sequence::create(RotateBy::create(.1, -angle),RotateBy::create(.1, angle), NULL),5), NULL),
                                                              JumpBy::create(duration, Vec2(0, parent->getContentSize().height*.25), parent->getContentSize().height*.4, 1),
                                 Sequence::create(DelayTime::create(duration*.6),FadeOut::create(duration*.4), NULL)
                                 ,NULL);
        auto sound=CallFunc::create([]{
            Common::playSE("chalin.mp3");
        });
        
        auto seq_0=TargetedAction::create(money,Sequence::create(move_back,spawn,sound, NULL));
        auto delay=DelayTime::create(1);
        
        actions.pushBack(seq_0);
        actions.pushBack(delay);
        
        //buddha cutin
        auto back=Sprite::createWithSpriteFrameName("anim_buddha.png");
        back->setPosition(parent->getContentSize()/2);
        back->setOpacity(0);
        parent->addChild(back);
        
        auto fadein=FadeIn::create(2.5);
        auto particle=CallFunc::create([parent]{
            std::vector<Vec2>positions={Vec2(.455, .625),Vec2(.555, .625)};
            for (auto pos:positions) {
                auto particle=ParticleSystemQuad::create("fire.plist");
                particle->setLife(1);
                particle->setStartSize(parent->getContentSize().width*.025);
                particle->setStartColor(Color4F(100, 0, 100, 150));
                particle->setSpeed(parent->getContentSize().width*.01);
                particle->setDuration(1);
                particle->setAutoRemoveOnFinish(true);
                particle->setPosition(pos.x*parent->getContentSize().width,pos.y*parent->getContentSize().height);
                parent->addChild(particle);
            }
        });
        auto sound_1=CallFunc::create([]{
            Common::playSE("horror.mp3");
        });
        
        auto seq_1=TargetedAction::create(back, Sequence::create(fadein,DelayTime::create(1),sound_1,particle, NULL));
        actions.pushBack(seq_1);
        actions.pushBack(delay->clone());

        
        //分岐
        if (index<2) {
            std::vector<std::string>vec={"back_36.png","back_26.png"};

            auto back_1=Sprite::createWithSpriteFrameName(vec.at(index));
            back_1->setPosition(parent->getContentSize()/2);
            back_1->setOpacity(0);
            parent->addChild(back_1);
            
            auto remove_buddha=TargetedAction::create(back, RemoveSelf::create());
            
            auto seq_2=TargetedAction::create(back_1, Sequence::create(fadein->clone(),
                                                                       DelayTime::create(1),
                                                                       remove_buddha,
                                                                       CallFunc::create([]{
                Common::playSE("pinpon.mp3");
            }),
                                                                       DelayTime::create(1),
                                                                       FadeOut::create(2)
                                                                       ,NULL));
            actions.pushBack(seq_2);
            actions.pushBack(delay->clone());
        }
        else
        {
            auto back_1=Sprite::createWithSpriteFrameName("back_48.png");
            back_1->setPosition(parent->getContentSize()/2);
            back_1->setOpacity(0);
            parent->addChild(back_1);
            
            auto sup=Sprite::createWithSpriteFrameName("sup_48_mark.png");
            sup->setPosition(parent->getContentSize()/2);
            sup->setOpacity(0);
            parent->addChild(sup);
            
            auto fadein_1=TargetedAction::create(back_1, fadein->clone());
            auto remove_buddha=TargetedAction::create(back, RemoveSelf::create());
            auto fadein_2=TargetedAction::create(sup, FadeIn::create(.7));
            auto sound=CallFunc::create([]{
                Common::playSE("pinpon.mp3");
            });
            auto fadeouts=Spawn::create(TargetedAction::create(back_1, FadeOut::create(2)),
                                        TargetedAction::create(sup, FadeOut::create(2)), NULL);
            
            auto seq_3=Sequence::create(fadein_1,remove_buddha,sound,fadein_2,DelayTime::create(1),fadeouts, NULL);
            actions.pushBack(seq_3);
        }
        
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key.compare(0, 3, "pig")==0)
    {
        int index=atoi(key.substr(4,1).c_str());
        float duration_scale=.2;
        auto pig=parent->getChildByName<Sprite*>(StringUtils::format("sup_41_%d.png",index));
        
        auto original_scale=pig->getScale();
        auto scale_0=ScaleTo::create(duration_scale, original_scale*1.1, original_scale*.9);
        auto scale_1=ScaleTo::create(duration_scale, original_scale*.95, original_scale*1.05);
        auto scale_2=ScaleTo::create(duration_scale, original_scale);
        
        auto seq_pig=TargetedAction::create(pig,Sequence::create(scale_0,scale_1,scale_2, NULL));
        
        auto particle=CallFunc::create([parent,index](){
            Common::playSE(StringUtils::format("syamisen_%d.mp3",index).c_str());
            
            auto speed=parent->getContentSize().height/3;
            auto startSize=parent->getContentSize().width*.1;
            
            auto particle=ParticleSystemQuad::create("particle_music.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width*(.2+index*.2),parent->getContentSize().height*.55);
            particle->setSpeed(speed);
            particle->setStartSize(startSize);
            particle->setAngle(120-60*(index/2));
            particle->setGravity(Vec2(0, -parent->getContentSize().height*.3));
            if (index==0) {
                particle->setStartColor(Color4F(1, 0, 0, 1));
                particle->setEndColor(Color4F(1, 0, 0, 0));
            }
            else if (index==1){
                particle->setStartColor(Color4F(0, 0, 1, 1));
                particle->setEndColor(Color4F(0, 0, 1, 0));
            }
            else if (index==2){
                particle->setStartColor(Color4F(0, 1, 1, 1));
                particle->setEndColor(Color4F(0, 1, 1, 0));
            }
            else{
                particle->setStartColor(Color4F(1, 1, 0, 1));
                particle->setEndColor(Color4F(1, 1, 0, 0));
            }
            
            parent->addChild(particle);
        });
        
        //
        std::string answer="120330";
        
        if (AnswerManager::getInstance()->getPasscode().length()>=answer.length()) {
            AnswerManager::getInstance()->resetPasscode();
        }
        
        AnswerManager::getInstance()->appendPasscode(StringUtils::format("%d",index));
        log("入力中 %s",AnswerManager::getInstance()->getPasscode().c_str());
        
        bool correct=false;
        if (AnswerManager::getInstance()->getPasscode()==answer&&!DataManager::sharedManager()->getEnableFlagWithFlagID(32)) {
            correct=true;
            AnswerManager::getInstance()->enFlagIDs.push_back(27);
        }
        
        auto call=CallFunc::create([this,correct,callback](){
            //正解判定
            if (correct) {
                Common::playSE("pinpon.mp3");
                callback(true);
            }
            else{
                callback(false);
            }
        });
        
        parent->runAction(Sequence::create(Spawn::create(seq_pig,particle, NULL),call,NULL));
    }
    else if(key=="cloud")
    {
        Common::playSE("pig_1.mp3");
        
        auto pig=parent->getChildByName<Sprite*>("sup_41_none.png");
        auto duration_scale=.2;
        
        auto original_scale=pig->getScale();
        auto scale_0=ScaleTo::create(duration_scale, original_scale*1.1, original_scale*.9);
        auto scale_1=ScaleTo::create(duration_scale, original_scale*.95, original_scale*1.05);
        auto scale_2=ScaleTo::create(duration_scale, original_scale);
        
        auto seq_pig=TargetedAction::create(pig,Sequence::create(scale_0,scale_1,scale_2, NULL));
        
        //吹き出し
        auto cloud=AnchorSprite::create(Vec2(.8, .55), parent->getContentSize(), "sup_41_cloud.png");
        cloud->setOpacity(0);
        cloud->setScale(.1);
        parent->addChild(cloud);
        
        auto scale_3=EaseIn::create(Spawn::create(FadeIn::create(.3),ScaleTo::create(.3, 1), NULL), 1.5);
        auto scale_4=EaseOut::create(Spawn::create(FadeOut::create(.3),ScaleTo::create(.3, .1), NULL), 1.5);
        auto seq_cloud=TargetedAction::create(cloud,Sequence::create(scale_3,DelayTime::create(1),scale_4,RemoveSelf::create(), NULL));
        
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        
        parent->runAction(Sequence::create(Spawn::create(seq_pig,seq_cloud, NULL),call,NULL));
    }
    else if(key=="tanuki"){
        //たぬきを縮小するぞ
        auto back=Sprite::createWithSpriteFrameName("back_5.png");
        back->setCascadeOpacityEnabled(true);
        back->setOpacity(0);
        back->setPosition(parent->getContentSize()/2);
        parent->addChild(back);
        
        auto sup=Sprite::createWithSpriteFrameName("sup_5_door.png");
        sup->setPosition(back->getPosition());
        back->addChild(sup);
        
        auto tanuki=AnchorSprite::create(Vec2(.5, .07), parent->getContentSize(), "sup_5_tanuki.png");
        back->addChild(tanuki);
        
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        auto sound_0=CallFunc::create([this]{
            Common::playSE("magic07.mp3");
        });
        
        auto angle=5;
        auto rotate_count=8;
        auto duration_rotate=2.0/rotate_count;
        auto scale=TargetedAction::create(tanuki,Spawn::create(EaseIn::create(ScaleTo::create(2, .5), 1.5) ,
                                                               Sequence::create(RotateBy::create(duration_rotate, angle),
                                                                                Repeat::create(Sequence::create(RotateBy::create(duration_rotate, -angle*2),
                                                                                                                RotateBy::create(duration_rotate, angle*2), NULL), (rotate_count-2)/2),
                                                                                RotateBy::create(duration_rotate, -angle),NULL),NULL));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,DelayTime::create(1),sound_0,scale,DelayTime::create(1),call, NULL));
    }
    else{
        callback(true);
    }
}
