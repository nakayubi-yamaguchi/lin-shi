//
//  AnchorSprite.cpp
//  Airplane-mobile
//
//  Created by 成田凌平 on 2017/09/16.
//

#include "AnchorSprite.h"

AnchorSprite* AnchorSprite::create(cocos2d::Vec2 anchor, cocos2d::Size parentSize,std::string fileName)
{
    auto node=new AnchorSprite();
    if (node&&node->init(anchor, parentSize,false, fileName)) {
        node->autorelease();
        return node;
    }
    CC_SAFE_DELETE(node);
    return node;
}

AnchorSprite* AnchorSprite::create(cocos2d::Vec2 anchor, cocos2d::Size parentSize, bool invisible, std::string fileName)
{
    auto node=new AnchorSprite();
    if (node&&node->init(anchor, parentSize,invisible, fileName)) {
        node->autorelease();
        return node;
    }
    CC_SAFE_DELETE(node);
    return node;
}

bool AnchorSprite::init(cocos2d::Vec2 anchor, cocos2d::Size parentSize, bool invisible, std::string fileName)
{
    if (!EscapeStageSprite::init(fileName)) {
        return false;
    }
    
    setAnchorPoint(anchor);
    setCascadeOpacityEnabled(true);
    setScale(parentSize.width/getContentSize().width);
    setPosition(anchor.x*parentSize.width,anchor.y*parentSize.height);
    setName(fileName);
    if (invisible) {
        setOpacity(0);
    }
    
    
    return true;
}
