//
//  CustomActionKeys.h
//  Kebab
//
//  Created by 成田凌平 on 2016/12/22.
//
//touch.plistのカスタムアクションキーと一致させること

#ifndef CustomActionKeys_h
#define CustomActionKeys_h

//パス back.plistに記載されているもの
#define CustomActionNameOnBack_Anchor "anchorSprites"
#define CustomActionNameOnBack_Changing "changingSprites"
#define CustomActionNameOnBack_Clipping "clippingNodes"

#define CustomActionNameOnBack_Drag "dragSprites"
#define CustomActionNameOnBack_Input "useInput"
#define CustomActionNameOnBack_InputRotate "useInputRotate"
#define CustomActionNameOnBack_Label "labelSupplements"
#define CustomActionNameOnBack_Order "orderSprites"
#define CustomActionNameOnBack_Pattern "pattern"
#define CustomActionNameOnBack_Rotate "rotateSprites"
#define CustomActionNameOnBack_Switch "switchSprites"
#define CustomActionNameOnBack_Slider "sliderSprites"
#define CustomActionNaameOnBack_Spine "spine"//




//タッチ時のアクション
#define CustomAction_ChangingSprite "changing"//"_%d"の形でindexをつける 自動変化
#define CustomAction_DragSprite "drag"//_%d"の形でindexをつける dragできるスプライトを使用
#define CustomAction_Input "input"//"_%d"の形で パスコード入力など %d部分をインプットする
#define CustomAction_InputRotate "input_rotate"//"_%d"の形で パスコード入力など %d部分をインプットする
#define CustomAction_Label "label"//"_%d"の形でindexをつける インクリメントラベルよう
#define CustomAction_SliderSprite "slider"//_%d"の形でindexをつける スライドするスプライトを使用
#define CustomAction_SwitchSprite "switch"//"_%d"の形でindexをつける 回転スプライトとかよう
#define CustomAction_RotateSprite "rotate"//_%d"の形でindexをつける 回転するスプライトを使用
#define CustomAction_OrderSprite "order"//_%d"の形でindexをつける 並び替えスプライトを使用
#define CustomAction_SliderSprite "slider"//_%d"の形でindexをつける スライドするスプライトを使用
#define CustomAction_RotateSprite "rotate"//_%d"の形でindexをつける 回転するスプライトを使用


#define CustomAction_Anchor "anchor"//アンカーポイントのみセット
#define CustomAction_PatternSprite "pattern"//
#define CustomAction_Spine "spine"//

#define CustomAction_ShowCMMessage "showCMMessage"//中間地点CM
#define CustomAction_ShowAd "showAd"//広告表示


#define CustomAction_HouseAd "house"//"_%d"の形でindexをつける
typedef enum{
    HouseAdIndex_Balentien=0,
    HouseAdIndex_ClassRoom,
    HouseAdIndex_WhiteDay
}HouseAdIndex;


#define CustomBackAction_CreateHint "createHintButton"//タッチ領域にヒントを作成

//customAction
#define CustomAction_Clear "clear"
#define CustomAction_ReviewLayer "showreviewlayer"
#define CustomAction_Title "title"

typedef enum{
    ReplaceTypeNone=0,
    ReplaceTypeFadeIn,
    ReplaceTypeLeft,
    ReplaceTypeRight,
    ReplaceTypeSlowFadeIn
}ReplaceType;

#endif /* CustomActionKeys_h */
