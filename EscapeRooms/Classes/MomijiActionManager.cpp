//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "MomijiActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"

using namespace cocos2d;

MomijiActionManager* MomijiActionManager::manager =NULL;

MomijiActionManager* MomijiActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new MomijiActionManager();
    }
    return manager;
}

#pragma mark - Back
void MomijiActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
#pragma mark 煙
    if(key=="smoke") {
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(4)) {
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(9)) {//秋刀魚を乗っけていたら
                Common::playSE("takibi.mp3");
                showBakedSmokeParticle(parent, false);
            }
            else {//火がついているだけ。
                showDefaultSmokeParticle(parent, false);
            }
        }
    }
#pragma mark 火
    else if(key=="fire" && DataManager::sharedManager()->getEnableFlagWithFlagID(4)) {
        showFireParticle(parent, backID);
    }
#pragma mark 池の中の魚
    else if(key=="fishin") {
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(7)) {
            
            Vector<FiniteTimeAction*> actions;
            Sprite* beforeSp = NULL;
            for (int i=0; i<4; i++) {
                int num = i%3+1;
                auto fileName = StringUtils::format("sup_%d_fish_%d.png", backID, num);
                auto fish = num==1? parent->getChildByName<Sprite*>(fileName.c_str()) : createSpriteToCenter(fileName, true, parent, true);
                
                if (beforeSp!=NULL) {
                    Vector<FiniteTimeAction*> spawnActions;
                    spawnActions.pushBack(createChangeAction(.5, .5, beforeSp, fish));
                    
                    if (backID == 64) {
                        spawnActions.pushBack(createSoundAction("pochan.mp3"));
                    }
                    
                    actions.pushBack(spawnActions);
                }
                
                actions.pushBack(DelayTime::create(1.5));
                beforeSp = fish;
            }
            
            //後でアニメーション停止するためにparentにrunActionしていない。
            beforeSp->runAction(RepeatForever::create(Sequence::create(actions)));
        }
    }
#pragma mark 月面
    else if (key == "moon"){
        //星のパーティクル
        std::vector<Vec2> positions = {Vec2(.075, .818), Vec2(.083, .878), Vec2(.153, .894), Vec2(.183, .841), Vec2(.258, .826), Vec2(.325, .8), Vec2(.397, .818)};
        float per = 1.45;
        std::vector<float> sizes = {1, 0, 1, 2, 0, 1, 2};
        
        auto particleBatch = ParticleBatchNode::create("kira.png");
        parent->addChild(particleBatch);
        int i = 0;
        for (auto pos : positions) {
            auto particle = ParticleSystemQuad::create("kirakira.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width*pos.x, parent->getContentSize().height*pos.y);
            particle->setTotalParticles(15);
            particle->setStartSpinVar(10);
            particle->setLife(.5);
            particle->setLifeVar(.2);
            particle->setAngleVar(180);
            particle->setStartSize(parent->getContentSize().width*.04*pow(per, sizes.at(i)));
            particle->setEndColor(Color4F(0, 0, 0, .5));
            particle->setEndSize(particle->getStartSize());
            particleBatch->addChild(particle);
            
            i++;
        }
        
        //うさぎ
        std::vector<int> rabbitAnswers = {1, 1, 1, 0, 1, 1, 0 ,0};
        Vector<FiniteTimeAction*> rabbitActions;
        Vector<FiniteTimeAction*> swingActions;
        for (int i = 0; i<2; i++) {
            auto after = parent->getChildByName<Sprite*>(StringUtils::format("sup_77_%d_2.png",i));
            auto before = createSpriteToCenter(StringUtils::format("sup_77_%d_1.png",i), true, parent, true);
            auto changeToBefore = createChangeAction(.3, .3, after, before);
            auto changeToAfter = createChangeAction(.2, .2, before, after);
            
            auto swingSeq = Sequence::create(changeToBefore,
                                             createSoundAction("swing_0.mp3"),
                                             changeToAfter,
                                             DelayTime::create(.5),
                                             NULL);
            swingActions.pushBack(swingSeq);
        }
        
        for (int answer : rabbitAnswers) {
            rabbitActions.pushBack(swingActions.at(answer)->clone());
        }

        //アニメーション終了後のアニメーション
        rabbitActions.pushBack(DelayTime::create(1.2));
        
        auto rabbitSeq = Sequence::create(rabbitActions);
        auto repeatSwing = Repeat::create(rabbitSeq, 1000);//RepeatForeverを使うとdelayのあと動かなくなる。
        
        parent->runAction(Sequence::create(DelayTime::create(.8), repeatSwing, NULL));
    }
#pragma mark 中庭の雲
    else if (key == "backcloud") {
        
        auto fileName = "smoke.png";
        
        float alphaRatio = .17;
        float alphaVarRatio = .1;
        
        float sizeRatio = .12;
        float sizeVarRatio = .1;

        //初回は背景に貼り付け。
        //画像一括貼り付け用
        auto batchNode = SpriteBatchNode::create(fileName);
        
        Vector<FiniteTimeAction*> spawnActions;
        for(int i=0;i<15;i++){
            //batchNodeからテクスチャを取得
            auto image = Sprite::createWithTexture(batchNode->getTexture());
            
            //ランダムな位置に設定
            auto randomPosX = (float)RandomHelper::random_int(0, (int)parent->getContentSize().width*100)/100;
            auto randomY = (float)RandomHelper::random_int(0, (int)(parent->getContentSize().height*.2)*100)/100;
            auto randomPosY = parent->getContentSize().height*.8+randomY;
            image->setPosition(Vec2(randomPosX, randomPosY));

            //パーティクルと同じ動きをさせる
            //透明度をランダムに
            image->setBlendFunc((BlendFunc) {GL_SRC_ALPHA, GL_ONE});
            auto alphaVar = (double)RandomHelper::random_int(0, (int)alphaVarRatio*100)/100*pow(-1, (double)RandomHelper::random_int(0, 1));
            image->setOpacity(255*(alphaRatio+alphaVar));
            
            //サイズをランダムに
            auto sizeVar = (double)RandomHelper::random_int(0, (int)sizeVarRatio*100)/100*pow(-1, (double)RandomHelper::random_int(0, 1));
            image->setScale((sizeRatio + sizeVar)*parent->getContentSize().width/image->getContentSize().width);
            
            image->setRotation(RandomHelper::random_int(0, 360));
            
            //SpriteBatchNodeに貼り付ける
            batchNode->addChild(image);
            
            //貼った画像のアクション
            auto moveAction = MoveBy::create(40, Vec2(parent->getContentSize().width, 0));
            auto smokeAction = TargetedAction::create(image, Sequence::create(moveAction, RemoveSelf::create(), NULL));
            spawnActions.pushBack(smokeAction);
        }
        parent->runAction(Spawn::create(spawnActions));
        
        //一括貼り付け
        parent->addChild(batchNode);
        
        //パーティクル貼り付け
        std::vector<Vec2> poses = {Vec2(.06,.8), Vec2(.37, .66)};
        std::vector<Vec2> posVars = {Vec2(.1,.2), Vec2(.35, .075)};
        auto particleBatch = ParticleBatchNode::create(fileName);
        parent->addChild(particleBatch);
        for (int i = 0; i<poses.size(); i++) {
            auto particle=ParticleSystemQuad::create("smoke.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width*poses.at(i).x, parent->getContentSize().height*poses.at(i).y);
            particle->setPosVar(Vec2(parent->getContentSize().width*posVars.at(i).x, parent->getContentSize().height*posVars.at(i).y));
            particle->setSpeed(parent->getContentSize().width*.02);
            particle->setTotalParticles(55);
            particle->setAngle(5);
            particle->setAngleVar(10);
            particle->setStartSpinVar(180);
            
            particle->setLife(20);
            particle->setLifeVar(7);
            particle->setGravity(Vec2(1, 0));
            particle->setStartColor(Color4F(1, 1, 1, alphaRatio));
            particle->setStartColorVar(Color4F(0, 0, 0, alphaVarRatio));
            
            particle->setStartSize(parent->getContentSize().width*sizeRatio);
            particle->setStartSizeVar(parent->getContentSize().width*sizeVarRatio);
            particle->setEndSize(particle->getStartSize());
    
            particleBatch->addChild(particle);
        }
    }
#pragma mark 穴からネズミ出現。間違い探し
    else if(key=="back_9")
    {
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            auto cover = createSpriteToCenter("sup_9_cover.png", false, parent, true);
            cover->setLocalZOrder(1);
            
            auto moveDistance = parent->getContentSize().width*.15;
            auto rotation = 10.0;
            auto mistake = parent->getChildByName("sup_9_mistake.png");
            //mistake->setAnchorPoint(Vec2(parent->getContentSize().width*.61, parent->getContentSize().height*.31));
            mistake->setPositionX(mistake->getPositionX()+moveDistance);
            mistake->setRotation(rotation);
            
            auto spawn = Spawn::create(MoveBy::create(.5, Vec2(-moveDistance, 0)),
                                       RotateBy::create(.5, -rotation), NULL);
            auto seq=TargetedAction::create(mistake, Sequence::create(DelayTime::create(1),
                                                                      spawn,
                                                                      createSoundAction("nyu.mp3"),
                                                                      CallFunc::create([this]{
                DataManager::sharedManager()->setTemporaryFlag("mistake_9", 1);
            }), NULL));
            
            mistake->runAction(seq);
        }
    }
}

#pragma mark - Touch
void MomijiActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
#pragma mark 欲しいもの。雲
    if(key=="cloud"){
        auto createBounceActionLambda = [this, parent](std::string fileName, Vec2 anchorPoint)->FiniteTimeAction*{
            
            auto target = parent->getChildByName(fileName.c_str());
            target->setAnchorPoint(anchorPoint);
            target->setPosition(anchorPoint.x*parent->getContentSize().width, anchorPoint.y*parent->getContentSize().height);
            
            return createBounceAction(target, .3, .1);
        };
        
        Vector<FiniteTimeAction*>actions_spawn;
        
        auto num=DataManager::sharedManager()->getNowBack();
        Vec2 cloud_anchor;
        
        if (num == 23) {//包丁欲しがる、ピンキー
            cloud_anchor = Vec2(.54,.68);
            auto anchor = Vec2(400.0/600, 210.0/660);//ブタの中央下基準
            
            log("アンカーポイント{%f, %f}",anchor.x, anchor.y);
            actions_spawn.pushBack(createBounceActionLambda("sup_23_pig.png", anchor));
            actions_spawn.pushBack(createBounceActionLambda("sup_23_pighand_1.png", anchor));
        }
        else if (num == 57) {
            cloud_anchor = Vec2(.6, .71);
            auto anchor = Vec2(448.0/600, 210.0/660);//ブタの中央下基準

            actions_spawn.pushBack(createBounceActionLambda("sup_57_pig_1.png", anchor));
        }
        
    
        actions_spawn.pushBack(createSoundAction("pig.mp3"));

        
        auto cloud = cloudAction(parent, StringUtils::format("sup_%d_cloud.png",num), cloud_anchor);
        actions_spawn.pushBack(cloud);
        
        auto call = CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(Spawn::create(actions_spawn),call, NULL));
    }
#pragma mark ハサミちょきちょき
    else if(key == "scissors") {
        Vector<FiniteTimeAction*> actions;
        
        Common::playSE("p.mp3");
        
        auto item=createAnchorSprite("sup_32_scissors.png", .5, .5, true, parent, true);
        item->setPosition(parent->getContentSize().width*385/600, parent->getContentSize().height*210/660);

        //fade-in
        actions.pushBack(TargetedAction::create(item, FadeIn::create(1)));
        
        actions.pushBack(createSoundAction("chokichoki.mp3"));

        //回転しながら紐に近づく
        auto rotate = Sequence::create(RotateBy::create(.3, 25),
                                       RotateBy::create(.3, -30), NULL);
        auto spawn = Spawn::create(Repeat::create(rotate, 3),
                                   MoveTo::create(1.8, Vec2(parent->getContentSize().width*345/600, parent->getContentSize().height*274/660)), NULL);
        
        actions.pushBack(TargetedAction::create(item, spawn));
        
        actions.pushBack(DelayTime::create(.3));
        
        //コールバック
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 鍵開ける
    else if(key == "key") {
        Common::playSE("p.mp3");
        
        auto key = AnchorSprite::create(Vec2(.5, .5), parent->getContentSize(),true, "sup_38_key_0.png");
        parent->addChild(key);
        auto fadein=TargetedAction::create(key, FadeIn::create(.7));
        auto sound=createSoundAction("kacha.mp3");
        auto key_1=createSpriteToCenter("sup_38_key_1.png", true, parent, true);
        auto change=createChangeAction(1, .7, key, key_1, "kacha.mp3");
        auto fadeout = TargetedAction::create(key_1, FadeOut::create(.3));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(sound,fadein,DelayTime::create(.7),change,DelayTime::create(.4),fadeout, DelayTime::create(.3),call,NULL));
    }
#pragma mark ドライバー
    else if(key=="driver") {
        Common::playSE("p.mp3");
        Vector<FiniteTimeAction*>actions;
        
        auto item=createAnchorSprite("sup_9_driver.png", .5, .5, true, parent, true);
        item->setPosition(parent->getContentSize().width*125/600,parent->getContentSize().height*295/660);
        auto fadein_item=TargetedAction::create(item, FadeIn::create(1));
        actions.pushBack(fadein_item);
        
        std::vector<Vec2>vecs={Vec2(475.0/600, 295.0/660),Vec2(125.0/600, 220.0/660),Vec2(473.0/600, 220.0/660)};
        for (int i=0; i<vecs.size()+1; i++) {
            auto shake=Spawn::create(jumpAction(parent->getContentSize().height*.005, .1, item, 1.5, 3),
                                     createSoundAction("kacha_2.mp3"), NULL);
            actions.pushBack(shake);
            auto remove_screw=TargetedAction::create(parent->getChildByName(StringUtils::format("sup_9_screw_%d.png",i)), Sequence::create(FadeOut::create(.3),
                                                                                                                                           RemoveSelf::create(), NULL));
            actions.pushBack(remove_screw);
            auto delay=DelayTime::create(1);
            actions.pushBack(delay);
            
            if (i==vecs.size()) {//ドライバーをフェードアウト
                auto fadeout=TargetedAction::create(item, Spawn::create(MoveBy::create(1, Vec2(0, -parent->getContentSize().height*.1)),
                                                                        FadeOut::create(1), NULL));
                actions.pushBack(fadeout);
                actions.pushBack(DelayTime::create(.1));
            }
            else {
                auto vec=vecs.at(i);
                log("{%f,%f}",vec.x,vec.y);
                auto move=TargetedAction::create(item, Spawn::create(MoveTo::create(.7, Vec2(parent->getContentSize().width*vec.x, parent->getContentSize().height*vec.y)), NULL));
                actions.pushBack(move);
                actions.pushBack(DelayTime::create(.3));
            }
        }
        
        //板外し
        auto boardFadeout = TargetedAction::create(parent->getChildByName("sup_9_board.png"), Sequence::create(FadeOut::create(.3),RemoveSelf::create(),NULL));
        actions.pushBack(Spawn::create(boardFadeout, createSoundAction("kata.mp3"), NULL));
        actions.pushBack(DelayTime::create(.4));

        //マウスジャンプ
        auto stencil=createSpriteToCenter("sup_9_clip.png", false, parent);
        auto clip=ClippingNode::create(stencil);
        clip->setInverted(false);
        clip->setAlphaThreshold(0.0f);
        
        auto mouse = createSpriteToCenter("sup_9_mouse_1.png", false, parent);
        mouse->setPositionX(parent->getContentSize().width*.3);
        clip->addChild(mouse);
        parent->addChild(clip);
        
        //ジャンプ音
        int jumpCount = 4;
        float duration = 2;
        Vector<FiniteTimeAction*> soundActions;
        for (int i = 0; i<jumpCount; i++) {
            soundActions.pushBack(DelayTime::create(duration/jumpCount));
            soundActions.pushBack(createSoundAction("nyu.mp3"));
        }
        auto jumpSoundSequence = Sequence::create(soundActions);
        
        actions.pushBack(Spawn::create(TargetedAction::create(mouse, JumpTo::create(duration, parent->getContentSize()/2, parent->getContentSize().height*.06, jumpCount)), jumpSoundSequence, NULL));

        //遅延
        actions.pushBack(DelayTime::create(.5));
        
        //マウス切り替え
        auto mouse2 = createSpriteToCenter("sup_9_mouse_2.png", true, parent, true);
        actions.pushBack(createChangeAction(.5, .5, mouse, mouse2));
        actions.pushBack(createSoundAction("piyo.mp3"));
        actions.pushBack(DelayTime::create(.3));

        //萩の月を食べに行く
        auto back_36 = createSpriteToCenter("back_36.png", true, parent);
        back_36->setCascadeColorEnabled(true);
        parent->addChild(back_36);
        
        Vector<Sprite*> haginoBefores;
        Vector<Sprite*> haginoAfters;
        Vector<Sprite*> mouses;
        for (int i=0; i<2; i++) {
            auto haginoName = StringUtils::format("sup_36_hagino_%d_0.png",i);
            haginoBefores.pushBack(createSpriteToCenter(haginoName, false, back_36, true));
            
            auto haginoNameAfter = StringUtils::format("sup_36_hagino_%d_1.png",i);
            haginoAfters.pushBack(createSpriteToCenter(haginoNameAfter, true, back_36, true));
            
            auto mouseName = StringUtils::format("sup_36_mouse_%d.png",i);
            mouses.pushBack(createSpriteToCenter(mouseName, true, back_36, true));
        }
        actions.pushBack(TargetedAction::create(back_36, FadeIn::create(.5)));

        //ネズミ、かじかじ
        Vector<FiniteTimeAction*> mouseActions;
        mouseActions.pushBack(TargetedAction::create(mouses.at(0), FadeIn::create(.5)));

        for (int i=0; i<2; i++) {
            auto mouse_ = mouses.at(i);
            
            auto shake = Spawn::create(jumpAction(parent->getContentSize().height*.008, .1, mouse_, 1.5, 3),
                                     createSoundAction("throw.mp3"), NULL);
            mouseActions.pushBack(shake);
            
            FiniteTimeAction* fadeAction;
            if (i!=1) {//最後じゃ無い
                fadeAction = createChangeAction(.5, .5, mouse_, mouses.at(i+1));
            }
            else {//最後なら、ネズミは消すだけ。
                fadeAction = TargetedAction::create(mouse_, FadeOut::create(.5));
            }
            
            mouseActions.pushBack(Spawn::create(fadeAction,
                                                createChangeAction(.5, .5, haginoBefores.at(i), haginoAfters.at(i)), NULL));
            mouseActions.pushBack(DelayTime::create(.7));
        }
        actions.pushBack(mouseActions);
        
        mouseActions.pushBack(DelayTime::create(1));

        /*
        auto change=createChangeAction(1, .7, plate, createSpriteToCenter("sup_73_mark.png", true, parent,true));
        actions.pushBack(change);
        actions.pushBack(DelayTime::create(1));*/
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
#pragma mark もみじ饅頭ゲット
    else if(key == "momijimanju"){
        auto call_story_0=CallFunc::create([this,parent,callback](){
            auto story=StoryLayer::create("ending1", [this,callback,parent](Ref*ref){
                Common::playSE("kapa.png");
                auto oju=parent->getChildByName<Sprite*>("sup_25_momijibox.png");
                auto oju_1=createSpriteToCenter("sup_25_momijibox_1.png", true, parent);
                parent->addChild(oju_1);
                
                auto delay=DelayTime::create(.5);
                auto change=createChangeAction(1, .7, oju, oju_1);
                auto call_story_1=CallFunc::create([this,parent,callback,oju_1](){
                    auto story=StoryLayer::create("ending2", [this,callback,parent,oju_1](Ref*ref){
                        
                        auto fadeout=TargetedAction::create(oju_1, FadeOut::create(.7));
                        auto call=CallFunc::create([callback]{
                            callback(true);
                        });
                        
                        parent->runAction(Sequence::create(fadeout,call, NULL));
                    });
                    Director::getInstance()->getRunningScene()->addChild(story);
                });
                
                parent->runAction(Sequence::create(delay,change,call_story_1, NULL));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        parent->runAction(Sequence::create(call_story_0, NULL));
    }
#pragma mark クリア
    else if (key=="clear"){
        Common::playSE("dogara.mp3");
        
        auto open=createSpriteToCenter("sup_12_open.png", true, parent);
        parent->addChild(open);
        
        auto fadein=TargetedAction::create(open, FadeIn::create(.2));
        
        auto call=CallFunc::create([]{
            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

            auto story=StoryLayer::create("ending3", [](Ref*ref){
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        parent->runAction(Sequence::create(fadein,DelayTime::create(.5),call, NULL));
    }
#pragma mark 扉を開けるな
    else if (key=="dontopen"){
        
        auto story=StoryLayer::create("dontopen", [callback,parent](Ref*ref){
            callback(true);
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
#pragma mark 猿の絵画アニメーション
    else if (key == "drawing"){
        Vector<FiniteTimeAction*> actions;
        Common::playSE("p.mp3");
        //遅延アクション
        auto delay = DelayTime::create(.7);

        //背景45でスケッチブック置く
        auto sketchbook=createSpriteToCenter("sup_45_sketchbook_0.png", true, parent,true);
        auto sketchbookFadein=TargetedAction::create(sketchbook, Spawn::create(FadeIn::create(1),createSoundAction("koto.mp3"), NULL));
        actions.pushBack(sketchbookFadein);
        actions.pushBack(delay);

        //背景44アクション
        auto back=createSpriteToCenter("back_44.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 44);
        
        createSpriteToCenter("sup_44_sketchbook.png", false, back, true);

        auto sup_crayon = back->getChildByName<Sprite*>("sup_44_crayon_before.png");
        auto back44Fadein = TargetedAction::create(back, FadeIn::create(.5));
        actions.pushBack(back44Fadein);
        actions.pushBack(delay->clone());
        
        //猿ジャンプ
        auto monkey = back->getChildByName<Sprite*>("sup_44_monkey.png");
        monkey->setLocalZOrder(1);
        float defaultScaleY = monkey->getScaleY();
        monkey->setAnchorPoint(Vec2(.208, 0.257));
        monkey->setPosition(monkey->getPosition().x-(.5-monkey->getAnchorPoint().x)*back->getContentSize().width,
                            monkey->getPosition().y-(.5-monkey->getAnchorPoint().y)*back->getContentSize().height);
        auto scaleSmallAct = ScaleTo::create(.2, monkey->getScaleX(), defaultScaleY * 0.92);

        auto delayAct = DelayTime::create(0.15f);
        auto scaleBigAct = ScaleTo::create(0.1f, monkey->getScaleX(), defaultScaleY);
        auto jumpAct = JumpBy::create(0.24f, Vec2(0, 0), parent->getContentSize().height*.1, 1);
        auto spawn = Spawn::create(scaleBigAct, jumpAct, createSoundAction("pop_1.mp3"), NULL);
        
        actions.pushBack(TargetedAction::create(monkey, Repeat::create(Sequence::create(scaleSmallAct, delayAct, spawn, NULL), 2)));
        actions.pushBack(DelayTime::create(.3));
        
        //猿の手アクション
        auto sup_hand=createSpriteToCenter("sup_44_monkeyhand.png", true, back, true);
        sup_hand->setLocalZOrder(1);

        //auto monkeyhandFadein = Spawn::create(TargetedAction::create(sup_hand, FadeIn::create(.5)),createSoundAction("monkey.mp3"), NULL);
        
        //クレヨンと猿の手入れ替わり
        auto change_Crayon_Hand = createChangeAction(.5, .5, sup_crayon, sup_hand, "monkey.mp3");
        actions.pushBack(change_Crayon_Hand);
        actions.pushBack(delay->clone());

        //バック44アクション
        auto back44Fadeout = TargetedAction::create(back, Sequence::create(FadeOut::create(.5), RemoveSelf::create(), NULL));
        
        //バック45のクレヨンアクション
        auto crayon_before = parent->getChildByName<Sprite*>("sup_45_crayon_before.png");
        auto crayonFadeout = TargetedAction::create(crayon_before, Sequence::create(FadeOut::create(.5), RemoveSelf::create(), NULL));
        
        auto back44_crayon_spawn = Spawn::create(back44Fadeout, crayonFadeout, NULL);
        actions.pushBack(back44_crayon_spawn);
        actions.pushBack(delay->clone());
        
        //お絵かき猿後ろ姿
        Vector<FiniteTimeAction*> monkeyActions;
        for (int i=0; i<4; i++) {
            auto monkeyFileName = StringUtils::format("sup_45_monkey_%d.png",i);
            auto monkey = createSpriteToCenter(monkeyFileName, true, parent, true);
            auto monkeyFadein = TargetedAction::create(monkey, FadeIn::create(.5));
            auto monkeyFadeout = TargetedAction::create(monkey, FadeOut::create(.5));

            auto delay_ = DelayTime::create(.4);
            auto penSound = createSoundAction("pen.mp3");
            
            auto sketchbookFileName = StringUtils::format("sup_45_sketchbook_%d.png",i+1);
            auto sketchbook = createSpriteToCenter(sketchbookFileName, true, parent, true);
            auto change_Monkey_Sketch = createChangeAction(.5, .5, monkey, sketchbook);
            
            auto sketchAction = Sequence::create(Spawn::create(monkeyFadein, penSound, NULL),
                                                 delay_,
                                                 change_Monkey_Sketch,
                                                 monkeyFadeout,NULL);
            monkeyActions.pushBack(sketchAction);
        }
        auto monkeySequence = Sequence::create(monkeyActions);
        actions.pushBack(monkeySequence);
        
        //完了通知
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
    
        parent->runAction(Sequence::create(actions));
    }
#pragma mark マッチ着火
    else if(key=="match")
    {//マッチアニメ
        Common::playSE("p.mp3");
        Vector<FiniteTimeAction*> actions;

        //Vector<FiniteTimeAction*>beforeActions;
        //マッチ着火
        auto fireParticle = showFireParticle(parent, DataManager::sharedManager()->getNowBack());
        fireParticle->setTotalParticles(0);
        auto match=createSpriteToCenter("sup_58_match.png", true, parent, true);
        auto seq_match=TargetedAction::create(match, Sequence::create(FadeIn::create(1),
                                                                      Repeat::create(swingAction(Vec2(0, parent->getContentSize().height*.02), .1, match, 1.51, false), 2),
                                                                      DelayTime::create(.3),
                                                                      CallFunc::create([this, fireParticle]{
            Common::playSE("huo.mp3");
            fireParticle->setTotalParticles(150);
        }),
                                                                      FadeOut::create(1),
                                                                      NULL));
        actions.pushBack(seq_match);
        
        //ちょっと待機
        actions.pushBack(DelayTime::create(1));
        
        //魚もうちわも持っていたら
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(9) && DataManager::sharedManager()->getEnableFlagWithFlagID(15)) {
            //遷移
            auto back57 = createSpriteToCenter("back_57.png", true, parent, true);
            SupplementsManager::getInstance()->addSupplementToMain(back57, 57);
            auto smoke = showBakedSmokeParticle(back57, false);
            smoke->setTotalParticles(0);
            
            actions.pushBack(Spawn::create(CallFunc::create([this, fireParticle, back57](){
                fireParticle->setDuration(1);//火を消す
                showBakedSmokeParticle(back57, false);//煙表示
            }),
                                           TargetedAction::create(back57, FadeIn::create(1)),
                                           createSoundAction("takibi.mp3"), NULL));
            
            actions.pushBack(DelayTime::create(.5));
            
            //うちわアクション
            actions.pushBack(fanAction(back57, smoke));
            
            showDefaultSmokeParticle(parent, false)->setTotalParticles(0);
            
            //待機
            actions.pushBack(DelayTime::create(1));
        }
      
        //57に完全遷移 & end
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 魚を網の上に乗せる
    else if (key == "fishOn"){
        Common::playSE("p.mp3");
        
        Vector<FiniteTimeAction*> actions;
        //魚を置く
        auto fish=createSpriteToCenter("sup_57_fish.png", true, parent,true);
        auto fishFadein=TargetedAction::create(fish, Spawn::create(FadeIn::create(1),createSoundAction("koto.mp3"), NULL));
        actions.pushBack(fishFadein);
        
        //火が付いていたら
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(4)) {
            
            auto smallSmoke = showDefaultSmokeParticle(parent, false);
            smallSmoke->setName("");//間違って参照されないように。
            
            //新規のパーティクル
            auto bigSmoke = showBakedSmokeParticle(parent, true);
            bigSmoke->setTotalParticles(0);
            
            auto createParticle = CallFunc::create([this, parent, smallSmoke]{
                log("煙を操作する");
                
                //表示中のパーティクル
                smallSmoke->setDuration(1);

                //bigsmokeを発生させる。
                showBakedSmokeParticle(parent, false);
            });
            actions.pushBack(createParticle);
            actions.pushBack(createSoundAction("takibi.mp3"));
            
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(15)) {//うちわも持っていたら
                log("うちわを持っている");
                
                actions.pushBack(DelayTime::create(1));
                
                //うちわ
                actions.pushBack(fanAction(parent, bigSmoke));
            }
            else {
                log("うちわを持ってない。");
            }
        }
        
        //完了通知
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark うちわをあげる
    else if(key == "fan") {//うちわをあげる。
        Common::playSE("p.mp3");
        
        Vector<FiniteTimeAction*> actions;
        
        //うちわ渡す
        auto fan = createSpriteToCenter("sup_57_pig_1_hand.png", true, parent, true);
        auto fanFadein=TargetedAction::create(fan, FadeIn::create(1));
        actions.pushBack(fanFadein);
        actions.pushBack(DelayTime::create(.4));
        
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(4) && DataManager::sharedManager()->getEnableFlagWithFlagID(9)) {//火もついてるし、秋刀魚も乗っている
            
            //うちわ仰ぐ
            actions.pushBack(fanAction(parent, NULL));
        }
    
        //完了通知
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
        
    }
#pragma mark りんごを切る
    else if (key == "cutapple"){
        Common::playSE("p.mp3");
        
        Vector<FiniteTimeAction*> actions;
        
        //包丁渡す
        auto knife=createSpriteToCenter("sup_23_knife.png", true, parent,true);
        auto knifeFadein=TargetedAction::create(knife, Spawn::create(FadeIn::create(1), MoveTo::create(1, Vec2(parent->getContentSize().width/2, parent->getContentSize().height*.55)), ScaleTo::create(1, .8),NULL));
        actions.pushBack(knifeFadein);
        actions.pushBack(DelayTime::create(.4));
        actions.pushBack(TargetedAction::create(knife, FadeOut::create(.5)));
        
        //包丁持たせる
        auto pinkyHand = parent->getChildByName<Sprite*>("sup_23_pighand_1.png");
        auto pinkyHandWithKnife = createSpriteToCenter("sup_23_pighand_2.png", true, parent, true);
        actions.pushBack(createChangeAction(.5, .5, pinkyHand, pinkyHandWithKnife));
        
        
        //投げるりんごと置いてあるりんご入れ替え
        auto throwApple = AnchorSprite::create(Vec2(311.0f/600.0f, 200.0f/660.0f), parent->getContentSize(), true, "sup_23_apple_throw.png");
        parent->addChild(throwApple);
        auto apple = parent->getChildByName<Sprite*>("sup_23_apple_1.png");
        actions.pushBack(DelayTime::create(.3));
        actions.pushBack(createChangeAction(.5, .5, apple, throwApple));
        
        //りんごを投げる & 包丁振り回す
        Vector<FiniteTimeAction*> spawnActions;
        float totalDuration = 2;
        spawnActions.pushBack(createSoundAction("fall.mp3"));
        
        //////りんごを上下
        float upDuration = 1;
        auto moveVec = Vec2(0, parent->getContentSize().height*.4);
        auto moveTop = MoveBy::create(upDuration, moveVec);
        auto moveFall = MoveBy::create(totalDuration-upDuration, Vec2(0, -moveVec.y*.8));
        auto move = Sequence::create(moveTop, moveFall, NULL);
        spawnActions.pushBack(move);
        
        //////ピンキー包丁を持つ&振るアニメ
        Vector<SpriteFrame*>frames;
        for (int i=2; i>0; i--) {
            frames.pushBack(EscapeStageSprite::createWithSpriteFileName(StringUtils::format("sup_23_pighand_%d.png",i+1))->getSpriteFrame());
        }
        auto animation=Animation::createWithSpriteFrames(frames);
        animation->setDelayPerUnit(5.0f/60.0f);//??フレームぶん表示
        animation->setRestoreOriginalFrame(true);
        animation->setLoops(10);
        
        Vector<FiniteTimeAction*> kataSounds;
        for (int i=0; i<10; i++) {
            kataSounds.pushBack(createSoundAction("kata.mp3"));
            kataSounds.pushBack(DelayTime::create(.2));
        }
        
        auto knifeSeq = Sequence::create(createSoundAction("pig.mp3"),
                                         TargetedAction::create(pinkyHandWithKnife, Animate::create(animation)),
                                         DelayTime::create(1), NULL);
        auto knifeSpawn = Spawn::create(knifeSeq, Sequence::create(kataSounds), NULL);
        spawnActions.pushBack(Sequence::create(DelayTime::create(upDuration), knifeSpawn, NULL));
        
        //////りんごを小さく
        auto scale = ScaleTo::create(totalDuration, .6);
        spawnActions.pushBack(scale);

        //////りんごをフェードアウト
        float fadeoutDuration = .5;
        auto fadeout = FadeOut::create(fadeoutDuration);
        auto fadeoutSeq = Sequence::create(DelayTime::create(totalDuration-fadeoutDuration), fadeout, NULL);
        spawnActions.pushBack(fadeoutSeq);

        //////カットりんごを並べる。
        Vector<FiniteTimeAction*> cutAppleActions;
        for (int i=0; i<6; i++) {
            auto cutApple = createSpriteToCenter(StringUtils::format("sup_23_cutapple_%d.png",i), true, parent, true);
            float originY = cutApple->getPositionY();
            cutApple->setPositionY(originY+parent->getContentSize().height*.1);
            
            auto cutAppleSpawn = Spawn::create(MoveTo::create(.2, Vec2(cutApple->getPositionX(), originY)),
                                               FadeIn::create(.2),
                                               createSoundAction("koto.mp3"), NULL);
            cutAppleActions.pushBack(Sequence::create(TargetedAction::create(cutApple, cutAppleSpawn),
                                              DelayTime::create(.05), NULL));
        }
        spawnActions.pushBack(Sequence::create(DelayTime::create(2),
                                               Sequence::create(cutAppleActions), NULL));
        actions.pushBack(TargetedAction::create(throwApple, Spawn::create(spawnActions)));

        //完了通知
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 釣り
    else if (key == "fishing"){
        Common::playSE("p.mp3");

        Vector<FiniteTimeAction*> actions;
        
        //つりざおフェードイン
        auto rod = createAnchorSprite("sup_64_fishingrod.png", .933, .621, true, parent, true);
        actions.pushBack(TargetedAction::create(rod, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.5));
        
        //つりざお振る
        actions.pushBack(TargetedAction::create(rod, RotateBy::create(1, 50)));
        actions.pushBack(DelayTime::create(.3));
        actions.pushBack(TargetedAction::create(rod, Spawn::create(RotateBy::create(.2, -50),
                                                                   createSoundAction("swing_0.mp3"), NULL)));

        //つりざおピクピク
        auto pikupikuSequence = Sequence::create(DelayTime::create(1), jumpAction(parent->getContentSize().height*.01, .1, rod, 1.5, 3), NULL);
        actions.pushBack(Repeat::create(pikupikuSequence, 3));
        actions.pushBack(DelayTime::create(.7));
        
        //釣り上げる
        auto end = createSpriteToCenter("sup_64_fishingrod_fish.png", true, parent, true);
        auto changeAction = createChangeAction(.3, .3, rod, end);
        
        Vector<FiniteTimeAction*> fishRemoveActions;
        for (int i=0; i<3; i++) {
            auto fileName = StringUtils::format("sup_64_fish_%d.png", i+1);
            auto fish = parent->getChildByName<Sprite*>(fileName.c_str());
            
            fishRemoveActions.pushBack(TargetedAction::create(fish, FadeOut::create(.5)));
            auto call = CallFunc::create([fish](){
                fish->stopAllActions();
            });
            fishRemoveActions.pushBack(call);
        }
        actions.pushBack(Spawn::create(changeAction, createSoundAction("bashaa.mp3"), Spawn::create(fishRemoveActions), NULL));

        //完了通知
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark スコップで掘る
    else if(key == "scoop") {
        Vector<FiniteTimeAction*> actions;
        Common::playSE("p.mp3");
        auto scop=AnchorSprite::create(Vec2(.61, .375), parent->getContentSize(), "sup_60_scoop.png");
        scop->setOpacity(0);
        parent->addChild(scop);
        
        //animate
        auto distance=parent->getContentSize().width*.15;
        auto angle = 5;
        auto duration_dig = .25;
        auto first = Sequence::create(RotateBy::create(.1, -angle),
                                      MoveBy::create(.5, Vec2(distance, distance)),
                                      FadeIn::create(.5), NULL);
        
        auto dig = Repeat::create(Sequence::create(EaseIn::create(Spawn::create(MoveBy::create(duration_dig, Vec2(-distance, -distance)),
                                                                              RotateBy::create(duration_dig, angle), NULL),1.5),
                                                 CallFunc::create([]{Common::playSE("dig.mp3");}),DelayTime::create(.3),
                                                 EaseOut::create(Spawn::create(MoveBy::create(duration_dig, Vec2(distance, distance)),
                                                                               RotateBy::create(duration_dig, -angle), NULL), 1.5), NULL), 3);
        
        auto fadeout=FadeOut::create(.5);
        auto seq_scop=TargetedAction::create(scop, Sequence::create(first,dig,fadeout, NULL));
        
        actions.pushBack(seq_scop);
        
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark リスを逃す
    else if (key == "releaseSquirre") {
        
        Vector<FiniteTimeAction*> actions;
        
        //back53に遷移
        auto back53 = createSpriteToCenter("back_53.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back53, 53);
        actions.pushBack(TargetedAction::create(back53, FadeIn::create(.5)));
        
        //扉を開ける。→リス逃走
        auto lock = back53->getChildByName<Sprite*>("sup_53_padlock.png");
        auto close_squirre = back53->getChildByName<Sprite*>("sup_53_close.png");
        auto open_squirre = createSpriteToCenter("sup_53_open.png", true, back53, true);
        auto squirre = back53->getChildByName<Sprite*>("sup_53_squirre.png");
        auto change_close_open = Spawn::create(createChangeAction(.5, .5, close_squirre, open_squirre),
                                               createSoundAction("olddoor_1.mp3"), NULL);
        auto runSquirre = Spawn::create(TargetedAction::create(squirre, FadeOut::create(.5)),
                                        createSoundAction("squirre.mp3"), NULL);
      
        actions.pushBack(Sequence::create(TargetedAction::create(lock, FadeOut::create(.5)),
                                          DelayTime::create(.5),
                                          change_close_open,
                                          DelayTime::create(1),
                                          runSquirre, NULL));
        
        //完了通知
        auto call=CallFunc::create([callback]{
            DataManager::sharedManager()->setOtherFlag("squirre", 1);
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
}

#pragma mark - Item
void MomijiActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造
   
}

#pragma mark - Custom
void MomijiActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    if (key == CustomAction_Title){
        clearParticleAction(parent);
    }
}

#pragma mark - Show Answer
void MomijiActionManager::showAnswerAction(std::string key, cocos2d::Node *parent)
{
    
}

#pragma mark - Private

ParticleSystemQuad* MomijiActionManager::showFireParticle(cocos2d::Node *parent,int backID)
{
    auto clipName = "clip";
    auto fireName = "fire";
    auto clipNode = parent->getChildByName<ClippingNode*>(clipName);
    
    if (clipNode != NULL) {
        auto particle = clipNode->getChildByName<ParticleSystemQuad*>(fireName);
        if (particle != NULL) {//あるなら貼らずに返す。
            log("炎パーティクルあるから返すよ。");
            return particle;
        }
    }
    
    auto clipImageName = StringUtils::format("sup_%d_clip.png",backID);
    auto stencil=createSpriteToCenter(clipImageName, false, parent);
    auto clip=ClippingNode::create(stencil);
    clip->setName(clipName);
    clip->setInverted(false);
    clip->setAlphaThreshold(0.0f);
    
    auto particle=ParticleSystemQuad::create("fire.plist");
    particle->setName(fireName);
    particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
    particle->setAutoRemoveOnFinish(true);
    particle->setPosition(parent->getContentSize().width*.5, parent->getContentSize().height*.55);
    particle->setTotalParticles(150);
    particle->setStartColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 150));
    particle->setPosVar(Vec2(parent->getContentSize().width*.075, parent->getContentSize().height*.05));
    particle->setStartSize(parent->getContentSize().width*.075);
    particle->setSpeed(parent->getContentSize().height*.08);
    
    clip->addChild(particle);
    parent->addChild(clip);
    
    return particle;
}

ParticleSystemQuad* MomijiActionManager::showDefaultSmokeParticle(cocos2d::Node *parent, bool isNew)
{
    auto smokeName = "smokeParticle";
    auto particle = parent->getChildByName<ParticleSystemQuad*>(smokeName);
    if (particle != NULL && !isNew) {//あるなら貼らずに返す。
        log("煙パーティクルあるから返すよ。");
        return particle;
    }
    
    log("煙を新規作成");
    particle=ParticleSystemQuad::create("smoke.plist");
    particle->setAutoRemoveOnFinish(true);
    particle->setName(smokeName);
    particle->setPosition(parent->getContentSize().width/2, parent->getContentSize().height*.5);
    particle->setPosVar(Vec2(parent->getContentSize().width*.2, 0));
    particle->setSpeed(parent->getContentSize().height*.1);
    particle->setTotalParticles(50);
    particle->setAngle(90);
    particle->setAngleVar(10);
    particle->setLife(2);
    particle->setStartSize(parent->getContentSize().width*.1);
    particle->setEndSize(parent->getContentSize().width*.2);
    particle->setLocalZOrder(2);
    parent->addChild(particle);

    return particle;
}

ParticleSystemQuad* MomijiActionManager::showBakedSmokeParticle(cocos2d::Node *parent, bool isNew)
{
    auto particle = showDefaultSmokeParticle(parent, isNew);
    particle->setPositionY(parent->getContentSize().height*.47);
    particle->setTotalParticles(150);
    particle->setLife(2.3);
    
    return particle;
}

FiniteTimeAction* MomijiActionManager::fanAction(Node *parent, ParticleSystemQuad *smoke)
{
    Vector<FiniteTimeAction*> actions;
    
    auto pig = parent->getChildByName<Sprite*>("sup_57_pig_1.png");
    auto pighand = parent->getChildByName<Sprite*>("sup_57_pig_1_hand.png");

    //煙パーティクル
    log("ファンアクション");
    auto smokeParticle = smoke == NULL? showDefaultSmokeParticle(parent, false) : smoke;//既存の表示中のパーティクルを取得
    smokeParticle->setLife(2.5);

    //一歩引く豚と入れ替わり
    auto pig2 = createSpriteToCenter("sup_57_pig_2.png", true, parent, true);
    actions.pushBack(Spawn::create(TargetedAction::create(pighand, FadeOut::create(.5)),
                                   createChangeAction(.5, .5, pig, pig2), NULL));
    
    //豚の咆哮
    actions.pushBack(createSoundAction("pig.mp3"));
    
    //うちわを振る
    auto fanhand = createSpriteToCenter("sup_57_pig_2_hand.png", true, parent, true);
    auto delay = DelayTime::create(.07);
    auto fanSequence = TargetedAction::create(fanhand, Sequence::create(FadeIn::create(.01), createSoundAction("bohu.mp3") , delay, FadeOut::create(.01), delay->clone(), NULL));
    auto repeatFan = Repeat::create(fanSequence, 10);
    actions.pushBack(repeatFan);
    
    //煙の風向きを変える。
    auto changeSmoke = CallFunc::create([smokeParticle](){
        log("チェンジm");
        smokeParticle->setGravity(Vec2(-120, 0));
    });
    actions.pushBack(changeSmoke);
    
    //継続的に振る.2倍
    actions.pushBack(repeatFan->clone());
    actions.pushBack(repeatFan->clone());

    //back_22に遷移
    auto back22 = createSpriteToCenter("back_22.png", true, parent, true);
    back22->setLocalZOrder(2);//煙パーティクルが上に来れないように。
    SupplementsManager::getInstance()->addSupplementToMain(back22, 22);
    actions.pushBack(TargetedAction::create(back22, FadeIn::create(1)));
    actions.pushBack(DelayTime::create(2));

    //煙出てくる
    auto smoke22 = showDefaultSmokeParticle(back22, true);//新規で煙を出現
    smoke22->setTotalParticles(0);
    smoke22->setPositionX(0);
    smoke22->setGravity(Vec2(100, 0));
    actions.pushBack(CallFunc::create([smoke22](){smoke22->setTotalParticles(50);}));

    actions.pushBack(DelayTime::create(2));
    
    //猫喜び〜 & びっくり
    auto anchor = Vec2(84.0f/600.0f, 298.0f/660.0f);
    auto cat_22 = back22->getChildByName("sup_22_cat.png");
    cat_22->setAnchorPoint(anchor);
    cat_22->setPosition(anchor.x*parent->getContentSize().width, anchor.y*parent->getContentSize().height);
    auto cat_bounce = createBounceAction(cat_22, .2, .1);
    auto attention=createSpriteToCenter("sup_22_attention.png", true, back22, true);
    
    actions.pushBack(Spawn::create(cat_bounce,
                                   createSoundAction("surprise.mp3"),
                                   TargetedAction::create(attention,
                                                          Spawn::create(FadeIn::create(.1),
                                                                        jumpAction(parent->getContentSize().height*.1, .2, attention, 1.5), NULL)), NULL));
    
    actions.pushBack(DelayTime::create(.5));
    
    
    //back_57に戻りながら、豚仰ぎ、back22のパーティクルも消す。
    auto spawn_57_fan = Spawn::create(TargetedAction::create(back22, FadeOut::create(1)),
                                      Sequence::create(repeatFan->clone(), repeatFan->clone(), NULL),
                                      CallFunc::create([smoke22](){smoke22->setDuration(.3);}),NULL);
    actions.pushBack(spawn_57_fan);
    
    //猫参上
    auto cat = createSpriteToCenter("sup_57_cat.png", true, parent, true);
    auto spawn_cat_fan = Spawn::create(Sequence::create(DelayTime::create(1), createSoundAction("cat_0.mp3"), TargetedAction::create(cat, FadeIn::create(.5)), NULL), repeatFan->clone(), NULL);
    actions.pushBack(spawn_cat_fan);
    actions.pushBack(DelayTime::create(.4));
    
    //豚をうちわ受け取り時の状態に戻す。
    auto afterPig = parent->getChildByName<Sprite*>("sup_57_pig_2.png");
    auto beforePig = parent->getChildByName<Sprite*>("sup_57_pig_1.png");
    
    actions.pushBack(Spawn::create(createChangeAction(.5, .5, afterPig, beforePig),
                                   TargetedAction::create(pighand, FadeIn::create(.5)), NULL));
    
    //煙を真上に戻す
    actions.pushBack(CallFunc::create([smokeParticle](){
        smokeParticle->setGravity(Vec2(0, 0));
        DataManager::sharedManager()->setEnableFlag(16);//うちわアニメーション完了のお知らせ。
        log("うちわアニメ完了フラグ");
    }));
    
    return Sequence::create(actions);
}

void MomijiActionManager::clearParticleAction(cocos2d::Node *parent)
{
    for (int i = 0; i<2; i++) {
        auto particle=ParticleSystemQuad::create("particle_texture.plist");
        particle->setAutoRemoveOnFinish(true);
        particle->resetSystem();
        particle->setDuration(-1);
        particle->setTotalParticles(50);
        particle->setTexture(Sprite::create(StringUtils::format("momiji_%d.png",i).c_str())->getTexture());
        particle->setStartSize(parent->getContentSize().width*.06);
        particle->setStartSizeVar(parent->getContentSize().width*.005);
        particle->setEndSize(particle->getStartSize());
        particle->setSpeed(parent->getContentSize().height*.11);
        particle->setPosition(parent->getContentSize().width/2,parent->getContentSize().height+particle->getStartSize());
        particle->setPosVar(Vec2(parent->getContentSize().width*.8, 0));
        parent->addChild(particle);
    }
}
