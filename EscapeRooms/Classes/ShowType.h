//
//  ShowType.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2019/02/05.
//

#ifndef ShowType_h
#define ShowType_h

typedef enum{
    ShowType_Normal=0,
    ShowType_Answer,//答えを表示
    ShowType_Answer_Big,//答えを表示 拡大表示版
    ShowType_OnPhoto,//写真に表示 supを保持せず現状を貼るだけ
    ShowType_Hint,//ヒント表示用画像 supをはるのみ
    ShowType_Hint_Big//ヒント表示 拡大表示版
}ShowType;



#endif /* ShowType_h */
