//
//  EscapeStageSprite.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2018/07/04.
//

#include "EscapeStageSprite.h"
#include "Utils/Common.h"

EscapeStageSprite* EscapeStageSprite::createWithSpriteFileName(const std::string &spriteFileName)
{
    auto node =new EscapeStageSprite;
    
    if (node&&node->init(spriteFileName)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool EscapeStageSprite::init(const std::string &spriteFileName)
{
    log("生成するSpriteFile名は[%s]",spriteFileName.c_str());
    if (LocalMode == 2) {//TexturePacker利用時のみ読み込み方法違う
        if (!Sprite::initWithFile(spriteFileName.c_str())) {
            log("png読み込み失敗");
            if (!Sprite::initWithFile(spriteFileName.substr(0,spriteFileName.size()-3)+"jpg")) {
                log("jpgを代替使用");
                return false;
            }
        }
    } else {
        if (!Sprite::initWithSpriteFrameName(spriteFileName.c_str())) {
            if (!Sprite::initWithSpriteFrameName(spriteFileName.substr(0,spriteFileName.size()-3)+"jpg")) {
                log("jpgを代替使用");
                return false;
            }
        }
    }
   
    return true;
}

void EscapeStageSprite::prepareLocalTest()
{
    if (LocalMode == 1) {
        log("デバッグアトラスを読み込みます");
        
        for (int i=0; i<15; i++) {
            auto path = Common::getLocalTestDir();
            auto altas = StringUtils::format("%satlas%d", path.c_str(), i);
            SpriteFrameCache::getInstance()->addSpriteFramesWithFile(altas + ".plist", altas + ".png");
        }
       
        for (int i=0; i<12; i++) {
            auto path = Common::getLocalTestDir();
            auto altas = StringUtils::format("%satlas_p%d", path.c_str(), i);
            SpriteFrameCache::getInstance()->addSpriteFramesWithFile(altas + ".plist", altas + ".png");
        }
        
        for (int i=0; i<12; i++) {
            auto path = Common::getLocalTestDir();
            auto altas = StringUtils::format("%satlas_j%d", path.c_str(), i);
            SpriteFrameCache::getInstance()->addSpriteFramesWithFile(altas + ".plist", altas + ".jpg");
        }
        
    } else if (LocalMode == 2) {
        log("アトラスは使いません");
        auto localResourcesPath = Common::getLocalTestDir();
        //フォルダ名が[texture_packer]でも[texture_packer_"testName"]でもOK!
        std::vector<std::string> packerFolderNames;
        packerFolderNames.push_back(StringUtils::format("texture_packer"));
        packerFolderNames.push_back(StringUtils::format("texture_packer_%s",LocalTestTitle));
        
        //texture_packerフォルダ内のフォルダ名。新しいフォルダが増えたら、配列に追加する必要あり。
        std::vector<std::string> searchfolderNames;
        searchfolderNames.push_back("/back");
        searchfolderNames.push_back("/supplements");
        searchfolderNames.push_back("/item");
        searchfolderNames.push_back("/etc");
        searchfolderNames.push_back("/animation");
        searchfolderNames.push_back("/etc_jpg");
        searchfolderNames.push_back("/supplements_jpg");

        //サーチパス配列作成
        std::vector<std::string> searchPaths;
        
        for (auto packerName : packerFolderNames) {
            for (auto searchFolderName : searchfolderNames) {
                auto searchPashString = StringUtils::format("%s%s%s",localResourcesPath.c_str(),packerName.c_str(),searchFolderName.c_str());
                searchPaths.push_back(searchPashString);                
            }
        }
        FileUtils::getInstance()->setSearchPaths(searchPaths);
    }
}
