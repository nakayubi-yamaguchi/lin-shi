//
//  CustomerListScene.cpp
//  Regitaro2
//
//  Created by 成田凌平 on 2016/03/26.

#include "EscapeCollectionScene.h"
#include "TitleScene.h"
#include "DataManager.h"
#include "EscapeCollectionCell.h"
#include "EscapeDataManager.h"
#include "EscapeLoadingScene.h"
#include "InfoLayer.h"
#include "SettingDataManager.h"
#include "ConfirmAlertLayer.h"
#include "UIParts/NCLoadingScene.h"
#include "UIParts/MenuForScroll.h"
#include "Utils/Common.h"
#include "Utils/RewardMovieManager.h"
#include "Utils/CustomAlert.h"
#include "Utils/NotificationBridge.h"
#include "Utils/InterstitialBridge.h"
#include "Utils/ContentsAlert.h"
#include "Utils/BannerBridge.h"

#include "SettingLayer.h"

#include "Utils/AutoRenewableVerification.h"
#include "Utils/UtilsMethods.h"
#include "ShopMenuLayer.h"
#include "Utils/TextFieldAlert.h"

#include "BonusLayer.h"
#include "MyPageScene.h"
#include "AnalyticsManager.h"
#include "AchievementLayer.h"

#include "StampLayer.h"
#include "NotificationKeys.h"
#include "Utils/ContentsAlert.h"

#include "VoteLayer.h"

//このクラスで使用するか微妙
#include "Utils/FireBaseBridge.h"
#include "Utils/HttpAccessManager.h"
#include "UIParts/MenuItemScale.h"

#include "ClearScene.h"
#include "Utils/ValueHelper.h"
#include "UserDataManager.h"

using namespace cocos2d;

Scene* EscapeCollectionScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = EscapeCollectionScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

bool EscapeCollectionScene::init() {
    if (!LayerColor::initWithColor(Color4B(Common::getColorFromHex(WhiteColor)))) {
        return false;
    }
    
    setName("Collection");
    
    //起動カウントの分析
    if (DataManager::sharedManager()->isSavedDateNew(UserKey_LaunchCountDate)) {
        AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_LaunchCount);
        DataManager::sharedManager()->saveDate(UserKey_LaunchCountDate);
        DataManager::sharedManager()->incrementLoginCount();
    }
    
    m_barHeight = getContentSize().height*.1;
    playableMinIndex=INT_MAX;
    
   
    //上部メニュー
    createUpperMenu();

    //コレクション
    createContents();
    
    
    
    return true;
}

void EscapeCollectionScene::onEnterTransitionDidFinish()
{
    LayerColor::onEnterTransitionDidFinish();
    
    Common::performProcessForDebug([](){
        DataManager::sharedManager()->addCoin(300);
    }, nullptr);
    
    //本番ではいらない
    //ItemStore_plugin::ItemStore::checkAppStoreExpired(ItemID_Subscription);
    //ItemStore_plugin::ItemStore::checkIsInTimeSubscription();
    
    //複数のタイトルプレイ時
    BannerBridge::removeAllPanelAd();
    
    //初回起動時は通知許可のアラートを表示する。
    showFirstOpenAlert();
    
    /*アチーブメント
     auto ddd = EscapeDataManager::getInstance()->getAchievementDataDir();
     log("達成項目のディレクトリ%s", ddd.c_str());*/
    
    //タイマー処理
    /*
     auto timer = NCPieChart::create("circle.png");
     timer->setPosition(getContentSize()/2);
     timer->addPieChartDetails({
     {"",1234, BlueColor},
     {"",500, DataManager::sharedManager()->getColorCodeData("pink").c_str()},
     {"",100, DataManager::sharedManager()->getColorCodeData("green").c_str()}
     });
     timer->setScale(3);
     timer->showPieChart(this);
     */
    
    return;
    Common::performProcessForDebug([this](){
        
        auto delay = DelayTime::create(1);
        auto call = CallFunc::create([this](){
            auto wideband = Sprite::createWithSpriteFrameName("wideband.png");
            wideband->setColor(Common::getColorFromHex(MyColor));
            wideband->setPosition(getContentSize()/2);
            
            
            this->addChild(wideband);
            
            float indicatorAreaSquare = wideband->getContentSize().height*.8;
            
            //インジケーター
            auto indicator = NCIndicator::create();
            indicator->setScale(indicatorAreaSquare*.8/indicator->getContentSize().height);
            indicator->setPosition(indicatorAreaSquare/2, wideband->getContentSize().height/2);
            wideband->addChild(indicator);
            
            //ダウンロード中ラベル
            auto dlLabel = Label::createWithTTF("Down", HiraginoMaruFont, wideband->getContentSize().height*.45);
            dlLabel->setTextColor(Color4B(Common::getColorFromHex(WhiteColor2)));
            dlLabel->setPosition(indicatorAreaSquare+/*dlLabel->getContentSize().width/2*/(wideband->getContentSize().width-indicatorAreaSquare)/2-(indicatorAreaSquare-indicator->getContentSize().width)/2, wideband->getContentSize().height/2);
            wideband->addChild(dlLabel);
        });
        
        this->runAction(Sequence::create(delay, call, NULL));
        
        
        int num = 2;
        auto playTitleMap = EscapeDataManager::getInstance()->getTitleMapWithIndex(num);
        
        auto versionString = playTitleMap[MapKey_TitleVersion].asString();
        auto fileName = playTitleMap[MapKey_FileName].asString();
        auto directoryName = EscapeDataManager::getInstance()->transformFileToDirectory(fileName);
        
        auto resourceXDir = TitlesDir + directoryName + fileName + "_" + "resource" + "_%d";
        auto serverDir = StringUtils::format(resourceXDir.c_str(), atoi(versionString.c_str())) + ".zip"; //Titles/Airplane/airplane_resource_1.zip
        auto outDir = EscapeDataManager::getInstance()->getWritableFileDir(TitlesDir + directoryName);
        
        //outDir = StringUtils::format("%s%s_resource_%d.zip", outDir.c_str(),fileName.c_str(),atoi(versionString.c_str()));
        log("ヤッホサーバーディレクトリは%s,出力ディレクトリは%s",serverDir.c_str(), outDir.c_str());
        
        bool isExist = FileUtils::getInstance()->isDirectoryExist(outDir.c_str());
        
        bool isEsc = FileUtils::getInstance()->isFileExist(outDir.c_str());
        
        if (isExist) {
            log("存在しています。");
        }
        else {
            //FireBaseBridge::getInstance()->downloadFile(serverDir.c_str(), nullptr, nullptr);
        }
    }, nullptr);
}

#pragma mark- UI
void EscapeCollectionScene::createUpperMenu()
{
    float icon_menu_width=m_barHeight*.8;
    float icon_space=icon_menu_width*0;
    //バー
    auto barSprite = Sprite::create();
    barSprite->setTextureRect(Rect(0, 0, getContentSize().width, m_barHeight));
    barSprite->setColor(Common::getColorFromHex(BlueColor));
    addChild(barSprite);

    //黒い仕切りバーを表示
    auto blackBar = Sprite::create();
    blackBar->setTextureRect(Rect(0, 0, barSprite->getContentSize().width, 2));
    blackBar->setColor(Color3B::GRAY);
    blackBar->setPosition(barSprite->getContentSize().width/2, blackBar->getContentSize().height/2);
    barSprite->addChild(blackBar);
    
    
    //title
    auto title = Label::createWithTTF("NAKAYUBI\nESCAPE ROOMS", HiraginoMaruFont, m_barHeight*.225);
    title->setPosition(barSprite->getContentSize()/2);///(getContentSize().width-icon_menu_width*3-icon_space*2)/2,barSprite->getContentSize().height/2);
    title->enableBold();
    title->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    title->setLineHeight(title->getLineHeight()*.9);
    title->setTextColor(Color4B(Common::getColorFromHex(DataManager::sharedManager()->getColorCodeData("white"))));
    barSprite->addChild(title);
    
    //トップバーの高さ
    auto topbarMenuItem = MenuItemSprite::create(barSprite, barSprite, [this](Ref* sender){
        if (m_collectionView) {
            //スクロールがトップを表示している状態で押すと、意味不明なところに飛ぶ。
            m_collectionView->scrollToTop(1, true);
            
            //デバッグ時のみ、動画再生する。
            Common::performProcessForDebug([this](){
                //assert(false);
                //InterstitialBridge::showIS(INTERSTITIAL_APPID);
                
                //return;
                if (RewardMovieManager::sharedManager()->isPrepared()) {
                    RewardMovieManager::sharedManager()->play([](bool complete){
                        log("プレイ完了しました。%d", complete);
                        auto alert = CustomAlert::create("リワードテスト", StringUtils::format("動画の表示が完了しました。%d",complete), {"OK"}, nullptr);
                        alert->showAlert();
                    });
                }
                else {
                    auto alert = CustomAlert::create("リワードテスト", "読み込めていないよ", {"OK"}, nullptr);
                    alert->showAlert();
                }
            }, nullptr);
        }
    });
    topbarMenuItem->setPosition(getContentSize().width/2, getContentSize().height-m_barHeight/2);
    
    auto topMenu = Menu::create(topbarMenuItem, NULL);
    topMenu->setPosition(Vec2::ZERO);
    addChild(topMenu);
    
    //上部ボタン
    Vector<MenuItem*> lrMenuItems;
    
    //sprite作成lambda
    auto backSprite = [this,icon_menu_width](MenuType type, GLubyte opacity){
        auto contentsSpSize = icon_menu_width*.5;

        auto back = Sprite::create();
        back->setTextureRect(Rect(0, 0, icon_menu_width, icon_menu_width));
        back->setOpacity(0);
        
        auto menuColor = Common::getColorFromHex(WhiteColor2);
        std::string underName;
        
        //badge作成,消去lambda
        auto manageBadge=[icon_menu_width](Node*parent,bool visible){
            auto badgeName="badge";

            auto badge=parent->getChildByName(badgeName);
            if (visible&&!badge) {
                auto spr=Sprite::createWithSpriteFrameName("tab_badge.png");
                spr->setScale(icon_menu_width*.3/spr->getContentSize().width/parent->getScale());//parentにかかわらず同じ大きさになるように
                spr->setPosition(0, parent->getContentSize().height*.9);
                spr->setName(badgeName);
                parent->addChild(spr);
            }
            else if (badge&&!visible){
                parent->getChildByName(badgeName)->removeFromParent();
            }
        };
        
        if (type==MenuType_Shop)
        {//ショップ コイン画像とラベルを貼り付ける
            underName = Common::localize("SHOP", "ショップ");

            auto space = contentsSpSize*.1;
            //コインの画像
            auto coinSprite = Sprite::createWithSpriteFrameName("coin_image.png");
            coinSprite->setScale(contentsSpSize*.8/coinSprite->getContentSize().height);
            coinSprite->setPosition(icon_menu_width/2, icon_menu_width/2);
            coinSprite->setOpacity(opacity);
            back->addChild(coinSprite);
            
            //所持コインラベル
            auto coinLabel = Label::createWithTTF("", LocalizeFont, coinSprite->getBoundingBox().size.height*.75);
            coinLabel->setTextColor(Color4B(menuColor));
            coinLabel->setOpacity(opacity);
            coinLabel->setPosition(coinSprite->getPositionX()+coinSprite->getBoundingBox().size.width/2 + space +coinLabel->getContentSize().width/2, coinSprite->getPositionY());
            back->addChild(coinLabel);
            
            auto calculatePosition = [coinSprite, coinLabel, space, this](){
                auto coinText = DataManager::sharedManager()->getCoinSt();
                coinLabel->setString(coinText);
                
                //コイン画像とラベルのサイズを計算して位置をずらす
                auto totalWidth = coinSprite->getBoundingBox().size.width + space + coinLabel->getContentSize().width;
                coinSprite->setPositionX(m_barHeight/2-totalWidth/2+coinSprite->getBoundingBox().size.width/2);
                coinLabel->setPositionX(m_barHeight/2+totalWidth/2-coinLabel->getContentSize().width/2);
            };
            
            //位置を合わせる
            calculatePosition();
            
            //コイン変更時の通知を登録します。
            DataManager::sharedManager()->addChangeCoinNotification(this, [coinLabel, calculatePosition](EventCustom*event)
                                                                    {
                                                                        calculatePosition();
                                                                    });
            
            //バッジ登録
            manageBadge(coinSprite,!UserDefault::getInstance()->getBoolForKey(UserKey_ClickBeginnersPack));
            
            //通知を登録します。
            auto listner=EventListenerCustom::create(NotificationKeyClickBeginners, [manageBadge,coinSprite](EventCustom*event){
                manageBadge(coinSprite,!UserDefault::getInstance()->getBoolForKey(UserKey_ClickBeginnersPack));
            });
            Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listner, this);
        }
        else{//ショップ以外
            std::string iconName;
            if (type==MenuType_Setting) {
                underName=Common::localize("SETTING", "設定");
                iconName="setting.png";
            }
            else if (type==MenuType_Stamp) {
                underName=Common::localize("STAMP", "スタンプ");
                iconName="stampcard_icon.png";
            }
            else{
                underName=Common::localize("MISSION", "ミッション");
                iconName="trophy.png";
            }
            
            auto mainSprite = Sprite::createWithSpriteFrameName(iconName);
            mainSprite->setScale(contentsSpSize/mainSprite->getContentSize().height);
            mainSprite->setColor(menuColor);
            mainSprite->setPosition(back->getContentSize()/2);
            mainSprite->setOpacity(opacity);
            
            if (type==MenuType_Achievement) {
                auto canAddBadge=DataManager::sharedManager()->existAchievableItems();
                manageBadge(mainSprite,canAddBadge);
                //通知を登録します。
                auto listner=EventListenerCustom::create(NotificationKeyAchievementBadge, [manageBadge,mainSprite](EventCustom*event){
                    auto canAddBadge=DataManager::sharedManager()->existAchievableItems();

                    manageBadge(mainSprite,canAddBadge);
                });

                Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listner, this);
            }
            else if (type==MenuType_Stamp) {
                auto canAddBadge=(MaxGetStampCount-UserDefault::getInstance()->getIntegerForKey(StampLayer::getKeyOfStampCountToday().c_str())>0);
                manageBadge(mainSprite,canAddBadge);
                //通知を登録します。
                auto listner=EventListenerCustom::create(NotificationKeyStampBadge, [manageBadge,mainSprite](EventCustom*event){
                    auto canAddBadge=(MaxGetStampCount-UserDefault::getInstance()->getIntegerForKey(StampLayer::getKeyOfStampCountToday().c_str())>0);
                    manageBadge(mainSprite,canAddBadge);
                });
                
                Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listner, this);
            }
            
            back->addChild(mainSprite);
        }
        
        auto menuName = Label::createWithTTF(underName.c_str(), HiraginoMaruFont, m_barHeight/8);
        menuName->setPositionX(back->getBoundingBox().size.width/2);
        menuName->setPositionY(back->getBoundingBox().size.height/2-contentsSpSize*.7);
        menuName->setTextColor(Color4B(menuColor));
        menuName->enableBold();
        menuName->setOpacity(opacity);
        back->addChild(menuName);
        
        return back;
    };
    
    
    //それぞれのボタンを作っていく
    //セッティングボタン
    auto settingMenuItem = MenuItemScale::create(backSprite(MenuType_Setting, 255), backSprite(MenuType_Setting, 255/2), [this](Ref* sender){
        
        Common::playClick();
        
        auto settingLayer = SettingLayer::create(SettingType_OnCollection);
        
        auto sortedType = SettingDataManager::sharedManager()->getSortType();
        auto orderType = SettingDataManager::sharedManager()->getIsAscend();
        settingLayer->setCallback([this, sortedType, orderType](Ref* sender)
        {
            auto after_sortedType = SettingDataManager::sharedManager()->getSortType();
            auto after_orderType = SettingDataManager::sharedManager()->getIsAscend();
            if (sortedType != after_sortedType || orderType != after_orderType) {
                playableMinIndex=INT_MAX;
                EscapeDataManager::getInstance()->sortTitleDataVector();
                m_collectionView->reloadData();
            }
        });
        settingLayer->showAlert(this);
    });
    settingMenuItem->setPosition(icon_menu_width/2/*getContentSize().width-icon_menu_width*2.5-icon_space*2*/,getContentSize().height-m_barHeight/2);
    lrMenuItems.pushBack(settingMenuItem);
    
    //スタンプカードボタン
    auto stampMenuItem = MenuItemScale::create(backSprite(MenuType_Stamp, 255), backSprite(MenuType_Stamp, 255/2), [this](Ref* sender){
        
        Common::playClick();
        
        this->showStampLayer();
    });
    stampMenuItem->setPosition(icon_menu_width*1.5+icon_space*2,getContentSize().height-m_barHeight/2);
    lrMenuItems.pushBack(stampMenuItem);
    
    //ショップボタン
    auto cartItem = MenuItemScale::create(backSprite(MenuType_Shop, 255), backSprite(MenuType_Shop, 255/2),[this](Ref* sender){
        Common::playClick();
        
        this->showShopAlert();
    });
    cartItem->setPosition(getContentSize().width-m_barHeight/2, settingMenuItem->getPositionY());
    lrMenuItems.pushBack(cartItem);
    
    //アチーブメント
    auto achievementItem = MenuItemScale::create(backSprite(MenuType_Achievement, 255), backSprite(MenuType_Achievement, 255/2),[this](Ref* sender){
        Common::playClick();
        
        auto achievement = AchievementLayer::create([](Ref*sender){
            
        });
        achievement->showAlert(this);
    });
    achievementItem->setPosition(cartItem->getPosition().x-icon_menu_width-icon_space, settingMenuItem->getPositionY());
    lrMenuItems.pushBack(achievementItem);
    
    auto lrMenu = Menu::createWithArray(lrMenuItems);
    lrMenu->setPosition(Vec2::ZERO);
    addChild(lrMenu);
}

void EscapeCollectionScene::createContents()
{
    
    //投票バナー
    float voteSpHeight = 0;
    /*
    auto voteVector = EscapeDataManager::getInstance()->getVoteDataVector();
    if (voteVector.size() > 0) {
        auto voteValue = voteVector.at(0);
        auto voteMap = voteValue.asValueMap();
        VoteTerm term = EscapeDataManager::getInstance()->getVoteTerm(voteMap);
        auto themeSt = EscapeDataManager::getInstance()->getLocalizeStringForVoteWithKey(voteMap, MapKey_Theme);
        if (themeSt.size() > 0 && term != VoteTerm_Hide) {
            //投票への導線
            voteSpHeight = BannerBridge::getBannerHeight();
            auto voteSp = Sprite::createWithSpriteFrameName("battle.png");
            auto themeLabel = Label::createWithTTF(themeSt.c_str(), Common::getUsableFontPath(HiraginoMaruFont), voteSp->getContentSize().height/3.5);
            //themeLabel->enableBold();
            themeLabel->setTextColor(Color4B(Common::getColorFromHex(BlackColor)));
            //themeLabel->enableOutline(Color4B(Common::getColorFromHex(WhiteColor2)), 2);
            
            themeLabel->setPosition(voteSp->getContentSize().width/2, voteSp->getContentSize().height*.75);
            voteSp->addChild(themeLabel);
            
            auto voteMenuItem = MenuItemScale::create(voteSp, voteSp, [this, term](Ref* sender){
                Common::playClick();
                
                if (UserDataManager::getInstance()->getVoteDataMap().size() == 0 && term != VoteTerm_Result) {//投票したことがない && 結果発表じゃない。
                    
                    auto alert = ContentsAlert::create("battle.png", AlertTitle_INFORMATION, "ナカユビのキャラクターたちによる、選挙機能が実装されました！動画のご視聴もしくはコインのご利用で公約を掲げたキャラクターに投票することができます。見事勝ち抜いたキャラの公約を実際にアプリ内に反映して参りますので、是非ご参加くださいませ！\n\nなお、投票は1日に1度だけ行うことができます。", {"投票する"}, [](Ref*sender){
                        auto voteLayer = VoteLayer::create();
                        voteLayer->showAlert();
                    });
                   
                    alert->displayCloseButton();
                    alert->showAlert();
                }
                else {//投票したことある。
                    auto voteLayer = VoteLayer::create();
                    voteLayer->showAlert();
                }
            });
            voteMenuItem->setTappedScale(.95);
            voteMenuItem->setScale(voteSpHeight/voteSp->getContentSize().height);
            voteMenuItem->setPosition(Vec2(getContentSize().width/2, getContentSize().height-m_barHeight-voteSpHeight/2));
            
            auto voteMenu = Menu::create(voteMenuItem, NULL);
            voteMenu->setPosition(Vec2::ZERO);
            
            addChild(voteMenu);
        }
     }*/
    
    //コレクションビューを生成
    auto collectionViewSize=Size(getContentSize().width, getContentSize().height-m_barHeight-BannerBridge::getBannerHeight()-voteSpHeight);
    
    int inColumnCount = (NativeBridge::isTablet())? 3:2;//横にどれだけ並べるか.タブレットなら3個、スマホなら2個
    
    //レイアウト作成
    CollectionLayout layout = {15, 17, inColumnCount};
    
    //セルのサイズ
    auto width = (collectionViewSize.width-layout.spaceInColumn*(layout.inColumnCount+1))/layout.inColumnCount;
    cellSize = Size(width, width);
    
    m_collectionView = NCCollectionView::create(layout, this, collectionViewSize);
    m_collectionView->setDelegate(this);
    m_collectionView->setScrollBarEnabled(true);
    m_collectionView->setBounceEnabled(true);
    m_collectionView->setDirection(ui::ScrollView::Direction::VERTICAL);
    m_collectionView->setPosition(Vec2(0, BannerBridge::getBannerHeight()));//フッターバナーの分は開けておく
    m_collectionView->setScrollBarWidth(layout.spaceInColumn/3);
    m_collectionView->setScrollBarPositionFromCornerForVertical(Vec2(layout.spaceInColumn/2, m_collectionView->getScrollBarPositionFromCornerForVertical().y));
    
    addChild(m_collectionView);
}

#pragma mark - ルーレット
void EscapeCollectionScene::showRouletteLayer()
{
    //ログインボーナス
    auto bonus=BonusLayer::create([this]{
        this->showFirstOpenAlert();
    });
    bonus->setName("bonus");
    addChild(bonus);
}

#pragma mark - スタンプ
void EscapeCollectionScene::showStampLayer()
{
    auto stamp=StampLayer::create([this]{
        
    });
    addChild(stamp);
}

#pragma mark - ショップ
void EscapeCollectionScene::showShopAlert()
{
    auto shopMenu = ShopMenuLayer::create([](Ref*sender){
        
    });
    shopMenu->showAlert(this);
}

#pragma mark- コレクションビューのデータソース
ssize_t EscapeCollectionScene::numberOfCellsInCollectionView(NCCollectionView *collection)
{
    auto titleVector = EscapeDataManager::getInstance()->getTitleDataSortedVector();

    return titleVector.size();
}

Size EscapeCollectionScene::collectionCellSizeForIndex(NCCollectionView *collection, ssize_t idx)
{
    return cellSize;
}

CollectionViewCell* EscapeCollectionScene::collectionCellAtIndex(NCCollectionView *collection, ssize_t idx)
{
    auto key = "collectionの検査です。";
    Common::startTimeForDebug(key);

    auto fileName = EscapeDataManager::getInstance()->getTitleNameWithIndex((int)idx);
    auto fileDir = EscapeDataManager::getInstance()->getIconDirWithFile(fileName);
    
    Common::stopTimeForDebug(key);

    
    auto back = Sprite::create();
    back->setTextureRect(Rect(0, 0, cellSize.width, cellSize.height));
    back->setColor(Common::getColorFromHex(WhiteColor2));
    
    Common::stopTimeForDebug(key);

    //アイコンスプライト
    Sprite* contentsSprite = Sprite::create(fileDir);
    if (!contentsSprite) {
        contentsSprite = Sprite::createWithSpriteFrameName("logo.png");
    }
    
    Common::stopTimeForDebug(key);

    auto releaseCondition = DataManager::sharedManager()->getIsValidTimeCondition((int)idx);
    
    Common::stopTimeForDebug(key);

    //log("ファイルネーム%sリリースコンディションは%d::%d", fileName.c_str(), releaseCondition,DataManager::sharedManager()->checkTimeCondition((int)idx, MapKey_LockedDate));
    bool isRepairing = EscapeDataManager::getInstance()->isRepairing((int)idx);
    if (releaseCondition == TitleCondition_Waiting || releaseCondition == TitleCondition_LOCKED || isRepairing) {//リリース待ちのやつ、またはロック
        //グレースケールシェーダーの設定
        auto glProgram = GLProgram::createWithFilenames("shaders/grayscale.vert", "shaders/grayscale.flag");
        auto glProgmramState = GLProgramState::getOrCreateWithGLProgram(glProgram);
        contentsSprite->setGLProgramState(glProgmramState);
    }
    
    Common::stopTimeForDebug(key);

    
    contentsSprite->setScale(cellSize.width*.95/contentsSprite->getContentSize().width);
    contentsSprite->setPosition(back->getContentSize()/2);
    contentsSprite->setName(IconName);
    back->addChild(contentsSprite);
    
    Common::stopTimeForDebug(key);

    auto cell = EscapeCollectionCell::create((int)idx);
    cell->setTappedScale(.95);
    cell->setName(fileName);
    cell->setNormalImage(back);
    cell->setSelectedImage(back);
    
    Common::stopTimeForDebug(key);

    log("release condition %d",releaseCondition);
    
    //選択すべきステージを際立たせる clearしておらず、かつプレイ可能なステージ
    if (DataManager::sharedManager()->getPlayableWithNoCoins((int)idx)&&
        idx<playableMinIndex) {
        playableMinIndex=(int)idx;
        
        auto frame=Sprite::createWithSpriteFrameName("frame_stage.png");
        frame->setScale(cellSize.width/frame->getContentSize().width);
        frame->setPosition(cellSize/2);
        cell->addChild(frame);
        
        auto scale_original=frame->getScale();
        auto duration=.7;
        auto seq=Sequence::create(ScaleBy::create(0, .9),
                                  FadeTo::create(0, 100),
                                  EaseIn::create(Spawn::create(ScaleTo::create(duration, scale_original),
                                                               FadeTo::create(duration, 200), NULL), 1.5),
                                  NULL);
        
        frame->runAction(Repeat::create(seq, -1));
    }
    Common::stopTimeForDebug(key);
    
    //タイトル名を下部に表示
    auto localizedAppName = EscapeDataManager::getInstance()->getTitleLocalNameWithIndex((int)idx);
    cell->showDetail(localizedAppName.c_str());
    
    Common::stopTimeForDebug(key);

    cell->showBand();

    Common::stopTimeForDebug(key);

    return cell;
}

#pragma mark- コレクションビューデリゲート
void EscapeCollectionScene::collectionCellTouched(NCCollectionView* collection, CollectionViewCell* cell)
{
    Common::playClick();
    
    int index = (int)cell->getIdx();

    auto escapeDataManager = EscapeDataManager::getInstance();
    
    auto titleType = escapeDataManager->getTypeTitleCondition(index);
    
#pragma mark 投票なら
    if (titleType == TitleCondition_VOTE) {
        auto voteVector = EscapeDataManager::getInstance()->getVoteDataVector();
        auto voteValue = voteVector.at(0);
        auto voteMap = voteValue.asValueMap();
        
        VoteTerm term = EscapeDataManager::getInstance()->getVoteTerm(voteMap);
        if (UserDataManager::getInstance()->getVoteDataMap().size() == 0 && term != VoteTerm_Result) {//投票したことがない && 結果発表じゃない。
            
            auto alert = ContentsAlert::create("battle.png", AlertTitle_INFORMATION, "ナカユビのキャラクターたちによる、選挙機能が実装されました！動画のご視聴もしくはコインのご利用で公約を掲げたキャラクターに投票することができます。見事勝ち抜いたキャラの公約を実際にアプリ内に反映して参りますので、是非ご参加くださいませ！\n\nなお、投票は1日に1度だけ行うことができます。", {"投票する"}, [](Ref*sender){
                auto voteLayer = VoteLayer::create();
                voteLayer->showAlert();
            });
            
            alert->displayCloseButton();
            alert->showAlert();
        }
        else {//投票したことある。
            auto voteLayer = VoteLayer::create();
            voteLayer->showAlert();
        }
        return;
    }
    
    
#pragma mark 広告なら
    if (titleType == TitleCondition_AD) {
        auto message=DataManager::sharedManager()->getSystemMessage("openappstore");
        auto alert = ConfirmAlertLayer::create(ConfirmAlertType_Twice, message.c_str(), [index](Ref*ref){
            auto selectIndex=((CustomAlert*)ref)->getSelectedButtonNum();
            if (selectIndex==1) {
                auto url = EscapeDataManager::getInstance()->getAdUrl(index);
                NativeBridge::openUrl(url.c_str());
            }
        });
        
        alert->showAlert(this);
        return;
    }
    
    auto releaseCondition = DataManager::sharedManager()->getIsValidTimeCondition(index);

    Common::performProcessForDebug([&releaseCondition](){
        //releaseCondition = TitleCondition_Unplayed;
    }, nullptr);
    
#pragma mark 修理中のステージ
    if (escapeDataManager->isRepairing(index)) {
        
        auto localizeName = escapeDataManager->getTitleLocalNameWithIndex(index);
        
        auto pig = Sprite::createWithSpriteFrameName("repair_apology.png");

        float contentsHeight = pig->getContentSize().height;
        auto contentsSp = Sprite::create();
        contentsSp->setTextureRect(Rect(0, 0, contentsHeight*2, contentsHeight));
        contentsSp->setCascadeOpacityEnabled(true);
        pig->setPosition(contentsSp->getContentSize()/2);
        contentsSp->addChild(pig);
        
        auto text = StringUtils::format(DataManager::sharedManager()->getSystemMessage("repair_text").c_str(), localizeName.c_str());
        auto apoloAlert = ContentsAlert::create(contentsSp, AlertTitle_INFORMATION, text, {"OK"}, nullptr);
        apoloAlert->showAlert();
        
        return;
    }
    
#pragma mark まだ動画公開日にも達していない。
    if (releaseCondition == TitleCondition_Waiting){
        auto predate = escapeDataManager->getDateString(index, MapKey_VideoDate);
        auto releasedate = escapeDataManager->getDateString(index, MapKey_ReleaseDate);

        auto releaseSt =StringUtils::format(DataManager::sharedManager()->getSystemMessage("schedule_release").c_str(),releasedate.c_str());
        auto preSt =StringUtils::format(DataManager::sharedManager()->getSystemMessage("schedule_pre").c_str(),predate.c_str());
        auto messageSt = releaseSt + "\n" + preSt;

        auto alert = ConfirmAlertLayer::create(ConfirmAlertType_Single, messageSt, NULL);
        alert->showAlert(this);

        return;
    }

#pragma mark アプリバージョンに対応していない。アプデしろ
    if (!escapeDataManager->canPlayTitleOnAppVersion(index)){
        auto title = DataManager::sharedManager()->getSystemMessage("update");
        
        auto message = DataManager::sharedManager()->getSystemMessage("not_correspond_error");
        
        auto update = DataManager::sharedManager()->getSystemMessage("update");
        
        auto cancel = DataManager::sharedManager()->getSystemMessage("cancel");
        
        auto alert = CustomAlert::create(title, message, {cancel, update}, [](Ref* sender){
            
            auto customAlert = (CustomAlert*)sender;
            int selectedNum = customAlert->getSelectedButtonNum();
            
            if (selectedNum == 1) {//ストアに飛んでアップデートさせる。
                NativeBridge::openUrl(MyURL);
            }
        });
        alert->showAlert(this);
        return;
    }
    
#pragma mark 動画視聴開始期間。先読み期間
    if (releaseCondition == TitleCondition_Pre){
        
        auto title = DataManager::sharedManager()->getSystemMessage("pre");
        
        auto releasedate = escapeDataManager->getDateString(index, MapKey_ReleaseDate);
        auto releaseDateSt = DataManager::sharedManager()->getSystemMessage("schedule_release");
        auto releaseSt = StringUtils::format(releaseDateSt.c_str(), releasedate.c_str());
        auto preText = DataManager::sharedManager()->getSystemMessage("preText");
        auto messageSt = StringUtils::format(preText.c_str(), CoinSpot_Pre);
        auto use = DataManager::sharedManager()->getSystemMessage("use");
        auto cancel = DataManager::sharedManager()->getSystemMessage("cancel");
        
        std::vector<std::string> buttons = {cancel, use};
        
        auto apologyCode = EscapeDataManager::getInstance()->getApologyCode();
        
        //日本androidのみで使用。お詫びキーが0以外
        if (Common::isAndroid() && Common::localize("", "ja") == "ja" && apologyCode!=0)
            buttons.push_back("サポート");
        
        auto alert = CustomAlert::create(title, releaseSt + "\n" + messageSt, buttons, [this, cell, index](Ref* sender){;
            
            auto customAlert = (CustomAlert*)sender;
            int selectedNum = customAlert->getSelectedButtonNum();
            
            log("動画視聴ボタンが押されました。%d",RewardMovieManager::sharedManager()->isPrepared());
            if (selectedNum == 0)//キャンセル
                return;
            
            if (selectedNum == 2) {//日本androidのみでお詫びコード入力用
                auto apologyAl = TextFieldAlert::create("不具合のお詫び", "「脱出ゲーム - さくら」にて、コインを使用したにも関わらず正常に先行プレイを行えなかった方向けのサポート機能です。該当する方はsupport@nakayubi-corp.jpまでお問い合わせくださいませ。", {"しない", "入力"}, "コードを入力", [this, index](Ref* sender){
                    
                    auto textFieldAl = (TextFieldAlert*)sender;
                    
                    auto selectedNum = textFieldAl->getSelectedButtonNum();
                    
                    if (selectedNum == 0) {//キャンセル
                        textFieldAl->close(nullptr);
                    }
                    else {
                        auto apologyCode = EscapeDataManager::getInstance()->getApologyCode();
                        auto inputText = textFieldAl->getText();
                        
                        auto inputTextLongLong = atoll(inputText.c_str());
                        
                        log("入力されている数字は%lld::%lld", inputTextLongLong, apologyCode);
                        if (inputTextLongLong == apologyCode && inputTextLongLong != 0) {//入力数字とシリアルコードが一致,かつ入力されている
                            textFieldAl->close([this, index](Ref*sender){
                                auto contentsAl = ContentsAlert::create("purchase.png", "認証完了", "先行プレイが可能になりました。この度はご不便をおかけし申し訳ございませんでした。それでは新作をお楽しみくださいませ！", {"開始する"}, [this, index](Ref* sender){
                                    //指定インデックスをセットして、ローディングシーンに遷移。
                                    this->transition(index);
                                    
                                    //解放状態をセーブする。指定インデックスをセットする
                                    DataManager::sharedManager()->recordEternalDataOnPlay(RecordKey_PreMovie, Value(true));
                                    
                                    //イベント名を先行プレイをコインで解放用に変更してあげること
                                    auto eventName = "PreCoin";
                                    auto titleName = EscapeDataManager::getInstance()->getPlayTitleName();
                                    auto eventWithTitle = StringUtils::format("%s_%s", eventName, titleName.c_str());
                                    
                                    AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName(eventName);
                                    AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName(eventWithTitle.c_str());
                                });
                                contentsAl->showAlert(this);
                            });
                        }
                        else {//間違い
                            //シェイクビジョン
                            textFieldAl->shakeAlert();
                        }
                    }
                });
                apologyAl->showAlert();
                
                return;
            }
            
            //以下、先行プレイを行う用
            bool enoughCoin = DataManager::sharedManager()->isEnoughCoin(CoinSpot_Pre);
            log("コインが足りるかどうか%d",enoughCoin);
            
            if (enoughCoin) {//コインが十分にある
                auto confirmSt = DataManager::sharedManager()->getSystemMessage("confirm");
                auto preConfirmSt = StringUtils::format(DataManager::sharedManager()->getSystemMessage("preConfirmText").c_str(), CoinSpot_Pre);
                
                auto confirmAl = CustomAlert::create(confirmSt.c_str(), preConfirmSt.c_str(), {"NO", "YES"}, [this, index](Ref* sender){
                    
                    auto al = (ContentsAlert*)sender;
                    auto selectedNum = al->getSelectedButtonNum();
                    
                    if (selectedNum == 1) {//先行プレイする
                        //先行プレイを開始します。
                        EscapeDataManager::getInstance()->setIsPlayPre(true);
                        
                        this->transition(index);
                    }
                });
                confirmAl->showAlert(this);
            }
            else{//コインが足りない
                auto closeSt = DataManager::sharedManager()->getSystemMessage("close");
                auto shopSt = DataManager::sharedManager()->getSystemMessage("shop");
                auto shortageSt = DataManager::sharedManager()->getSystemMessage("shortageCoin");
                
                auto shortageAlert = ContentsAlert::create("shortage.png", AlertTitle_INFORMATION, shortageSt.c_str(), {closeSt.c_str(), shopSt.c_str()}, [this](Ref* sender){
                    
                    auto al = (ContentsAlert*)sender;
                    auto selectedNum = al->getSelectedButtonNum();
                    
                    if (selectedNum == 1) {
                        auto shopLayer = ShopMenuLayer::create(nullptr);
                        shopLayer->showAlert(this);
                    }
                });
                shortageAlert->showAlert(this);
            }
            
        });
        alert->showAlert(this);
        
        return;
    }
    
#pragma mark 一定期間経過でロックされている
    if (releaseCondition == TitleCondition_LOCKED){
        auto title = DataManager::sharedManager()->getSystemMessage("locked");
        
        //コイン使っても解放できないモード
        bool isAbleUnlock = (EscapeDataManager::getInstance()->getTypeTitleCondition(index)==TitleCondition_COLLABORATION)? false : true;
        auto unlockedText = isAbleUnlock? DataManager::sharedManager()->getSystemMessage("unlockedText") : DataManager::sharedManager()->getSystemMessage("unlockedText_nocoin");
        auto titleName = EscapeDataManager::getInstance()->getTitleLocalNameWithIndex(index);

        auto deadline = Common::dateWithTimeInterval(TimeUnit_Day, FireBaseBridge::getInstance()->getServerTimestamp(), 60*60*24*Period_Unlocked);//Common::dateWithTimeIntervalDate(FireBaseBridge::getInstance()->getServerTimestamp(), Period_Unlocked);
        auto deadlineSt = Common::getMonthDateSt((int)deadline);
        
        auto messageSt = isAbleUnlock? StringUtils::format(unlockedText.c_str(), titleName.c_str(), CoinSpot_Unlocked, deadlineSt.c_str()) : StringUtils::format(unlockedText.c_str(), titleName.c_str());
        
        auto use = DataManager::sharedManager()->getSystemMessage("use");
        auto close = isAbleUnlock? DataManager::sharedManager()->getSystemMessage("close") : "CLOSE";
        
        std::vector<std::string> buttons;
        buttons.push_back(close);
        
        if (isAbleUnlock) {
            buttons.push_back(use);
        }
        
        auto alert = CustomAlert::create(title, messageSt, buttons, [this, cell, index](Ref* sender){
            
            auto customAlert = (CustomAlert*)sender;
            int selectedNum = customAlert->getSelectedButtonNum();
            
            if (selectedNum == 0)//キャンセル
                return;
            
            bool enoughCoin = DataManager::sharedManager()->isEnoughCoin(CoinSpot_Unlocked);
            log("コインが足りるかどうか%d",enoughCoin);
            
            if (enoughCoin) {//コインが十分にある
                auto confirmSt = DataManager::sharedManager()->getSystemMessage("confirm");
                
                auto unlockedConfirmSt = StringUtils::format(DataManager::sharedManager()->getSystemMessage("unlockedConfirmText").c_str(), CoinSpot_Unlocked);
                
                auto confirmAl = CustomAlert::create(confirmSt.c_str(), unlockedConfirmSt.c_str(), {"NO", "YES"}, [this, index](Ref* sender){
                    
                    auto al = (ContentsAlert*)sender;
                    auto selectedNum = al->getSelectedButtonNum();
                    
                    if (selectedNum == 1) {
                        //コインを使います。
                        DataManager::sharedManager()->useCoin(CoinSpot_Unlocked);
                        
                        //コイン使用したら、解放状態をセーブする。指定インデックスをセットする
                        EscapeDataManager::getInstance()->setPlayTitleNum(index);
                        
                        ValueMap map;
                        map[RecordKey_UnlockedTimeStamp] = Value((double)FireBaseBridge::getInstance()->getServerTimestamp());
                        auto unlockedCount = DataManager::sharedManager()->getEternalRecordDataOnPlay(RecordKey_UnlockedCount);
                        map[RecordKey_UnlockedCount] = Value(unlockedCount.asInt()+1);
                        DataManager::sharedManager()->recordEternalDataMapOnPlay(map);
                        
                        log("ロックを解放した日付は%f,,解放した回数は%d",DataManager::sharedManager()->getEternalRecordDataOnPlay(RecordKey_UnlockedTimeStamp).asDouble(),DataManager::sharedManager()->getEternalRecordDataOnPlay(RecordKey_UnlockedCount).asInt());
                        
                        //イベント名を先行プレイをコインで解放用に変更してあげること
                        auto eventName = "UnlockedCoin";
                        auto titleName = EscapeDataManager::getInstance()->getPlayTitleName();
                        auto eventWithTitle = StringUtils::format("%s_%s", eventName, titleName.c_str());
                        
                        AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName(eventName);
                        AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName(eventWithTitle.c_str());
                        
                        auto useCoinSt = DataManager::sharedManager()->getSystemMessage("useCoinWithUnlocked");
                        UtilsMethods::showThanksAlert(useCoinSt.c_str(), [this ,index](Ref* sender){
                            
                            transition(index);
                            
                        });
                    }
                });
                confirmAl->showAlert(this);
            }
            else{//コインが足りない
                auto closeSt = DataManager::sharedManager()->getSystemMessage("close");
                auto shopSt = DataManager::sharedManager()->getSystemMessage("shop");
                auto shortageSt = DataManager::sharedManager()->getSystemMessage("shortageCoin");
                
                auto shortageAlert = ContentsAlert::create("shortage.png", AlertTitle_INFORMATION, shortageSt.c_str(), {closeSt.c_str(), shopSt.c_str()}, [this](Ref* sender){
                    
                    auto al = (ContentsAlert*)sender;
                    auto selectedNum = al->getSelectedButtonNum();
                    
                    if (selectedNum == 1) {
                        auto shopLayer = ShopMenuLayer::create(nullptr);
                        shopLayer->showAlert(this);
                    }
                });
                shortageAlert->showAlert(this);
            }
            
        });
        alert->showAlert(this);
        
        return;
    }
    
    
    //return;
    if (LocalMode) {
        //プレイするゲームを選択
        EscapeDataManager::getInstance()->setPlayTitleNum((int)cell->getIdx());

        for (int i=0; i<12; i++) {
            auto altas = StringUtils::format("atlas%d",i);
            if (!SpriteFrameCache::getInstance()->isSpriteFramesWithFileLoaded(altas + ".plist")) {//キャッシュから削除されていたら、追加
                SpriteFrameCache::getInstance()->addSpriteFramesWithFile(altas + ".plist", altas + ".png");
            }
        }
        
        Director::getInstance()->replaceScene(TransitionFade::create(.5, TitleScene::createScene(),Color3B::WHITE));
    }
    else{
        auto localizedAppName = EscapeDataManager::getInstance()->getTitleLocalNameWithIndex(index);
        
        bool needUpdate = DataManager::sharedManager()->isUpdateCondition(index);
        
        auto messageKey = (needUpdate)? "resource_update":"information";
        auto message = DataManager::sharedManager()->getSystemMessage(messageKey);
        
        //カウントダウンが始まっていたら、ポップアップテキストにロックされる日付を明記する。
        if (DataManager::sharedManager()->getIsValidTimeCondition(index) == TitleCondition_COUNTDOWN_TOLOCKED){
            int days = DataManager::sharedManager()->getDaysToLocked(index);

            int relockedDate = (int)Common::dateWithTimeInterval(TimeUnit_Day, FireBaseBridge::getInstance()->getServerTimestamp(), 60*60*24*days);//Common::dateWithTimeIntervalDate(FireBaseBridge::getInstance()->getServerTimestamp(), days);
            
            auto relockedDateSt = Common::getMonthDateSt(relockedDate);
            message += StringUtils::format(DataManager::sharedManager()->getSystemMessage("will_locked_date").c_str(), relockedDateSt.c_str());
        }
        
        auto messageSt = StringUtils::format(message.c_str(), localizedAppName.c_str());
      
        auto alert= ConfirmAlertLayer::create(ConfirmAlertType_Twice, messageSt, [this, index](Ref* sender){
            auto al = (ConfirmAlertLayer*)sender;
            
            if (al->getSelectedButtonNum() == 1) {
                transition(index);
            }
        });
        alert->showAlert(this);
    }
}

#pragma mark- 起動時アラート
void EscapeCollectionScene::showFirstOpenAlert()
{
    //アラートを表示するラムダ式
    auto showAlert = [this](const char *title, const char*message, const ccMenuCallback &callback){
        auto alert = CustomAlert::create(title, message, {"OK"}, callback);
        alert->setCloseDuration(.3);
        alert->setMessageLineHeight(alert->getMessageLineHeight()*1.3);
        alert->showAlert(this);
    };
    
#pragma mark 初回
    if (UserDefault::getInstance()->getBoolForKey(UserKey_FirstOpen) == false) {
        auto title = DataManager::sharedManager()->getSystemMessage("welcome");
        auto message = DataManager::sharedManager()->getSystemMessage("welcome_text");
        
        showAlert(title.c_str(), message.c_str(), [showAlert, this](Ref* sender)
                  {
                      auto finishFirstOpen = [this](){
                          //保存
                          UserDefault::getInstance()->setBoolForKey(UserKey_FirstOpen, true);
                          AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_TutorialAlert);
                          auto titleVector = EscapeDataManager::getInstance()->getTitleDataSortedVector();
                          int titleCount = (int)titleVector.size();
                          this->transition(titleCount-1);//チュートリアルは絶対最後のやつ
                      };
                      
                      Common::performProcessForDebug([this]
                                                     {//debug==2
                                                         UserDefault::getInstance()->setBoolForKey(UserKey_FirstOpen, true);
                                                         this->showFirstOpenAlert();
                                                     }, [this,finishFirstOpen]{
                                                         auto tutorialTitle = DataManager::sharedManager()->getSystemMessage("tutorial_title");
                                                         auto tutorialMessage = DataManager::sharedManager()->getSystemMessage("tutorial_promote_message");
                                                         
                                                         auto tutorialAl = CustomAlert::create(tutorialTitle.c_str(), tutorialMessage.c_str(), {"OK"}, [this,finishFirstOpen](Ref* sender){
                                                             
                                                             auto needSize = EscapeDataManager::getInstance()->getAboutAllTitleSize();
                                                             auto freeMB = NativeBridge::storageAvailableMb();
                                                             
                                                             if (freeMB > needSize *1.1) {//端末の空き容量に気持ち余裕があるなら
                                                                 auto message = StringUtils::format(DataManager::sharedManager()->getSystemMessage("download_on_tutorial").c_str(), needSize);
                                                                 auto preDownloadAlert = CustomAlert::create(AlertTitle_INFORMATION, message.c_str(), {"NO", "YES"}, [this,finishFirstOpen](Ref* sender){
                                                                     auto al = (ConfirmAlertLayer*)sender;
                                                                     if (al->getSelectedButtonNum() == 1) {//チュートリアル中にダウンロードを開始する。
                                                                         log("チュートリアル中にデータのダウンロードを開始するよ");
                                                                         //ダウンロード待機中状態に変更
                                                                         EscapeDataManager::getInstance()->setAllTitleDataDownloadStatus(AllTitleDataDownloadStatus_WAITING);
                                                                     }
                                                                     
                                                                     //どのボタン押されても遷移。
                                                                     finishFirstOpen();
                                                                 });
                                                                 preDownloadAlert->showAlert();
                                                             }
                                                             else {
                                                                 finishFirstOpen();
                                                             }
                                                         });
                                                         tutorialAl->showAlert(this);
                                                     });
                  });
    }
#pragma mark 初心者パック
    else if (DataManager::sharedManager()->canGotDailyCoin()){//デイリーボーナス
        //デイリーボーナスをプレゼント。データの保存とコインプレゼント。

        DataManager::sharedManager()->gotDailyBonus();//先にデータを操作する
        
        int bonusCoin = UserDefault::getInstance()->getIntegerForKey(UserKey_DailyBonusCoin);
        int bonusCount = UserDefault::getInstance()->getIntegerForKey(UserKey_DailyBonusCount);
        auto getcoinSt = StringUtils::format(DataManager::sharedManager()->getSystemMessage("getCoinWithNum").c_str(), bonusCoin) + "\n" + StringUtils::format(DataManager::sharedManager()->getSystemMessage("beginnersPackLastCount").c_str(), bonusCount);
        
        auto title = DataManager::sharedManager()->getSystemMessage("beginnersPack");
        auto alert = ContentsAlert::create(StringUtils::format("get_coin_%d.png",RandomHelper::random_int(0, 1)), title, getcoinSt.c_str(), {"OK"}, [this](Ref* sender){
            this->showFirstOpenAlert();
        });
        alert->showAlert(this);
    }
#pragma mark 通知登録
    else if(UserDefault::getInstance()->getBoolForKey(UserKey_RegistNotification) == false){//通知の登録促進

        auto title = DataManager::sharedManager()->getSystemMessage("recommend_info");
        std::string iosMessage = DataManager::sharedManager()->getSystemMessage("recommend_info_text");
        auto androidMessage = DataManager::sharedManager()->getSystemMessage("recommend_info_text_android");
        auto message=(SystemOS == "iOS") ? iosMessage : androidMessage;
        
        if (AnalyticsManager::getInstance()->getAnalyticsNotificationType()==AnalyticsNotificationType_Normal||
            (SystemOS != "iOS")) {
            showAlert(title.c_str(), message.c_str(), [showAlert, this](Ref* sender){
                //アラートを全て見終わったら、フラグを立てて、通知の許可アラートを出す。
                UserDefault::getInstance()->setBoolForKey(UserKey_RegistNotification, true);
                
                std::vector<std::string> topics = {Common::localize("en", "ja", "ch", "tw", "ko"), SystemOS};
                NotificationBridge::registerRemoteNotificationSetting(topics);
            });
        }
        else{
            //buttonを追加
            auto alert=CustomAlert::create(title.c_str(), message.c_str(), {"NO",Common::localize("OK", "許可")}, [this](Ref*sender){
                //OKした場合のみ、通知の許可アラートを出す。
                UserDefault::getInstance()->setBoolForKey(UserKey_RegistNotification, true);
                auto _sender=(CustomAlert*)sender;
                if (_sender->getSelectedButtonNum()==1) {
                    std::vector<std::string> topics = {Common::localize("en", "ja", "ch", "tw", "ko"), SystemOS};
                    NotificationBridge::registerRemoteNotificationSetting(topics);
                }
            });
            alert->showAlert(this);
            if (AnalyticsManager::getInstance()->getAnalyticsNotificationType()==AnalyticsNotificationType_Again_HighLight) {
                alert->setButtonColor(Color3B::ORANGE, 1);
            }
        }
    }
    //2度目 Ios&&analyticsTypeがagain&&/*ステージをチュートリアル以外で1つクリアしている*/
    else if((SystemOS == "iOS")&&
            UserDefault::getInstance()->getBoolForKey(UserKey_RegistNotification_Again) == false&&
            (AnalyticsManager::getInstance()->getAnalyticsNotificationType()==AnalyticsNotificationType_Again||
             AnalyticsManager::getInstance()->getAnalyticsNotificationType()==AnalyticsNotificationType_Again_HighLight)
    /*&&
            DataManager::sharedManager()->getTotalStageRecord(RecordKey_ClearGame)>1*/){
        
        auto title = DataManager::sharedManager()->getSystemMessage("recommend_info");
        std::string iosMessage = DataManager::sharedManager()->getSystemMessage("recommend_info_text");
        auto androidMessage = DataManager::sharedManager()->getSystemMessage("recommend_info_text_android");
        auto message=(SystemOS == "iOS") ? iosMessage : androidMessage;
        
        //一度確認アラート
        showAlert(title.c_str(), message.c_str(), [showAlert, this](Ref* sender){
            //アラートを全て見終わったら、フラグを立てて、通知の許可アラートを出す。
            UserDefault::getInstance()->setBoolForKey(UserKey_RegistNotification_Again, true);
            
            std::vector<std::string> topics = {Common::localize("en", "ja", "ch", "tw", "ko"), SystemOS};
            NotificationBridge::registerRemoteNotificationSetting(topics);
        });
    }
#pragma mark 通常
    else {
        //通知を登録する前にこれまで登録した分を削除し再登録
        for (int i=NotificationTag_Login; i < NotificationTag_Stage; i++) {
            NotificationBridge::cancelLocalNotification(i);
            NotificationBridge::fireLocalNotification(60*60*24*(i+1), DataManager::sharedManager()->getSystemMessage("notification").c_str(), i);
        }
        
        //なぜか解放されてしまうから、時間あける。
        auto delay = DelayTime::create(.1);
        auto call = CallFunc::create([this](){
            //1/2でline@、beginners packに振り分け
            int random = RandomHelper::random_int(0, 2);
            auto announceDetail = EscapeDataManager::getInstance()->getAnnounceDetail();

            if (random==0 && Common::localize("en", "ja")=="ja")
            {//line@
                int friendBonus = 3;
                UtilsMethods::showPromoteBecomingFriends(friendBonus, [friendBonus](){
                    log("友達になってくれたので、お礼をします");
                    
                    DataManager::sharedManager()->addCoin(friendBonus);
                    UtilsMethods::showThanksAlert(DataManager::sharedManager()->getSystemMessage("promoteFriendsThanksText").c_str(), nullptr);
                });
            }
            else if(random == 1 && announceDetail.canDelivery){
                int ratio = announceDetail.ratio;
                
                if (RandomHelper::random_int(1, ratio) == 1) {
                    auto spr = Sprite::createWithSpriteFrameName("purchase.png");
                    auto alert = ContentsAlert::create(spr, AlertTitle_INFORMATION, announceDetail.text, {"NO", "YES"}, [announceDetail](Ref*sender){
                        auto alert = (ContentsAlert*)sender;
                        int selectedNum = alert->getSelectedButtonNum();
                        
                        if (selectedNum == 1) {
                            NativeBridge::openUrl(announceDetail.url.c_str());
                        }
                    });
                    alert->showAlert();
                }                
            }
            else{
                UtilsMethods::showBeginnersPackRecommendAlert([this](Ref*ref){
                    auto _ref=(ContentsAlert*)ref;
                    if (_ref->getSelectedButtonNum()==1) {
                        this->showShopAlert();
                    }
                });
            }
            
        });
        runAction(Sequence::create(delay, call, NULL));
    }
}

#pragma mark- ゲームローディング画面遷移
void EscapeCollectionScene::transition(int index)
{
    EscapeDataManager::getInstance()->setPlayTitleNum(index);
    EscapeDataManager::getInstance()->getTypePlayTitleCondition();
    
    if (EscapeDataManager::getInstance()->getTypePlayTitleCondition() != TitleCondition_TUTORIAL) {
        //1回しか送られないのであんしん
        AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_ChooseFirstStage);
    }
    
    
    Director::getInstance()->replaceScene(TransitionFade::create(.5, EscapeLoadingScene::createScene(LoadingType_Titles),Color3B::WHITE));
}

#pragma mark- 購入レイヤーのデリゲート
void EscapeCollectionScene::didFinishPurchase(std::string itemID, bool isRestore)
{
    log("購入に成功しました");
    auto thanksAlert = ContentsAlert::create("purchase_sub.png", "Success", "ご購入ありがとうございます。引き続き「Escape Rooms」をお楽しみください。", {"OK"}, nullptr);
    thanksAlert->showAlert(this);
}

void EscapeCollectionScene::didHappenedError(std::string itemID, bool restore, ErrorType happenedError)
{
    auto errorAlert = CustomAlert::create("", "エラーが発生しました。", {"CLOSE"}, nullptr);
    errorAlert->showAlert(this);
    log("購入に失敗しました");
}

#pragma mark- デストラクタ
EscapeCollectionScene::~EscapeCollectionScene()
{
    log("コレクションシーンを解放");
}
