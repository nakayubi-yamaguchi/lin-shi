//
//  StarsSprites.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/08.
//
//

#include "StarsSprite.h"
#include "Utils/FireBaseBridge.h"

#include "EscapeDataManager.h"
#include "DataManager.h"

using namespace cocos2d;

StarsSprite* StarsSprite::create(float width)
{
    return create(width, SortType_Star);
}

StarsSprite* StarsSprite::create(float width, SortType sortType)
{
    auto node=new StarsSprite;
    if (node&&node->init(width, sortType)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool StarsSprite::init(float width, SortType sortType)
{
    if (!Sprite::create()) {
        return false;
    }
    m_sortType = sortType;
    setTextureRect(Rect(0, 0, width, width/5));
    setCascadeOpacityEnabled(true);
    
    starWidth=width*.175;
    starSpace=(width-starWidth*5)/6;
    
    createStarFrame();
    setRate(0);//デフォルトは0
    
    return true;
}

#pragma mark - UI
void StarsSprite::createStarFrame()
{
    auto fileName = (m_sortType == SortType_Difficulty)? "difficulty.png": "starframe.png";
    for (int i=0; i<5; i++) {
        
        auto grayFrame=Sprite::createWithSpriteFrameName(fileName);
        grayFrame->setScale(starWidth/grayFrame->getContentSize().width);
        grayFrame->setPosition((starWidth+starSpace)*(-2+i),0);
        grayFrame->setColor(Color3B(200, 200, 200));
        addChild(grayFrame);
        grayFrames.pushBack(grayFrame);
        
        auto star = Sprite::createWithSpriteFrameName(fileName);
        star->setCascadeOpacityEnabled(true);
        star->setColor(getStarColor());
        
        //マスクは重いから、プログレスバーに変更
        auto progress = ProgressTimer::create(star);
        progress->setPosition(grayFrame->getPosition());
        progress->setScale(grayFrame->getScale());
        progress->setBarChangeRate(Vec2(1, 0));
        progress->setMidpoint(Vec2(0, 0.5));
        progress->setCascadeOpacityEnabled(true);
        progress->setType(ProgressTimer::Type::BAR);        
        
        addChild(progress);
        stars.pushBack(progress);
        
        /*
        //mask用スプライト
        auto star=Sprite::createWithSpriteFrameName(fileName);
        star->setScale(frame->getScale());
        star->setPosition(frame->getPosition());
        
        auto yellow=Sprite::create();
        yellow->setColor(getStarColor());

        yellow->setTextureRect(Rect(0, 0, starWidth,starWidth));
        yellow->setAnchorPoint(Vec2(0, .5));
        yellow->setName("mask");
        yellow->setPosition(frame->getPosition().x-starWidth/2,frame->getPosition().y);

        auto clip=ClippingNode::create();
        clip->setStencil(star);
        clip->setAlphaThreshold(0);//マスクする閾値を設定 0にすれば透明箇所をマスクしない
        clip->setInverted(false);//mask箇所を反転
        clip->setCascadeOpacityEnabled(true);
        clip->addChild(yellow);
        addChild(clip);
        
        masks.pushBack(yellow);
        */
    }
}

#pragma mark - アクセサ
void StarsSprite::setRate(float _rate)
{
    rate=_rate;
    
    auto counter=0;
    
    /*
    if (masks.size()>0) {
        for (auto spr : masks) {
            spr->setTextureRect(Rect(0, 0, starWidth*MIN(1, _rate-counter), starWidth));
            
            counter++;
        }
    }*/
    
    if (stars.size()>0) {
        for (auto progress : stars) {
            //spr->setTextureRect(Rect(0, 0, starWidth*MIN(1, _rate-counter), starWidth));
            
            progress->setPercentage(MIN(1, _rate-counter) * 100);
            
            counter++;
        }
    }
}

void StarsSprite::setRate(float rate, bool animate)
{
    setRate(rate);
    
    if (animate) {
        Vector<FiniteTimeAction*>actions_frame;
        
        auto duration=.1;
        int counter=0;
        for (auto spr : stars) {
            if (counter<rate) {
                auto originalScale=starWidth/spr->getContentSize().width;

                spr->setColor(getStarColor());
                auto scale_0=ScaleTo::create(duration, originalScale*1.2);
                auto scale_1=ScaleTo::create(duration, originalScale);
                
                actions_frame.pushBack(TargetedAction::create(spr, Sequence::create(scale_0,scale_1, NULL)));
            }
            
            counter++;
        }

        runAction(Spawn::create(actions_frame));
    }
}

float StarsSprite::getRate()
{
    return rate;
}

void StarsSprite::setStarColor(cocos2d::Color3B color)
{
    for (auto spr : stars) {
        spr->setColor(color);
    }
}

Color3B StarsSprite::getStarColor()
{
    if (stars.size()>0) {
       return stars.at(0)->getColor();
    }
    
    return (m_sortType == SortType_Difficulty)? Common::getColorFromHex(DataManager::sharedManager()->getColorCodeData("red")):Color3B::YELLOW;
}

#pragma mark- パブリック
void StarsSprite::readAverageRate(int idx)
{
    auto titleName = EscapeDataManager::getInstance()->getTitleNameWithIndex(idx);
    
    //std::string directoryName = fileName + "/";
    std::transform(titleName.begin(), titleName.begin()+1, titleName.begin(), ::toupper);
    
    auto firebaseDir = StringUtils::format("%s%s/%s/", FirebaseDownloadDir, Stars, titleName.c_str());
    
    //読み取りディレクトリを名前にする。
    m_firebaseStarDir = firebaseDir;
    
    //log("星の読み取りを始めます%s",firebaseDir.c_str());
    FireBaseBridge::getInstance()->loadingValue(firebaseDir.c_str(), [this,idx](bool successed , Value value){
        
        if (this->getReferenceCount()==0) {
            return;
        }
        
        int starPhase = 5;//五段階評価
        int userCount = 0;//評価された数
        int starCount = 0;//獲得星数
        
        auto readMap = value.asValueMap();
        for (int i = 0; i < starPhase; i++) {
            auto key = StringUtils::format("%d%s",i+1,Stars);
            auto user = readMap[key.c_str()].asInt();
            userCount += user;
            
            starCount += (i+1)*user;
            //log("ユーザー%d人が星%dつを付けました",user, i+1);
            if (i == starPhase-1) {//ラスト
                if (userCount == 0)
                    userCount = 1;
                float averageRate = (float)starCount/(float)userCount;
                
                log("[%s]の平均は%f点",EscapeDataManager::getInstance()->getTitleNameWithIndex(idx).c_str(),averageRate);
                this->setRate(averageRate, false);
            }
        }
    });
}

void StarsSprite::setRateForSorted(int index)
{
    float rateForSorted;
    
    if (m_sortType == SortType_Difficulty) {
        rateForSorted = EscapeDataManager::getInstance()->getSortItemValueWithIndex(index, ClearData);
    }
    else {
        rateForSorted = EscapeDataManager::getInstance()->getSortItemValueWithIndex(index, Stars);
    }
    //log("ソート用のレートは%f",rateForSorted);
    setRate(rateForSorted);
}

Spawn* StarsSprite::getFadeoutAction(float duration)
{
    Vector<FiniteTimeAction*> starFadeoutVector;
    for (auto progress: stars) {
        auto progressFadeout = TargetedAction::create(progress, FadeOut::create(duration));
        starFadeoutVector.pushBack(progressFadeout);
    }
    
    auto selfFadeout = TargetedAction::create(this, FadeOut::create(duration));
    starFadeoutVector.pushBack(selfFadeout);
    
    return Spawn::create(starFadeoutVector);
}

#pragma mark - デストラクタ
StarsSprite::~StarsSprite()
{
    //log("スターを解放::%s:::文字列は%d", m_firebaseStarDir.c_str(),(int)m_firebaseStarDir.size());

    if (m_firebaseStarDir.size() > 0) {
        FireBaseBridge::getInstance()->removeAllObserver(m_firebaseStarDir.c_str());
    }
}
