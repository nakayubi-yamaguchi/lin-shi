//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "LaboratoryActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "PatternSprite.h"

using namespace cocos2d;

LaboratoryActionManager* LaboratoryActionManager::manager =NULL;

LaboratoryActionManager* LaboratoryActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new LaboratoryActionManager();
    }
    return manager;
}

#pragma mark - Back
void LaboratoryActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="screen"&&DataManager::sharedManager()->getEnableFlagWithFlagID(35))
    {
        Vector<FiniteTimeAction*>actions;
        
        actions.pushBack(DelayTime::create(.5));
        
        for(int i=0;i<5;i++){
            auto screen=createSpriteToCenter(StringUtils::format("sup_%d_%d.png",backID,i), true, parent);
            parent->addChild(screen);
            
            actions.pushBack(TargetedAction::create(screen, Sequence::create(FadeIn::create(.1),DelayTime::create(.2),FadeOut::create(.4), NULL)));
        }

        
        parent->runAction(Repeat::create(Sequence::create(actions),UINT_MAX));
    }
    else if(key=="chick")
    {
        Vector<FiniteTimeAction*>actions;
        auto duration=1.0;
        std::vector<float> pos_x_0s={.425,.65,.35};
        std::vector<float> pos_x_1s={.575,.5,.5};

        for (int i=0; i<3; i++) {
            auto chick=createSpriteToCenter("sup_57_chick.png", false, parent);
            chick->setPositionY(parent->getContentSize().height*(.8+.06*i));
            parent->addChild(chick);
            auto original_scale=chick->getScale();

            if (i==1) {
                chick->setScaleX(-original_scale);
            }
            
            auto move_0=EaseInOut::create(MoveTo::create(duration, Vec2(parent->getContentSize().width*pos_x_0s.at(i), chick->getPosition().y)), 1.5);
            auto turn=Sequence::create(DelayTime::create(duration/2),
                                       ScaleBy::create(duration/2, -1, 1), NULL);
            auto spawn_0=Spawn::create(move_0,turn, NULL);
            
            auto move_1=EaseInOut::create(MoveTo::create(duration, Vec2(parent->getContentSize().width*pos_x_1s.at(i), chick->getPosition().y)), 1.5);
            auto spawn_1=Spawn::create(move_1,turn->clone(), NULL);


            actions.pushBack(TargetedAction::create(chick, Sequence::create(spawn_0,spawn_1, NULL)));
        }
        
        parent->runAction(Repeat::create(Spawn::create(actions), UINT_MAX));
    }
    else if(key=="back_45"){
        Vector<FiniteTimeAction*>actions;
        
        auto posY=parent->getContentSize().height*.425;
        for (int i=0; i<6; i++) {
            if (i==1&&DataManager::sharedManager()->isPlayingMinigame()) {
                auto mistake=parent->getChildByName<Sprite*>("sup_45_mistake.png");
                mistake->setPositionY(posY);
                actions.pushBack(TargetedAction::create(mistake, Sequence::create(FadeIn::create(1),
                                                                                  MoveTo::create(1, parent->getContentSize()/2),
                                                                                  createSoundAction("kyuu.mp3"),
                                                                                  CallFunc::create([]{
                    DataManager::sharedManager()->setTemporaryFlag("mistake_kappa", 1);
                }),
                                                                                  NULL)));
                continue;
            }
            auto cat=createSpriteToCenter(StringUtils::format("sup_45_cat_%d.png",i), true, parent,true);
            cat->setPositionY(posY);
            actions.pushBack(TargetedAction::create(cat, Sequence::create(FadeIn::create(1),
                                                                          MoveTo::create(1, parent->getContentSize()/2),
                                                                          createSoundAction("cat_0.mp3"),
                                                                          DelayTime::create(1),
                                                                          MoveTo::create(1, Vec2(cat->getPosition().x, posY))
                                                                          , NULL)));
        }
        
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            parent->runAction(Sequence::create(actions.at(2),actions.at(1),actions.at(0),actions.at(5),actions.at(4),actions.at(3), NULL));
        }
        else{
            parent->runAction(Repeat::create(Sequence::create(actions.at(2),actions.at(1),actions.at(0),actions.at(5),actions.at(4),actions.at(3),DelayTime::create(2), NULL), UINT_MAX));
        }
    }
    else if(key=="mouse"&&DataManager::sharedManager()->getEnableFlagWithFlagID(35)){
        this->runMouse(parent,false);
    }
    else if(key=="clear"&&!DataManager::sharedManager()->isPlayingMinigame()){
        auto layer=SupplementsManager::getInstance()->createDisableLayer();
        Director::getInstance()->getRunningScene()->addChild(layer);
        
        auto door=parent->getChildByName<Sprite*>("sup_82_door.png");
        auto seq_door=TargetedAction::create(door, Sequence::create(DelayTime::create(1.5),
                                                                    createSoundAction("zuzuzu.mp3"),
                                                                    EaseOut::create(MoveBy::create(2, Vec2(-parent->getContentSize().width*.25, 0)), 1.5),
                                                                    DelayTime::create(1), NULL));
        auto call=CallFunc::create([this]{
            auto story = StoryLayer::create("ending", [](Ref* sen){
                EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

                auto story = StoryLayer::create("ending_1", [](Ref* sen){
                    Director::getInstance()->replaceScene(TransitionFade::create(8, ClearScene::createScene(), Color3B::WHITE));
                });
                Director::getInstance()->getRunningScene()->addChild(story);
                
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        parent->runAction(Sequence::create(seq_door,call, NULL));
    }
    else if(key=="pepper"&&DataManager::sharedManager()->isPlayingMinigame())
    {
        auto mistake=parent->getChildByName<Sprite*>("sup_10_mistake.png");
        
        auto delay=DelayTime::create(2);
        auto sound=createSoundAction("robot_move.mp3");
        auto move=TargetedAction::create(mistake, EaseIn::create(MoveBy::create(.7, Vec2(0, parent->getContentSize().height*.05)), 1.5));
        auto call=CallFunc::create([]{
            DataManager::sharedManager()->setTemporaryFlag("mistake_pepper", 1);
        });
        
        parent->runAction(Sequence::create(delay,sound,move,call, NULL));
    }
}

#pragma mark - Touch
void LaboratoryActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="crane")
    {//クレーン
        auto back=createSpriteToCenter("back_36.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        SupplementsManager::getInstance()->addSupplementToMain(back, 36);
        
        auto box_0=back->getChildByName<Sprite*>("sup_36_box_0.png");
        auto box_1=createSpriteToCenter("sup_36_box_1.png", true, parent);
        parent->addChild(box_1);
        
        //fadein
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        //change
        auto change=createChangeAction(2, 1, box_0, box_1,"weeen.mp3");
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_back,delay,change,delay->clone(),call,NULL));
    }
    else if(key=="birdHouse"){
        Common::playSE("kacha.mp3");
        //鍵
        auto key_0=createSpriteToCenter("sup_7_key_0.png", true, parent);
        parent->addChild(key_0);
        auto key_1=createSpriteToCenter("sup_7_key_1.png", true, parent);
        parent->addChild(key_1);
        
        auto action_key_0=TargetedAction::create(key_0, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto action_key_1=Spawn::create(createSoundAction("kacha.mp3"),
                                        createChangeAction(1, .5, key_0, key_1), NULL);
        
        //open
        auto open=createSpriteToCenter("sup_7_open.png", true, parent);
        parent->addChild(open);
        auto bird=createSpriteToCenter("sup_7_bird.png", true, parent);
        parent->addChild(bird);
        auto bird_1=createSpriteToCenter("sup_7_open_1.png", true, parent, true);
        
        auto fadein_open=TargetedAction::create(open, Spawn::create(FadeIn::create(1),
                                                                    createSoundAction("gacha.mp3"), NULL));
        auto fadein_bird=TargetedAction::create(bird, Spawn::create(FadeIn::create(1),
                                                                    createSoundAction("bird.mp3"), NULL));
        auto change_bird=Spawn::create(createChangeAction(1, .7, bird, bird_1),
                                       TargetedAction::create(open, FadeOut::create(1)),
                                       createSoundAction("pinpon.mp3"), NULL);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(action_key_0,delay,action_key_1,delay->clone(),fadein_open,delay->clone(),fadein_bird,delay->clone(),change_bird,delay->clone(),call, NULL));
    }
    else if(key=="pepper"){
        //パターン正解後
        auto locked=parent->getChildByName<Sprite*>("sup_11_locked.png");
        auto success=createSpriteToCenter("sup_11_success.png", true, parent);
        parent->addChild(success);
        
        auto seq_character=Sequence::create(createChangeAction(.5, .3, locked, success),
                                            TargetedAction::create(success,Repeat::create(Sequence::create(DelayTime::create(.2),
                                                                                                           FadeOut::create(.1),
                                                                                                           DelayTime::create(.2),
                                                                                                           FadeIn::create(.1),
                                                                                                           DelayTime::create(.2), NULL), 2) ), NULL);
        
        auto back_10=createSpriteToCenter("back_10.png", true, parent);
        back_10->setCascadeOpacityEnabled(true);
        parent->addChild(back_10);
        
        SupplementsManager::getInstance()->addSupplementToMain(back_10, 10);
        back_10->getChildByName("sup_10_screen_0.png")->removeFromParent();
        
        auto card=createSpriteToCenter("sup_10_card.png", true, parent);
        card->setPositionY(parent->getContentSize().height*.45);
        back_10->addChild(card);
        
        auto fadein_back=TargetedAction::create(back_10, FadeIn::create(1));
        auto seq_card=TargetedAction::create(card, Sequence::create(FadeIn::create(1),
                                                                    createSoundAction("emission.mp3"),
                                                                    MoveTo::create(1, parent->getContentSize()/2), NULL));
        auto delay=DelayTime::create(1);
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_character,fadein_back,seq_card,delay,call,NULL));
    }
    else if(key=="pc"){
        //pc正解
        auto screen=parent->getChildByName("sup_12_screen_0.png");
        Vector<FiniteTimeAction*>actions;
        auto duration=1.0;
        actions.pushBack(TargetedAction::create(screen, FadeOut::create(duration)));
        for (auto label : SupplementsManager::getInstance()->labelSupplements) {
            actions.pushBack(TargetedAction::create(label, FadeOut::create(duration)));
        }
        auto fadeouts=Spawn::create(actions);
        auto delay=DelayTime::create(1);
        
        auto open=createSpriteToCenter("sup_12_open.png", true, parent, true);
        auto fadein_open=Spawn::create(createSoundAction("kacha.mp3"),
                                       TargetedAction::create(open, FadeIn::create(1)), NULL);
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadeouts,delay->clone(),fadein_open,delay->clone(),call, NULL));
    }
    else if(key=="usb")
    {//usbさす
        Common::playSE("kacha.mp3");
        auto usb=createSpriteToCenter("sup_12_usb.png", true, parent, true);
        auto fadein_usb=TargetedAction::create(usb, FadeIn::create(1));
        
        //pcをon
        auto screen=createSpriteToCenter("sup_12_screen_1.png", true, parent, true);
        auto fadein_screen=TargetedAction::create(screen, Spawn::create(FadeIn::create(1),
                                                                        createSoundAction("robot_start.mp3"), NULL));
        auto delay=DelayTime::create(.7);
        
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_usb,delay,fadein_screen,delay->clone(),call, NULL));
    }
    else if(key=="magichand"){
        Common::playSE("p.mp3");
        auto backNum=DataManager::sharedManager()->getNowBack();
        auto magichand=createSpriteToCenter(StringUtils::format("sup_%d_magichand.png",backNum), true, parent, true);
        auto item=parent->getChildByName<Sprite*>((backNum==28?"sup_28_match.png":"sup_40_eraser.png"));
        auto original_scale=magichand->getScale();
        magichand->setScale(1.5*original_scale);
        magichand->setPositionY(parent->getContentSize().height*.25);
        auto duration_catch=1.0;
        auto fadein_magichand=TargetedAction::create(magichand, FadeIn::create(1));
        auto catch_magichand=TargetedAction::create(magichand, Spawn::create(ScaleTo::create(duration_catch, original_scale),
                                                                             MoveTo::create(duration_catch, parent->getContentSize()/2), NULL));
        auto sound=createSoundAction("kacha.mp3");
        auto delay=DelayTime::create(1);
        auto duration_move=1.0;
        auto move=Spawn::create(MoveBy::create(duration_move, Vec2(0, -parent->getContentSize().height*.1)),
                                FadeOut::create(duration_move), NULL);
        auto moves=Spawn::create(TargetedAction::create(magichand, move),
                                 TargetedAction::create(item, move->clone()), NULL);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_magichand,catch_magichand,sound,delay,moves,delay->clone(),call, NULL));
    }
    else if(key=="bear"){
        //くまが脅かす
        auto bear_sit=parent->getChildByName<Sprite*>("sup_28_bear_sit.png");
        auto bear_fear=AnchorSprite::create(Vec2(0, .65), parent->getContentSize(),true, "sup_28_bear_fear.png");
        parent->addChild(bear_fear);
        auto pos_original=bear_fear->getPosition();
        auto pos_after_x=pos_original.x-parent->getContentSize().width*.1;
        bear_fear->setPositionX(pos_after_x);
        bear_fear->setRotation(-90);
        
        auto duration=.3;
        auto fear_action_0=Spawn::create(TargetedAction::create(bear_sit, FadeOut::create(.3)),
                                         TargetedAction::create(bear_fear, Spawn::create(MoveTo::create(duration, pos_original),
                                                                                         RotateBy::create(duration, 90),
                                                                                         FadeIn::create(.2), NULL)),
                                         createSoundAction("lion.mp3"), NULL);
        auto delay=DelayTime::create(1);
        auto fear_action_1=Spawn::create(TargetedAction::create(bear_sit, FadeIn::create(.5)),
                                         TargetedAction::create(bear_fear, Spawn::create(MoveTo::create(duration, Vec2(pos_after_x, pos_original.y)),
                                                                                         RotateBy::create(duration, -90),
                                                                                         FadeOut::create(.5), NULL)),
                                          NULL);
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(fear_action_0,delay,fear_action_1,call, NULL));
    }
    else if(key=="ball"){
        Common::playSE("p.mp3");
        
        auto ball=createSpriteToCenter("sup_27_ball.png", true, parent,true);
        ball->setLocalZOrder(2);
        auto original_scale=ball->getScale();
        ball->setScale(original_scale*2);
        
        auto fadein_ball=TargetedAction::create(ball, FadeIn::create(1));
        
        auto duration_throw=.7;
        auto throw_ball=TargetedAction::create(ball, Sequence::create(fadein_ball,
                                                                   MoveBy::create(.7, Vec2(0, -parent->getContentSize().height*.1)),
                                                                   DelayTime::create(1),
                                                                   Spawn::create(createSoundAction("throw.mp3"),
                                                                                 EaseOut::create(MoveTo::create(duration_throw, Vec2(ball->getPosition().x-parent->getContentSize().width*.05, parent->getContentSize().height*1.25)), 1.5),
                                                                                 RotateBy::create(duration_throw, 360),
                                                                                 ScaleBy::create(duration_throw, .5),NULL), NULL));
        auto delay=DelayTime::create(1);
        auto fall=TargetedAction::create(ball, Sequence::create(CallFunc::create([ball]{ball->setLocalZOrder(0);}),
                                                                Spawn::create(EaseIn::create(MoveTo::create(duration_throw, Vec2(parent->getContentSize().width*.375, parent->getContentSize().height*.6)), 1.5),
                                                                               NULL),
                                                                Spawn::create(JumpTo::create(duration_throw, Vec2(ball->getPosition().x-parent->getContentSize().width*.05,parent->getContentSize().height*.5), parent->getContentSize().height*.2, 1),
                                                                              ScaleBy::create(duration_throw, .8),
                                                                              FadeOut::create(duration_throw),
                                                                              createSoundAction("pop_2.mp3"), NULL)
                                                                ,NULL));
        auto sit=parent->getChildByName<Sprite*>("sup_27_bear_sit.png");
        auto play=createSpriteToCenter("sup_27_bear_play.png", true, parent, true);
        auto bear_action=createChangeAction(1, .7, sit, play,"lion.mp3");
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(throw_ball,delay,fall,delay->clone(),bear_action,delay->clone(),call, NULL));
    }
    else if(key=="match"){
        Common::playSE("p.mp3");
        
        auto match=createSpriteToCenter("sup_72_match.png", true, parent,true);
        
        Vector<FiniteTimeAction*>actions;

        auto fadein_match=TargetedAction::create(match, FadeIn::create(1));
        actions.pushBack(fadein_match);
        
        auto distance=parent->getContentSize().height*.01;
        
        auto getFiteAction=[parent](int index){
            auto addFire=CallFunc::create([parent,index]{
                Common::playSE("huo.mp3");
                
                auto particles=DataManager::sharedManager()->getParticleVector(false, 72);
                auto particle=SupplementsManager::getInstance()->getParticle(parent, particles.at(index).asValueMap());
                parent->addChild(particle);
            });
            return addFire;
        };
        
        for (int i=0; i<4; i++) {
            auto shake=TargetedAction::create(match, Repeat::create(Sequence::create(MoveBy::create(.1, Vec2(-distance, -distance)),
                                                                                     MoveBy::create(.1, Vec2(distance, distance)), NULL), 2));
            actions.pushBack(shake);
            actions.pushBack(getFiteAction(i));
            actions.pushBack(DelayTime::create(.3));
            
            if (i<3) {
                auto move=EaseInOut::create(MoveBy::create(2, Vec2(parent->getContentSize().width*.22, 0)), 1.2);
                actions.pushBack(TargetedAction::create(match, move));
            }
        }
        
        Vector<FiniteTimeAction*> spawn_actions;
        auto fadeout_match=TargetedAction::create(match, FadeOut::create(2));
        spawn_actions.pushBack(fadeout_match);
        for (int i=0; i<4; i++) {
            auto liquid=createSpriteToCenter(StringUtils::format("sup_72_%d.png",i), true, parent, true);
            liquid->setLocalZOrder(1);
            
            spawn_actions.pushBack(TargetedAction::create(liquid, FadeIn::create(2)));
        }

        actions.pushBack(Spawn::create(spawn_actions));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="apple"){
        Common::playSE("p.mp3");
        auto apple=createSpriteToCenter("sup_41_apple.png", true, parent,true);
        auto pig=createSpriteToCenter("sup_41_pig.png", true, parent,true);
        
        auto fadein_apple=TargetedAction::create(apple, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto fadein_pig=TargetedAction::create(pig, Spawn::create(createSoundAction("pig.mp3"),
                                                                  FadeIn::create(1), NULL));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_apple,delay,fadein_pig,delay->clone(),call, NULL));
    }
    else if(key=="eggMachine"){
        //孵化器せいかいご
        auto back_25=createSpriteToCenter("back_25.png", true, parent, true);
        back_25->setCascadeOpacityEnabled(true);
        SupplementsManager::getInstance()->addSupplementToMain(back_25, 25);
        auto close=back_25->getChildByName<Sprite*>("sup_25_close.png");
        auto open=createSpriteToCenter("sup_25_open.png", true, parent,true);
        
        auto fadein_back=TargetedAction::create(back_25, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto change=createChangeAction(1, .7, close, open, "kacha.mp3");
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_back,delay,change,delay->clone(),call, NULL));
    }
    else if(key=="egg"){
        Common::playSE("p.mp3");
        
        //卵をセット
        auto egg=createSpriteToCenter("sup_25_eggs.png", true, parent, true);
        auto chicks=createSpriteToCenter("sup_25_chicks.png", true, parent,true);
        auto close=createSpriteToCenter("anim_25_close.png", true, parent,true);
        auto lid=parent->getChildByName<Sprite*>("sup_25_open.png");
        
        auto put_egg=TargetedAction::create(egg, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto change=createChangeAction(1, .7, lid, close,"kacha.mp3");
        auto timer=createSoundAction("timer.mp3");
        auto delay_1=DelayTime::create(3);
        auto change_1=Spawn::create(createChangeAction(1, .7, close, chicks, "piyo.mp3"),
                                    TargetedAction::create(lid, FadeIn::create(.7)), NULL);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(put_egg,delay,change,timer,delay_1,change_1,delay->clone(),call, NULL));
    }
    else if(key=="chicken")
    {//ひよこをあげる
        Common::playSE("p.mp3");
        
        auto chick=AnchorSprite::create(Vec2(.5, .5), parent->getContentSize(),true,StringUtils::format("sup_53_chick.png"));
        parent->addChild(chick);
        auto chicken_0=parent->getChildByName<Sprite*>("sup_53_chicken_0.png");
        auto chicken_1=createSpriteToCenter("sup_53_chicken_1.png", true, parent, true);
        auto chicken_2=createSpriteToCenter("sup_53_chicken_2.png", true, parent, true);
        auto chicken_3=createSpriteToCenter("sup_53_chicken_3.png", true, parent, true);

        //anim
        auto seq_chick=TargetedAction::create(chick, Sequence::create(FadeIn::create(1),
                                                                      createSoundAction("piyo.mp3"),
                                                                      EaseOut::create(ScaleBy::create(2, .8), 1.5), NULL));
        auto delay_short=DelayTime::create(1);
        auto change_0=Spawn::create(createChangeAction(1, .7, chicken_0, chicken_1,"pop.mp3"),
                                    TargetedAction::create(chick, FadeOut::create(1)), NULL);
        auto change_1=createChangeAction(1, .7, chicken_1, chicken_2,"pop.mp3");
        auto change_2=createChangeAction(1, .7, chicken_2, chicken_3,"chicken.mp3");
        auto delay=DelayTime::create(2);

        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(seq_chick,delay_short,change_0,delay_short->clone(),change_1,delay_short->clone(),change_2,delay,call, NULL));
    }
    else if(key=="chicken_cloud")
    {
        auto cloud=AnchorSprite::create(Vec2(.528, .31), parent->getContentSize(),true,StringUtils::format("sup_53_cloud.png"));
        parent->addChild(cloud);
        auto original_scale=cloud->getScale();
        //anim
        auto scale_0=ScaleTo::create(0, .1);
        auto spawn_0=Spawn::create(ScaleTo::create(.25, original_scale),FadeIn::create(.25), NULL);
        auto sound=createSoundAction("nyu.mp3");
        auto delay_0=DelayTime::create(1);
        auto spawn_1=Spawn::create(ScaleTo::create(.25, original_scale*.1),FadeOut::create(.25),NULL);
        auto remove=RemoveSelf::create();
        auto seq_cloud=TargetedAction::create(cloud,Sequence::create(scale_0,spawn_0,sound,delay_0,spawn_1,remove, NULL));
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(seq_cloud,call, NULL));
    }
    else if(key=="pig"){
        auto pig_0=parent->getChildByName<Sprite*>("sup_40_pig.png");
        auto pig_fear=createSpriteToCenter("sup_40_pig_fear.png", true, parent, true);
        
        auto change=Spawn::create(createChangeAction(.5, .2, pig_0, pig_fear),
                                  createBounceAction(pig_fear, .1),
                                  createSoundAction("fence.mp3"),
                                  createSoundAction("pig.mp3"), NULL);
        
        auto delay=DelayTime::create(1);
        auto reverse=createChangeAction(.7, .3, pig_fear, pig_0);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(change,delay,reverse,call, NULL));
    }
    else if(key=="card"){
        auto ok=createSpriteToCenter("sup_21_ok.png", true, parent,true);
        auto card=createSpriteToCenter("sup_21_card.png", true, parent,true);
        
        auto original_scale=card->getScale();
        card->setScale(original_scale*1.5);
        
        auto seq_card=TargetedAction::create(card, Spawn::create(ScaleTo::create(.7, original_scale),
                                                                 FadeIn::create(.7), NULL));
        auto sound_pi=createSoundAction("pi.mp3");
        auto delay=DelayTime::create(.7);
        auto fadein_ok=TargetedAction::create(ok, FadeIn::create(1));
        auto sound=createSoundAction("pinpon.mp3");
        auto fadeout_card=TargetedAction::create(card, FadeOut::create(.7));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("gacha.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_card,sound_pi,delay,fadein_ok,sound,delay->clone(),fadeout_card,delay->clone(),call, NULL));
    }
    else if(key=="cheese"){
        Common::playSE("p.mp3");
        auto cheese=createSpriteToCenter("sup_76_cheese.png", true, parent, true);
        cheese->setLocalZOrder(1);
        auto put_cheese=TargetedAction::create(cheese, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        
        auto mouse_0=parent->getChildByName<Sprite*>("sup_76_mouse_0.png");
        auto mouse_eat=createSpriteToCenter("anim_76_mouse_0.png", true, parent, true);
        
        auto moveToDish=createChangeAction(1, 1, mouse_0, mouse_eat);
        
        auto duration_eat=2.0;
        auto eat=Spawn::create(Repeat::create(swingAction(Vec2(0, parent->getContentSize().height*.01), duration_eat/8, mouse_eat, 1.5, false), 2),
                               TargetedAction::create(cheese, Sequence::create(DelayTime::create(duration_eat/2),
                                                                               FadeOut::create(duration_eat/2), NULL)),
                               createSoundAction("pop.mp3"), NULL);
        
        auto mouse_move=createSpriteToCenter("anim_76_mouse_move.png", true, parent,true);
        auto moveToWheel_1=Spawn::create(createChangeAction(1, .5, mouse_eat, mouse_move),
                                         createSoundAction("run.mp3"),
                                         TargetedAction::create(mouse_move, JumpTo::create(2, Vec2(parent->getContentSize().width*.7, parent->getContentSize().height*.5), parent->getContentSize().height*.05, 3)), NULL);
        
        auto fadeout_move=TargetedAction::create(mouse_move, FadeOut::create(.5));
        auto run_call=CallFunc::create([callback,this,parent]{
            Common::playSE("run.mp3");
            this->runMouse(parent, true);
        });
        
        //
        auto back_75=createSpriteToCenter("back_75.png", true, parent,true);
        back_75->setCascadeOpacityEnabled(true);
        back_75->setLocalZOrder(1);
        SupplementsManager::getInstance()->addSupplementToMain(back_75, 75);
        back_75->getChildByName("sup_75_mouse.png")->removeFromParent();
        createSpriteToCenter("sup_75_mouse_run.png", false, back_75,true);
        auto anim_screen=createSpriteToCenter("anim_75_screen.png", true, back_75,true);
        
        auto fadein_back=TargetedAction::create(back_75, FadeIn::create(1));
        auto seq_screen=TargetedAction::create(anim_screen, Sequence::create(createSoundAction("robot_start.mp3"),
                                                                             Repeat::create(Sequence::create(EaseOut::create(FadeIn::create(.5), 1.5),
                                                                                                             DelayTime::create(.3),
                                                                                                             EaseIn::create(FadeOut::create(.5), 1.5), NULL), 2),
                                                                             delay->clone(),
                                                                             NULL));
        auto fadeout_back=TargetedAction::create(back_75, FadeOut::create(1));
                
        auto call=CallFunc::create([callback,this,parent]{
            Common::playSE("pinpon.mp3");

            callback(true);
        });
        
        auto spawn_call=Spawn::create(fadeout_move,run_call, NULL);
        
        parent->runAction(Sequence::create(put_cheese,delay,moveToDish,delay->clone(),eat,delay->clone(),moveToWheel_1,delay->clone(),spawn_call,delay->clone(),
                                           fadein_back,delay->clone(),seq_screen,DelayTime::create(.5),fadeout_back,delay->clone(),call, NULL));
    }
    else if(key.compare(0,6,"handle")==0){
        Common::playSE("twist.mp3");
        
        auto index=atoi(key.substr(7,1).c_str());
        auto handle=parent->getChildByName<Sprite*>(StringUtils::format("sup_80_%d.png",index));
        
        auto angle=360;
        auto rotate=TargetedAction::create(handle, RotateBy::create(.5, angle));
        
        auto call=CallFunc::create([callback,this,parent]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(rotate,call, NULL));
    }
    else if(key=="putbar"){
        parent->runAction(putAction(parent, "sup_16_bar.png", "kacha.mp3", .5, callback));
    }
    else if(key=="clear"){
        auto anim_wake_0=createSpriteToCenter("anim_45_0.png", true, parent);
        parent->addChild(anim_wake_0);
        auto anim_wake_1=createSpriteToCenter("anim_45_1.png", true, parent);
        parent->addChild(anim_wake_1);
        
        auto seq_wake=Sequence::create(createSoundAction("pinpon.mp3"),
                                       TargetedAction::create(anim_wake_0, FadeIn::create(1)),
                                       DelayTime::create(1),
                                       createSoundAction("pig.mp3"),
                                       TargetedAction::create(anim_wake_1, FadeIn::create(1)), NULL);
        auto delay=DelayTime::create(1);
        auto addStory=CallFunc::create([parent]{
            auto story=StoryLayer::create("ending", [parent]{
                EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

                auto duration_delay=2;
                auto duration_fade=1;
                Vector<Sprite*>frames;
                for (int i=0; i<4; i++) {
                    auto frame=Sprite::createWithSpriteFrameName(StringUtils::format("ending_%d.png",i));
                    frame->setPosition(parent->getContentSize()/2);
                    frame->setOpacity(0);
                    parent->addChild(frame);
                    
                    frames.pushBack(frame);
                }
                
                //最初
                auto delay=DelayTime::create(.5);
                //0
                auto fadein_0=TargetedAction::create(frames.at(0), FadeIn::create(duration_fade));
                auto delay_0=DelayTime::create(duration_delay);
                auto sound_keyboard=CallFunc::create([](){
                    Common::playSE("keyboard_1.mp3");
                });
                
                //1
                auto fadein_1=TargetedAction::create(frames.at(1), FadeIn::create(duration_fade));
                
                //2
                auto fadein_2=TargetedAction::create(frames.at(2), FadeIn::create(duration_fade));
                
                //3
                frames.at(3)->setScale(.2);
                auto spawn=TargetedAction::create(frames.at(3), Spawn::create(FadeIn::create(duration_fade*1.5),EaseIn::create(ScaleTo::create(duration_fade*1.5, 1),2), NULL));
                auto sound_pig=CallFunc::create([](){
                    Common::playSE("pig.mp3");
                    Common::playSE("pig_1.mp3");
                    Common::playSE("pig_1.mp3");
                });
                
                auto call=CallFunc::create([parent](){
                    auto story = StoryLayer::create("ending1", [](Ref* sen){
                        Director::getInstance()->replaceScene(TransitionFade::create(8, ClearScene::createScene(), Color3B::WHITE));
                    });
                    Director::getInstance()->getRunningScene()->addChild(story);
                });
                
                parent->runAction(Sequence::create(delay,fadein_0,sound_keyboard,delay_0,fadein_1,sound_keyboard->clone(),delay_0->clone(),fadein_2,sound_keyboard->clone(),delay_0->clone(),spawn,sound_pig,delay_0->clone(),call, NULL));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        parent->runAction(Sequence::create(delay,seq_wake,delay->clone(),addStory, NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark ドラッグ
void LaboratoryActionManager::dragAction(std::string key,cocos2d::Node *parent, Vec2 pos)
{
    
}

#pragma mark - Item
void LaboratoryActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{
    if (key=="eraser") {
        Common::playSE("p.mp3");
        auto eraser=createSpriteToCenter("anim_eraser.png", true, parent->itemImage,true);
        auto hint=createSpriteToCenter("paper_hint.png", true, parent->itemImage,true);
        
        //move
        auto distance=parent->itemImage->getContentSize().width*.075;
        auto duration=.2;
        Vector<FiniteTimeAction*>moves;
        for (int i=1; i<6; i++) {
            auto move=MoveBy::create(duration+i*.025, Vec2(distance*i*pow(-1, i)+distance*1*(i/2), distance*.75*i*pow(-1, i)-distance*.5*(i/2)));
            moves.pushBack(move);
        }
        auto move=Sequence::create(moves);
        auto seq_eraser=TargetedAction::create(eraser, Sequence::create(FadeIn::create(1),createSoundAction("goshigoshi.mp3"),move, NULL));
        auto fadein_hint=TargetedAction::create(hint, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_eraser,fadein_hint,delay,call, NULL));
    }
}

#pragma mark - Labo特有
void LaboratoryActionManager::runMouse(cocos2d::Node *parent, bool animate)
{
    auto mouse_0=createSpriteToCenter("anim_76_mouse_run_0.png", true, parent,true);

    Vector<SpriteFrame*>frames;
    for (int i=0; i<2; i++) {
        auto spr=Sprite::createWithSpriteFrameName(StringUtils::format("anim_76_mouse_run_%d.png",i));
        frames.pushBack(spr->getSpriteFrame());
    }
    auto animation=Animation::createWithSpriteFrames(frames);
    animation->setDelayPerUnit(5.0f/60.0f);//??フレームぶん表示
    animation->setLoops(UINT_MAX);
    auto animateAction=TargetedAction::create(mouse_0, Animate::create(animation));
    
    Vector<FiniteTimeAction*>actions;
    actions.pushBack(animateAction);
    
    if (animate) {
        actions.pushBack(TargetedAction::create(mouse_0, FadeIn::create(1)));
    }else{
        mouse_0->setOpacity(255);
    }
    
    parent->runAction(Spawn::create(actions));
}
