//
//  ChocolateShopActionManager.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/11/29.
//
//

#ifndef __EscapeContainer__OmoideActionManager__
#define __EscapeContainer__OmoideActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class OmoideActionManager:public CustomActionManager
{    
public:
    static OmoideActionManager* manager;
    static OmoideActionManager* getInstance();
   
    //over ride
    int getDefaultBackNum();
    void backAction(std::string key,int backID,Node*parent,ShowType type);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void touchBeganAction(std::string key,Node*parent);
    
    void dragAction(std::string key,Node*parent,Vec2 pos);
    
    void itemBackAction(std::string key,PopUpLayer*parent);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    
    void customAction(std::string key,Node*parent);
    void storyAction(std::string key,Node*parent,const onFinished& callback);

    
    void showAnswerAction(std::string key, Node *parent, ShowType showType);
    void cameraShotAction(int backID);
    void arrowAction(std::string key,Node*parent,const onFinished& callback);


private:
    ParticleSystemQuad* showFireParticle(cocos2d::Node *parent, int backID);
    
    void showSmokeParticle(cocos2d::Node *parent, int backID);
    
    std::vector<float>bubble_scales={.3,.15,.22,.3,.15,.22};
    
    /**back.plistで使用するcustomActionは将棋さしてから使う*/
    //std::vector<Vec2> syogiPoses;
    Vec2 moveUnitVec = Vec2(.16, .146);//将棋コマの移動距離
    std::vector<Vec2> komaAnchors = {Vec2(.34, .652), Vec2(.663, .206), Vec2(.177, .503), Vec2(.827, .355),Vec2(.172, .652), Vec2(.827, .206)};


    void showBubbles(cocos2d::Node *parent, int backID,bool correct);
    FiniteTimeAction* hitFutonAction(cocos2d::Node *parent, int index, ShowType type);
    
    /**移動後の将棋位置を比率で返す*/
    Vec2 getMovedSyogiPos(cocos2d::Node *parent, int index);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
