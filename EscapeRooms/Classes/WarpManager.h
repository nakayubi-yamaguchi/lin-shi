//
//  WarpManager.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/03/24.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________WarpManager_____
#define _____PROJECTNAMEASIDENTIFIER________WarpManager_____

#include "cocos2d.h"
#include "CustomActionKeys.h"

//Warp構造体
struct Warp{
    int warpID;
    ReplaceType replaceType;
};

USING_NS_CC;

class WarpManager
{
public:
    static WarpManager* manager;
    static WarpManager* getInstance();
    std::vector<Warp>warps;
    
    Warp getWarpStruct(ValueMap map);
    void clear();
private:

};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
