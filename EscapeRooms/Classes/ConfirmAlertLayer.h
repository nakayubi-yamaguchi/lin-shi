//
//  ConfirmAlertLayer.h
//  Kebab
//
//  Created by yamaguchinarita on 2016/12/23.
//
//

#ifndef __Kebab__ConfirmAlertLayer__
#define __Kebab__ConfirmAlertLayer__

#include "cocos2d.h"
#include "Utils/CustomAlert.h"

USING_NS_CC;

typedef enum
{
    ConfirmAlertType_Single = 1,
    ConfirmAlertType_Twice
}ConfirmAlertType;


class ConfirmAlertLayer : public CustomAlert
{
public:
    
    static ConfirmAlertLayer *create(ConfirmAlertType buttonCount, std::string message, const ccMenuCallback &callback);
    bool init(ConfirmAlertType buttonCoutnt, std::string message, const ccMenuCallback &callback);
    
    ~ConfirmAlertLayer();    
};

#endif /* defined(__Kebab__ConfirmAlertLayer__) */
