//
//  MyPageScene.cpp
//  EscapeRooms
//
//  Created by yamaguchinarita on 2018/03/19.
//
//

#include "MyPageScene.h"
#include "Utils/Common.h"
#include "EscapeDataManager.h"
#include "DataManager.h"
#include "ConfirmAlertLayer.h"
#include "SettingDataManager.h"

#include "UIParts/MenuItemScale.h"
#include "Utils/CustomAlert.h"
#include "Utils/BannerBridge.h"
#include "ShopMenuLayer.h"
#include "Utils/CustomDirector.h"
#include "AchievementMenuLayer.h"


using namespace cocos2d;

Scene* MyPageScene::createScene() {
	auto scene = Scene::create();
	auto layer = MyPageScene::create();
	scene->addChild(layer);
	
    return scene;
}

bool MyPageScene::init() {
    if (!LayerColor::initWithColor(Color4B::WHITE)) {
        return false;
    }
    
    // 初期化
    createMain();
    
    createMenues();
    
    return true;
}

void MyPageScene::createMain()
{
    auto back=Sprite::createWithSpriteFrameName("mypage_image.png");
    back->setScale(getContentSize().height/back->getContentSize().height);//幅基準でめいっぱい表示
    back->setName("back");
    back->setPosition(getContentSize()/2);
    addChild(back);
    
    auto title_height=getContentSize().height*.15;
    
    auto title_back=Sprite::create();
    title_back->setTextureRect(Rect(0, 0, getContentSize().width, title_height));
    title_back->setPosition(getContentSize().width/2,getContentSize().height*.8);
    title_back->setColor(Color3B::BLACK);
    title_back->setOpacity(150);
    
    addChild(title_back);
    
    auto title=Sprite::createWithSpriteFrameName("mypage_title.png");
    title->setScale(title_height/title->getContentSize().height);//title_backの高さ基準
    title->setPosition(title_back->getPosition());
    addChild(title);
    
    //所持コインラベル
    /*
    auto coinText = DataManager::sharedManager()->getCoinSt();
    auto coinLabel = Label::createWithTTF(coinText, LocalizeFont, 100);
    coinLabel->setPosition(getContentSize()/2);
    addChild(coinLabel);
    */
    
    /*
    auto calculatePosition = [coinLabel, this](){
        auto coinText = DataManager::sharedManager()->getCoinSt();
        coinLabel->setString(coinText);
        
        //コイン画像とラベルのサイズを計算して位置をずらす
        auto totalWidth = coinSprite->getBoundingBox().size.width + space + coinLabel->getContentSize().width;
        coinSprite->setPositionX(m_barHeight/2-totalWidth/2+coinSprite->getBoundingBox().size.width/2);
        coinLabel->setPositionX(m_barHeight/2+totalWidth/2-coinLabel->getContentSize().width/2);
    };*/
    
    //位置を合わせる
    //calculatePosition();
    
    //通知を登録します。
    DataManager::sharedManager()->addChangeCoinNotification(this, [/*calculatePosition*/](EventCustom*event)
                                                            {
                                                                //calculatePosition();
                                                            });
    
}

void MyPageScene::createMenues()
{
    float menuItemHeight = getContentSize().width/6;;
    
    //背景画像を生成するラムダ式
    auto createBackSprite = [](const char* fileName)->Sprite*{
        auto backSprite = Sprite::createWithSpriteFrameName("circle.png");
        backSprite->setColor(Common::getColorFromHex(BlueColor));
        backSprite->setCascadeOpacityEnabled(true);
        
        auto itemSprite = Sprite::createWithSpriteFrameName(fileName);
        itemSprite->setPosition(backSprite->getContentSize()/2);
        itemSprite->setColor(Common::getColorFromHex(WhiteColor));
        backSprite->addChild(itemSprite);
        
        return backSprite;
    };
    
    //ラベルを作成するラムダ式
    auto createMenuLabel = [this,menuItemHeight](const char*text, MenuItem* menuItem){
        auto menuLabel = Label::createWithSystemFont(text, "", menuItemHeight/5);//createWithTTF(text, LocalizeFont, menuItemHeight/5);
        menuLabel->setTextColor(Color4B::WHITE);
        //menuLabel->enableShadow();
        //menuLabel->enableGlow(Color4B(Common::getColorFromHex(LightBlackColor)));
        menuLabel->setPosition(menuItem->getPositionX(), menuItem->getPositionY()-menuItem->getBoundingBox().size.height/2-menuLabel->getContentSize().height/2);
        addChild(menuLabel);
    };
    
    float rowCount = 4;//横に4個並べる。
    float widthSpace = (getContentSize().width-rowCount*menuItemHeight)/(rowCount+1);
    Vector<MenuItem*> menuItems;
    //メニューアイテムに共通する処理をまとめる。
    auto setupMenuItem = [&menuItems, menuItemHeight](MenuItem* menuItem, std::string name){
        menuItem->setScale(menuItemHeight/menuItem->getContentSize().height);
        menuItem->setName(name);
        menuItems.pushBack(menuItem);
    };
    
    //<!------自動削除ボタン-------!>
    auto onMenuItem = MenuItemScale::create(createBackSprite("autoDelete.png"), createBackSprite("autoDelete.png"),nullptr);
    
    auto offMenuItem = MenuItemScale::create(createBackSprite("notAutoDelete.png"), createBackSprite("notAutoDelete.png"),nullptr);
    
    auto autoDeleteMenuItem = MenuItemToggle::createWithCallback([this](Ref*sender){
        Common::playClick();
        
        auto toggle = (MenuItemToggle*)sender;
        auto on = toggle->getSelectedIndex();
        
        std::vector<std::string> messages = {
            Common::localize("Automatic cache deletion was turned off. If you play many titles, there is a possibility that the capacity of the application will enlarge.",
                             "キャッシュの自動削除をOFFにしました。多くのタイトルをプレイしているとアプリの容量が肥大する可能性があります。",
                             "",
                             "",
                             ""),
            Common::localize("Automatic cache deletion was turned on. The image data of the title once played is deleted automatically. Data to be deleted is re-downloadable.",
                             "キャッシュの自動削除をONにしました。一度プレイし終わったタイトルの画像データは自動的に削除されます。なお、削除されるデータは再ダウンロード可能です。",
                             "",
                             "",
                             ""),
        };
        auto confirmAlert = ConfirmAlertLayer::create(ConfirmAlertType_Single, messages.at(on), nullptr);
        confirmAlert->showAlert(this);
        
        SettingDataManager::sharedManager()->setAutoRemoveCathe(on);
        
    }, offMenuItem, onMenuItem, NULL);
    autoDeleteMenuItem->setCascadeOpacityEnabled(true);
    autoDeleteMenuItem->setSelectedIndex(SettingDataManager::sharedManager()->getAutoRemoveCathe());
    setupMenuItem(autoDeleteMenuItem, DataManager::sharedManager()->getSystemMessage("auto_delete_cache"));
    
    //<!------サウンドボタン-------!>
    std::vector<std::string>labelNames={"BGM","SE"};
    for (int i=0; i<2; i++) {
        auto onMenuItem = MenuItemScale::create(createBackSprite("on.png"), createBackSprite("on.png"),nullptr);
        
        auto offMenuItem = MenuItemScale::create(createBackSprite("off.png"), createBackSprite("off.png"),nullptr);
        
        auto soundMenuItem = MenuItemToggle::createWithCallback([i](Ref*sender){
            Common::playClick();
            
            auto toggle = (MenuItemToggle*)sender;
            auto stopSound = toggle->getSelectedIndex();
            
            if(i==0){
                SettingDataManager::sharedManager()->setStopBgm(stopSound);
            }
            else{
                SettingDataManager::sharedManager()->setStopSe(stopSound);
            }
        }, onMenuItem, offMenuItem, NULL);
        soundMenuItem->setCascadeOpacityEnabled(true);
        
        if(i==0){
            soundMenuItem->setSelectedIndex(SettingDataManager::sharedManager()->getStopBgm());
        }
        else{
            soundMenuItem->setSelectedIndex(SettingDataManager::sharedManager()->getStopSe());
        }
        
        setupMenuItem(soundMenuItem, labelNames.at(i));
    }
    
    //<!------ゴミ箱ボタン-------!>
    auto trashMenuItem = MenuItemScale::create(createBackSprite("trash.png"), createBackSprite("trash.png"), [this](Ref* sender){
        auto title = DataManager::sharedManager()->getSystemMessage("delete_data");
        auto message = DataManager::sharedManager()->getSystemMessage("delete_data_text");
        
        Common::playClick();
        
        auto alert = CustomAlert::create(title, message, {"Cancel","OK"}, [this](Ref* sender){
            
            auto alert = (CustomAlert*)sender;
            auto selectedNum = alert->getSelectedButtonNum();
            
            if (selectedNum == 1) {//ok
                log("削除します");
                EscapeDataManager::getInstance()->deleteAllResourceData();
                
                auto completeMessage = DataManager::sharedManager()->getSystemMessage("delete_complete");
                auto completeAlert = ConfirmAlertLayer::create(ConfirmAlertType_Single , completeMessage, nullptr);
                completeAlert->showAlert(this);
                
            }
        });
        alert->setCloseDuration(.3);
        alert->showAlert(this);
    });
    
    setupMenuItem(trashMenuItem, DataManager::sharedManager()->getSystemMessage("delete_data"));
    
    //<!------戻るボタン-------!>
    auto backMenuItem = MenuItemScale::create(createBackSprite("back.png"), createBackSprite("back.png"), [this](Ref* sender){
        Common::playClick();
        
        Director::getInstance()->popScene();
    });
    setupMenuItem(backMenuItem, "Back");

    //<!------カスタムボタン-------!>
    auto myPageCustomMenuItem = MenuItemScale::create(createBackSprite("customMypage.png"), createBackSprite("customMypage.png"), [this](Ref* sender){
        Common::playClick();
        
        for (auto child : getChildren()) {//これから、
            
            //child->setEnabled(false);
            if (child->getName() == "back") {//背景画像
                auto scale = ScaleTo::create(.2, .7);
                child->runAction(scale);
            }
            else {
                auto fadeout = FadeOut::create(.2);
                auto remove = RemoveSelf::create();
                child->runAction(Sequence::create(fadeout, remove, NULL));
            }
        }
    });
    setupMenuItem(myPageCustomMenuItem, "Custom");
    
    //<!------ショップボタン-------!>
    auto shopMenuItem = MenuItemScale::create(createBackSprite("shop.png"), createBackSprite("shop.png"), [this](Ref* sender){
        auto title = DataManager::sharedManager()->getSystemMessage("delete_data");
        auto message = DataManager::sharedManager()->getSystemMessage("delete_data_text");
        
        Common::playClick();
        
        auto shopMenu = ShopMenuLayer::create([](Ref*sender){
            
        });
        shopMenu->showAlert(this);
    });
    setupMenuItem(shopMenuItem, DataManager::sharedManager()->getSystemMessage("shop"));
    
    //<!------実績ボタン-------!>
    auto achievementMenuItem = MenuItemScale::create(createBackSprite("achievement.png"), createBackSprite("achievement.png"), [this](Ref* sender){
        Common::playClick();
        
        auto achievementMenu = AchievementMenuLayer::create([](Ref*sender){
            
        });
        achievementMenu->showAlert(this);
        
    });
    setupMenuItem(achievementMenuItem, "Achievement");
    
    for (int i=0; i<menuItems.size(); i++) {//メニューアイテムのポジションを決定
        auto menuItem = menuItems.at(i);
        int column = i/rowCount;
        int row = (int)i%(int)rowCount;
        
        menuItem->setScale(menuItemHeight/menuItem->getContentSize().height);

        menuItem->setPosition(widthSpace+menuItemHeight/2+row*(widthSpace+menuItemHeight), BannerBridge::getBannerHeight()+menuItemHeight*.9+menuItemHeight*1.4*column);
        
        createMenuLabel(menuItem->getName().c_str(), menuItem);
    }
    
    auto menu = Menu::createWithArray(menuItems);
    menu->setPosition(Vec2::ZERO);
    addChild(menu);
}
