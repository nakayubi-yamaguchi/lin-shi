//
//  EscapeDataManager.h
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/09/28.
//
//
#ifndef __EscapeContainer__EscapeDataManager__
#define __EscapeContainer__EscapeDataManager__

#include "cocos2d.h"
#include "Utils/Common.h"
#include "Utils/HttpAccessManager.h"

#define IconsDir "Icons/"
#define TitlesDir "Titles/"
#define DataListsDir "DataLists/"//ユーザーデータ

//タイトル情報内のキー.外部から使用する
#define MapKey_TitleVersion "title_version"
#define MapKey_FileName "file_name"
#define MapKey_TitleName "localized_name"
#define MapKey_NotPlayed "not_played"//これだけmapのキーではない。newが表示されてから一定期間たったらステータスを変更する。
#define MapKey_ReleaseDate "release_date"
#define MapKey_VideoDate "playable_video"
#define MapKey_LockedDate "locked_date"

//VoteInfo内
#define MapKey_Theme "theme"//(Dictionary)
#define MapKey_KeyName "key_name"//(String)
#define MapKey_ImageColorCode "imageColorCode"//(String)
#define MapKey_Candidates "candidates"//(Array)
#define MapKey_CandidateName "localized_name"//(Array)
#define MapKey_Simplicity "localized_simplicity"//(Dictionary)
#define MapKey_Detail "localized_detail"//(Dictionary)
#define MapKey_DeadlineDate "deadline_date"//(Dictionary)
#define MapKey_InterimDate "interim_date"//(Dictionary)
#define MapKey_HideDate "hide_date"//(Dictionary) テーブル表示化する前だけ使用する。


#define MapKey_Year "year"
#define MapKey_Month "month"
#define MapKey_Day "day"

#define MapKey_Star "star"
#define MapKey_Difficulty "difficulty"
#define MapKey_HintCollection "hint_collection"//全ステージの移植が完了したら、不要になるキー

//外部からの呼び出し拡張子
#define PngExt ".png"
#define JpgExt ".jpg"

#define Stars "Stars"
#define ClearData "ClearData"
#define Users "Users"
#define Votes "Votes"


#if ((DebugMode)==0)//本番用
#define FirebaseDownloadDir "Release/"
#else//デバッグ用
#define FirebaseDownloadDir "Debug/"
#endif

//同じバンド同士が同時に表示されることはない。
typedef enum {
    TitleCondition_None = 0,//該当なし:
    TitleCondition_Waiting,//動画視聴期間にも達していない期間:{*期間バンド*}
    TitleCondition_Pre,//プレ動画視聴開始期間:左:プレイしたらバンドが消える。{*期間バンド*}
    TitleCondition_New,//新作期間:左:公開日から5日経ったら消える。プレイしたらバンドが消える。{*期間バンド*}
    TitleCondition_Unplayed,//旧作期間:左:プレイしたらバンドが消える。{*期間バンド*}
    TitleCondition_UP,//右:端末にバージョンリソースがある時は消えるバンド
    TitleCondition_AD,//左:選択しても消えない。{**属性バンド**}
    TitleCondition_REMAKE,//右:選択しても消えない。{**属性バンド**}
    TitleCondition_TUTORIAL,//左:選択しても消えない。{**属性バンド**}
    TitleCondition_LOCKED,//左:期間経過でロック。{*期間バンド*}
    TitleCondition_CLEAR,//左:選択しても消えないバンド。
    TitleCondition_COLLABORATION,//左:選択しても消えない。{**属性バンド**}
    TitleCondition_CLEAR_MINIGAME,//左:選択しても消えない。
    TitleCondition_ISPLAYING,//左:選択しても消えない。
    TitleCondition_COUNTDOWN_TOLOCKED,//左:選択しても消えない。{*期間バンド*}
    TitleCondition_VOTE,//左:選択しても消えない。{**属性バンド**}
    TitleCondition_IN_REPAIRING,//右:選択しても消えない。
}TitleCondition;

typedef enum {
    AllTitleDataDownloadStatus_NONE = 0,//何もしていない状態
    AllTitleDataDownloadStatus_WAITING,//ダウンロード待機中。[チュートリアル中にダウンロードする]から[ダウンロード開始]まで。
    AllTitleDataDownloadStatus_DOWNLOADING,//ダウンロード中。
}
AllTitleDataDownloadStatus;

//新作の期間とアンロックの期間を定義。
typedef enum {
    Period_New = 5,
    Period_Unlocked = 6//当日含めると7日解放されている。
}Period;

struct AnnounceDetail{
    std::string text;
    std::string url;//遷移するURL
    bool canDelivery;//表示許可
    int ratio;//表示頻度　ratio回に1回表示
};

//投票期間
typedef enum {
    VoteTerm_NONE = 0,//-中間発表までの期間
    VoteTerm_Interim,//中間発表期間-結果発表までの期間
    VoteTerm_Result,//結果発表期間-非表示までの期間
    VoteTerm_Hide //非表示-
    
}VoteTerm;


USING_NS_CC;
typedef std::function<void()> onFinishedAddCathe;

/**Version.plistとTitleリソースのダウンロードコールバック*/
typedef std::function<void(bool successed, std::string errorDescription)> onFinishedLoadingFile;
typedef std::function<void(bool successed, std::string fileName)> onFinishedLoadingEachIcon;

class EscapeDataManager
{
public:
    static EscapeDataManager* manager;
    static EscapeDataManager* getInstance();
    
    int iconCount;
    
    //セッター
    void setPlayTitleNum(int num);
    
    /**先行リリースしようとしてる。isPre:true→先行リリース処理中。false→先行リリース処理終わり。*/
    void setIsPlayPre(bool isPre);
    bool getIsPlayPre();
    
    /**お詫びコードを取得*/
    long long getApologyCode();

#pragma mark- マップ管理関連
    //マップ情報を取得
    ValueMap getVersionValueMap();
    
#pragma mark AppInfo関連
    /**どのversion.plistを使用するか確定している時に使用する。読み込みデータをstatic保持する。例:CollectionSceneとかで使用*/
    ValueMap getAppDataMap();
#pragma mark TitleInfo関連
    /**version.plistから全てのタイトルの配列を取得。オリジナルのデータだからあんまり使わない。*/
    ValueVector getTitleDataVector();
    
    /**ソートされたタイトルデータの配列を取得。基本的にはソート後のデータ配列を使用する。*/
    ValueVector getTitleDataSortedVector();
    
    /**タイトルデータの配列をソートする。*/
    void sortTitleDataVector();
    
    /**引数インデックスタイトルのマップを返す。*/
    ValueMap getTitleMapWithIndex(int index);
    
    /**引数インデックスタイトルのマップを返す。isSorted:ソートされた配列からmapを取得するかどうか。*/
    ValueMap getTitleMapWithIndex(int index, bool isSorted);
    
    /**指定インデックスタイトルの名前を返す。*/
    std::string getTitleNameWithIndex(int index);
    
    /**指定インデックスタイトルの名前を返す。isSorted:ソートされた配列からmapを取得するかどうか。*/
    std::string getTitleNameWithIndex(int index, bool isSorted);

    /**現在プレイ中タイトルの名前を返す。*/
    std::string getPlayTitleName();
    
    /**指定インデックスタイトルのローカライズされた名前を返す。*/
    std::string getTitleLocalNameWithIndex(int index);
    std::string getTitleLocalNameWithIndex(int index, bool isSorted);

    /**現在プレイ中タイトルのバージョンを返す。*/
    std::string getPlayTitleVersion();
    
    /**タイトルのリソースが保存されているか。*/
    bool isExistTitleResource(int num);
    
    /**プレイタイトルのリソースが保存されているか。*/
    bool isExistPlayTitleResource();
    
    /**引数インデックスのタイトルデータのタイプを確認.ad,remake,tutorialかそれ以外か*/
    TitleCondition getTypeTitleCondition(int index);

    /**引数インデックスのタイトルデータのタイプを確認.ad,remake,tutorialかそれ以外か*/
    TitleCondition getTypeTitleCondition(int index, bool isSorted);
    
    /**プレイタイトルのタイトルデータのタイプを確認.ad,remake,tutorialかそれ以外か*/
    TitleCondition getTypePlayTitleCondition();
    
    /**引数インデックスのタイトルデータの公開状態を確認.Waiting, Pre, New, Locked, None。plistデータからの判定*/
    TitleCondition getReleaseTitleCondition(int index);
    
    /**引数インデックスタイトルがアプリバージョンに対応しているかどうか。*/
    bool canPlayTitleOnAppVersion(int index);
    
    /**自社広告の場合、広告のURLを返す。*/
    std::string getAdUrl(int index);
    
    /**引数インデックスタイトルの日付データをint型で取得。*/
    int getIntDateFromVersionPlist(int index, const char* conditionMapKey, int intervalDay);

    /**引数インデックスタイトルの日付データをstring型で取得。国ごとに好きなフォーマットで*/
    std::string getDateString(int index, const char* conditionMapKey);
    
    /**ソート項目の値を取得します。MapKey_Star または MapKey_Difficulty*/
    float getSortItemValueWithIndex(int index, const char* sortItemKey);

    /**ソート項目の値を取得します。MapKey_Star または MapKey_Difficulty*/
    float getSortItemValueWithIndex(int index, const char* sortItemKey, bool isSorted);
    
    /**プレイ中タイトルのヒント画面がコレクション表示に対応しているかどうか。*/
    bool getPlayTitleIsCollectionDisplayForHint();

    /**Indexタイトルが修理中かどうか*/
    bool isRepairing(int index);

    
#pragma mark VoteInfo関連
    ValueVector getVoteDataVector();

    
    /**選択した投票内容が期限切れかどうか*/
    
    /**
     選択投票内容の選択日付を現在日と比較。<=現在日ならtrue

     @param voteValueMap 選択投票内容
     @param mapKey 選択日付キー
     @return true:経過している。　false:まだ経過していない
     */
    bool isPassedVoteDate(ValueMap voteValueMap, const char* mapKey);
    
    /**
     選択投票内容の選択日付を現在日と比較。あと何日残っているか。

     @param voteValueMap 選択投票内容
     @param mapKey 選択日付キー
     @return 残り日
     */
    int getDaysToVoteDate(ValueMap voteValueMap, const char* mapKey);

    /**指定キーのローカライズテキストバリューを取得*/
    std::string getLocalizeStringForVoteWithKey(ValueMap voteValueMap, const char* key);
    
    /**投票期間状態を取得*/
    VoteTerm getVoteTerm(ValueMap voteValueMap);
    
    /**投票に関わる日付を取得*/
    std::string getDateStringForVote(ValueMap voteValueMap, const char* mapkey);
#pragma mark- ディレクトリ関連
    /**引数ファイルディレクトリの先頭に書き込み可能ディレクトリをつけて返す。*/
    std::string getWritableFileDir(const std::string fileDir);
    
    /**アイコンのディレクトリ ファイル名をつけて返す。*/
    std::string getIconDirWithFile(const std::string noExtFileName);
    
    /**引数インデックスタイトルのディレクトリを返す。*/
    std::string getTitleDirWithIndex(int index);
    
    /**現在プレイ中タイトルのディレクトリを返す。*/
    std::string getPlayTitleDir();
    
    /**指定タイトルのリソースディレクトリを返す。*/
    std::string getTitleResourceDir(int num);
    
    /**現在プレイ中タイトルのリソースディレクトリを返す。ゲームプレイ時リソース読み込み等で使用。リソースはバージョン別に持っている。*/
    std::string getPlayTitleResourceDir();
    
    /**プレイ中タイトルのリソースディレクトリ ファイル名をつけて返す。ゲームプレイ時リソース読み込み等で使用。*/
    std::string getPlayTitleResourceDirWithFile(const char* fileName);
    
    /**プレイ中タイトルのサウンドリソースディレクトリ。Titles/[タイトル名]/~resource/sounds/"サウンド名.mp3"*/
    std::string getPlayTitleSoundResourceDirWithSoundFile(const char* soundfileName);
    
    /**プレイ中タイトルの存在しているサウンドリソースディレクトリ。元積みかダウンロードリソースにあるかどうかで、dir変わる。*/
    std::string getPlayTitleSoundResourceDirWithExistSoundFile(const char* soundfileName);

    /**引数インデックスタイトルのディレクトリを返す。*/
    std::string getTitleDataDirWithIndex(int index);

    /**インデックスタイトルのディレクトリにファイル名をつけて返す。*/
    std::string getTitleDataDirWithIndexAndFile(int index, const char* fileName);
    
    /**プレイ中タイトルのディレクトリにファイル名をつけて返す。プレイデータの読み込み等で使用。*/
    std::string getPlayTitleDataDirWithFile(const char* fileName);
    
    /**インデックスタイトルのディレクトリにファイル名をつけて返す。プレイデータの保存等で使用*/
    std::string getTitleDataDirForSaveWithIndexAndFile(int index, const char* fileName);
    
    /**プレイ中タイトルのディレクトリにファイル名をつけて返す。プレイデータの保存等で使用。*/
    std::string getPlayTitleDataDirForSaveWithFile(const char* fileName);
    
    
    /**アチーブメント項目のデータディレクトリ*/
    std::string getAchievementDataDir();
    
    
    
    /**現在プレイ中タイトルがシェイクジェスチャーを利用するタイトルかどうか*/
    bool isUsedShakeListenerOnPlayTitle();
    
    /**Titles/タイトル名/Datas/にValueMapまたはValueVectorを保存*/
    void saveData(int index, const char* recordFilePath, Value savedValue);
    
    /**プレイ中のデータをTitles/タイトル名/Datas/にValueMapまたはValueVectorを保存*/
    void saveDataOnPlay(const char* recordFilePath, Value savedValue);
    
#pragma mark- ダウンロード関連
    /**"Debug or Releaseの中を全て取得する。*/
    void loadingAppData(const std::function<void(bool success)>& callback);
    
    /**起動時にアプリのアップデートが必要かどうかを確認する。*/
    bool checkNeedAppVersion();
    
    /**サーバー上のバージョンとローカルのバージョンが一致しているか確認。不一致の場合は最新のバージョン情報をダウンロードして、アイコン画像を取得。*/
    void checkVersion(const onFinishedLoadingFile& callback);
    
    /**version.plist情報をもとに、アイコン画像をローディング*/
    void loadingEachIconData(const onFinishedLoadingEachIcon& callback);
    
    /**サーバーから全てのタイトルデータをダウンロードする。ダウンロード完了は通知で受け取る。*/
    void loadingAllTitleData();
    
    /**ダウンロードが必要なステージインデックスの配列。*/
    ValueVector getNeedDownloadTitleIndexVector();
    
    /**ダウンロードに必要な空き容量*/
    int getAboutAllTitleSize();
    
    /**サーバーから指定インデックスタイトルデータのをダウンロードする。*/
    void loadingTitleData(int index, const onFinishedLoadingFile& callback);
    
    /**サーバーからプレイタイトルをダウンロードする。*/
    void loadingPlayTitleData(const onFinishedLoadingFile& callback);
    
#pragma mark- キャッシュ関連
    /**プレイするタイトルのアトラスをキャッシュに追加するか削除*/
    void manageCatheAtlas(bool isAdd, const onFinishedAddCathe& callback);
    
    /**プレイ中のリソースデータを削除*/
    void deleteResourceDataOfPlayTitle();
    
    /**全てのリソースデータの削除。*/
    void deleteAllResourceData();
    
#pragma mark- Firebaseデータ利用関連
    Value getAnnounceValue();
    AnnounceDetail getAnnounceDetail();
    
private:
    int m_playTitleNum;
    bool m_isPlayPre;//先行プレイをしようとしているフラグ。
    
    /**FirebaseDatabaseの[Debug or Release直下のデータ]*/
    CC_SYNTHESIZE(ValueMap, m_FB_AppDataMap, FB_AppDataMap);
    ValueVector m_sortedTitleVector;//ソートされたタイトル配列。
    Value m_needTitleIndexVector;//ダウンロードが必要なタイトルインデックスの配列Value
    CC_SYNTHESIZE(AllTitleDataDownloadStatus, m_allTitleDataDownloadStatus, AllTitleDataDownloadStatus);//チュートリアル中にダウンロードするかどうか。

#pragma mark- サウンド関連
    /**サウンドをプリロードする。*/
    void preloadSound(const char* fileName);
    
    /**ゲームプレイ中のサウンドはこれを介して再生する。サウンドの所在を確認し再生する。*/
    void playSound(const char* fileName, bool isBGM);

#pragma mark- プライベート
    /**ソートされる時に場所が固定されるタイトルの条件。チュートリアルもしくは広告or投票*/
    bool conditionOfFixedTitle(TitleCondition condition);
    
    /**クリアデータを持つタイトルコンディション。広告、投票以外*/
    bool hasClearDataTitleCondition(TitleCondition condition);
    
    /**orderVector:version.plistで言う何番目かが入っている配列。広告やチュートリアルが省かれた、ひっくり返したりできる配列。*/
    ValueVector sortedValueVector(std::vector<int> orderVector, bool needReverse);
    
    /**ファイル名からディレクトリ名を返す。先頭が大文字になる。airplane→Airplane*/
    std::string transformFileToDirectory(const std::string fileName);
    
    /**ダウンロードURLを取得した後、ダウンロードする*/
    void getUrlAndDownload(const std::string serverDir, const std::string outDir, const onFinishedLoadingFile& callback);
    
    /**キャッシュを操作するメソッド*/
    void operateCathe(int i, bool isAdd, const onFinishedAddCathe& callback);
    
    //void operateCathe(int i, bool isAdd, , const onFinishedAddCathe& callback);
    
    /**リソースがない、または、アップデートがあるタイトルなどチェックしてダウンロードする。*/
    void loadingNeededTitleData(int i, const std::function<void()> &callback);
    
    /**第一引数versionが第二引数needVersion以上かどうか。*/
    bool isSupportedVersion(std::string versionSt, std::string needVersionSt);
};

#endif /* defined(__EscapeContainer__EscapeDataManager__) */
