//
//  GameScene.cpp
//  Kebab
//
//  Created by 成田凌平 on 2016/12/17.
//
//

#include "GameScene.h"
#include "Utils/Common.h"
#include "NotificationKeys.h"
#include "DataManager.h"
#include "Utils/FireBaseBridge.h"
#include "Utils/InterstitialBridge.h"
#include "Utils/BannerBridge.h"
#include "DescriptionLayer.h"
#include "EscapeDataManager.h"
#include "ZOrders.h"
#include "BabyRoomActionManager.h"
#include "TutorialActionManager.h"
#include "Utils/NotificationBridge.h"
#include "AnalyticsManager.h"
#include "StoryLayer.h"
#include "SupplementsManager.h"
#include "PigHat2ActionManager.h"
#include "UIParts/NCDownloadingBar.h"
#include "UIParts/NCIndicator.h"
#include "Utils/ShakeGestureManager.h"

using namespace cocos2d;

Scene* GameScene::createScene() {
	auto scene = Scene::create();
	auto layer = GameScene::create();
	scene->addChild(layer);
	
    return scene;
}

bool GameScene::init() {

    if (!LayerColor::initWithColor(Color4B(0, 0, 0, 255))) {
        return false;
    }
    
    mainImageHeight=getContentSize().width*1.1;
    underBannerHeight=BannerBridge::getBannerHeight();
    topBannerHeight=underBannerHeight;
    underMenuHeight=(getContentSize().height-underBannerHeight-topBannerHeight-mainImageHeight)/2;
    //メニュー表示枠が小さすぎる時topのバナーを出さない
    if (underMenuHeight<40*Common::getDensity()) {
        DataManager::sharedManager()->setHideTopBanner(true);
        BannerBridge::removeBanner(NativeBridge::judgeID(BANNER_APPID_HEADER, BANNER_APPID_HEADER_TABLET));
        topBannerHeight=0;
        underMenuHeight=(getContentSize().height-underBannerHeight-mainImageHeight)/2;
        
        log("メニュー領域が小さすぎます。%f topBanne%f",underMenuHeight,40*Common::getDensity());
    }
    
    topMenuHeight = underMenuHeight;
    
    //シェイク検知開始
    ShakeGestureManager::getInstance()->startShakeGesture();
    
    //背景のデフォルトを決定
    setDefaultBackNum();
    
    //トップメニュー
    createTopMenu();

    //アンダーメニュー
    createUnderMenu();

    //メインの画像を作成
    createMain();

    //チュートリアル中のダウンロードバー
    createDownloadBar();
    
    //通知
    addNotification();

    //記録開始
    DataManager::sharedManager()->record(RecordType_StartDate);

    
    return true;
}

void GameScene::onEnterTransitionDidFinish()
{
    if (!isPlay) {
        auto filename=DataManager::sharedManager()->getBgmName(false);
        EscapeDataManager::getInstance()->playSound(filename.c_str(), true);
        
        if(!DataManager::sharedManager()->isShowedOpeningStory())
        {//データなし>ストーリーをながす
            //ストーリー中はpanelなし
            DataManager::sharedManager()->setStopPanelAd(true);

            auto lambdaShowStory = [this](){
                auto customManager=SupplementsManager::getInstance()->getCustomActionManager();
                customManager->openingAction( this, [this](bool success){
                    auto type = EscapeDataManager::getInstance()->getTypePlayTitleCondition();
                    DataManager::sharedManager()->setOtherFlag(StartGameKey, 1);

                    if (success) {
                        this->showDiscription();
                    }
                    else{
                        //特定のアクションが　セットされていない
                        auto storyLayer = StoryLayer::create(StoryKey_Opening, [this,type](Ref* sen){
                            DataManager::sharedManager()->setStopPanelAd(false);
                            bool canShowDisctiption=true;
                            if (type == TitleCondition_TUTORIAL) {
                                canShowDisctiption=false;
                                
                                mainSprite->reloadSprite(false, ReplaceTypeNone);
                            }
                            
                            if (canShowDisctiption) {
                                this->showDiscription();
                            }
                        });
                        addChild(storyLayer);
                    }
                });
            };
            
            
            Common::performProcessForDebug(
                                           [lambdaShowStory](){
                                               DataManager::sharedManager()->setStopPanelAd(false);
                                               //lambdaShowStory();
                                           },
                                           [lambdaShowStory](){
                                               //release時の挙動 
                                               lambdaShowStory();
                                           });
        }

        isPlay = true;        
    }
    
    SupplementsManager::getInstance()->showPanel(mainSprite);

    Common::performProcessForDebug([](){
        
        //DataManager::sharedManager()->addNewItem(2);
        //DataManager::sharedManager()->addNewItem(3);
        //DataManager::sharedManager()->addNewItem(4);
        //DataManager::sharedManager()->addNewItem(6);
        //DataManager::sharedManager()->addNewItem(7);
        //DataManager::sharedManager()->addNewItem(8);
        //DataManager::sharedManager()->addNewItem(9);
        //DataManager::sharedManager()->addNewItem(10);
        //DataManager::sharedManager()->addNewItem(11);
        //DataManager::sharedManager()->addNewItem(12);
        //DataManager::sharedManager()->addNewItem(13);
        //DataManager::sharedManager()->addNewItem(14);
        //DataManager::sharedManager()->addNewItem(16);
        //DataManager::sharedManager()->addNewItem(12);
        //DataManager::sharedManager()->addNewItem(14);
        //DataManager::sharedManager()->addNewItem(24);
        //DataManager::sharedManager()->addNewItem(5);
        //DataManager::sharedManager()->addNewItem(5);

        //DataManager::sharedManager()->addCoin(5);
        
        if (EscapeDataManager::getInstance()->getTypePlayTitleCondition() == TitleCondition_TUTORIAL) {
            return ;
        }
        
        //DataManager::sharedManager()->setDisableFlag(17);
        //フラグを強制セット 小数対応版 min,maxを渡す
        
        //DataManager::sharedManager()->setNowBack(1, ReplaceTypeNone);
        //DataManager::sharedManager()->setEnableFlags(0, 17);
        //DataManager::sharedManager()->setEnableFlags(0, 22);
        //DataManager::sharedManager()->setEnableFlag(22);
        //DataManager::sharedManager()->setEnableFlags(0, 26);
        //DataManager::sharedManager()->setEnableFlags(0, 28);
        //DataManager::sharedManager()->setEnableFlags(0, 47);
        //DataManager::sharedManager()->setEnableFlags(0, 52);

        //DataManager::sharedManager()->setDisableFlag(54);
    },nullptr);
}

void GameScene::showDiscription()
{
    auto description = DescriptionLayer::create([this](Ref* sender){
        SupplementsManager::getInstance()->showPanel(mainSprite);
    }, 200, false);
    
    description->setLocalZOrder(999);
    this->addChild(description);
}

void GameScene::setDefaultBackNum()
{
    auto manager = SupplementsManager::getInstance()->getCustomActionManager();
    
    Common::performProcessForDebug([manager](){
        DataManager::sharedManager()->setNowBack(manager->getDefaultBackNum(), ReplaceTypeNone);
    }, [manager](){
        DataManager::sharedManager()->setNowBack(manager->getDefaultBackNum(), ReplaceTypeNone);
    });
}

#pragma mark- UI
void GameScene::createMain()
{
    mainSprite=MainSprite::create(Rect(getContentSize().width/2, underMenuHeight+underBannerHeight+mainImageHeight/2, getContentSize().width, mainImageHeight));
    mainSprite->setDelegate(this);
    addChild(mainSprite);
    
    DataManager::sharedManager()->setMainSpriteRect(Rect(0, underMenuHeight+underBannerHeight, getContentSize().width, mainImageHeight));
}

void GameScene::createTopMenu()
{
    topSprite=TopMenuSprite::create(Rect(getContentSize().width/2, mainImageHeight+underBannerHeight+underMenuHeight+topMenuHeight/2, getContentSize().width, topMenuHeight));
    addChild(topSprite);
    
    //上部分の空白を覆っておく(dragでスプライトがはみ出すことがある)
    auto height=getContentSize().height-topSprite->getPosition().y-topMenuHeight/2;
    if (height>0) {
        auto cover=Sprite::create();
        cover->setTextureRect(Rect(0,0, getContentSize().width, height));
        cover->setColor(Color3B::BLACK);
        cover->setLocalZOrder(999);
        cover->setPosition(getContentSize().width/2,getContentSize().height-height/2);
        addChild(cover);
    }
}

void GameScene::createUnderMenu()
{
    underSprite=UnderMenuSprite::create(Rect(getContentSize().width/2, underBannerHeight+underMenuHeight/2, getContentSize().width, underMenuHeight));
    addChild(underSprite);
    
    //した部分の空白を覆っておく(dragでスプライトがはみ出すことがある)
    auto height=underSprite->getPositionY()-underSprite->getBoundingBox().size.height/2;
    auto cover=Sprite::create();
    cover->setTextureRect(Rect(0,0, getContentSize().width, height));
    cover->setColor(Color3B::BLACK);
    cover->setLocalZOrder(999);
    cover->setPosition(getContentSize().width/2,height/2);
    addChild(cover);
}

void GameScene::createDownloadBar()
{
    if (EscapeDataManager::getInstance()->getAllTitleDataDownloadStatus() == AllTitleDataDownloadStatus_WAITING) {

        auto mainSpriteRect = DataManager::sharedManager()->getMainSpriteRect();
        auto tutorialSafeArea = mainSpriteRect.size.height*.068;//チュートリアル内のライトに被らない高さ。
        auto dlbHeight = mainSpriteRect.size.height*.06;
        auto downloadBar = NCDownloadingBar::create();
        downloadBar->setScale(dlbHeight/downloadBar->getContentSize().height);
        downloadBar->setPosition(getContentSize().width-downloadBar->getBoundingBox().size.width/2*1.2, mainSpriteRect.origin.y+mainSpriteRect.size.height-tutorialSafeArea/2);
        downloadBar->setLocalZOrder(1000);//アニメーションSpriteよりも手前に
        this->addChild(downloadBar);
        
        EscapeDataManager::getInstance()->loadingAllTitleData();

        auto finishDownloadListner=EventListenerCustom::create(NotificationKey_Complete_AllDownload, [this, downloadBar](EventCustom*event)
                                                               {
                                                                   log("ダウンロード完了しました。");
                                                                   auto fadeout = FadeOut::create(.5);
                                                                   auto remove = RemoveSelf::create();
                                                                   downloadBar->runAction(Sequence::create(fadeout, remove, NULL));
                                                               });
        Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(finishDownloadListner,this);
    }
}

#pragma mark- 通知
void GameScene::addNotification()
{
    log("通知が登録されておるぞよい");
    auto changeCameraListner=EventListenerCustom::create(NotificationKeyWillChangeBack, [this](EventCustom*event)
    {//カメラ変更しろ通知
        //移動用矢印のリロード
        underSprite->loadData();
        underSprite->reloadArrows();
        
        //メイン画像のリロード
        auto replaceType=(Value*)event->getUserData();
        mainSprite->reloadSprite(true,(ReplaceType)replaceType->asInt());
    });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(changeCameraListner,this);
    
    auto listener=EventListenerCustom::create(NotificationKeyWillEnterForeGround, [this](Event*event)
    {//バックグラウンド復帰通知
        DataManager::sharedManager()->record(RecordType_StartDate);
        
        //ヒント動画を見た後は出さないように
        if (!DataManager::sharedManager()->getIsShowingPromoteHint()) {
            log("ヒント表示中じゃないのでISを出すぞ");
            //広告表示 表示できないので通知先(title,game)で仕方なく遅延実行
            //3回に一回表示 初回必中
            static int count=0;
            if (count%3==0) {
                Director::getInstance()->getScheduler()->schedule(schedule_selector(GameScene::showIS), this, 0, 0, .5, false);
            }
            
            count++;
        }
        else{
            log("ヒント表示中なのでISは出しません");
        }
    });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener,this);
    
    auto listener_didenter=EventListenerCustom::create(NotificationKeyDidEnterBackGround, [this](Event*event)
                                              {//バックグラウンド突入
                                                  DataManager::sharedManager()->record(RecordType_StopDate);
                                                  //通知を削除、再登録
                                                  std::vector<int>times={60*60,60*60*24,60*60*24*2,60*60*24*3,60*60*24*7};
                                                  log("通知テキスト %s",DataManager::sharedManager()->getNotificationString().c_str());
                                                  
                                                  for (int i=0; i < times.size(); i++) {
                                                      auto notification_id=NotificationTag_Stage+i;
                                                      NotificationBridge::cancelLocalNotification(notification_id);

                                                      NotificationBridge::fireLocalNotification(times.at(i), DataManager::sharedManager()->getNotificationString().c_str(), notification_id);
                                                  }
                                              });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener_didenter,this);
    
    auto listener_popup=EventListenerCustom::create(NotificationKeyWillShowPopUp, [this](Event*event)
    {//popup表示通知
        mainSprite->showPopUp();
    });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener_popup,this);
    
    auto listener_changeSelectedID=EventListenerCustom::create(NotificationKeyChangeSelectedItemID, [this](Event*event)
                                                    {//選択アイテム変更通知
                                                        if (EscapeDataManager::getInstance()->getTypePlayTitleCondition()==TitleCondition_TUTORIAL) {
                                                            TutorialActionManager::getInstance()->backAction("changeSlectedItemID",DataManager::sharedManager()->getNowBack(), mainSprite);
                                                        }
                                                    });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener_changeSelectedID,this);
}

#pragma mark- main delegate
void GameScene::changeMainSprite(ReplaceType replaceType)
{
    //アニメーション終わるまでタッチ不可にする
    auto layer=SupplementsManager::getInstance()->createDisableLayer();
    layer->setLocalZOrder(999);
    addChild(layer);
    

    auto newMain=MainSprite::create(Rect(getContentSize().width/2, underMenuHeight+underBannerHeight+mainImageHeight/2, getContentSize().width, mainImageHeight));

    newMain->setDelegate(this);

    //アニメーションを各々作成
    Vector<FiniteTimeAction*>actions;
    auto duration=.2;
    if (replaceType==ReplaceTypeFadeIn||replaceType==ReplaceTypeSlowFadeIn) {
        newMain->setOpacity(0);
        auto duration_short=.15;
        if (replaceType==ReplaceTypeSlowFadeIn) {
            duration_short=.5;
        }
        actions.pushBack(TargetedAction::create(newMain,FadeIn::create(duration_short)));
    }
    else if(replaceType==ReplaceTypeLeft)
    {//左に目線を移すよな動き
        newMain->setPosition(-getContentSize().width/2, newMain->getPosition().y);
        auto move_new=TargetedAction::create(newMain,MoveTo::create(duration, Vec2(getContentSize().width/2, newMain->getPosition().y)));
        auto move_old=TargetedAction::create(mainSprite,MoveTo::create(duration, Vec2(getContentSize().width*1.5, newMain->getPosition().y)));
        actions.pushBack(Spawn::create(move_new,move_old, NULL));
    }
    else if(replaceType==ReplaceTypeRight)
    {//右に目線を移すよな動き
        newMain->setPosition(getContentSize().width*1.5, newMain->getPosition().y);
        auto move_new=TargetedAction::create(newMain,MoveTo::create(duration, Vec2(getContentSize().width/2, newMain->getPosition().y)));
        auto move_old=TargetedAction::create(mainSprite,MoveTo::create(duration, Vec2(-getContentSize().width*.5, newMain->getPosition().y)));
        actions.pushBack(Spawn::create(move_new,move_old, NULL));
    }
    
    addChild(newMain);
    
    //入れ替えアクション
    auto replace=CallFunc::create([this,newMain,layer](){
        mainSprite->removeFromParent();
        mainSprite=newMain;
        layer->removeFromParent();
        
        SupplementsManager::getInstance()->showPanel(mainSprite);
    });
    
    actions.pushBack(replace);
    
    runAction(Sequence::create(actions));
}

#pragma mark- 広告
void GameScene::showIS(float delay)
{
    InterstitialBridge::showIS(INTERSTITIAL_APPID);
}

#pragma mark- dealloc
GameScene::~GameScene()
{
    log("Game解放");
    //シェイク検知終了
    ShakeGestureManager::getInstance()->stopShakeGesture();
}
