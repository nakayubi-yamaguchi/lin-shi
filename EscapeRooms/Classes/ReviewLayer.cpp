//
//  ReviewLayer.cpp
//  Kebab
//
//  Created by yamaguchinarita on 2016/12/22.
//
//

#include "ReviewLayer.h"
#include "Utils/Common.h"
#include "UIParts/MenuItemScale.h"
#include "TitleScene.h"
#include "DataManager.h"
#include "Utils/InterstitialBridge.h"
#include "Utils/FireBaseBridge.h"
#include "GameScene.h"
#include "EscapeDataManager.h"

#include "EscapeLoadingScene.h"
#include "AnalyticsManager.h"

#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"


using namespace cocos2d;

bool ReviewLayer::init() {
    if (!LayerColor::initWithColor(Color4B(0, 0, 0, 200))) {
        return false;
    }
    
    
    if (DataManager::sharedManager()->isPlayingMinigame())
    {//minigameをクリア
        setClearType(ClearType_MiniGame);
        //アナリティクス
        auto eventName=StringUtils::format("AfterMinigame_%s",EscapeDataManager::getInstance()->getPlayTitleName().c_str());
        AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName(eventName.c_str());
    }
    else
    {//本編をクリア
        auto minigameMap=DataManager::sharedManager()->getMiniGameData();
        auto minigameFlag=minigameMap["startFlagID"].asFloat();
        
        std::vector<int>removeItemIDs;
        if (!minigameMap["removeItemID"].isNull()) {
            removeItemIDs.push_back(minigameMap["removeItemID"].asInt());
        }
        if (!minigameMap["removeItemIDs"].isNull()) {
            for (auto v:minigameMap["removeItemIDs"].asValueVector()) {
                removeItemIDs.push_back(v.asInt());
            }
        }
        
        for (auto remove : removeItemIDs) {
            DataManager::sharedManager()->removeItemWithID(remove);
        }
        
        std::vector<int>getItemIDs;
        if (!minigameMap["getItemID"].isNull()) {
            getItemIDs.push_back(minigameMap["getItemID"].asInt());
        }
        if (!minigameMap["getItemIDs"].isNull()) {
            for (auto v:minigameMap["getItemIDs"].asValueVector()) {
                getItemIDs.push_back(v.asInt());
            }
        }
        
        for (auto get : getItemIDs) {
            DataManager::sharedManager()->addNewItem(get);
        }
        
        setClearType(ClearType_Normal);
        DataManager::sharedManager()->setEnableFlag(minigameFlag);
        //アナリティクス
        auto eventName=StringUtils::format("AfterGame_%s",EscapeDataManager::getInstance()->getPlayTitleName().c_str());

        AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName(eventName.c_str());

        auto manager=SupplementsManager::getInstance()->getCustomActionManager();
        manager->customAction(CustomAction_ReviewLayer, NULL);
    }
    
    //一時的なデータをリセットする
    DataManager::sharedManager()->resetAllData(false);

    
    createListener();
    createLabel();
    createMenu();
    
    return true;
}

#pragma mark- プライベート
void ReviewLayer::createListener()
{//タッチ透過させない
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [this](Touch *touch,Event*event)->bool{
        
        return true;
    };
    auto dip = Director::getInstance()->getEventDispatcher();
    dip->addEventListenerWithSceneGraphPriority(listener, this);
}

void ReviewLayer::createLabel()
{
    std::string title;
    std::string message;
    
    if (getClearType()==ClearType_Normal)
    {//ミニゲーム
        title=Common::localize("New game!","ミニゲーム解放!");
        message=DataManager::sharedManager()->getSystemMessage("released_minigame");
    }
    else
    {//完全クリア後
        title=Common::localize("Please","開発者からのお願い");
        message=DataManager::sharedManager()->getSystemMessage("please_review");
    }
    //タイトルラベル
    auto onegaiLabel = Label::createWithTTF(title, MyFont, MAX(getContentSize().height*.03 ,25*1.5));
    onegaiLabel->setTextColor(Color4B::WHITE);
    onegaiLabel->setPosition(getContentSize().width/2, getContentSize().height*.65);
    addChild(onegaiLabel);
    
    auto messageLabel = Label::createWithTTF(message, Common::getUsableFontPath(MyFont), MAX(getContentSize().height*.03,30));
    messageLabel->setWidth(getContentSize().width*.8);
    messageLabel->setTextColor(Color4B::WHITE);
    messageLabel->setPosition(getContentSize()/2);
    addChild(messageLabel);
}

void ReviewLayer::createMenu()
{
    //選択ボタン
    auto buttonSize=Size(getContentSize().width*.3, getContentSize().width*.1);
    
    Vector<MenuItem*> menuItems;
    float fontSize = buttonSize.height*.4;

    if(getClearType()==ClearType_Normal)
    {//ミニゲームへ
        std::vector<std::string> menuTexts = {DataManager::sharedManager()->getSystemMessage("review"),"PLAY GAME"};

        for (int i = 0; i < menuTexts.size(); i++) {
            auto butBack=Sprite::create();
            butBack->setTextureRect(Rect(0, 0, buttonSize.width, buttonSize.height));
            if (i==0) {
                butBack->setColor(Common::getColorFromHex(MyColor));
            }
            else{
                butBack->setColor(Color3B::ORANGE);
            }
            
            auto menuText = menuTexts.at(i);
            
            auto butLabel = Label::createWithTTF(menuText.c_str(), Common::getUsableFontPath(MyFont), fontSize);
            butLabel->setAlignment(TextHAlignment::CENTER);
            butLabel->setPosition(butBack->getContentSize()/2);
            butBack->addChild(butLabel);
            
            auto item = MenuItemScale::create(butBack, butBack, [this](Ref*sender){
                
                auto touchMenuItem = (MenuItemScale*)sender;
                int touchTag = touchMenuItem->getTag();
                
                if (touchTag == 0) {//レビューする
                    Application::getInstance()->openURL(MyURL);
                    
                    auto eventName=StringUtils::format("AfterGame_Review_%s",EscapeDataManager::getInstance()->getPlayTitleName().c_str());
                    AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName(eventName.c_str());

                }
                else{//プレイ
                    Common::stopBgm();
                    
                    auto layer=createDisableLayer();
                    this->addChild(layer);
                    
                    //セーブデータを再度読み込みする
                    DataManager::sharedManager()->loadAllData();
                    
                    Director::getInstance()->replaceScene(TransitionFade::create(1,GameScene::createScene(),Color3B::WHITE));
                    auto eventName=StringUtils::format("AfterGame_StartMinigame_%s",EscapeDataManager::getInstance()->getPlayTitleName().c_str());
                    AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName(eventName.c_str());

                }
            });
            item->setTag(i);
            menuItems.pushBack(item);
        }

        auto doneMenu=Menu::createWithArray(menuItems);
        doneMenu->alignItemsHorizontallyWithPadding(50);
        doneMenu->setPosition(getContentSize().width/2, BannerBridge::getBannerHeight()+buttonSize.height*2);
        addChild(doneMenu);
        
    }
    else{
        //メニュ-作成ラムダ
        auto sprite=[fontSize](std::string text,Size size){
            auto sprite=Sprite::create();
            sprite->setColor(Color3B::ORANGE);
            sprite->setTextureRect(Rect(0, 0, size.width, size.height));
            auto label=Label::createWithTTF(text, Common::getUsableFontPath(MyFont), fontSize);
            label->setAlignment(TextHAlignment::CENTER);
            label->setPosition(sprite->getContentSize()/2);
            sprite->addChild(label);
            
            return sprite;
        };
        
        //下段
        auto reviewSt = DataManager::sharedManager()->getSystemMessage("review");

        auto review=sprite(reviewSt, buttonSize);
        review->setColor(Color3B::ORANGE);
        auto reviewItem=MenuItemScale::create(review, review, [this](Ref*sender){
            Application::getInstance()->openURL(MyURL);
            //auto eventName=StringUtils::format("AfterMiniGame_Review_%s",EscapeDataManager::getInstance()->getPlayTitleName().c_str());
            //FireBaseBridge::getInstance()->analyticsWithName(eventName.c_str());
        });
        
        auto titleSt = DataManager::sharedManager()->getSystemMessage("back_to_home");
        auto title=sprite(titleSt,buttonSize);
        title->setColor(Common::getColorFromHex(MyColor));
        auto titleItem=MenuItemScale::create(title, title, [this](Ref*sender){
            Common::stopBgm();
            
            //auto eventName=StringUtils::format("AfterMiniGame_BackToTitle_%s",EscapeDataManager::getInstance()->getPlayTitleName().c_str());
            //FireBaseBridge::getInstance()->analyticsWithName(eventName.c_str());
            
            Director::getInstance()->replaceScene(TransitionFade::create(.5,EscapeLoadingScene::createScene(LoadingType_RemoveCathe),Color3B::WHITE));
        });
                                            
        auto doneMenu=Menu::create(reviewItem,titleItem, NULL);
        doneMenu->alignItemsHorizontallyWithPadding(50);
        doneMenu->setPosition(getContentSize().width/2, BannerBridge::getBannerHeight()+buttonSize.height);
        addChild(doneMenu);
        
        //上段 LINEリンク
        if(Common::localize("", "ja")=="ja"){
            auto line=sprite("LINEスタンプ発売中!",Size(buttonSize.width*2,buttonSize.height));
            line->setColor(Common::getColorFromHex("1dcd00"));
            auto lineItem=MenuItemScale::create(line, line, [this](Ref*sender){
                
                auto url="https://line.me/S/shop/sticker/author/164194";
                NativeBridge::openUrl(Common::localizeURL(url).c_str());
                
                auto eventName=StringUtils::format("AfterMiniGame_LINE");
                AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName(eventName.c_str());                
            });
            
            auto menu_1=Menu::create(lineItem, NULL);
            menu_1->setPosition(getContentSize().width/2, doneMenu->getPosition().y+buttonSize.height*1.25);
            addChild(menu_1);
        }
    }
}

#pragma mark- アラート
void ReviewLayer::alertDismissed(int tag, int buttonIndex)
{
    if (tag==1&&buttonIndex==0)
    {//
        NativeBridge::openUrl(MyURL);
    }
}

#pragma mark- タッチ非透過
LayerColor* ReviewLayer::createDisableLayer()
{
    auto layer=LayerColor::create(Color4B(0, 0, 0, 0));
    layer->setLocalZOrder(999);
    
    auto listner=EventListenerTouchOneByOne::create();
    listner->setSwallowTouches(true);//透過させない
    listner->onTouchBegan=[this](Touch*touch,Event*event)
    {
        return true;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listner, layer);
    
    return layer;
}

#pragma mark- 解放
ReviewLayer::~ReviewLayer()
{
    log("レビューレイヤーを解放する");
}
