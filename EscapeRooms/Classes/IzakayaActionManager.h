//
//  IzakayaActionManager.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/10/17.
//
//

#ifndef __EscapeContainer__IzakayaActionManager__
#define __EscapeContainer__IzakayaActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class IzakayaActionManager:public CustomActionManager
{
public:
    static IzakayaActionManager* manager;
    static IzakayaActionManager* getInstance();
    
    //over ride
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void customAction(std::string key,Node*parent);
private:

    std::string regiAns;
    
    void setRegiText(std::string appendText);
    
    /**num:左から0,1,2*/
    void showNabeParticle(Node* parent, int backID,int num, float duration);
    
    /**ビールを注ぐアクション*/
    FiniteTimeAction* pourAction(Node* parent);
    
    /**火のパーティクル*/
    void showFireParticle(Node* parent,int backNum);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
