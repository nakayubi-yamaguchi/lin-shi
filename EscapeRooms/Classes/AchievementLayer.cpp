//
//  ContentsAlert.cpp
//  EscapeRooms
//
//  Created by yamaguchinarita on 2017/11/22.
//
//

#include "AchievementLayer.h"
#include "StampLayer.h"
#include "Utils/Common.h"
#include "Utils/RewardMovieManager.h"
#include "Utils/ContentsAlert.h"
#include "Utils/UtilsMethods.h"
#include "UIParts/MenuItemScale.h"
#include "UIParts/MenuForScroll.h"
#include "UIParts/TextMenuItem.h"
#include "UIParts/GaugeSprite.h"
#include "AnalyticsManager.h"

using namespace cocos2d;

AchievementLayer* AchievementLayer::create(const ccMenuCallback &callback)
{
    auto node =new AchievementLayer();
    if (node && node->init(callback)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool AchievementLayer::init(const ccMenuCallback &callback) {
    if (!CustomAlert::init(Common::localize("Acievements", "達成項目"), "", {"CLOSE"}, callback)) {
        return false;
    }
    
    setTitleColor(LightBlackColor);
    
    reloadData();
    createUI();
    
    return true;
}

#pragma mark- Private
void AchievementLayer::reloadData()
{
    if (achievements.size()==0) {
        achievements=DataManager::sharedManager()->getAchievements();
    }
    
    
    achievementStrings.clear();
    achievementNowPoints.clear();
    achievementPoints.clear();
    achievementThumnails.clear();
    
    for (auto achievement:achievements)
    {
        //ゲージ上表示テキスト
        achievementStrings.push_back(DataManager::sharedManager()->getAchievementString(achievement));
        //ゲージに表示するテキスト
        auto achievePoint=DataManager::sharedManager()->getAchievementPoint(achievement);
        achievementPoints.push_back(achievePoint);
        auto nowPoint=DataManager::sharedManager()->getNowPoint(achievement);
        achievementNowPoints.push_back(nowPoint);
        //サムネイル
        achievementThumnails.pushBack(Sprite::createWithSpriteFrameName(DataManager::sharedManager()->getAchievementThumbnailName(achievement)));
    }
}


void AchievementLayer::createUI()
{
    minSize = Size(getContentSize().width*.9, getContentSize().height*.7);

    //メインスプライトとタイトルラベルの位置を変更
    mainSprite->setTextureRect(Rect(0, 0, minSize.width, minSize.height));
    arrange();

    titleLabel->setPositionY(mainSprite->getContentSize().height-space);
    
    auto label=Label::createWithTTF(DataManager::sharedManager()->getSystemMessage("achievement_detail"), MyFont, titleLabel->getContentSize().height*.5);
    label->setTextColor(titleLabel->getTextColor());
    label->setPosition(titleLabel->getPosition().x,titleLabel->getPosition().y-titleLabel->getContentSize().height*.5-label->getContentSize().height*.5);
    mainSprite->addChild(label);
    
    //テーブルビューを生成
    auto tableViewSize=Size(mainSprite->getContentSize().width, mainSprite->getContentSize().height-titleLabel->getContentSize().height-label->getContentSize().height-m_menuItems.at(0)->getPositionY()-space*2.5);//謎のspace
    auto width=tableViewSize.width;
    cellSize=Size(width,width/5.5);
    
    m_tableView =TableView::create(this, tableViewSize);
    m_tableView->setDirection(TableView::Direction::VERTICAL);
    m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
    m_tableView->setDelegate(this);
    m_tableView->setPosition((mainSprite->getContentSize().width-tableViewSize.width)/2, m_menuItems.at(0)->getPositionY()+space*1.5);
    
    //アラートを削除する際にオパシティが透過するように。
    m_tableView->setCascadeOpacityEnabled(true);
    for (auto subView : m_tableView->getChildren()) {
        subView->setCascadeOpacityEnabled(true);
    }
    
    mainSprite->addChild(m_tableView);
}

#pragma mark - 達成ボタン押下
void AchievementLayer::pushAchieve(ssize_t idx)
{
    Common::playSE("p.mp3");
    //スタンプカード表示
    auto stamp=StampLayer::create(1,[this,idx]
    {//close callback
        
    },[this,idx](bool byMovie)
    {//save callback
        if (!byMovie)
        {
        //達成後スタンプを開いた後に動画で追加取得された場合を排除
            DataManager::sharedManager()->setAchievementPoint(achievements.at(idx));
        }
        
        //アイテムデータを作り直してリロード
        this->reloadData();
        m_tableView->reloadData();
    });
    addChild(stamp);
}

#pragma mark - Table
#pragma mark data source
ssize_t AchievementLayer::numberOfCellsInTableView(TableView *table)
{
    int count = (int)achievements.size();
    
    return count;
}

Size AchievementLayer::cellSizeForTable(TableView* table)
{
    return cellSize;
}

TableViewCell* AchievementLayer::tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx)
{
    auto cell = table->dequeueCell();
    cell = new TableViewCell();
    cell->setCascadeOpacityEnabled(true);
    cell->autorelease();
    
    //白背景
    auto backSprite = Sprite::create();
    backSprite->setTextureRect(Rect(0, 0, cellSize.width, cellSize.height));
    backSprite->setPosition(cellSize/2);
    backSprite->setColor(Common::getColorFromHex(WhiteColor2));
    cell->addChild(backSprite);
    
    //選択状態
    auto selectedSprite = Sprite::create();
    selectedSprite->setTextureRect(backSprite->getTextureRect());
    selectedSprite->setColor(Common::getColorFromHex(LightBlackColor));
    selectedSprite->setOpacity(0);
    selectedSprite->setName("Selected");
    selectedSprite->setPosition(backSprite->getPosition());
    cell->addChild(selectedSprite);
    
    // ボーダーライン
    Sprite* line = Sprite::create();
    line->setAnchorPoint(Point(0, 0));
    line->setTextureRect(Rect(0, 0, cellSize.width, 1));
    line->setColor(Common::getColorFromHex(LightBlackColor));
    cell->addChild(line);
    
    //サムネイル画像
    auto thumbnailSpace=cellSize.width*.02;
    auto thumbnailWidth=cellSize.height*.6;
    auto thumbnailSprite = achievementThumnails.at(idx);
    thumbnailSprite->setScale(thumbnailWidth/thumbnailSprite->getContentSize().height);
    thumbnailSprite->setPosition(thumbnailSprite->getBoundingBox().size.width*.5+thumbnailSpace, cellSize.height/2);
    cell->addChild(thumbnailSprite);
    
    //テキスト
    auto itemText = achievementStrings.at(idx);
    auto itemFontSize =cellSize.height*.22;
    auto itemLabel = Label::createWithTTF(itemText, Common::getUsableFontPath(HiraginoMaruFont), itemFontSize);
    itemLabel->setPosition(thumbnailSprite->getPosition().x+thumbnailSprite->getBoundingBox().size.width/2+thumbnailSpace+itemLabel->getContentSize().width/2,cellSize.height-itemLabel->getContentSize().height*.8);
    itemLabel->enableBold();
    itemLabel->setTextColor(Color4B(Common::getColorFromHex(LightBlackColor)));
    cell->addChild(itemLabel);
    
    //gauge
    auto spr_g=Sprite::create();
    spr_g->setTextureRect(Rect(0, 0, cellSize.width*.4, cellSize.height*.25));
    spr_g->setColor(Common::getColorFromHex(GreenColor));
    auto spr_back_g=Sprite::create();
    spr_back_g->setTextureRect(spr_g->getTextureRect());
    spr_back_g->setColor(Color3B::GRAY);
    
    auto nowPoint=achievementNowPoints.at(idx);
    auto point=achievementPoints.at(idx);
    
    auto complete=(point==0);
    
    auto gauge=GaugeSprite::create(spr_g, spr_back_g);
    gauge->setPosition(thumbnailSprite->getPosition().x+thumbnailSprite->getBoundingBox().size.width*.5+thumbnailSpace+gauge->getContentSize().width*.5, cellSize.height*.3);
    if (complete) {
        gauge->setPercentage(100);
        gauge->createDisplayLabel("Complete", gauge->getContentSize().height);
    }
    else{
        gauge->setMidPoint(Vec2(0, .5));
        gauge->setPercentage(MIN(100, (float)nowPoint/(float)point*100));
        gauge->createDisplayLabel(StringUtils::format("%d/%d",nowPoint,point), gauge->getContentSize().height);
        gauge->setDisplayLabelPos(gauge->getContentSize().width-gauge->displayLabel->getContentSize().width/2, 0);
    }
    
    cell->addChild(gauge);
    
    
    float but_height = cellSize.height/2;
    auto but_width=but_height*3.3;

    //ボタン作成
    Size but_size = Size(but_width, but_height);
    Color3B but_color = Common::getColorFromHex(MyColor);
    auto push_but = [this, idx](Ref*sender){
        this->pushAchieve(idx);
    };
    
    if (AnalyticsManager::getInstance()->getAnalyticsShopButtonType()==AnalyticsShopButtonType_Normal) {
        TextMenuItem*  button;
        
        if (complete||nowPoint<point) {
            button = TextMenuItem::create(DataManager::sharedManager()->getSystemMessage("notAchieved"), but_size, but_color, false, push_but);
            button->setButtonColor(Color3B::BLACK);
            button->setOpacity(100);
            button->setEnabled(false);
        }
        else {//獲得できる時はGETテキストで立体的にする。
            button = TextMenuItem::create("GET!", but_size, but_color, true, push_but);
        }
        
        button->setTappedScale(.9);
        button->setPosition(cellSize.width-button->getContentSize().width/2*1.2, cellSize.height*.4);
        
        auto purchaseMenu = MenuForScroll::createWithArray({button});
        purchaseMenu->setPosition(Vec2::ZERO);
        
        cell->addChild(purchaseMenu);
    }
    else{
        //丸パターン
        auto sprite = Sprite::createWithSpriteFrameName("rounded.png");
        sprite->setCascadeOpacityEnabled(true);

        std::string text="GET!";
        
        
        MenuItemScale*button;
        if (complete||nowPoint<point) {
            sprite->setColor(Color3B::GRAY);
            button = MenuItemScale::create(sprite,sprite, push_but);
            button->setEnabled(false);
            
            text=DataManager::sharedManager()->getSystemMessage("notAchieved");
        }
        else {//獲得できる時はGETテキストで立体的にする。
            sprite->setColor(Common::getColorFromHex(DarkGrayColor));
            auto top_sprite = Sprite::createWithSpriteFrameName("rounded.png");
             top_sprite->setPosition(sprite->getContentSize().width/2, sprite->getContentSize().height*.54);
            top_sprite->setColor(but_color);
             sprite->addChild(top_sprite);
            button = MenuItemScale::create(sprite,sprite, push_but);
        }
        
        button->setScale(but_height/button->getContentSize().height);
        button->setTappedScale(.9);
        button->setPosition(cellSize.width-button->getBoundingBox().size.width/2*1.2, cellSize.height*.4);
        
        //ラベル
        auto label = Label::createWithTTF(text, Common::getUsableFontPath(LocalizeFont), sprite->getContentSize().height*.45);
        
        label->setPosition(sprite->getContentSize()/2);
        label->enableBold();
        sprite->addChild(label);
        
        auto purchaseMenu = MenuForScroll::createWithArray({button});
        purchaseMenu->setPosition(Vec2::ZERO);
        
        cell->addChild(purchaseMenu);
    }
    
    
    return cell;
}

#pragma mark delegate
void AchievementLayer::tableCellHighlight(TableView* table, TableViewCell* cell)
{
    auto selectedSprite = cell->getChildByName("Selected");
    auto fadein = FadeTo::create(.1, 255/3);
    selectedSprite->runAction(fadein);
}

void AchievementLayer::tableCellUnhighlight(cocos2d::extension::TableView *table, cocos2d::extension::TableViewCell *cell)
{
    auto selectedSprite = cell->getChildByName("Selected");
    auto fadeout = FadeOut::create(.1);
    selectedSprite->runAction(fadeout);
}

void AchievementLayer::tableCellTouched(cocos2d::extension::TableView *table, cocos2d::extension::TableViewCell *cell)
{
    return;
}
