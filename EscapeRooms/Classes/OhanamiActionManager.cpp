//
//  SchoolFesActionManager.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2018/11/29.
//
//

#include "OhanamiActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "GameScene.h"
#include "TouchManager.h"

using namespace cocos2d;

OhanamiActionManager* OhanamiActionManager::manager =NULL;

OhanamiActionManager* OhanamiActionManager::getInstance()
{
    if (manager==NULL) {
        manager = new OhanamiActionManager();
    }
        
    return manager;
}

#pragma mark - 背景
int OhanamiActionManager::getDefaultBackNum()
{
    int backNum=1;
    
    Common::performProcessForDebug([&backNum]{
        backNum= 1;
    });
    
    return backNum;
}

#pragma mark - Back
void OhanamiActionManager::backAction(std::string key, int backID, Node *parent, ShowType type)
{
#pragma mark 自販機
    if(key == "vendingmachine") {
        Vector<FiniteTimeAction*> actions;
        actions.pushBack(DelayTime::create(.6));

        std::vector<int> answer = {0, 3, 2, 1, 2, 3};
        
        Vector<Sprite*> sups;
        for (int i = 0; i < 4; i++) {
            auto file = StringUtils::format("sup_%d_%d.png", backID, i);
            auto sup = createSpriteToCenter(file.c_str(), true, parent, true);
            sups.pushBack(sup);
        }
        
        //回答
        for (int num : answer) {
            //actions.pushBack(createSoundAction("p.mp3", type));
            actions.pushBack(TargetedAction::create(sups.at(num), FadeIn::create(.3)));
            actions.pushBack(DelayTime::create(.5));
            actions.pushBack(TargetedAction::create(sups.at(num), FadeOut::create(.3)));
            
        }
        actions.pushBack(DelayTime::create(1));
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
#pragma mark 湯気
    else if(key == "steam") {
        std::vector<float> posXs = {.273, .427, .737};
        
        for (float xPosRatio : posXs) {
            float yPosRatio = .554;
            float startSize = parent->getContentSize().width*.025;
            float speed = parent->getContentSize().height*.06;
            
            auto particle=ParticleSystemQuad::create("smoke.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width*xPosRatio, parent->getContentSize().height*yPosRatio);
            particle->setPosVar(Vec2(parent->getContentSize().width*.04, 0));
            particle->setSpeed(speed);
            particle->setTotalParticles(215);
            //particle->setAngle(150);
            //particle->setAngleVar(10);
            particle->setLife(1.9);
            particle->setStartSize(startSize);
            particle->setEndSize(particle->getStartSize()*3);
            particle->setLocalZOrder(1);
            //particle->setDuration(.8);
            parent->addChild(particle);
        }
    }
#pragma mark 滑り台
    else if(key == "slide") {
        
        Vector<FiniteTimeAction*> actions;
        actions.pushBack(DelayTime::create(1));
        auto close = parent->getChildByName<Sprite*>("sup_74_close.png");
        auto open = createSpriteToCenter("sup_74_open.png", true, parent, true);
        open->setLocalZOrder(1);
        
        //扉開く
        actions.pushBack(createSoundAction("gacha,mp3", type));
        actions.pushBack(createChangeAction(.1, .1, close, open));
        actions.pushBack(DelayTime::create(.3));
        
        //うさぎ貼り付け
        Vector<Sprite*> waitingRabbits;
        Vector<Sprite*> slidingRabbits;
        
        bool  isMinigame = DataManager::sharedManager()->isPlayingMinigame();
        
        for (int i = 0; i < 4; i++) {
            auto waiting_rabbit = createSpriteToCenter(StringUtils::format("sup_74_rabbit_0_%d.png", i+1), true, parent, true);
            waitingRabbits.pushBack(waiting_rabbit);
            
            auto sliding_rabbit = createSpriteToCenter(StringUtils::format("sup_74_rabbit_1_%d.png", i+1), true, parent, true);
            slidingRabbits.pushBack(sliding_rabbit);
        }
        
        float waitingPosX = -parent->getContentSize().width*.1;
        std::vector<int> answer = {0, 1, 2, 3};
        
        if (isMinigame) {
            auto waiting = createSpriteToCenter("sup_74_mistake_0.png", true, parent, true);
            waitingRabbits.pushBack(waiting);
            
            auto sliding = parent->getChildByName<Sprite*>("sup_74_mistake_1.png");
            sliding->setOpacity(0);
            sliding->retain();
            sliding->removeFromParent();
            
            parent->addChild(sliding);
            sliding->release();
            slidingRabbits.pushBack(sliding);
            
            answer.pop_back();
            answer.push_back(4);
        }
        
        int i = 0;
        for (int num : answer) {
            //滑るところに遷移
            auto waitingR = waitingRabbits.at(num);
            auto slidingR = slidingRabbits.at(num);
            
            actions.pushBack(CallFunc::create([waitingR, waitingPosX](){
                waitingR->setPositionX(waitingPosX);
                waitingR->setOpacity(255);
            }));
            actions.pushBack(TargetedAction::create(waitingR, JumpTo::create(2, Vec2(parent->getContentSize().width/2, waitingR->getPositionY()), parent->getContentSize().height*.02, 6)));
            actions.pushBack(createChangeAction(.1, .1, waitingR, slidingR));
            actions.pushBack(DelayTime::create(.6));
            
            //滑るとこに移動
            auto slideSpawn = Spawn::create(ScaleTo::create(.3, 1.2), JumpBy::create(.3, Vec2(0, parent->getContentSize().height*.04), parent->getContentSize().height*.02, 2), NULL);
            actions.pushBack(TargetedAction::create(slidingR, slideSpawn));
            actions.pushBack(DelayTime::create(.6));
            
            if (i == answer.size()-1 && isMinigame) {//最後でなおかつミニゲーム
                actions.pushBack(createSoundAction("pig.mp3"));
                actions.pushBack(CallFunc::create([](){
                    DataManager::sharedManager()->setTemporaryFlag("mistake_pig", 1);
                }));
            }
            else {
                //滑る
                auto slideDown = Spawn::create(ScaleTo::create(.8, 1.4), JumpBy::create(.8, Vec2(0, -parent->getContentSize().height), parent->getContentSize().height*.5, 1), NULL);
                actions.pushBack(createSoundAction("pyuu.mp3", type));
                actions.pushBack(TargetedAction::create(slidingR, slideDown));
                actions.pushBack(DelayTime::create(.7));
                
                //元のサイズに戻す
                actions.pushBack(CallFunc::create([parent, slidingR](){
                    slidingR->setOpacity(0);
                    slidingR->setPositionY(parent->getContentSize().height/2);
                    slidingR->setScale(1);
                }));
            }
            
            i++;
        }      

        if (isMinigame) {
            parent->runAction(Sequence::create(actions));
        }
        else {
            //扉閉まる
            actions.pushBack(createSoundAction("gacha,mp3", type));
            actions.pushBack(createChangeAction(.1, .1, open, close));
            
            actions.pushBack(DelayTime::create(1));
            parent->runAction(Repeat::create(Sequence::create(actions), -1));
        }
    }
#pragma mark 歌う
    else if (key == "sing") {
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(13)) {
            parent->runAction(singAction(parent));
        }
    }
#pragma mark ビールで酔っ払う
    else if (key == "skew_beer") {
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(33)) {
            auto pig_2 = parent->getChildByName<Sprite*>("sup_12_beer_pig_2.png");
            transformToAnchorSprite(parent, pig_2, Vec2(.5, .189));
            parent->runAction(skewWithBeerAction(pig_2));
        }
    }
#pragma mark くじ
    else if (key == "kuji") {
        log("くじの回答はbackを先に通っています");
        for (int i = 0; i < 4; i++) {
            //なぜかcreateClippingNodeToCenterでクリップ作成した後に、stencilのアンカーポイントを変えるとうまくいかない
            auto name = StringUtils::format("sup_47_clip_%d.png", i);
            auto stencil = createSpriteToCenter(name, false, parent, false);
            transformToAnchorSprite(parent, stencil, Vec2(.5, .856));
            stencil->setScaleY(.7);
            
            auto clip=ClippingNode::create(stencil);
            clip->setAlphaThreshold(0);
            clip->setInverted(false);
            clip->setCascadeOpacityEnabled(true);
            clip->setName(name);
            parent->addChild(clip);
           
            auto rope = createSpriteToCenter(StringUtils::format("sup_47_%d.png", i), false, parent, false);
            clip->addChild(rope);
        }
    }
#pragma mark 飛行機投げ合うアニメ
    else if (key == "airplane") {
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(20)) {
            parent->runAction(Repeat::create(airplaneAction(parent), -1));
        }
    }
#pragma mark スワンボートぷかぷか
    else if (key == "swanboat") {
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(10)) {
            auto swanboat = parent->getChildByName<Sprite*>("sup_24_swanboat.png");
            parent->runAction(swingAction(Vec2(0, -parent->getContentSize().height*.01), 1.5, swanboat, 1.5, false, -1));
        }
    }
}

#pragma mark - Touch
void OhanamiActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    Vector<FiniteTimeAction*> actions;
    //コールバック
    auto call = CallFunc::create([callback]{
        callback(true);
    });
    
    log("タッチキーは%s",key.c_str());

#pragma mark 吹き出し（クラウド）
    if(key.substr(0, 5) == "cloud") {
        
        int num = DataManager::sharedManager()->getNowBack();
        if (num == 25) {//池側係員
            actions.pushBack(cloudBounceAction(parent, {"sup_25_pig_0.png"}, Vec2(.5, .082), "sup_25_cloud.png", Vec2(.643, .642), "pig.mp3"));
        }
        else if (num == 68) {//犬骨
            actions.pushBack(cloudBounceAction(parent, {"sup_68_dog_0.png"}, Vec2(.5, .391), "sup_68_cloud.png", Vec2(.577, .582), "dog.mp3"));
        }
        else if (num == 13) {//マイク欲しがる
            actions.pushBack(cloudBounceAction(parent, {"sup_13_pig_0.png"}, Vec2(.5, .43), "sup_13_cloud.png", Vec2(.58, .645), "pig.mp3"));
        }
        else if (num == 62) {//コイン欲しがる。きゅうりくれる
            actions.pushBack(cloudBounceAction(parent, {"sup_62_pig_0.png"}, Vec2(.5, .091), "sup_62_cloud.png", Vec2(.638, .641), "pig.mp3"));
        }
        else if (num == 85) {//コイン欲しがる。きゅうりくれる
            actions.pushBack(cloudBounceAction(parent, {"sup_85_kappa_0.png"}, Vec2(.5, .386), "sup_85_cloud.png", Vec2(.638, .641), "kyuu.mp3"));
        }
        else if (num == 12) {//ビール欲しがる。
            actions.pushBack(cloudBounceAction(parent, {"sup_12_beer_pig_0.png"}, Vec2(.5, .0), "sup_12_cloud.png", Vec2(.638, .641), "pig.mp3"));
        }
        else if (num == 82) {//りんご欲しがる。
            actions.pushBack(cloudBounceAction(parent, {"sup_82_pig_0.png"}, Vec2(.5, .0), "sup_82_cloud.png", Vec2(.577, .339), "pig.mp3"));
        }
        else if (num == 71) {//紙飛行機欲しがる。
            auto pig_0 = parent->getChildByName<Sprite*>("sup_71_pig_0_1.png");
            transformToAnchorSprite(parent, pig_0, Vec2(.352, .191));
            auto pig_0_bounce = createBounceAction(pig_0, .3, .1);
            auto cloudBounce = cloudBounceAction(parent, {"sup_71_pig_1_1.png"}, Vec2(.648, .191), "sup_71_cloud.png", Vec2(.533, .577), "pig.mp3");
            auto spawn = Spawn::create(cloudBounce, pig_0_bounce, NULL);
            actions.pushBack(spawn);
        }
        
        actions.pushBack(call);
    }
#pragma mark - アイテム使用系
    else if (key.substr(0, 4) == "use_") {
        
        auto key_split = Common::splitString(key, "_");
        auto item = key_split.at(1);//key.substr(4);
        
        log("アイテムをあげる%s",item.c_str());
        actions.pushBack(useItemSoundAction());
#pragma mark チケットあげる
        if (item == "ticket") {
            auto pig_0 = parent->getChildByName("sup_25_pig_0.png");
            transformToAnchorSprite(parent, pig_0, Vec2(.5, .082));
            
            auto ticket = createSpriteToCenter("sup_25_ticket.png", true, parent, true);
            actions.pushBack(giveAction(ticket,  1.0, .8, true));
            
            //豚喜ぶ
            actions.pushBack(createSoundAction("pig.mp3"));
            actions.pushBack(createBounceAction(pig_0, .3, .15));
            actions.pushBack(DelayTime::create(1));
            
            //移動
            actions.pushBack(createSoundAction("run.mp3"));
            actions.pushBack(TargetedAction::create(pig_0, JumpBy::create(1.25, Vec2(parent->getContentSize().width*.33, 0), parent->getContentSize().height*.02, 6)));
            actions.pushBack(DelayTime::create(.3));
            
            //画像切り替え
            auto pig_1 = createSpriteToCenter("sup_25_pig_1.png", true, parent, true);
            actions.pushBack(createChangeAction(.1, .1, pig_0, pig_1));
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark ドライバー使用
        else if (item == "driver") {

            auto lid = parent->getChildByName("sup_65_board_0.png");
            
            //ドライバーカタカタ
            std::vector<Vec2> positions = {Vec2(.357, .332), Vec2(.643, .332), Vec2(.355, .253), Vec2(.643, .253)};
            auto driverAct = driverAction(DataManager::sharedManager()->getNowBack(), positions, parent, 2);
            actions.pushBack(driverAct);
            
            //remove lid
            actions.pushBack(DelayTime::create(.5));
            actions.pushBack(createSoundAction("kacha.mp3"));
            actions.pushBack(TargetedAction::create(lid, FadeOut::create(1)));
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark マジックハンド
        else if (item == "magichand") {
            auto hand=createSpriteToCenter("sup_69_magichand.png", true, parent,true);
            actions.pushBack(TargetedAction::create(hand, Sequence::create(FadeIn::create(1),
                                                                           DelayTime::create(1),
                                                                           EaseInOut::create(MoveBy::create(1, Vec2(-parent->getContentSize().width*.168, parent->getContentSize().height*.032)), 1.5), NULL)));
            
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark 酒入れる
        else if (item == "sake") {
            Vector<Sprite*> sakes;
            for (int i = 0; i < 4; i++) {
                auto hana = createSpriteToCenter(StringUtils::format("sup_7_sake_%d.png", i), true, parent, true);
                sakes.pushBack(hana);
            }
            
            //酒表示
            auto sake = createSpriteToCenter("sup_7_bottle.png", true, parent, true);
            transformToAnchorSprite(parent, sake, Vec2(.067, .392));
            actions.pushBack(TargetedAction::create(sake, FadeIn::create(.3)));
            actions.pushBack(DelayTime::create(.5));

            //酒入れる
            std::vector<float> posXs = {.07, .403, .647, .89};
            int i = 0;
            for (float posX : posXs) {
                actions.pushBack(TargetedAction::create(sake, EaseOut::create(MoveTo::create(.5, Vec2(parent->getContentSize().width*posX, sake->getPositionY())), 2)));
                actions.pushBack(DelayTime::create(.5));
                actions.pushBack(createSoundAction("jobojobo.mp3"));
                
                actions.pushBack(TargetedAction::create(sakes.at(i), FadeIn::create(.3)));
                
                actions.pushBack(DelayTime::create(.8));
            
                i++;
            }
            
            //酒消す
            auto fadeout = TargetedAction::create(sake, Spawn::create(MoveBy::create(1, Vec2(0, -parent->getContentSize().height*.1)), FadeOut::create(1), NULL));
            actions.pushBack(fadeout);
        }
#pragma mark 宴会側に鍵使用
        else if (item == "key") {
            auto key = createSpriteToCenter("sup_11_key.png", true, parent, true);
            transformToAnchorSprite(parent, key, Vec2(.5, .52));
            
            //鍵さす
            actions.pushBack(createSoundAction("kata.mp3"));
            actions.pushBack(TargetedAction::create(key, FadeIn::create(.4)));
            actions.pushBack(DelayTime::create(.5));
            
            //鍵回す
            actions.pushBack(createSoundAction("kacha_1.mp3"));
            actions.pushBack(TargetedAction::create(key, RotateTo::create(.4, 45)));
            actions.pushBack(DelayTime::create(.5));

            //鍵取る
            actions.pushBack(TargetedAction::create(key, Spawn::create(FadeOut::create(.4),
                                                                       ScaleTo::create(.4, 1.1), NULL)));
            actions.pushBack(DelayTime::create(.6));

            //箱を取る
            auto lid = parent->getChildByName<Sprite*>("sup_11_lid.png");
            actions.pushBack(createSoundAction("kata.mp3"));
            actions.pushBack(TargetedAction::create(lid, Spawn::create(FadeOut::create(.4),
                                                                       ScaleTo::create(.4, 1.1), NULL)));
            
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark 犬に骨あげる
        else if (item == "bone") {
            //骨を置く
            auto bone = createSpriteToCenter("sup_68_bone.png", true, parent, true);
            actions.pushBack(giveAction(bone, 1, .8, true));
            actions.pushBack(DelayTime::create(.7));
            
            //犬飛び起きる
            auto anchor = Vec2(.5, .391);
            auto dog_0 = parent->getChildByName<Sprite*>("sup_68_dog_0.png");
            transformToAnchorSprite(parent, dog_0, anchor);
            auto dog_1 = createAnchorSprite("sup_68_dog_1.png", anchor.x, anchor.y, true, parent, true);
            actions.pushBack(createChangeAction(.5, .5, dog_0, dog_1));
            actions.pushBack(DelayTime::create(.8));
            
            //グニャン→BOW！！
            actions.pushBack(createBounceAction(dog_1, .3, .25));
            actions.pushBack(createSoundAction("dog.mp3"));
            actions.pushBack(DelayTime::create(.6));
            
            //骨持つのやめる
            actions.pushBack(createChangeAction(.4, .4, dog_1, dog_0));
            actions.pushBack(DelayTime::create(.3));
            
            //犬が掘って、紙幣を取得
            Vector<FiniteTimeAction*> spawnActions;
            auto dogSpawn = Spawn::create(createBounceAction(dog_0, .2, .25),
                                          TargetedAction::create(dog_0, MoveBy::create(.2, Vec2(0, -parent->getContentSize().height*.01))), NULL);
            auto dogSeq = Sequence::create(dogSpawn, DelayTime::create(.2), NULL);
            spawnActions.pushBack(Repeat::create(dogSeq, 6));

            Vector<FiniteTimeAction*> digActions;
            for (int i=0; i<6; i++) {
                auto call=CallFunc::create([parent,i]{
                    auto pos = Vec2(.5, .34);
                    auto particle=ParticleSystemQuad::create("particle_shower.plist");
                    particle->setAutoRemoveOnFinish(true);
                    particle->setPosition(parent->getContentSize().width*pos.x,parent->getContentSize().height*pos.y);
                    particle->setStartSize(parent->getContentSize().width*.01);
                    particle->setEndSize(particle->getStartSize()/2);
                    
                    Common::playSE("za.mp3");
                    particle->setLife(.4);
                    particle->setDuration(.4);
                    
                    parent->addChild(particle);
                });
                digActions.pushBack(call);
                digActions.pushBack(DelayTime::create(.4));
            }
            spawnActions.pushBack(Sequence::create(digActions));
            actions.pushBack(Spawn::create(spawnActions));
            
            //山無くして、紙幣加えている
            auto soil_0 = parent->getChildByName<Sprite*>("sup_68_soil_0.png");
            auto soil_1 = parent->getChildByName<Sprite*>("sup_68_soil_1.png");
            auto dog_3 = createSpriteToCenter("sup_68_dog_3.png", true, parent, true);
            transformToAnchorSprite(parent, dog_3, Vec2(.5, .241));
            actions.pushBack(createChangeAction(.4, .4, {soil_0, dog_0}, {soil_1, dog_3}));
            actions.pushBack(DelayTime::create(.8));
            
            //グニャん→ワン
            actions.pushBack(createSoundAction("dog.mp3"));
            actions.pushBack(createBounceAction(dog_3, .3, .25));
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark マイクをあげる
        else if (item == "microphone") {
            auto microphone = createSpriteToCenter("sup_13_microphone.png", true, parent, true);
            actions.pushBack(TargetedAction::create(microphone, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark コインをあげる
        else if (item == "coin") {
            //コインを渡す
            auto coin = createSpriteToCenter("sup_62_coin.png", true, parent, true);
            actions.pushBack(giveAction(coin, 1, .8, true));
            actions.pushBack(DelayTime::create(1));
            
            //きゅうりに手をかける
            auto pig_0 = parent->getChildByName<Sprite*>("sup_62_pig_0.png");
            auto pig_1 = createSpriteToCenter("sup_62_pig_1.png", true, parent, true);
            actions.pushBack(createChangeAction(.3, .3, pig_0, pig_1));
            actions.pushBack(swingAction(Vec2(parent->getContentSize().width*.03, 0), .3, pig_1, 1.5, false, 5));
            actions.pushBack(DelayTime::create(.6));
            
            //きゅうりをくれる
            auto pig_2 = createSpriteToCenter("sup_62_pig_2.png", true, parent, true);
            actions.pushBack(createChangeAction(0, .0, pig_1, pig_2, "poku.mp3"));
            actions.pushBack(DelayTime::create(1.5));
        }
#pragma mark きゅうりをあげる
        else if (item == "cucumber") {
            //きゅうりを渡す
            auto cucumber = createSpriteToCenter("sup_85_cucumber.png", true, parent, true);
            actions.pushBack(giveAction(cucumber, 1, .8, true));
            actions.pushBack(DelayTime::create(.5));
            
            //カッパきゅうりを持つ
            auto kappa_0 = parent->getChildByName<Sprite*>("sup_85_kappa_0.png");
            auto kappa_1 = createSpriteToCenter("sup_85_kappa_1.png", true, parent, true);
            actions.pushBack(createChangeAction(.2, .2, kappa_0, kappa_1));
            actions.pushBack(DelayTime::create(.6));
            
            //back84に遷移
            auto back84 = createSpriteToCenter("back_84.png", true, parent, true);
            SupplementsManager::getInstance()->addSupplementToMain(back84, 84, true);
            auto kappa_84_0 = back84->getChildByName<Sprite*>("sup_84_kappa_0.png");
            kappa_84_0->removeFromParent();
            auto kappa_84_1 = createSpriteToCenter("sup_84_kappa_1.png", false, back84, true);
            actions.pushBack(TargetedAction::create(back84, FadeIn::create(.4)));
            actions.pushBack(DelayTime::create(.6));
            
            //遷移前のカッパ消す
            actions.pushBack(TargetedAction::create(kappa_1, FadeOut::create(.1)));
            
            //カッパ桶から出る
            auto kappa_84_2 = createSpriteToCenter("sup_84_kappa_2.png", true, back84, true);
            actions.pushBack(createChangeAction(.3, .3, kappa_84_1, kappa_84_2, "kyuu.mp3"));
            actions.pushBack(DelayTime::create(1));
            
            //カッパは下に降りる
            auto kappa_84_3 = createSpriteToCenter("sup_84_kappa_3.png", true, back84, true);
            actions.pushBack(createChangeAction(.3, .3, kappa_84_2, kappa_84_3));
            actions.pushBack(DelayTime::create(.8));
            
            actions.pushBack(TargetedAction::create(back84, FadeOut::create(.5)));
        }
#pragma mark 石像にりんご飴
        else if (item == "applecandy") {
            //りんご飴
            auto cucumber = createSpriteToCenter("sup_82_applecasket.png", true, parent, true);
            actions.pushBack(giveAction(cucumber, 1, .8, true));
            actions.pushBack(DelayTime::create(.5));
            
            //りんご飴を咥える
            auto pig_0 = parent->getChildByName<Sprite*>("sup_82_pig_0.png");
            auto pig_1 = createSpriteToCenter("sup_82_pig_1.png", true, parent, true);
            actions.pushBack(createChangeAction(.2, .2, pig_0, pig_1, "pig.mp3"));
            actions.pushBack(DelayTime::create(.8));
            
            //back81に遷移
            auto back81 = createSpriteToCenter("back_81.png", true, parent, true);
            SupplementsManager::getInstance()->addSupplementToMain(back81, 81, true);
            auto pig_81_0 = back81->getChildByName<Sprite*>("sup_81_pig_0.png");
            createSpriteToCenter("sup_81_apple.png", false, pig_81_0, true);
            actions.pushBack(TargetedAction::create(back81, FadeIn::create(.6)));
            actions.pushBack(DelayTime::create(1));

            //豚を切り替える
            auto pig_81_1 = createSpriteToCenter("sup_81_pig_1.png", true, back81, true);
            actions.pushBack(createChangeAction(.5, .5, pig_81_0, pig_81_1, "pig.mp3"));
            actions.pushBack(DelayTime::create(1.2));
        }
#pragma mark 紙幣を入れる
        else if (item == "bill") {
            //紙幣を挿入
            auto clip = createClippingNodeToCenter("sup_65_clip.png", parent);
            auto bill = createSpriteToCenter("sup_65_bill.png", true, parent, false);
            transformToAnchorSprite(parent, bill, Vec2(.5, .711));
            clip->addChild(bill);
            
            actions.pushBack(TargetedAction::create(bill, FadeIn::create(.6)));
            actions.pushBack(DelayTime::create(.4));
            
            //紙幣を入れていく
            auto spawn = Spawn::create(ScaleTo::create(1, .59), MoveTo::create(1, Vec2(bill->getPositionX(), parent->getContentSize().height*.603)), NULL);
            auto bill_spawn = TargetedAction::create(bill, spawn);
            actions.pushBack(createSoundAction("emission.mp3"));
            actions.pushBack(bill_spawn);
            actions.pushBack(DelayTime::create(.6));
            
            //back65にコインを貼っておく
            auto coin_ = createSpriteToCenter("sup_65_coin.png", true, parent, true);
            
            //back63に遷移
            auto back63=createSpriteToCenter("back_63.png", true, parent,true);
            back63->setLocalZOrder(1);
            SupplementsManager::getInstance()->addSupplementToMain(back63, 63, true);
            actions.pushBack(TargetedAction::create(back63, FadeIn::create(1)));
            actions.pushBack(DelayTime::create(.4));
            
            //back65にコインを表示
            actions.pushBack(TargetedAction::create(coin_, FadeIn::create(.1)));
            
            //ビール落下
            auto clip_63 = createClippingNodeToCenter("sup_63_clip.png", back63);
            
            auto beer = createSpriteToCenter("sup_63_beer.png", false, parent);
            beer->setPositionY(parent->getContentSize().height*.65);
            clip_63->addChild(beer);
            
            auto downAction = Sequence::create(//Common::createSoundAction("goro.mp3"),
                                               EaseIn::create(MoveTo::create(.2, parent->getContentSize()/2), 2) ,
                                               JumpTo::create(.2, parent->getContentSize()/2, parent->getContentSize().height*.01, 2),
                                               /*DelayTime::create(.5),
                                                FadeOut::create(.7),*/NULL);
            actions.pushBack(createSoundAction("goro.mp3"));
            actions.pushBack(TargetedAction::create(beer, downAction));
            actions.pushBack(DelayTime::create(1));
            
            //コイン落下
            auto coin = createSpriteToCenter("sup_63_coin.png", false, parent);
            coin->setPositionY(parent->getContentSize().height*.65);
            clip_63->addChild(coin);
            
            actions.pushBack(createSoundAction("chalin.mp3"));
            actions.pushBack(TargetedAction::create(coin, downAction->clone()));
            actions.pushBack(DelayTime::create(1));
            
            //back63消す
            actions.pushBack(TargetedAction::create(back63, FadeOut::create(.5)));
            actions.pushBack(DelayTime::create(.8));
        }
#pragma mark ビールをあげる
        else if (item == "beer") {
            //ビールを渡す
            auto beer = createSpriteToCenter("sup_12_beer.png", true, parent, true);
            actions.pushBack(giveAction(beer, 1, .8, true));
            actions.pushBack(DelayTime::create(.5));
            
            auto pig_0 = parent->getChildByName<Sprite*>("sup_12_beer_pig_0.png");
            auto pig_1 = createSpriteToCenter("sup_12_beer_pig_1.png", true, parent, true);
            auto pig_2 = createSpriteToCenter("sup_12_beer_pig_2.png", true, parent, true);
            transformToAnchorSprite(parent, pig_2, Vec2(.5, .189));
            
            actions.pushBack(createChangeAction(.4, .4, pig_0, pig_1, "pig_1.mp3"));
            actions.pushBack(DelayTime::create(.8));
            
            actions.pushBack(createChangeAction(.4, .4, pig_1, pig_2, "pop_1.mp3"));
            actions.pushBack(DelayTime::create(.5));
        }
#pragma mark 紙飛行機渡す
        else if(item == "paperairplane"){
            auto paperairplane = createSpriteToCenter("sup_71_paperairplane.png", true, parent, true);
            actions.pushBack(giveAction(paperairplane,  1.0, .8, true));
            actions.pushBack(DelayTime::create(.6));
            
            //紙飛行機受け取る
            auto pig_1_1 = parent->getChildByName<Sprite*>("sup_71_pig_1_1.png");
            auto pig_1_2 = createSpriteToCenter("sup_71_pig_1_2.png", true, parent, true);
            actions.pushBack(createChangeAction(.4, .4, pig_1_1, pig_1_2, "pig.mp3"));
            actions.pushBack(DelayTime::create(.8));
            
        }
        
        //完了
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark - 屋台回答後、屋台開ける
    else if(key == "applecandy_action"){
        
        AnswerManager::getInstance()->getItemIDs.push_back(18);
        
        //屋台に切り替え
        auto back44 = createSpriteToCenter("back_44.png", true, parent, true);
        createSpriteToCenter("sup_44_open.png", false, back44, true);
        auto pig_0 = createSpriteToCenter("sup_44_pig_0.png", false, back44, true);
        transformToAnchorSprite(back44, pig_0, Vec2(.5, .402));
        pig_0->setPositionY(parent->getContentSize().height*.3);
        auto pig_1 = createSpriteToCenter("sup_44_pig_1.png", true, back44, true);
        transformToAnchorSprite(back44, pig_1, pig_0->getAnchorPoint());
        pig_1->setPositionY(pig_0->getPositionY());
        
        createSpriteToCenter("sup_44_cover.png", false, back44, true);
        createSpriteToCenter("sup_44_applecasket.png", false, back44, true);
        actions.pushBack(TargetedAction::create(back44, FadeIn::create(.6)));
        actions.pushBack(DelayTime::create(.7));
    
        //入れ替わり
        actions.pushBack(createBounceAction(pig_0, .3, .3));
        actions.pushBack(createChangeAction(.3, .3, pig_0, pig_1, "pig.mp3"));
        actions.pushBack(DelayTime::create(.6));
        
        //上がる
        actions.pushBack(TargetedAction::create(pig_1, EaseOut::create(MoveTo::create(.6, Vec2(pig_1->getPositionX(), parent->getContentSize().height*pig_1->getAnchorPoint().y)), 2)));
        actions.pushBack(DelayTime::create(1.2));
        
        //グニョン&鳴く
        actions.pushBack(createSoundAction("pig.mp3"));
        actions.pushBack(createBounceAction(pig_1, .45, .2));
        actions.pushBack(DelayTime::create(1));
        
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark 屋台くじ回答
    else if(key.substr(0, 4) == "rope"){
        
        auto index = atoi(key.substr(5).c_str());
        log("ロープアクション入ります%d",index);

        actions.pushBack(kujiAction(parent, index, ShowType_Normal));
        
        actions.pushBack(call);
    }
#pragma mark 屋台くじ正解後
    else if(key.substr(0, 6) == "answer"){
        
        auto index = atoi(key.substr(7).c_str());
        
        //back46に遷移
        auto back46 = createSpriteToCenter("back_46.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back46, 46, true);
        actions.pushBack(TargetedAction::create(back46, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.5));
        
        //
        auto robot_0 = back46->getChildByName<Sprite*>("sup_46_robot_0.png");
        transformToAnchorSprite(back46, robot_0, Vec2(.5, .367));
        robot_0->setPositionY(parent->getContentSize().height*.3);
        auto robot_1 = createSpriteToCenter(StringUtils::format("sup_46_robot_%d.png", index), true, back46, true);
        transformToAnchorSprite(back46, robot_1, robot_0->getAnchorPoint());
        robot_1->setPositionY(robot_0->getPositionY());
        createSpriteToCenter("sup_46_cover.png", false, back46, true);
        actions.pushBack(TargetedAction::create(back46, FadeIn::create(.6)));
        actions.pushBack(DelayTime::create(.7));
        
        //入れ替わり
        actions.pushBack(createBounceAction(robot_0, .3, .3));
        actions.pushBack(createChangeAction(.3, .3, robot_0, robot_1, "robot_move.mp3"));
        actions.pushBack(DelayTime::create(.6));
        
        //上がる
        actions.pushBack(TargetedAction::create(robot_1, EaseOut::create(MoveTo::create(.6, Vec2(robot_1->getPositionX(), parent->getContentSize().height*robot_1->getAnchorPoint().y)), 2)));
        actions.pushBack(DelayTime::create(1.2));
        
        actions.pushBack(call);
    }
#pragma mark お守り並び替え正解後
    else if(key == "complete_amulet"){
        auto pig_0 = parent->getChildByName<Sprite*>("sup_87_pig_0.png");
        auto pig_1 = createSpriteToCenter("sup_87_pig_1.png", true, parent, true);
        auto pig_2 = createSpriteToCenter("sup_87_pig_2.png", true, parent, true);
        
        actions.pushBack(createSoundAction("pig.mp3"));
        actions.pushBack(DelayTime::create(.6));
        actions.pushBack(createChangeAction(.4, .4, pig_0, pig_1, "pop_1.mp3"));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createChangeAction(.5, .5, pig_1, pig_2));
        actions.pushBack(DelayTime::create(.9));
        
        //エンディング
        actions.pushBack(CallFunc::create([parent]{
            auto story=StoryLayer::create("ending", []{
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        }));
    }
#pragma mark スワンボートで移動
    else if(key=="swanboat"){
        
        auto black = Sprite::create();
        black->setColor(Color3B::BLACK);
        black->setPosition(parent->getContentSize()/2);
        black->setOpacity(0);
        black->setTextureRect(Rect(0, 0, parent->getContentSize().width, parent->getContentSize().height));
        parent->addChild(black);
        
        actions.pushBack(TargetedAction::create(black, FadeIn::create(1)));
        actions.pushBack(createSoundAction("wave.mp3"));
        actions.pushBack(DelayTime::create(.5));
        
        actions.pushBack(call);
    }
#pragma mark - 指定無し
    else{
        actions.pushBack(call);
    }
    parent->runAction(Sequence::create(actions));
}

#pragma mark - ヒント機能のAnswer
void OhanamiActionManager::showAnswerAction(std::string key, Node *parent, ShowType showType)
{
#pragma mark くじ回答
    if (key=="kuji") {
        Vector<FiniteTimeAction*>actions;
        
        
        int backNum = !DataManager::sharedManager()->getEnableFlagWithFlagID(16)? 47 : 50;
        
        //バックアクションを読んで必要な画像を貼っておく
        backAction(key, backNum, parent, showType);

        auto answer = DataManager::sharedManager()->getBackData(backNum)["useInput"].asValueMap()["answer"].asValueMap()["answer"].asString();
        
        log("回答です%s",answer.c_str());
        actions.pushBack(DelayTime::create(.5));
        for (int i=0; i<answer.size(); i++) {
            
            auto index=atoi(answer.substr(i,1).c_str());
            log("一部回答です%d",index);
            
            if (parent) {
                log("parentがあります");
            }

            actions.pushBack(kujiAction(parent, index, showType));
        }
        actions.pushBack(DelayTime::create(1.5));
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
}

#pragma mark - touch began
void OhanamiActionManager::touchBeganAction(std::string key, Node *parent)
{
}


#pragma mark - Drag
void OhanamiActionManager::dragAction(std::string key, Node *parent, Vec2 pos)
{
    
}

#pragma mark - Item
void OhanamiActionManager::itemBackAction(std::string key, PopUpLayer *parent)
{
}

void OhanamiActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造 addする際はcreateSpriteOnClipを使用するとよい
    Vector<FiniteTimeAction*>actions;
    log("アイテムアクション発動 : %s",key.c_str());
    actions.pushBack(useItemSoundAction());
#pragma mark ビールを栓抜きで抜く
    if (key == "use_opener") {
        
        auto cap = parent->itemImage->getChildByName("beer_cap.png");
        
        auto anime_0 = createSpriteToCenter("anim_beer_0.png", true, parent->itemImage, true);
        actions.pushBack(TargetedAction::create(anime_0, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        
        auto anime_1 = createSpriteToCenter("anim_beer_1.png", true, parent->itemImage, true);
        actions.pushBack(createChangeAction(.1, .1, {cap, anime_0}, {anime_1}, "po.mp3"));
        actions.pushBack(DelayTime::create(1.2));
    }
    
    //call
    auto call=CallFunc::create([callback](){
        callback(true);
    });
    actions.pushBack(call);
    
    parent->runAction(Sequence::create(actions));
}

#pragma mark - Custom
void OhanamiActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    if (key == CustomAction_Title){
        clearParticleAction(parent);
    }
}

#pragma mark - Camera Shot
void OhanamiActionManager::cameraShotAction(int backID)
{
}

#pragma mark - Arrow Action
void OhanamiActionManager::arrowAction(std::string key, Node *parent, const onFinished &callback)
{
#pragma mark 神社から桟橋に戻る時
    if (key == "swanboat") {
        touchAction(key, parent, callback);
        
    }
    else {
        callback(true);
    }
}

#pragma mark - Private
FiniteTimeAction* OhanamiActionManager::singAction(Node* parent)
{
    auto singer=parent->getChildByName("sup_13_pig_0.png");
    
    auto particle=[parent](int index){
        auto call=CallFunc::create([parent,index]{
            Common::playSE("pop.mp3");
            
            auto pos=Vec2(parent->getContentSize().width*.413, parent->getContentSize().height*.682);
            std::vector<float> sizes = {.1, .15, .2};
            
            auto size = parent->getContentSize().width*sizes.at(index);
            auto particle = ParticleSystemQuad::create("particle_music.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(pos);
            particle->setLife(2.5);
            particle->resetSystem();
            particle->setStartSize(size);
            particle->setEndSize(size);
            particle->setTotalParticles(5);
            particle->setGravity(Vec2(size*.2, -size*2));
            particle->setStartColor(Color4F::BLACK);
            particle->setEndColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 0));
            particle->setEndColorVar(Color4F(0, 0, 0, 0));
            
            particle->setAngle(110);
            particle->setSpeed(size*2);
            particle->setEndSpin(10);
            particle->setEndSpinVar(20);
            parent->addChild(particle);
        });
        
        return Sequence::create(call, NULL);
    };
    
    Vector<FiniteTimeAction*>actions;
    actions.pushBack(DelayTime::create(.5));
    auto angle = 2.5;
    auto duration = 1.2;
    auto duration_delay = .2;
    
    std::vector<int> answer = {0, 1, 2, 2, 1};
    for (int i : answer) {
        auto skew=TargetedAction::create(singer,Sequence::create(particle(i),
                                                                 EaseOut::create(SkewTo::create(duration, -angle, 0), 1.5),
                                                                 DelayTime::create(duration_delay),
                                                                 EaseIn::create(SkewTo::create(duration, angle, 0),1.5),
                                                                 DelayTime::create(duration_delay),
                                                                 NULL));
        actions.pushBack(skew);
    }
    
    actions.pushBack(DelayTime::create(1.5));
    
    auto rep=Repeat::create(Sequence::create(actions), -1);
    return rep;
}

FiniteTimeAction* OhanamiActionManager::skewWithBeerAction(Sprite *target)
{
    Vector<FiniteTimeAction*> skewActions;
    auto angle = 15;
    auto duration = 1.2;
    auto duration_delay = .2;
    
    Vector<FiniteTimeAction*> skews;//左右の順で入っている
    for (int i = 0; i < 2; i++) {
        
        auto startSkew = Spawn::create(SkewTo::create(duration, angle*pow(-1, i+1), 0),
                                       createBounceAction(target, duration/2, .05), NULL);
        
        auto backSkew = Spawn::create(SkewTo::create(duration, 0, 0),
                                      createBounceAction(target, duration/3, .05), NULL);
        
        auto skew = Sequence::create(EaseOut::create(startSkew, 1.5),
                                     createSoundAction("pig.mp3"),
                                     DelayTime::create(duration_delay),
                                     EaseIn::create(backSkew, 1.5),
                                     DelayTime::create(duration_delay), NULL);
        
        skews.pushBack(skew);
    }
    
    //回答部分
    std::vector<int> answer = {0, 1, 1, 0, 1, 1};
    for (int num : answer) {
        skewActions.pushBack(skews.at(num));
    }
    auto skewSeq = Sequence::create(skewActions);
    return TargetedAction::create(target, skewSeq);
}

FiniteTimeAction* OhanamiActionManager::kujiAction(Node *parent, int index, ShowType type)
{
    auto clip = parent->getChildByName<ClippingNode*>(StringUtils::format("sup_47_clip_%d.png", index));
    auto stencil = (Sprite*)clip->getStencil();
    
    auto scale = stencil->getScaleY();
    auto seq = Sequence::create(createSoundAction("swing_0.mp3", type),
                                ScaleTo::create(.3, 1, 1),
                                ScaleTo::create(.3, 1, scale), NULL);
    
    return TargetedAction::create(stencil, EaseInOut::create(seq, 1.5));
}


ActionInterval* OhanamiActionManager::bezierCustomActions(Node *parent, Sprite *target, Vec2 endPos, OhanamiBezierType bezierType, bool fromLeft, float pow_)
{
    
    auto createBezierPoses = [endPos, fromLeft](std::vector<Vec2> targetVecs)->std::vector<std::vector<Vec2>>{
        if (fromLeft) {//左から投げる。
            std::reverse(targetVecs.begin(), targetVecs.end());
        }
        //終着点を配列に追加
        targetVecs.push_back(endPos);
        
        //ベジェポイントの配列に変換
        std::vector<std::vector<Vec2>> bezierPoses;
        while (targetVecs.size() > 0) {
            bezierPoses.push_back({targetVecs.at(0), targetVecs.at(1), targetVecs.at(2)});
            
            //3個消す
            targetVecs.erase(targetVecs.begin());
            targetVecs.erase(targetVecs.begin());
            targetVecs.erase(targetVecs.begin());
        }
        return bezierPoses;
    };
       

    ActionInterval* customBezierAction;
    
    switch (bezierType) {
        case OhanamiBezierType::ROTATION:{
            float dura = 2;
            std::vector<Vec2> rotationVecs = {Vec2(.358, .429), Vec2(.358, .835), Vec2(.495, .792),
                Vec2(.63, .835), Vec2(.63, .429)};
            std::vector<std::vector<Vec2>> posVecs = createBezierPoses(rotationVecs);
            customBezierAction = Spawn::create(bezierActions(parent, posVecs, dura), RotateBy::create(dura, 360*pow_), NULL);
            break;
        }
        case OhanamiBezierType::MOUNTAIN:{
            float dura = 2;
            std::vector<Vec2> mountainVecs = {Vec2(.745, .697), Vec2(.245, .697)};
            std::vector<std::vector<Vec2>> posVecs = createBezierPoses(mountainVecs);

            customBezierAction = Sequence::create(CallFunc::create([target, pow_](){target->setRotation(15*pow_);}),
                                                  Spawn::create(bezierActions(parent, posVecs, dura), RotateTo::create(dura, -30*pow_), NULL), NULL);
            break;
        }
        case OhanamiBezierType::WAVE:{
            float dura = 2.5;
            
            std::vector<Vec2> waveVecs = {
                Vec2(.795, .671), Vec2(.593, .671), Vec2(.593, .574),
                Vec2(.593, endPos.y), Vec2(.393, endPos.y), Vec2(.393, .574),
                Vec2(.393, .671), Vec2(.193, .671)};
            
            waveVecs = {
                Vec2(.779, .671), Vec2(.593, .671), Vec2(.593, .574),
                Vec2(.593, endPos.y), Vec2(.407, endPos.y), Vec2(.407, .574),
                Vec2(.407, .671), Vec2(.221, .671)};
            
            std::vector<std::vector<Vec2>> posVecs = createBezierPoses(waveVecs);
            
            auto rotate = Sequence::create(RotateTo::create(dura/3, -50*pow_),
                                           RotateTo::create(dura/3, 50*pow_),
                                           RotateTo::create(dura/3, -50*pow_), NULL);
            customBezierAction = Sequence::create(CallFunc::create([target, pow_](){target->setRotation(50*pow_);}),
                                            Spawn::create(bezierActions(parent, posVecs, dura), rotate, NULL), NULL);
            break;
        }
        case OhanamiBezierType::STRAIGHT:{
            float dura = 1.5;
            customBezierAction = MoveTo::create(dura, Vec2(parent->getContentSize().width*endPos.x, parent->getContentSize().height*endPos.y));
            break;
        }
            
        default:
            break;
    }
    
    return customBezierAction;
}

FiniteTimeAction* OhanamiActionManager::airplaneAction(Node *parent)
{
    Vector<FiniteTimeAction*> actions;
    //豚移動
    std::vector<float> poses = {.105, .897};//{.078, .892};
    float posY = .471;//.42;
    float handrotate = 80;
    //auto pig_0 = parent->getChildByName<Sprite*>("sup_71_pig_0.png");
    auto pighand_0 = parent->getChildByName<Sprite*>("sup_71_pighand_0.png");
    transformToAnchorSprite(parent, pighand_0, Vec2(.067, .441));
    pighand_0->setRotation(-handrotate);
    
    //auto pig_1 = parent->getChildByName<Sprite*>("sup_71_pig_1.png");
    auto pighand_1 = parent->getChildByName<Sprite*>("sup_71_pighand_1.png");
    transformToAnchorSprite(parent, pighand_1, Vec2(.935, .441));
    pighand_1->setRotation(handrotate);

    auto airplane_0 = parent->getChildByName<Sprite*>("sup_72_airplane_0.png");
    airplane_0->setOpacity(0);
    auto airplane_1 = parent->getChildByName<Sprite*>("sup_72_airplane_1.png");

    airplane_1->setPosition(parent->getContentSize().width*poses.at(1), parent->getContentSize().height*posY);

    Vector<Sprite*> airplanes = {airplane_0, airplane_1};
    Vector<Sprite*> hands = {pighand_0, pighand_1};

    actions.pushBack(DelayTime::create(.5));
    
    //回答
    std::vector<int> answer = {0, 1, 2, 3, 1, 2};
    int i = 0;
    Vector<FiniteTimeAction*> bezierActions;
    for (int num : answer) {
        bool fromLeft = i%2;
        float pow_ = pow(-1, fromLeft);
        
        //紙飛行機の位置と向きを合わせておく
        actions.pushBack(CallFunc::create([parent, airplanes, fromLeft, poses, posY](){
            int n = 0;
            for (auto targetAirplane : airplanes) {
                targetAirplane->setRotation(0);
                targetAirplane->setPosition(parent->getContentSize().width*poses.at(n), parent->getContentSize().height*posY);
                n++;
            }
        }));
        
        auto targetAirplane = airplanes.at(!fromLeft);
        auto targetHand = hands.at(!fromLeft);
        auto bezier = bezierCustomActions(parent, targetAirplane, Vec2(poses.at(fromLeft), posY), (OhanamiBezierType)num, fromLeft, pow_);
        float firstDuration = .1;
        auto airplaneAction = TargetedAction::create(targetAirplane, Sequence::create(MoveBy::create(firstDuration, Vec2(parent->getContentSize().width*.02*pow_, 0)), createSoundAction("hyu.mp3"), EaseInOut::create(bezier, 1.5), NULL));
        auto handAction = TargetedAction::create(targetHand, Sequence::create(RotateBy::create(firstDuration, 10*pow_), RotateBy::create(.4, -50*pow_), RotateBy::create(.2, 40*pow_), NULL));
        actions.pushBack(Spawn::create(airplaneAction, handAction, NULL));
        
        actions.pushBack(createChangeAction(.1, .1, airplanes.at(!fromLeft), airplanes.at(fromLeft)));
        
        i++;
    }
    
    actions.pushBack(DelayTime::create(1));
    
    return Sequence::create(actions);
}


void OhanamiActionManager::clearParticleAction(cocos2d::Node *parent)
{
    auto particle=ParticleSystemQuad::create("particle_texture.plist");
    particle->setAutoRemoveOnFinish(true);
    particle->resetSystem();
    particle->setDuration(-1);
    particle->setTotalParticles(50);
    particle->setTexture(Sprite::create("sakura.png")->getTexture());
    particle->setStartSize(parent->getContentSize().width*.05);
    particle->setStartSizeVar(parent->getContentSize().width*.005);
    particle->setEndSize(particle->getStartSize());
    particle->setSpeed(parent->getContentSize().height*.2);
    particle->setPosition(parent->getContentSize().width/2,parent->getContentSize().height+particle->getStartSize());
    particle->setPosVar(Vec2(parent->getContentSize().width*.8, 0));
    parent->addChild(particle);
}

