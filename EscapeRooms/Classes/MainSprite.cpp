//
//  MainLayer.cpp
//  Kebab
//
//  Created by 成田凌平 on 2016/12/18.
//
//

#include "MainSprite.h"
#include "DataManager.h"
#include "NotificationKeys.h"
#include "PopUpLayer.h"
#include "AnimateLayer.h"
#include "Utils/Common.h"
#include "Utils/FireBaseBridge.h"
#include "Utils/InterstitialBridge.h"
#include "ConfirmAlertLayer.h"
#include "SettingDataManager.h"
#include "AnchorSprite.h"
#include "ZOrders.h"

#include "EscapeDataManager.h"

#include "SupplementsManager.h"
#include "TouchManager.h"
#include "Utils/AudioManager.h"

#include "AnswerManager.h"
#include "Utils/NativeAdManager.h"

#include "SettingScene.h"
#include "AnalyticsManager.h"
#include "Utils/BannerBridge.h"

#include "Utils/ShakeGestureManager.h"

#include "ClearScene.h"


using namespace cocos2d;

MainSprite* MainSprite::create(Rect rect) {
	auto node =new MainSprite;
    if (node&&node->init(rect)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool MainSprite::init(Rect rect) {
    
    if (!EscapeStageSprite::init(DataManager::sharedManager()->getBackName())) {
        return false;
    }
    
    setPosition(Vec2(rect.origin.x, rect.origin.y));
    setLocalZOrder(Zorder_Main);

    //画像作成
    setScale(rect.size.width/getContentSize().width, rect.size.height/getContentSize().height);
    setCascadeOpacityEnabled(true);
    
    //
    loadData();
    //補完画像追加
    addSupplements();
    //背景のカスタムアクション、サウンドをチェック
    backCustomAction(true);
    
    //listnerを登録
    addEventListner();
    
    return true;
}

#pragma mark- データ
void MainSprite::loadData()
{
    touchVec=DataManager::sharedManager()->getTouchDataArray();
    DataManager::sharedManager()->reloadNowBack();
    
    //パスコードのリセット
    AnswerManager::getInstance()->resetPasscode();
}

#pragma mark - 補完画像
void MainSprite::addSupplements()
{
    SupplementsManager::getInstance()->addSupplementToMain(this,DataManager::sharedManager()->getNowBack(),true);
    SupplementsManager::getInstance()->clearPatternSprites(false);
}

#pragma mark- カスタムアクション-ロード時
void MainSprite::backCustomAction(bool didWarp)
{//背景のカスタムアクション
    auto map=DataManager::sharedManager()->getBackData();
    
    //移動時サウンド
    if(didWarp){
        if (!map["playSEWithFlag"].isNull()) {
            auto dic=map["playSEWithFlag"].asValueMap();
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(dic["flag"].asInt())) {
                Common::playSE(dic["name"].asString().c_str());
            }
        }
        else if (!map["playSE"].isNull()) {
            Common::playSE(map["playSE"].asString().c_str());
        }
    }
    
    //カスタムアクション
    if (!map["customActionKey"].isNull()) {
        auto key=map["customActionKey"].asString();
        SupplementsManager::getInstance()->customBackAction(this,DataManager::sharedManager()->getNowBack(), key,ShowType_Normal);
    }
}


#pragma mark- Gesture
void MainSprite::addEventListner()
{
    auto listner=EventListenerTouchOneByOne::create();
    listner->onTouchBegan=[this](Touch*touch,Event*event)
    {//タッチ可能か判定->可能な場合は処理するmapを保持する。->touchEndで処理
        log("タッチ開始");

        if (TouchManager::getInstance()->getIsManagingTouch()) {
            log("他のタッチを処理中です");
            return true;
        }
        
        //init
        TouchManager::getInstance()->resetTouchInfo();
        
        //thisを基準にしたtouchpointを取得
        auto pos=Vec2(touch->getLocation().x, touch->getLocation().y-(getPosition().y-getBoundingBox().size.height/2));

        Rect rect=Rect(0, 0, getBoundingBox().size.width, getBoundingBox().size.height);
        if (rect.containsPoint(pos))
        {//main内をタップ
            TouchManager::getInstance()->touchStartPos=pos;

            //全てのタッチ領域をちぇっくする
            int i = 0;
            for (Value v :touchVec)
            {
                i++;
                
                auto map=v.asValueMap();
                
                if (TouchManager::getInstance()->isContained(this, map, pos))
                {//タッチ領域なら処理
                    //タッチ情報の確認、保持
                    auto result=TouchManager::getInstance()->manageTouchStart(map, touch, this, false);
                    if (result==TouchType_Continue) {
                        continue;
                    }
                    else{
                        break;
                    }
                }
                
            }
        }
        else{
            //main外
            return false;
        }
        
        return true;
    };
    
    listner->onTouchMoved=[this](Touch*touch,Event*event)
    {//ドラッグ処理
        TouchManager::getInstance()->dragAction(false,touch,this,false);
        
        return true;
    };
    
    listner->onTouchEnded=[this](Touch*touch,Event*evnet)
    {//touchBegan(or shake)で保持したmapをもとに処理を行う。
        this->manageTouchMap();
        
        return true;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listner, this);
    
    //シェイクジェスチャーを登録
    ShakeGestureManager::getInstance()->addShakeGesture(this, [this](){
        //*注意事項：複数回呼ばれます。PromoteHintLayerなど上にレイヤーが表示されている時も呼ばれています。
        auto shakeV=DataManager::sharedManager()->getBackData()["shakeMap"];
        if (!shakeV.isNull()&&
            DataManager::sharedManager()->getShowingItemID()==0&&
            !DataManager::sharedManager()->getIsShowingSideBar()&&
            !DataManager::sharedManager()->getIsShowingPromoteHint()
            ) {
            if (TouchManager::getInstance()->getIsManagingTouch()) {
                log("他のタッチを処理中です");
                return;
            }
            
            //log("シェイクアクション");
            TouchManager::getInstance()->resetTouchInfo();
            TouchManager::getInstance()->manageTouchStart(shakeV.asValueMap(), NULL, this, false);
            this->manageTouchMap();
        }
    });
}


void MainSprite::manageTouchMap()
{
    //priorityがあれば
    auto priorityWarpMap=TouchManager::getInstance()->priorityWarpMap;
    if (priorityWarpMap.size()>0) {
        auto replaceType=ReplaceTypeFadeIn;
        if (!priorityWarpMap["replaceType"].isNull()) {
            replaceType=(ReplaceType)priorityWarpMap["replaceType"].asInt();
        }
        warp(priorityWarpMap["warp"].asInt(),replaceType);
        auto se=priorityWarpMap["playSE"];
        if (!se.isNull()) {
            Common::playSE(se.asString().c_str());
        }
        
        return ;
    }
    
    
    auto touchMap=TouchManager::getInstance()->touchMap;
    if (touchMap.size()==0) {
        return ;
    }
    
    //処理をスタート タッチがぶつかる可能性のあるシーンに注意 customAction処理で途中returnされる場合がある
    if (!DataManager::sharedManager()->getBackData()["shakeMap"].isNull()) {
        TouchManager::getInstance()->setIsManagingTouch(true);
    }
    
    //特有のseがあれば再生
    if (!touchMap["playSE"].isNull()) {
        AudioManager::getInstance()->playSE(touchMap["playSE"].asString().c_str(), true);
    }
    
    //カスタムアクションを実行。指定がなければすぐcallback帰ってきます
    this->manageCustomAction(touchMap["customActionKey"].asString(),[this](Ref*ref)
                             {
                                 //2個目のカスタムアクション
                                 auto key=TouchManager::getInstance()->touchMap["customActionKey_1"].asString();
                                 if (key.size()==0) {
                                     key=AnswerManager::getInstance()->getCustomActionKey_1();
                                 }
                                 
                                 
                                 this->manageCustomAction(key,[this](Ref*ref){
                                     //supplementsを取り除くアニメーションチェック
                                     auto removeMap=TouchManager::getInstance()->touchMap["removeAnimate"];
                                     std::string fileName;
                                     float duration=1;
                                     if (!removeMap.isNull()) {
                                         fileName=removeMap.asValueMap()["name"].asString();
                                         duration=removeMap.asValueMap()["duration"].asFloat();
                                     }
                                     
                                     
                                     SupplementsManager::getInstance()->animateSupplement(this,fileName, SupAnimateType_FadeOut, duration,0, [this](Ref*ref){
                                         auto animationID=TouchManager::getInstance()->touchMap["animationID"].asInt();
                                         if (AnswerManager::getInstance()->animationIDs.size()>0) {
                                             animationID=AnswerManager::getInstance()->animationIDs.at(0);
                                             AnswerManager::getInstance()->animationIDs.clear();
                                         }
                                         
                                         Value animation=DataManager::sharedManager()->getAnimationData(animationID);
                                         //メインアニメーションがあれば実行
                                         this->showAnimateLayer(animation, [this](Ref*ref){
                                             //終了後のカスタムアクション
                                             auto key=TouchManager::getInstance()->touchMap["customActionKey_2"].asString();
                                             if (key.size()==0) {
                                                 key=AnswerManager::getInstance()->getCustomActionKey_2();
                                             }
                                             
                                             this->manageCustomAction(key,[this](Ref*ref){
                                                 
                                                 //間違い探し処理
                                                 this->manageMysteryAction(TouchManager::getInstance()->touchMap["mysteryID"], [this](Ref*ref){
                                                     //データ処理へ
                                                     this->manageDataByTouch(TouchManager::getInstance()->touchMap);
                                                     //処理の終了
                                                     TouchManager::getInstance()->setIsManagingTouch(false);
                                                 });
                                             });
                                         });
                                     });
                                 });
                             });
}

#pragma mark カスタムアクション-タッチ
void MainSprite::manageCustomAction(std::string key, ccMenuCallback callback)
{//処理を進めるタイミングでcallbackを返す
    if (key.length()==0)
    {//カスタムアクションなし
        callback(NULL);
    }
    else{
        TouchManager::getInstance()->customAction(key, this, false, [callback](bool success){
            if (success) {
                callback(NULL);
            }
            else{
                //処理を中断
                TouchManager::getInstance()->setIsManagingTouch(false);
            }
        });
    }
}


#pragma mark アニメーションレイヤ
void MainSprite::showAnimateLayer(Value animation, ccMenuCallback callback)
{
    if (animation.isNull()) {
        callback(NULL);
        return;
    }
    
    //needFlag
    if (!animation.asValueMap()["needFlagIDs"].isNull()) {
        auto flags=animation.asValueMap()["needFlagIDs"].asValueVector();
        for (auto v :flags) {
            if (!DataManager::sharedManager()->getEnableFlagWithFlagID(v.asFloat()))
            {//指定フラグがたっていないのでアニメーションはなし
                callback(NULL);
                return;
            }
        }
    }
        
    auto animateLayer=AnimateLayer::create(animation.asValueMap(),Rect(this->getPosition().x, this->getPosition().y, this->getBoundingBox().size.width, this->getBoundingBox().size.height),this,[this,callback](Ref*sender)
                                               {//アニメーション終了callback
                                                   callback(NULL);
                                               });
    animateLayer->setLocalZOrder(999);
    animateLayer->setNode(this);
    //親(gameScene)に貼る
    this->getParent()->addChild(animateLayer);
}

#pragma mark- 間違い探し正解処理
void MainSprite::manageMysteryAction(cocos2d::Value mystery, ccMenuCallback callback)
{
    if (mystery.isNull()) {
        callback(NULL);
        return;
    }
    auto mysteryID=mystery.asInt();

    SupplementsManager::getInstance()->correctAction(this, false, mysteryID, [callback](bool success){
        callback(NULL);
    });
}

#pragma mark- データ処理
void MainSprite::manageDataByTouch(ValueMap map)
{//タッチによるフラグ、アイテム周りのデータ処理    
    //フラグ処理
    AnswerManager::getInstance()->manageEnFlagIDs(map);
    
    //カメラ処理
    AnswerManager::getInstance()->manageCameraData(map);
    
    //retain処理がない&&アイテム使用時 アイテムを削除する( なければ実行されない安全なメソッド)
    AnswerManager::getInstance()->manageRemoveItem(map,false);
    
    //間違い探し処理
    AnswerManager::getInstance()->manageMysteryData();
    
    //reload
    auto needReload=map["needReload"].asBool();
    
    //item
    auto itemID=map["getItemID"].asInt();
    if (itemID>0||AnswerManager::getInstance()->getItemIDs.size()>0)
    {//取得できるアイテムがあればポップアップ表示と取得
        if (AnswerManager::getInstance()->getItemIDs.size()>0) {
            itemID=AnswerManager::getInstance()->getItemIDs.at(0);
        }
        
        //se
        Common::playSE("p.mp3");
        //アイテム表示
        DataManager::sharedManager()->setShowingItemID(itemID);
        DataManager::sharedManager()->addNewItem(itemID);
        this->showPopUp();
        
        //アイテム取得時は指定がない限りリロード
        if (map["needReload"].isNull()) {
            needReload=true;
        }
    }
    
    //
    if (needReload)
    {//補完画像がある場合などはreloadする
        this->reloadSprite(false,ReplaceTypeNone);
    }
    
    Warp warpStruct;
    if(WarpManager::getInstance()->warps.size()>0)
    {//優先
        warpStruct.warpID=WarpManager::getInstance()->warps.at(0).warpID;
        warpStruct.replaceType=WarpManager::getInstance()->warps.at(0).replaceType;
        WarpManager::getInstance()->warps.clear();
    }
    else{
        warpStruct=WarpManager::getInstance()->getWarpStruct(map);
    }
    
    if (warpStruct.warpID>0)
    {//遷移先のIdがあれば移動
        warp(warpStruct.warpID,warpStruct.replaceType);
    }
    
    AnswerManager::getInstance()->clear();
}

void MainSprite::warp(int warpID, ReplaceType replaceType)
{
    DataManager::sharedManager()->setNowBack(warpID,replaceType);
    
    if (replaceType==ReplaceTypeNone)
    {//animationがある場合はmainSpriteを再createする
        SupplementsManager::getInstance()->showPanel(this);
    }
}

#pragma mark- 画像変更
void MainSprite::reloadSprite(bool didWarp, ReplaceType replaceType)
{
    if(replaceType==ReplaceTypeNone)
    {//アニメーションなし.textureのみ入れ替え
        //補完画像を全てremove
        removeAllChildren();
        stopAllActions();
        
        //mainのテクスチャの入れ替え
        auto size=getBoundingBox().size;
        auto spr = EscapeStageSprite::createWithSpriteFileName(DataManager::sharedManager()->getBackName());
        setSpriteFrame(spr->getSpriteFrame());
        setScale(size.width/getContentSize().width);
        
        //データリロード
        loadData();
        addSupplements();
        //背景のカスタムアクション、サウンドをチェック
        backCustomAction(didWarp);
    }
    else
    {//遷移のアニメーション指定あり。->gameSceneで処理する
        mDelegate->changeMainSprite(replaceType);
        //ネイティブ広告を非表示にします。
        SupplementsManager::getInstance()->hidePanel();
    }
}

#pragma mark- ポップアップ
void MainSprite::showPopUp()
{
    //ネイティブ広告を非表示にします。
    SupplementsManager::getInstance()->hidePanel();

    auto popup=PopUpLayer::create([this]{
        SupplementsManager::getInstance()->showPanel(this);
    });
    popup->setMainRect(Rect(getPosition().x, getPosition().y, getBoundingBox().size.width, getBoundingBox().size.height));
    popup->setTouchThroughRect(Rect(0, getPosition().y+getBoundingBox().size.height/2, getBoundingBox().size.width, UINT_MAX));
    getParent()->addChild(popup);
}

#pragma mark delay
void MainSprite::delayAndCallBack(float duration, ccMenuCallback callback)
{
    auto layer=SupplementsManager::getInstance()->createDisableLayer();
    getParent()->addChild(layer);
    
    auto wait=DelayTime::create(duration);
    auto call=CallFunc::create([callback,layer](){
        layer->removeFromParent();
        callback(NULL);
    });
    
    runAction(Sequence::create(wait,call, NULL));
}

#pragma mark- dealloc
MainSprite::~MainSprite()
{
    //log("mainを解放");
}
