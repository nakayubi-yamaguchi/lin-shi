//
//  TopMenuSprite.cpp
//  Kebab
//
//  Created by 成田凌平 on 2016/12/17.
//

#include "TopMenuSprite.h"
#include "Utils/Common.h"
#include "UIParts/MenuItemScale.h"
#include "DataManager.h"
#include "NotificationKeys.h"
#include "Utils/NativeAdManager.h"
#include "ZOrders.h"
#include "SupplementsManager.h"
#include "SettingScene.h"
#include "EscapeDataManager.h"
#include "AnalyticsManager.h"
#include "PromoteHintLayer.h"
#include "GameScene.h"

using namespace cocos2d;

TopMenuSprite* TopMenuSprite::create(cocos2d::Rect rect)
{
    auto node=new TopMenuSprite();
    if (node&&node->init(rect)) {
        node->autorelease();
        return node;
    }
    CC_SAFE_DELETE(node);
    return node;
}

bool TopMenuSprite::init(cocos2d::Rect rect)
{
    if (!Sprite::init()) {
        return false;
    }
    
    setTextureRect(Rect(0, 0, rect.size.width, rect.size.height));
    setPosition(Vec2(rect.origin.x, rect.origin.y));
    setColor(Color3B::BLACK);
    setLocalZOrder(Zorder_TopMenu);
    setEnable(true);
    
    //枠間のスペース
    menuSpace=5*Common::getDensity();
    //アイテム格納枠サイズ
    boxWidth=MIN((getContentSize().width-menuSpace*(MaxItem+1))/(float)(MaxItem+1),getContentSize().height);
    DataManager::sharedManager()->setItemBoxWidth(boxWidth);
    //メニューボタンサイズ
    menuWidth=boxWidth;
    
    //UI作成
    if (EscapeDataManager::getInstance()->getTypePlayTitleCondition()!=TitleCondition_TUTORIAL) {
        createMenuButton();
    }
    createItemBoxes();
    
    //ミニゲーム中か判別
    if (DataManager::sharedManager()->isPlayingMinigame()) {
        auto foundCount=DataManager::sharedManager()->getFoundMysteryCount();
        auto mysteryCount=DataManager::sharedManager()->getMiniGameData()["mysteryCount"].asInt();
        if (foundCount==mysteryCount) {
            //過去バージョン(3.2以下)によるバグ検知
            DataManager::sharedManager()->resetMinigameData();
            foundCount=0;
        }
        
        adaptMiniGame();
    }
    
    //
    addNotification();
    
    return true;
}

#pragma mark- UI
void TopMenuSprite::createMenuButton()
{
    
    std::string fileName="hint_icon.png";
    
    auto spr=Sprite::createWithSpriteFrameName(fileName);
    auto selected=Sprite::createWithSpriteFrameName(fileName);
    selected->setOpacity(125);
    
    auto menuItem=MenuItemScale::create(spr,selected,[this](Ref*ref){
        Common::playClick();
        auto menuIt = (MenuItemScale*)ref;
        //ポップアップを表示する時は広告を非表示。
        SupplementsManager::getInstance()->hidePanel();
        menuIt->setEnabled(false);

        auto push = CallFunc::create([this](){
            this->showHintLayer();
        });
        auto delay = DelayTime::create(.5);
        auto callback = CallFunc::create([menuIt](){
            menuIt->setEnabled(true);
        });
        
        this->runAction(Sequence::create(push, delay, callback, NULL));
    });
    menuItem->setScale(menuWidth/spr->getContentSize().width);
    auto menu=Menu::create(menuItem, NULL);
    menu->setPosition(menuWidth/2+menuSpace, getContentSize().height/2);
    addChild(menu);
}

void TopMenuSprite::showHintLayer()
{
    auto promoteHintAlert = PromoteHintLayer::create([this](Ref* sender){
        if (DataManager::sharedManager()->getShowingItemID()==0) {
            //panelの再表示
            auto parent=(GameScene*)getParent();
            SupplementsManager::getInstance()->showPanel(parent->mainSprite);
        }
    });
    promoteHintAlert->showAlert(Director::getInstance()->getRunningScene());
}

void TopMenuSprite::createItemBoxes()
{
    //画像作成lambda
    auto sprite=[this](bool selected,int itemID)
    {
        auto spr=SupplementsManager::getInstance()->createItemBox(itemID);
        
        if (!selected&&spr!=NULL) {
            spr->setOpacity(120);
        }
        
        return spr;
    };
    
    auto items=DataManager::sharedManager()->items;
    Vector<MenuItem*>menuitems;
    
    for (int i=0; i<items.size(); i++)
    {
        int itemID=items[i];
        
        auto spr=sprite(false,itemID);
        if (spr==NULL) {
            log("存在しないitemID");
            continue;
        }
        auto selected=sprite(true,itemID);
        
        auto menuitem=MenuItemScale::create(spr, spr,[](Ref*ref){});
        auto menuitem_selected=MenuItemScale::create(selected, selected,[](Ref*ref){});
        
        auto toggle=MenuItemToggle::createWithCallback([this](Ref*ref){
            auto _ref=(MenuItemToggle*)ref;
            if (_ref->getSelectedIndex()==1)
            {//選択を開始
                //表示中のアイテムなら選択できなくする
                if (DataManager::sharedManager()->getShowingItemID()==_ref->getTag()) {
                    log("表示中のアイテムですよ");
                    _ref->setSelectedIndex(0);
                    return ;
                }
                
                //se
                Common::playSE("cha.mp3");
                
                DataManager::sharedManager()->setSelectedItemID(_ref->getTag());//tag=itemID
                //このアイテム以外の選択状態をリセット
                for (Node*child :itemsMenu->getChildren()) {
                    if (child->getTag()!=_ref->getTag()) {
                        auto _child=(MenuItemToggle*)child;
                        _child->setSelectedIndex(0);
                    }
                }
            }
            else
            {//選択中のアイテムを再選択->ポップアップ表示してもらう
                DataManager::sharedManager()->setSelectedItemID(0);//選択解除

                if (DataManager::sharedManager()->getShowingItemID()>0) {
                    log("すでにポップアップ表示中");
                    return ;
                }
                DataManager::sharedManager()->setShowingItemID(_ref->getTag());//tag=itemID
                Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyWillShowPopUp);
            }
            
        }, menuitem,menuitem_selected,NULL);
        toggle->setScale(boxWidth/spr->getContentSize().width);
        toggle->setTag(itemID);//itemIDとtagを結びつけておく
        
        //初期選択状態
        if (itemID==DataManager::sharedManager()->getSelectedItemID()) {
            toggle->setSelectedIndex(1);
        }
        else{
            toggle->setSelectedIndex(0);
        }
        
        menuitems.pushBack(toggle);
    }
    
    itemsMenu=Menu::createWithArray(menuitems);
    itemsMenu->alignItemsHorizontallyWithPadding(menuSpace);
    itemsMenu->setPosition(menuSpace*2+boxWidth*(menuitems.size()*.5+1)+menuSpace*(menuitems.size()-1)*.5, getContentSize().height/2);
    addChild(itemsMenu);
}

void TopMenuSprite::adaptMiniGame()
{
    minigameLabel=Label::createWithTTF("", MyFont, boxWidth*.8);
    reloadMinigameLabel();//テキストセット
    
    minigameLabel->setPosition(getContentSize().width-minigameLabel->getContentSize().width/2-menuSpace,getContentSize().height/2);
    addChild(minigameLabel);
}

void TopMenuSprite::reloadMinigameLabel()
{
    if (minigameLabel) {
        auto foundCount=DataManager::sharedManager()->getFoundMysteryCount();
        auto mysteryCount=DataManager::sharedManager()->getMiniGameData()["mysteryCount"].asInt();
        
        minigameLabel->setString(StringUtils::format("%d / %d",foundCount,mysteryCount));
    }
}

void TopMenuSprite::reloadItemBox()
{
    //所持アイテムをリロード(removeしてaddしている。。)
    itemsMenu->removeFromParent();
    this->createItemBoxes();
}

#pragma mark- 通知
void TopMenuSprite::addNotification()
{
    Director::getInstance()->getEventDispatcher()->addCustomEventListener(NotificationKeyReloadItems, [this](Event*event){
        this->reloadItemBox();
    });
    
    Director::getInstance()->getEventDispatcher()->addCustomEventListener(NotificationKeyFoundMystery, [this](Event*event){
        //ミニゲーム用
        this->reloadMinigameLabel();
    });
}

#pragma mark- dealloc
TopMenuSprite::~TopMenuSprite(){
    log("Top解放");
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(NotificationKeyReloadItems);
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(NotificationKeyFoundMystery);
}

