//
//  InfoLayer.h
//  Kebab
//
//  Created by yamaguchinarita on 2016/12/21.
//
//

#ifndef __Kebab__InfoLayer__
#define __Kebab__InfoLayer__

#include "cocos2d.h"

USING_NS_CC;

class InfoLayer : public cocos2d::LayerColor
{
public:
    Sprite* mainSprite;//editboxやらラベル包括
    Label* titleLabel;
    
    bool init();
    //static InfoLayer *create();
    
    CREATE_FUNC(InfoLayer);
    
    ~InfoLayer();
    
private:
    //UI
    void createMain();
    
    //タッチ
    void addTouchListener();
    
    //表示アニメーション
    void showAnimation();

};

#endif /* defined(__Kebab__InfoLayer__) */
