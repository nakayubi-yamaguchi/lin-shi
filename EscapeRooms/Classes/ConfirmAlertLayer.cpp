//
//  ConfirmAlertLayer.cpp
//  Kebab
//
//  Created by yamaguchinarita on 2016/12/23.
//
//

#include "ConfirmAlertLayer.h"

using namespace cocos2d;

ConfirmAlertLayer* ConfirmAlertLayer::create(ConfirmAlertType buttonCount, std::string message, const ccMenuCallback &callback){
    auto layer =new ConfirmAlertLayer;
    
    if (layer&&layer->init(buttonCount, message, callback)) {
        layer->autorelease();
    }
    
    return layer;
}

bool ConfirmAlertLayer::init(ConfirmAlertType buttonCount, std::string message, const ccMenuCallback &callback) {
    
    std::vector<std::string> buttonTexts;
    
    if (buttonCount == ConfirmAlertType_Single) {
        buttonTexts = {"CLOSE"};
    }
    else{
        buttonTexts = {"CLOSE", "OK"};
    }

    if (!CustomAlert::init(AlertTitle_INFORMATION, message, buttonTexts,false, callback,nullptr)) {
        return false;
    }
    
    return true;
}

#pragma mark- 解放
ConfirmAlertLayer::~ConfirmAlertLayer()
{
    log("Customレイヤーを解放する。");
}


