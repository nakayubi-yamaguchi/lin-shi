//
// Created by RyoKosuge on 2019/04/02.
//

#include "RewardPlayer.h"
#include "../Utils/AdmobRewardBridge.h"
#include "../AdX/AdXRewardBridge.h"
#include "../Fluct/FluctRewardBridge.h"

std::once_flag RewardPlayer::initFlag;
RewardPlayer *RewardPlayer::instance = nullptr;

RewardPlayer::RewardPlayer() {
    this->movieType = RewardMovieType::FLUCT;
}

void RewardPlayer::refresh(RewardMovieType rewardMovieType){
    this->movieType = rewardMovieType;
    log("ムービータイプは[%s]",getMovieType().c_str());
}

void RewardPlayer::load() {
    switch (this->movieType) {
        case RewardMovieType::FLUCT:
            FluctAdXReward::getInstance()->setDelegate(this);
            FluctAdXReward::getInstance()->load();
            break;
        case RewardMovieType::ADMOB:
            AdmobRewardBridge::setDelegate(this);
            AdmobRewardBridge::loadingAdmobReward();
            break;
    }
}

bool RewardPlayer::isReady() {
    bool ready = false;
    switch (this->movieType) {
        case RewardMovieType::FLUCT:
            ready = FluctAdXReward::getInstance()->isReady();
            break;
        case RewardMovieType::ADMOB:
            ready = AdmobRewardBridge::isReady();
            break;
    }
    return ready;
}

void RewardPlayer::show() {
    switch (this->movieType) {
        case RewardMovieType::FLUCT:
            FluctAdXReward::getInstance()->show();
            break;
        case RewardMovieType::ADMOB:
            AdmobRewardBridge::show();
            break;
    }
}

void RewardPlayer::setDelegate(RewardPlayerDelegate* delegate)
{
    this->delegate = delegate;
}

// FluctAdXRewardDelegate
void RewardPlayer::didStartFluctAdXReward() {
    if (this->delegate != nullptr) {
        this->delegate->didStartReward();
    }
}

void RewardPlayer::didCloseFluctAdXReward(bool completed) {
    if (this->delegate != nullptr) {
        this->delegate->didCloseReward(completed);
    }
}

// AdMobRewardBridgeDelegate
void RewardPlayer::didStart() {
    if (this->delegate != nullptr) {
        this->delegate->didStartReward();
    }
}

void RewardPlayer::didClose(bool completed) {
    if (this->delegate != nullptr) {
        this->delegate->didCloseReward(completed);
    }
}
