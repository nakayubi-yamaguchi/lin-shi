
#ifndef PROJ_ANDROID_STUDIO_REWARDPLAYER_H
#define PROJ_ANDROID_STUDIO_REWARDPLAYER_H

#include "../FluctAdX/FluctAdXReward.h"
#include "../Utils/AdmobRewardBridge.h"

#include <mutex>
#include <cassert>

enum class RewardMovieType {
    FLUCT = 0,
    ADMOB,
};

class RewardPlayerDelegate {
public:
    virtual void didStartReward() {};
    virtual void didCloseReward(bool completed) {};
};

class RewardPlayer: public FluctAdXRewardDelegate, public AdmobRewardBridgeDelegate {
public:
    static RewardPlayer* getInstance() {
        std::call_once(initFlag, create);
        assert(instance);
        return instance;
    }

    RewardPlayer(const RewardPlayer&) = delete;

    RewardPlayer& operator=(const RewardPlayer&) = delete;

    RewardPlayer(RewardPlayer&&) = delete;

    RewardPlayer& operator=(RewardPlayer&&) = delete;

    std::string getMovieType() {
        switch (movieType) {
            case RewardMovieType::FLUCT:
                return "fluct";
            case RewardMovieType::ADMOB:
                return "admob";
        }
    }

    void refresh(RewardMovieType rewardMovieType);

    void load();

    bool isReady();

    void show();

    void setDelegate(RewardPlayerDelegate* delegate);

private:
    static void create() {
        instance = new RewardPlayer;
    }

    static std::once_flag initFlag;
    static RewardPlayer* instance;

    RewardMovieType movieType;
    RewardPlayerDelegate* delegate;

    RewardPlayer();
    ~RewardPlayer() = default;

    // FluctAdXRewardDelegate
    void didStartFluctAdXReward();
    void didCloseFluctAdXReward(bool completed);

    // AdMobRewardBridgeDelegate
    void didStart();
    void didClose(bool completed);
};


#endif //PROJ_ANDROID_STUDIO_REWARDPLAYER_H
