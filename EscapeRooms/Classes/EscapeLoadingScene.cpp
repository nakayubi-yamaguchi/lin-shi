//
//  EscapeLoadingScene.cpp
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/09/30.
//
//

#include "EscapeLoadingScene.h"
#include "EscapeDataManager.h"
#include "TitleScene.h"
#include "DataManager.h"
#include "EscapeCollectionScene.h"
#include "EscapeCollectionCell.h"
#include "ConfirmAlertLayer.h"
#include "Utils/BannerBridge.h"
#include "SettingDataManager.h"
#include "Utils/NativeAdManager.h"
#include "Utils/FireBaseBridge.h"
#include "Utils/InterstitialBridge.h"

#include "Utils/DownloadManager.h"
#include "Utils/UtilsMethods.h"

#include "ClearScene.h"
#include "AnalyticsManager.h"
#include "GameScene.h"

#include "Utils/BannerManager.h"

using namespace cocos2d;

Scene* EscapeLoadingScene::createScene(LoadingType type) {
	
    std::vector<LoadingType> loadingTypes = {type};
    return createScene(loadingTypes);
}

Scene* EscapeLoadingScene::createScene(std::vector<LoadingType> typeVector) {
    auto scene = Scene::create();
    auto layer = EscapeLoadingScene::create(typeVector);
    scene->addChild(layer);
    
    return scene;
}

EscapeLoadingScene* EscapeLoadingScene::create(std::vector<LoadingType> typeVector) {
    auto node =new EscapeLoadingScene;
    if (node && node->init(typeVector)) {
        node->autorelease();
        return node;
    }
    
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool EscapeLoadingScene::init(std::vector<LoadingType> typeVector) {
    if (!LayerColor::initWithColor(Color4B(Common::getColorFromHex(WhiteColor)))) {
        return false;
    }

    m_loadingTypeVector = typeVector;
    m_performNum = 0;
    
    createMain();
    showThumbImages();
    
    return true;
}

void EscapeLoadingScene::onEnterTransitionDidFinish()
{

    Common::performProcessForDebug([](){}, [this](){
        showPanelAd();
    });
    startLoadingTask();
    //
    createUserID();
    
    if (getLoadingType()==LoadingType_Icons) {
        //起動後時間あけないで呼ぶと値が帰ってこないので注意
        //ABテスト用データを取得
        fetchABTests();
    }
}

#pragma mark- パブリック
LoadingType EscapeLoadingScene::getLoadingType()
{
    return  m_loadingTypeVector[m_performNum];
}

std::vector<LoadingType> EscapeLoadingScene::getLoadingTypes()
{
    return m_loadingTypeVector;
}

#pragma mark- プライベート
void EscapeLoadingScene::createMain()
{
    // 初期化
    SwithInfo swicthInfo;
    swicthInfo.fileName = "band.png";;
    swicthInfo.switchCount = 7;
    swicthInfo.frameTime = .18;
    swicthInfo.frameWidth = getContentSize().width*.7;
    
    indicator = NCIndicator::create(swicthInfo);
    indicator->setPosition(getContentSize().width/2, (getContentSize().height < 1200)? getContentSize().height*.63: getContentSize().height*.55);
    addChild(indicator);
    
    //log("デバイスの高さは%f",getContentSize().height);
    
    // 状態表示ラベル
    float fontSize = getContentSize().height*.025;
    std::string conditionText = Common::localize("Loading...", "読み込み中...");
    
    conditionLabel = Label::createWithTTF(conditionText.c_str(), Common::getUsableFontPath(HiraginoMaruFont), fontSize);
    conditionLabel->enableBold();
    conditionLabel->enableItalics();
    conditionLabel->setPosition(getContentSize().width/2, indicator->getPositionY()+indicator->getBoundingBox().size.height/2+conditionLabel->getContentSize().height);
    conditionLabel->setTextColor(Color4B(Common::getColorFromHex(BlackColor)));
    addChild(conditionLabel);
    //log("コンディションラベルは{%f,%f}",conditionLabel->getPositionX(),conditionLabel->getPositionY());
    
    //進捗ラベル
    progressLabel = Label::createWithTTF("", HiraginoMaruFont, fontSize);
    progressLabel->enableBold();
    progressLabel->enableItalics();

    progressLabel->setTextColor(conditionLabel->getTextColor());
    addChild(progressLabel);
}

void EscapeLoadingScene::showThumbImages()
{//読み込み中とかのラベルの上スペースの中心に表示
    auto icon_width=getContentSize().width*.19;
    auto cloud_width=icon_width*4;
    auto icon_posX=getContentSize().width/2-(icon_width+cloud_width)/2+icon_width/2;
    auto condition_top=conditionLabel->getPositionY()+conditionLabel->getContentSize().height/2;
    auto icon_posY=condition_top+(getContentSize().height-BannerBridge::getBannerHeight()-condition_top)/2;
    auto cloud_posX=icon_posX+(icon_width+cloud_width)/2;
    
    auto talk_icon=Sprite::createWithSpriteFrameName("pig_talk.png");
    talk_icon->setScale(icon_width/talk_icon->getContentSize().width);
    talk_icon->setPosition(icon_posX, icon_posY);
    addChild(talk_icon);
    
    auto cloud=Sprite::createWithSpriteFrameName("pig_talk_cloud.png");
    cloud->setScale(cloud_width/cloud->getContentSize().width);
    cloud->setPosition(cloud_posX,icon_posY);
    addChild(cloud);
    
    auto talk_label_size=Size(cloud->getContentSize().width*.875, cloud->getContentSize().height-cloud->getContentSize().width*.1);
    auto margin_left=cloud->getContentSize().width*.04;//吹き出しの突起ぶん少しずらしてあげる
    
    talkLabel=Label::createWithTTF(DataManager::sharedManager()->getTalkMessage(), Common::getUsableFontPath(MyFont), talk_label_size.height*.33);
    talkLabel->setWidth(talk_label_size.width);
    talkLabel->setHeight(talk_label_size.height);
    talkLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
    talkLabel->setTextColor(Color4B::BLACK);
    talkLabel->setPosition(cloud->getContentSize().width/2+margin_left,cloud->getContentSize().height/2);
    cloud->addChild(talkLabel);
}

void EscapeLoadingScene::showPanelAd()
{
    //進捗ラベルの下
    auto loadingbarUnder = getContentSize().height-(indicator->getPositionY()-indicator->getContentSize().height);
    //バナーの上
    auto bannerUpper = getContentSize().height-BannerBridge::getBannerHeight();
    
    //y座標は進捗ラベルとバナーの中間をとる
    auto position = Vec2(getContentSize().width/2, (loadingbarUnder+bannerUpper)/2);
    
    BannerBridge::createRectangle(BANNER_APPID_RECTANGLE, position);
}

void EscapeLoadingScene::startLoadingTask()
{
    //ダウンロードに失敗した場合、バナーを削除する。
    auto removeBannerCallback = [](Ref* sender){
        //ダウンロード失敗時は先行プレイしようとしててもやめる。
        EscapeDataManager::getInstance()->setIsPlayPre(false);
        
        BannerBridge::removeBanner(NativeBridge::judgeID(BANNER_APPID_HEADER, BANNER_APPID_HEADER_TABLET));
        Director::getInstance()->replaceScene(TransitionFade::create(.5, EscapeCollectionScene::createScene(),Color3B::WHITE));
    };
    
    auto failedAlert = [this, removeBannerCallback](std::string message){
        BannerBridge::hideBanner(BANNER_APPID_RECTANGLE);
        
        auto alert = ConfirmAlertLayer::create(ConfirmAlertType_Single, message, removeBannerCallback);
        alert->showAlert(this);
    };
    
    switch (getLoadingType()) {
#pragma mark Loading Icon
        case LoadingType_Icons:{
            //アイコン画像をローディング。セルに自動反映。
            EscapeCollectionCell::loadingIconImage();
            
            auto delay = DelayTime::create(4);
            auto callback = CallFunc::create([this](){
                
                this->checkLoadingTypePerform([](){
                    BannerBridge::hideBanner(BANNER_APPID_RECTANGLE);
                    
                    Director::getInstance()->replaceScene(TransitionFade::create(.5, EscapeCollectionScene::createScene(),Color3B::BLACK));
                });
            });
            
            runAction(Sequence::create(delay, callback, NULL));
            
            break;
        }
#pragma mark To Title
        case LoadingType_Titles:{
            addNotification();
            
            if (!NativeBridge::isTablet()) {
                Common::performProcessForDebug([](){
                    BannerManager::getInstance()->createBanner(BannerPosition_Top);
                }, []{
                    BannerManager::getInstance()->createBanner(BannerPosition_Top);
                });
            }

                
            auto transitionTitle = [this](){
                EscapeDataManager::getInstance()->manageCatheAtlas(true, [](){
                    
                    BannerBridge::hideBanner(BANNER_APPID_RECTANGLE);
                    
                    //先行プレイをしようとしているかどうか
                    auto isPlayPre = EscapeDataManager::getInstance()->getIsPlayPre();
                    log("タイトル画面に遷移します。::先行プレイを開始しようとしている%d",isPlayPre);
                    
                    if (isPlayPre) {//先行プレイをしようとしている
                        //コインを使います。
                        DataManager::sharedManager()->useCoin(CoinSpot_Pre);
                        //コイン使用したら、解放状態をセーブする。指定インデックスをセットする
                        DataManager::sharedManager()->recordEternalDataOnPlay(RecordKey_PreMovie, Value(true));
                        //先行プレイ回数を保存
                        UserDefault::getInstance()->setIntegerForKey(UserKey_PrePlayCount, UserDefault::getInstance()->getIntegerForKey(UserKey_PrePlayCount,0)+1);
                        
                        //ユーザーデータを保存。
                        DataManager::sharedManager()->postUserData();
                        
                        //イベント名を先行プレイをコインで解放用に変更してあげること
                        auto eventName = "PreCoin";
                        auto titleName = EscapeDataManager::getInstance()->getPlayTitleName();
                        auto eventWithTitle = StringUtils::format("%s_%s", eventName, titleName.c_str());
                        
                        AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName(eventName);
                        AnalyticsManager::getInstance()->sendFirebaseAnalyticsWithName(eventWithTitle.c_str());

                        //リソースの読み込みが完了したので、先行プレイするフラグをなくす。
                        EscapeDataManager::getInstance()->setIsPlayPre(false);
                        
                        auto useCoinSt = DataManager::sharedManager()->getSystemMessage("useCoinWithPre");
                        UtilsMethods::showThanksAlert(useCoinSt.c_str(), [](Ref* sender){
                            Director::getInstance()->replaceScene(TransitionFade::create(.5, TitleScene::createScene(),Color3B::BLACK));
                        });
                    }
                    else {
                        bool replaceGameScene=false;

                        if (EscapeDataManager::getInstance()->getTypePlayTitleCondition()==TitleCondition_TUTORIAL) {
                            replaceGameScene=true;
                            DataManager::sharedManager()->resetAllData(true);//データ削除.クリア情報保持。
                        }
                        
                        if (replaceGameScene) {
                            Director::getInstance()->replaceScene(TransitionFade::create(1, GameScene::createScene(), Color3B::BLACK));
                        }
                        else{
                            
                            bool debug = false;
                            Common::performProcessForDebug([&debug](){
                                //debug = true;
                            }, nullptr);
                            
                            if (!debug) {
                                Director::getInstance()->replaceScene(TransitionFade::create(.5, TitleScene::createScene(),Color3B::BLACK));
                            }
                            else {
                                Director::getInstance()->replaceScene(TransitionFade::create(.5, ClearScene::createScene(),Color3B::BLACK));
                            }
                            
                        }
                    }
                });
            };
            
            bool isExistResource = EscapeDataManager::getInstance()->isExistPlayTitleResource();
            TitleCondition isType = EscapeDataManager::getInstance()->getTypePlayTitleCondition();

            if (isExistResource || isType == TitleCondition_TUTORIAL) {//リソースがある.もしくはチュートリアルなら.何秒かローディング時間とってから
                transitionTitle();
            }
            else{
                bool isConnected = FireBaseBridge::getInstance()->getIsConnected() || NativeBridge::isOnline();
                log("firebaseの接続状況%d,nativeの接続状況%d",FireBaseBridge::getInstance()->getIsConnected() , NativeBridge::isOnline());
                log("ネット接続状態%d",isConnected);
                if (!isConnected) {
                    auto delay = DelayTime::create(1);
                    auto callback = CallFunc::create([this,removeBannerCallback](){
                        BannerBridge::hideBanner(BANNER_APPID_RECTANGLE);

                        auto message = DataManager::sharedManager()->getSystemMessage("network_error_text");
                        auto alert = ConfirmAlertLayer::create(ConfirmAlertType_Single, message, removeBannerCallback);
                        alert->showAlert(this);
                    });
                    
                    runAction(Sequence::create(delay, callback, NULL));
                    break;
                }
                
                auto freeMb = NativeBridge::storageAvailableMb();
                if (freeMb < 30) {//端末の空き容量が30mbを切っていたら(jpgにしたから軽くなった。)
                    auto delay = DelayTime::create(2);
                    auto callback = CallFunc::create([failedAlert, freeMb](){
                        auto message = DataManager::sharedManager()->getSystemMessage("no_storage");
                        auto messageSt = StringUtils::format(message.c_str(), NativeBridge::transformThreeComma((float)freeMb).c_str());
                        
                        failedAlert(messageSt);
                    });
                    runAction(Sequence::create(delay, callback, NULL));
                    break;
                }
                
                EscapeDataManager::getInstance()->loadingPlayTitleData([transitionTitle, this, failedAlert](bool successed, std::string errorDescription){
                    log("タイトルのローディング成功したかどうか%d",successed);
                    if (!successed){
                        //auto message = DataManager::sharedManager()->getSystemMessage("download_failed");
                        //エラーの内容を表示する
                        failedAlert(errorDescription);
                    }
                    else {//リソースデータのダウンロード成功！
                        transitionTitle();
                    }
                });
            }
            
            break;
        }
#pragma mark To Collection Scene
        case LoadingType_RemoveCathe:{//コレクションシーンに遷移するとき
            
            BannerManager::getInstance()->removeBanner(BannerPosition_Top);
            
            bool isPlayingMinigame = DataManager::sharedManager()->isPlayingMinigame();
            DataManager::sharedManager()->resetAllData(false);
            
            //キャッシュ削除
            EscapeDataManager::getInstance()->manageCatheAtlas(false,[this, isPlayingMinigame](){
                
                TitleCondition isType = EscapeDataManager::getInstance()->getTypePlayTitleCondition();

                //auto title=EscapeDataManager::getInstance()->getPlayTitleName();

                if (SettingDataManager::sharedManager()->getAutoRemoveCathe() && isPlayingMinigame) {
                    log("リソースのデータを削除します");
                    EscapeDataManager::getInstance()->deleteResourceDataOfPlayTitle();
                }
                
                EscapeDataManager::getInstance()->setPlayTitleNum(0);//やらなくても良いけど、一応選択しているタイトル番号を破棄する。
                
                //ゲーム終了のタイミングだから少々長めに広告を見てもらってもいいでしょう。3秒は見ろ。
                auto duration=3.0;
                
                Common::performProcessForDebug([&duration](){duration=.2;});
                
                auto delay = DelayTime::create(duration);
                auto transition = CallFunc::create([this,isType]{
                    
                    this->checkLoadingTypePerform([isType](){
                        BannerBridge::hideBanner(BANNER_APPID_RECTANGLE);
                        
                        //タイトルコレクションシーンに戻るときにインステ表示
                        if (isType!=TitleCondition_TUTORIAL) {
                            InterstitialBridge::showIS(INTERSTITIAL_LAUNCH_APPID);
                        }
                        
                        //コレクション画面に遷移
                        float duration = .5;
                        Director::getInstance()->replaceScene(TransitionFade::create(duration, EscapeCollectionScene::createScene(),Color3B::WHITE));
                        log("コレクションシーンに遷移します、");
                    });
                });
                
                runAction(Sequence::create(delay, transition, NULL));
            });
            break;
        }
#pragma mark All Load
        case LoadingType_AllLoadTitle:{

            addNotification();
            
            auto needTitleIndexVector = EscapeDataManager::getInstance()->getNeedDownloadTitleIndexVector();
            auto freeMb = NativeBridge::storageAvailableMb();
            auto needMb = EscapeDataManager::getInstance()->getAboutAllTitleSize();
            auto dlStatus = EscapeDataManager::getInstance()->getAllTitleDataDownloadStatus();

            if (needTitleIndexVector.size() == 0 || (freeMb < needMb && dlStatus == AllTitleDataDownloadStatus_NONE)) {
                log("AllDlダウンロードするものがないので、2秒後に遷移するぜ");
                auto delay = DelayTime::create(2);
                auto callfunc = CallFunc::create([this](){
                    this->transitionOnAllTitleDL();
                });
                
                runAction(Sequence::create(delay, callfunc, NULL));
            }
            else {
                log("AllDlダウンロードするぜ");
                EscapeDataManager::getInstance()->loadingAllTitleData();
            }
        }
        default:
            break;
    }
}

void EscapeLoadingScene::addNotification()
{
    log("ローディングシーンで通知を登録します");
    auto progressListner=EventListenerCustom::create(NotificationKey_Progress, [this](EventCustom*event)
                                                       {
                                                           auto data = (Value*)event->getUserData();
                                                           auto progress = data->asFloat();
                                                           
                                                           progressLabel->setString(StringUtils::format("%d%%",(int)progress));
                                                           progressLabel->setPosition(getContentSize().width/2, indicator->getPositionY()-indicator->getBoundingBox().size.height/2-conditionLabel->getContentSize().height);
                                                       });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(progressListner,this);
    
    auto downloadedListner=EventListenerCustom::create(NotificationKey_Download, [this](EventCustom*event)
                                                   {
                                                       log("ローディングシーンで通知:ダウンロード中{%f,%f}",conditionLabel->getPositionX(),conditionLabel->getPositionY());

                                                       //リソースを全てダウンロードする場合は書き込まない。
                                                       if (this->getLoadingType() != LoadingType_AllLoadTitle) {
                                                           conditionLabel->setString(DataManager::sharedManager()->getSystemMessage("downloading"));
                                                           conditionLabel->setTextColor(Color4B(Common::getColorFromHex(BlackColor)));
                                                       }
                                                   });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(downloadedListner,this);
    
    auto unZipListner=EventListenerCustom::create(NotificationKey_Unzip, [this](EventCustom*event)
                                                   {
                                                       if (this->getLoadingType() != LoadingType_AllLoadTitle) {

                                                           conditionLabel->setString(DataManager::sharedManager()->getSystemMessage("expand_file"));
                                                           conditionLabel->setTextColor(Color4B(Common::getColorFromHex(BlackColor)));
                                                           log("ローディングシーンで通知:展開中{%f,%f}",conditionLabel->getPositionX(),conditionLabel->getPositionY());
                                                       }
                                                   });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(unZipListner,this);
    
    //全データダウンロード用
    auto dlAllDataListner=EventListenerCustom::create(NotificationKey_Progress_AllDownload, [this](EventCustom*event)
                                                  {
                                                      auto data = (Value*)event->getUserData();
                                                      auto progressSt = data->asString();
                                                      log("通知を正常に受け取りましたぜ。%s",progressSt.c_str());
                                                      auto downloadingSt = DataManager::sharedManager()->getSystemMessage("downloading");
                                                      conditionLabel->setString(StringUtils::format("%s[%s]", downloadingSt.c_str(), progressSt.c_str()));
                                                  });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(dlAllDataListner, this);
    
    //全データダウンロード用
    auto dlAllDataCompListner=EventListenerCustom::create(NotificationKey_Complete_AllDownload, [this](EventCustom*event)
                                                      {
                                                          this->transitionOnAllTitleDL();
                                                      });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(dlAllDataCompListner, this);
}

void EscapeLoadingScene::checkLoadingTypePerform(const std::function<void ()> &onLastLoadingTypeCallback)
{
    if (m_performNum == m_loadingTypeVector.size()-1) {//最後なら
        if (onLastLoadingTypeCallback) {
            onLastLoadingTypeCallback();
        }
    }
    else {//まだ続ける
        m_performNum++;
        
        startLoadingTask();
    }
}

void EscapeLoadingScene::transitionOnAllTitleDL()
{
    this->checkLoadingTypePerform([](){
        BannerBridge::hideBanner(BANNER_APPID_RECTANGLE);
        
        Director::getInstance()->replaceScene(TransitionFade::create(.5, EscapeCollectionScene::createScene(),Color3B::BLACK));
    });
}

#pragma mark - remoteConfig
void EscapeLoadingScene::fetchABTests()
{    
    FireBaseBridge::getInstance()->fetchRemoteConfig("registerNotification_1", [](bool success,Value value){
        log("分析 通知localPush登録タイプをfetch %d",value.asInt());
        AnalyticsManager::getInstance()->setAnalyticsNotificationType((AnalyticsNotificationType)(value.asInt()));
    });
    FireBaseBridge::getInstance()->fetchRemoteConfig("shopButtonType", [](bool success,Value value){
        log("分析 ショップボタンタイプをfetch %d",value.asInt());
        AnalyticsManager::getInstance()->setAnalyticsShopButtonType((AnalyticsShopButtonType)(value.asInt()));
    });
}

#pragma mark - UserID生成
void EscapeLoadingScene::createUserID()
{//ユーザIDがなければ生成する
    if (DataManager::sharedManager()->getUserID().size()==0) {
        DataManager::sharedManager()->setUserID(NativeBridge::createUUID());
        log("ユーザIDを生成します : %s",DataManager::sharedManager()->getUserID().c_str());
    }
    
    if (!UserDefault::getInstance()->getBoolForKey("SetUserID")) {
        FireBaseBridge::getInstance()->setUserID(DataManager::sharedManager()->getUserID().c_str());
        UserDefault::getInstance()->setBoolForKey("SetUserID", true);
    }
    
    DataManager::sharedManager()->postUserData();
}

#pragma mark- 解放
EscapeLoadingScene::~EscapeLoadingScene()
{
    log("エスケイプローディングシーンを解放");
}

