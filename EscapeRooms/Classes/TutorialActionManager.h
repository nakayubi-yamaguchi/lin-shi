//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__TutorialActionManager__
#define __EscapeContainer__TutorialActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

typedef enum{
    FingerDirection_Right=0,
    FingerDirection_Top,
    FingerDirection_Left
}FingerDirection;

class TutorialActionManager : public CustomActionManager
{
public:
    static TutorialActionManager* manager;
    static TutorialActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void arrowAction(std::string key,Node*parent,const onFinished& callback);
    
private:
    void createFinger(Vec2 position,Node*parent,FingerDirection direction);
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
