//
//  SideBarLayer.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/07/26.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________SideBarLayer_____
#define _____PROJECTNAMEASIDENTIFIER________SideBarLayer_____

#include "cocos2d.h"
#include "EscapeStageSprite.h"
#include "Utils/Common.h"

USING_NS_CC;

typedef std::function<void()> SideBarCloseCallback;
typedef enum{
    SideBarType_FromLeft=0,
    SideBarType_FromRight
}SideBarType;

class SideBarLayer : public cocos2d::LayerColor
{
public:
    static SideBarLayer*create(SideBarType type,const SideBarCloseCallback& callback);
    bool init(SideBarType type,const SideBarCloseCallback& callback);
    
    SideBarCloseCallback m_callback;
    CC_SYNTHESIZE(SideBarType, sideBarType, SideBarType);
    
private:
    Menu* menu;
    Sprite* menuBack;
    EscapeStageSprite*photoSprite;
    Sprite*photoBack;
    ClippingNode*clip;//dragSpriteなどはみ出すspriteもあるのでphotoSpriteはclipの上にadd
    
    float menu_width;
    void createMenu();
    void createPhotoSprite(int ID,bool animate,const ccMenuCallback& callback);
    void show();
    void close();
    
    //
    void pushHome();
    void pushCamera();
    
    //タッチを透過させないためのlayerを取得
    LayerColor* createDisableLayer();
    
    /**表示完了*/
    CC_SYNTHESIZE(bool, isAnimated, IsAnimated);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
