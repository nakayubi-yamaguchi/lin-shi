//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "PigHatActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "WarpManager.h"

using namespace cocos2d;

float picture_posY=1.25;
float hawk_attack_posX=.2;

PigHatActionManager* PigHatActionManager::manager =NULL;

PigHatActionManager* PigHatActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new PigHatActionManager();
    }
    return manager;
}

#pragma mark - Back
void PigHatActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="musicbox"&&DataManager::sharedManager()->getEnableFlagWithFlagID(30))
    {
        Vector<FiniteTimeAction*>actions;
        Vector<FiniteTimeAction*>actions_doll;
        Vector<FiniteTimeAction*>actions_particle;
        
        actions.pushBack(DelayTime::create(1));
        if(backID==54){
            actions.pushBack(createSoundAction("music_box.mp3"));
        }

        for(int i=0;i<3;i++){
            auto doll_after=createSpriteToCenter(StringUtils::format("sup_%d_pig_%d.png",backID,i+1), true, parent);
            parent->addChild(doll_after);
            auto doll_anim=createChangeAction(1.5, 1, parent->getChildByName<Sprite*>(StringUtils::format("sup_%d_pig_%d.png",backID,i)),doll_after );
            
            actions_doll.pushBack(doll_anim);
            actions_doll.pushBack(DelayTime::create(1));
        }
        actions_doll.pushBack(createChangeAction(.5, .3, parent->getChildByName<Sprite*>(StringUtils::format("sup_%d_pig_3.png",backID)), parent->getChildByName<Sprite*>(StringUtils::format("sup_%d_pig_0.png",backID))));
        
        auto particle=[this,parent](int index){
            auto call=CallFunc::create([parent,index]{
                std::vector<Color4F>starts={Color4F::RED,Color4F::BLUE,Color4F::BLUE,Color4F::RED,Color4F::RED};
                std::vector<Color4F>starts_1={Color4F::RED,Color4F::BLUE,Color4F::RED,Color4F::RED,Color4F::BLUE};
                std::vector<std::vector<Color4F>>vecs={starts,starts_1};
                for (int i=0; i<2; i++) {
                    auto pos=Vec2(parent->getContentSize().width*(.25+.45*i), parent->getContentSize().height*.55);
                    auto size=parent->getContentSize().width*.15;
                    
                    auto particle=ParticleSystemQuad::create("particle_music.plist");
                    particle->setAutoRemoveOnFinish(true);
                    particle->setPosition(pos);
                    particle->setLife(2);
                    particle->resetSystem();
                    particle->setStartSize(size);
                    particle->setEndSize(size);
                    particle->setGravity(Vec2(size*1*pow(-1, i), -size*.3));
                    particle->setEndSpin(30*pow(-1, i));
                    particle->setEndSpinVar(10);

                    auto colors=vecs.at(i);
                    auto color=colors.at(index);
                    particle->setStartColor(color);
                    particle->setEndColor(Color4F(color.r, color.g, color.b, 0));
                    
                    particle->setAngle(135-90*i);
                    particle->setSpeed(size*1.5);
                    parent->addChild(particle);
                }
            });
            
            return call;
        };
        
        for (int i=0; i<5; i++) {
            actions_particle.pushBack(particle(i));
            actions_particle.pushBack(DelayTime::create(1.75));
        }
        
        //marge
        if (backID==54) {
            actions.pushBack(Spawn::create(Sequence::create(actions_doll),
                                           Sequence::create(actions_particle), NULL));
        }
        else{
            actions.pushBack(Sequence::create(actions_doll));
        }
        actions.pushBack(DelayTime::create(2));
        
        parent->runAction(Repeat::create(Sequence::create(actions),UINT_MAX));
    }
    else if(key=="chick")
    {
        Vector<FiniteTimeAction*>actions;
        auto duration=5.5;//全体のduration
        auto center_posX=.6;
        std::vector<float> pos_x_0s={.525,.75,.45};//1回目の移動position
        std::vector<float> pos_x_1s={.675,.6,.6};//2回目の移動position

        for (int i=0; i<3; i++) {
            auto chick=createSpriteToCenter("sup_57_chick.png", false, parent);
            chick->setPositionX(parent->getContentSize().width*center_posX);
            chick->setPositionY(parent->getContentSize().height*(.76+.075*i));
            parent->addChild(chick);
            auto original_scale=chick->getScale();

            if (i==1) {
                chick->setScaleX(-original_scale);
            }
            
            auto distance_all=(abs(pos_x_0s.at(i)-center_posX)+abs(pos_x_1s.at(i)-center_posX))*2;
            
            auto duration_0=duration*abs(pos_x_0s.at(i)-center_posX)/distance_all;
            auto move_0=EaseInOut::create(MoveTo::create(duration_0, Vec2(parent->getContentSize().width*pos_x_0s.at(i), chick->getPosition().y)), 1.5);
            auto turn_0=Sequence::create(DelayTime::create(duration_0/2),
                                       ScaleBy::create(duration_0/2, -1, 1), NULL);
            auto spawn_0=Spawn::create(move_0,turn_0, NULL);
            
            auto duration_1=duration*abs(pos_x_0s.at(i)-pos_x_1s.at(i))/distance_all;
            auto move_1=EaseInOut::create(MoveTo::create(duration_1, Vec2(parent->getContentSize().width*pos_x_1s.at(i), chick->getPosition().y)), 1.5);
            auto turn_1=Sequence::create(DelayTime::create(duration_1/2),
                                         ScaleBy::create(duration_1/2, -1, 1), NULL);
            auto spawn_1=Spawn::create(move_1,turn_1, NULL);

            //中心に戻る
            auto duration_2=duration*abs(pos_x_1s.at(i)-center_posX)/distance_all;
            auto move_2=EaseInOut::create(MoveTo::create(duration_2, Vec2(parent->getContentSize().width*center_posX, chick->getPositionY())), 1.5);

            auto delay=DelayTime::create(1);
            actions.pushBack(TargetedAction::create(chick, Sequence::create(spawn_0,spawn_1,move_2,delay, NULL)));
        }
        
        parent->runAction(Repeat::create(Spawn::create(actions), UINT_MAX));
    }
    else if(key=="nose"){
        auto hawk=parent->getChildByName("sup_41_hawk.png");
        
        Vector<FiniteTimeAction*>actions;
        std::vector<int>indexs={0,1,1,0,1,0};
        for (int i=0; i<indexs.size(); i++) {
            auto bounce=createBounceAction(hawk, .5);
            actions.pushBack(Spawn::create(bounce,Sequence::create(DelayTime::create(.5),
                                                                   createSoundAction("nose.mp3"),
                                                                   createNoseAction(parent, indexs.at(i)), NULL), NULL));
            actions.pushBack(DelayTime::create(1.5));
        }
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),Sequence::create(actions), NULL), UINT_MAX));
    }
    else if(key=="mole")
    {
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(27)) {
            Vector<FiniteTimeAction*>actions;
            auto underPosY=parent->getContentSize().height*.42;
            for (int i=0; i<3;i++) {
                auto pig=createSpriteToCenter(StringUtils::format("sup_%d_pig_%d.png",backID,i), true, parent);
                pig->setPositionY(underPosY);
                parent->addChild(pig);
                
                actions.pushBack(TargetedAction::create(pig, Sequence::create(FadeIn::create(.1),MoveTo::create(1, Vec2(pig->getPositionX(), parent->getContentSize().height*.49)),
                                                                              createSoundAction("nyu.mp3"),DelayTime::create(1),MoveTo::create(1, Vec2(pig->getPositionX(), underPosY)), NULL)));
            }
            
            auto rep=Repeat::create(Sequence::create(actions.at(1),actions.at(2),actions.at(0),actions.at(2)->clone(),actions.at(1)->clone(),DelayTime::create(1), NULL), UINT_MAX);
            
            parent->runAction(Sequence::create(createSoundAction("gatagata.mp3"),DelayTime::create(1),rep, NULL));
        }
        else if (DataManager::sharedManager()->isPlayingMinigame()){
            auto mistake=parent->getChildByName("sup_17_mistake.png");
            auto moveup=TargetedAction::create(mistake, Spawn::create(FadeIn::create(.1),
                                                                      MoveBy::create(1,Vec2(0, parent->getContentSize().height*.1)),
                                                                      createSoundAction("nyu.mp3"),NULL));
            auto call=CallFunc::create([this]{
                DataManager::sharedManager()->setTemporaryFlag("mistake_17", 1);
            });
            
            parent->runAction(Sequence::create(createSoundAction("gatagata.mp3"),DelayTime::create(2),moveup,call, NULL));
        }
    }
    else if(key=="hourglass"){
        //指定indexのフレームで画像をchangeする
        std::vector<std::vector<int>>indexes={
            {0,1,2},
            {0,1,3},
            {0,4,8},
            {0,3,6},
            {0,3,7},
            {0,2,4},
            {0,2,5}
        };
        
        std::map<std::string, int>map;
        for (int i=0; i<7; i++) {
            //init
            map[StringUtils::format("%d",i)]=0;
        }
        
        Vector<FiniteTimeAction*>sequenceActions;
        auto change_duration=1.0;
        for (int i=0; i<9; i++) {
            Vector<FiniteTimeAction*>spawnActions;
            for (int j=0; j<7; j++) {
                auto vec=indexes.at(j);
                auto itr=std::find(vec.begin(), vec.end(), i);
                if (std::distance(vec.begin(), itr)!=vec.size()) {
                    //change
                    auto index_now=map[StringUtils::format("%d",j)];
                    
                    if(index_now==0){
                        auto sand=createSpriteToCenter(StringUtils::format("sup_32_%d_1.png",j), true, parent);
                        parent->addChild(sand);
                        spawnActions.pushBack(TargetedAction::create(sand, FadeIn::create(change_duration)));
                    }
                    else{
                        auto sand=createSpriteToCenter(StringUtils::format("sup_32_%d_%d.png",j,index_now+1), true, parent);
                        parent->addChild(sand);
                        spawnActions.pushBack(createChangeAction(change_duration*1.5, change_duration, parent->getChildByName<Sprite*>(StringUtils::format("sup_32_%d_%d.png",j,index_now)), sand));
                    }
                    
                    map[StringUtils::format("%d",j)]=index_now+1;
                }
            }
            
            //sequenceに追加
            if (spawnActions.size()>0) {
                sequenceActions.pushBack(createSoundAction("sa.mp3"));
                sequenceActions.pushBack(Spawn::create(spawnActions));
            }
            sequenceActions.pushBack(DelayTime::create(1));
        }
        parent->runAction(Sequence::create(DelayTime::create(1.5),Sequence::create(sequenceActions), NULL));
    }
    else if(key=="picture"){
        auto picture=parent->getChildByName(StringUtils::format("sup_%d_picture.png",backID));
        picture->setLocalZOrder(1);
        
        auto isSet_0=DataManager::sharedManager()->getEnableFlagWithFlagID(24);
        auto isSet_1=DataManager::sharedManager()->getEnableFlagWithFlagID(25);
        auto isMoved=DataManager::sharedManager()->getEnableFlagWithFlagID(26);

        if (isSet_0) {
            auto p_0=createSpriteToCenter(StringUtils::format("sup_%d_poster_0.png",backID), false, parent);
            picture->addChild(p_0);
        }
        if (isSet_1) {
            auto p_1=createSpriteToCenter(StringUtils::format("sup_%d_poster_1.png",backID), false, parent);
            picture->addChild(p_1);
        }
        
        if (isMoved) {
            //変位を調整
            auto distance=picture_posY;
            if (backID==5) {
                distance=.75;
            }
            else if(backID==1){
                distance=.58;
            }
            picture->setPositionY(parent->getContentSize().height*distance);
        }
    }
    else if(key=="back_4"){
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(27)) {
            auto hawk=getHawkAttackSprite(parent);
            hawk->setOpacity(255);
            parent->addChild(hawk);
            hawk->setPositionX(parent->getContentSize().width*hawk_attack_posX);
        }
    }
#pragma mark まちがい
    else if(key=="diane"&&DataManager::sharedManager()->isPlayingMinigame()){
        auto mistake=parent->getChildByName("sup_50_mistake.png");
        mistake->setPositionX(parent->getContentSize().width*1.5);
        auto delay=DelayTime::create(1);
        auto fadein=TargetedAction::create(mistake, FadeIn::create(.5));
        auto movein=TargetedAction::create(mistake, MoveTo::create(1, parent->getContentSize()/2));
        
        auto call=CallFunc::create([]{
            DataManager::sharedManager()->setTemporaryFlag("mistake_50", 1);
        });
        
        parent->runAction(Sequence::create(delay,fadein,movein,call ,NULL));
    }
}

#pragma mark - Touch
void PigHatActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="pen")
    {//羽根ペン
        Common::playSE("p.mp3");
        
        auto mark=createSpriteToCenter("sup_42_mark.png", true, parent);
        parent->addChild(mark);
        
        auto item=createSpriteToCenter("sup_42_pen.png", true, parent);
        parent->addChild(item);
        
        //fadein
        auto fadein=FadeIn::create(1);
        auto sound=CallFunc::create([this]{
            Common::playSE("pen.mp3");
        });
        //move
        auto distance=parent->getContentSize().width*.05;
        auto duration=.2;
        Vector<FiniteTimeAction*>moves;
        for (int i=1; i<6; i++) {
            auto move=MoveBy::create(duration+i*.025, Vec2(distance*i*pow(-1, i)+distance*.8*(i/2), distance*.75*i*pow(-1, i)-distance*.3*(i/2)));
            moves.pushBack(move);
        }
        
        auto seq=TargetedAction::create(item, Sequence::create(fadein,sound,Sequence::create(moves), NULL));
        auto change=Spawn::create(TargetedAction::create(item, FadeOut::create(.7)),
                                  TargetedAction::create(mark, FadeIn::create(.5)), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq,change,DelayTime::create(.5),call,NULL));
    }
    else if(key=="axe"){
        Common::playSE("p.mp3");
        //斧
        auto axe=AnchorSprite::create(Vec2(.09, .6), parent->getContentSize(), true, "sup_10_axe.png");
        parent->addChild(axe);
        
        auto lid=parent->getChildByName<Sprite*>("sup_10_lid.png");
        auto box=createSpriteToCenter("sup_10_box.png", true, parent);
        parent->addChild(box);
        
        Vector<FiniteTimeAction*>actions;
        auto fadein=FadeIn::create(1);
        auto moveup=Spawn::create(MoveBy::create(1.75, Vec2(0, parent->getContentSize().height*.2)),
                                  Sequence::create(EaseOut::create(RotateBy::create(.5, 15), 1.5),
                                                   EaseOut::create(RotateBy::create(1.25, -30), 1.5), NULL), NULL);
        auto movedown=Spawn::create(EaseIn::create(MoveBy::create(.4, Vec2(0, -parent->getContentSize().height*.2)), 1.5),
                                    EaseIn::create(RotateBy::create(.4, 50), 1.5), NULL);
        
        auto smoke=CallFunc::create([parent]{
            auto size=parent->getContentSize().width*.1;
            
            auto particle=ParticleSystemQuad::create("smoke.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setTotalParticles(150);
            particle->setPosition(parent->getContentSize().width*(.55),parent->getContentSize().height*.535);
            particle->setPosVar(Vec2(parent->getContentSize().width*.2,parent->getContentSize().height*.15));
            particle->setLife(2);
            particle->setDuration(.5);
            particle->resetSystem();
            particle->setStartSize(size);
            particle->setStartSizeVar(size*.5);
            particle->setEndSize(size*2);
            particle->setStartColor(Color4F(1, 1, 1, .6));
            particle->setSpeed(size*.5);
            particle->setGravity(Vec2(0, size*1));
            particle->setEndSpin(30);
            particle->setEndSpinVar(30);
            particle->setAngle(90);
            particle->setAngleVar(270);
            parent->addChild(particle);
        });
        
        auto remove_lid=createChangeAction(1, .5, lid, box);
        auto fadeouts=Spawn::create(FadeOut::create(1),
                                    remove_lid,
                                    createSoundAction("firewood.mp3"), NULL);
        
        auto seq_axe=TargetedAction::create(axe, Sequence::create(fadein,moveup,DelayTime::create(.5),movedown,smoke,fadeouts, NULL));
        actions.pushBack(seq_axe);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="handle"){
        Common::playSE("p.mp3");
        
        //オルゴールのハンドル
        auto handle=createSpriteToCenter("sup_54_handle_0.png", true, parent);
        parent->addChild(handle);
        
        Vector<FiniteTimeAction*>actions;
        auto fadein=TargetedAction::create(handle, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        
        for(int i=0;i<3;i++){
            auto handle_after=createSpriteToCenter(StringUtils::format("sup_54_handle_%d.png",i+1), true, parent);
            parent->addChild(handle_after);
            auto handle_anim=createChangeAction(.5, .3, parent->getChildByName<Sprite*>(StringUtils::format("sup_54_handle_%d.png",i)),handle_after );
            
            if (i==0) {
                actions.pushBack(createSoundAction("gigigi.mp3"));
            }
            actions.pushBack(handle_anim);
            
            if (i==2) {
                actions.pushBack(createChangeAction(.5, .3, parent->getChildByName<Sprite*>("sup_54_handle_3.png"), handle));
            }
        }
        
        auto repeat=Repeat::create(Sequence::create(actions), 2);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,repeat,delay->clone(),call,NULL));
    }
    else if(key.compare(0,5,"arrow")==0){
        //棚上部入力
        Common::playSE("kacha.mp3");
        
        auto index=atoi(key.substr(6,1).c_str());
        auto arrow=parent->getChildByName<Sprite*>(StringUtils::format("sup_28_arrow_%d.png",index));
        
        auto arrow_seq=TargetedAction::create(arrow, Sequence::create(FadeOut::create(0),DelayTime::create(.15),FadeIn::create(0), NULL));
        
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        
        parent->runAction(Sequence::create(arrow_seq,call, NULL));
    }
    else if(key=="beer")
    {//バーニャエールを注ぐ
        Common::playSE("p.mp3");
        auto angle=15;
        auto beer=AnchorSprite::create(Vec2(.6, .68), parent->getContentSize(), true, "sup_15_beer.png");
        beer->setRotation(angle);
        parent->addChild(beer);
        
        //溜まったビール
        auto anim=createSpriteToCenter("anim_15_beer.png", true, parent);
        parent->addChild(anim);
        
        //あわ
        auto bubble=createSpriteToCenter("sup_15_bubble.png", true, parent);
        parent->addChild(bubble);
        
        auto delay=DelayTime::create(.5);
        auto beer_fadein=TargetedAction::create(beer, FadeIn::create(1));
        auto delay_1=DelayTime::create(1);
        auto rotates_beer=TargetedAction::create(beer, Sequence::create(createSoundAction("beer.mp3")
                                                                        ,EaseInOut::create(RotateBy::create(1, -angle), 1.5),
                                                                        swingAction(10, 1, beer, 1.5, true, 1),
                                                                        Spawn::create(FadeOut::create(1),
                                                                                      MoveBy::create(1, Vec2(0, parent->getContentSize().height*.2)), NULL), NULL));
        
        auto bubble_fadein=Sequence::create(DelayTime::create(1),
                                            TargetedAction::create(anim, FadeIn::create(1)),
                                            DelayTime::create(1),
                                            TargetedAction::create(bubble, FadeIn::create(1)), NULL);
        auto spawn=Spawn::create(rotates_beer,bubble_fadein, NULL);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(delay,beer_fadein,delay_1,spawn,call, NULL));
    }
    else if(key.compare(0,6,"poster")==0){
        auto index=atoi(key.substr(7,1).c_str());
        
        auto correct=(DataManager::sharedManager()->getEnableFlagWithFlagID(25)&&index==0)||
        (DataManager::sharedManager()->getEnableFlagWithFlagID(24)&&index==1);
        
        Vector<FiniteTimeAction*>actions;
        auto picture=parent->getChildByName("sup_16_picture.png");
        auto poster=createSpriteToCenter(StringUtils::format("sup_16_poster_%d.png",index), true, parent);
        picture->addChild(poster);
        
        auto fadein_poster=Spawn::create(TargetedAction::create(poster, FadeIn::create(.7)),
                                         createSoundAction("kacha.mp3"), NULL);
        actions.pushBack(fadein_poster);
        
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        
        auto shake=TargetedAction::create(picture, Sequence::create(createSoundAction("gatagata.mp3"),
                                                                    swingAction(3, .1, picture, 1.2, true), NULL));
        if (correct) {
            actions.pushBack(shake);
        }
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key.compare(0,7,"picture")==0){
        //絵画かたかた
        Common::playSE("kacha.mp3");
        
        auto index=atoi(key.substr(8,1).c_str());
        AnswerManager::getInstance()->appendPasscode(StringUtils::format("%d",index));
        auto correct=(AnswerManager::getInstance()->getPasscode()=="011010");
        
        auto picture=parent->getChildByName<Sprite*>(StringUtils::format("sup_16_picture.png"));
        picture->setLocalZOrder(1);
        picture->stopAllActions();
        
        auto angle=-10*pow(-1, index);
        auto rotate=TargetedAction::create(picture, Sequence::create(EaseInOut::create(RotateTo::create(.1, angle), 1.5),
                                                                     EaseInOut::create(RotateTo::create(.1, 0), 1.5), NULL));
        
        auto call=CallFunc::create([callback,correct,picture,parent,this](){
            log("正解判定%d",correct);
            if (!correct) {
                callback(correct);
            }
            else{
                //正解処理
                Common::playSE("pinpon.mp3");
                auto hint=createSpriteToCenter("sup_16_hint.png", false, parent);
                parent->addChild(hint);
                
                auto delay=DelayTime::create(.5);
                auto move=Spawn::create(EaseOut::create(MoveTo::create(1.5, Vec2(picture->getPositionX(), parent->getContentSize().height*picture_posY)), 2),
                                        createSoundAction("tanaopen"), NULL);
                auto call=CallFunc::create([callback]{
                    Common::playSE("pinpon.mp3");
                    callback(true);
                });
                picture->runAction(Sequence::create(delay,move,delay->clone(),call, NULL));
            }
        });
        
        parent->runAction(Sequence::create(rotate,call, NULL));
    }
    else if(key=="cloche"){
        auto back_51=createSpriteToCenter("back_51.png", true, parent);
        back_51->setCascadeOpacityEnabled(true);
        parent->addChild(back_51);
        SupplementsManager::getInstance()->addSupplementToMain(back_51, 51);
        
        //クローシュ
        auto cloche=back_51->getChildByName<Sprite*>("sup_51_cloche.png");
        auto apple=createSpriteToCenter("sup_51_apple.png", true, parent);
        parent->addChild(apple);
        
        auto fadein_back=TargetedAction::create(back_51, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto fadeout_cloche=Spawn::create(createChangeAction(1, .5, cloche, apple),
                                          createSoundAction("kacha.mp3"), NULL);
        auto seq_cloche=Sequence::create(fadein_back,delay,fadeout_cloche, NULL);
        //ぶた物置
        auto back_17=createSpriteToCenter("back_17.png", true, parent);
        back_17->setCascadeOpacityEnabled(true);
        parent->addChild(back_17);
        SupplementsManager::getInstance()->addSupplementToMain(back_17, 17);
        
        auto pig_in_barrel=createSpriteToCenter("sup_17_pig_2.png", true, back_17);
        pig_in_barrel->setPositionY(parent->getContentSize().height*.425);
        back_17->addChild(pig_in_barrel);
        
        auto pig_appear=createSpriteToCenter("sup_17_pig_appear.png", true, back_17);
        back_17->addChild(pig_appear);
        
        auto pig_jump=createSpriteToCenter("sup_17_pig_jump.png", true, back_17);
        back_17->addChild(pig_jump);
        auto pig_shirai_0=AnchorSprite::create(Vec2(.713, .625),back_17->getContentSize(), true, "sup_17_pig_shirai_0.png");;
        back_17->addChild(pig_shirai_0);
        auto pig_shirai_1=AnchorSprite::create(Vec2(.713, .625),back_17->getContentSize(), true, "sup_17_pig_shirai_1.png");
        pig_shirai_1->setLocalZOrder(1);
        back_17->addChild(pig_shirai_1);
        auto pig_land=createSpriteToCenter("sup_17_pig_land.png", true, back_17);
        back_17->addChild(pig_land);
        auto pig_run=createSpriteToCenter("sup_17_pig_run.png", true, back_17);
        back_17->addChild(pig_run);
        
        auto fadein_back_1=TargetedAction::create(back_17, FadeIn::create(1));
        auto moveup_pig=TargetedAction::create(pig_in_barrel, Sequence::create(FadeIn::create(1),
                                                                               createSoundAction("nyu.mp3"),
                                                                               MoveTo::create(1, Vec2(pig_in_barrel->getPosition().x, parent->getContentSize().height*.49)), NULL));
        auto delay_short=DelayTime::create(1);
        auto appear_pig=Spawn::create(createChangeAction(1, .7, pig_in_barrel, pig_appear),
                                      createSoundAction("pop.mp3"), NULL);
        auto jump_pig=Spawn::create(createChangeAction(1, .7, pig_appear, pig_jump),
                                      createSoundAction("jump.mp3"), NULL);
        
        auto jump_shirai=JumpBy::create(3.5, Vec2(-parent->getContentSize().width*.4,-parent->getContentSize().height*.35), parent->getContentSize().height*.3, 1);
        auto spawn_shirai=Spawn::create(createChangeAction(.5, .3, pig_jump, pig_shirai_0),
                                        TargetedAction::create(pig_shirai_0, jump_shirai),
                                        TargetedAction::create(pig_shirai_1, jump_shirai->clone()),
                                        Sequence::create(DelayTime::create(1),
                                                         Spawn::create(createChangeAction(.5, .2, pig_shirai_0, pig_shirai_1),
                                                                       createSoundAction("rolling.mp3"),
                                                                       TargetedAction::create(pig_shirai_1, RotateBy::create(1.5, 360*3)), NULL), NULL),
                                        NULL);
        
        auto land_pig=Spawn::create(createChangeAction(1, .7, pig_shirai_1, pig_land),
                                    createSoundAction("landing.mp3"), NULL);
        auto run_pig=Sequence::create(Spawn::create(createChangeAction(1, .7, pig_land, pig_run),
                                                     NULL),
                                      createSoundAction("run.mp3"),
                                      TargetedAction::create(pig_run, Spawn::create(ScaleBy::create(1, 1.2),
                                                                                    FadeOut::create(1), NULL)), NULL);
        
        auto seq_warehouse=Sequence::create(fadein_back_1,moveup_pig,delay_short,appear_pig,delay_short->clone(),jump_pig,spawn_shirai,land_pig,delay_short->clone(),run_pig, NULL);
        
        //ぶた 窓引き
        auto back_4=createSpriteToCenter("back_4.png", true, parent);
        back_4->setCascadeOpacityEnabled(true);
        parent->addChild(back_4);
        SupplementsManager::getInstance()->addSupplementToMain(back_4, 4);
        
        back_4->getChildByName("sup_4_cloche.png")->removeFromParent();
        
        auto pig_eat=AnchorSprite::create(Vec2(.5, .5), parent->getContentSize(),true, "sup_4_pig_eat.png");
        back_4->addChild(pig_eat);
        
        auto fadein_back_4=TargetedAction::create(back_4, FadeIn::create(1));
        auto eat_pig=Spawn::create(TargetedAction::create(pig_eat, FadeIn::create(1)),
                                   createSoundAction("pig.mp3"), NULL);
        
        auto remove_back_17=TargetedAction::create(back_17, RemoveSelf::create());
        
        auto seq_back_4=Sequence::create(fadein_back_4,delay_short->clone(),eat_pig,remove_back_17, NULL);
        //ホーク
        auto back_41=createSpriteToCenter("back_41.png", true, parent);
        back_41->setCascadeOpacityEnabled(true);
        parent->addChild(back_41);
        SupplementsManager::getInstance()->addAnchorSpritesToMain(back_41, DataManager::sharedManager()->getBackData(41));
        
        auto fadein_back_41=TargetedAction::create(back_41, FadeIn::create(1));
        
        auto hawk=back_41->getChildByName<Sprite*>("sup_41_hawk.png");

        //表情演出
        auto attention=createSpriteToCenter("sup_41_attention.png", true, parent);
        back_41->addChild(attention);
        auto angry=createSpriteToCenter("sup_41_angry.png", true, parent);
        hawk->addChild(angry);
        
        auto attention_action=TargetedAction::create(attention, Spawn::create(createSoundAction("surprise.mp3"),
                                                                              jumpAction(parent->getContentSize().height*.05, .3, attention, 1.5),
                                                                              FadeIn::create(.1),
                                                                              NULL));
        auto fadein_angry=createChangeAction(.7, .5, attention, angry);
        auto scale_hawk=createBounceAction(hawk, .25);
        auto bless=Spawn::create(createNoseAction(parent, 0),
                                 createNoseAction(parent, 1),
                                 createSoundAction("nose.mp3"),
                                 DelayTime::create(1), NULL);
        auto seq_angry=Sequence::create(attention_action,DelayTime::create(.5),fadein_angry,scale_hawk,bless, NULL);

        //一旦storyはさむよ
        auto story_hawk=CallFunc::create([callback,this,parent,back_41,back_4,pig_eat,hawk]{
            auto story=StoryLayer::create("hawk_attack_0", [callback,this,parent,back_41,back_4,pig_eat,hawk]{
                
                auto story_hawk_1=CallFunc::create([callback,this,parent,back_41,back_4,pig_eat,hawk]{
                    auto story=StoryLayer::create("hawk_attack_1", [this,callback,parent,back_41,back_4,pig_eat,hawk]{
                        auto fadeout_back=TargetedAction::create(back_41, FadeOut::create(1));
                        auto delay=DelayTime::create(1);
                        
                        auto seq_hawk_up=Sequence::create(delay,fadeout_back, NULL);
                        
                        //突進
                        auto pig_fly=AnchorSprite::create(Vec2(.42, .2), parent->getContentSize(),true, "sup_4_pig_fly.png");
                        back_4->addChild(pig_fly);
                        auto pig_knockout=AnchorSprite::create(Vec2(.5, .5), parent->getContentSize(),true, "sup_4_pig_knockout.png");
                        back_4->addChild(pig_knockout);
                        auto hawk_attack=getHawkAttackSprite(parent);
                        hawk_attack->setPositionX(-parent->getContentSize().width/2);
                        back_4->addChild(hawk_attack);
                        
                        auto smoke=CallFunc::create([parent]{
                            auto size=parent->getContentSize().width*.1;
                            
                            auto particle=ParticleSystemQuad::create("smoke.plist");
                            particle->setAutoRemoveOnFinish(true);
                            particle->setTotalParticles(150);
                            particle->setPosition(parent->getContentSize().width*(.45),parent->getContentSize().height*.2);
                            particle->setPosVar(Vec2(parent->getContentSize().width*.1,parent->getContentSize().height*.1));
                            particle->setLife(2);
                            particle->setDuration(1);
                            particle->resetSystem();
                            particle->setStartSize(size);
                            particle->setStartSizeVar(size*.5);
                            particle->setEndSize(size*2);
                            particle->setStartColor(Color4F(1, 1, 1, .6));
                            particle->setSpeed(size*.5);
                            particle->setGravity(Vec2(0, size*1));
                            particle->setEndSpin(30);
                            particle->setEndSpinVar(30);
                            particle->setAngle(90);
                            particle->setAngleVar(270);
                            parent->addChild(particle);
                        });
                        
                        //
                        auto duration_attack=.7;
                        auto duration_attack_after=.3;
                        auto angle_attack=30;
                        auto spawn_attack_0=TargetedAction::create(hawk_attack, Spawn::create(EaseIn::create(JumpTo::create(duration_attack, Vec2(parent->getContentSize().width*.15, hawk_attack->getPosition().y+parent->getContentSize().height*.15), parent->getContentSize().height*.1, 2), 1.5),
                                                                                              RotateBy::create(duration_attack, angle_attack), NULL));
                        auto spawn_attack_1=Spawn::create(TargetedAction::create(hawk_attack, Spawn::create(RotateBy::create(duration_attack_after, -angle_attack),
                                                                                                            JumpTo::create(duration_attack_after, Vec2(parent->getContentSize().width*hawk_attack_posX, 0), parent->getContentSize().height*.2, 1), NULL)),
                                                          //豚が吹っ飛ぶ
                                                          Sequence::create(createChangeAction(.1, .1, pig_eat, pig_fly),
                                                                           createSoundAction("pig.mp3"),
                                                                           TargetedAction::create(pig_fly, Spawn::create(JumpTo::create(1, Vec2(parent->getContentSize().width*.8, pig_fly->getPositionY()), parent->getContentSize().height*.3, 1),
                                                                                                                         RotateBy::create(1, 90),
                                                                                                                         ScaleBy::create(1, .7), NULL)),
                                                                           TargetedAction::create(pig_fly, Spawn::create(jumpAction(parent->getContentSize().height*.025, .1, pig_fly, 1.5),
                                                                                                                         createSoundAction("knockout.mp3"),
                                                                                                                         FadeOut::create(.2),
                                                                                                                         NULL)),
                                                                           TargetedAction::create(pig_knockout, FadeIn::create(.1)),
                                                                           DelayTime::create(2),
                                                                           NULL),
                                                          NULL);
                        auto seq_hawk_attack=Sequence::create(TargetedAction::create(hawk_attack, FadeIn::create(1)),
                                                              spawn_attack_0,
                                                              createSoundAction("hit.mp3"),
                                                              smoke,
                                                              spawn_attack_1,
                                                              NULL);
                        //ストーリーをもういっちょ
                        auto story_after_attack=CallFunc::create([callback,parent]{
                            auto story=StoryLayer::create("hawk_attack_after", [callback,parent]{
                                auto delay=DelayTime::create(.3);
                                auto call=CallFunc::create([callback]{
                                    Common::playSE("pinpon.mp3");
                                    callback(true);
                                });
                                parent->runAction(Sequence::create(delay,call, NULL));
                                
                            });
                            Director::getInstance()->getRunningScene()->addChild(story);
                        });
                        
                        parent->runAction(Sequence::create(seq_hawk_up,seq_hawk_attack,story_after_attack, NULL));

                    });
                    Director::getInstance()->getRunningScene()->addChild(story);
                });
                
                //とんとこストーリーと同時に走らせる
                auto duration_run=1.4;
                auto run_hawk=TargetedAction::create(hawk, Sequence::create(Spawn::create(ScaleBy::create(duration_run, 1.5),
                                                                                          createSoundAction("horce_run.mp3"),
                                                                                          Sequence::create(DelayTime::create(duration_run/2),
                                                                                                           FadeOut::create(duration_run/2), NULL),
                                                                                          MoveBy::create(duration_run, Vec2(0, -parent->getContentSize().height*1)),
                                                                                          jumpAction(parent->getContentSize().height*.2, duration_run/8, hawk, 1.5,4), NULL), NULL));
                
                
                
                parent->runAction(Spawn::create(run_hawk,story_hawk_1, NULL));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        
        
        parent->runAction(Sequence::create(seq_cloche,delay->clone(),seq_warehouse,delay->clone(),seq_back_4,delay->clone(),fadein_back_41,delay_short->clone(),seq_angry,story_hawk, NULL));
    }
    else if(key.compare(0,4,"card")==0){
        auto index=atoi(key.substr(5,1).c_str());
        auto canOpen=(index==0&&DataManager::sharedManager()->getEnableFlagWithFlagID(34)&&DataManager::sharedManager()->getEnableFlagWithFlagID(35))||
        (index==1&&DataManager::sharedManager()->getEnableFlagWithFlagID(33)&&DataManager::sharedManager()->getEnableFlagWithFlagID(35))||
        (index==2&&DataManager::sharedManager()->getEnableFlagWithFlagID(33)&&DataManager::sharedManager()->getEnableFlagWithFlagID(34));
        
        if (canOpen) {
            Common::playSE("gacha.mp3");
            Warp warp;
            warp.warpID=13;
            warp.replaceType=ReplaceTypeFadeIn;
            WarpManager::getInstance()->warps.push_back(warp);
        }
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(call, NULL));
    }
    else if(key=="clear"){
        auto plate=parent->getChildByName("sup_13_signboard.png");
        
        auto fadeout_board=TargetedAction::create(plate, Sequence::create(FadeOut::create(1),createSoundAction("pinpon.mp3"), NULL));
        auto delay=DelayTime::create(1.5);
        auto addStory=CallFunc::create([parent]{

            auto story=StoryLayer::create("ending", [parent]{
                
                auto call=CallFunc::create([parent](){
                    Director::getInstance()->replaceScene(TransitionFade::create(8, ClearScene::createScene(), Color3B::WHITE));
                });
                
                parent->runAction(Sequence::create(call, NULL));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        parent->runAction(Sequence::create(fadeout_board,delay,addStory, NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark ドラッグ
void PigHatActionManager::dragAction(std::string key,cocos2d::Node *parent, Vec2 pos)
{
    
}

#pragma mark - Item
void PigHatActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent->itemImageにaddChild
    if (key=="opener") {
        log("opener");
        Common::playSE("p.mp3");
        
        auto lid=parent->itemImage->getChildByName("beer_angle_lid.png");
        auto opener=createSpriteToCenter("beer_anim.png", true, parent->itemImage);
        parent->itemImage->addChild(opener);
        
        auto fadein_opener=TargetedAction::create(opener, FadeIn::create(1));
        auto delay=DelayTime::create(1.5);
        auto moves=Spawn::create(MoveBy::create(1, Vec2(0, parent->itemImage->getContentSize().height*.2)),
                                 FadeOut::create(1), NULL);
        auto spawn=Spawn::create(TargetedAction::create(lid, moves),
                                 TargetedAction::create(opener, moves->clone()),
                                 createSoundAction("po.mp3"), NULL);
        auto delay_1=DelayTime::create(.7);
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_opener,delay,spawn,delay_1,call, NULL));
    }
}

#pragma mark - PigHat特有
CallFunc* PigHatActionManager::createNoseAction(cocos2d::Node *parent,int index)
{
    auto call=CallFunc::create([parent,index]{
        auto size=parent->getContentSize().width*.025;
        
        auto particle=ParticleSystemQuad::create("smoke.plist");
        particle->setAutoRemoveOnFinish(true);
        particle->setTotalParticles(40);
        particle->setPosition(parent->getContentSize().width*(.45+.05*index),parent->getContentSize().height*.425);
        particle->setLife(1.2);
        particle->setDuration(.45);
        particle->resetSystem();
        particle->setStartSize(size);
        particle->setEndSize(size*8);
        particle->setStartColor(Color4F::WHITE);
        particle->setSpeed(size*20);
        particle->setGravity(Vec2(size*3*pow(-1, index), size*20));
        particle->setEndSpin(30);
        particle->setEndSpinVar(30);
        particle->setAngle(235+70*index);
        particle->setAngleVar(2);
        parent->addChild(particle);
    });
    
    return call;
}

Sprite* PigHatActionManager::getHawkAttackSprite(cocos2d::Node *parent)
{
    auto spr=AnchorSprite::create(Vec2(.5, .0), parent->getContentSize(),true, "sup_4_hawk.png");
    return spr;
}

#pragma mark - カスタムアクション
void PigHatActionManager::customAction(std::string key,Node*parent)
{
    if (key == CustomAction_Title) {//タイトルシーンでは権利表記をコードで表示する。
        auto text = "©鈴木央・講談社／「劇場版 七つの大罪」製作委員会";
        auto rightsLabel = Label::createWithSystemFont(text, "", 30);
        
        while (Director::getInstance()->getRunningScene()->getContentSize().width*.7 < rightsLabel->getContentSize().width) {//ラベルが画面をはみ出している間はずっと通る。
            rightsLabel->setSystemFontSize(rightsLabel->getSystemFontSize()-1);
        }

        rightsLabel->setPosition(parent->getContentSize().width/2, BannerBridge::getBannerHeight()+rightsLabel->getContentSize().height*.7);
        parent->addChild(rightsLabel);
    }
}
