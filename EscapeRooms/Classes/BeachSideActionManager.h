//
//  IzakayaActionManager.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/10/17.
//
//

#ifndef __EscapeContainer__BeachSideActionManagerManager__
#define __EscapeContainer__BeachSideActionManagerManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class BeachSideActionManager:public CustomActionManager
{
public:
    static BeachSideActionManager* manager;
    static BeachSideActionManager* getInstance();
    
    //over ride
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void touchBeganAction(std::string key,Node*parent);
    void dragAction(std::string key,Node*parent,Vec2 pos);

    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void customAction(std::string key,Node*parent);
private:
    void addShellParticle(Node*parent);
    FiniteTimeAction* moveMoai(Node*parent,bool animate);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
