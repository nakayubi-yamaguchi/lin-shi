//
//  SchoolFesActionManager.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2018/11/29.
//
//

#include "AmusementParkActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "GameScene.h"
#include "TouchManager.h"

using namespace cocos2d;

AmusementParkActionManager* AmusementParkActionManager::manager =NULL;

AmusementParkActionManager* AmusementParkActionManager::getInstance()
{
    if (manager==NULL) {
        manager = new AmusementParkActionManager();
    }
        
    return manager;
}

#pragma mark - 背景
int AmusementParkActionManager::getDefaultBackNum()
{
    /*
    if(!DataManager::sharedManager()->isShowedOpeningStory()) {
        return 0;
    }*/
    
    return 1;
}

#pragma mark - Back
void AmusementParkActionManager::backAction(std::string key, int backID, Node *parent, ShowType type)
{
#pragma mark 間違い探し カッパ
    if (key == "kappa") {
        
        if (!DataManager::sharedManager()->isPlayingMinigame()) {
            return;
        }
        
        auto clip = createClippingNodeToCenter("sup_5_clip.png", parent);
        //clipにはcreateSpriteToCenterでaddchildできない。
  
        auto kappa = parent->getChildByName("sup_5_mistake.png");
        kappa->retain();
        kappa->removeFromParent();
        
        kappa->setLocalZOrder(2);
        kappa->setPositionY(parent->getContentSize().height*.333);
        clip->addChild(kappa);
        kappa->release();
        
        Vector<FiniteTimeAction*> actions;
        
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createSoundAction("bashaa.mp3", type));
        auto shower=CallFunc::create([parent, clip]{
            BlendFunc func;
            func.dst=1;
            func.src=770;
            
            auto particle=ParticleSystemQuad::create("particle_shower.plist");
            particle->setTexture(Sprite::create("particle_fire.png")->getTexture());
            particle->setStartColor(Color4F(.5, .5, 1, 1));
            particle->setEndColor(Color4F(.5,.5,1,0));
            particle->setBlendFunc(func);
            particle->setAutoRemoveOnFinish(true);
            particle->setDuration(.4);
            particle->setPosition(parent->getContentSize().width*.5, parent->getContentSize().height*.4);
            particle->setAngle(90);
            particle->setAngleVar(5);
            particle->setSpeed(parent->getContentSize().height*.8);
            particle->setSpeedVar(particle->getSpeed()*.1);
            particle->setGravity(Vec2(0, -parent->getContentSize().height*3));
            particle->setTotalParticles(100);
            particle->setAngle(90);
            particle->setAngleVar(15);
            particle->setLife(2);
            particle->setLifeVar(.25);
            particle->setStartSize(parent->getContentSize().width*.03);
            particle->setStartSizeVar(parent->getContentSize().width*.01);
            particle->setEndSize(parent->getContentSize().width*.04);
            particle->setEndSizeVar(parent->getContentSize().width*.02);
            
            clip->addChild(particle);
        });
        actions.pushBack(shower);
        
        auto easeoutMove = EaseOut::create(MoveTo::create(1, parent->getContentSize()/2), 2);
        actions.pushBack(createSoundAction("kyuu.mp3", type));
        actions.pushBack(TargetedAction::create(kappa, easeoutMove));
        
        auto call=CallFunc::create([this,parent,kappa]{
            parent->addChild(kappa);
            DataManager::sharedManager()->setTemporaryFlag("mistake_kappa", 1);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 花火
    else if (key == "hanabi") {
        
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(46)) {
            return;
        }
        
        auto clip = createClippingNodeToCenter("sup_4_alpha.png", parent);
        
        //花火ぱーちくる
        std::vector<Color4F> colors = {Color4F::RED, Color4F::GREEN, Color4F::WHITE, Color4F::ORANGE};
        
        std::vector<int> answers = {0, 1, 2, 3, 1, 2, 0};
        Vector<FiniteTimeAction*> actions;
        
        int i = 0;
        for (int answer : answers) {
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(CallFunc::create([this, i, clip, parent, colors, answer](){
                Vec2 startPos = Vec2(parent->getContentSize().width*.2*(answer+1), parent->getContentSize().height*.4);
                Vec2 endPos = Vec2(startPos.x, parent->getContentSize().height*.85);
                this->showHanabi(clip, parent, i, colors.at(answer), startPos, endPos);
            }));
            i++;
        }
        actions.pushBack(DelayTime::create(3.5));//次の発射までの時間
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
#pragma mark ジェットコースター 出口側版
    else if (key == "jetcoaster") {
    
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(47)) {
            auto jetAction = jetcoasterActionForExit(backID, parent);
            parent->runAction(jetcoasterActionForBack(jetAction));
        }
        
        if (backID==1) {
            backAction("wheel", backID, parent, type);
        }
    }
#pragma mark ジェットコースター サーカス側版
    else if(key=="jetcoaster_circus"){
     
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(47)) {
            auto jetAction = jetcoasterActionForCircus(backID, parent, true);
            parent->runAction(jetcoasterActionForBack(jetAction));
        }
        
        backAction("freefall", backID, parent, type);
    }
#pragma mark ジェットコースター 気球側版
    else if(key=="jetcoaster_balloon"){

        if (DataManager::sharedManager()->getEnableFlagWithFlagID(47)) {
            auto jetAction = jetcoasterActionForBalloon(backID, parent, true);
            parent->runAction(jetcoasterActionForBack(jetAction));
        }
        
        if (backID==2 ||
            backID==23) {
            backAction("balloon", backID, parent, type);
        }
    }
#pragma mark ジェットコースター 乗り場版
    else if(key=="jetcoaster_platform"){
        
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(47)) {
            auto jetAction = jetcoasterActionForPlatform(backID, parent, true);
            parent->runAction(jetcoasterActionForBack(jetAction));
        }
    }
#pragma mark 火の輪っか
    else if (key == "firering") {
        
        auto createParticle = [](){
            auto particle = ParticleSystemQuad::create("particle_firering.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setTotalParticles(700);
            
            return particle;
        };
        
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(41)) {
            if (backID == 50) {
                std::vector<Vec2> ringPoses = {Vec2(.5, .583), Vec2(.5, .561), Vec2(.5, .553)};
                std::vector<float> minRadiuses = {.3, .17, .08};
                std::vector<float> maxRadiuses = {.27, .15, .075};
                
                int i = 0;
                for (auto ringPos : ringPoses) {
                    auto particle = createParticle();
                    particle->setName(StringUtils::format("fire_%d",i));
                    particle->setPosition(Vec2(parent->getContentSize().width*ringPos.x, parent->getContentSize().height*ringPos.y));
                    particle->setStartRadius(parent->getContentSize().width*maxRadiuses.at(i));
                    particle->setStartRadiusVar(particle->getStartRadius()*.07);
                    particle->setEndRadius(parent->getContentSize().width*minRadiuses.at(i));
                    
                    
                    particle->setStartSize(particle->getStartSize()/(i+4));
                    particle->setEndSize(particle->getEndSize()/(i+4));
                    
                    particle->setTotalParticles(2600-1000*i);
                    
                    parent->addChild(particle);
                    
                    i++;
                }
                
                //豚が震えている
                auto pig = parent->getChildByName<Sprite*>("sup_50_pig_0.png");
                pig->runAction(Repeat::create(swingAction(Vec2(parent->getContentSize().width*.004, 0), .1, pig, 1.5, false, 5), -1));
            }
            else {
                //小中大の順。左下をアンカーポイントとして、矩形のRect配列
                std::vector<Rect> rects = {Rect(.528, .47, .067, .076), Rect(.462, .447, .105, .123), Rect(.392, .424, .14, .173)};
                
                int i = 0;
                for (auto rect : rects) {
                    float width = parent->getContentSize().width*rect.size.width;
                    float height = parent->getContentSize().height*rect.size.height;
                    auto sprite = Sprite::create();
                    sprite->setTextureRect(Rect(0, 0, height, height));
                    sprite->setOpacity(0);
                    sprite->setPosition(parent->getContentSize().width*(rect.origin.x+rect.size.width/2), parent->getContentSize().height*(rect.origin.y+rect.size.height/2));
                    parent->addChild(sprite);
                    
                    auto particle = createParticle();
                    particle->setPosition(sprite->getContentSize()/2);
                    particle->setStartRadius(height/2);
                    particle->setStartRadiusVar(particle->getStartRadius()*.1);
                    particle->setEndRadius(particle->getStartRadius()*1.005);
                    particle->setStartSize(particle->getStartSize()/4.2);
                    particle->setStartSizeVar(particle->getStartSizeVar()/4.2);
                    particle->setEndSize(particle->getEndSize()/4.2);
                    
                    //大きい輪っかほどパーティクル数増やす
                    particle->setTotalParticles(400 + 500 * i);
                    
                    sprite->setScaleX(width/height);
                    
                    sprite->addChild(particle);
                    
                    i++;
                }
            }
        }
        
        
        if (backID == 46) {
            backAction("piero", backID, parent, type);
        }
        
    }
#pragma mark ピエロ
    else if (key == "piero") {
        
        Vector<FiniteTimeAction*> actions;
        std::vector<Vec2> anchorPoses;
        if (backID == 46) {//一番引き
            anchorPoses = {Vec2(.023, .476), Vec2(.115, .479), Vec2(.193, .464)};
        }
        else if (backID == 47) {//ステージ。引き
            anchorPoses = {Vec2(.203, .391), Vec2(.392, .471), Vec2(.58, .471), Vec2(.77, .429)};
        }
        else if (backID == 48) {//左アップ
            anchorPoses = {Vec2(.215, .192), Vec2(.708, .408)};
        }
        else if (backID == 49) {//右アップ
            anchorPoses = {Vec2(.218, .408), Vec2(.705, .302)};
        }
        
        //全員フラフラ
        for (int i = 0; i < anchorPoses.size(); i++) {
            auto piero = parent->getChildByName(StringUtils::format("sup_%d_piero_%d.png", backID, i));
            transformToAnchorSprite(parent, piero, anchorPoses.at(i));
            piero->setLocalZOrder(2);
            actions.pushBack(swingAction(2.5, 1, piero, 2, true, -1));
        }
        
        parent->runAction(Spawn::create(actions));
    }
#pragma mark 青いペンライト使う掲示板
    else if (key == "lightboard") {
        
        bool onPhotoFlag = type == ShowType_OnPhoto && DataManager::sharedManager()->getTemporaryFlag("lightup_onPhoto").asInt()==1;
        bool onHintFlag = (type == ShowType_Hint || type == ShowType_Hint_Big) && DataManager::sharedManager()->getFlagIDKey(DataManager::sharedManager()->getFlagIDForHint()) == "10";
        
        if (onPhotoFlag || onHintFlag) {
            createSpriteToCenter("sup_10_light.png", false, parent, true);
        }
    }
#pragma mark フリーフォール
    else if (key=="freefall") {
        Vector<FiniteTimeAction*>actions;
        auto sheet=createAnchorSprite(StringUtils::format("sup_%d_sheet.png",backID), .5, .34, false, parent, true);
        createSpriteToCenter(StringUtils::format("sup_%d_cover.png",backID), false, parent,true);
        
        actions.pushBack(DelayTime::create(1));
        
        //moveup
        auto duration_up=6;
        auto distanceY=parent->getContentSize().height*.55;
        auto distanceX=0.0;
        auto scale_rate=.95;
        
        if (backID==3) {
            distanceX=parent->getContentSize().width*.015;
            distanceY=parent->getContentSize().height*.33;
        }
        
        actions.pushBack(jumpAction(-distanceY*.01, .3, sheet, 1.5));
        actions.pushBack(TargetedAction::create(sheet, EaseInOut::create(Spawn::create(ScaleBy::create(duration_up, scale_rate),
                                                                                       createSoundAction("weeen.mp3", type),
                                                                                       MoveBy::create(duration_up, Vec2(distanceX, distanceY)),
                                                                                       NULL), 1.2)));
        actions.pushBack(jumpAction(distanceY*.01, .3, sheet, 1.5));
        actions.pushBack(DelayTime::create(1));
        
        //screem lambda
        auto screem=[this,parent,type,backID](int index){
            std::vector<Vec2> poses={Vec2(.4, .6),Vec2(.45, .6),Vec2(.55, .6),Vec2(.6, .6)};
            std::vector<Vec2> jumpVecs={Vec2(-.15, .05),Vec2(-.1, .15),Vec2(.1, .15),Vec2(.15, .05)};
            std::vector<float>angles={-10,-5,5,10};
            
            auto jumpVec=jumpVecs.at(index);
            auto pos=Vec2(parent->getContentSize().width*poses.at(index).x, parent->getContentSize().height*poses.at(index).y);
            auto angle=angles.at(index);
            auto height=parent->getContentSize().height*(.05+jumpVec.y);
            auto duration=4.5;//全体のduration
            auto fileName=StringUtils::format("screem_%d.png",index);
            auto soundName=(index%2==0)?"pig.mp3":"pig_1.mp3";
            
            auto screem=createSpriteToCenter(fileName, true, parent, true);
            auto scale=parent->getContentSize().width*.3/screem->getContentSize().width;
            
            screem->setPosition(pos);
            screem->setScale(scale*.01);
            auto spawn=Spawn::create(Sequence::create(FadeIn::create(duration*.2),
                                                      DelayTime::create(duration*.6),
                                                      FadeOut::create(duration*.2), NULL),
                                     JumpBy::create(duration/2, Vec2(parent->getContentSize().width*jumpVec.x, parent->getContentSize().height*jumpVec.y), height, 1),
                                     RotateBy::create(duration/2, angle),
                                     EaseBackOut::create(ScaleTo::create(duration/2, scale)),
                                     this->createSoundAction(soundName, type),
                                     NULL);
            
            return TargetedAction::create(screem,Sequence::create(DelayTime::create(.15*(index+2)),
                                                                  spawn,
                                                                  MoveTo::create(0, pos),
                                                                  ScaleTo::create(0, scale*.01),
                                                                  RotateTo::create(0, 0), NULL));
        };
        
        //fall
        auto duration_down=3.0;
        Vector<FiniteTimeAction*>spawns;
        Vector<FiniteTimeAction*>falls;
        falls.pushBack(DelayTime::create(duration_down*.1));
        if (backID==40) {
            falls.pushBack(createSoundAction("fall.mp3", type));
        }
        
        spawns.pushBack(MoveBy::create(duration_down, Vec2(-distanceX, -distanceY)));
        spawns.pushBack(Sequence::create(falls));
        spawns.pushBack(ScaleBy::create(duration_down, 1.0/scale_rate));
        if (backID==40) {
            for (int i=0; i<4; i++) {
                spawns.pushBack(screem(i));
            }
        }
        
        actions.pushBack(TargetedAction::create(sheet, EaseInOut::create(Spawn::create(spawns), 3.0)));
        actions.pushBack(DelayTime::create(1));
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
#pragma mark 気球
    else if(key=="balloon")
    {
        Vector<FiniteTimeAction*>actions;
        auto duration=5.0;
        std::vector<float>ys={.15,.05,.25,.2};
        
        if (backID==2) {
            ys={.06,.03,.12,.09};
        }
        else if (backID==35){
            ys={.05,.02,.1,.075};
        }else if (backID==33){
            ys={.04,.02,.075,.06};
        }
        
        auto clip=createClippingNodeToCenter(StringUtils::format("sup_%d_clip.png",backID), parent);
        clip->setLocalZOrder(2);
        clip->setCascadeOpacityEnabled(true);
        for (int i=0; i<4; i++) {
            auto balloon=createSpriteToCenter(StringUtils::format("sup_%d_balloon_%d.png",backID,i), false, parent);
            clip->addChild(balloon);
            auto distance=parent->getContentSize().height*ys.at(i);
            
            auto flow=Sequence::create(EaseInOut::create(MoveBy::create(duration, Vec2(0, distance)), 1.5),
                                       DelayTime::create(.3),
                                       EaseInOut::create(MoveBy::create(duration, Vec2(0, -distance)), 1.5),
                                       DelayTime::create(.3),
                                       NULL);
            actions.pushBack(TargetedAction::create(balloon, flow));
        }
        
        
        parent->runAction(Repeat::create(Spawn::create(actions), -1));
    }
#pragma mark 噴水
    else if(key=="water")
    {
        auto grid_width=.1;
        auto amplitude=0.05;
        auto radius=1.0;
        createRipple3DSprite(parent, "sup_25_water.png", 3.0, Size(grid_width, grid_width), Vec2(.5, .525), radius, 4, amplitude, -1);
        
        createSpriteToCenter("sup_25_cover.png", false, parent,true);
        
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(23)) {
            auto size = parent->getContentSize().width*.12;
            auto pos = Vec2(parent->getContentSize().width*.35, parent->getContentSize().height*.45);
            
            auto particleBatch = ParticleBatchNode::create("kira.png");
            particleBatch->setName("coinparticle");
            parent->addChild(particleBatch, 1);
            auto particle = ParticleSystemQuad::create("kirakira.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(pos);
            particle->setTotalParticles(12);
            particle->setStartSpinVar(10);
            particle->setLife(1);
            particle->setLifeVar(0);
            particle->setAngleVar(10);
            particle->setStartSize(size);
            particle->setEndColor(Color4F(0, 0, 0, .5));
            particle->setEndSize(particle->getStartSize()*1.2);
            particle->setEndSpin(5);
            particle->setEndSpinVar(15);
            particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
            
            particleBatch->addChild(particle);
        }
    }
#pragma mark - 観覧車
    else if(key=="wheel"&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(37)){
        Vector<FiniteTimeAction*>actions;
        auto duration_part=5.0;//1/8回転分の速度
        auto angle=360.0/8;
        if (backID==1) {
            auto wheel=createAnchorSprite("sup_1_wheel.png", .81, .765, false, parent, true);
            wheel->setLocalZOrder(1);
            actions.pushBack(TargetedAction::create(wheel, RotateBy::create(duration_part, angle)));
            
            std::vector<Vec2>cart_anchors={Vec2(.81, .8915),Vec2(.715, .8515),Vec2(.6765, .765),Vec2(.715, .68),Vec2(.81, .6379),
                Vec2(.908, .68),Vec2(.945, .765),Vec2(.908, .8515),
            };
            for (int i=0; i<8; i++) {
                auto anchor=cart_anchors.at(i);
                auto cart=createAnchorSprite(StringUtils::format("sup_1_cart_%d.png",i), anchor.x, anchor.y, false, wheel, true);
                cart->setLocalZOrder(1);

                actions.pushBack(TargetedAction::create(cart, RotateBy::create(duration_part, -angle)));
            }
            
            parent->runAction(Repeat::create(Spawn::create(actions), -1));
        }
        else if(backID==9){
            //up版
            int pinkIndex=0;
            std::vector<WheelLightType>lights={WheelLightType_None,WheelLightType_Down,WheelLightType_Down,WheelLightType_None,WheelLightType_Up,WheelLightType_Up,WheelLightType_None,WheelLightType_Down};
            for (int i=0; i<lights.size(); i++) {
                actions.pushBack(DelayTime::create(1));
                actions.pushBack(wheelAction(parent, (i==pinkIndex), false, lights.at(i),i));
            }
            
            parent->runAction(Repeat::create(Sequence::create(actions), -1));
            
        }
    }
#pragma mark 目
    else if(key=="eyes")
    {
        Vector<FiniteTimeAction*>actions;
        auto duration=1.0;
        std::vector<int>indexs={0,1,2,3,2,1};
        
        auto eye_0=parent->getChildByName("sup_12_eye_0.png");
        auto eye_1=parent->getChildByName("sup_12_eye_1.png");
        //ウインク
        auto clip=createClippingNodeToCenter(StringUtils::format("sup_%d_clip.png",backID), parent);
        auto lid=createSpriteToCenter("sup_12_lid.png", false, parent);
        lid->setPositionY(parent->getContentSize().height*.55);
        clip->addChild(lid);
        
        actions.pushBack(DelayTime::create(1));
        auto reset=Sequence::create(DelayTime::create(.15),
                                    MoveTo::create(0, parent->getContentSize()/2), NULL);
        actions.pushBack(Spawn::create(swingAction(Vec2(0, parent->getContentSize().height/2-lid->getPositionY()), .3, lid, 1.5, false),
                                       TargetedAction::create(eye_0, reset),
                                       TargetedAction::create(eye_1, reset->clone()), NULL));
        auto distance=parent->getContentSize().width*.01;
        
        for (auto index : indexs) {
            actions.pushBack(DelayTime::create(.5));
            auto x=parent->getContentSize().width/2;
            auto y=parent->getContentSize().height/2;
            if (index==0) {
                y+=distance;
            }
            else if (index==1){
                x-=distance;
            }
            else if (index==2){
                x+=distance;
            }
            else{
                y-=distance*1.2;
            }
            
            auto moveto=EaseInOut::create(MoveTo::create(duration, Vec2(x, y)), 1.5);
            actions.pushBack(Spawn::create(TargetedAction::create(eye_0, moveto),
                                           TargetedAction::create(eye_1, moveto->clone()),
                                           NULL));
        }
        actions.pushBack(DelayTime::create(.5));
        
        /*auto moveto=EaseInOut::create(MoveTo::create(duration, parent->getContentSize()/2), 1.5);
         
         actions.pushBack(Spawn::create(TargetedAction::create(eye_0, moveto),
         TargetedAction::create(eye_1, moveto->clone()),
         NULL));*/
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
}

#pragma mark - Touch
void AmusementParkActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    Vector<FiniteTimeAction*> actions;
    //コールバック
    auto call = CallFunc::create([callback]{
        callback(true);
    });

#pragma mark 吹き出し（クラウド）
    if(key.substr(0, 5) == "cloud") {
        int num = DataManager::sharedManager()->getNowBack();
        
        if (num == 6) {//コイン豚
            actions.pushBack(cloudBounceAction(parent, {"sup_7_pig_0.png"}, Vec2(.5, .375), "sup_6_cloud.png", Vec2(.415, .64), "pig.mp3"));
        }
        else if (num == 7) {//紙幣豚
            actions.pushBack(cloudBounceAction(parent, {"sup_7_pig_0.png"}, Vec2(.5, .375), "sup_7_cloud.png", Vec2(.415, .64), "pig.mp3"));
        }
        else if (num == 24) {//観覧車前子豚
            actions.pushBack(cloudBounceAction(parent, {"sup_24_pig_0.png"}, Vec2(.5, .135), "sup_24_cloud.png", Vec2(.368, .57), "pig.mp3"));
        }
        else if (num == 43) {//ポップコーン入れ
            actions.pushBack(cloudBounceAction(parent, {"sup_43_pig_0.png"}, Vec2(.332, .264), "sup_43_cloud.png", Vec2(.275, .541), "pig.mp3"));
        }
        else if (num == 44) {//サーカス豚
            actions.pushBack(cloudBounceAction(parent, {"sup_44_pig_0.png"}, Vec2(.5, .1), "sup_44_cloud.png", Vec2(.43, .5), "pig.mp3"));
        }
        else if (num == 61) {//魔女豚
            actions.pushBack(cloudBounceAction(parent, {"sup_61_pig_0.png"}, Vec2(.5, .327), "sup_61_cloud.png", Vec2(.347, .721), "pig.mp3"));
        }
        else if (num == 66) {//車掌豚
            actions.pushBack(cloudBounceAction(parent, {"sup_66_pig_0.png"}, Vec2(.5, .1), "sup_66_cloud.png", Vec2(.43, .583), "pig.mp3"));
        }
        else if (num == 72) {//緑スタンプ豚
            actions.pushBack(cloudBounceAction(parent, {"sup_72_pig_0.png"}, Vec2(.5, .312), "sup_72_cloud.png", Vec2(.33, .603), "pig.mp3"));
        }
        actions.pushBack(call);
    }
#pragma mark クリア
    else if(key=="clear"){
        actions.pushBack(useItemSoundAction());

        //エンディング
        actions.pushBack(CallFunc::create([parent]{
            auto story=StoryLayer::create("ending", []{
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        }));
    }
#pragma mark - アイテム使用系
    else if (key.substr(0, 4) == "use_") {
        
        auto key_split = Common::splitString(key, "_");
        auto item = key_split.at(1);//key.substr(4);
        
        //任意の箇所でコールバックを返す場合。
        bool isCustom = key_split.at(key_split.size()-1) == "customcall";
        
        log("アイテムをあげる%s",item.c_str());
        actions.pushBack(useItemSoundAction());
#pragma mark ポップコーンあげる
        if (item == "popcorn") {
            //ポップコーンを渡す
            auto pig_0 = parent->getChildByName("sup_24_pig_0.png");
            transformToAnchorSprite(parent, pig_0, Vec2(.5, .135));
            auto popcorn = createSpriteToCenter("sup_24_popcorn.png", true, parent,true);
            actions.pushBack(giveAction(popcorn, 1.0, .8, true));
            actions.pushBack(DelayTime::create(.5));
            
            //ポップコーン受け取り
            auto pig_1 = createSpriteToCenter("sup_24_pig_1.png", true, parent, true);
            createSpriteToCenter("sup_24_pig_banana.png", false, pig_1, true);

            actions.pushBack(createChangeAction(.5, .5, pig_0, pig_1, "pig.mp3"));
            actions.pushBack(DelayTime::create(.7));

            //バナナに手をかける
            auto pig_2 = createSpriteToCenter("sup_24_pig_2.png", true, parent, true);
            actions.pushBack(createChangeAction(.3, .3, pig_1, pig_2));
            actions.pushBack(swingAction(Vec2(parent->getContentSize().width*.03, 0), .3, pig_2, 1.5, false, 5));
            actions.pushBack(DelayTime::create(.6));
            
            //バナナをくれる
            auto pig_3 = createSpriteToCenter("sup_24_pig_3.png", true, parent, true);
            actions.pushBack(createChangeAction(0, .0, pig_2, pig_3, "poku.mp3"));
            actions.pushBack(DelayTime::create(1.5));
        }
#pragma mark レンチ使用
        else if (item == "wrench") {
            
            //back30
            auto back_30 = createSpriteToCenter("back_30.png", true, parent, true);
            SupplementsManager::getInstance()->addSupplementToMain(back_30, 30, true);
            actions.pushBack(TargetedAction::create(back_30, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(.6));
            
            //ボルト外し
            std::vector<Vec2> vecs = {Vec2(.132, .244), Vec2(.87, .244)};
            for (int i = 0; i < vecs.size(); i++) {
               
                Vec2 vec = vecs.at(i);
                auto wrench = createAnchorSprite(StringUtils::format("sup_30_wrench_%d.png",i), vec.x, vec.y, true, back_30, true);
                
                auto bolt = back_30->getChildByName(StringUtils::format("sup_30_bolt_%d.png",i));
                
                auto fadein = TargetedAction::create(wrench, FadeIn::create(1));
                auto angle = 20*pow(-1, i);
                auto seq = TargetedAction::create(wrench, Sequence::create(EaseInOut::create(RotateBy::create(.5, angle), 1.5),
                                                                         createSoundAction("kacha_1.mp3"),
                                                                         Spawn::create(EaseInOut::create(RotateBy::create(.5, angle), 1.5),
                                                                                       TargetedAction::create(bolt, FadeOut::create(.5)), NULL),
                                                                         DelayTime::create(.3),
                                                                         FadeOut::create(.5), NULL));
                
                actions.pushBack(Sequence::create(fadein,seq, NULL));
            }
            actions.pushBack(DelayTime::create(.8));
            
            //ボードを外す
            auto board = back_30->getChildByName<Sprite*>("sup_30_board.png");
            actions.pushBack(createSoundAction("goto.mp3"));
            actions.pushBack(TargetedAction::create(board, FadeOut::create(.5)));
            actions.pushBack(DelayTime::create(1));
            
            //back31に遷移
            auto back31 = createSpriteToCenter("back_31.png", true, parent, true);
            SupplementsManager::getInstance()->addSupplementToMain(back31, 31, true);
            actions.pushBack(createSoundAction("run.mp3"));
            actions.pushBack(TargetedAction::create(back31, FadeIn::create(.6)));
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark ポップコーン入れをあげる
       else if (item == "popcorncase") {
           //ポップコーン入れを渡す
           auto pig_0 = parent->getChildByName("sup_43_pig_0.png");
           transformToAnchorSprite(parent, pig_0, Vec2(.5, .135));
           auto popcorn = createSpriteToCenter("sup_43_popcorn_case.png", true, parent,true);
           actions.pushBack(giveAction(popcorn, 1.0, .8, true));
           actions.pushBack(DelayTime::create(.5));
           
           //豚スキップ
           auto pig_1 = createSpriteToCenter("sup_43_pig_1.png", true, parent, true);
           actions.pushBack(createChangeAction(.5, .5, pig_0, pig_1, "pig.mp3"));
           actions.pushBack(DelayTime::create(.5));
           actions.pushBack(TargetedAction::create(pig_1, JumpTo::create(1.2, Vec2(parent->getContentSize().width*.9, parent->getContentSize().height*.44), parent->getContentSize().height*.1, 4)));
           
           //豚ポップコーン入れる
           auto pig_2 = createSpriteToCenter("sup_43_pig_2.png", true, parent, true);
           actions.pushBack(createChangeAction(.5, .5, pig_1, pig_2));
           actions.pushBack(DelayTime::create(.5));
           
           //豚ガサゴソ
           actions.pushBack(createSoundAction("gasagoso.mp3"));
           actions.pushBack(DelayTime::create(1));
           //actions.pushBack(Repeat::create(createBounceAction(pig_2, .3), 3));
        }
#pragma mark 火の輪にバケツ使用
       else if (item == "fullbuckets") {
           auto buckets_0 = createSpriteToCenter("sup_50_bucket_0.png", true, parent,true);
           actions.pushBack(TargetedAction::create(buckets_0, FadeIn::create(1)));
           auto buckets_1 = createSpriteToCenter("sup_50_bucket_1.png", true, parent,true);
           buckets_1->setLocalZOrder(1);
           
           //move
           auto distance=parent->getContentSize().height*.05;
           actions.pushBack(TargetedAction::create(buckets_0, Sequence::create(MoveBy::create(.2, Vec2(0, -distance)),
                                                                               DelayTime::create(.7),
                                                                               EaseInOut::create(MoveBy::create(.2, Vec2(0, distance)), 1.5),
                                                                               NULL)));
           actions.pushBack(Spawn::create(createChangeAction(.5, .1, buckets_0, buckets_1,"pop_3.mp3"),
                                          swingAction(Vec2(0, parent->getContentSize().height*.01), .2, buckets_1, 1.2, false), NULL));
           //水かける
           auto water=CallFunc::create([parent]{
               Common::playSE("bashaa.mp3");
               
               BlendFunc func;
               func.dst=1;
               func.src=770;
               
               auto particle=ParticleSystemQuad::create("particle_shower.plist");
               particle->setTexture(Sprite::create("particle_fire.png")->getTexture());
               particle->setStartColor(Color4F(.5, .5, 1, 1));
               particle->setEndColor(Color4F(.5,.5,1,0));
               particle->setBlendFunc(func);
               particle->setAutoRemoveOnFinish(true);
               particle->setDuration(.75);
               particle->setPosition(parent->getContentSize().width*.847, parent->getContentSize().height*.174);
               particle->setSpeed(parent->getContentSize().height*1);
               particle->setSpeedVar(particle->getSpeed()*.1);
               particle->setGravity(Vec2(0, -parent->getContentSize().height*1));
               particle->setTotalParticles(400);
               particle->setAngle(110);
               particle->setAngleVar(20);
               particle->setLife(1);
               particle->setLifeVar(.25);
               particle->setStartSize(parent->getContentSize().width*.04);
               particle->setStartSizeVar(parent->getContentSize().width*.02);
               particle->setEndSize(parent->getContentSize().width*.08);
               particle->setEndSizeVar(parent->getContentSize().width*.02);
               
               parent->addChild(particle);
           });
           actions.pushBack(water);
           
           actions.pushBack(DelayTime::create(1));
           
           //火を消す & 輪っかを黒くする
           auto extinguishFire = CallFunc::create([parent, this, call]{
               for (int i = 0; i < 3; i++) {
                   auto fire = parent->getChildByName<ParticleSystemQuad*>(StringUtils::format("fire_%d",i));
                   fire->setDuration(.5);
               }
           });
           auto ring = createSpriteToCenter("sup_50_ring.png", true, parent, true);
           actions.pushBack(extinguishFire);
           actions.pushBack(TargetedAction::create(ring, FadeIn::create(.5)));
           actions.pushBack(DelayTime::create(.5));
           
           //豚の震え止まる
           auto pig_0 = parent->getChildByName<Sprite*>("sup_50_pig_0.png");
           actions.pushBack(CallFunc::create([pig_0](){
               pig_0->stopAllActions();
           }));
           
           //バケツを消す
           auto bucketSpawn = TargetedAction::create(buckets_1, Spawn::create(FadeOut::create(1),
                                                                              MoveBy::create(1, Vec2(0, -distance)), NULL));
           actions.pushBack(bucketSpawn);
           actions.pushBack(DelayTime::create(.3));
          
           
           //奥の豚飛ぼうとする
           actions.pushBack(this->createSoundAction("pig.mp3"));
           actions.pushBack(this->createBounceAction(pig_0, .3));
           actions.pushBack(DelayTime::create(1));
           
           //飛んでくる
           auto pig_1 = createAnchorSprite("sup_50_pig_1.png", .5, .521, true, parent, true);
           float originalScale = pig_1->getScale();
           Vec2 originalPos = pig_1->getPosition();
           pig_1->setScale(.4);
           pig_1->setPositionY(parent->getContentSize().height*.555);
           
           actions.pushBack(createChangeAction(.2, .2, pig_0, pig_1));
           actions.pushBack(createSoundAction("fall.mp3"));
           float jumpDuration = 1.3;
           actions.pushBack(TargetedAction::create(pig_1, Spawn::create(MoveTo::create(jumpDuration, originalPos),
                                                                        RotateBy::create(jumpDuration, 360),
                                                                        ScaleTo::create(jumpDuration, originalScale), NULL)));
           
           //豚通り抜け
           auto pig_2 = createSpriteToCenter("sup_50_pig_2.png", true, parent, true);
           auto doll = createSpriteToCenter("sup_50_doll.png", true, parent, true);
           actions.pushBack(createChangeAction(.1, .1, {pig_1}, {pig_2, doll}));
           
           //ジャンプ着地と人形放り投げ
           //Pig2軽くジャンプして着地
           auto pig_3 = createSpriteToCenter("sup_50_pig_3.png", true, parent, true);
           auto jumpSE = createSoundAction("jump.mp3");
           auto smallJump = EaseOut::create(MoveTo::create(.3, Vec2(pig_2->getPositionX(), parent->getContentSize().height*.708)), 2);
           auto fallAction = EaseInOut::create(MoveTo::create(.6, Vec2(pig_2->getPositionX(), parent->getContentSize().height*.102)), 2);
           auto pig2_Seq = TargetedAction::create(pig_2, Sequence::create(jumpSE, smallJump, fallAction, NULL));
           auto pig_seq = Sequence::create(pig2_Seq, createChangeAction(.1, .1, pig_2, pig_3, "landing.mp3"), NULL);
           
           
           //人形放り投げる
           ccBezierConfig config;
           config.controlPoint_1 = Vec2(parent->getContentSize().width*.21, parent->getContentSize().height*.789);
           config.controlPoint_2 = Vec2(parent->getContentSize().width*.083, parent->getContentSize().height*.952);
           config.endPosition = Vec2(parent->getContentSize().width*-.247, parent->getContentSize().height*.952);
           
           auto throwAction = EaseIn::create(BezierTo::create(.8, config), 1);
           auto rotateAction = RotateBy::create(.5, 30);
           auto doll_Spawn = TargetedAction::create(doll, Spawn::create(createSoundAction("pop_3.mp3"),
                                                                        throwAction,
                                                                        rotateAction, NULL));
           
           actions.pushBack(Spawn::create(pig_seq, doll_Spawn, NULL));
           actions.pushBack(DelayTime::create(.6));
       }
        
#pragma mark スタンプカード
       else if (item == "stampcard") {
           
           int nowBackID = DataManager::sharedManager()->getNowBack();
           
           auto getFileName = [nowBackID](std::string fileSubName)->std::string{
               auto fileName = StringUtils::format("sup_%d_%s.png", nowBackID, fileSubName.c_str());
               return fileName;
           };
           
           //スタンプカードを渡す
           auto stampcard = createSpriteToCenter(getFileName("stampcard"), true, parent, true);
           stampcard->setLocalZOrder(4);
           
           std::vector<int> flagIDs = {20, 16, 26};//赤、緑、青のフラグ
           for (int i = 0; i < flagIDs.size(); i++) {
               if (DataManager::sharedManager()->getEnableFlagWithFlagID(flagIDs.at(i))) {//条件.他のスタンプを押してもらっていたら
                   createSpriteToCenter(getFileName(StringUtils::format("stamp_%d", i)) , false, stampcard, true);
               }
           }
           actions.pushBack(giveAction(stampcard, 1.0, .8, true));
           
           //豚が受け取る
           auto pig_0 = parent->getChildByName<Sprite*>(getFileName("pig_0"));
           pig_0->setLocalZOrder(3);
           auto pig_1 = createSpriteToCenter(getFileName("pig_1"), true, parent, true);
           pig_1->setLocalZOrder(3);
           actions.pushBack(createChangeAction(.3, .3, pig_0, pig_1, "pop_4.mp3"));
           actions.pushBack(DelayTime::create(.6));
           
           //豚が前のめり
           auto syuniku_open = createSpriteToCenter(getFileName("syuniku_open"), true, parent, true);
           auto pig_2 = createSpriteToCenter(getFileName("pig_2"), true, parent, true);
           pig_2->setLocalZOrder(3);
           actions.pushBack(createChangeAction(.3, .3, pig_1, pig_2, "pig.mp3"));
           actions.pushBack(DelayTime::create(.8));
           
           //ハンコを持って朱肉へ
           auto hanko = parent->getChildByName<Sprite*>(getFileName("hanko"));
           auto pig_3 = createSpriteToCenter(getFileName("pig_3"), true, pig_2, true);
           pig_3->setLocalZOrder(3);
           actions.pushBack(createSoundAction("pop_5.mp3"));
           actions.pushBack(createChangeAction(.3, .3, {hanko}, {pig_3, syuniku_open}));/*createChangeAction(.3, .3, syuniku, {pig_3}, "pop_5.mp3")*/
           actions.pushBack(DelayTime::create(1.2));

           //スタンプカードと朱肉を持つ
           auto pig_4 = createSpriteToCenter(getFileName("pig_4"), true, parent, true);
           pig_4->setLocalZOrder(3);
           actions.pushBack(createChangeAction(.3, .3, pig_2, pig_4, "pop_2.mp3"));
           actions.pushBack(DelayTime::create(1));
           
           //スタンプを押す
           auto pig_5 = createSpriteToCenter(getFileName("pig_5"), true, parent, true);
           pig_5->setLocalZOrder(3);
           actions.pushBack(createChangeAction(.3, .3, pig_4, pig_5, "pig.mp3"));
           actions.pushBack(DelayTime::create(1));
           
           //選択アイテムに応じてgetitemIDをセット
           auto selectedItem=DataManager::sharedManager()->getSelectedItemID();
           std::vector<int>itemIDs={9,23,24,25};
           for (auto i=0;i<itemIDs.size();i++) {
               if (selectedItem==itemIDs.at(i)) {
                   AnswerManager::getInstance()->getItemIDs.push_back(itemIDs.at(i+1));
                   break;
               }
           }
       }
#pragma mark 杖をあげる
       else if (item == "stick") {
           //棒をあげる
           auto stick = createSpriteToCenter("sup_61_light.png", true, parent, true);
           actions.pushBack(giveAction(stick, 1.0, .8, true));

           //豚が棒を受け取る
           auto pig_0 = parent->getChildByName<Sprite*>("sup_61_pig_0.png");
           auto pig_1 = createSpriteToCenter("sup_61_pig_1.png", true, parent, true);
           actions.pushBack(createChangeAction(.5, .5, pig_0, pig_1, "pop_2.mp3"));
           actions.pushBack(DelayTime::create(.7));
           
           //back4に遷移
           auto back4 = createSpriteToCenter("back_4.png", true, parent, true);
           SupplementsManager::getInstance()->addSupplementToMain(back4, 4, true);
           auto sup_4_pig_0 = back4->getChildByName<Sprite*>("sup_4_pig_0.png");
           sup_4_pig_0->removeFromParent();
           actions.pushBack(TargetedAction::create(back4, FadeIn::create(.5)));
           actions.pushBack(DelayTime::create(1));
           
           //魔法使う
           auto sup_4_pig_1 = createSpriteToCenter("sup_4_pig_1.png", false, back4, true);
           auto sup_4_pig_2 = createSpriteToCenter("sup_4_pig_2.png", true, back4, true);
           
           auto magicParticle = ParticleSystemQuad::create("particle_star_magic.plist");
           magicParticle->setAutoRemoveOnFinish(true);
           magicParticle->setStartSize(parent->getContentSize().width/15);
           magicParticle->setPosition(parent->getContentSize().width*.862, parent->getContentSize().height*.48);
           magicParticle->setPosVar(Vec2::ZERO);
           int magicTotal = magicParticle->getTotalParticles();
           magicParticle->setTotalParticles(0);
           back4->addChild(magicParticle);
           
           actions.pushBack(createChangeAction(.5, .5, sup_4_pig_1, sup_4_pig_2, "pig.mp3"));
           actions.pushBack(DelayTime::create(1.3));
           
           //空に向かってパーティクルを表示
           auto callParticle = CallFunc::create([parent, magicParticle, magicTotal](){
               magicParticle->setTotalParticles(magicTotal);
           });
           actions.pushBack(callParticle);
           actions.pushBack(createSoundAction("magic07.mp3"));
           
           float particleDuration = 1.5;
           actions.pushBack(TargetedAction::create(magicParticle, EaseInOut::create(Spawn::create(ScaleTo::create(particleDuration, .6), MoveTo::create(particleDuration, Vec2(parent->getContentSize().width*.4, parent->getContentSize().height*.785)), NULL), 2)));
           actions.pushBack(CallFunc::create([magicParticle](){
               magicParticle->setDuration(.3);
           }));
           actions.pushBack(DelayTime::create(.9));
           
           //back4消す
           actions.pushBack(TargetedAction::create(back4, FadeOut::create(.6)));
           actions.pushBack(DelayTime::create(.5));
           
           actions.pushBack(createSoundAction("pig.mp3"));           
           actions.pushBack(DelayTime::create(.8));
       }
#pragma mark 金orコインあげる
        else if (item == "money"||
                 item=="coin") {
            auto backID=DataManager::sharedManager()->getNowBack();
            auto pig_0=parent->getChildByName(StringUtils::format("sup_7_pig_0.png"));
            transformToAnchorSprite(parent, pig_0, Vec2(.5, .375));
            auto money=createSpriteToCenter(StringUtils::format("sup_%d_%s.png",backID,item.c_str()), true, parent,true);
            actions.pushBack(giveAction(money, 1.0, .8, true));
            
            //豚喜ぶ
            actions.pushBack(createSoundAction("pig.mp3"));
            actions.pushBack(createBounceAction(pig_0, .3, .15));
            actions.pushBack(DelayTime::create(.3));
            
            //豚切り替わる
            auto pig_1 = createAnchorSprite(StringUtils::format("sup_7_pig_1.png"), pig_0->getAnchorPoint().x, pig_0->getAnchorPoint().y, true, parent, true);
            actions.pushBack(createChangeAction(.4, .2, pig_0, pig_1));
            actions.pushBack(Repeat::create(createBounceAction(pig_1, .3, .05), 2));
            
            //豚切り替わる
            actions.pushBack(createSoundAction("pig.mp3"));
            auto pig_2 = createAnchorSprite(StringUtils::format("sup_%d_pig_2.png",backID), pig_0->getAnchorPoint().x, pig_0->getAnchorPoint().y, true, parent, true);
            actions.pushBack(Spawn::create(createChangeAction(.4, .2, pig_1, pig_2),
                                           createBounceAction(pig_2, .3, .05),
                                           NULL));
            actions.pushBack(DelayTime::create(1));
        }

#pragma mark チケット+ロボ
        else if (item == "ticket") {
            auto clip_0=createClippingNodeToCenter("sup_63_clip_0.png", parent);
            auto ticket=createAnchorSprite("sup_63_ticket.png",.5,.8, true, parent,false);
            clip_0->addChild(ticket);
            
            //insert
            actions.pushBack(TargetedAction::create(ticket, FadeIn::create(1)));
            actions.pushBack(TargetedAction::create(ticket, Spawn::create(createSoundAction("printer_1.mp3"),
                                                                          ScaleBy::create(1, .8),
                                                                          MoveBy::create(1, Vec2(0, parent->getContentSize().height*.1)), NULL)));
            actions.pushBack(DelayTime::create(1));
            
            //emittion
            auto clip_1=createClippingNodeToCenter("sup_63_clip_1.png", parent);
            auto pass=createAnchorSprite("sup_63_pass.png",.5,.5, false, parent,false);
            pass->setPositionY(parent->getContentSize().height*.65);
            clip_1->addChild(pass);
            
            actions.pushBack(TargetedAction::create(pass, Sequence::create(createSoundAction("emission.mp3"),
                                                                           MoveTo::create(.5, parent->getContentSize()/2),
                                                                           createSoundAction("koto.mp3"),
                                                                           DelayTime::create(2),
                                                                           FadeOut::create(1),
                                                                           NULL)));
        }
#pragma mark パスポートあげる
        else if (item == "pass") {
            auto pig_0=parent->getChildByName("sup_66_pig_0.png");
            transformToAnchorSprite(parent, pig_0, Vec2(.5, .1));
            auto pass=createSpriteToCenter("sup_66_pass.png", true, parent,true);
            actions.pushBack(giveAction(pass, 1.0, .8, true));
            
            //豚喜ぶ
            actions.pushBack(createSoundAction("pig.mp3"));
            actions.pushBack(createBounceAction(pig_0, .3, .15));
            actions.pushBack(DelayTime::create(1));
            
            //移動
            actions.pushBack(createSoundAction("run.mp3"));
            actions.pushBack(TargetedAction::create(pig_0, JumpBy::create(1.25, Vec2(parent->getContentSize().width*.33, 0), parent->getContentSize().height*.02, 6)));
            actions.pushBack(DelayTime::create(1));

            //豚切り替わる
            /*auto pig_1 = createAnchorSprite("sup_66_pig_1.png", pig_0->getAnchorPoint().x, pig_0->getAnchorPoint().y, true, parent, true);
            actions.pushBack(createChangeAction(.4, .2, pig_0, pig_1));
            actions.pushBack(Repeat::create(createBounceAction(pig_1, .3, .05), 2));
            
            actions.pushBack(DelayTime::create(1));
             */
        }        
#pragma mark マジックハンド
        else if (item == "hand") {
            auto coin=parent->getChildByName<ParticleBatchNode*>("coinparticle");
            
            auto hand=createSpriteToCenter("sup_25_hand.png", true, parent,true);
            actions.pushBack(TargetedAction::create(hand, Sequence::create(FadeIn::create(1),
                                                                           DelayTime::create(1),
                                                                           EaseInOut::create(MoveBy::create(1, Vec2(parent->getContentSize().width*.1, -parent->getContentSize().height*.1)), 1.5),
                                                                           createSoundAction("pochan.mp3"), NULL)));
            //fadeout
            actions.pushBack(Spawn::create(TargetedAction::create(hand, FadeOut::create(1)),
                                           CallFunc::create([coin]{
                auto particle=(ParticleSystemQuad*)coin->getChildren().at(0);
                particle->setDuration(1);
            }),
                                           NULL));
            
        }
#pragma mark バケツ
        else if (item == "bucket") {
            auto bucket_0=createSpriteToCenter("sup_25_bucket_0.png", true, parent,true);
            auto bucket_1=createSpriteToCenter("sup_25_bucket_1.png", true, parent,true);
            
            actions.pushBack(TargetedAction::create(bucket_0, Sequence::create(FadeIn::create(1),
                                                                               DelayTime::create(1),
                                                                               EaseInOut::create(MoveBy::create(1, Vec2(-parent->getContentSize().width*.1, -parent->getContentSize().height*.1)), 1.5),
                                                                               createSoundAction("dopon.mp3"), NULL)));
            //change->fadeout
            actions.pushBack(createChangeAction(1, .5, bucket_0, bucket_1,"pop.mp3"));
            actions.pushBack(DelayTime::create(.5));
            actions.pushBack(TargetedAction::create(bucket_1, FadeOut::create(1)));
        }
#pragma mark Usb
        else if (item == "usb") {
            auto usb=createSpriteToCenter("sup_10_usb.png", true, parent,true);
            
            actions.pushBack(putAction(usb, "kacha.mp3", 1.0, 1));
            //screen
            auto screen=createSpriteToCenter("sup_10_screen.png", true, parent, true);
            actions.pushBack(createSoundAction("robot_start.mp3"));
            actions.pushBack(TargetedAction::create(screen, FadeIn::create(1)));
            actions.pushBack(DelayTime::create(.5));
        }
#pragma mark サーカスチケット
        else if (item == "circusticket") {
            auto pig_0=parent->getChildByName("sup_44_pig_0.png");
            transformToAnchorSprite(parent, pig_0, Vec2(.5, .1));
            auto pass=createSpriteToCenter("sup_44_ticket.png", true, parent,true);
            actions.pushBack(giveAction(pass, 1.0, .8, true));
            
            //豚喜ぶ
            actions.pushBack(createSoundAction("pig.mp3"));
            actions.pushBack(createBounceAction(pig_0, .3, .15));
            actions.pushBack(DelayTime::create(1));
            
            //移動
            actions.pushBack(createSoundAction("run.mp3"));
            actions.pushBack(TargetedAction::create(pig_0, JumpBy::create(1.25, Vec2(parent->getContentSize().width*.33, 0), parent->getContentSize().height*.02, 6)));
            actions.pushBack(DelayTime::create(1));
            
            //豚切り替わる
            /*auto pig_1 = createAnchorSprite("sup_66_pig_1.png", pig_0->getAnchorPoint().x, pig_0->getAnchorPoint().y, true, parent, true);
             actions.pushBack(createChangeAction(.4, .2, pig_0, pig_1));
             actions.pushBack(Repeat::create(createBounceAction(pig_1, .3, .05), 2));
             
             actions.pushBack(DelayTime::create(1));
             */
        }
#pragma mark ライト
        else if (item == "light") {
            auto light=createSpriteToCenter("sup_10_light.png", true, parent,true);
            actions.pushBack(TargetedAction::create(light, FadeIn::create(1)));
            actions.pushBack(DelayTime::create(.8));
            
            DataManager::sharedManager()->setTemporaryFlag("lightup", 1);
        }
#pragma mark バー
        else if (item.compare(0,3,"bar")==0) {
            auto index=atoi(item.substr(3).c_str());
            auto bar=createSpriteToCenter(StringUtils::format("sup_67_%d_0.png",index), true, parent,true);
            actions.pushBack(putAction(bar, "kacha.mp3", 1, .5));
        }
        
        if (!isCustom) {
            //完了
            actions.pushBack(correctSoundAction());
            actions.pushBack(call);
        }
    }
#pragma mark - バナナ
    else if(key.compare(0,6,"banana")==0){
        auto index=atoi(key.substr(7).c_str());
        actions.pushBack(bananaAction(parent, index, ShowType_Normal));
        actions.pushBack(call);
    }
#pragma mark - 機関車
    else if(key=="startSmoke"){
        auto back_68=createSpriteToCenter("back_68.png", true, parent,true);
        actions.pushBack(TargetedAction::create(back_68, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createSoundAction("train.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        //smoke
        actions.pushBack(CallFunc::create([this,parent]{
            auto particle=SupplementsManager::getInstance()->getParticle(parent, DataManager::sharedManager()->getParticleVector(false, 68).at(0).asValueMap());
            parent->addChild(particle);
        }));
        actions.pushBack(DelayTime::create(4));
        
        //replace=
        auto back_62=createSpriteToCenter("back_62.png", true, parent,true);
        back_62->setLocalZOrder(1);
        SupplementsManager::getInstance()->addSupplementToMain(back_62, 62, true);
        actions.pushBack(TargetedAction::create(back_62, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(correctSoundAction());
        
        actions.pushBack(call);
    }
#pragma mark - ホーム移動
    else if(key.compare(0,7,"transTo")==0){
        auto backID=atoi(key.substr(8).c_str());
        actions.pushBack(createSoundAction("train.mp3"));
        auto back=createSpriteToCenter(StringUtils::format("back_%d.png",backID), true, parent,true);

        auto black=Sprite::create();
        black->setOpacity(0);
        black->setColor(Color3B::BLACK);
        black->setTextureRect(Rect(0, 0, parent->getContentSize().width, parent->getContentSize().height));
        black->setPosition(parent->getContentSize()/2);
        parent->addChild(black);
        actions.pushBack(TargetedAction::create(black, FadeIn::create(1)));
        actions.pushBack(TargetedAction::create(back, FadeIn::create(1)));
        actions.pushBack(TargetedAction::create(black, FadeOut::create(1)));
        actions.pushBack(call);
    }
#pragma mark - 観覧車停止
    else if(key=="stopWheel"){
        auto back=createSpriteToCenter("back_9.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 9, true);
        actions.pushBack(TargetedAction::create(back, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(wheelAction(back, true, true, WheelLightType_None, 0));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark ジェットコースター動き出す
    else if (key == "jetcoaster") {
        //back31に遷移
        auto back31 = createSpriteToCenter("back_31.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back31, 31, true);
        actions.pushBack(TargetedAction::create(back31, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1.2));
        
        //ジェットコースター発射
        actions.pushBack(jetcoasterActionForPlatform(31, back31, false));
        actions.pushBack(DelayTime::create(.6));
        
        //back_2に遷移
        auto back2 = createSpriteToCenter("back_2.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back2, 2, true);
        backAction("balloon", 2, back2, ShowType_Normal);
        actions.pushBack(createChangeAction(.5, .5, back31, back2));
        actions.pushBack(jetcoasterActionForBalloon(2, back2, false));
        actions.pushBack(DelayTime::create(1.2));
        
        //風船画像切り替え
        auto balloon_31_0 = back31->getChildByName<Sprite*>("sup_31_balloon_0.png");
        auto balloon_31_1 = createSpriteToCenter("sup_31_balloon_1.png", true, back31, true);
        actions.pushBack(createChangeAction(0, 0, balloon_31_0, balloon_31_1));

        //back_3に遷移
        auto back3 = createSpriteToCenter("back_3.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back3, 3, true);
        backAction("freefall", 3, back3, ShowType_Normal);
        actions.pushBack(createChangeAction(.5, .5, back2, back3));
        actions.pushBack(jetcoasterActionForCircus(3, back3, false));
        actions.pushBack(DelayTime::create(1.2));
        
        //back_1に遷移
        auto back1 = createSpriteToCenter("back_1.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back1, 1, true);
        backAction("wheel", 1, back1, ShowType_Normal);

        actions.pushBack(createChangeAction(.5, .5, back3, back1));
        actions.pushBack(jetcoasterActionForExit(1, back1));
        actions.pushBack(DelayTime::create(1.2));
        
        //back32で帰って来る
        auto back32 = createSpriteToCenter("back_32.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back32, 32, true);
        actions.pushBack(createChangeAction(.5, .5, back1, back32));
        actions.pushBack(DelayTime::create(.6));
        
        //ジェットコースター帰って来る。
        auto coaster = createSpriteToCenter("sup_32_jetcoaster.png", false, back32, true);
        transformToAnchorSprite(back32, coaster, Vec2(.705, .292));
        
        //ジェットコースターのベクトル。等倍して、希望位置までずらす&風船を割る
        auto posVec = Vec2(parent->getContentSize().width*-.54, parent->getContentSize().height*-.16);
        auto coasterPos = coaster->getPosition();
        float coasterScale = 1.28;//コースターの最終的な大きさ
        coaster->setScale(.6);
        coaster->setPosition(Vec2(back32->getContentSize().width*1.172, back32->getContentSize().height*.441));
        
        float jetBalloonDuration = 1.8;
        auto jetSpawn = createJetSpawn(coaster, back32, jetBalloonDuration, coasterScale, coasterPos+posVec*1.5, 1.9);
        
        auto balloon_0 = createSpriteToCenter("sup_32_balloon_0.png", false, back32, true);
        auto balloon_1 = createSpriteToCenter("sup_32_balloon_1.png", true, back32, true);
        auto changeBalloon = createChangeAction(.1, .1, balloon_0, balloon_1, "pan.mp3");
        float balloonDuration = 1;
        auto balloonSeq = Sequence::create(DelayTime::create(balloonDuration), changeBalloon, DelayTime::create(jetBalloonDuration-balloonDuration), NULL);
        
        actions.pushBack(Spawn::create(jetSpawn, balloonSeq, NULL));
        
        //上にあがる
        auto balloon_2 = createSpriteToCenter("sup_32_balloon_2.png", true, back32, true);
        actions.pushBack(createChangeAction(.4, .4, balloon_1, balloon_2, "pop.mp3"));
        actions.pushBack(DelayTime::create(1.2));

        //back31に戻る
        actions.pushBack(TargetedAction::create(back32, FadeOut::create(.5)));
        actions.pushBack(DelayTime::create(.5));
        
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark フォト
    else if(key=="photo"){
        auto back_79=createSpriteToCenter("back_79.png", true, parent,true);
        back_79->setLocalZOrder(3);
        SupplementsManager::getInstance()->addSupplementToMain(back_79, 79,true);
        auto door=createSpriteToCenter("sup_79_open.png", true, back_79 ,true);
        auto camera=createSpriteToCenter("sup_79_camera.png", true, back_79,true);
        actions.pushBack(TargetedAction::create(back_79, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createSoundAction("gacha.mp3"));
        actions.pushBack(Spawn::create(TargetedAction::create(camera, FadeIn::create(1)),
                                       TargetedAction::create(door, FadeIn::create(1)), NULL));
        
        actions.pushBack(TargetedAction::create(camera, FadeOut::create(.7)));
        actions.pushBack(DelayTime::create(.5));
        
        //カメラ表示
        auto camera_1 = createSpriteToCenter("sup_79_camera_1.png", true, back_79, true);
        camera_1->setLocalZOrder(3);
        actions.pushBack(giveAction(camera_1, .6, 1, false));
        actions.pushBack(DelayTime::create(.7));
        
        //カメラパシャり
        actions.pushBack(createSoundAction("camera.mp3"));
        auto upperFrame = Sprite::create();
        upperFrame->setLocalZOrder(camera_1->getLocalZOrder());
        upperFrame->setTextureRect(Rect(0, 0, parent->getContentSize().width, parent->getContentSize().height/2));
        upperFrame->setColor(Color3B::BLACK);
        upperFrame->setAnchorPoint(Vec2(.5, 1));
        upperFrame->setScaleY(0);
        upperFrame->setPosition(Vec2(parent->getContentSize().width/2, parent->getContentSize().height));
        parent->addChild(upperFrame);
        
        auto bottomFrame = Sprite::create();
        bottomFrame->setLocalZOrder(camera_1->getLocalZOrder());
        
        bottomFrame->setTextureRect(Rect(0, 0, parent->getContentSize().width, parent->getContentSize().height/2));
        bottomFrame->setColor(Color3B::BLACK);
        bottomFrame->setAnchorPoint(Vec2(.5, 0));
        bottomFrame->setScaleY(0);
        bottomFrame->setPosition(Vec2(parent->getContentSize().width/2, 0));
        
        parent->addChild(bottomFrame);
        
        auto seq = Sequence::create(EaseIn::create(ScaleTo::create(.2, 1, 1), 2),
                                    EaseOut::create(ScaleTo::create(.2, 1, 0), 2), NULL);
        actions.pushBack(getActionWithTargets({upperFrame, bottomFrame}, seq));
        actions.pushBack(DelayTime::create(.7));
        
        //カメラ切り替え
        auto camera_2 = createSpriteToCenter("sup_79_camera_2.png", true, back_79, true);
        camera_2->setLocalZOrder(camera_1->getLocalZOrder()+1);
        actions.pushBack(createChangeAction(.5, .5, camera_1, camera_2));
        
        //現像
        auto cheki = createSpriteToCenter("sup_79_cheki.png", true, back_79, true);
        cheki->setLocalZOrder(camera_1->getLocalZOrder());
        transformToAnchorSprite(back_79, cheki, Vec2(.5, .448));
        
        auto distance_1 = parent->getContentSize().height*.2;
        auto fadein = TargetedAction::create(cheki, FadeIn::create(2));//delay混み
        auto sound = CallFunc::create([]{Common::playSE("printer_1.mp3");});
        
        auto move_1=TargetedAction::create(cheki, EaseOut::create(MoveBy::create(1.5, Vec2(0, distance_1)),1.5));
        
        auto seq_ = Sequence::create(fadein , sound, move_1, NULL);
        actions.pushBack(seq_);
        actions.pushBack(DelayTime::create(1.5));
        
        //チェキアップに遷移&写真浮かび上がる。
        auto cheki_2 = createSpriteToCenter("sup_79_cheki_2.png", true, back_79, true);
        cheki_2->setLocalZOrder(camera_1->getLocalZOrder());
        auto cheki_3 = createSpriteToCenter("sup_79_cheki_3.png", true, back_79, true);
        cheki_3->setLocalZOrder(camera_1->getLocalZOrder());
        
        actions.pushBack(createChangeAction(.5, .5, {camera_2, cheki}, {cheki_2}));
        actions.pushBack(DelayTime::create(.8));
        
        actions.pushBack(TargetedAction::create(cheki_3, FadeIn::create(.7)));
        actions.pushBack(DelayTime::create(.7));
        
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark 最後のドア開く
    else if(key=="openDoor"){
        auto back_12=createSpriteToCenter("back_12.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back_12, 12,true);
        auto door=back_12->getChildByName("sup_12_door.png");
        
        auto clip=createClippingNodeToCenter("sup_12_clip_door.png", parent);
        
        SupplementsManager::getInstance()->customBackAction(back_12, 12, "eyes", ShowType_Normal);
        actions.pushBack(TargetedAction::create(back_12, FadeIn::create(1)));
        actions.pushBack(CallFunc::create([door,clip]{
            door->retain();
            door->removeFromParent();
            clip->addChild(door);
        }));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createSoundAction("gogogo.mp3"));
        actions.pushBack(TargetedAction::create(door, MoveBy::create(2.0, Vec2(0, parent->getContentSize().height*.33))));
        actions.pushBack(CallFunc::create([this,parent]{
            this->touchAction("clear", parent, nullptr);
        }));
    }
#pragma mark - 指定無し
    else{
        actions.pushBack(call);
    }
    parent->runAction(Sequence::create(actions));
}

void AmusementParkActionManager::showAnswerAction(std::string key, Node *parent, ShowType showType)
{
#pragma mark さる
    if (key=="monkey") {
        Vector<FiniteTimeAction*>actions;
        auto answer=DataManager::sharedManager()->getBackData(55)["useInput"].asValueMap()["answer"].asValueMap()["answer"].asString();
        
        actions.pushBack(DelayTime::create(.5));
        for (int i=0; i<answer.size(); i++) {
            auto index=atoi(answer.substr(i,1).c_str());
            actions.pushBack(bananaAction(parent, index, showType));
        }
        actions.pushBack(DelayTime::create(1.5));
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
}

#pragma mark - touch began
void AmusementParkActionManager::touchBeganAction(std::string key, Node *parent)
{
}


#pragma mark - Drag
void AmusementParkActionManager::dragAction(std::string key, Node *parent, Vec2 pos)
{
    
}

#pragma mark - Item
void AmusementParkActionManager::itemBackAction(std::string key, PopUpLayer *parent)
{
}

void AmusementParkActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造 addする際はcreateSpriteOnClipを使用するとよい
    Vector<FiniteTimeAction*>actions;
    log("アイテムアクション発動 : %s",key.c_str());
    actions.pushBack(useItemSoundAction());
#pragma mark ステッキオン
    if (key=="stick_on") {
        auto stick_off=parent->itemImage->getChildByName("stick_off.png");
        auto stick_on=createSpriteToCenter("stick_on.png", true, parent->itemImage,true);
        auto light=createSpriteToCenter("stick_light.png", true, parent->itemImage,true);
        
        actions.pushBack(createChangeAction(1, .7, stick_off, stick_on,"kacha.mp3"));
        actions.pushBack(createSoundAction("pop.mp3"));
        actions.pushBack(TargetedAction::create(light, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
    }
#pragma mark - ステッキくみあわせ
    else if(key=="combine_stick"){
        auto stick_anim=createSpriteToCenter("stick_anim.png", true, parent->backImage,true);
        auto light=createSpriteToCenter("stick_anim_light.png", false, stick_anim,true);
        actions.pushBack(createChangeAction(1.0, 1.0, parent->itemImage, stick_anim));
        
        auto star=createSpriteToCenter("stick_star.png", true, stick_anim,true);
        actions.pushBack(createChangeAction(1.0, 1.0, light, star,"kacha.mp3"));
        actions.pushBack(DelayTime::create(1));
        auto star_light=createSpriteToCenter("stick_star_light.png", true, stick_anim,true);
        actions.pushBack(createChangeAction(1.0, 1.0, star, star_light,"pop.mp3"));
        actions.pushBack(DelayTime::create(1));
        
    }
    //call
    auto call=CallFunc::create([callback](){
        callback(true);
    });
    actions.pushBack(call);
    
    parent->runAction(Sequence::create(actions));
}

#pragma mark - Custom
void AmusementParkActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    if (key == CustomAction_Title){
        auto clip = createClippingNodeToCenter("room_image_clip.png", parent);
        //花火ぱーちくる
        std::vector<Color4F> colors = {Color4F::RED, Color4F::GREEN, Color4F::WHITE, Color4F::ORANGE};
        
        Vector<FiniteTimeAction*> actions;
        
        int i = 0;
        for (Color4F color : colors) {
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(CallFunc::create([i, parent, color, this, clip](){
                Vec2 startPos = Vec2(parent->getContentSize().width*.15*(i+1), parent->getContentSize().height*.4);
                Vec2 endPos = Vec2(startPos.x, parent->getContentSize().height*.83);
                this->showHanabi(clip, parent, i, color, startPos, endPos);
            }));
            i++;
        }
        actions.pushBack(DelayTime::create(3.5));//次の発射までの時間
        
        auto repeat = Repeat::create(Sequence::create(actions), -1);
        
        parent->runAction(repeat);
    }
}

#pragma mark - Camera Shot
void AmusementParkActionManager::cameraShotAction(int backID)
{
    if (backID == 10) {
        int lightupValue = DataManager::sharedManager()->getTemporaryFlag("lightup").asInt();
        DataManager::sharedManager()->setTemporaryFlag("lightup_onPhoto", lightupValue);
    }
}

#pragma mark - Private
FiniteTimeAction* AmusementParkActionManager::wheelAction(Node *parent, bool isPink, bool stopAnimate, WheelLightType lightType, int index)
{
    auto angle=17.5;
    auto originalRotation=-angle;
    auto duration=5.0;
    auto ease=1.0;
    auto arm=createAnchorSprite("sup_9_arm.png", .5, 3.25, false, parent, true);
    arm->setRotation(originalRotation);
    
    auto fileName=StringUtils::format("sup_9_%s.png",(isPink?"pink":"blue"));
    auto wheel=createAnchorSprite(fileName, .5, .878, false, arm, true);
    wheel->setRotation(-originalRotation);
    if (lightType==WheelLightType_Down||
        lightType==WheelLightType_Up) {
        createSpriteToCenter(StringUtils::format("sup_9_light_%d.png",lightType), false, wheel, true);
    }
    
    if (isPink) {
        createAnchorSprite("sup_9_pig_0.png",.5,.525, false, wheel,true);
        createSpriteToCenter("sup_9_close.png", false, wheel,true);
    }
    
    if (stopAnimate) {
        angle=angle/2;
        ease=1.5;
        duration/=2;
    }
    
    
    Vector<FiniteTimeAction*>actions;
    auto rotate=TargetedAction::create(arm, EaseOut::create(
                                                            Spawn::create(RotateBy::create(duration, angle*2),
                                                                          createSoundAction("weeen.mp3"),
                                                                          TargetedAction::create(wheel, RotateBy::create(duration, -angle*2)),
                                                                          NULL),
                                                            ease));
    actions.pushBack(rotate);
    
    if (stopAnimate) {
        //豚が飛び出すアニメを作成
        actions.pushBack(createSoundAction("batan_2.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        auto close=wheel->getChildByName("sup_9_close.png");
        auto open=createSpriteToCenter("sup_9_open.png", true, wheel,true);
        actions.pushBack(createChangeAction(.5, .2, close, open,"gacha.mp3"));
        actions.pushBack(DelayTime::create(1));
        auto pig_0=wheel->getChildByName("sup_9_pig_0.png");
        auto pig_1=createAnchorSprite("sup_9_pig_1.png",.5,.05, true, parent,true);
        auto duration_jump=1.0;
        actions.pushBack(createBounceAction(pig_0, .25, .1));
        
        auto jump=TargetedAction::create(pig_0, Spawn::create(createSoundAction("jump.mp3"),
                                                              JumpTo::create(duration_jump, Vec2(parent->getContentSize().width*.45, parent->getContentSize().height*.1), parent->getContentSize().height*.25, 1),
                                                              ScaleBy::create(duration_jump, 1.1),
                                                              Sequence::create(DelayTime::create(duration_jump*.2),
                                                                               CallFunc::create([pig_0]{pig_0->setLocalZOrder(1);}),
                                                                               DelayTime::create(duration_jump*.6),
                                                                               createChangeAction(.2, .2, pig_0, pig_1),
                                                                               NULL),
                                                              NULL));
        actions.pushBack(jump);
        actions.pushBack(Spawn::create(createBounceAction(pig_1, .1),
                                       createSoundAction("landing.mp3"), NULL));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createSoundAction("run.mp3"));
        actions.pushBack(TargetedAction::create(pig_1, JumpBy::create(1, Vec2(-parent->getContentSize().width*.1, -parent->getContentSize().height*.45), parent->getContentSize().height*.05, 5)));
    }else{
        //reset
        actions.pushBack(Spawn::create(TargetedAction::create(wheel, RotateTo::create(0, -originalRotation)),
                                       TargetedAction::create(arm, RotateTo::create(0, originalRotation)),
                                       NULL));
    }
    
    return Sequence::create(actions);
}

FiniteTimeAction* AmusementParkActionManager::bananaAction(Node *parent, int index, ShowType type)
{
    Vector<FiniteTimeAction*>actions;
    std::vector<Vec2>anchors_monkey={Vec2(.5, .648),Vec2(.25, .398),Vec2(.75, .398),Vec2(.5, .197)};
    auto anchor_monkey=anchors_monkey.at(index);
    
    //give banana
    auto scale=.8;
    auto bananaName=StringUtils::format("sup_55_banana_%d.png",index);
    auto banana=parent->getChildByName(bananaName);
    if (!banana) {
        banana=createAnchorSprite(bananaName, anchor_monkey.x, anchor_monkey.y+.15, true, parent, true);
    }
    banana->setLocalZOrder(1);
    actions.pushBack(createSoundAction("pop.mp3", type));
    actions.pushBack(giveAction(banana, .5, scale, false));
    
    //monkey
    auto clipName="sup_55_clip.png";
    auto clip=parent->getChildByName(clipName);
    if (!clip) {
        clip=createClippingNodeToCenter(clipName, parent);
    }
    
    auto monkeyName=StringUtils::format("sup_55_monkey_%d.png",index);
    auto monkey=parent->getChildByName(monkeyName);
    auto distance=parent->getContentSize().height*.3;
    if (!monkey) {
        monkey=createAnchorSprite(monkeyName,anchor_monkey.x,anchor_monkey.y, false, parent, false);
        monkey->setPositionY(monkey->getPositionY()-distance);
        clip->addChild(monkey);
    }
    
    auto swing=swingAction(Vec2(0, parent->getContentSize().height*.02), .3, banana, 1.2, false, 2);
    //take back
    auto takeBack=TargetedAction::create(banana, Spawn::create(ScaleBy::create(1.0, 1.0/scale),
                                                               FadeOut::create(1.0),
                                                               NULL));
    
    auto move_monkey=TargetedAction::create(monkey, Sequence::create(EaseInOut::create(MoveBy::create(.5, Vec2(0, distance)), 1.4),
                                                                     createSoundAction("monkey.mp3", type),
                                                                     Repeat::create(createBounceAction(monkey, .3, .05), 1),
                                                                     EaseInOut::create(MoveBy::create(.5, Vec2(0, -distance)), 1.4), NULL));
    
    actions.pushBack(Spawn::create(Sequence::create(swing,takeBack, NULL),
                                   move_monkey, NULL));
    
    
    
    return Sequence::create(actions);
}

FiniteTimeAction* AmusementParkActionManager::jetcoasterActionForBack(FiniteTimeAction *jetcoasterAction)
{
    Vector<FiniteTimeAction*> actions;
    actions.pushBack(DelayTime::create(5));
    
    auto seq = Sequence::create(jetcoasterAction, DelayTime::create(30), NULL);
    actions.pushBack(Repeat::create(seq, -1));
    
    return Sequence::create(actions);
}

FiniteTimeAction* AmusementParkActionManager::jetcoasterActionForExit(int backID, Node *parent)
{
    auto jetcoaster = createAnchorSprite("sup_1_jetcoaster.png", .52, .824, false, parent, true);
    auto posX = parent->getContentSize().width*1.1;
    jetcoaster->setPositionX(posX);
    
    //{controlPoint1, controlPoint2, endPoint}の配列
    std::vector<std::vector<Vec2>> posVector = {
        {Vec2(.905, .761), Vec2(.907, .832), Vec2(.848, .832)},//上り
        {Vec2(.772, .832), Vec2(.78, .761), Vec2(.703, .761)},//下り
        {Vec2(.628, .761), Vec2(.59, .832), Vec2(.525, .832)},//上り
        {Vec2(.438, .832), Vec2(.432, .759), Vec2(.323, .759)},//下り
        {Vec2(.19, .759), Vec2(.233, .906), Vec2(.155, .906)},//上り
        {Vec2(.09, .906), Vec2(.107, .738), Vec2(0, .738)}//下り
    };
    std::vector<int> rotates = {35, 35, 35, 30, 65, 75};
    
    Vector<FiniteTimeAction*> actions;
    
    //スタート位置に来る
    actions.pushBack(MoveTo::create(.5, Vec2(parent->getContentSize().width*.987, parent->getContentSize().height*.761)));
    
    int i = 0;
    for (auto poses : posVector) {
        //コンフィグ作成
        ccBezierConfig config;
        config.controlPoint_1 = Vec2(parent->getContentSize().width*poses.at(0).x, parent->getContentSize().height*poses.at(0).y);
        config.controlPoint_2 = Vec2(parent->getContentSize().width*poses.at(1).x, parent->getContentSize().height*poses.at(1).y);
        config.endPosition = Vec2(parent->getContentSize().width*poses.at(2).x, parent->getContentSize().height*poses.at(2).y);
        
        float bezierDuration = .8;
        auto bezier = BezierTo::create(bezierDuration, config);
        FiniteTimeAction *easeBezierAction;
        if (i%2 == 0) {
            easeBezierAction = EaseOut::create(bezier, 1);
        }
        else {
            easeBezierAction = EaseIn::create(bezier, 1);
        }
        
        auto rotate = RotateTo::create(bezierDuration/2, rotates.at(i)*pow(-1, i));
        auto rotateBack = RotateTo::create(bezierDuration/2, 0);
        auto rotateSeq = Sequence::create(rotate, rotateBack, NULL);
        
        actions.pushBack(Spawn::create(easeBezierAction, rotateSeq, NULL));
        
        i++;
    }
    
    actions.pushBack(MoveBy::create(.5, Vec2(parent->getContentSize().width*-.05, 0)));
    actions.pushBack(CallFunc::create([jetcoaster, posX](){
        jetcoaster->setPositionX(posX);
    }));
    
    return TargetedAction::create(jetcoaster, Sequence::create(actions));
}

FiniteTimeAction* AmusementParkActionManager::jetcoasterActionForCircus(int backID, Node *parent, bool fromBackAction)
{
    Vector<FiniteTimeAction*> actions;
    auto coaster=createSpriteToCenter(StringUtils::format("sup_%d_coaster.png",backID), false, parent,true);
    createSpriteToCenter(StringUtils::format("sup_%d_rail.png",backID), false, parent,true);
    auto firstPos=Vec2(1.05, .5);
    auto secondPos=Vec2(-.15, .9);
    if (backID==40) {
        firstPos=Vec2(1.07, .5);
        secondPos=Vec2(-.15, .87);
    }
    coaster->setPosition(parent->getContentSize().width*firstPos.x, parent->getContentSize().height*firstPos.y);
    
    float duration = 30;
    if (!fromBackAction) {
        duration = 5;
    }
    
    actions.pushBack(MoveTo::create(duration, Vec2(parent->getContentSize().width*secondPos.x, parent->getContentSize().height*secondPos.y)));
    actions.pushBack(CallFunc::create([coaster,firstPos,parent]{
        coaster->setPosition(parent->getContentSize().width*firstPos.x, parent->getContentSize().height*firstPos.y);
    }));
    
    return TargetedAction::create(coaster, Sequence::create(actions));
}

FiniteTimeAction* AmusementParkActionManager::jetcoasterActionForBalloon(int backID, Node *parent, bool fromBackAction)
{
    Vector<FiniteTimeAction*>actions;
    auto coaster=createAnchorSprite(StringUtils::format("sup_%d_coaster.png",backID),.5,.5, false, parent,true);
    createSpriteToCenter(StringUtils::format("sup_%d_rail.png",backID), false, parent,true);
    auto firstPos= !fromBackAction? Vec2(.8, .5) : Vec2(1.1, .5);
    auto secondPos=Vec2(-.2, .5);
    auto scale=1.0;
    auto ease=1.4;
    auto duration= !fromBackAction? 2.5 : 5.0;
    
    if (backID==23) {
        duration=3.0;
    }
    else if(backID==26){
        firstPos=Vec2(1.1, .57);
        secondPos=Vec2(-.2, .7);
        scale=1.1;
    }
    else if(backID==27){
        firstPos=Vec2(1.5, .32);
        secondPos=Vec2(-.2, .62);
    }
    
    coaster->setPosition(parent->getContentSize().width*firstPos.x, parent->getContentSize().height*firstPos.y);
    
    actions.pushBack(EaseIn::create(Spawn::create(MoveTo::create(duration, Vec2(parent->getContentSize().width*secondPos.x, parent->getContentSize().height*secondPos.y)),
                                                  ScaleBy::create(duration, scale),
                                                  NULL),
                                    ease));
    
    actions.pushBack(CallFunc::create([coaster,firstPos,parent,scale]{
        coaster->setPosition(parent->getContentSize().width*firstPos.x, parent->getContentSize().height*firstPos.y);
        coaster->setScale(coaster->getScale()/scale);
    }));
    
    return TargetedAction::create(coaster, Sequence::create(actions));
}

FiniteTimeAction* AmusementParkActionManager::jetcoasterActionForPlatform(int backID, Node *parent, bool fromBackAction)
{
    auto jetcoaster = parent->getChildByName<Sprite*>("sup_31_jetcoaster.png");
    transformToAnchorSprite(parent, jetcoaster, Vec2(.44, .318));
    Vec2 distinateVec2 = Vec2(-.167, .483);

    Vector<FiniteTimeAction*>actions;
    
    actions.pushBack(createJetSpawn(jetcoaster, parent, 2.5, .5, Vec2(parent->getContentSize().width*distinateVec2.x, parent->getContentSize().height*distinateVec2.y), 2));
    
    if (fromBackAction) {
        
        auto directionVec = jetcoaster->getAnchorPoint()-distinateVec2;
        auto posVec = jetcoaster->getAnchorPoint() + directionVec*1.1;
        auto pos = Vec2(parent->getContentSize().width*posVec.x, parent->getContentSize().height*posVec.y);
        jetcoaster->setPosition(pos);
        
        float scale = 1.2;
        jetcoaster->setScale(scale);
        
        actions.pushBack(CallFunc::create([jetcoaster, pos, scale](){
            jetcoaster->setPosition(pos);
            jetcoaster->setScale(scale);
        }));
    }
    
    return Sequence::create(actions);
}

FiniteTimeAction* AmusementParkActionManager::createJetSpawn(Sprite *targetSprite, Node *parent, float jetDuration, float scaleS, Vec2 jetPos, float easeRate)
{
    auto scale = ScaleTo::create(jetDuration, scaleS);
    
    auto move =  MoveTo::create(jetDuration, Vec2(jetPos.x, jetPos.y));
    auto scaleMove = Spawn::create(scale, move, NULL);
    
    FiniteTimeAction* easeMove = EaseIn::create(scaleMove, easeRate);
    
    float jumpDuration = .1;
    float jumpWaitDuration = .3;
    
    auto jump = jumpAction(parent->getContentSize().height*.003, jumpDuration, targetSprite, 2);
    auto jumpSeq = Sequence::create(jump, DelayTime::create(jumpWaitDuration), NULL);
    auto jumpRepeat = Repeat::create(jumpSeq, jetDuration/(jumpDuration + jumpWaitDuration));
    auto jetSpawn = Spawn::create(createSoundAction("weeen.mp3"), scale, easeMove, jumpRepeat, NULL);
    
    return TargetedAction::create(targetSprite, jetSpawn);
}

void AmusementParkActionManager::showHanabi(Node *clip, Node *parent, int index, Color4F color, Vec2 startPos, Vec2 endPos)
{
    float meteorDuration = 2;
    auto meteorParticle = ParticleSystemQuad::create("particle_meteor.plist");
    meteorParticle->setAutoRemoveOnFinish(true);
    meteorParticle->setLife(.3);
    meteorParticle->setStartSize(parent->getContentSize().width/60);
    meteorParticle->setStartSizeVar(meteorParticle->getStartSize()*.1);
    meteorParticle->setPosVar(Vec2(0, meteorParticle->getPosVar().y));
    meteorParticle->setEndSize(0);
    meteorParticle->setStartColor(color);
    meteorParticle->setPosition(startPos);
    meteorParticle->setDuration(meteorDuration);
    clip->addChild(meteorParticle);
    
    Vector<FiniteTimeAction*> actions;
    if (index % 4 == 0) {//連チャンうるせえ
        actions.pushBack(createSoundAction("hanabi_pyuu.mp3"));
    }
    actions.pushBack(EaseOut::create(MoveTo::create(meteorDuration, endPos), 3));
    actions.pushBack(createSoundAction("hanabi_don.mp3"));
    actions.pushBack(CallFunc::create([color, endPos, parent, clip](){
        auto particle = ParticleSystemQuad::create("particle_hanabi.plist");
        particle->setAutoRemoveOnFinish(true);
        particle->setPosition(endPos);
        particle->setStartColor(color);
        particle->setStartSize(parent->getContentSize().width/70);
        particle->setStartSizeVar(particle->getStartSize()*.1);
        particle->setEndSize(particle->getStartSize()*.9);
        particle->setEndSizeVar(0);
        particle->setGravity(Vec2(0, -50));
        particle->setLife(1.3);
        
        particle->setSpeed(parent->getContentSize().width/14);
        particle->setSpeedVar(particle->getSpeed()*1.25);
        
        clip->addChild(particle);
    }));
    
    meteorParticle->runAction(Sequence::create(actions));
}
