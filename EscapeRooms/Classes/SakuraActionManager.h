//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__SakuraActionManager__
#define __EscapeContainer__SakuraActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class SakuraActionManager:public CustomActionManager
{
public:
    static SakuraActionManager* manager;
    static SakuraActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void customAction(std::string key,Node*parent);
    void clearParticleAction(Node*parent);

    ParticleSystemQuad* createFire(Node*parent,float size,Vec2 pos);
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
