//
//  DescriptionLayer.hpp
//  Princess
//
//  Created by 成田凌平 on 2017/09/09.
//
//

#ifndef DescriptionLayer_h
#define DescriptionLayer_h

#include "cocos2d.h"

USING_NS_CC;

class DescriptionLayer : public cocos2d::LayerColor
{
public:
    static DescriptionLayer* create(const ccMenuCallback &callback,float opacity,bool singleImage);

    virtual bool init();
    virtual bool init(const ccMenuCallback &callback,float opacity,bool singleImage);
    bool isShowed;
    float mOpacity;
    int showIndex;
    int endIndex;

    CREATE_FUNC(DescriptionLayer);
    CC_SYNTHESIZE(bool,useMultiImage, UseMultiImage)
    ~DescriptionLayer();
    
private:
    ccMenuCallback mCallback;
    
    //UI
    void createMain();

    //タッチ
    void addTouchListener();
    /**画面orSkipタッチで呼び出し 次のレイヤ表示or閉じる処理*/
    void end();
};

#endif /* DescriptionLayer_hpp */
