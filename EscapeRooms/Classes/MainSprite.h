//
//  MainLayer.h

//notes
//2/20 manageAnswerメソッドを修正,orderSpriteにanimateを追加
//3/7 needFlagID,needItemID,を追加 needTemporaryFlagを改変,partilceTypeを追記
//4/7-4/19 v.3.1 バグ修正(order,slide,temporaryFlag),supplementManagerにparticle処理、カスタム以外の補完画像追加処理を移行,WarpManagerの追加,priorityWarpにflagIDs追記,particleにvar_angle,texture,finishColor,zOrderを追記,switch,sliderのtemporaryFlagKey処理の簡易化 answerMapにcustomActionKey_1を適用

//4/30 rotateSpriteの修正

#ifndef __Kebab__MainSprite__
#define __Kebab__MainSprite__

#include "cocos2d.h"
#include "WarpManager.h"
#include "EscapeStageSprite.h"

USING_NS_CC;

class MainSpriteDelegate
{
public:
    virtual void changeMainSprite(ReplaceType replaceType){};
};

class MainSprite : public EscapeStageSprite
{
public:
    static MainSprite* create(Rect rect);
    virtual bool init(Rect rect);
    
    MainSpriteDelegate*mDelegate;
    void setDelegate(MainSpriteDelegate*p){mDelegate=p;};
    
    //画像変更
    void reloadSprite(bool didWarp,ReplaceType replaceType);
    void backCustomAction(bool didWarp);
    
    void showIS(float dalay);
    
    //delay
    void delayAndCallBack(float duration,ccMenuCallback callback);

    //ポップアップ
    void showPopUp();
        
private:
    ValueVector touchVec;//plistから取得したタッチ領域を格納
    
    //データ
    void loadData();
    //補完処理
    void addSupplements();
    
    //タッチ
    void addEventListner();
    /**touchMapをもとに処理を開始*/
    void manageTouchMap();
    
    //**カスタムアクションがあれば実行する。終了後コールバック*/
    void manageCustomAction(std::string key,ccMenuCallback callback);
    
    void showAnimateLayer(Value animation,ccMenuCallback callback);//アニメーションレイヤを表示するかどうか
    //void animateSupplement(std::string fileName,SupAnimateType type,float duration,float value,ccMenuCallback callback);//指定の補完画像をアニメーション
    void manageMysteryAction(Value mystery,ccMenuCallback callback);//間違い探しの処理
    
    void manageDataByTouch(ValueMap map);//タッチによるフラグ、アイテム周りのデータ処理
    void warp(int warpID,ReplaceType replaceType);//warp処理

    ~MainSprite();
};

#endif /* defined(__Kebab__MainLayer__) */
