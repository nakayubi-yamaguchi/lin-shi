//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "TheaterActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"

using namespace cocos2d;

TheaterActionManager* TheaterActionManager::manager =NULL;

TheaterActionManager* TheaterActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new TheaterActionManager();
    }
    return manager;
}

#pragma mark - Back
void TheaterActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="robo"&&DataManager::sharedManager()->getEnableFlagWithFlagID(10))
    {
        //action
        Vector<FiniteTimeAction*>actions;
        std::vector<std::string>names={StringUtils::format("sup_%d_lefthand.png",backID),
            StringUtils::format("sup_%d_righthand.png",backID)
        };
        
        bool left=true;
        for (auto name : names) {
            auto arm=parent->getChildByName(name);
            actions.pushBack(Spawn::create(swingAction(90*pow(-1, left), .7, arm, 1.5, false),
                                           createSoundAction("robot_move.mp3"), NULL));
            left=false;
        }
        
        std::vector<int>indexs={1,0,0,1,1,0};
        Vector<FiniteTimeAction*>actions_1;
        actions_1.pushBack(DelayTime::create(1));
        for (auto index : indexs) {
            actions_1.pushBack(actions.at(index)->clone());
        }
        actions_1.pushBack(DelayTime::create(1));

        
        parent->runAction(Repeat::create(Sequence::create(actions_1), UINT_MAX));
    }
    else if(key=="cinema"){
        //シネマ文字を両方はめていたら。
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(13)&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(14)) {
            lightup(false,backID, parent, nullptr);
        }
        
        //背景4なら321も
        if (backID== 4) {
            backAction("321",backID, parent);
        }
    }
    else if(key=="321"){//スクリーン
        Vector<FiniteTimeAction*>actions;
        for (int i=3; i>=0; i--) {
            auto spr=createSpriteToCenter(StringUtils::format("sup_%d_%d.png",backID,i), true, parent,true);
            
            Vector<cocos2d::FiniteTimeAction *> arrayOfActions;
            arrayOfActions.pushBack(FadeIn::create(.1));
            if (backID != 4)//背景が4以外で音を出す。
                arrayOfActions.pushBack(createSoundAction("pi_1.mp3"));
            arrayOfActions.pushBack(DelayTime::create((i==0)? 2: 1));//最後の豚のポーズだけ気持ち長め。
            arrayOfActions.pushBack(FadeOut::create(.1));
            
            auto seq=TargetedAction::create(spr, Sequence::create(arrayOfActions));
            
            actions.pushBack(seq);
        }
        
        auto rep=Repeat::create(Sequence::create(actions), UINT_MAX);
        
        parent->runAction(rep);
    }
    else if(key == "drink") {
        log("ドリンクを配置します");
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(26)) {
            //透け防止
            auto sliderSprite = parent->getChildren().at(0);
            auto drink = createSpriteToCenter("sup_49_drinkup.png", true, parent, true);
            drink->setLocalZOrder(sliderSprite->getLocalZOrder()-1);
            
            auto temporaryFlag = DataManager::sharedManager()->getTemporaryFlag("slide_49_0").asInt();
            
            float duration = temporaryFlag? .2: .55;
            
            drink->runAction(FadeIn::create(duration));
            log("確認%f",duration);

            log("ドリンクを配置します2%d",sliderSprite->getLocalZOrder());
        }
    }
    else if(key=="clear"&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(37)){//クリア
        EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

        auto story=StoryLayer::create("ending", []{
            Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
}

#pragma mark - Touch
void TheaterActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="film")
    {//フィルム持ち上げ
        auto back=createSpriteToCenter("back_16.png", true, parent,true);
        back->setCascadeOpacityEnabled(true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 16);
        auto lid=back->getChildByName<Sprite*>("sup_16_lid.png");
        auto open=createSpriteToCenter("sup_16_lid_open.png", true, back,true);
        auto sword=createSpriteToCenter("sup_16_sword.png", true, back,true);

        //fadein
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        
        auto change=Spawn::create(createChangeAction(1, .5, lid, open),
                                  TargetedAction::create(sword, FadeIn::create(.5)),
                                  createSoundAction("kapa.mp3"), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein,DelayTime::create(1),change,DelayTime::create(.7),call,NULL));
    }
    else if(key=="getticket"){
        //パターン正解後
        auto back=createSpriteToCenter("back_47.png", true, parent,true);
        auto ticket_0=createSpriteToCenter("sup_47_ticket_0.png", true, parent,true);
        auto ticket_1=createSpriteToCenter("sup_47_ticket_1.png", true, parent,true);

        auto delay_short=DelayTime::create(.3);
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto fadein_ticket_0=TargetedAction::create(ticket_0, Spawn::create(FadeIn::create(.7),
                                                                            createSoundAction("printer_1.mp3"), NULL));
        auto fadein_ticket_1=createChangeAction(.7, .3, ticket_0, ticket_1);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(delay_short,fadein_back,delay,fadein_ticket_0,fadein_ticket_1,delay->clone(),call,NULL));
    }
    else if(key=="cloud"){
        //吹き出し
        //Common::playSE("pig_1.mp3");
        
        Vector<FiniteTimeAction*>actions;
        
        auto num=DataManager::sharedManager()->getNowBack();
        auto cloud_anchor=Vec2(.5,.65);
        if (num==26) {
            cloud_anchor=Vec2(.566,.533);
        }
        else if (num==40) {
            cloud_anchor=Vec2(.391,.656);
        }
        
        auto cloud=AnchorSprite::create(cloud_anchor, parent->getContentSize(), StringUtils::format("sup_%d_cloud.png",num));
        cloud->setOpacity(0);
        parent->addChild(cloud);
        auto original_scale=cloud->getScale();
        cloud->setScale(.1);
        
        auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(.5, original_scale),
                                                                                    FadeIn::create(.5), NULL),
                                                                      createSoundAction("nyu.mp3"),
                                                                      DelayTime::create(.5),
                                                                      Spawn::create(ScaleTo::create(.5, .1),
                                                                                    FadeOut::create(.5), NULL), NULL));
        
        if (num==26) {
            //豚のアニメ
            auto angle=20;
            auto rotate_left=swingAction(angle, .2, parent->getChildByName("sup_26_piglefthand.png"), 1.5, true, 2);
            auto rotate_right=swingAction(-angle, .2, parent->getChildByName("sup_26_pigrighthand.png"), 1.5, true, 2);
            auto spawn=Spawn::create(rotate_left,rotate_right,createSoundAction("pig_1.mp3"),seq_cloud, NULL);
            actions.pushBack(spawn);
        }else{
            actions.pushBack(seq_cloud);
        }
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="giveticket"){
        Common::playSE("p.mp3");
        //チケット渡し
        auto ticket=createSpriteToCenter("sup_10_ticket.png", true, parent,true);
        auto smile=createSpriteToCenter("sup_10_smile.png", true, parent,true);
        
        auto seq_ticket=TargetedAction::create(ticket, Sequence::create(FadeIn::create(.5),
                                               ScaleBy::create(1, .8), NULL));
        auto delay=DelayTime::create(1);
        auto spawn_smile=createChangeAction(1, .7, ticket, smile,"manager_2.mp3");
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_ticket,delay,spawn_smile,delay->clone(),call,NULL));
    }
    else if(key.compare(0,4,"regi")==0){
        Common::playSE("keyboard.mp3");
        auto str=key.substr(5,1);
        auto index=DataManager::sharedManager()->getTemporaryFlag("regi").asInt();
        auto correct=false;
        std::string text="";
        for (auto label:SupplementsManager::getInstance()->labelSupplements) {
            text.append(label->getString());
        }
        
        if(str=="e"){
            if (text=="321"&&DataManager::sharedManager()->getEnableFlagWithFlagID(7)) {
                correct=true;
            }
            else{
                //reset
                Common::playSE("boo.mp3");
                DataManager::sharedManager()->setTemporaryFlag("regi", 0);
                for (auto label:SupplementsManager::getInstance()->labelSupplements) {
                    label->setString("0");
                }
            }
        }
        else{
            if (index<3) {
                auto label=SupplementsManager::getInstance()->labelSupplements.at(index);
                //レジ入力
                label->setString(str);
                DataManager::sharedManager()->setTemporaryFlag("regi", index+1);
            }
        }
        
        auto but=parent->getChildByName(StringUtils::format("sup_36_%s.png",str.c_str()));
        
        auto seq_but=TargetedAction::create(but, Sequence::create(FadeOut::create(0),
                                                                  DelayTime::create(.1),
                                                                  FadeIn::create(0),NULL));
        auto call=CallFunc::create([callback,correct](){
            //Common::playSE("pinpon.mp3");
            callback(correct);
        });
        
        parent->runAction(Sequence::create(seq_but,call, NULL));
    }
    else if(key.compare(0,7,"picture")==0)
    {//かたかた揺らす
        Common::playSE("kacha.mp3");
        
        auto index=atoi(key.substr(8,1).c_str());
        
        auto picture=SupplementsManager::getInstance()->switchSprites.at(0);
        auto swing=swingAction(-20*pow(-1, index), .1, picture, 2);
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(swing,call, NULL));
    }
    else if(key.compare(0,5,"put_a")==0){
        Common::playSE("p.mp3");
        Vector<FiniteTimeAction*>actions;
        
        auto index=atoi(key.substr(6,1).c_str());
        auto enable=(index==0&&DataManager::sharedManager()->getEnableFlagWithFlagID(14))||
        (index==1&&DataManager::sharedManager()->getEnableFlagWithFlagID(13));
        
        auto a=createSpriteToCenter(StringUtils::format("sup_50_a_%d.png",index), true, parent,true);
        auto fadein_a=TargetedAction::create(a, FadeIn::create(1));
        actions.pushBack(fadein_a);

        auto call=CallFunc::create([callback,parent,enable,this]{
            Common::playSE("kacha.mp3");

            if (enable) {
                this->lightup(true,DataManager::sharedManager()->getNowBack(),parent, callback);
            }
            else{
                callback(true);
            }
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if (key=="clock"){
        //とけい正解後
        auto back=createSpriteToCenter("back_27.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 27);
        auto hint=createSpriteToCenter("sup_27_softcream.png", true, back,true);
        
        auto delay=DelayTime::create(1);
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto fadein_hint=TargetedAction::create(hint, Spawn::create(FadeIn::create(1),
                                                                    createSoundAction("robot_start.mp3"), NULL));
        auto fadeout_back=TargetedAction::create(back, FadeOut::create(1));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(delay,fadein_back,delay->clone(),fadein_hint,delay->clone(),fadeout_back,delay->clone(),call, NULL));
    }
    else if(key=="popcorn"){
        Common::playSE("p.mp3");
        Vector<FiniteTimeAction*>actions_handle;
        //ハンドルあげる
        auto handle=createSpriteToCenter("anim_26_handle.png", true, parent,true);
        auto give_handle=TargetedAction::create(handle, Sequence::create(FadeIn::create(1),
                                                                         ScaleBy::create(1, .8),
                                                                         FadeOut::create(.5), NULL));
        
        //レバーをまわす一連のうごき
        auto duration_handle=1;
        std::vector<int>indexs={0,1,2,0,1,2,3,4};
        for (auto i:indexs) {
            auto filename=StringUtils::format("anim_26_%d.png",i);
            auto anim=parent->getChildByName<Sprite*>(filename);
            if (!anim) {
                anim=createSpriteToCenter(filename, true, parent, true);
            }
            
            if (i==0) {
                Vector<FiniteTimeAction*>actions_0;
                auto fadein=TargetedAction::create(anim, Spawn::create(FadeIn::create(duration_handle),
                                                                       createSoundAction("gigigi.mp3"), NULL));
                actions_0.pushBack(fadein);
                auto pig=parent->getChildByName<Sprite*>("sup_26_pig_0.png");
                auto arm=parent->getChildByName<Sprite*>("sup_26_piglefthand.png");
                auto arm_1=parent->getChildByName<Sprite*>("sup_26_pigrighthand.png");
                auto anim_1=parent->getChildByName<Sprite*>("anim_26_2.png");

                actions_0.pushBack(TargetedAction::create(pig, FadeOut::create(duration_handle*.7)));
                actions_0.pushBack(TargetedAction::create(arm, FadeOut::create(duration_handle*.7)));
                actions_0.pushBack(TargetedAction::create(arm_1, FadeOut::create(duration_handle*.7)));
                if (anim_1) {
                    actions_0.pushBack(TargetedAction::create(anim_1, FadeOut::create(duration_handle*.7)));
                }
                
                actions_handle.pushBack(Spawn::create(actions_0));
            }
            else{
                auto before=parent->getChildByName<Sprite*>(StringUtils::format("anim_26_%d.png",i-1));
                if (i==4) {
                    actions_handle.pushBack(DelayTime::create(1));
                    actions_handle.pushBack(createSoundAction("pop.mp3"));

                    auto change=createChangeAction(duration_handle, duration_handle*.7, before, anim);
                    actions_handle.pushBack(Spawn::create(change,
                                                           NULL));
                    
                }
                else{
                    if (i==3) {//立ち上がる
                        actions_handle.pushBack(DelayTime::create(.5));
                        actions_handle.pushBack(createSoundAction("pig_1.mp3"));
                    }
                    
                    auto change=createChangeAction(duration_handle, duration_handle*.7, before, anim);
                    if(i==3){
                        auto fade_pop=TargetedAction::create(parent->getChildByName("sup_26_popcorncup.png"), FadeOut::create(duration_handle*.7));
                        actions_handle.pushBack(Spawn::create(change,fade_pop, NULL));
                    }
                    else{
                        actions_handle.pushBack(change);
                    }
                }
            }
        }
        
        auto seq_handle=Sequence::create(actions_handle);
        auto delay=DelayTime::create(1);
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(give_handle,seq_handle,delay,call,NULL));
    }
    else if(key=="coin"){
        Common::playSE("p.mp3");
        auto coin=createSpriteToCenter("sup_9_coin.png", true, parent,true);
        
        auto item=createSpriteToCenter(StringUtils::format("sup_9_cork.png"), false, parent,true);
        item->setPositionY(parent->getContentSize().height*.65);
        
        auto seq_coin=TargetedAction::create(coin, Sequence::create(FadeIn::create(.7),
                                                                    DelayTime::create(.7),
                                                                    createSoundAction("chalin.mp3"),
                                                                    FadeOut::create(.7),NULL));
        
        auto seq_item=TargetedAction::create(item, Sequence::create(Common::createSoundAction("koto.mp3"),
                                                                  EaseIn::create(MoveTo::create(.2, parent->getContentSize()/2), 2) ,
                                                                  JumpTo::create(.2, parent->getContentSize()/2, parent->getContentSize().height*.01, 2),
                                                                  DelayTime::create(.5),
                                                                  FadeOut::create(.7),NULL));
        
        auto call=CallFunc::create([callback](){
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_coin,DelayTime::create(1),seq_item,call, NULL));
    }
    else if(key=="finalhint"){
        auto back=createSpriteToCenter("back_51.png", true, parent,true);
        
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto hint=createSpriteToCenter("sup_51_finalhint.png", true, back,true);
        
        auto fadein_hint=TargetedAction::create(hint, Spawn::create(FadeIn::create(1),
                                                                       createSoundAction("robot_start.mp3"), NULL));
        
        auto back_1=createSpriteToCenter("back_40.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back_1, 40);
        auto fadein_back_1=TargetedAction::create(back_1, FadeIn::create(1));
        
        auto delay=DelayTime::create(1);
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_back,delay,fadein_hint,delay->clone(),fadein_back_1,delay->clone(),call, NULL));
    }
    else if(key=="stick"){
        Common::playSE("p.mp3");
        auto bar=createSpriteToCenter("sup_7_bar.png", true, parent,true);
        auto item=parent->getChildByName("sup_7_handle.png");
        
        auto fadein_bar=TargetedAction::create(bar, FadeIn::create(1));
        auto move=MoveBy::create(2.5, Vec2(0, -parent->getContentSize().height*.5));
        auto spawn_move=Spawn::create(TargetedAction::create(bar, move),
                                      TargetedAction::create(item, move->clone()),
                                      createSoundAction("sa.mp3"), NULL);
        
        auto call=CallFunc::create([callback](){
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_bar,spawn_move,call, NULL));
    }
    else if(key=="screw"){
        Common::playSE("p.mp3");
        
        auto anim_0=createSpriteToCenter("anim_52_0.png", true, parent,true);
        auto anim_1=createSpriteToCenter("anim_52_1.png", true, parent,true);

        auto fadein_0=TargetedAction::create(anim_0, Spawn::create(FadeIn::create(1), NULL));
        auto changes=Sequence::create(createSoundAction("gigigi.mp3"),
                                      createChangeAction(.7, .5, anim_0, anim_1),
                                      DelayTime::create(.3),
                                      
                                      createChangeAction(.7, .5, anim_1, anim_0),
                                      DelayTime::create(.3), NULL);
        auto fadeout_s=TargetedAction::create(anim_0, FadeOut::create(1));
        
        auto back=createSpriteToCenter("back_29.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 29,true);
        
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto head=createSpriteToCenter("sup_29_yellowhead.png", true, back,true);
        auto fadein_head=TargetedAction::create(head, Spawn::create(FadeIn::create(1),
                                                                    createSoundAction("pinpon.mp3"), NULL));
        auto delay=DelayTime::create(1);
        auto fadeout_back=TargetedAction::create(back, FadeOut::create(1));
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_0,Repeat::create(changes, 2),Spawn::create(fadein_back,fadeout_s, NULL),delay,fadein_head,delay->clone(),fadeout_back,call, NULL));
    }
    else if(key=="clear"){
        auto plate=parent->getChildByName("sup_13_signboard.png");
        
        auto fadeout_board=TargetedAction::create(plate, Sequence::create(FadeOut::create(1),createSoundAction("pinpon.mp3"), NULL));
        auto delay=DelayTime::create(1.5);
        auto addStory=CallFunc::create([parent]{
            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

            auto story=StoryLayer::create("ending", [parent]{
                auto call=CallFunc::create([parent](){
                    Director::getInstance()->replaceScene(TransitionFade::create(8, ClearScene::createScene(), Color3B::WHITE));
                });
                
                parent->runAction(Sequence::create(call, NULL));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        parent->runAction(Sequence::create(fadeout_board,delay,addStory, NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void TheaterActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造
    if (key=="bag") {
        Common::playSE("p.mp3");
        auto rope=parent->itemImage->getChildByName("bag_up_rope.png");
        auto sword=createSpriteOnClip("bag_up_sword.png", true, parent,true);
        auto original_pos_sword=sword->getPosition();
        sword->setPosition(original_pos_sword.x+sword->getContentSize().width*.1, original_pos_sword.y-sword->getContentSize().height*.1);
        //
        auto seq_sword=TargetedAction::create(sword, Sequence::create(Spawn::create(FadeIn::create(1),
                                                                                    MoveTo::create(2, original_pos_sword), NULL),
                                                                      DelayTime::create(1),
                                                                      Spawn::create(EaseOut::create(MoveBy::create(1, Vec2(-sword->getContentSize().width*.2, -parent->backImage->getContentSize().height*1)), 2),
                                                                                    createSoundAction("sword.mp3"),
                                                                                    TargetedAction::create(rope, FadeOut::create(1)),
                                                                                    NULL), NULL));
        
        auto open=createSpriteOnClip("bag_open.png", true, parent,true);
        auto book=createSpriteToCenter("bag_up_book.png", true, open);
        book->setPositionY(open->getContentSize().height*.25);
        open->addChild(book);
        
        //cover
        createSpriteToCenter("bag_open_cover.png", false, open, true);
        
        auto change=createChangeAction(1, .7, parent->itemImage, open,"bohu.mp3");
        auto delay=DelayTime::create(1);
        auto get_book=TargetedAction::create(book, Spawn::create(FadeIn::create(.5),
                                                                 MoveTo::create(1, open->getContentSize()/2),
                                                                 createSoundAction("sa.mp3"), NULL));
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(seq_sword,delay,change,delay->clone(),get_book,delay->clone(),call, NULL));
    }
    else if(key=="book")
    {
        Common::playSE("p.mp3");
        auto key_0=createSpriteOnClip("book_1_key_0.png", true, parent,true);
        auto key_1=createSpriteOnClip("book_1_key_1.png", true, parent, true);
        auto open=createSpriteOnClip("book_1_open.png", true, parent, true);
        
        auto fadein_key=TargetedAction::create(key_0, Sequence::create(FadeIn::create(1),
                                                                    createSoundAction("key_0.mp3"), NULL));
        auto delay=DelayTime::create(1);
        auto change=Sequence::create(createChangeAction(1, .7, key_0, key_1),
                                     createSoundAction("key_1.mp3"), NULL);
        auto change_1=Spawn::create(createChangeAction(.5, .3, key_1, open,"pinpon.mp3"),
                                    TargetedAction::create(parent->itemImage, FadeOut::create(.3)), NULL);
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_key,delay,change,delay->clone(),change_1,delay->clone(),call, NULL));
    }
}

#pragma mark - Private
void TheaterActionManager::lightup(bool animate, int backID, cocos2d::Node *parent, const onFinished &callback)
{
    auto lightup=createSpriteToCenter(StringUtils::format("sup_%d_lightup.png",backID), true,parent,true );
    
    if (animate) {
        auto delay=DelayTime::create(1);
        auto fadein=TargetedAction::create(lightup, FadeIn::create(1));
        auto call=CallFunc::create([callback]{
            if (callback) {
                Common::playSE("pinpon.mp3");
                callback(true);
            }
        });
        parent->runAction(Sequence::create(delay,fadein,delay->clone(),call, NULL));
    }
    else{
        lightup->setOpacity(255);
        if (callback) {
            callback(true);
        }
    }
}
