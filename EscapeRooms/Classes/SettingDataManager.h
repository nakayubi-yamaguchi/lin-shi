//
//  SettingDataManager.h
//  Kebab
//
//  Created by 成田凌平 on 2016/12/17.
//
//システム管理 音とか

#ifndef __Kebab__SettingDataManager__
#define __Kebab__SettingDataManager__

#include "cocos2d.h"

USING_NS_CC;

typedef enum {//ソートのタイプ
    SortType_Date = 0,
    SortType_Difficulty,
    SortType_Star
}SortType;


class SettingDataManager
{
public:
    static SettingDataManager* manager;
    static SettingDataManager* sharedManager();
    
    void setStopBgm(bool var);
    bool getStopBgm();
    void setStopSe(bool var);
    bool getStopSe();
    void setStopVibe(bool var);
    bool getStopVibe();
    
    void setShowTapImage(bool var);
    bool getShowTapImage();
    
    void setAutoRemoveCathe(bool var);
    bool getAutoRemoveCathe();
    
    void setSortType(SortType sortType);
    SortType getSortType();
    
    void setIsAscend(bool isAscend);
    bool getIsAscend();
private:
    void init();
    
    bool stopBgm;
    bool stopSe;
    bool stopVibe;

};

#endif /* defined(__Kebab__SettingDataManager__) */
