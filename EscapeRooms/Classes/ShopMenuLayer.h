//
//  SettingLayer.h
//  EscapeRooms
//
//  Created by yamaguchinarita on 2017/11/22.
//
//

#ifndef _____EscapeContainer________ShopMenuLayer_____
#define _____EscapeContainer________ShopMenuLayer_____

#include "cocos2d.h"
#include "Utils/CustomAlert.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "Utils/PurchaseLayer.h"

USING_NS_CC;
USING_NS_CC_EXT;

#define UserKey_ClickBeginnersPack "clickBeginnersPack"//いずれかをタップでtrue

class ShopMenuLayer : public CustomAlert, TableViewDelegate, TableViewDataSource
{
public:
    static ShopMenuLayer* create(const ccMenuCallback &callback);
    virtual bool init(const ccMenuCallback &callback);
    
    /**初心者パックのtotalコインを取得*/
    int getTotalCoinsByBeginnersPack(bool isBig);
    /**初心者パックのオフ率を取得*/
    float getDiscountRateByBeginnersPack(bool isBig);
private:
    int dailyBonusCount;//デイリーボーナスをもらえる回数。
    int m_coinItemNum;//コインアイテムがどこまでか。0スタート
    bool tappedPackRecommendButton;//分析用。安いメニューで表示される推薦ボタンを押したか
    
    std::vector<std::string> prices;//価格
    std::vector<std::string> items;//購入メニュー
    std::vector<std::string> goodValues;//お得
    std::vector<std::string> itemIds;//アイテムIDたち
    std::map<std::string, std::map<std::string, int>> beginnersItemsMap;//初心者パックの内容。初回コインと、以後10日で1日にもらえる金額

    Sprite *m_pointSprite;
    TableView *m_tableView;
    Size cellSize;
    
    void createItemData();
    void createPremiumMenu();
    
    /**引数のアイテムIDがコイン関連かどうか。*/
    bool isCoinItem(std::string itemId);
    
    void showPurchaseAlert(ssize_t idx);
    void showPurchaseSuccessAlert(ssize_t idx);
    
    // TableViewDelegateがScrollViewDelegateを継承している事情で必要
    virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view) override{};
    virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) override{};
    
    /** セルのサイズを設定*/
    virtual Size cellSizeForTable(TableView* table) override;
    /** セルの中身を設定*/
    virtual TableViewCell* tableCellAtIndex(TableView* table,ssize_t idx) override;
    /** セルの数を設定*/
    virtual ssize_t numberOfCellsInTableView(TableView* table) override;
    
    /** セルが選択された時*/
    virtual void tableCellHighlight(TableView* table, TableViewCell* cell) override;
    /** セルが選択解除された時*/
    virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell) override;
    /** セルをタップしたときの処理*/
    virtual void tableCellTouched(TableView* table,TableViewCell* cell) override;
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
