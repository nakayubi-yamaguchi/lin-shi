//
//  StoryLayer.h
//  BalentienRoom
//
//  Created by yamaguchinarita on 2017/02/11.
//
//

#ifndef __BalentienRoom__StoryLayer__
#define __BalentienRoom__StoryLayer__

#include "cocos2d.h"

//キャラクタ画像は幅基準で合わせている
USING_NS_CC;


typedef enum
{
    CharacterPosition_None = 0,
    CharacterPosition_Left,
    CharacterPosition_LeftOut,
    CharacterPosition_Center,
    CharacterPosition_Right,
}CharacterPosition;

struct CharacterInfo {
    std::string fileName;
    CharacterPosition position;
    ValueVector supplements;
    float slideRatio;//有効数字0−1.キャラクターをどれくらいカットインバーに食い込ませるか。0でカットインバーにぴったりくっつく。
};

struct Message{//メッセージ構造体
    std::string text;
    Color3B backColor;
    Color4B textColor;
    //std::string soundName;//Deprecated
    std::string backId;
    std::string imageName;
    std::string customActionKey;
    //chara情報管理
    std::vector<CharacterInfo>characterInfos;
    std::string playSE;
    bool playEndingBGM;
};
typedef std::function<void()> storyCallBack;

//ストーリーを追加する場合。plistの文字と一致しているか確認すること。
#define StoryKey_Opening "opening"
#define StoryKey_NotEnd "notEnd"

#define StoryKey_Ending "ending"
#define StoryKey_Ending2 "ending2"

class StoryLayer : public cocos2d::LayerColor
{
public:
    
    static StoryLayer* create(std::string messageName, const ccMenuCallback &callback);
    /*引数なしで使う場合**/
    static StoryLayer* create(std::string messageName, const storyCallBack &callback);

    virtual bool init(float delay,std::string messageName, const ccMenuCallback &callback, const storyCallBack &callback_onfinish);
    
    std::vector<Message>getMessageVector(std::string messageName);
    static CharacterPosition transformCharaPosition(std::string positionName);

    ValueMap replaceMap;
    void setReplace(int index,int backNum);
    
    //ゲッター
    std::string getMessage();
    
    Sprite*cutinSprite;
private:
    float m_firstShowDelay;//最初一文を表示するまでの待ち時間

    CC_SYNTHESIZE(bool, canClose, CanClose);
    CC_SYNTHESIZE(bool, endDelay, EndDelay);
    
    ccMenuCallback m_orgCallback;
    storyCallBack m_orgCallback_onFinished;

    int m_showMessageNum;//何番目のメッセージを表示しているか
    std::vector<Message> m_showMessages;
    Label *showLabel;
    Sprite *showImage;
    //Sprite *showCharacter;

    void addTouchListener();
    
    void show();
    void showCutIn();
    void removeAnimation();

    void createMessageLabel();
    void createCharacter();
    
    /**表示メッセージの中に不適切な形がないか確認。*/
    void checkingShowMessage();
    
    /**現在表示中のメッセージ構造体を取得*/
    Message getShowMessage();
    /***/
    CharacterInfo getCharacterInfo(ValueMap map);
    ~StoryLayer();
};

#endif /* defined(__BalentienRoom__StoryLayer__) */
