//
//  TopMenuSprite.cpp
//  Kebab
//
//  Created by 成田凌平 on 2016/12/17.
//
//

#include "UnderMenuSprite.h"
#include "DataManager.h"
#include "PlistKeys.h"
#include "Utils/Common.h"
#include "UIParts/MenuItemScale.h"
#include "ZOrders.h"
#include "WarpManager.h"
#include "PromoteHintLayer.h"
#include "Utils/NativeAdManager.h"
#include "SupplementsManager.h"
#include "GameScene.h"
#include "Utils/ValueHelper.h"

using namespace cocos2d;

UnderMenuSprite* UnderMenuSprite::create(cocos2d::Rect rect)
{
    auto node=new UnderMenuSprite();
    if (node&&node->init(rect)) {
        node->autorelease();
        return node;
    }
    CC_SAFE_DELETE(node);
    return node;
}

bool UnderMenuSprite::init(cocos2d::Rect rect)
{
    if (!Sprite::init()) {
        return false;
    }
    
    setTextureRect(Rect(0, 0, rect.size.width, rect.size.height));
    setPosition(Vec2(rect.origin.x, rect.origin.y));
    setColor(Color3B::BLACK);
    setLocalZOrder(Zorder_Menu);

    setEnable(true);

    loadData();
    createArrows();
    
    return true;
}

#pragma mark- データロード
void UnderMenuSprite::loadData()
{//遷移先のデータを取得
    arrowMap=DataManager::sharedManager()->getBackData();
}

#pragma mark- UI
void UnderMenuSprite::createArrows()
{//遷移用の矢印を作成。
    float width = getContentSize().height * ((NativeBridge::isTablet())? 1.2 : .8);
    log("下のボタンの幅は%f",getContentSize().height);
    
    //fileNames
    std::vector<std::string> vec={"left_arrow.png","center_arrow.png","right_arrow.png"};
    //defaultの遷移タイプ
    std::vector<ReplaceType>replaces={ReplaceTypeLeft,ReplaceTypeFadeIn,ReplaceTypeRight};
    //spriteにつけるname
    std::vector<std::string>keys={BackDataKeyArrowLeft,BackDataKeyArrowCenter,BackDataKeyArrowRight};
    
    for (int i=0; i<vec.size(); i++) {
        auto spr=Sprite::createWithSpriteFrameName(vec[i]);
        
        auto menuItem=MenuItemScale::create(spr, spr,[this,i,replaces](Ref*ref)
        {//指定のシーンに移動
            if (getEnable()==false) {
                return ;
            }
            
            
            if (DataManager::sharedManager()->checkAutoPromoteHint()) {
                //自動ヒント表示
                this->showPromoteHintLayer();
                return ;
            }
            
            //callbackまでfalse
            auto layer=SupplementsManager::getInstance()->createDisableLayer();
            this->addChild(layer);
            
            auto _ref=(MenuItem*)ref;
            auto map= arrowMap[_ref->getName()].asValueMap();

            auto manager=SupplementsManager::getInstance()->getCustomActionManager();
            manager->arrowAction(map["customActionKey"].asString(), ((GameScene*)getParent())->mainSprite,[this,replaces,i,_ref,layer](bool success){
                
                auto map= arrowMap[_ref->getName()].asValueMap();

                //タッチ可能になる
                layer->removeFromParent();
                
                auto replaceType=replaces[i];
                
                //指定の遷移アニメーションがあれば。
                if (!map["replaceType"].isNull()) {
                    replaceType=(ReplaceType)map["replaceType"].asInt();
                }
                
                //フラグ処理があれば
                if (!map["setTemporaryFlags"].isNull()) {
                    auto flagmaps=map["setTemporaryFlags"].asValueVector();
                    for (auto v : flagmaps) {
                        auto flagmap=v.asValueMap();
                        DataManager::sharedManager()->setTemporaryFlag(flagmap["key"].asString(), flagmap["value"].asInt());
                    }
                }
                else if (!map["setTemporaryFlag"].isNull()) {
                    auto flagmap=map["setTemporaryFlag"].asValueMap();
                    DataManager::sharedManager()->setTemporaryFlag(flagmap["key"].asString(), flagmap["value"].asInt());
                }
                
                //
                if (map["stopSE"].asBool()) {
                    Common::stopAllSE();
                }
                
                //
                if (map["stopAction"].asBool()) {
                    auto scene=(GameScene*)getParent();
                    scene->mainSprite->stopAllActions();
                }
                //SEがあれば
                if (!map["playSE"].isNull()) {
                    Common::playSE(map["playSE"].asString().c_str());
                }
                else{
                    Common::playSE("cha.mp3");
                }
                
                //warpIDの取得 replaceTypeは独自実装
                auto warpStruct=WarpManager::getInstance()->getWarpStruct(map);
                
                //gameSceneに通知。遷移の処理をまかせる
                DataManager::sharedManager()->setNowBack(warpStruct.warpID,replaceType);
            });
        });
        menuItem->setScale(width/spr->getContentSize().height);
        menuItem->setName(keys[i]);
        arrows.pushBack(menuItem);
    }
    
    auto menu=Menu::createWithArray(arrows);
    menu->setPosition(getContentSize().width/2, getContentSize().height/2);
    menu->alignItemsHorizontallyWithPadding((getContentSize().width-width*vec.size())/(vec.size()-1));//スペースをセット
    addChild(menu);
    
    //状態をリロード
    reloadArrows();
}

void UnderMenuSprite::reloadArrows()
{//矢印状態のリロード。遷移先がない場合は透明+タッチ不可にする
    for (int i=0; i<arrows.size(); i++) {
        auto menuitem=arrows.at(i);
        auto map= arrowMap[menuitem->getName()];

        if (!map.isNull()) {
            menuitem->setOpacity(255);
            menuitem->setEnabled(true);
        }
        else{
            menuitem->setOpacity(0);
            menuitem->setEnabled(false);
        }
    }
}

void UnderMenuSprite::showPromoteHintLayer()
{
    SupplementsManager::getInstance()->hidePanel();
    auto hint=PromoteHintLayer::create([this](Ref*ref){
        //reset
        DataManager::sharedManager()->setEnFlagTime(time(NULL));
        //パネル再表示
        auto parent=(GameScene*)this->getParent();
        SupplementsManager::getInstance()->showPanel(parent->mainSprite);
    });
    hint->showAlert(Director::getInstance()->getRunningScene());
}

#pragma mark- 解放
UnderMenuSprite::~UnderMenuSprite()
{
    log("Under解放");
}
