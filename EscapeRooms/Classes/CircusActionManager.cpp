//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "CircusActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "GameScene.h"
#include "TouchManager.h"

using namespace cocos2d;

CircusActionManager* CircusActionManager::manager =NULL;

CircusActionManager* CircusActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new CircusActionManager();
    }
    return manager;
}


#pragma mark - Back
void CircusActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="back_2")
    {//ロボの目
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(1));
        std::vector<int>indexs={0,1,0,1,2};
        auto eye_0=createSpriteToCenter("sup_2_eye_0.png", true,parent, true);
        auto eye_1=createSpriteToCenter("sup_2_eye_1.png", true,parent, true);
        auto duration=1;
        auto ease=1.2;
        
        for (int i=0; i<indexs.size(); i++) {
            auto index=indexs.at(i);
            auto flash=Sequence::create(EaseOut::create(FadeIn::create(duration), ease),
                                        EaseIn::create(FadeOut::create(duration), ease), NULL);
            if (index==0) {
                actions.pushBack(TargetedAction::create(eye_0, flash));
            }
            else if (index==1) {
                actions.pushBack(TargetedAction::create(eye_1, flash));
            }
            else{
                actions.pushBack(Spawn::create(TargetedAction::create(eye_0, flash),
                                               TargetedAction::create(eye_1, flash->clone()), NULL));
            }
            
            actions.pushBack(DelayTime::create(1));
        }
        
        actions.pushBack(DelayTime::create(1));
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
    else if(key=="pillar")
    {//ピエロの柱
        auto pillar=createAnchorSprite(StringUtils::format("sup_%d_pillar.png",backID), .5, 0, false, parent, true);
        bool left=false;
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(6)) {
            left=true;
            createSpriteToCenter(StringUtils::format("sup_%d_c_0.png",backID), false, pillar,true);
        }
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(7)) {
            createSpriteToCenter(StringUtils::format("sup_%d_c_1.png",backID), false, pillar,true);
            
            if (left) {
                parent->runAction(movePillarAction(parent, pillar, backID, false));
            }
        }
    }
    else if(key=="mouse")
    {//ねずみ
        std::vector<int>indexs={1,2,0,2,0,1};
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(1));
        auto counter=0;
        for (auto index : indexs) {
            auto name=StringUtils::format("sup_27_mouse_%d.png",index);
            if (DataManager::sharedManager()->isPlayingMinigame()&&
                counter==5) {
                name="sup_27_mistake.png";
            }

            auto mouse=parent->getChildByName(name);
            if (mouse==NULL) {
                mouse=createSpriteToCenter(name, true, parent,true);
            }
            actions.pushBack(TargetedAction::create(mouse, Sequence::create(createSoundAction("sa.mp3"),
                                                                            FadeIn::create(.5),
                                                                            DelayTime::create(1),
                                                                             NULL)
                                                    ));
            if (counter!=5||
                !DataManager::sharedManager()->isPlayingMinigame()) {
                actions.pushBack(TargetedAction::create(mouse, Sequence::create(createSoundAction("sa.mp3"),
                                                                                FadeOut::create(.5), NULL)));
                actions.pushBack(DelayTime::create(1));
            }
            counter++;
        }

        if (DataManager::sharedManager()->isPlayingMinigame()) {
            actions.pushBack(CallFunc::create([]{
                DataManager::sharedManager()->setTemporaryFlag("mistake_mouse", 1);
            }));
            parent->runAction(Repeat::create(Sequence::create(actions), 1));
        }
        else{
            actions.pushBack(DelayTime::create(1));
            parent->runAction(Repeat::create(Sequence::create(actions), -1));
        }
    }
    else if(key=="rope")
    {//綱渡り
        //mistake
        if (DataManager::sharedManager()->isPlayingMinigame()&&
            DataManager::sharedManager()->getTemporaryFlag("switch_sup_11_0").asInt()==1) {
            auto mistake=parent->getChildByName("sup_11_mistake.png");
            mistake->setOpacity(255);
        }
        
        //rope ぐらぐら
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(36)) {
            auto pig=parent->getChildByName("sup_11_pig_0.png");
            parent->runAction(swingAction(2.5, 1, pig, 2, true, -1));
        }
    }
    else if(key=="doll"){
        auto lid=parent->getChildByName("sup_65_lid.png");
        auto doll=createAnchorSprite("sup_65_doll.png", .5, .21, true, parent, true);
        auto pos_original=doll->getPosition();
        auto scale_original=doll->getScale();
        doll->setPositionY(0);
        doll->setScaleY(scale_original*.1);
        auto cover=createSpriteToCenter("sup_65_cover.png", true, parent,true);
        
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(.5));
        auto fadeout_lid=Sequence::create(TargetedAction::create(lid, FadeOut::create(1)),
                                          createSoundAction("kapa.mp3"), NULL);
        actions.pushBack(fadeout_lid);
        actions.pushBack(TargetedAction::create(cover, FadeIn::create(0)));
        actions.pushBack(TargetedAction::create(doll, FadeIn::create(.5)));
        
        auto duration=.3;
        auto ease=1.5;
        auto doll_appear=TargetedAction::create(doll, EaseIn::create(Spawn::create(MoveTo::create(.5, pos_original),
                                                                                   ScaleTo::create(.5, scale_original), NULL), ease));
        actions.pushBack(doll_appear);
        
        //上下に跳ねるactionをつなぎとして挟む?
        auto bounce=Sequence::create(
                                     EaseInOut::create(ScaleTo::create(duration, scale_original,scale_original*1.1), ease),
                                     createSoundAction("spring.mp3"),
                                     EaseInOut::create(ScaleTo::create(duration, scale_original,scale_original*.9), ease),
                                     EaseInOut::create(ScaleTo::create(duration, scale_original,scale_original*1.1), ease),
                                     EaseInOut::create(ScaleTo::create(duration/2, scale_original), ease), NULL);
        actions.pushBack(TargetedAction::create(doll, bounce));
        
        //spring action
        std::vector<int>indexs={0,0,1,1,0,1,0,1};
        for (auto index : indexs) {
            
            //actions.pushBack(TargetedAction::create(doll, bounce));
            
            
            auto angle=15;
            auto skew=Sequence::create(EaseInOut::create(SkewTo::create(duration*3.5/2, angle*pow(-1, index), 0), ease),
                                                                    EaseInOut::create(SkewTo::create(duration*3.5/2, 0, 0), ease), NULL);
            actions.pushBack(TargetedAction::create(doll, Spawn::create(bounce->clone(),skew, NULL)));
        }
        
        //減衰
        auto bounce_end=Sequence::create(
                                         EaseInOut::create(ScaleTo::create(duration, scale_original,scale_original*.975), ease),
                                         EaseInOut::create(ScaleTo::create(duration/2, scale_original), ease), NULL);
        actions.pushBack(TargetedAction::create(doll, bounce_end));
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="back_44")
    {//揺らめくはた
        Vector<FiniteTimeAction*>actions;
        
        std::vector<int>indexs={0,5,1,4,2,3,0};
        actions.pushBack(DelayTime::create(1));
        for (auto index : indexs) {
            actions.pushBack(TargetedAction::create(parent->getChildByName(StringUtils::format("sup_44_%d.png",index)), Sequence::create(createSoundAction("wind_short.mp3")
                                                                                                                                         ,FadeOut::create(.3),DelayTime::create(.7),FadeIn::create(.3), NULL)));
            actions.pushBack(DelayTime::create(1));
        }
        
        actions.pushBack(DelayTime::create(1));
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="sleep"&&!DataManager::sharedManager()->getEnableFlagWithFlagID(31))
    {//鼻ちょうちん
        Vector<FiniteTimeAction*>actions;
        auto balloon=createAnchorSprite("sup_35_balloon.png", .57, .6, true, parent, true);
        auto scale=balloon->getScale();
        balloon->setScale(scale*.01);
        balloon->setLocalZOrder(1);
        actions.pushBack(DelayTime::create(1));
        
        auto duration=2.0;
        actions.pushBack(TargetedAction::create(balloon, Repeat::create(Sequence::create(createSoundAction("snoring.mp3"),
                                                                                         EaseInOut::create(Spawn::create(ScaleTo::create(duration, scale),
                                                                                                                         FadeTo::create(duration, 200), NULL),1.5),
                                                                                         EaseInOut::create(ScaleTo::create(duration, scale*.3),1.5),
                                                                                         NULL), -1)));
        
        balloon->runAction(Sequence::create(actions));
    }
    else if(key=="gauge"&&DataManager::sharedManager()->getEnableFlagWithFlagID(33)){
        hammerGaugeAction(parent, backID, false);
    }
    else if(key=="water"&&DataManager::sharedManager()->getEnableFlagWithFlagID(41)){
        std::vector<int>indexs={0,2,1,2,0,1};
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(.5));
        for (int index : indexs) {
            auto name=StringUtils::format("sup_54_water_%d.png",index);
            auto water=parent->getChildByName(name);
            if (!water) {
                water=createSpriteToCenter(name, true, parent, true);
            }
            actions.pushBack(TargetedAction::create(water, Sequence::create(createSoundAction("jobojobo.mp3"),
                                                                            FadeIn::create(.25),
                                                                            DelayTime::create(2),
                                                                            FadeOut::create(.25), NULL)));
        }
        actions.pushBack(DelayTime::create(1));
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
}

#pragma mark - Touch
void CircusActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="back_56"||
       key=="back_57"||
       key=="back_58"||
       key=="back_21"||
       key=="back_6"||
       key=="back_8"||
       key=="back_72"||
       key=="back_73"||
       key=="back_74"||
       key=="back_75"||
       key=="back_78"||
       key=="back_62"||

       key=="back_40"||
       key=="back_41"){
        //ギミック正解後 switch open
        Vector<FiniteTimeAction*>actions;
        
        int backID=0;
        
        std::string back;
        std::string door;
        std::string sup_name="";
        std::string soundName="gacha.mp3";
        bool removeDoor=false;
        bool playPinpon=true;
        
        if (key=="back_56") {
            back="back_50.png";
            backID=50;
            door="sup_50_0_1.png";
            sup_name="sup_50_c.png";
        }
        else if (key=="back_57") {
            back="back_50.png";
            backID=50;
            door="sup_50_1_1.png";
            sup_name="sup_50_bag.png";
        }
        else if (key=="back_58") {
            back="back_50.png";
            backID=50;
            door="sup_50_2_1.png";
            sup_name="sup_50_key.png";
        }
        else if (key=="back_21") {
            back="back_20.png";
            backID=20;
            door="sup_20_lid.png";
            sup_name="sup_20_device.png";
            soundName="pop.mp3";
            removeDoor=true;
        }
        else if (key=="back_6") {
            back="back_5.png";
            backID=5;
            door="sup_5_lid.png";
            sup_name="sup_5_handle.png";
            soundName="pop.mp3";
            removeDoor=true;
        }
        else if (key=="back_40") {
            back="back_39.png";
            backID=39;
            door="sup_39_balloon_0.png";
            soundName="pop.mp3";
            removeDoor=true;
            playPinpon=false;
        }
        else if (key=="back_41") {
            back="back_39.png";
            backID=39;
            door="sup_39_balloon_1.png";
            soundName="pop.mp3";
            removeDoor=true;
            playPinpon=false;
        }
        else if (key=="back_8") {
            back="back_7.png";
            backID=7;
            door="sup_7_lid.png";
            sup_name="sup_7_scope.png";
            soundName="pop.mp3";
            removeDoor=true;
        }
        else if (key=="back_72") {
            back="back_71.png";
            backID=71;
            door="sup_71_2_1.png";
            sup_name="sup_71_device.png";
        }
        else if (key=="back_73") {
            back="back_71.png";
            backID=71;
            door="sup_71_1_1.png";
            sup_name="sup_71_bag.png";
        }
        else if (key=="back_74") {
            back="back_71.png";
            backID=71;
            door="sup_71_0_1.png";
            sup_name="sup_71_card.png";
        }
        else if (key=="back_75") {
            back="back_71.png";
            backID=71;
            door="sup_71_apple.png";
            soundName="pop.mp3";
            removeDoor=true;
            playPinpon=false;
        }
        else if (key=="back_78") {
            back="back_77.png";
            backID=77;
            door="sup_77_hammer.png";
            soundName="pop.mp3";
            removeDoor=true;
            playPinpon=false;
        }
        else if (key=="back_62") {
            back="back_59.png";
            backID=59;
            door="sup_59_lid.png";
            soundName="pop.mp3";
            sup_name="sup_59_handle.png";
            removeDoor=true;
        }
        
        auto open=openDoorAction(parent, back, backID, door, sup_name, soundName,removeDoor);
        actions.pushBack(open);
        
        if (backID==59) {
            backAction("pillar", backID, parent->getChildByName(back));
        }
        
        auto call=CallFunc::create([callback,playPinpon]{
            if (playPinpon) {
                Common::playSE("pinpon.mp3");
            }
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="back_28")
    {//鍵開け
        Common::playSE("p.mp3");
        auto back_num=DataManager::sharedManager()->getNowBack();
        auto item=createAnchorSprite(StringUtils::format("sup_%d_key_0.png",back_num), .5, .5, true, parent, true);
        auto item_after=createAnchorSprite(StringUtils::format("sup_%d_key_1.png",back_num), .5, .5, true, parent, true);
        auto open=createSpriteToCenter(StringUtils::format("sup_%d_0_1.png",back_num), true, parent, true);
        createSpriteToCenter(StringUtils::format("sup_%d_c.png",back_num), false, open, true);

        auto seq=TargetedAction::create(item, Sequence::create(FadeIn::create(1),
                                                               createSoundAction("kacha.mp3"),
                                                                DelayTime::create(1),
                                                               createChangeAction(1, .5, item, item_after,"key.mp3"),
                                                               DelayTime::create(1),
                                                               Spawn::create(TargetedAction::create(open, FadeIn::create(1)),
                                                                             TargetedAction::create(item_after, FadeOut::create(1)),
                                                                             createSoundAction("kapa.mp3"),
                                                                             NULL),
                                                                NULL));
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq,DelayTime::create(1),call, NULL));
    }
    else if(key=="handle")
    {//屋台ハンドル
        Common::playSE("p.mp3");
        auto item=createAnchorSprite("sup_51_handle.png", .5, .5, true, parent, true);
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(TargetedAction::create(item, FadeIn::create(1)));
        actions.pushBack(createSoundAction("kacha.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        actions.pushBack(Repeat::create(TargetedAction::create(item,Spawn::create(EaseInOut::create(RotateBy::create(2, 360), 1.5),
                                                                                                     createSoundAction("gigigi.mp3"),
                                                                                                     NULL)), 2));
        
        actions.pushBack(DelayTime::create(1));
        
        //遷移
        auto back=createSpriteToCenter("back_50.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 50, true);
        createSpriteToCenter("sup_50_handle.png", false,back,true);
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        actions.pushBack(fadein_back);
        actions.pushBack(DelayTime::create(1));
        auto open=createSpriteToCenter("sup_50_open.png", true,back,true);
        createSpriteToCenter("sup_50_battery.png", false,open,true);

        actions.pushBack(TargetedAction::create(open, Spawn::create(FadeIn::create(1),
                                                                    createSoundAction("sa.mp3"), NULL)));
        actions.pushBack(DelayTime::create(1));
        auto call=CallFunc::create([callback,this,parent]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if (key.compare(0,4,"putC")==0){
        Common::playSE("p.mp3");
        //Cを置く
        Vector<FiniteTimeAction*>actions;
        auto index=atoi(key.substr(5,1).c_str());
        auto C=createSpriteToCenter(StringUtils::format("sup_60_c_%d.png",index), true, parent,true);
        actions.pushBack(TargetedAction::create(C, Sequence::create(FadeIn::create(1),
                                                                    createSoundAction("kacha.mp3"), NULL)));
        
        auto movePillar=((index==0&&DataManager::sharedManager()->getEnableFlagWithFlagID(7))||
                         (index==1&&DataManager::sharedManager()->getEnableFlagWithFlagID(6)));
        
        if (movePillar) {
            auto back=createSpriteToCenter("back_59.png", true, parent,true);
            SupplementsManager::getInstance()->addSupplementToMain(back, 59);
            auto pillar=createSpriteToCenter("sup_59_pillar.png", false, back,true);
            for (int i=0; i<2; i++) {
                createSpriteToCenter(StringUtils::format("sup_59_c_%d.png",i), false, pillar,true);
            }
            actions.pushBack(DelayTime::create(1));

            actions.pushBack(TargetedAction::create(back, FadeIn::create(1)));
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(movePillarAction(parent, pillar, 59, true));
            
            Warp warp;
            warp.warpID=59;
            warp.replaceType=ReplaceTypeNone;
            
            WarpManager::getInstance()->warps.push_back(warp);
        }
        
        actions.pushBack(DelayTime::create(1));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="donut"){
        //ドーナツめくり
        auto index=DataManager::sharedManager()->getTemporaryFlag("donut").asInt();
        auto distance=parent->getContentSize().height*.2;
        Vector<FiniteTimeAction*>actions;
        if (index<3) {
            Common::playSE("pop_short.mp3");
            auto donut=parent->getChildByName(StringUtils::format("sup_55_donut_%d.png",index));
            auto moves=TargetedAction::create(donut, Spawn::create(FadeOut::create(1),MoveBy::create(1, Vec2(0, distance)), NULL));
            actions.pushBack(Sequence::create(moves,
                                              CallFunc::create([index]{
                DataManager::sharedManager()->setTemporaryFlag("donut", index+1);
            }),NULL));
        }
        else{
            Vector<FiniteTimeAction*>spawns;
            for (int i=0; i<3; i++) {
                auto sup=parent->getChildByName(StringUtils::format("sup_55_donut_%d.png",i));
                spawns.pushBack(TargetedAction::create(sup, Spawn::create(FadeIn::create(.1),MoveTo::create(.1, parent->getContentSize()/2), NULL)));
            }
            actions.pushBack(Spawn::create(spawns));
            DataManager::sharedManager()->setTemporaryFlag("donut", 0);
        }

        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="cloud"){
        Vector<FiniteTimeAction*>actions_spawn;
        
        auto num=DataManager::sharedManager()->getNowBack();
        
        auto cloud_anchor=Vec2(322.0/600.0,440.0/660.0);
        if (num==42) {
            actions_spawn.pushBack(createSoundAction("pig.mp3"));
        }
        else if(num==24){
            cloud_anchor=Vec2(.7,.5);
            actions_spawn.pushBack(createSoundAction("pig.mp3"));
        }
        
        auto cloud=cloudAction(parent, StringUtils::format("sup_%d_cloud.png",num), cloud_anchor);
        actions_spawn.pushBack(cloud);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(Spawn::create(actions_spawn),call, NULL));
    }
    else if(key.compare(0,3,"hat")==0)
    {//シルクハットめくり
        Common::playSE("pop_short.mp3");
        auto index=atoi(key.substr(4,1).c_str());
        auto hat=parent->getChildByName(StringUtils::format("sup_61_hat_%d.png",index));

        Vector<FiniteTimeAction*>actions;
        
        
        auto passcode=AnswerManager::getInstance()->getPasscode().append(StringUtils::format("%d",index));
        if ((DataManager::sharedManager()->getBackData(61)["useInput"].asValueMap()["answer"].asValueMap()["answer"].asString()==passcode)&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(8)) {
            auto move=TargetedAction::create(hat, Sequence::create(EaseInOut::create(MoveBy::create(.5, Vec2(0, parent->getContentSize().height*.2)), 1.5),
                                                                   DelayTime::create(.5),
                                                                   NULL));
            
            actions.pushBack(move);
            auto bird=createSpriteToCenter("sup_61_bird.png", false, parent,true);
            auto move_bird=TargetedAction::create(bird, Spawn::create(JumpBy::create(.5, Vec2(0, -parent->getContentSize().height*.05), parent->getContentSize().height*.025, 2),
                                                                      ScaleBy::create(1, 1.2),
                                                                      createSoundAction("bird.mp3"), NULL));
            actions.pushBack(move_bird);
        }
        else{
            auto move=moveHatAction(hat,parent);
            
            actions.pushBack(move);
        }
        
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="bird")
    {//鳥をさしだす
        Common::playSE("p.mp3");
        auto pig=parent->getChildByName("sup_42_pig_0.png");
        auto stick=parent->getChildByName("sup_42_stick.png");
        auto pig_1=createSpriteToCenter("sup_42_pig_1.png", true, parent,true);
        auto pig_2=createSpriteToCenter("sup_42_pig_2.png", true, parent,true);

        auto item=createAnchorSprite("sup_42_bird.png",.25,.5, true, parent, true);
        auto give=giveAction(item, 1, .7, true);
        auto delay=DelayTime::create(1);
        auto change_1=Spawn::create(createChangeAction(1, .5, pig, pig_1, "pop.mp3"),
                                    TargetedAction::create(stick, FadeOut::create(1)), NULL);
        auto change_2=createChangeAction(1, .5, pig_1, pig_2, "pig.mp3");

        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(give,delay,change_1,delay->clone(),change_2,delay->clone(),call, NULL));
    }
    else if(key=="shake")
    {//shakeActionから呼び出し 3回振ったらおちる仕組み
        Common::playSE("pop_short.mp3");
        Vector<FiniteTimeAction*>actions;

        auto key_count="shakeCount";
        int count=DataManager::sharedManager()->getTemporaryFlag(key_count).asInt();
        count++;
        auto fall=(count==2);
        DataManager::sharedManager()->setTemporaryFlag(key_count, count);
        
        auto pig=parent->getChildByName("sup_11_pig_0.png");
        pig->stopAllActions();
        pig->setRotation(0);
        auto shake=TargetedAction::create(pig, Spawn::create(swingAction(20, .4, pig, 2,true),
                                                             createBounceAction(pig, .2, .1), NULL));
        actions.pushBack(shake);
        
        if (fall) {
            auto umbrella=createSpriteToCenter("sup_11_umbrella.png", true, parent,true);
            auto stick=createSpriteToCenter("sup_11_longstick.png", true, parent,true);
            auto pig_1=createSpriteToCenter("sup_11_pig_1.png", true, parent,true);

            auto fallAction=TargetedAction::create(pig, Sequence::create(EaseIn::create(RotateBy::create(.4, 20), 1.5),
                                                                         swingAction(5, .2, pig, 1.5,true,2),
                                                                         createSoundAction("pop_short.mp3"),
                                                                         Spawn::create(RotateBy::create(1, 25),
                                                                                       JumpBy::create(1, Vec2(parent->getContentSize().width*.2, -parent->getContentSize().height*.2), parent->getContentSize().height*.2, 1),
                                                                                       Sequence::create(DelayTime::create(.8),
                                                                                                        Spawn::create(createChangeAction(.2, .1, pig, pig_1,"hit.mp3"),
                                                                                                                      TargetedAction::create(umbrella, FadeIn::create(.5)),
                                                                                                                      TargetedAction::create(stick, FadeIn::create(.5)), NULL), NULL)
                                                                         , NULL),
                                                   NULL));
            actions.pushBack(fallAction);
            actions.pushBack(createSoundAction("pig_1.mp3"));
            actions.pushBack(DelayTime::create(2));
        }
        
        
        auto call=CallFunc::create([callback,fall,parent,this](){
            if (fall) {
                Common::playSE("pinpon.mp3");
                callback(true);
            }
            else{
                //falseを返す場合は例外的にここでタッチを有効化する
                this->backAction("rope", DataManager::sharedManager()->getNowBack(), parent);
                TouchManager::getInstance()->setIsManagingTouch(false);
                callback(false);
            }
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="ice")
    {//ice作成
        Common::playSE("p.mp3");
        auto item=createSpriteToCenter("sup_53_stick.png", true, parent, true);
        auto ice_1=createSpriteToCenter("sup_53_ice.png", true, parent,true);

        auto back=createSpriteToCenter("back_52.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 52, true);
        auto item_1=createSpriteToCenter("sup_52_stick_0.png", false, back,true);
        auto item_2=createSpriteToCenter("sup_52_stick_1.png", true, back,true);
        auto ice=createSpriteToCenter("sup_52_ice.png", true, back,true);

        auto fadein_item=TargetedAction::create(item, Sequence::create(FadeIn::create(1),
                                                                  createSoundAction("kacha.mp3"), NULL));
        auto delay=DelayTime::create(1);
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto fadein_ice_1=TargetedAction::create(ice_1, FadeIn::create(0));
        auto stick_action=createChangeAction(1, .7, item_1, item_2,"weeen.mp3");
        
        auto fadein_ice=TargetedAction::create(ice, FadeIn::create(2));
        auto fadeout_back=TargetedAction::create(back, FadeOut::create(1));
        auto call=CallFunc::create([callback,parent]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_item,delay,fadein_back,fadein_ice_1,delay->clone(),stick_action,fadein_ice,delay->clone(),fadeout_back,delay->clone(),call, NULL));
    }
    else if(key.compare(0,7,"balloon")==0)
    {//風船anim
        Common::playSE("p.mp3");
        auto index=atoi(key.substr(8,1).c_str());
        Vector<FiniteTimeAction*>actions;
        auto item=createSpriteToCenter(StringUtils::format("sup_9_balloon_%d.png",index), true, parent,true);
        
        auto fadein_item=TargetedAction::create(item, Sequence::create(FadeIn::create(1),
                                                                         createSoundAction("pop_short.mp3.mp3"), NULL));
        actions.pushBack(fadein_item);
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        
        if (index==1) {
            //豚よさらば
            auto back_7=createSpriteToCenter("back_7.png", true, parent,true);
            SupplementsManager::getInstance()->addSupplementToMain(back_7, 7, true);
            auto balloon_1=createSpriteToCenter("sup_7_balloon_1.png",false,back_7,true);
            auto pig_0=back_7->getChildByName("sup_7_pig_0.png");
            
            auto pig_1=createAnchorSprite("sup_7_pig_1.png", .5, 1, true, back_7, true);
            
            auto fadein_back=TargetedAction::create(back_7, FadeIn::create(1));
            actions.pushBack(fadein_back);
            actions.pushBack(delay->clone());
            
            auto change_pig=Spawn::create(TargetedAction::create(back_7->getChildByName("sup_7_balloon_0.png"), FadeOut::create(1)),
                                          TargetedAction::create(balloon_1, FadeOut::create(1)),
                                          createChangeAction(1, .5, pig_0, pig_1,"pop.mp3"),
                                          NULL) ;
            actions.pushBack(change_pig);
            actions.pushBack(delay->clone());
            
            auto distance=parent->getContentSize().width*.2;
            
            ccBezierConfig config;
            config.controlPoint_1=Vec2(-distance, parent->getContentSize().height*.25);
            config.controlPoint_2=Vec2(distance*2, parent->getContentSize().height*.5);
            config.endPosition=Vec2(0, parent->getContentSize().height*1);
            
            auto bezier=TargetedAction::create(pig_1, Spawn::create(EaseIn::create(BezierBy::create(6, config), 2),
                                                                    swingAction(2.5, .8, pig_1, 1.5,true,2), NULL));
            actions.pushBack(bezier);
            actions.pushBack(delay->clone());
        }

        auto call=CallFunc::create([callback,parent]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="openCurtain")
    {//カーテンをあける
        Common::playSE("sa.mp3");
        
        auto curtain=parent->getChildByName("sup_7_close.png");
        auto open=TargetedAction::create(curtain, FadeOut::create(.2));
        auto call=CallFunc::create([callback,parent]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(open,
                                           DelayTime::create(.2),
                                           call, NULL));
    }
    else if(key=="putA")
    {//トランプを置く
        Common::playSE("p.mp3");
        
        auto card=createSpriteToCenter(StringUtils::format("sup_43_A.png"), true, parent,true);
        auto card_after=createSpriteToCenter("sup_43_card_1.png", true, parent,true);
        auto fadein_card=TargetedAction::create(card, Sequence::create(FadeIn::create(1),
                                                                       createSoundAction("pop_short.mp3"), NULL));
        auto delay=DelayTime::create(1);
        
        auto back_42=createSpriteToCenter("back_42.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back_42, 42, true);
        createSpriteToCenter("sup_42_A.png", false, back_42,true);
        auto card_1=createSpriteToCenter("sup_42_card_1.png", true, back_42,true);
        auto pig=back_42->getChildByName("sup_42_pig_0.png");
        auto pig_3=createSpriteToCenter("sup_42_pig_3.png", true, back_42,true);
        auto pig_4=createSpriteToCenter("sup_42_pig_4.png", true, back_42,true);
        pig_4->setLocalZOrder(2);

        auto fadein_back=TargetedAction::create(back_42, FadeIn::create(1));
        auto fadein_after=TargetedAction::create(card_after, FadeIn::create(0));
        auto fadein_pig_3=createChangeAction(1, .5, pig,pig_3, "pig_3,pop_short.mp3");
        auto drum=createSoundAction("drumroll.mp3");
        auto delay_2=DelayTime::create(3.5);
        auto fadein_card_1=TargetedAction::create(card_1, FadeIn::create(0));
        auto change_pig=createChangeAction(1, .5, pig_3, pig_4, "pig.mp3");
        auto fadeout_back=TargetedAction::create(back_42, FadeOut::create(1));
        auto call=CallFunc::create([callback,parent]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_card,delay,fadein_back,fadein_after,delay->clone(),fadein_pig_3,drum,delay_2,fadein_card_1,change_pig,delay->clone(),fadeout_back,delay->clone(),call, NULL));
    }
    else if(key=="apple")
    {//りんごをさしだす
        Common::playSE("p.mp3");
        auto pig=parent->getChildByName("sup_24_pig_0.png");
        auto pig_1=createSpriteToCenter("sup_24_pig_1.png", true, parent,true);//りんごを受け取るアニメ
        auto open=createSpriteToCenter("sup_24_open_0.png", true, parent,true);//豚入り
        open->setLocalZOrder(1);
        auto pig_2=createSpriteToCenter("sup_24_pig_2.png", true, parent,true);//ごそごそ
        auto arm=createAnchorSprite("sup_24_arm.png", .85, .38, true, parent, true);//けんさす手
        arm->setLocalZOrder(1);
        auto pig_3=createSpriteToCenter("sup_24_pig_3.png", true, parent,true);//けんさす
        pig_3->setLocalZOrder(1);
        auto pig_4=createSpriteToCenter("sup_24_pig_4.png", true, parent,true);//finish
        pig_4->setLocalZOrder(2);
        Vector<FiniteTimeAction*>actions;
        
        auto item=createAnchorSprite("sup_24_apple.png", .9, .5, true, parent, true);
        auto give=giveAction(item, 1, .7, true);
        actions.pushBack(give);
        actions.pushBack(createSoundAction("pig.mp3"));
        actions.pushBack(createBounceAction(pig, .2,.1));
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        auto change_1=createChangeAction(1, .5, pig, pig_1, "pop.mp3");
        actions.pushBack(change_1);
        actions.pushBack(delay->clone());
        
        auto change_2=createChangeAction(1, .5, pig_1, pig, "pop_short.mp3");
        actions.pushBack(change_2);
        actions.pushBack(delay->clone());
        
        auto openAction=TargetedAction::create(open, Spawn::create(FadeIn::create(1),
                                                                   createSoundAction("gacha.mp3"), NULL));
        auto closeAction=TargetedAction::create(open, Spawn::create(FadeOut::create(.3),
                                                                   createSoundAction("gacha.mp3"), NULL));
        actions.pushBack(openAction);
        actions.pushBack(createSoundAction("pig_1.mp3"));
        actions.pushBack(DelayTime::create(2));
        actions.pushBack(closeAction);
        
        auto change_3=createChangeAction(1, .5, pig, pig_2, "pop.mp3");
        actions.pushBack(change_3);
        actions.pushBack(delay->clone());

        auto change_4=Spawn::create(createChangeAction(1, .5, pig_2, pig_3, "pop.mp3"),
                                    TargetedAction::create(arm, FadeIn::create(1)), NULL);
        actions.pushBack(change_4);
        actions.pushBack(delay->clone());
        
        //けんさすぞ
        auto cover=createSpriteToCenter("sup_24_cover.png", true, parent,true);
        cover->setLocalZOrder(1);
        actions.pushBack(TargetedAction::create(cover, FadeIn::create(.1)));
        auto angle=30;
        for (int i=0; i<5; i++) {
            if (i>2) {
                auto move=MoveBy::create(.5, Vec2(0, -parent->getContentSize().height*.05));
                actions.pushBack(Spawn::create(TargetedAction::create(arm, move),
                                               TargetedAction::create(pig_3, move->clone()), NULL));
            }
            auto sword=createSpriteToCenter(StringUtils::format("sup_24_sword_%d.png",i), true, parent,true);
            sword->setPositionX(parent->getContentSize().width);
            auto seq_sword=TargetedAction::create(sword, Sequence::create(FadeIn::create(1),
                                                                          createSoundAction("throw.mp3"),
                                                                          Spawn::create(EaseInOut::create(MoveTo::create(1, parent->getContentSize()/2), 1.5),
                                                                                        swingAction(angle, .3, arm, 1.5), NULL),
                                                                          createSoundAction("sword_1.mp3"), NULL));
            actions.pushBack(seq_sword);
            
        }
        
        actions.pushBack(DelayTime::create(2));
        
        auto change_5=Spawn::create(createChangeAction(1, .5, pig_3, pig_4, "pop.mp3"),
                                    TargetedAction::create(arm, FadeOut::create(1)), NULL);
        actions.pushBack(change_5);
        actions.pushBack(delay->clone());

        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="nankin")
    {
        Common::playSE("p.mp3");
        
        Vector<FiniteTimeAction*>actions;
        auto key_0=createSpriteToCenter("sup_12_key_0.png", true, parent,true);
        auto key_1=createSpriteToCenter("sup_12_key_1.png", true, parent,true);
        auto fadein_0=TargetedAction::create(key_0, Sequence::create(FadeIn::create(1),
                                                                     createSoundAction("kacha.mp3"), NULL));
        actions.pushBack(fadein_0);
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        auto fadein_1=Sequence::create(createChangeAction(1, .5, key_0, key_1),
                                                                     createSoundAction("key_1.mp3"), NULL);
        actions.pushBack(fadein_1);
        actions.pushBack(delay->clone());
        
        auto back=createSpriteToCenter("back_11.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 11, true);
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        actions.pushBack(fadein);
        actions.pushBack(delay->clone());
        auto chain=back->getChildByName("sup_11_chain.png");
        actions.pushBack(TargetedAction::create(chain, Spawn::create(FadeOut::create(1),
                                                                     createSoundAction("kacha_2.mp3"), NULL)));
        auto open=createSpriteToCenter("sup_11_0_1.png", true, back,true);
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            createSpriteToCenter("sup_11_mistake.png", false, open,true);
        }
        else{
            createSpriteToCenter("sup_11_bag.png", false, open,true);
        }
        actions.pushBack(TargetedAction::create(open, FadeIn::create(1)));
        actions.pushBack(delay->clone());

        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="pin"){
        Common::playSE("p.mp3");
        auto balloon=parent->getChildByName("sup_35_balloon.png");
        auto pin=createSpriteToCenter("sup_35_pin.png", true, parent, true);
        pin->setLocalZOrder(1);
        auto pig=parent->getChildByName("sup_35_pig.png");
        auto lid=parent->getChildByName("sup_35_lid.png");
        auto cake=createSpriteToCenter("sup_35_cake.png", true, parent,true);
        cake->setLocalZOrder(1);
        auto pig_anim=createSpriteToCenter("sup_35_pig_arm.png", true, parent,true);
        pig_anim->setLocalZOrder(1);
        
        auto seq_pin=TargetedAction::create(pin, Sequence::create(FadeIn::create(1),
                                                                  DelayTime::create(1),
                                                                  Spawn::create(jumpAction(Vec2(parent->getContentSize().width*.07, parent->getContentSize().height*.07), .1, pin, 2),
                                                                                Sequence::create(DelayTime::create(.1),
                                                                                                 CallFunc::create([balloon]{
                                                                                    balloon->stopAllActions();
                                                                                    Common::stopAllSE();
                                                                                }),
                                                                                                 createSoundAction("pan.mp3"),
                                                                                                 TargetedAction::create(balloon, RemoveSelf::create()), NULL), NULL),
                                                                  DelayTime::create(1),
                                                                  FadeOut::create(1),
                                                                  NULL)
                                            );
        auto delay=DelayTime::create(1);
        auto seq_pig=TargetedAction::create(pig, Sequence::create(createSoundAction("pig.mp3"),
                                                                  createBounceAction(pig, .25, .05),
                                                                  delay->clone(),
                                                                  Spawn::create(createChangeAction(1, .5, lid, cake,"pop_short.mp3"),
                                                                                TargetedAction::create(pig_anim, FadeIn::create(.5)), NULL),
                                                                  delay->clone(),
                                                                  TargetedAction::create(pig_anim, FadeOut::create(1)),
                                                                  NULL));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_pin,delay,seq_pig,delay->clone(),call, NULL));
    }
    else if(key=="hammer"){
        Common::playSE("p.mp3");
        auto hammer=createAnchorSprite("sup_5_hammer.png", .83, .24, true, parent, true);
        hammer->setLocalZOrder(1);
        auto distance=parent->getContentSize().height*.1;
        auto angle=20;
        
        auto seq_hammer=TargetedAction::create(hammer, Sequence::create(FadeIn::create(1),
                                                                        DelayTime::create(1),
                                                                        EaseInOut::create(Spawn::create(MoveBy::create(1, Vec2(0, distance)),
                                                                                                        RotateBy::create(1, angle),
                                                                                                        createSoundAction("swing_0.mp3"), NULL), 1.5),
                                                                        DelayTime::create(.5),
                                                                        EaseIn::create(Spawn::create(MoveBy::create(.2, Vec2(0, -distance)),
                                                                                                     RotateTo::create(.2, 0), NULL), 1.5),
                                                                        createSoundAction("hit.mp3"),
                                                                        EaseOut::create(Spawn::create(MoveBy::create(.2, Vec2(0, distance/2)),
                                                                                                      RotateBy::create(.2, angle), NULL), 1.5),
                                                                        FadeOut::create(1),
                                                                        NULL));
        
        auto gauge=hammerGaugeAction(parent, 5, true);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(seq_hammer,gauge,call, NULL));
    }
    else if(key=="longstick"){
        Common::playSE("p.mp3");
        Vector<FiniteTimeAction*>actions;
        auto paper=parent->getChildByName("sup_76_paper.png");
        auto stick=createSpriteToCenter("sup_76_stick_0.png", true, parent,true);
        auto stick_1=createSpriteToCenter("sup_76_stick_1.png", true, parent,true);
        auto fadein_0=TargetedAction::create(stick, FadeIn::create(1));
        actions.pushBack(fadein_0);
        actions.pushBack(DelayTime::create(1));
        auto fadein_1=Spawn::create(createChangeAction(1, .5, stick, stick_1),
                                    TargetedAction::create(paper, FadeOut::create(1)), NULL);
        actions.pushBack(fadein_1);
        actions.pushBack(DelayTime::create(1));
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="water"){
        Common::playSE("p.mp3");
        auto handle_0=createSpriteToCenter("sup_54_handle_0.png", true, parent,true);
        auto handle_1=createSpriteToCenter("sup_54_handle_1.png", true, parent,true);

        auto fadein_0=TargetedAction::create(handle_0, FadeIn::create(1));
        
        auto twist=Repeat::create(Sequence::create(createChangeAction(.3, .1, handle_0, handle_1,"twist.mp3"),
                                                   DelayTime::create(.3),
                                                   createChangeAction(.3, .1, handle_1, handle_0),
                                                   DelayTime::create(.3), NULL), 2);
        
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_0,DelayTime::create(1),twist,DelayTime::create(.5),call,NULL));
    }
    else if(key.compare(0,6,"putBag")==0){
        Common::playSE("p.mp3");
        auto index=atoi(key.substr(7,1).c_str());
        Vector<FiniteTimeAction*>actions;
        auto putbag=TargetedAction::create(createSpriteToCenter(StringUtils::format("sup_79_bag_%d.png",index), true, parent,true), Sequence::create(FadeIn::create(1),createSoundAction("bohu.mp3"),DelayTime::create(1), NULL));
        actions.pushBack(putbag);
        
        if (index==2) {
            actions.pushBack(DelayTime::create(1));
            
            auto back=createSpriteToCenter("back_77.png", true, parent,true);
            SupplementsManager::getInstance()->addSupplementToMain(back, 77, true);
            createSpriteToCenter("sup_77_bag_2.png", false, back,true);
            
            auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
            actions.pushBack(fadein_back);
            
            actions.pushBack(DelayTime::create(1));

            auto open=createSpriteToCenter("sup_77_0_1.png", true, parent,true);
            createSpriteToCenter("sup_77_elephant.png", false, open,true);
            auto openAction=TargetedAction::create(open, Spawn::create(FadeIn::create(1),
                                                                       createSoundAction("gacha.mp3"), NULL));
            actions.pushBack(openAction);
            
            actions.pushBack(DelayTime::create(1));

        }
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="clear")
    {
        auto addStory=CallFunc::create([parent]{
            auto story=StoryLayer::create("ending", [parent]{
                EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

                auto story=StoryLayer::create("ending_1", [parent]{
                    Director::getInstance()->replaceScene(TransitionFade::create(8, ClearScene::createScene(), Color3B::WHITE));
                });
                Director::getInstance()->getRunningScene()->addChild(story);
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        parent->runAction(Sequence::create(addStory, NULL));
    }
    else if(key=="back_11")
    {
        Vector<FiniteTimeAction*>actions;
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            auto mistake=parent->getChildByName("sup_11_mistake.png");
            if (DataManager::sharedManager()->getTemporaryFlag("switch_sup_11_0").asInt()==0) {
                mistake->setOpacity(0);
            }
            else{
                mistake->setOpacity(255);
            }
        }
        
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="treasure")
    {
        auto call=CallFunc::create([callback]{
            auto story=StoryLayer::create("get", [callback]{
                callback(true);
            });
            
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        parent->runAction(call);
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void CircusActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造 addする際はcreateSpriteOnClipを使用するとよい
    if (key.compare(0,7,"battery")==0) {
        auto index=atoi(key.substr(8,1).c_str());
        auto item=createSpriteToCenter(StringUtils::format("switch_device_sup_%d.png",index), true, parent->itemImage,true);
        
        auto fadein=TargetedAction::create(item, Sequence::create(FadeIn::create(.5),
                                                                  createSoundAction("kacha.mp3"),
                                                                  DelayTime::create(1), NULL));
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,call, NULL));
    }
    else if(key=="stage_open"){
        Vector<FiniteTimeAction*>actions;
        auto item=createSpriteToCenter("switch_device_sup_2.png", true, parent->itemImage,true);
        auto fadein=TargetedAction::create(item, Sequence::create(FadeIn::create(.5),
                                                                  createSoundAction("kacha.mp3"),
                                                                  DelayTime::create(1), NULL));
        actions.pushBack(fadein);
        
        auto back=createSpriteToCenter("back_1.png", true, Director::getInstance()->getRunningScene(),true);
        back->setScale(DataManager::sharedManager()->getMainSpriteRect().size.width/back->getContentSize().width);
        back->setPositionY(DataManager::sharedManager()->getMainSpriteRect().origin.y+DataManager::sharedManager()->getMainSpriteRect().size.height*.5);
        back->setLocalZOrder(parent->getLocalZOrder());
        

        SupplementsManager::getInstance()->addSupplementToMain(back, 1, true);
        actions.pushBack(TargetedAction::create(back, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(TargetedAction::create(parent, RemoveSelf::create()));

        auto open=createSpriteToCenter("sup_1_open.png", true, back,true);
        createSpriteToCenter("sup_1_chain.png", false, open,true);
        createSpriteToCenter("sup_1_pig_rope_0.png", false, open,true);
        actions.pushBack(TargetedAction::create(open, Spawn::create(FadeIn::create(1),
                                                                    createSoundAction("spotlight_0.mp3"), NULL)));
        actions.pushBack(DelayTime::create(1));
        if (DataManager::sharedManager()->getNowBack()!=1) {
            actions.pushBack(TargetedAction::create(back, FadeOut::create(1)));
            actions.pushBack(DelayTime::create(1));
        }
        else{
            Warp warp;
            warp.replaceType=ReplaceTypeNone;
            warp.warpID=1;
            WarpManager::getInstance()->warps.push_back(warp);
        }
        
        auto call=CallFunc::create([callback,back]{
            back->removeFromParent();
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        Director::getInstance()->getRunningScene()->runAction(Sequence::create(actions));
    }
    else if(key=="device")
    {
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(1));
        auto item=createSpriteToCenter("open_device.png", true, parent->itemImage,true);
        auto fadein=TargetedAction::create(item, Sequence::create(FadeIn::create(.5),
                                                                  createSoundAction("kacha.mp3"),
                                                                  DelayTime::create(1), NULL));
        actions.pushBack(fadein);

        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        Director::getInstance()->getRunningScene()->runAction(Sequence::create(actions));
    }
}

#pragma mark - Story
void CircusActionManager::storyAction(std::string key, cocos2d::Node *parent, const onFinished &callback)
{
    if (key=="run") {
        auto pig=parent->getChildByName("pigpolice.png");
        
        parent->runAction(Sequence::create(DelayTime::create(1),
                                  createSoundAction("run.mp3"),
                                  TargetedAction::create(pig, MoveBy::create(.5, Vec2(-parent->getContentSize().width*1.5, 0))),
                                  CallFunc::create([callback]{
            callback(true);
        }), NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark - Custom
void CircusActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    if (key==CustomAction_ReviewLayer) {
        DataManager::sharedManager()->removeItemWithID(27);
    }
    else if(key==CustomAction_Clear)
    {
        Vector<FiniteTimeAction*>actions;
        auto balloon=createSpriteToCenter(StringUtils::format("room_image_balloon.png"), false, parent,true);
        auto seq=swingAction(Vec2(0, parent->getContentSize().height*.1), 2, balloon, 1.5, false, -1);
        actions.pushBack(seq);
        
        parent->runAction(Spawn::create(actions));
    }
}

#pragma mark - Show Answer
void CircusActionManager::showAnswerAction(std::string key, cocos2d::Node *parent)
{
    if (key=="hat") {
        
        auto answer=DataManager::sharedManager()->getBackData(61)["useInput"].asValueMap()["answer"].asValueMap()["answer"].asString();
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(1));
        for (int i=0; i<answer.size(); i++) {
            auto index=atoi(answer.substr(i,1).c_str());
            auto hat=parent->getChildByName(StringUtils::format("sup_61_hat_%d.png",index));
            auto action=moveHatAction(hat,parent);
            actions.pushBack(action);
            actions.pushBack(DelayTime::create(.5));
        }
        actions.pushBack(DelayTime::create(1));
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
}

#pragma mark - Private
FiniteTimeAction* CircusActionManager::moveHatAction(Node *node, Node *parent)
{
    auto distance=parent->getContentSize().height*.2;

    return TargetedAction::create(node, Sequence::create(EaseInOut::create(MoveBy::create(.5, Vec2(0, distance)), 1.5),
                                                 DelayTime::create(.5),
                                                        EaseInOut::create(MoveBy::create(.5, Vec2(0, -distance)), 1.5), NULL));
}

FiniteTimeAction* CircusActionManager::movePillarAction(cocos2d::Node *parent, cocos2d::Node *pillar, int backID, bool animate)
{
    //柱を移動
    float posX=parent->getContentSize().width*.2;
    if (backID==4) {
        posX=parent->getContentSize().width*.425;
    }
    
    if (animate) {
        return TargetedAction::create(pillar, Spawn::create(EaseInOut::create(MoveTo::create(1, Vec2(posX, pillar->getPositionY())), 1.5),
                                                            createSoundAction("zuzuzu.mp3"), NULL));
    }
    else{
        pillar->setPositionX(posX);
        return nullptr;
    }
}

FiniteTimeAction* CircusActionManager::hammerGaugeAction(cocos2d::Node *parent, int backID, bool animate)
{
    int count=18;
    if (animate) {
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<count; i++) {
            if (i%2==0) {
                actions.pushBack(createSoundAction("pi.mp3"));
            }
            auto gauge=createSpriteToCenter(StringUtils::format("sup_%d_%d.png",backID,i), true, parent,true);
            actions.pushBack(TargetedAction::create(gauge, Spawn::create(FadeIn::create(.05), NULL)));
        }
        return Sequence::create(actions);

    }
    else{
        for (int i=0; i<count; i++) {
            createSpriteToCenter(StringUtils::format("sup_%d_%d.png",backID,i), false, parent,true);
        }
        
        return nullptr;
    }
}
