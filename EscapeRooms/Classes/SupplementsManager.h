//
//  SupplementsManager.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/01/17.
//

#ifndef _____PROJECTNAMEASIDENTIFIER________SupplementsManager_____
#define _____PROJECTNAMEASIDENTIFIER________SupplementsManager_____

#include "cocos2d.h"
#include "CountUpLabel.h"
#include "SwitchSprite.h"
#include "RotateSprite.h"
#include "PatternSprite.h"
#include "SliderSprite.h"
#include "OrderSprite.h"
#include "DragSprite.h"
#include "ChangingSprite.h"
#include "CustomActionManager.h"
#include "ShowType.h"
#include <spine/spine-cocos2dx.h>
#include "FingerSprite.h"

USING_NS_CC;
typedef std::function<void(bool success)>actionCallback;



typedef enum{
    SupAnimateType_Add=0,
    SupAnimateType_Flash,
    SupAnimateType_FadeOut,
    SupAnimateType_SwingRotate
}SupAnimateType;

class SupplementsManager
{
public:
    static SupplementsManager* manager;
    static SupplementsManager* getInstance();
    
    void addSupplementToMain(Node*parent,int backNum);
    /**全種類のsupplementをadd*/
    void addSupplementToMain(Node*parent,int backNum,bool addAllsup);
    void addSupplementToMain(Node*parent,int backNum,bool addAllsup,ShowType showType);

    void addAnchorSpritesToMain(Node*parent,ValueMap backMap);
    void addSupplementToPopUp(Node*parent,int itemID,int angle,bool addAllsup,ShowType showType);
    
#pragma mark Item
    /**アイテムボックスを作成*/
    Sprite* createItemBox(int itemID);
    
#pragma mark Common
    Sprite* addSupplementToCenter(Node*parent , std::string name,bool invisible,bool addChild);
    void transformToAnchorSprite(Node *parent, Node *targetNode, Vec2 anchorPoint);
#pragma mark Spine
    void addSpineSupplement(Node*parent,ValueMap backMap,ShowType showType);
    spine::SkeletonAnimation* addSpine(Node* parent,std::string name);
    
#pragma mark ClippingNode
    void addClippingNode(Node*parent,bool onItem,ValueMap backMap,ShowType showType);
    
#pragma mark Input
    void addInputSupplement(Node*parent,bool onItem,ValueMap backMap,ShowType showType);
    void addInputSupplement(Node*parent,bool onItem,int angle,ValueMap backMap,ShowType showType);
    void addInputRotateSupplement(Node*parent,bool onItem,ValueMap backMap,ShowType showType);

#pragma mark Changing
    Vector<ChangingSprite*>changingSprites;

    void addChangingSupplement(Node*parent,bool onItem,ValueMap backMap,ShowType showType);
#pragma mark Label
    Vector<CountUpLabel*>labelSupplements;
    Vector<CountUpLabel*>labelSupplements_onItem;

    void addLabelSupplement(Node*parent,bool onItem,ValueMap backMap,ShowType showType);
    void addLabelSupplement(Node*parent,bool onItem,int angle,ValueMap backMap,ShowType showType);
    /**labelに指定文字を強制セットする(showType=Answerの時のみ)*/
    void addLabelSupplement(Node*parent,bool onItem,int angle,ValueMap backMap,ShowType showType,std::vector<std::string>strings);

#pragma mark Switch
    Vector<SwitchSprite*>switchSprites;
    Vector<SwitchSprite*>switchSprites_onItem;

    void addSwitchSupplements(Node*parent,bool onItem,ValueMap backMap,ShowType showType);
    void addSwitchSupplements(Node*parent,bool onItem,int angle,ValueMap backMap,ShowType showType);

#pragma mark Rotate
    Vector<RotateSprite*>rotateSprites;
    void addRotateSupplements(Node*parent,bool onItem,ValueMap backMap,ShowType showType);
#pragma mark Slider
    Vector<SliderSprite*>sliderSprites;
    void addSliderSupplements(Node*parent,bool onItem,ValueMap backMap,ShowType showType);
#pragma mark Order
    Vector<OrderSprite*>orderSprites;
    void addOrderSupplements(Node*parent,bool onItem,ValueMap backMap,ShowType showType);

#pragma mark Drag
    Vector<DragSprite*>dragSprites;
    void addDragSupplements(Node*parent,bool onItem,ValueMap backMap,ShowType showType);

#pragma mark Pattern
    Vector<PatternSprite*>patternSprites;
    Vector<PatternSprite*>patternSprites_onItem;

    /**Patternはドラッグのタイミングで生成されるため、遷移時などにclear()してあげる*/
    void clearPatternSprites(bool onItem);
    void addPatternSprite(Node*parent,bool onItem,ValueMap backMap,ShowType showType);
#pragma mark Mystery
    void addMysterySup(Node*parent,bool onItem,int ID);
    void correctAction(Node* parent,bool onItem,int ID,const onFinished& callback);
    ValueMap getMysterySupMap(int mysteryID,bool onItem,int ID);
#pragma mark Particle
    Vector<ParticleSystemQuad*>particles;
    void addParticle(Node*parent,int backID,bool onItem,ShowType showType);
    ParticleSystemQuad* getParticle(Node*parent,ValueMap particleMap);
    ParticleSystemQuad* getParticleFromPlist(std::string fileName);

#pragma mark Panel
    std::vector<std::string>showPanelIDs;
    void showPanel(Node*parent);
    void showPanel(Node*parent,int num);
    void hidePanel();
    
#pragma mark Finger
    std::string fingerName="finger";
    /**animateがtrueの時 pointAnimateを呼び出す*/
    FingerSprite* addFinger(Node* parent,Vec2 vec,bool animate);


#pragma mark Supplement Animate
    /**指定の補完画像をアニメーション*/
    void animateSupplement(Node*parent,std::string fileName,SupAnimateType type,float duration,float value,ccMenuCallback callback);
    FiniteTimeAction* swingAction(Node*node,float angle,float duration,std::string soundName);
#pragma mark Layer
    LayerColor* createDisableLayer();
    
#pragma mark BackAction
    void customBackAction(Node*parent,int backID,std::string key,ShowType showType);
    void customTouchAction(Node*parent,std::string key,const actionCallback&callback);
    CustomActionManager* getCustomActionManager();
    CustomActionManager* getCustomActionManager(std::string title);
    
#pragma mark 汎用
    /**補完画像を表示して良いか*/
    bool checkAddable(ValueMap map);
    bool checkAddable(ValueMap map,int angle);
    /**ShowTypeに応じてkeyをappend*/
    std::string appendKey(std::string key,ShowType type);
    /**Debug用Spriteを表示*/
    void addDebugSprite(Node*parent,Rect rect);
private:
    
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
