//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "GuestHouseActionManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"

using namespace cocos2d;

GuestHouseActionManager* GuestHouseActionManager::manager =NULL;

GuestHouseActionManager* GuestHouseActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new GuestHouseActionManager();
    }
    return manager;
}

#pragma mark - Back
void GuestHouseActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="light"&&DataManager::sharedManager()->getEnableFlagWithFlagID(25))
    {        
        Vector<FiniteTimeAction*>actions;
        std::vector<int>nums={3,4,0,5,6,1,2};
        for (auto i:nums) {
            auto light=Sprite::createWithSpriteFrameName(StringUtils::format("sup_%d_%d.png",backID,i));
            light->setOpacity(0);
            light->setPosition(parent->getContentSize()/2);
            parent->addChild(light);
            
            auto flash=TargetedAction::create(light,Sequence::create(EaseOut::create(FadeIn::create(.15), 1.5) ,EaseIn::create(FadeOut::create(.15), 1.5), NULL));

            actions.pushBack(flash);
        }
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),Sequence::create(actions), NULL),UINT_MAX));
    }
    else if(key=="cat"){
        Vector<FiniteTimeAction*>actions;
        if (backID==7) {
            std::vector<Vec2>vecs={Vec2(.15, .62),Vec2(.29, .62),Vec2(.566, .62),Vec2(.845, .62)};
            for (int i=0; i<4; i++) {
                auto spr_0=Sprite::createWithSpriteFrameName(StringUtils::format("sup_7_%d_0.png",i));
                spr_0->setPosition(parent->getContentSize()/2);
                parent->addChild(spr_0);
                
                auto spr_1=Sprite::createWithSpriteFrameName(StringUtils::format("sup_7_%d_1.png",i));
                spr_1->setPosition(parent->getContentSize()/2);
                spr_1->setOpacity(0);
                parent->addChild(spr_1);
                
                auto voice=AnchorSprite::create(vecs.at(i), parent->getContentSize(), StringUtils::format("sup_7_voice_%d.png",i));
                voice->setOpacity(0);
                parent->addChild(voice);
                
                //animation
                auto original_scale=voice->getScale();
                auto duration=1.0;
                
                voice->setScale(.1);
                
                auto change=Spawn::create(TargetedAction::create(spr_0, FadeOut::create(duration)),
                                          TargetedAction::create(spr_1, FadeIn::create(duration*.5)),
                                          TargetedAction::create(voice, Spawn::create(FadeIn::create(duration/2),ScaleTo::create(duration/2, original_scale), NULL)),
                                          CallFunc::create([]{Common::playSE("cat_0.mp3");}), NULL);
                
                auto change_1=Spawn::create(TargetedAction::create(spr_0, FadeIn::create(duration*.5)),
                                            TargetedAction::create(spr_1, FadeOut::create(duration)),
                                            TargetedAction::create(voice, Spawn::create(FadeOut::create(duration/2),ScaleTo::create(duration/2, .1), NULL)),
                                            NULL);
                
                actions.pushBack(Sequence::create(change,DelayTime::create(.5),change_1,DelayTime::create(1), NULL));
            }
        }
        else{
            for (int i=0; i<4; i++) {
                auto duration=1.0;

                auto spr_0=Sprite::createWithSpriteFrameName(StringUtils::format("sup_6_%d.png",i));
                spr_0->setPosition(parent->getContentSize()/2);
                parent->addChild(spr_0);
                
                auto change=TargetedAction::create(spr_0, Sequence::create(FadeIn::create(duration),
                                                                           FadeOut::create(duration), NULL));
                actions.pushBack(change);
            }
        }
        
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),actions.at(0),actions.at(2),actions.at(3),actions.at(1),actions.at(0)->clone(),actions.at(2)->clone(), NULL),UINT_MAX));
    }
    else if(key=="map"){
        if(DataManager::sharedManager()->getEnableFlagWithFlagID(34)){
            //map animate
            auto layer=LayerColor::create(Color4B(0, 0, 0, 0));
            layer->setLocalZOrder(999);
            
            auto listner=EventListenerTouchOneByOne::create();
            listner->setSwallowTouches(true);//透過させない
            listner->onTouchBegan=[this](Touch*touch,Event*event)
            {
                return true;
            };
            
            Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listner, layer);
            
            Director::getInstance()->getRunningScene()->addChild(layer);
            
            //draw
            auto node=DrawNode::create();
            parent->addChild(node);
            
            auto width=parent->getContentSize().width*.012;
            auto color=Color4F(.2, .2, .8, 1);

            //move lambda
            auto actions=[node,width,color](Vec2 _preV,float distance_x,float distance_y,int count){
                Vector<FiniteTimeAction*>actions;
                //上に移動
                for(int i=0;i<count;i++){
                    auto preV=Vec2(_preV.x+distance_x/count*i, _preV.y+distance_y/count*i);
                    auto endV=Vec2(preV.x+distance_x/count, preV.y+distance_y/count);
                    
                    auto delay=DelayTime::create(.05);
                    auto call=CallFunc::create([node,preV,endV,width,color]{
                        node->drawSegment(preV, endV,width, color);
                    });
                    
                    auto seq=Sequence::create(delay,call, NULL);
                    actions.pushBack(seq);
                }
                
                return Sequence::create(actions);
            };
            
            
            //
            auto sound=CallFunc::create([]{Common::playSE("pinpon.mp3");});
            //上に移動
            auto pos_0=Vec2(parent->getContentSize().width*.2275, parent->getContentSize().height*.24);
            auto distance_y_0=parent->getContentSize().height*.11;
            auto seq_0=actions(pos_0,0,distance_y_0,20);
            //右に移動
            auto pos_1=Vec2(pos_0.x, pos_0.y+distance_y_0);
            auto distance_x_1=parent->getContentSize().width*.36;
            auto seq_1=actions(pos_1,distance_x_1,0,40);
            //上に移動
            auto pos_2=Vec2(pos_1.x+distance_x_1, pos_1.y);
            auto distance_y_2=parent->getContentSize().height*.16;
            auto seq_2=actions(pos_2,0,distance_y_2,20);
            //右に移動
            auto pos_3=Vec2(pos_2.x, pos_2.y+distance_y_2);
            auto distance_x_3=distance_x_1/2;
            auto seq_3=actions(pos_3,distance_x_3,0,20);
            
            //上に移動
            auto pos_4=Vec2(pos_3.x+distance_x_3, pos_3.y);
            auto distance_y_4=parent->getContentSize().height*.16;
            auto seq_4=actions(pos_4,0,distance_y_4,20);
            
            //終点
            auto pos_5=Vec2(pos_4.x, pos_4.y+distance_y_4);
            //
            auto call=CallFunc::create([layer]{
                layer->removeFromParent();
                Common::playSE("pinpon.mp3");
                DataManager::sharedManager()->setEnableFlag(35);
            });
            
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(35)) {
                layer->removeFromParent();
                node->drawSegment(pos_0, pos_1,width, color);
                node->drawSegment(pos_1, pos_2,width, color);
                node->drawSegment(pos_2, pos_3,width, color);
                node->drawSegment(pos_3, pos_4,width, color);
                node->drawSegment(pos_4, pos_5,width, color);
            }
            else{
                parent->runAction(Sequence::create(DelayTime::create(.5),sound,seq_0,seq_1,seq_2,seq_3,seq_4,DelayTime::create(.5),call, NULL));
            }
        }
        else{
            //no network
            auto spr=Sprite::createWithSpriteFrameName("sup_16_error.png");
            spr->setPosition(parent->getContentSize()/2);
            spr->setOpacity(0);
            parent->addChild(spr);
            
            auto scale_0=ScaleBy::create(.5, .1);
            auto appear=Spawn::create(EaseIn::create(ScaleBy::create(.3, 10),2),
                                      FadeIn::create(.2), NULL);
            
            spr->runAction(Sequence::create(scale_0,appear, NULL));
        }
    }
    else if(key=="clear"){
        if(!DataManager::sharedManager()->isPlayingMinigame())
        {
            auto storyLayer = StoryLayer::create(StoryKey_Ending, [this](Ref* sen){
                EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

                auto story_1=StoryLayer::create(StoryKey_Ending2, [this](Ref* sen){
                    Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
                });
                Director::getInstance()->getRunningScene()->addChild(story_1);
            });
            Director::getInstance()->getRunningScene()->addChild(storyLayer);
        }
    }
    else if(key=="mouse"&&DataManager::sharedManager()->isPlayingMinigame()){
        auto cover=Sprite::createWithSpriteFrameName("sup_46_cover.png");
        cover->setPosition(parent->getContentSize()/2);
        parent->addChild(cover);
        
        auto mouse=parent->getChildByName("sup_46_mistake.png");
        mouse->setOpacity(0);
        
        auto delay=DelayTime::create(1);
        auto fadein=TargetedAction::create(mouse, FadeIn::create(.1));
        auto move=TargetedAction::create(mouse,EaseIn::create(MoveBy::create(1, Vec2(parent->getContentSize().width*.1, 0)), 1.5));
        auto call=CallFunc::create([]{
            DataManager::sharedManager()->setTemporaryFlag("mouse", 1);
        });
        
        parent->runAction(Sequence::create(delay,fadein,move,call, NULL));
    }
}

#pragma mark - Touch
void GuestHouseActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key.compare(0,7,"reserve")==0)
    {
        Common::playSE("kacha.mp3");
        auto chara=key.substr(8,1);
        auto length_now=AnswerManager::getInstance()->getPasscode().size();
        auto correct=false;
        
        auto spr=Sprite::createWithSpriteFrameName(StringUtils::format("sup_15_selected_%s.png",chara.c_str()));
        spr->setPosition(parent->getContentSize()/2);
        parent->addChild(spr);
        
        if (chara=="o")
        {//決定
            if (AnswerManager::getInstance()->getPasscode()=="330275"&&DataManager::sharedManager()->getEnableFlagWithFlagID(4)&&!DataManager::sharedManager()->getEnableFlagWithFlagID(5)) {
                //りんご
                correct=true;
            }
            else if (AnswerManager::getInstance()->getPasscode()=="050227"&&DataManager::sharedManager()->getEnableFlagWithFlagID(13)&&!DataManager::sharedManager()->getEnableFlagWithFlagID(14)) {
                //みかん
                correct=true;
            }
            else{
                Common::playSE("boo.mp3");
                AnswerManager::getInstance()->resetPasscode();
                for (auto label :SupplementsManager::getInstance()->labelSupplements) {
                    label->setString("");
                }
            }
        }
        else
        {//append or delete
            if (chara=="x"){
                //1文字削除
                if (length_now>0) {
                    auto label=SupplementsManager::getInstance()->labelSupplements.at(length_now-1);
                    label->setString("");
                }
            }
            else{
                if (length_now<6) {
                    auto label=SupplementsManager::getInstance()->labelSupplements.at(length_now);
                    label->setString(chara);
                }
            }
            
            AnswerManager::getInstance()->resetPasscode();
            for (auto label :SupplementsManager::getInstance()->labelSupplements) {
                AnswerManager::getInstance()->appendPasscode(label->getString());
            }
        }
        
        auto fades=TargetedAction::create(spr, Sequence::create(FadeIn::create(0),DelayTime::create(.05),FadeOut::create(.0), NULL));
        auto call=CallFunc::create([correct,callback]{
            
            if (correct) {
                Common::playSE("pinpon.mp3");
                callback(true);
            }
            else{
                callback(false);
            }
        });
        
        parent->runAction(Sequence::create(fades,call, NULL));
    }
    else if(key.compare(0,4,"give")==0)
    {
        //auto index=atoi(key.substr(5,1).c_str());
        
        auto anim=Sprite::createWithSpriteFrameName("anim_15_0.png");
        anim->setPosition(parent->getContentSize()/2);
        anim->setOpacity(0);
        parent->addChild(anim);
        
        auto anim_1=Sprite::createWithSpriteFrameName("anim_15_1.png");
        anim_1->setPosition(parent->getContentSize()/2);
        anim_1->setOpacity(0);
        parent->addChild(anim_1);
        
        auto fadein_0=TargetedAction::create(anim, FadeIn::create(1));
        auto delay_0=DelayTime::create(1);
        auto sound=CallFunc::create([]{Common::playSE("manager_2.mp3");});
        auto fadein_1=TargetedAction::create(anim_1, FadeIn::create(.5));
        
        auto back=Sprite::createWithSpriteFrameName("back_12.png");
        back->setPosition(parent->getContentSize()/2);
        back->setOpacity(0);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        std::vector<std::string>supNames;
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(1)) {
            supNames.push_back("sup_12_battery.png");
        }
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(5)) {
            supNames.push_back("sup_12_card_apple.png");
        }
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(14)) {
            supNames.push_back("sup_12_card_orange.png");
        }
        
        for (auto name :supNames) {
            auto sup=Sprite::createWithSpriteFrameName(name);
            sup->setPosition(back->getContentSize()/2);
            sup->setName(name);
            back->addChild(sup);
        }
        
        auto delay=DelayTime::create(.3);
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(delay,fadein_0,delay_0,sound,fadein_1,delay_0->clone(),fadein,delay_0->clone(),call,NULL));
    }
    else if(key=="battery")
    {
        Common::playSE("p.mp3");
        
        auto battery=Sprite::createWithSpriteFrameName("sup_46_battery.png");
        battery->setPosition(parent->getContentSize()/2);
        battery->setOpacity(0);
        parent->addChild(battery);
        
        auto fadein_0=TargetedAction::create(battery, FadeIn::create(.7));
        auto delay_0=DelayTime::create(.5);
        auto sounds=CallFunc::create([parent]{
            Common::playSE("rock.mp3");
            for(int i=0;i<2;i++){
                auto particle=ParticleSystemQuad::create("particle_wave.plist");
                particle->setAutoRemoveOnFinish(true);
                particle->setDuration(7);
                particle->setStartSize(parent->getContentSize().width*.1);
                particle->setEndSize(parent->getContentSize().width*.33);
                particle->resetSystem();
                particle->setLife(.75);
                particle->setTotalParticles(5);
                particle->setStartColor(Color4F(0, 200, 0, 255));
                particle->setEndColor(Color4F(0, 200, 0, 0));
                particle->setPosition(Vec2(parent->getContentSize().width/2, parent->getContentSize().height*(.35+i*.375)));
                parent->addChild(particle);
            }
        });
        auto delay_1=DelayTime::create(6);
        
        //scene change
        auto back_1=Sprite::createWithSpriteFrameName("back_45.png");
        back_1->setOpacity(0);
        back_1->setPosition(parent->getContentSize()/2);
        back_1->setCascadeOpacityEnabled(true);
        back_1->setLocalZOrder(parent->getLocalZOrder()+1);
        parent->addChild(back_1);
        
        std::vector<std::string>supNames={"sup_45_battery.png","anim_45_pig_0.png","anim_45_pig_1.png","sup_45_open.png","sup_45_close.png"};
        for (auto name : supNames) {
            auto spr=Sprite::createWithSpriteFrameName(name);
            spr->setName(name);
            spr->setPosition(parent->getContentSize()/2);
            back_1->addChild(spr);
            
            if (name==supNames.at(1)||name==supNames.at(2)) {
                spr->setOpacity(0);
            }
        }
        
        auto fadein_1=TargetedAction::create(back_1, FadeIn::create(1));
        auto delay_2=DelayTime::create(.7);
        
        auto remove_curtain=Spawn::create(TargetedAction::create(back_1->getChildByName("sup_45_close.png"), FadeOut::create(.7)),
                                          TargetedAction::create(back_1->getChildByName("anim_45_pig_0.png"), FadeIn::create(.7)),
                                          CallFunc::create([]{Common::playSE("sa.mp3");}),NULL);
        auto remove_pig_0=Spawn::create(TargetedAction::create(back_1->getChildByName("anim_45_pig_0.png"),FadeOut::create(.7)),
                                        TargetedAction::create(back_1->getChildByName("anim_45_pig_1.png"),FadeIn::create(.7)),
                                        CallFunc::create([]{Common::playSE("pig.mp3");}), NULL);
        
        auto remove_pig_1=TargetedAction::create(back_1->getChildByName("anim_45_pig_1.png"),FadeOut::create(.7));
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_0,delay_0,sounds,delay_1,fadein_1,delay_2,remove_curtain,delay_2->clone(),remove_pig_0,delay_2->clone(),remove_pig_1,delay_2->clone(),call,NULL));
    }
    else if(key=="darts")
    {
        Common::playSE("p.mp3");
        auto darts=Sprite::createWithSpriteFrameName("anim_53_darts_0.png");
        darts->setPosition(parent->getContentSize()/2);
        darts->setOpacity(0);
        parent->addChild(darts);
        
        auto pig=parent->getChildByName("sup_53_pig.png");
        
        Vector<Sprite*>flying_darts;
        for(int i=1;i<4;i++){
            //flying darts
            auto darts_1=Sprite::createWithSpriteFrameName("anim_53_darts_1.png");
            darts_1->setPosition(parent->getContentSize()/2);
            darts_1->setOpacity(0);
            pig->addChild(darts_1);
            
            flying_darts.pushBack(darts_1);
        }
        
        auto fadein_0=TargetedAction::create(darts,Spawn::create(FadeIn::create(.7),
                                                                 MoveBy::create(2,Vec2(0, parent->getContentSize().height*.1)), NULL) );
        auto delay=DelayTime::create(1);
        auto change_darts=Spawn::create(TargetedAction::create(darts, FadeOut::create(1)),
                                        TargetedAction::create(flying_darts.at(0), FadeIn::create(1)),
                                        TargetedAction::create(flying_darts.at(1), FadeIn::create(1)),
                                        TargetedAction::create(flying_darts.at(2), FadeIn::create(1)),
                                        CallFunc::create([]{Common::playSE("paku.mp3");}), NULL);
        
        //pig animate
        auto skew=TargetedAction::create(pig,Sequence::create(EaseOut::create(SkewBy::create(1, -10, 0), 1.5),
                                                              DelayTime::create(1),
                                                              EaseIn::create(SkewBy::create(.2, 10, 0),1.5),
                                                               NULL));
        
        auto shot=Spawn::create(MoveTo::create(.2, Vec2(parent->getContentSize().width*1.5, parent->getContentSize().height*.25)),
                                CallFunc::create([]{Common::playSE("hyu.mp3");}), NULL);
        
        auto shots=Spawn::create(TargetedAction::create(flying_darts.at(0), shot),
                                 TargetedAction::create(flying_darts.at(1),Sequence::create(DelayTime::create(.1),shot->clone(), NULL)),
                                    TargetedAction::create(flying_darts.at(2), Sequence::create(DelayTime::create(.2),shot->clone(), NULL)), NULL);
        
        //scene change
        auto back=Sprite::createWithSpriteFrameName("back_51.png");
        back->setPosition(parent->getContentSize()/2);
        back->setOpacity(0);
        parent->addChild(back);
        
        Vector<FiniteTimeAction*> put_actions;
        for (int i=0; i<3; i++) {
            auto sup=Sprite::createWithSpriteFrameName(StringUtils::format("anim_51_%d.png",i));
            sup->setPosition(parent->getContentSize()/2);
            sup->setOpacity(0);
            sup->setTag(i);
            back->addChild(sup);
            
            auto put=Spawn::create(FadeIn::create(.1),
                                   CallFunc::create([]{Common::playSE("koto.mp3");}), NULL);
            auto seq=Sequence::create(DelayTime::create(.1*i),
                                      TargetedAction::create(sup, put),
                                      NULL);
            put_actions.pushBack(seq);
        }
        
        auto fadein_1=TargetedAction::create(back, FadeIn::create(1));
        
        auto fadeout=Spawn::create(TargetedAction::create(back, FadeOut::create(1.5)),
                                   TargetedAction::create(back->getChildByTag(0), FadeOut::create(1.5)),
                                   TargetedAction::create(back->getChildByTag(1), FadeOut::create(1.5)),
                                   TargetedAction::create(back->getChildByTag(2), FadeOut::create(1.5)), NULL);
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_0,delay,change_darts,delay->clone(),skew,shots,delay->clone(),fadein_1,delay->clone(),Sequence::create(put_actions),delay->clone(),fadeout,call, NULL));
    }
    else if(key.compare(0,4,"disk")==0){
        Common::playSE("p.mp3");
        
        auto index=atoi(key.substr(5,1).c_str());
        
        std::vector<std::string>names={"sup_19_apple.png","sup_19_orange.png"};
        auto disk=AnchorSprite::create(Vec2(.325, .720), parent->getContentSize(), names.at(index));
        disk->setOpacity(0);
        parent->addChild(disk);
        
        auto no=Sprite::createWithSpriteFrameName("sup_19_no.png");
        no->setPosition(parent->getContentSize()/2);
        no->setLocalZOrder(2);
        no->setOpacity(0);
        parent->addChild(no);
        //
        auto fadein=TargetedAction::create(disk,FadeIn::create(1));
        auto move=TargetedAction::create(disk,Spawn::create(MoveBy::create(2, Vec2(parent->getContentSize().width*.1, parent->getContentSize().height*.3)),
                                CallFunc::create([]{Common::playSE("printer_1.mp3");}),
                                ScaleBy::create(2, .3), NULL));
        auto no_fadein=TargetedAction::create(no, FadeIn::create(.5));
        auto sound=CallFunc::create([]{Common::playSE("pinpon.mp3");});
        auto seq=Sequence::create(fadein,DelayTime::create(.5),move,no_fadein,sound, NULL);
        
        //scene change
        auto back=Sprite::createWithSpriteFrameName("back_55.png");
        back->setOpacity(0);
        back->setPosition(parent->getContentSize()/2);
        back->setLocalZOrder(3);
        parent->addChild(back);
        
        std::vector<std::string>names_1={"sup_55_apple.png","sup_55_orange.png"};
        auto sup=Sprite::createWithSpriteFrameName(names_1.at(index));
        sup->setOpacity(0);
        sup->setPosition(parent->getContentSize()/2);
        sup->setLocalZOrder(3);
        parent->addChild(sup);
        
        auto fadein_1=TargetedAction::create(back, FadeIn::create(1));
        auto fadein_2=TargetedAction::create(sup, FadeIn::create(1));
        
        auto fadeouts=Spawn::create(TargetedAction::create(back, FadeOut::create(2)),
                                    TargetedAction::create(sup, FadeOut::create(1)), NULL);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq,DelayTime::create(1),fadein_1,DelayTime::create(1),fadein_2,sound->clone(),DelayTime::create(1),fadeouts,call, NULL));
    }
    else if(key=="connect"){
        auto back=Sprite::createWithSpriteFrameName("back_26.png");
        back->setOpacity(0);
        back->setPosition(parent->getContentSize()/2);
        parent->addChild(back);
        
        auto sup=Sprite::createWithSpriteFrameName("sup_26_on.png");
        sup->setOpacity(0);
        sup->setPosition(parent->getContentSize()/2);
        parent->addChild(sup);
        
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto fadein_1=TargetedAction::create(sup, FadeIn::create(1));
        auto sound=CallFunc::create([]{Common::playSE("pinpon.mp3");});
        auto fadeouts=Spawn::create(TargetedAction::create(back, FadeOut::create(1)),
                                    TargetedAction::create(sup, FadeOut::create(1)), NULL);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,fadein_1,sound,delay->clone(),fadeouts,call, NULL));
    }
    else if(key=="error"){
        auto sup=Sprite::createWithSpriteFrameName("sup_14_error.png");
        sup->setOpacity(0);
        sup->setPosition(parent->getContentSize()/2);
        parent->addChild(sup);
        
        auto original=sup->getScale();
        auto scale=ScaleTo::create(.1, .01);
        auto appear=Spawn::create(EaseIn::create(ScaleTo::create(.3, original),2),FadeIn::create(.3), NULL);
        //auto sound=CallFunc::create([]{Common::playSE("nyu.mp3");});
        auto delay=DelayTime::create(2);
        auto disappear=Spawn::create(EaseOut::create(ScaleTo::create(.3, original*.01),2),FadeOut::create(.3), NULL);
        auto seq=TargetedAction::create(sup, Sequence::create(scale,appear,delay,disappear, NULL));
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq,call, NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void GuestHouseActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{
    if (key=="sw") {
        auto back=Sprite::createWithSpriteFrameName("back_33.png");
        back->setPosition(parent->getMainRect().origin.x,parent->getMainRect().origin.y);
        back->setScale(parent->getMainRect().size.width/back->getContentSize().width);
        back->setOpacity(0);
        parent->addChild(back);
        
        AnswerManager::getInstance()-> enFlagIDs.push_back(25);
        
        Vector<FiniteTimeAction*>actions;
        Vector<FiniteTimeAction*>actions_fadeout;
        for (int i=0; i<7; i++) {
            auto sup=Sprite::createWithSpriteFrameName(StringUtils::format("sup_33_%d.png",i));
            sup->setOpacity(0);
            sup->setScale(back->getScale());
            sup->setPosition(back->getPosition());
            parent->addChild(sup);
            
            actions.pushBack(TargetedAction::create(sup, Sequence::create(FadeIn::create(.7),FadeOut::create(.7),FadeIn::create(1), NULL)));
            actions_fadeout.pushBack(TargetedAction::create(sup,FadeOut::create(1)));
        }
        
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        auto sound=CallFunc::create([parent]{
            parent->backImage->setOpacity(0);
            parent->setOpacity(0);
            Common::playSE("pinpon.mp3");});
        auto fadeout=Spawn::create(TargetedAction::create(back, FadeOut::create(1)),
                                   Spawn::create(actions_fadeout),
                                   NULL);
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,Spawn::create(actions),sound,DelayTime::create(1),fadeout,call, NULL));
    }
}
