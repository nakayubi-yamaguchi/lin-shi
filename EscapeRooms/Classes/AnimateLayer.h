//
//  AnimateLayer.h
//  Kebab
//
//  Created by 成田凌平 on 2016/12/21.
//
//mainスプライトをアイテム選択状態でタッチした時に使用する

#ifndef __Kebab__AnimateLayer__
#define __Kebab__AnimateLayer__

#include "cocos2d.h"
USING_NS_CC;
class AnimateLayer : public cocos2d::LayerColor
{
public:
    virtual bool init(ValueMap animation,Rect rect,Node*parent,const ccMenuCallback callback);
    static AnimateLayer *create(ValueMap animation,Rect rect,Node*parent,const ccMenuCallback callback);
    
    CC_SYNTHESIZE(float, duration, Duration);
    CC_SYNTHESIZE(Node*, node, Node);
private:
    ccMenuCallback _callback;
    Menu*closeMenu;

    ValueVector images;//アニメーション表示する背景filenameを格納
    ValueVector images_remove;
    ValueVector SEs;//アニメーション表示の際の再生filenameを格納

    bool _fullscreen;//全画面パターンか
    bool _whiteBack;
    
    Rect imageRect;
    int showIndex;
    
    void addEventListner();
    void showAnimateImage();
    ~AnimateLayer();
};

#endif /* defined(__Kebab__AnimateLayer__) */
