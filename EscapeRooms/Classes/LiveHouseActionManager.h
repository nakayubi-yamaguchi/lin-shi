//
//  LiveHouseActionManager.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/03/30.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________LiveHouseActionManager_____
#define _____PROJECTNAMEASIDENTIFIER________LiveHouseActionManager_____

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;
class LiveHouseActionManager:public CustomActionManager
{
public:
    static LiveHouseActionManager* manager;
    static LiveHouseActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
private:

};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
