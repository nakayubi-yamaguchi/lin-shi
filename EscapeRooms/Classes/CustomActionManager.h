//
//  CustomActionManager.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/03/12.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________CustomActionManager_____
#define _____PROJECTNAMEASIDENTIFIER________CustomActionManager_____

#include "cocos2d.h"
#include "PopUpLayer.h"
#include "StoryLayer.h"
#include "AnswerManager.h"
#include "ShowType.h"
USING_NS_CC;

typedef std::function<void(bool successed)> onFinished;
USING_NS_CC;
class CustomActionManager
{
public:
    
#pragma mark - optional method
    virtual int getDefaultBackNum();
    virtual void openingAction(Node*node,const onFinished& callback);
    virtual void backAction(std::string key,int backID,Node*parent){};
    virtual void backAction(std::string key,int backID,Node*parent,ShowType type){};

    virtual void touchAction(std::string key,Node*parent,const onFinished& callback);
    /**touchBegan時に呼び出し*/
    virtual void touchBeganAction(std::string key,Node*parent){};
    /**drag時に呼び出し useDragがtrueの時のみ*/
    virtual void dragAction(std::string key,Node*parent,Vec2 pos){};

    /**popUp上で呼び出される*/
    virtual void itemBackAction(std::string key,PopUpLayer*parent){};
    /**popUp上で呼び出される*/
    virtual void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback){callback(true);};
    /**answer image上で呼び出される*/
    virtual void showAnswerAction(std::string key,Node*parent){};
    virtual void showAnswerAction(std::string key,Node*parent,ShowType showType){};

    /**storyLayer上で呼び出される*/
    virtual void storyAction(std::string key,Node*parent,const onFinished& callback){callback(true);};
    /**underSprite上で呼び出される*/
    virtual void arrowAction(std::string key,Node*parent,const onFinished& callback){callback(true);};

    /**その他タイミングでの呼び出し CustomAction_ReviewLayer等(アイテムの消去など),CustomAction_Title(particle降らす等),CustomAction_Clear(room_image_1)  はデフォルトで呼び出される*/
    virtual void customAction(std::string key,Node*parent){};
    
    /**SideBarLayerでの呼び出し。カメラボタンが押された時。*/
    virtual void cameraShotAction(int backID){};

    virtual void clearParticleAction(Node*parent);

#pragma mark - addChild
    virtual Sprite*createSpriteToCenter(std::string name,bool invisible,Node* parent);
    virtual Sprite*createSpriteToCenter(std::string name,bool invisible,Node* parent,bool addChild);
    virtual Sprite*createAnchorSprite(std::string name,float x,float y,bool invisible,Node* parent,bool addChild);
    /**targetSpriteのアンカーポイントを変更。ポジションがずれないようにする。*/
    virtual void transformToAnchorSprite(Node* parent, Node* targetNode, Vec2 anchorPoint);
    /**stencilとなるNodeのnameを渡す addchildまで行う*/
    virtual Node*createClippingNodeToCenter(std::string name,Node*parent);
    /**PopUp上のMask用clipnodeにaddする*/
    virtual Sprite*createSpriteOnClip(std::string name,bool invisible,PopUpLayer*popup,bool addChild);
    /**StoryLayerメッセージ上にaddする.CutInにaddchildし、タッチの際ともにfadeoutする*/
    virtual Sprite*createSpriteOnStoryCutIn(std::string name,bool invisible,float width,StoryLayer*parent);
#pragma mark - change
    virtual Spawn* createChangeAction(float duration_fadeout,float duration_fadein,Node*before,Node*after);
    virtual Spawn* createChangeAction(float duration_fadeout,float duration_fadein,Node*before,Node*after,std::string soundName);
    virtual Spawn* createChangeAction(float duration_fadeout,float duration_fadein,Node*before,Node*after,std::string soundName, ShowType type);
    /**指定nodeを順番に切り替え*/
    virtual FiniteTimeAction* createChangeActionSequence(float duration_fadeout,float duration_fadein,Vector<Node*> nodes,std::vector<std::string> soundnames);

    /**複数のノードを同時切り替え.サウンド無し*/
    virtual Spawn* createChangeAction(float duration_fadeout, float duration_fadein, Vector<Node*> befores, Vector<Node*> afters);
    /**複数のノードを同時切り替え.サウンドあり*/
    virtual Spawn* createChangeAction(float duration_fadeout, float duration_fadein, Vector<Node*> befores, Vector<Node*> afters, std::string soundName);
#pragma mark - bounce
    virtual FiniteTimeAction*createBounceAction(Node*node,float duration);

    /**例えばdeflectionが.1のときscale1.1、.05のとき1.05を最大としてぐにょっとなる 全体のdurationはduration*2.5*/
    virtual FiniteTimeAction*createBounceAction(Node*node,float duration,float deflection);
    /**例えばdeflectionが.1のときscale1.1、.05のとき1.05を最大としてぐにょっとなる 全体のdurationはduration*2.5*/
    virtual FiniteTimeAction*createBounceAction(Node*node,float duration,float deflection,float original_scale);
    /**ぐにょぐにょしながら指定scaleへ*/
    virtual TargetedAction*createBounceActionToTargetScale(Node*node,float duration, float deflection,float targetScale,float ease,float scale_original);
    /**現在地でバウンスしながらジャンプする.その場でrepeatCount回ジャンプする*/
    virtual FiniteTimeAction* bounceJumpAction(Node *targetNode, Vec2 targetAnchorPoint, Node* parent, std::string jumpSoundName, int repeatCount);
    /**jumpToPointまでバウンスしながらジャンプする。*/
    virtual FiniteTimeAction* bounceJumpToAction(Node *targetNode, Vec2 targetAnchorPoint, Vec2 jumpToPoint, Node *parent, float jumpDuration, float jumpHeight, std::string jumpSoundName);
    
#pragma mark - Jump
    /**jumpして元の位置に戻る 全体のdurationは*2*/
    virtual TargetedAction*jumpAction(float height,float duration,Node*node,float ease);
    /**jumpして元の位置に戻る １ジャンプのdurationは*2*/
    virtual TargetedAction*jumpAction(float height,float duration,Node*node,float ease,int repeat);
    //virtual TargetedAction*jumpAction(float height,float duration,Node*node,float ease,int repeat,float );

    virtual TargetedAction*jumpAction(Vec2 vec,float duration,Node*node,float ease);
    virtual TargetedAction*jumpAction(Vec2 vec,float duration,Node*node,float ease,int repeat);

#pragma mark - Swing
    /**total durationは*2*/
    virtual TargetedAction*swingAction(float angle,float duration,Node*node,float ease);
    /**total durationは*2 pendulumなら*4*/
    virtual TargetedAction*swingAction(float angle,float duration,Node*node,float ease,bool isPendulum);
    /**total durationは*2 pendulumなら*4*/
    virtual TargetedAction*swingAction(float angle,float duration,Node*node,float ease,bool isPendulum,int repeat);
    /**total durationは*2 pendulumなら*4*/
    virtual TargetedAction*swingAction(float angle,float duration,Node*node,float ease,bool isPendulum,int repeat,std::string soundName);
    virtual TargetedAction*swingAction(Vec2 vec,float duration,Node*node,float ease,bool isPendulum);
    virtual TargetedAction*swingAction(Vec2 vec,float duration,Node*node,float ease,bool isPendulum,int repeat);

    virtual TargetedAction*swingActionForever(float angle,float duration,Node*node,float ease);
    virtual TargetedAction*swingActionForever(Vec2 vec,float duration,Node*node,float ease);

#pragma mark - Skew
    virtual TargetedAction*skewAction(Node*node,float duration,float angle,int repeat);
    
#pragma mark - Cloud
    virtual FiniteTimeAction* cloudAction(Node*parent,std::string name,Vec2 anchor);
    virtual FiniteTimeAction* cloudAction(Node*parent,std::string name,Vec2 anchor,float duration);

    /**吹き出しと同時にキャラをバウンス*/
    virtual FiniteTimeAction* cloudBounceAction(Node *parent, std::vector<std::string> bounceCharaNames, Vec2 charaAnchorPoint, std::string cloudFileName, Vec2 cloudAnchorPoint, std::string charaVoiceName);
    /**吹き出しと同時にキャラをバウンス*/
    virtual FiniteTimeAction* cloudBounceAction(Node *parent, Node*character, std::string cloudFileName, Vec2 cloudAnchorPoint, std::string charaVoiceName);
#pragma mark - Sound
    /**アイテムを使用した時の音を再生するアクション*/
    virtual FiniteTimeAction* useItemSoundAction();
    /**正解時の音を再生するアクション*/
    virtual FiniteTimeAction* correctSoundAction();
    virtual CallFunc*createSoundAction(std::string name);
    virtual CallFunc*createSoundAction(std::string name,ShowType type);
#pragma mark - フリップ
    
    /**
     カードフリップアクション。カードの画像をあらかじめ貼ってある場合はplistのAnchorSpritesを使っている想定。
     @param parent 親Node
     @param frontFileName 手前に見えているファイル名
     @param backFileName 裏返っている面のファイル名
     @param anchorPoint フリップするカードのアンカーポイント
     @param flipDuration フリップする秒数
     @return フリップアクション
     */
    virtual FiniteTimeAction* flipCardAction(Node *parent, std::string frontFileName, std::string backFileName, Vec2 anchorPoint, float flipDuration);

#pragma mark - 汎用
    /**keyAction 命名規則あり sup_%d_key_0,sup_%d_key_1 が必要*/
    virtual FiniteTimeAction* keyAction(Node*parent,int backID);
    /**使用開始時の音を鳴らすか */
    virtual FiniteTimeAction* keyAction(Node*parent,int backID,bool sound);

    /**棚をあけるようなaction backを表示し(Optional)、doorSpriteをfadein supname(Optional)でdoorにsupを追加できる*/
    virtual FiniteTimeAction* openDoorAction(Node*parent,std::string backname,int backID,std::string doorname,std::string supName,std::string soundName);
    /**棚をあけるようなaction backを表示し(Optional)、doorSpriteをfadein supname(Optional)でdoorにsupを追加できる removeDoorがtrueのとき指定doorNameをfadeoutする 正解時に蓋などを消去するタイプ用*/
    virtual FiniteTimeAction* openDoorAction(Node*parent,std::string backname,int backID,std::string doorname,std::string supName,std::string soundName,bool removeDoor);
    /**addChildされたNodeをfadein*/
    virtual FiniteTimeAction* putAction(Node*node,std::string soundName,float duration_fadein,float delay);
    virtual Sequence* putAction(Node*parent,std::string fileName,std::string soundName,float duration,const onFinished&callback);
    /**fadein->nodeのanchorpointに向けてscale*/
    virtual TargetedAction* giveAction(Node*node,float duration,float scale,bool fadeout);
    virtual TargetedAction* giveAction(Node*node,float duration,float scale,bool fadeout,std::string soundName);

    virtual FiniteTimeAction*changeAnchorAction(Node*parent,Node*node,Vec2 vec);
#pragma mark - 特殊スプライト
    /**側のようなアクション(addChildまで行う) fileNameにファイル名を渡す size,amplitudeは0~1.0で指定(画面幅基準) waveSize:1波の間隔 waveCountで揺れ回数 waveAmplitudeで振幅. horizontal verticalで揺れ方向を指定 anchorPointの指定ができない parent/gridNode/spriteの構造に注意*/
    virtual Sprite* createWaveSprite(Node*parent ,std::string fileName ,float duration,float delay,Size waveSize,int waveCount,float waveAmplitude,bool horizontal,bool vertical,int repeat);
    /**水面3Dアクション(addChildまで行う) fileNameに水面ファイル名を渡す size,amplitudeは0~1.0で指定(画面幅基準)  waveSize:1波の間隔 waveCountで揺れ回数 waveAmplitudeで振幅. parent/gridNode/spriteの構造に注意*/
    virtual Sprite* createWave3DSprite(Node*parent ,std::string fileName ,float duration,Size waveSize,int waveCount,float waveAmplitude,int repeat);
    /**水面3Dアクション(波紋パターン)(addChildまで行う)  size,amplitudeは0~1.0で指定(画面幅基準) fileNameに水面ファイル名を渡す waveSize:1波の間隔 waveCountで揺れ回数 amplitudeで振幅. parent/gridNode/spriteの構造に注意*/
    virtual Sprite* createRipple3DSprite(Node*parent ,std::string fileName ,float duration,Size waveSize,Vec2 position,float radius,int waveCount,float amplitude,int repeat);
protected:


    /**鼻ちょうちん表示*/
    virtual FiniteTimeAction* balloonAction(Node *parent, std::string balloonFileName, Vec2 balloonAnchorPoint, float duration);
    
    /**びっくりマークと同時にキャラをバウンス*/
    virtual FiniteTimeAction* attentionAction(Node *parent, std::string charaName, Vec2 charaAnchorPoint, std::string attentionFileName);
    virtual FiniteTimeAction* attentionAction(Node *parent, std::string charaName, Vec2 charaAnchorPoint, std::string attentionFileName,float duration,float defrection);

    /**ドライバーアクション。ドライバーの補完画像は先っちょが画像の中心に来るように。*/
    virtual FiniteTimeAction* driverAction(int backNum, std::vector<Vec2> positions, Node* parent);
    
    /**ドライバーアクション。ドライバーの補完画像は先っちょが画像の中心に来るように。*/
    virtual FiniteTimeAction* driverAction(int backNum, std::vector<Vec2> positions, Node* parent, int zOrder);
    
    /**アイテムをセットするアニメーションを作成*/
    virtual FiniteTimeAction* setItemAction(Node* parent, Sprite* itemSprite);
    
    /**アイテムをセットのみのアニメーションを実行*/
    virtual void runSetItemAction(Node* parent, Sprite* itemSprite, const std::function<void(bool success)> &callback);

    /**同じアクションを複数のターゲットに適用する場合に使用*/
    virtual Spawn* getActionWithTargets(Vector<Sprite*> targets, FiniteTimeAction* action);
    
    /**引数アクションに音をつけて返す*/
    virtual FiniteTimeAction* getActionsWithSound(FiniteTimeAction* action, std::string soundName);
    
    /**マッチ点火アニメーション*/
    virtual FiniteTimeAction* matchAction(Node *parent, int backID, ParticleSystemQuad *fireParticle);
    
    
    /**ベジェのアニメ*/
    virtual FiniteTimeAction* bezierActions(Node* parent, std::vector<std::vector<Vec2>> posVecs, float duration);
#pragma mark 遅延実行
    void delayAndCallBack(Node*parent,float duration,bool touchEnable, const std::function<void()> callback); 
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
