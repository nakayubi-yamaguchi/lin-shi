//
//  BonusLayer.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/02/24.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________BonusLayer_____
#define _____PROJECTNAMEASIDENTIFIER________BonusLayer_____

#include "cocos2d.h"

typedef enum {
    Rarelity_UltraRare=0,
    Rarelity_SuperRare,
    Rarelity_Rare,
    Rarelity_Normal
}Rarelity;

typedef enum{
    DoorColor_Gold=0,
    DoorColor_Red,
    DoorColor_Green,
    DoorColor_White,
    DoorColor_Nakayubi
}DoorColor;

typedef enum {
    Ratio_UltraRare=2,
    Ratio_SuperRare=10,
    Ratio_Rare=35,
    Ratio_Normal=53
}Ratio;

typedef enum{
    Performance_Back_None=0,
    Performance_Back_Red,
    Performance_Back_Green,
    Performance_Back_Nakayubi
}Performance_Back;

typedef enum{
    Performance_Nu_None=0,
    Performance_Nu_One,
    Performance_Nu_Many,
    Performance_Nu_Gold
}Performance_Nu;

typedef enum{
    Performance_Roulette_None=0,
    Performance_Roulette_Reverse
}Performance_Roulette;

typedef enum{
    Performance_Crowd_None=0,
    Performance_Crowd_Pig
}Performance_Crowd;

//抽選構造体
struct Lottery{
    Rarelity rarelity;
    DoorColor doorColor;
    int coin;
};

typedef std::function<void()> onFinish;

class BonusLayer : public cocos2d::LayerColor
{
public:
    virtual bool init(const onFinish& callback);
    static BonusLayer* create(const onFinish& callback);
    LayerColor*backGroundLayer;
    onFinish mCallBack;
    /**v.3.1まで初回金扉確定させるよう*/
    //bool firstBonus;
    
    /**抽選とデータ付与*/
    void lottery();
    void save();
    
    Lottery lot;
    std::vector<int>doors;
    Performance_Back performance_back;
    Performance_Nu performance_nu;
    Performance_Roulette performance_roulette;
    Performance_Crowd performance_crowd;
    int stopPosIndex;
    int lotteryCount;//何回目の抽選か(1日の中で)
    bool isRare;//レア確定
    
    /**演出*/
    void setDoors(std::vector<int>colors);
    void flashBackColor();
    void addNuPig();
    void addCrowd();
    
    /**UIを表示します。Collectionから呼び出した場合、残り抽選回数を表示します*/
    void show(bool fromCollection);
    
    void createRoulette();
    void showResultAlert();
    /**リワード再生可能か確認*/
    void checkConditionOfReward();
    void restart();
    void addTouchListener();
    
    /**キー生成*/
    std::string getKeyOfLottelyCountToday();
    /**閉じる*/
    void close();
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
