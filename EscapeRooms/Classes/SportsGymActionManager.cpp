//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "SportsGymActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"

using namespace cocos2d;

SportsGymActionManager* SportsGymActionManager::manager =NULL;

SportsGymActionManager* SportsGymActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new SportsGymActionManager();
    }
    return manager;
}

#pragma mark - Back
void SportsGymActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="spring")
    {//びよん人形
        auto spring=parent->getChildByName(StringUtils::format("sup_%d_spring.png",backID));
        spring->setCascadeOpacityEnabled(true);
        createAnchorSprite("sup_24_balloon.png", .5, .353, false, spring, true);
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(26)) {
            for (int i=0; i<4; i++) {
                createSpriteToCenter(StringUtils::format("sup_24_%d.png",i), false, parent, true);
            }
        }
    }
    else if(key=="race")
    {//レース
        Vector<FiniteTimeAction*>spawns;
        auto delay=DelayTime::create(1);
        
        std::vector<int>charaIDs={5,3,2,6,1,4};
        
        int count=0;
        
        //lambda
        auto createRunner=[this,backID,parent](int charaID,int count,bool mistake,bool invisible){
            Vector<FiniteTimeAction*>sequences;
            std::vector<float>delays={3,3,5,7,0.5,4};
            std::vector<int>jumpCounts={20,0,15,10,15,15};
            std::vector<float>speeds={8,10,9,7,18,5};
            std::vector<float>anchorYs={.36,.44,.33,.28,.27,.26};
            Node*parent_node;

            std::string fileName;
            if (mistake) {
                parent_node=parent->getChildByName(StringUtils::format("sup_%d_mistake.png",backID));
                fileName=StringUtils::format("sup_%d_%d_0_mistake.png",backID,charaID);
            }
            else{
                fileName=StringUtils::format("sup_%d_%d_0.png",backID,charaID);
                parent_node=parent;
            }
            
            auto runner=this->createAnchorSprite(fileName, .5, anchorYs.at(count), true, parent_node, true);
            runner->setPosition(parent_node->getContentSize().width*1.15,runner->getPosition().y);
            if (invisible) {
                runner->setOpacity(0);
            }
            
            //移動
            auto distance_x=-parent_node->getContentSize().width*1.25;
            auto height=parent_node->getContentSize().height*.05;
            
            auto delay=DelayTime::create(delays.at(count));
            sequences.pushBack(delay);
            
            auto fadein=FadeIn::create(0);
            if (!invisible) {
                sequences.pushBack(fadein);
            }
            
            auto move=JumpBy::create(speeds.at(count), Vec2(distance_x, 0),height,jumpCounts.at(count));
            if (charaID!=3) {
                auto bounce=Repeat::create(createBounceAction(runner, speeds.at(count)/jumpCounts.at(count)/2, .1), UINT_MAX);
                sequences.pushBack(Spawn::create(move,bounce, NULL));
            }
            else{
                auto rotate=swingAction(10, 1, runner, 1.5, true, -1);
                sequences.pushBack(Spawn::create(move,rotate, NULL));
            }
            
            auto seq=Sequence::create(sequences);
            
            return TargetedAction::create(runner, seq);
        };
        
        for (int charaID : charaIDs) {
            
            if (DataManager::sharedManager()->isPlayingMinigame()) {
                spawns.pushBack(createRunner(charaID,count,false,true));
                spawns.pushBack(createRunner(charaID,count,true,false));
                if (count==0) {
                    spawns.pushBack(Sequence::create(DelayTime::create(3.5),
                                                     CallFunc::create([]{
                        log("タッチ可能になった");
                        DataManager::sharedManager()->setTemporaryFlag("mistake_75", 1);
                    }),
                                                     DelayTime::create(13),
                                                     CallFunc::create([]{
                        log("タッチ不可能になった");
                        DataManager::sharedManager()->setTemporaryFlag("mistake_75", 0);
                    }),
                                                     NULL));
                }
            }
            else{
                spawns.pushBack(createRunner(charaID,count,false,false));
            }
            
            count++;
        }
        
        spawns.pushBack(Sequence::create(DelayTime::create(5),
                                         createSoundAction("people_cheer1.mp3"), NULL));

        parent->runAction(Sequence::create(delay,Spawn::create(spawns), NULL));
    }
    else if(key=="boxing")
    {//ボクシング
        auto spring=parent->getChildByName(StringUtils::format("sup_%d_spring.png",backID));
        createAnchorSprite("sup_46_balloon.png", .5, .3, false, spring, true);
        
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(23)) {
            auto rabbit_0=parent->getChildByName("sup_46_rabbit_0_0.png");
            rabbit_0->setCascadeOpacityEnabled(false);
            auto rabbit_1=parent->getChildByName("sup_46_rabbit_1_0.png");
            rabbit_1->setCascadeOpacityEnabled(false);

            //必要フレームを事前追加
            for (int i=1; i<3; i++) {
                createSpriteToCenter(StringUtils::format("sup_46_rabbit_0_%d.png",i), true, rabbit_0, true);
                createSpriteToCenter(StringUtils::format("sup_46_rabbit_1_%d.png",i), true, rabbit_1, true);
            }
            
            //punch->swing
            Vector<FiniteTimeAction*>actions;
            actions.pushBack(DelayTime::create(1));
            std::vector<int>indexs={0,2,1,2,3,0};
            for (auto index : indexs) {
                auto isLeft=(index>1);//swing方向
                Node* rabbit;

                if (isLeft) {
                    rabbit=rabbit_0;
                }
                else{
                    rabbit=rabbit_1;
                }
                
                auto rabbit_step=rabbit->getChildByName(StringUtils::format("sup_46_rabbit_%d_1.png",!isLeft));
                auto rabbit_punch=rabbit->getChildByName(StringUtils::format("sup_46_rabbit_%d_2.png",!isLeft));

                auto pos_original=rabbit->getPosition();
                
                //back step
                auto distance_step=parent->getContentSize().width*.1*pow(-1, isLeft);
                auto backstep=Spawn::create(TargetedAction::create(rabbit, JumpBy::create(.3, Vec2(distance_step, 0), parent->getContentSize().height*.1, 1)),
                                            createChangeAction(.3, .1, rabbit, rabbit_step),
                                            createSoundAction("pop_4.mp3"),
                                            NULL);
                actions.pushBack(backstep);
                actions.pushBack(DelayTime::create(.5));
                //punch
                auto distance_punch=parent->getContentSize().width*.25*pow(-1, !isLeft);
                auto height_punch=parent->getContentSize().height*.19;
                auto punch=Spawn::create(TargetedAction::create(rabbit, JumpBy::create(.5, Vec2(distance_punch, height_punch), height_punch*1.2, 1)),
                                         createChangeAction(.5, .3, rabbit_step, rabbit_punch), NULL);
                actions.pushBack(punch);
                actions.pushBack(createSoundAction("hit.mp3"));

                //punch_back&swing
                auto punch_back=Spawn::create(Sequence::create(DelayTime::create(.1),
                                                               TargetedAction::create(rabbit, JumpTo::create(.3, pos_original, parent->getContentSize().height*.1, 1))
                                                               , NULL),
                                              createBounceAction(rabbit, .2,.05),
                                              Sequence::create(DelayTime::create(.2),
                                                               createChangeAction(.5, .3, rabbit_punch, rabbit), NULL),
                                              balloonAction(parent, backID, index),
                                              NULL);
                
                actions.pushBack(punch_back);
            }
            parent->runAction(Repeat::create(Sequence::create(actions),UINT_MAX));
        }
    }
    else if(key=="monkey"){
        Vector<Sprite*>monkeys;
        for(int i=0;i<4;i++)
        {
            auto m_0=createSpriteToCenter(StringUtils::format("sup_36_monkey_%d_0.png",i), false, parent, true);
            auto m_1=createSpriteToCenter(StringUtils::format("sup_36_monkey_%d_1.png",i), true, parent, true);
            monkeys.pushBack(m_0);
            monkeys.pushBack(m_1);
        }
        
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(1));
        std::vector<int>indexs={3,2,1,0,2,0,3};
        for (auto index : indexs) {
            auto before=monkeys.at(2*index);
            auto after=monkeys.at(2*index+1);

            auto up=createChangeAction(.7, .35, before,after, "monkey.mp3");
            actions.pushBack(up);
            actions.pushBack(DelayTime::create(1));
            auto down=createChangeAction(.7, .35, after,before, "pop_5.mp3");
            actions.pushBack(down);
        }
        actions.pushBack(DelayTime::create(2));
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
    else if(key=="shampoo")
    {
        Vector<FiniteTimeAction*>actions;
        auto kappa=parent->getChildByName(StringUtils::format("sup_%d_kappa_0.png",backID));
        Vector<SpriteFrame*>frames;
        for (int i=0; i<2; i++) {
            frames.pushBack(EscapeStageSprite::createWithSpriteFileName(StringUtils::format("sup_%d_kappa_%d.png",backID,i))->getSpriteFrame());
        }
        
        auto animation=Animation::createWithSpriteFrames(frames);
        animation->setDelayPerUnit(5.0f/60.0f);//??フレームぶん表示
        animation->setRestoreOriginalFrame(true);
        animation->setLoops(6);
        
        auto animate=TargetedAction::create(kappa, Spawn::create(Animate::create(animation),
                                                                 CallFunc::create([backID]{
            if (backID==58) {
                Common::playSE("goshigoshi_short.mp3");
            }
        }), NULL));
        actions.pushBack(animate);
        
        //bubble
        Vector<FiniteTimeAction*>spawns;
        std::vector<float>scales={.1,.15,.2,.15,.1,.15,.2,.15};
        for (int i=0; i<8; i++) {
            auto bubble=createSpriteToCenter("bubble.png", true, parent,true);
            bubble->setPositionY(parent->getContentSize().height*.3);
            auto bubble_width=parent->getContentSize().width*scales.at(i);
            bubble->setScale(.1);
            auto default_scale=bubble->getScale();
            auto target_scale=bubble_width/bubble->getContentSize().width;
            auto distance_x=parent->getContentSize().width*(-.25+.25*(i>2)+.25*(i>4));
            auto degrees=.2*(i==1||i==6)+.4*(i==0||i==5)+.1*(i==4)+.3*(i==3);
            auto distance_y=parent->getContentSize().height*(.2+degrees);
            
            if (backID==57) {
                bubble->setPositionX(parent->getContentSize().width*.8);
                bubble->setPositionY(parent->getContentSize().height*.325);
                target_scale/=2;
                distance_x=parent->getContentSize().width*(-.125+.125*(i>2)+.125*(i>4));
                distance_y=parent->getContentSize().height*(.1+degrees/2);
            }
            
            auto default_pos=bubble->getPosition();

            
            auto moves=TargetedAction::create(bubble, Spawn::create(FadeIn::create(1),
                                                                    Sequence::create(createBounceActionToTargetScale(bubble, 1,.1, target_scale/2,1.1,bubble->getScale()),
                                                                                     createBounceActionToTargetScale(bubble, 1, .1, target_scale,1.1,target_scale/2), NULL),//ふにょふにょ*2
                                                                    EaseOut::create(JumpBy::create(3.5, Vec2(distance_x, distance_y),distance_y/30,2), 2),
                                                                    swingAction(10, .5, bubble, 1.2, 3),
                                                                    NULL));
            spawns.pushBack(Sequence::create(CallFunc::create([i,backID]{
                if (i==0&&backID==58) {
                    Common::playSE("pop.mp3");
                }
            }),
                                             moves,
                                             CallFunc::create([i,backID]{
                if (i==0&&backID==58) {
                    Common::playSE("pan.mp3");
                }
            }),
                                             TargetedAction::create(bubble, FadeOut::create(0)),
                                             TargetedAction::create(bubble, ScaleTo::create(0, default_scale)),
                                             TargetedAction::create(bubble, MoveTo::create(0, default_pos)),
                                             NULL));
        }
        
        auto spawn_bubble=Spawn::create(spawns);
        actions.pushBack(Spawn::create(Sequence::create(DelayTime::create(1),
                                                        animate->clone(), NULL),
                                       spawn_bubble,
                                       NULL));
        actions.pushBack(DelayTime::create(1));
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
    else if(key=="piero"&&DataManager::sharedManager()->getEnableFlagWithFlagID(27))
    {
        auto cover=parent->getChildByName(StringUtils::format("sup_%d_cover.png",backID));
        slideAction(parent, cover, false, backID);
    }
    else if(key=="dance"
            &&DataManager::sharedManager()->getEnableFlagWithFlagID(40)
            &&!DataManager::sharedManager()->getEnableFlagWithFlagID(47))
    {
        auto seq=danceAction(parent, backID, true);
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),seq, NULL), -1));
    }
    else if(key=="heart"&&DataManager::sharedManager()->getEnableFlagWithFlagID(47))
    {
        auto pig=parent->getChildByName("sup_85_pig.png");
        //heart particle
        auto heart=[parent](int index){
            auto call=CallFunc::create([parent,index]{
                Common::playSE("pop.mp3");
                
                auto size=parent->getContentSize().width*.2;
                std::vector<std::vector<float>>colors={
                    {255,47,146},
                    {0,250,146},
                    {115,253,255},
                    {180,80,255}
                };
                
                auto rgb=colors.at(index);
                
                auto particle=ParticleSystemQuad::create("particle_heart.plist");
                particle->setAutoRemoveOnFinish(true);
                particle->setPosition(Vec2(parent->getContentSize().width*.5,parent->getContentSize().height* .55));
                particle->setDuration(.5);
                particle->setLife(1.75);
                particle->resetSystem();
                particle->setStartSize(size);
                particle->setStartSizeVar(0);
                particle->setEndSize(size*.5);
                particle->setEndSizeVar(0);
                particle->setGravity(Vec2(-pow(-1, (index==1||index==2))*size*1.5, -pow(-1, (index>1))*size*1.5));
                particle->setStartColor(Color4F(rgb.at(0)/255,rgb.at(1)/255,rgb.at(2)/255,1));
                particle->setEndColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 0));
                particle->setAngle(45+90*index);
                particle->setSpeed(size*3.5);
                parent->addChild(particle);
            });
            
            return call;
        };
        
        
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<4; i++) {
            auto bounce=createBounceAction(pig, .2);

            actions.pushBack(Sequence::create(Spawn::create(heart(i),
                                                            bounce, NULL),
                                              DelayTime::create(2), NULL));
        }

        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),Sequence::create(actions),DelayTime::create(1), NULL), UINT_MAX));
    }
    else if(key=="wave"&&DataManager::sharedManager()->getEnableFlagWithFlagID(40)){
        addWaveParticles(parent);
    }
}

#pragma mark - Touch
void SportsGymActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key.compare(0,6,"spring")==0)
    {//びよん
        auto index=atoi(key.substr(7,1).c_str());
        
        Node*mistake;
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            mistake=parent->getChildByName("sup_24_mistake.png");
            mistake->retain();
            mistake->removeFromParent();
            auto balloon=parent->getChildByName("sup_24_spring.png")->getChildByName("sup_24_balloon.png");
            balloon->addChild(mistake);
        }else{
            mistake=NULL;
        }

        //fadein
        auto button_action=buttonAction(index, parent);
        auto spawn=Spawn::create(button_action,
                                 this->balloonAction(parent, DataManager::sharedManager()->getNowBack(), index), NULL);
        auto call=CallFunc::create([callback,mistake,parent]{
            if (mistake) {
                mistake->retain();
                mistake->removeFromParent();
                parent->addChild(mistake);
            }
            callback(true);
        });
        parent->runAction(Sequence::create(spawn,call,NULL));
    }
    else if(key=="pedal"){
        //ペダルはめ
        Vector<FiniteTimeAction*>actions;
        
        auto put=putAction(parent, "sup_38_pedal.png", "p.mp3", 1, nullptr);
        actions.pushBack(put);
        actions.pushBack(createSoundAction("kacha.mp3"));
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        
        //screen
        auto back_39=createSpriteToCenter("back_39.png", true, parent, true);
        auto hand=createSpriteToCenter("sup_39_hand.png", true,back_39, true);
        hand->setPositionY(parent->getContentSize().height*.4);
        auto calorie=createSpriteToCenter("sup_39_calorie.png", true, back_39, true);
        auto fadein_back=TargetedAction::create(back_39, FadeIn::create(1));
        actions.pushBack(fadein_back);
        actions.pushBack(TargetedAction::create(parent->getChildByName<Sprite*>("sup_38_leg.png"), FadeOut::create(.1)));
        actions.pushBack(delay->clone());
        auto action_hand=TargetedAction::create(hand, Sequence::create(FadeIn::create(1),
                                                                       MoveTo::create(1, parent->getContentSize()/2),
                                                                       createSoundAction("pi.mp3"),
                                                                       delay->clone(),
                                                                       Spawn::create(MoveBy::create(1, Vec2(0, -parent->getContentSize().height*.1)),
                                                                                     FadeOut::create(1)
                                                                                     ,NULL),
                                                                       NULL));
        actions.pushBack(action_hand);
        actions.pushBack(delay->clone());
        
        auto back_40=createSpriteToCenter("back_40.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back_40, 40);
        auto pedal=createSpriteToCenter("sup_40_pedal.png", false, back_40, true);
        auto fadein_back_40=TargetedAction::create(back_40, FadeIn::create(1));
        actions.pushBack(fadein_back_40);
        actions.pushBack(delay->clone());
        
        auto pig_0=createSpriteToCenter("sup_40_pig_0.png", false, back_40,true);
        actions.pushBack(TargetedAction::create(pedal, FadeOut::create(0)));
        Vector<SpriteFrame*>frames;
        for (int i=0; i<2; i++) {
            frames.pushBack(EscapeStageSprite::createWithSpriteFileName(StringUtils::format("sup_40_pig_%d.png",i))->getSpriteFrame());
        }
        auto animation=Animation::createWithSpriteFrames(frames);
        animation->setDelayPerUnit(5.0f/60.0f);//??フレームぶん表示
        animation->setRestoreOriginalFrame(true);
        animation->setLoops(20);
        actions.pushBack(createSoundAction("pig.mp3"));
        actions.pushBack(TargetedAction::create(pig_0, Animate::create(animation)));
        actions.pushBack(DelayTime::create(1.5));
        
        auto pig_2=createSpriteToCenter("sup_40_pig_2.png", true, back_40, true);
        auto seq_pig_2=TargetedAction::create(pig_2, Sequence::create(createSoundAction("pig.mp3"),
                                                                      Spawn::create(createChangeAction(1, .7, pig_0, pig_2),
                                                                                    TargetedAction::create(pedal, FadeIn::create(.7)), NULL),
                                                                      delay->clone(),
                                                                      FadeOut::create(1),
                                                                      NULL));
        actions.pushBack(seq_pig_2);
        actions.pushBack(delay->clone());
        
        auto fadeout_back_40=TargetedAction::create(back_40, FadeOut::create(1));
        actions.pushBack(fadeout_back_40);
        actions.pushBack(delay->clone());
        auto fadein_cal=TargetedAction::create(calorie, Spawn::create(FadeIn::create(1),
                                                                      createSoundAction("robot_start.mp3"), NULL));
        actions.pushBack(fadein_cal);
        actions.pushBack(delay->clone());
        auto fadeout_back_39=TargetedAction::create(back_39, FadeOut::create(1));
        actions.pushBack(fadeout_back_39);
        actions.pushBack(delay->clone());
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="key")
    {//鍵アニメ
        Common::playSE("p.mp3");
        
        auto key=AnchorSprite::create(Vec2(.5, .5), parent->getContentSize(),true, "sup_28_key_0.png");
        parent->addChild(key);
        auto fadein=TargetedAction::create(key, FadeIn::create(.7));
        auto sound=createSoundAction("kacha.mp3");
        auto key_1=createSpriteToCenter("sup_28_key_1.png", true, parent, true);
        auto change=createChangeAction(1, .7, key, key_1, "kacha.mp3");
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,sound,DelayTime::create(.7),change,DelayTime::create(.7),call,NULL));
    }
    else if(key=="bar")
    {//懸垂バー
        Common::playSE("p.mp3");
        auto bar=createSpriteToCenter("sup_35_bar.png", true, parent, true);
        auto put_bar=TargetedAction::create(bar, FadeIn::create(1));
        auto sound=createSoundAction("kacha.mp3");
        auto delay=DelayTime::create(1);
        
        auto monkey_0=createSpriteToCenter("sup_35_monkey_0.png", true, parent, true);
        auto monkey_1=createSpriteToCenter("sup_35_monkey_1.png", true, parent, true);
        
        auto fadein_0=TargetedAction::create(monkey_0, Spawn::create(FadeIn::create(1),
                                                                     createSoundAction("monkey.mp3"), NULL));
        auto fadein_1=createChangeAction(1, .7, monkey_0, monkey_1,"monkey.mp3");
        //おさる
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(put_bar,sound,delay,fadein_0,delay->clone(),fadein_1,delay->clone(),call, NULL));
    }
    else if(key=="glove")
    {
        Common::playSE("p.mp3");
        auto glove=createAnchorSprite("sup_46_glove.png", .75, .6, true, parent, true);
        
        auto seq=TargetedAction::create(glove, Sequence::create(FadeIn::create(1),
                                                                ScaleBy::create(1, .8),
                                                                DelayTime::create(.7),
                                                                NULL));
        auto spawn=Spawn::create(TargetedAction::create(glove, FadeOut::create(.5)),
                                 createChangeAction(.7, .3, parent->getChildByName("sup_46_rabbit_1_0_noglove"), createSpriteToCenter("sup_46_rabbit_1_0.png", true, parent, true)),
                                 createSoundAction("pop.mp3"),
                                  NULL);
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq,spawn,DelayTime::create(1),call, NULL));
    }
    else if(key=="handle"){
        auto put= putAction(parent, "sup_60_handle_0.png", "kacha.mp3", 1, callback);
        
        parent->runAction(put);
    }
    else if(key=="water")
    {//水を出す
        Common::playSE("twist.mp3");
        Vector<FiniteTimeAction*>actions;
        
        auto handle=createSpriteToCenter("sup_60_handle_1.png", true, parent,true);
        auto duration=.15;
        auto twist=TargetedAction::create(handle, Repeat::create(Sequence::create(FadeIn::create(duration),
                                                                                  DelayTime::create(duration),
                                                                                  FadeOut::create(duration),
                                                                                  DelayTime::create(duration), NULL), 2));
        actions.pushBack(twist);
        auto remove_handle=TargetedAction::create(handle, RemoveSelf::create());
        actions.pushBack(remove_handle);
        auto water=createSpriteToCenter("sup_60_water.png", true, parent,true);
        auto fadein_water=TargetedAction::create(water, Spawn::create(FadeIn::create(1),
                                                                      createSoundAction("jaa.mp3"), NULL));
        actions.pushBack(fadein_water);
        auto fadeout_water=TargetedAction::create(water, Sequence::create(DelayTime::create(1),
                                                                          FadeOut::create(1),
                                                                          RemoveSelf::create(),
                                                                          NULL));
        
        if (DataManager::sharedManager()->getSelectedItemID()==4) {
            auto petbottle=createSpriteToCenter("sup_60_pet_0.png", true, parent,true);
            auto petbottle_1=createSpriteToCenter("sup_60_pet_1.png", true, parent,true);
            auto seq_bottle=Sequence::create(TargetedAction::create(petbottle, FadeIn::create(.5)),
                                             DelayTime::create(1),
                                             createChangeAction(1, .5, petbottle, petbottle_1),
                                             DelayTime::create(.5),
                                             TargetedAction::create(petbottle_1, FadeOut::create(.7)),
                                             NULL);
            actions.pushBack(Spawn::create(fadeout_water,
                                           seq_bottle, NULL));
        }
        else{
            actions.pushBack(fadeout_water);
        }
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if (key=="drink"){
        //水を飲ます
        Common::playSE("p.mp3");
        
        auto water=createSpriteToCenter("sup_41_water.png", true, parent,true);
        auto pig_0=parent->getChildByName("sup_41_pig_0.png");
        auto pig_1=createSpriteToCenter("sup_41_pig_1.png", true, parent,true);
        
        auto fadein_water=TargetedAction::create(water, Sequence::create(FadeIn::create(1),
                                                                         createSoundAction("drink.mp3"), NULL));
        auto delay=DelayTime::create(1.5);
        auto change_pig=Spawn::create(TargetedAction::create(water, FadeOut::create(1)),
                                      createChangeAction(1, .5, pig_0, pig_1,"pig.mp3"),
                                      NULL);
        //move
        auto back_4=createSpriteToCenter("back_4.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back_4, 4);
        auto fadein_back=TargetedAction::create(back_4, FadeIn::create(1));
        auto move_pig=createChangeAction(1, .7, back_4->getChildByName("sup_4_pig_down_bike.png"), createSpriteToCenter("sup_4_pig_move.png", true, back_4, true),"pig.mp3");
        
        //screen
        auto back_43=createSpriteToCenter("back_43.png", true, parent,true);
        auto hand=createSpriteToCenter("sup_43_hand.png", true, back_43,true);
        hand->setPositionY(parent->getContentSize().height*.4);
        auto calorie=createSpriteToCenter("sup_43_calorie.png", true, back_43, true);
        
        auto fadein_back_43=TargetedAction::create(back_43, FadeIn::create(1));
        auto remove_back_4=TargetedAction::create(back_4, RemoveSelf::create());
        auto remove_pig_1=TargetedAction::create(pig_1, RemoveSelf::create());
        auto action_hand=TargetedAction::create(hand, Sequence::create(FadeIn::create(1),
                                                                       MoveTo::create(1, parent->getContentSize()/2),
                                                                       createSoundAction("pi.mp3"),
                                                                       delay->clone(),
                                                                       Spawn::create(MoveBy::create(1, Vec2(0, -parent->getContentSize().height*.1)),
                                                                                     FadeOut::create(1)
                                                                                     ,NULL),
                                                                       NULL));
        
        //run
        auto back_42=createSpriteToCenter("back_42.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back_42, 42);
        auto pig_run_0=createSpriteToCenter("sup_42_pig_1.png", false,back_42,true);
        auto pig_tired=createSpriteToCenter("sup_42_pig_6.png", true, back_42,true);
        auto fadein_back_42=TargetedAction::create(back_42, FadeIn::create(1));
        
        Vector<SpriteFrame*>frames;
        for (int i=1; i<6; i++) {
            frames.pushBack(EscapeStageSprite::createWithSpriteFileName(StringUtils::format("sup_42_pig_%d.png",i))->getSpriteFrame());
        }
        auto animation=Animation::createWithSpriteFrames(frames);
        animation->setDelayPerUnit(5.0f/60.0f);//??フレームぶん表示
        animation->setRestoreOriginalFrame(true);
        animation->setLoops(10);
        auto animate=Spawn::create(TargetedAction::create(pig_run_0, Animate::create(animation)),
                                   createSoundAction("weeen.mp3"),
                                   createSoundAction("run.mp3"), NULL);

        auto change_pig_run=createChangeAction(1, .5, pig_run_0, pig_tired,"pig.mp3");
        auto fadeout_back_42=TargetedAction::create(back_42, FadeOut::create(1));
        
        auto fadein_cal=TargetedAction::create(calorie, Spawn::create(FadeIn::create(1),
                                                                      createSoundAction("robot_start.mp3"), NULL));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_water,delay,change_pig,delay->clone(),fadein_back,delay->clone(),move_pig,delay->clone(),fadein_back_43,remove_pig_1,remove_back_4,delay->clone(),action_hand,delay->clone(),fadein_back_42,delay->clone(),animate,change_pig_run,delay->clone(),fadeout_back_42,delay->clone(),fadein_cal,delay->clone(), call, NULL));
    }
    else if (key=="protein"){
        //プロテインを飲ます
        Common::playSE("p.mp3");
        
        auto item=createSpriteToCenter("sup_44_protein.png", true, parent,true);
        auto pig_0=parent->getChildByName("sup_44_pig_0.png");
        auto pig_1=createSpriteToCenter("sup_44_pig_1.png", true, parent,true);
        
        auto fadein_item=TargetedAction::create(item, Sequence::create(FadeIn::create(1),
                                                                         createSoundAction("drink.mp3"), NULL));
        auto delay=DelayTime::create(1.5);
        auto change_pig=Spawn::create(TargetedAction::create(item, FadeOut::create(1)),
                                      createChangeAction(1, .5, pig_0, pig_1,"pig.mp3"),
                                      NULL);
        
        //macho
        auto back_45=createSpriteToCenter("back_45.png", true, parent,true);
        auto pig_2=createSpriteToCenter("sup_45_pig_1.png", true, back_45,true);
        auto pig_3=createSpriteToCenter("sup_45_pig_2.png", true, back_45, true);
        
        auto fadein_back_45=TargetedAction::create(back_45, FadeIn::create(1));
        auto remove_pig_1=TargetedAction::create(pig_1, RemoveSelf::create());
        auto action_pig=Sequence::create(createSoundAction("pop_4.mp3"),
                                         TargetedAction::create(pig_2, FadeIn::create(1)),
                                         DelayTime::create(1),
                                         createChangeAction(1, .3, pig_2, pig_3,"pig.mp3"),
                                         NULL);
        
        //heart
        auto back_85=createSpriteToCenter("back_85.png", true, parent,true);
        auto pig_heart=createSpriteToCenter("sup_85_pig.png", true, back_85,true);
        
        auto fadein_back_33=TargetedAction::create(back_85, FadeIn::create(1));
        
        auto fadein_pig_heart=TargetedAction::create(pig_heart, Spawn::create(FadeIn::create(1),
                                                                      createSoundAction("pig_1.mp3"), NULL));
        auto fadeout_back_33=TargetedAction::create(back_85, FadeOut::create(1));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_item,delay,change_pig,delay->clone(),fadein_back_45,remove_pig_1,delay->clone(),action_pig,delay->clone(),fadein_back_33,delay->clone(),fadein_pig_heart,delay->clone(),fadeout_back_33,delay->clone(),call, NULL));
    }
    else if(key=="lock"){
        //ロック解除
        Vector<FiniteTimeAction*>actions;
        auto delay=DelayTime::create(.5);
        actions.pushBack(delay);
        
        auto back_81=createSpriteToCenter("back_82.png", true, parent,true);
        auto fadein_back=TargetedAction::create(back_81, FadeIn::create(1));
        actions.pushBack(fadein_back);
        actions.pushBack(delay->clone());

        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="health"){
        //ヘルス起動
        Common::playSE("pi.mp3");
        auto selected=createSpriteToCenter("sup_82_selected.png", true, parent,true);
        auto selectAction=TargetedAction::create(selected, Sequence::create(FadeIn::create(.1),
                                                                            DelayTime::create(.5), NULL));

        auto back=createSpriteToCenter("back_83.png", true, parent,true);
        auto fadein_back=TargetedAction::create(back, Spawn::create(FadeIn::create(1),
                                                                    createSoundAction("robot_start.mp3"), NULL));
        auto delay=DelayTime::create(1);
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(selectAction,fadein_back,delay,call, NULL));
    }
    else if(key=="setRemocon"){
        Common::playSE("p.mp3");
        
        auto remocon=createSpriteToCenter("sup_24_switch.png", true, parent,true);
        
        
        auto fadein_remocon=TargetedAction::create(remocon, FadeIn::create(1));
        auto sound=createSoundAction("kacha.mp3");
        auto delay=DelayTime::create(1);
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_remocon,fadein_remocon,sound,delay,call, NULL));
    }
    else if(key=="openPiero"){
        auto back_74=createSpriteToCenter("back_74.png", true, parent,true);
        auto mouse=createSpriteToCenter("sup_74_mouse.png", true, back_74,true);
        auto cover=createSpriteToCenter("sup_74_cover.png", false, back_74,true);
        
        auto fadein_back=TargetedAction::create(back_74, FadeIn::create(1));
        auto fadein_mouse=TargetedAction::create(mouse, FadeIn::create(0));
        auto delay=DelayTime::create(1);
        auto slide=slideAction(parent, cover, true,DataManager::sharedManager()->getNowBack());
        auto fadeout_back=TargetedAction::create(back_74, FadeOut::create(1));
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_back,fadein_mouse,delay,slide,delay->clone(),fadeout_back,delay->clone(),call, NULL));
    }
    else if(key=="cloud"){
        Vector<FiniteTimeAction*>actions_spawn;
        
        auto num=DataManager::sharedManager()->getNowBack();
        std::string soundName="pig_1.mp3";
        
        auto cloud_anchor=Vec2(.65,.62);
        if (num==33) {
            actions_spawn.pushBack(createBounceAction(parent->getChildByName("sup_33_pig_1.png"), .3, .05));
        }
        else if(num==25){
            cloud_anchor=Vec2(.55,.67);
            soundName="manager_sigh.mp3";
        }
        else if(num==41){
            cloud_anchor=Vec2(.475,.67);
            soundName="pig.mp3";
        }
        else if(num==44){
            cloud_anchor=Vec2(.6,.67);
            soundName="pig.mp3";
        }
        
        Common::playSE(soundName.c_str());
        
        auto cloud=AnchorSprite::create(cloud_anchor, parent->getContentSize(), StringUtils::format("sup_%d_cloud.png",num));
        cloud->setOpacity(0);
        parent->addChild(cloud);
        auto original_scale=cloud->getScale();
        cloud->setScale(.1);
        
        auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(.5, original_scale),
                                                                                    FadeIn::create(.5), NULL),
                                                                      createSoundAction("nyu.mp3"),
                                                                      DelayTime::create(.5),
                                                                      Spawn::create(ScaleTo::create(.5, .1),
                                                                                    FadeOut::create(.5), NULL), NULL));
        actions_spawn.pushBack(seq_cloud);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(Spawn::create(actions_spawn),call, NULL));
    }
    else if(key=="givecard"){
        Common::playSE("p.mp3");
        auto pig_0=parent->getChildByName("sup_33_pig_1.png");
        auto pig_1=createSpriteToCenter("sup_33_pig_2.png", true, parent,true);
        auto pig_2=createSpriteToCenter("sup_33_pig_3.png", true, parent,true);
        auto give=giveAction(createSpriteToCenter("sup_33_pass.png", true, parent,true), 1, .8, true);
        auto bounce_pig=Spawn::create(createBounceAction(pig_0, .2, .1),
                                      jumpAction(parent->getContentSize().height*.05, .2, pig_0, 1.5),
                                      createSoundAction("pig_1.mp3"),NULL);
        auto delay=DelayTime::create(1);
        auto change_0=createChangeAction(1, .7, pig_0, pig_1,"pop.mp3");
        auto change_1=createChangeAction(1, .7, pig_1, pig_2,"pop.mp3");
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(give,delay,bounce_pig,delay->clone(),change_0,delay->clone(),change_1,delay->clone(),call, NULL));
    }
    else if(key=="driver"){
        Common::playSE("p.mp3");
        auto item=createAnchorSprite("sup_69_driver.png", .5, .4, true, parent, true);
        auto cover=parent->getChildByName("sup_69_cover.png");
        auto scale_original=item->getScale();
        auto pos_original=item->getPosition();
        item->setScale(scale_original*.8);
        item->setPositionY(parent->getContentSize().height*.585);
        auto fadein_item=TargetedAction::create(item, FadeIn::create(1));
        auto shake=Spawn::create(jumpAction(parent->getContentSize().height*.005, .1, item, 1.5, 3),
                                 createSoundAction("kacha_2.mp3"), NULL);
        auto delay=DelayTime::create(1);
        auto move=TargetedAction::create(item, Spawn::create(MoveTo::create(1, pos_original),
                                                             ScaleTo::create(1, scale_original), NULL));
        auto fadeouts=Spawn::create(TargetedAction::create(item, Spawn::create(FadeOut::create(.5),
                                                                               MoveBy::create(.5, Vec2(0, parent->getContentSize().height*.2)), NULL)),
                                    TargetedAction::create(cover, FadeOut::create(1)), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_item,delay,shake,delay->clone(),move,delay->clone(),shake->clone(),delay->clone(),fadeouts,delay->clone(),call, NULL));
    }
    else if(key=="cd"){
        Common::playSE("p.mp3");
        auto item=createAnchorSprite("sup_69_cd.png", .5, .4, true, parent, true);
        auto back_68=createSpriteToCenter("back_68.png", true, parent, true);
        createSpriteToCenter("sup_68_cd.png", false, back_68,true);
        
        auto fadein_item=TargetedAction::create(item, Sequence::create(FadeIn::create(1),
                                                                       createSoundAction("kacha.mp3"), NULL));
        auto delay=DelayTime::create(1);
        auto fadein_back=TargetedAction::create(back_68, FadeIn::create(1));
        auto sounds=CallFunc::create([back_68,this]{
            Common::playBGM("dance_bgm.mp3");
            
            this->addWaveParticles(back_68);
        });
        auto delay_1=DelayTime::create(6);
        
        auto back_66=createSpriteToCenter("back_66.png", true, parent, true);
        back_66->setLocalZOrder(1);
        SupplementsManager::getInstance()->addSupplementToMain(back_66, 66,true);
        auto fadein_back_66=Sequence::create(TargetedAction::create(back_66, FadeIn::create(1)),
                                             TargetedAction::create(back_68, RemoveSelf::create()), NULL);
        
        auto dance=danceAction(back_66, 66, false);
        auto fadeout_back_66=TargetedAction::create(back_66, FadeOut::create(1));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_item,delay,fadein_back,delay->clone(),sounds,delay_1,fadein_back_66,delay->clone(),dance,delay->clone(),fadeout_back_66,call, NULL));
    }
    else if(key=="towel"){
        Common::playSE("p.mp3");
        auto mark=createSpriteToCenter("sup_64_mark.png", true, parent, true);
        auto towel=createAnchorSprite("sup_64_towel.png", .5, .5, true, parent, true);
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(TargetedAction::create(towel, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createSoundAction("goshigoshi.mp3"));
        
        Vector<FiniteTimeAction*>moves;
        auto move_duration=.35;
        for (int i=0; i<7; i++) {
            auto distance_x=0;
            auto distance_y=parent->getContentSize().height*.2;
            if (i%2==0) {
                distance_y*=-1;
            }
            else{
                distance_x=parent->getContentSize().width*.25;
            }
            
            auto move=TargetedAction::create(towel, EaseInOut::create(MoveBy::create(move_duration, Vec2(distance_x, distance_y)), 1.5));
            moves.pushBack(move);
        }
        actions.pushBack(Spawn::create(Sequence::create(moves),
                                       Sequence::create(DelayTime::create(move_duration*5),
                                                        TargetedAction::create(mark, FadeIn::create(move_duration*2)), NULL), NULL));
        actions.pushBack(TargetedAction::create(towel, FadeOut::create(.7)));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="coin"){
        Common::playSE("p.mp3");
        auto smile=createSpriteToCenter("sup_25_smile.png", true, parent, true);
        auto give=giveAction(createAnchorSprite("sup_25_coin.png", .7, .5, true, parent, true), 1, .7, true);
        
        auto delay=DelayTime::create(1);
        auto fadein_smile=TargetedAction::create(smile, Spawn::create(FadeIn::create(1),
                                                                      createSoundAction("manager_2.mp3"), NULL));
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(give,delay,fadein_smile,delay->clone(),call, NULL));
    }
    else if(key=="getcard"){
        auto cardkey=parent->getChildByName("sup_73_card_key.png");
        auto fadeout_card=TargetedAction::create(cardkey, FadeOut::create(1));
        auto delay=DelayTime::create(1);
        
        auto call=CallFunc::create([callback,parent]{
            auto addStory=CallFunc::create([callback]{
                auto story=StoryLayer::create("ending", [callback]{
                    callback(true);
                });
                Director::getInstance()->getRunningScene()->addChild(story);
            });
            parent->runAction(addStory);
        });
        parent->runAction(Sequence::create(fadeout_card,delay,call, NULL));
    }
    else if(key=="card_key"){
        Common::playSE("p.mp3");
        auto ok=createSpriteToCenter("sup_27_ok.png", true, parent,true);
        auto card=createSpriteToCenter("sup_27_card_key.png", true, parent,true);
        auto scale_original=card->getScale();
        card->setScale(scale_original*1.5);
        
        auto delay=DelayTime::create(1);
        auto seq_card=TargetedAction::create(card, Sequence::create(Spawn::create(FadeIn::create(1),
                                                                                  ScaleTo::create(1, scale_original), NULL),
                                                                    createSoundAction("pi.mp3"),
                                                                    DelayTime::create(1),
                                                                    Spawn::create(TargetedAction::create(ok, FadeIn::create(1)),
                                                                                  FadeOut::create(1),
                                                                                  ScaleBy::create(1, 1.25), NULL)
                                                                    , NULL));
        
        //scene change
        auto back_26=createSpriteToCenter("back_26.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back_26, 26);
        auto fadein_back_26=TargetedAction::create(back_26, FadeIn::create(1));
        auto close=back_26->getChildByName("sup_26_close.png");
        auto open=createSpriteToCenter("sup_26_open.png", true, back_26,true);
        
        auto open_action=createChangeAction(1, .5, close, open,"gacha.mp3");
        
        auto addStory=CallFunc::create([parent]{
            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

            auto story=StoryLayer::create("ending2", [parent]{
                
                auto call=CallFunc::create([parent](){
                    Director::getInstance()->replaceScene(TransitionFade::create(8, ClearScene::createScene(), Color3B::WHITE));
                });
                
                parent->runAction(Sequence::create(call, NULL));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        parent->runAction(Sequence::create(seq_card,delay,fadein_back_26,delay->clone(),open_action,delay->clone(),addStory, NULL));
    }
    else if(key=="dancestudio"&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(40))
    {
        Common::playBGM("dance_bgm.mp3");
        callback(true);
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void SportsGymActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造
   
}

#pragma mark - Custom
void SportsGymActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    if (key == CustomAction_ReviewLayer) {
        DataManager::sharedManager()->removeItemWithID(19);
    }
}

void SportsGymActionManager::arrowAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="dance_studio"&&
       DataManager::sharedManager()->getEnableFlagWithFlagID(40)){
        EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(false).c_str(), true);

    }
    
    callback(true);
}

#pragma mark - Show Answer
void SportsGymActionManager::showAnswerAction(std::string key, cocos2d::Node *parent)
{
    if (key=="piero") {
        backAction("spring", 24, parent);
        
        auto answer=DataManager::sharedManager()->getBackData(24)["useInput"].asValueMap()["answer"].asValueMap()["answer"].asString();
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(1));
        for (int i=0; i<answer.size(); i++) {
            auto index=atoi(answer.substr(i,1).c_str());
            auto action=buttonAction(index, parent);
            auto action_1=balloonAction(parent, 24, index);
            actions.pushBack(Spawn::create(action,action_1, NULL));
            actions.pushBack(DelayTime::create(.3));
        }
        actions.pushBack(DelayTime::create(1));
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
    else if(key=="back_72"){
        auto backData=DataManager::sharedManager()->getBackData(72);
        SupplementsManager::getInstance()->addLabelSupplement(parent,false,0, backData, ShowType_Answer, {"19","16","9","19"});
    }
}

#pragma mark - Private
FiniteTimeAction* SportsGymActionManager::buttonAction(int index, cocos2d::Node *parent)
{
    auto button=parent->getChildByName(StringUtils::format("sup_24_%d.png",index));

    return TargetedAction::create(button, Sequence::create(createSoundAction("kacha.mp3"),FadeOut::create(0),DelayTime::create(.1),FadeIn::create(.1), NULL));
}

Spawn* SportsGymActionManager::balloonAction(cocos2d::Node *parent, int backID,int index)
{//びよん
    auto spring=parent->getChildByName(StringUtils::format("sup_%d_spring.png",backID));
    auto balloon=spring->getChildByName(StringUtils::format("sup_%d_balloon.png",backID));
    
    std::vector<float> skews={-7.5,-5,5,7.5};
    auto skew=skews.at(index);
    std::vector<float> angles={-40.0,-15,15,40};
    auto angle=angles.at(index);
    auto ease=1.5;
    auto duration=.5;
    //fadein
    auto spring_action=TargetedAction::create(spring, Sequence::create(EaseInOut::create(Spawn::create(SkewTo::create(duration/2, skew, 1),
                                                                                                       RotateBy::create(duration/2, angle/2), NULL), ease),
                                                                       EaseInOut::create(Spawn::create(SkewTo::create(duration/2, 0, 0),
                                                                                                       RotateBy::create(duration/2, -angle/2), NULL), ease), NULL));
    auto balloon_action=TargetedAction::create(balloon, Sequence::create(EaseInOut::create(RotateBy::create(duration/2, angle/2), ease),
                                                                         CallFunc::create([backID]{
        if (backID==24) {
            Common::playSE("spring.mp3");
        }
    }),
                                                                         EaseInOut::create(RotateBy::create(duration/2, -angle/2), ease), NULL));
    
    
    
    auto spawn=Spawn::create(spring_action,balloon_action, NULL);
    
    return spawn;
}

Sequence* SportsGymActionManager::danceAction(cocos2d::Node *parent, int backID, bool complete)
{
    Vector<FiniteTimeAction*>actions;
    auto jump_distance=parent->getContentSize().height*.4;
    auto jump_duration=.7;
    auto pig_1=parent->getChildByName("sup_66_pig_1.png");
    auto default_pos_1=pig_1->getPosition();
    auto pig_2=createSpriteToCenter("sup_66_pig_2.png", true, parent,true);//jump
    auto default_pos_2=pig_2->getPosition();
    auto pig_3=createAnchorSprite("sup_66_pig_3.png", .5, .1, true, parent, true);//land
    auto default_pos_3=pig_3->getPosition();
    auto pig_4=createAnchorSprite("sup_66_pig_4.png", .5, .1, true, parent, true);//leg up
    auto default_pos_4=pig_4->getPosition();
    auto default_anchor_4=pig_4->getAnchorPoint();
    auto pig_5=createAnchorSprite("sup_66_pig_5.png", .5, .1, true, parent, true);//stand by on arm
    auto default_pos_5=pig_5->getPosition();
    auto default_anchor_5=pig_5->getAnchorPoint();
    auto pig_6=createAnchorSprite("sup_66_pig_6.png", .5, .1, true, parent, true);//up one leg
    //jump
    auto jump_1=Sequence::create(createBounceAction(pig_1, .15, .05),
                                 Spawn::create(TargetedAction::create(pig_1, EaseOut::create(MoveBy::create(jump_duration, Vec2(0, jump_distance)), 1.5)),
                                               Sequence::create(DelayTime::create(.2),
                                                                createChangeAction(.3, .15, pig_1, pig_2, "pop_4.mp3"),
                                                                NULL),
                                               NULL),
                                 TargetedAction::create(pig_1, MoveTo::create(0, default_pos_1)),
                                 NULL);
    actions.pushBack(jump_1);
    
    //landing
    auto land=Spawn::create(EaseIn::create(TargetedAction::create(pig_2, MoveBy::create(jump_duration, Vec2(0, -jump_distance))), 1.5),
                            Sequence::create(DelayTime::create(.3),
                                             createChangeAction(.3, .15, pig_2, pig_3),
                                             TargetedAction::create(pig_2, MoveTo::create(0, default_pos_2)),
                                             createSoundAction("landing.mp3"),
                                             createBounceAction(pig_3, .15,.05),
                                             NULL),
                            NULL);
    actions.pushBack(land);
    
    if (!complete) {
        return Sequence::create(actions);
    }
    
    actions.pushBack(DelayTime::create(.5));
    
    //jump&turn
    auto jumpAndTurn=Sequence::create(createChangeAction(.5, .25, pig_3, pig_4,"pop.mp3"),
                                      DelayTime::create(1),
                                      createBounceAction(pig_4, .2),
                                      changeAnchorAction(parent, pig_4, Vec2(.5, .4)),
                                      Spawn::create(jumpAction(Vec2(-parent->getContentSize().width*.05, jump_distance*1.5), jump_duration, pig_4, 1.5),
                                                    Sequence::create(DelayTime::create(jump_duration),
                                                                     createSoundAction("rolling.mp3"),
                                                                     TargetedAction::create(pig_4,EaseInOut::create(RotateBy::create(jump_duration*.4, -135), 1.5)),
                                                                     NULL),
                                                    Sequence::create(DelayTime::create(jump_duration*1.7),
                                                                     createChangeAction(jump_duration*.3, jump_duration*.15, pig_4, pig_5)
                                                                     , NULL)
                                                    , NULL),
                                      createSoundAction("landing.mp3"),
                                      createBounceAction(pig_5, .15,.05),
                                      CallFunc::create([pig_4,default_anchor_4,default_pos_4]{
        pig_4->setRotation(0);
        pig_4->setPosition(default_pos_4);
        pig_4->setAnchorPoint(default_anchor_4);
    }),
                                      NULL);
    actions.pushBack(jumpAndTurn);
    actions.pushBack(DelayTime::create(.5));
    //jumpAndTurn_2
    auto jumpAndTurn_2=Sequence::create(
                                      createBounceAction(pig_5, .2),
                                      Spawn::create(jumpAction(jump_distance*1.5, jump_duration, pig_5, 1.25),
                                                    Sequence::create(DelayTime::create(jump_duration),
                                                                     changeAnchorAction(parent, pig_5, Vec2(.5, .4)),
                                                                     createSoundAction("rolling.mp3"),
                                                                     TargetedAction::create(pig_5, RotateBy::create(jump_duration*.4, 135)),
                                                                     NULL),
                                                    Sequence::create(DelayTime::create(jump_duration*1.7),
                                                                     createChangeAction(jump_duration*.3, jump_duration*.15, pig_5, pig_6)
                                                                     , NULL)
                                                    , NULL),
                                    
                                        createSoundAction("landing.mp3"),
                                      createBounceAction(pig_6, .15,.05),
                                      CallFunc::create([pig_5,default_anchor_5,default_pos_5]{
        pig_5->setRotation(0);
        pig_5->setPosition(default_pos_5);
        pig_5->setAnchorPoint(default_anchor_5);
    }),
                                      NULL);
    actions.pushBack(jumpAndTurn_2);
    actions.pushBack(DelayTime::create(1));
    actions.pushBack(createChangeAction(.5, .25, pig_6, pig_1));
    actions.pushBack(DelayTime::create(1.5));
    
    return Sequence::create(actions);
}

FiniteTimeAction* SportsGymActionManager::slideAction(cocos2d::Node *parent, cocos2d::Node *node, bool animate,int backID)
{
    auto distance=parent->getContentSize().height*.4;
    if (animate) {
        return TargetedAction::create(node, Spawn::create(createSoundAction("su.mp3"),
                                                          EaseInOut::create(MoveBy::create(1, Vec2(0, distance)), 1.5), NULL));
    }
    else{
        node->setPositionY(node->getPositionY()+distance);
        return NULL;
    }
}

void SportsGymActionManager::addWaveParticles(Node*parent)
{
    for(int i=0;i<4;i++){
        auto pos_x=parent->getContentSize().width*.25;
        auto pos_y=parent->getContentSize().height*.875;
        if (i<2) {
            pos_x=parent->getContentSize().width*.745;
        }
        if (i%2==1) {
            pos_y=parent->getContentSize().height*.735;
        }
        
        auto particle=ParticleSystemQuad::create("particle_wave.plist");
        particle->setAutoRemoveOnFinish(true);
        particle->setDuration(-1);
        particle->setStartSize(parent->getContentSize().width*.05);
        particle->setEndSize(parent->getContentSize().width*.2);
        particle->resetSystem();
        particle->setLife(.75);
        particle->setTotalParticles(5);
        particle->setStartColor(Color4F(0, 200, 0, 200));
        particle->setEndColor(Color4F(0, 200, 0, 0));
        particle->setPosition(Vec2(pos_x, pos_y));
        parent->addChild(particle);
    }
}
