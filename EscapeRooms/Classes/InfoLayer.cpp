//
//  InfoLayer.cpp
//  Kebab
//
//  Created by yamaguchinarita on 2016/12/21.
//
//

#include "InfoLayer.h"
#include "Utils/Common.h"
#include "UIParts/MenuItemScale.h"
#include "UIParts/CustomSwitch.h"
#include "Utils/NativeBridge.h"
#include "SettingDataManager.h"
#include "DataManager.h"
#include "Utils/CustomAlert.h"

using namespace cocos2d;

bool InfoLayer::init() {
    if (!LayerColor::initWithColor(Color4B(0, 0, 0, 0))) {
        return false;
    }
    
    // 初期化
    addTouchListener();
    
    createMain();
    
    showAnimation();
    
    return true;
}

#pragma mark- UI
void InfoLayer::createMain()
{
    auto size=Size(getContentSize().width*.8, getContentSize().height*.3);
    
    mainSprite=Sprite::create();
    mainSprite->setColor(Color3B(230, 230, 230));
    mainSprite->setTextureRect(Rect(0, 0, size.width, size.height));
    mainSprite->setPosition(getContentSize().width/2, getContentSize().height/2);
    addChild(mainSprite);
    
    //タイトルラベル配置    
    titleLabel=Label::createWithTTF(AlertTitle_INFORMATION, MyFont, size.height*.1);
    titleLabel->setTextColor(Color4B(Common::getColorFromHex(BlackColor)));
    titleLabel->setHeight(size.height*.12);
    titleLabel->setPosition(mainSprite->getContentSize().width/2, mainSprite->getContentSize().height*.9);
    mainSprite->addChild(titleLabel);
    
    //確認用
    /*
    auto ssss = Sprite::create();
    ssss->setTextureRect(Rect(0, 0, titleLabel->getBoundingBox().size.width, titleLabel->getBoundingBox().size.height));
    ssss->setColor(Color3B::ORANGE);
    ssss->setPosition(titleLabel->getPosition());
    mainSprite->addChild(ssss);
    */
    
    //コンテンツ部分
    auto contents = Sprite::create();
    contents->setTextureRect(Rect(0, 0, size.width*.9, size.height*.6));
    
    //contents->setOpacity(0);
    contents->setColor(mainSprite->getColor());
    contents->setCascadeOpacityEnabled(true);

    contents->setPosition(mainSprite->getContentSize()/2);
    mainSprite->addChild(contents);
    
    log("ウィッジ%f",mainSprite->getContentSize().width);
    
    //サウンドと開発者
    std::vector<std::string> contentTexts = {
        DataManager::sharedManager()->getSystemMessage("sound"),
        DataManager::sharedManager()->getSystemMessage("auto_delete_cache"),
        DataManager::sharedManager()->getSystemMessage("other_app")};
    
    float contentsFontSize = contents->getBoundingBox().size.height/(contentTexts.size() + 3);
    
    float maxLabelWidth = 0;
    for (auto text : contentTexts) {
        auto cLabel = Label::createWithTTF(text, Common::getUsableFontPath(MyFont), contentsFontSize);

        maxLabelWidth = MAX(maxLabelWidth, cLabel->getContentSize().width);
    }
    
    for (int i=0; i < contentTexts.size(); i++) {
        float originY = contents->getBoundingBox().size.height*(contentTexts.size()-i)/contentTexts.size();
        
        //メニュー名
        auto contentsLabel = Label::createWithTTF(contentTexts.at(i), Common::getUsableFontPath(MyFont), contentsFontSize);
        contentsLabel->setTextColor(Color4B(Common::getColorFromHex(BlackColor)));
        
        if (i == 0 || i == 1) {//サウンド,画像データ削除
            //ラベルの位置を調整して貼る
            contentsLabel->setPosition(contentsLabel->getContentSize().width/2, originY-contentsLabel->getBoundingBox().size.height/2);
            contents->addChild(contentsLabel);

            //スイッチ
            auto _roleSwitch = CustomSwitch::create(MAX(contentsFontSize, 45) , [this,i](Ref* sender){
                
                auto switchButton=(CustomSwitch*)sender;
                auto on=switchButton->isOn();
                
                switch (i) {
                    case 0:
                    {
                        SettingDataManager::sharedManager()->setStopSe(!on);
                        SettingDataManager::sharedManager()->setStopBgm(!on);
                    }
                        break;
                    case 1:
                    {
                        SettingDataManager::sharedManager()->setAutoRemoveCathe(on);
                        break;
                    }
                        
                    default:
                        break;
                }
            });
            _roleSwitch->setOnColor(Common::getColorFromHex(MyColor));
            _roleSwitch->setOffColor(Color3B::GRAY);
            
            switch (i) {
                case 0://サウンド
                    _roleSwitch->setOn(!SettingDataManager::sharedManager()->getStopSe());

                    break;
                case 1:
                    _roleSwitch->setOn(SettingDataManager::sharedManager()->getAutoRemoveCathe());

                    break;
                    
                default:
                    break;
            }

            _roleSwitch->setPosition(contents->getBoundingBox().size.width-_roleSwitch->getSwitchSize().width/2, contentsLabel->getPositionY());
            contents->addChild(_roleSwitch);
        }
        else{//デベロッパー
            
            auto siteMenuItem = MenuItemLabel::create(contentsLabel, [](Ref*sender){
                //韓国語と英語圏なら語尾につく
                NativeBridge::openUrl(Common::localizeURL("http://nakayubi-corp.jp/products/products-escapegames").c_str());
            });
            
            siteMenuItem->setPosition(contents->getBoundingBox().size.width/2, originY-siteMenuItem->getBoundingBox().size.height/2);
            
            auto siteMenu = Menu::create(siteMenuItem, NULL);
            siteMenu->setPosition(0, 0);
            contents->addChild(siteMenu);
        }
    }
    
    //選択ボタン
    auto buttonSize=Size(size.width*.85, size.height*.16);
    
    Vector<MenuItem*> menuItems;
    
    auto butBack=Sprite::create();
    butBack->setTextureRect(Rect(0, 0, buttonSize.width, buttonSize.height));
    butBack->setColor(Common::getColorFromHex(MyColor));
    
    auto menuText = "CLOSE";
    
    float fontSize = buttonSize.height*.6;

    auto butLabel=Label::createWithTTF(menuText, MyFont, fontSize);
    butLabel->setAlignment(TextHAlignment::CENTER);
    butLabel->setPosition(butBack->getContentSize()/2);
    butBack->addChild(butLabel);
    
    auto item=MenuItemScale::create(butBack, butBack, [this](Ref*sender){
        Common::playClick();

        //フェードアウトしながら、小さくなる。
        float duration = .15;
        mainSprite->setCascadeOpacityEnabled(true);//mainspriteの透明度を子供にも反映
        auto fadeout = FadeOut::create(duration) ;
        auto mainFadeout = TargetedAction::create(mainSprite, Spawn::create(FadeOut::create(duration), ScaleTo::create(duration, .85), NULL));
        auto spawnFadeout = Spawn::create(fadeout, mainFadeout, NULL);
        auto remove = RemoveSelf::create();
        this->runAction(Sequence::create(spawnFadeout, remove, NULL));
    });
    item->setPosition(mainSprite->getContentSize().width/2, size.height*.1);
    
    menuItems.pushBack(item);
    
    auto doneMenu=Menu::createWithArray(menuItems);
    doneMenu->setPosition(0, 0);
    mainSprite->addChild(doneMenu);
}

void InfoLayer::showAnimation()
{
    /*
    //アニメーション
    mainSprite->setScale(.9);
    auto duration = .2;
    auto ease = EaseOut::create(ScaleTo::create(duration, 1), 2);
    auto scaleup = TargetedAction::create(mainSprite, ease);
    auto selfFadein = FadeTo::create(duration, 255/2);
    
    auto scaleAndFadein = Spawn::create(scaleup, selfFadein, NULL);
    
    runAction(scaleAndFadein);*/
    
    //アニメーション
    mainSprite->setScale(.8);
    auto ease = TargetedAction::create(mainSprite, EaseOut::create(ScaleTo::create(.15, 1.15), 2));
    auto ease2 = TargetedAction::create(mainSprite, EaseOut::create(ScaleTo::create(.1, 1), 2));
    auto selfFadein = FadeTo::create(.15, 80);
    auto spawn = Spawn::create(selfFadein, ease2, NULL);//背景がフェードインしながら、元のサイズに戻る
    
    runAction(Sequence::create(ease, spawn, NULL));
}

#pragma mark- タッチ
void InfoLayer::addTouchListener()
{
    auto listener=EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);//タッチ透過しない
    
    listener->onTouchBegan=[this](Touch*touch, Event*event)
    {
        return true;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

#pragma mark- 解放
InfoLayer::~InfoLayer()
{
    log("Infoレイヤーを解放する。");
}


