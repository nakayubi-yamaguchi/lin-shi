//
//  PromoteHintLayer.hpp
//  Princess
//
//  Created by ナカユビ on 2017/09/10.
//
//

#ifndef PromoteHintLayer_hpp
#define PromoteHintLayer_hpp


#include "cocos2d.h"
#include "Utils/CustomAlert.h"
#include "UIParts/GaugeSprite.h"
#include "UIParts/NCCollectionView.h"
#include "ShowType.h"

USING_NS_CC;

class PromoteHintLayer : public CustomAlert, CollectionViewDataSource, CollectionViewDelegate
{
    
public:
    
    static PromoteHintLayer* create(const ccMenuCallback &callback);
    
    virtual bool init();
    virtual bool init(const ccMenuCallback &callback);
    
    CREATE_FUNC(PromoteHintLayer);
    
private:
    float hintButtonWidth;
    float hintButtonHeight;
    float hintButtonSpace;
    
    /**ヒント表示用に立てたフラグキー.閉じた際にresetする*/
    std::vector<std::string>temporaryKeysOnHint;
    
    /**ヒント表示時の画像サイズ比率*/
    Vec2 showImageRatio=Vec2(1.0, 1.1);
    
    /*処理高速化のため保持する**/
    float flagID;

    int hintCount;
    
    /**横を1とした時の縦の比率*/
    float aspectRatio;
    /**answer表示時に画像を使用する*/
    ValueMap useImageInAnswerMap;
    /**ヒントボタンをならべる*/
    Sprite *m_hintSprite;
    
    /**プログレスバー*/
    GaugeSprite* progressBar;
    
    /**セルのサイズ*/
    Size cellSize;
    
    /**ヒント、答え、解説全ての配列。この順番で表示する*/
    ValueVector allHintVector;
    
    /**コレクション表示に対応しているか*/
    bool isCollection;
    
    /**flag.plistの書き方がEscapeRooms用かどうか。*/
    bool isEscapeRoomsGrammer;
    
    /**ヒントがいくつまで解放されているか。ヒントを解放とかしたら、リセットする*/
    int m_releasedHintNum;
    
    /**解放されているヒントたち*/
    ValueVector m_releasedHints;
    
    /**コレクションの場合、横に何個並べるか*/
    int m_inColumnCount;
    
    /**行数*/
    int m_rowCount;
    
    /**コレクションビュー*/
    NCCollectionView* m_collectionView;
    
#pragma mark- 初期化
    /**m_releasedHintNumを更新*/
    void loadReleasedNum();
    
    void createLayout();

    /** progress bar*/
    void createProgressBar();
    
    /**ヒントボタン部分を生成.リロードする場合もこれを呼ぶ*/
    void createHintButtonPart();
    
    /**コレクションビューを生成*/
    void createCollection();
    
    /**行数を読み込み*/
    void loadRowForCollection();
    
#pragma mark - 画像アラート表示
    /**詳細用アラートを表示*/
    void showDetailAlert(int index);
    
    /**解答用のアラートを表示*/
    void showAnswerImage();
    
    /**ヒント用のアラートを表示*/
    void showHintAlert(int index);

    /**ヒント表示する際のコールバックメソッド*/
    void pushShowHintButton(int index);

    /**ヒントを解放したことを記憶(動画、コイン)*/
    void recordReleasedHint(bool byCoin);
    
#pragma mark- UI作成、配置関連
    /**メインスプライトのレイアウトを整える*/
    void layoutMainSprite();
    
    /**ヒント画像を表示します。*/
    Sprite *createHintSprite(int index,ShowType showType);
    
    /**回答画像を表示します。*/
    Sprite *createAnswerSprite(ShowType showType);
    
    /**一覧で表示するボタンへラベル貼り付け*/
    void addButtonNameLabel(Sprite *parent, int index);
    
    /**プログレスバーの位置を調整*/
    void setProgressBarPos();
    
    /**コレクションのレイアウトを取得*/
    CollectionLayout getCollectionLayout();
    
    /**コレクションの高さを計算*/
    float getCollectionHeight();
    
#pragma mark- 表示テキスト関連
    /**本アラートに表示するメッセージを作成*/
    std::string createMessage();
    
    /**一覧で表示されるテキストを取得*/
    std::string getDisplayNameInList(int hintIndex);
    
    /**ヒント、アンサー詳細表示時のテキストを取得*/
    std::string getDetailText(int hintIndex);
    
    /**ヒント名を返す。例：Hint1 とか Answer*/
    std::string getHintTitleName(int hintIndex);
    
    /**ヒントのテキストを返す。*/
    std::string getHintText(int hintIndex);
    
#pragma mark- 判定関連
    /**ヒント画像を表示する仕様かどうか*/
    bool hasBackId(ValueMap hintMap);
    
    /**ヒントが見られるか確認*/
    bool isReleasedHint(int index);
    
    /**回答表示かどうか*/
    bool isAnswer(int index);
    
    /**コレクションに表示するセルの個数。*/
    int getDisplayHintCount();

#pragma mark- コレクション用
    //コレクションデータソース
    virtual ssize_t numberOfCellsInCollectionView(NCCollectionView *collection);
    virtual Size collectionCellSizeForIndex(NCCollectionView *collection, ssize_t idx);
    virtual CollectionViewCell* collectionCellAtIndex(NCCollectionView *collection, ssize_t idx);
    
    //コレクションデリゲート
    virtual void collectionCellTouched(NCCollectionView* collection, CollectionViewCell* cell);
    
#pragma mark- 解放
    ~PromoteHintLayer();
};

#endif /* PromoteHintLayer_hpp */
