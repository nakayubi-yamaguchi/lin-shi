//
//  TitleScene.h
//  Kebab
//
//  Created by yamaguchinarita on 2016/12/21.
//
//

#ifndef __Kebab__TitleScene__
#define __Kebab__TitleScene__

#include "cocos2d.h"
USING_NS_CC;

class TitleScene : public cocos2d::LayerColor
{
public:
    virtual bool init();
    static Scene *createScene();
    CREATE_FUNC(TitleScene);
    bool isTransition;
    
    void onEnterTransitionDidFinish();
    
    ~TitleScene();
private:
    
    void createMenus();
    void showSettingLayer();
    void replaceToGameScene();
    void recordTitleData();
    
    void addNotification();
    void showIS(float delay);
    
};


#endif /* defined(__Kebab__TitleScene__) */
