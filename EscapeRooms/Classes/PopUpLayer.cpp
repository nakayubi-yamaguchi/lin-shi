//
//  PopUpSprite.cpp
//  Kebab
//
//  Created by 成田凌平 on 2016/12/19.
//
//

#include "PopUpLayer.h"
#include "DataManager.h"
#include "AnimateLayer.h"
#include "Utils/Common.h"
#include "StoryLayer.h"
#include "ZOrders.h"
#include "EscapeDataManager.h"


#include "Utils/ShakeGestureManager.h"

#include "SupplementsManager.h"
#include "ClearScene.h"
#include "AnswerManager.h"
#include "TouchManager.h"
#include "EscapeStageSprite.h"

#include "WarpManager.h"

using namespace cocos2d;

PopUpLayer*PopUpLayer::create(const popupCloseCallback &callback)
{
    auto node=new PopUpLayer();
    if (node&&node->init(callback)) {
        node->autorelease();
        return node;
    }
    CC_SAFE_RELEASE(node);
    
    return node;
}

bool PopUpLayer::init(const popupCloseCallback &callback)
{
    if (!LayerColor::initWithColor(Color4B(0, 0, 0, 150))) {
        return false;
    }
    
    m_callback=callback;
    
    setColor(Color3B::BLACK);
    setLocalZOrder(Zorder_Menu);
    DataManager::sharedManager()->setShowingAngle(0);
    setEnable(false);
    
    //データロード
    loadData();
    
    //タッチを検知用
    addEventListner();
    
    //
    showItemImage();
    return true;
}

#pragma mark- 
void PopUpLayer::loadData()
{
    log("アイテムデータをロード %d",DataManager::sharedManager()->getShowingItemID());
    AnswerManager::getInstance()->resetPasscode(true);
    DataManager::sharedManager()->reloadNowItem();
    itemMap=DataManager::sharedManager()->getItemData(DataManager::sharedManager()->getShowingItemID());
}

#pragma mark 補完画像
void PopUpLayer::addSupplements()
{
    SupplementsManager::getInstance()->addSupplementToPopUp(this->itemImage, DataManager::sharedManager()->getShowingItemID(), DataManager::sharedManager()->getShowingAngle(), true, ShowType_Normal);
    SupplementsManager::getInstance()->clearPatternSprites(true);

    //(this->itemImage, DataManager::sharedManager()->getShowingItemID(), DataManager::sharedManager()->getShowingAngle());
    
    /*
    //label
    SupplementsManager::getInstance()->addLabelSupplement(this->itemImage, true, DataManager::sharedManager()->getShowingItemID(), ShowType_Normal);
    
    //input
    SupplementsManager::getInstance()->addInputSupplement(this->itemImage, true, DataManager::sharedManager()->getShowingItemID(), ShowType_Normal);
    
    //pattern
    SupplementsManager::getInstance()->clearPatternSprites(true);
    //
    SupplementsManager::getInstance()->addSwitchSupplements(this->itemImage, true, DataManager::sharedManager()->getShowingItemID(), ShowType_Normal);
    //
    SupplementsManager::getInstance()->addParticle(this->itemImage,DataManager::sharedManager()->getShowingItemID(), true);
    //
    SupplementsManager::getInstance()->addMysterySup(this->itemImage, true,DataManager::sharedManager()->getShowingItemID());*/

    //custom
    SupplementsManager::getInstance()->getCustomActionManager()->itemBackAction(itemMap["customActionKey"].asString(), this);
     
}

#pragma mark- UI
void PopUpLayer::showItemImage()
{
    float width=getContentSize().width*.8;//高さは幅より常に高いので幅基準ではみ出すことはない
    
    backImage=Sprite::create();
    backImage->setTextureRect(Rect(0, 0, width, width));
    backImage->setCascadeOpacityEnabled(true);
    backImage->setColor(Color3B(240, 240, 240));
    backImage->setPosition(getContentSize()/2);
    backImage->setOpacity(0);
    addChild(backImage);
    
    
    //main
    itemImage = EscapeStageSprite::createWithSpriteFileName(getShowingFileName());
    itemImage->setScale(width/itemImage->getContentSize().width);
    itemImage->setPosition(backImage->getContentSize()/2);
    itemImage->setCascadeOpacityEnabled(true);
    backImage->addChild(itemImage);
    
    //clip
    auto stencil=Sprite::create();
    stencil->setTextureRect(Rect(0, 0, width, width));
    stencil->setPosition(backImage->getContentSize()/2);
    
    clip=ClippingNode::create();
    clip->setStencil(stencil);
    clip->setAlphaThreshold(0);
    clip->setInverted(false);
    clip->setCascadeOpacityEnabled(true);
    backImage->addChild(clip);
    
    //close
    float close_width=width*.2;
    auto spr=Sprite::createWithSpriteFrameName("close.png");
    auto select=Sprite::createWithSpriteFrameName("close.png");
    select->setOpacity(125);
    auto menuitem=MenuItemSprite::create(spr, select, [this](Ref*ref){
        if (!this->getEnable()) {
            return ;
        }
        
        this->setEnable(false);
        this->setCascadeOpacityEnabled(true);
        
        auto back_action=TargetedAction::create(backImage,ScaleTo::create(.2, .1));
        auto spawn=Spawn::create(FadeOut::create(.2),back_action, NULL);
        auto call=CallFunc::create([this]{
            DataManager::sharedManager()->setShowingItemID(0);//reset
            DataManager::sharedManager()->setShowingAngle(0);//reset

            if (m_callback) {
                m_callback();
            }
        });
        auto remove=RemoveSelf::create();
        
        this->runAction(Sequence::create(spawn,call,remove, NULL));
    });
    menuitem->setScale(close_width/spr->getContentSize().width);
    closeMenu=Menu::create(menuitem, NULL);
    closeMenu->setPosition(0, backImage->getContentSize().height);
    backImage->addChild(closeMenu);
    
    //sup
    addSupplements();

    //animate
    auto original_scale=backImage->getScale();
    auto duration=.3;
    backImage->setScale(.1);
    auto scale=Sequence::create(ScaleTo::create(duration/2, original_scale*1.1),ScaleTo::create(duration/2, original_scale), NULL) ;
    auto fadein=FadeIn::create(duration);
    auto spawn=Spawn::create(scale,fadein, NULL);
    auto call=CallFunc::create([this]{this->setEnable(true);});
    backImage->runAction(Sequence::create(spawn,call, NULL));
}

FiniteTimeAction* PopUpLayer::fadeOutAll(float duration)
{
    auto fadeout=FadeOut::create(duration);
    
    return Spawn::create(TargetedAction::create(this, fadeout),
                         TargetedAction::create(this->backImage, fadeout->clone()),
                         TargetedAction::create(this->clip, fadeout->clone()),
                         TargetedAction::create(this->itemImage, fadeout->clone()), NULL);
}

#pragma mark- タッチ
void PopUpLayer::addEventListner()
{
    auto listner=EventListenerTouchOneByOne::create();
    listner->setSwallowTouches(true);//透過させない
    listner->onTouchBegan=[this](Touch*touch,Event*event)
    {
        TouchManager::getInstance()->resetTouchInfo();
        
        if (!getEnable()) {
            return true;
        }
        
        Size backSize=Size(backImage->getBoundingBox().size.width, backImage->getBoundingBox().size.height);
        
        Vec2 pos=Vec2(touch->getLocation().x, touch->getLocation().y);
        Rect rect=backImage->getBoundingBox();
        //log("たっぷ t:%f,%f",touch->getLocation().x,touch->getLocation().y);
        //log("れくと t:%f,%f",rect.origin.x,rect.origin.y);

        if (rect.containsPoint(touch->getLocation()))
        {//itemをタップ
            //タッチ領域を検索
            if(!itemMap["touches"].isNull()){
                auto touches=itemMap["touches"].asValueVector();
                for (auto v:touches) {
                    auto map=v.asValueMap();
                    //back基準に変換
                    auto newPos=backImage->convertToNodeSpace(pos);

                    if (TouchManager::getInstance()->isContained(backImage, map, newPos))
                    {//タッチ領域なら処理
                        //deprecateなmapを修正 change要素を直下に移動
                        if (!map["change"].isNull()) {
                            for (auto v : map["change"].asValueMap()) {
                                map[v.first]=v.second;
                            }
                        }
                        
                        //タッチ可能か判断
                        if (!TouchManager::getInstance()->checkTouchEnable(map)) {
                            //タッチの検索続行判定
                            if (map["touchThrough"].asBool()) {
                                log("タッチ検索を続行");
                                continue;
                            }
                            return true;
                        }
                        
                        TouchManager::getInstance()->touchMap=map;
                        
                        break;
                    }
                    else{
                        log("領域外です");
                    }
                }
            }
        }
        else if(getTouchThroughRect().containsPoint(pos)){
            //topSpriteの範囲のみタッチを透過する
            return false;
        }
        
        return true;
    };
    
    //move
    listner->onTouchMoved=[this](Touch*touch,Event*event){
        auto touchMap=TouchManager::getInstance()->touchMap;
        if(touchMap.size()>0){
            if (touchMap["useMoveAction"].asBool()) {
                //Vec2 pos=Vec2(touch->getLocation().x, touch->getLocation().y);
                //this->dragAction(false, pos);
                TouchManager::getInstance()->dragAction(false, touch, this->backImage, true);
            }
        }
        return true;
    };
    
    //end
    listner->onTouchEnded=[this](Touch*touch,Event*event){
        //
        this->manageTouchMap();
        
        return true;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listner, this);
    
    //シェイクジェスチャーを登録
    ShakeGestureManager::getInstance()->addShakeGesture(this, [this](){
        //*注意事項：複数回呼ばれます。PromoteHintLayerなど上にレイヤーが表示されている時も呼ばれています。
        auto shakeV=DataManager::sharedManager()->getItemData(DataManager::sharedManager()->getShowingItemID())["shakeMap"];
        if (!shakeV.isNull()&&
            !DataManager::sharedManager()->getIsShowingSideBar()&&
            !DataManager::sharedManager()->getIsShowingPromoteHint()
            ) {
            if (TouchManager::getInstance()->getIsManagingTouch()) {
                log("他のタッチを処理中です");
                return;
            }
            
            TouchManager::getInstance()->resetTouchInfo();
            TouchManager::getInstance()->manageTouchStart(shakeV.asValueMap(), NULL, this, true);

            this->manageTouchMap();
        }
    });
}


void PopUpLayer::changeAngle()
{//視点を変える
    if(!itemMap["angles"].isNull())
    {//視点を変えてアイテムを見れる場合。一時的に画像のみ差し変える
        auto angleMap=itemMap["angles"].asValueMap();
        auto angle=DataManager::sharedManager()->getShowingAngle();//現在の視点
        auto vec=angleMap["images"].asValueVector();
        if(vec.size()>angle)
        {//視点を変える画像がある
            if (!angleMap["ses"].isNull()) {
                auto ses=angleMap["ses"].asValueVector();
                if (ses.size()>angle) {
                    Common::playSE(ses[angle].asString().c_str());
                }
            }
            
            auto newSpr = EscapeStageSprite::createWithSpriteFileName(vec[angle].asString());
            itemImage->setSpriteFrame(newSpr->getSpriteFrame());
            DataManager::sharedManager()->setShowingAngle(angle+1);
            
            auto nodes=itemImage->getChildren();
            for (int i=(int)nodes.size()-1;i>=0;i--) {
                auto node=nodes.at(i);
                node->removeFromParent();
            }
            
            addSupplements();
        }
    }
}

#pragma mark タッチ後処理
void PopUpLayer::manageTouchMap()
{
    auto touchMap=TouchManager::getInstance()->touchMap;
    if (touchMap.size()>0) {
        //アングルをかえる
        if (touchMap["changeAngle"].asBool()) {
            this->changeAngle();
            return ;
        }
        
        //処理をスタート タッチがぶつかる可能性のあるシーンに注意 customAction処理で途中returnされる場合がある
        if (!DataManager::sharedManager()->getItemData(DataManager::sharedManager()->getShowingItemID())["shakeMap"].isNull()) {
            TouchManager::getInstance()->setIsManagingTouch(true);
        }
        
        //customActionを処理
        this->manageCustomAction(touchMap["customActionKey"].asString(), [this](Ref*ref)
                                 {
                                     Value animation=DataManager::sharedManager()->getAnimationData(TouchManager::getInstance()->touchMap["animationID"].asInt());
                                     
                                     closeMenu->setOpacity(0);//いったんcloseボタンを隠す
                                     //アニメーション処理
                                     this->showAnimateLayer(animation, [this](Ref*ref){
                                         //カスタムアクション2個目
                                         auto key=TouchManager::getInstance()->touchMap["customActionKey_1"].asString();
                                         if (key.size()==0) {
                                             //answerによる保持をチェック
                                             key=AnswerManager::getInstance()->getCustomActionKey_1();
                                         }
                                         
                                         this->manageCustomAction(key, [this](Ref*ref)
                                                                  {
                                                                      auto mystery=TouchManager::getInstance()->touchMap["mysteryID"];
                                                                      
                                                                      //間違い探し処理
                                                                      this->manageMysteryAction(mystery, [this](Ref*ref){
                                                                          //データ処理へ
                                                                          closeMenu->setOpacity(255);
                                                                          this->manageDataByTouch(TouchManager::getInstance()->touchMap);
                                                                          //処理の終了
                                                                          TouchManager::getInstance()->setIsManagingTouch(false);
                                                                      });
                                                                  });
                                     });
                                 });
    }
}

#pragma mark custom Action
void PopUpLayer::manageCustomAction(std::string key, ccMenuCallback callback)
{
    if (key.size()==0) {
        log("カスタム無し");
        callback(NULL);
    }
    else{
        TouchManager::getInstance()->customAction(key, this, true, [callback](bool success){
            if(success){
                callback(NULL);
            }
            else{
                //処理を中断
                TouchManager::getInstance()->setIsManagingTouch(false);
            }
        });
    }
}

#pragma mark animate
void PopUpLayer::showAnimateLayer(Value animation, ccMenuCallback callback)
{
    if (animation.isNull()) {
        callback(NULL);
        return;
    }

    auto animationMap=animation.asValueMap();
    Rect rect;
    if (!animationMap["onPopUp"].isNull()&&!animationMap["onPopUp"].asBool()) {
        rect=Rect(backImage->getPosition().x, backImage->getPosition().y, getContentSize().width, getContentSize().width*1.1);
        log("POPUPフルで表示");
    }
    else{
        rect=Rect(backImage->getPosition().x, backImage->getPosition().y, backImage->getContentSize().width, backImage->getContentSize().height);
    }
    
    auto animateLayer=AnimateLayer::create(animationMap,rect,this,[this,callback](Ref*sender)
                                           {//アニメーション終了callback
                                               callback(NULL);
                                           });
    animateLayer->setLocalZOrder(999);
    //親(gameScene)に貼る
    getParent()->addChild(animateLayer);
}

#pragma mark - ドラッグ
/*void PopUpLayer::dragAction(bool isInBegan, cocos2d::Vec2 pos)
{
    //posをbackImageに基準に変換
    pos=Vec2(pos.x-backImage->getBoundingBox().origin.x, pos.y-backImage->getBoundingBox().origin.y);

    auto key=TouchManager::getInstance()->touchMap["customActionKey"].asString();
    
    if (key==CustomAction_PatternSprite) {
        if (SupplementsManager::getInstance()->patternSprites_onItem.size()==0) {
            //新規作成
            SupplementsManager::getInstance()->addPatternSprite(backImage, true, DataManager::sharedManager()->getShowingItemID(), ShowType_Normal);
        }
        
        SupplementsManager::getInstance()->patternSprites_onItem.at(0)->drag(pos,true);
    }
}*/

#pragma mark- 間違い探し正解処理
void PopUpLayer::manageMysteryAction(cocos2d::Value mystery, ccMenuCallback callback)
{
    if (mystery.isNull()) {
        callback(NULL);
        return;
    }
    
    SupplementsManager::getInstance()->correctAction(itemImage, true, mystery.asInt(),[this,callback](bool success){
        closeMenu->setOpacity(255);
        callback(NULL);
    });
}


#pragma mark データ処理
void PopUpLayer::manageDataByTouch(ValueMap map)
{
    //使用チェック
    std::vector<int> getItemIDs;
    if (!map["getItemIDs"].isNull()) {
        for (auto v : map["getItemIDs"].asValueVector()) {
            getItemIDs.push_back(v.asInt());
        }
    }
    if (!map["getItemID"].isNull()) {
        getItemIDs.push_back(map["getItemID"].asInt());
    }
    for (auto itemID : AnswerManager::getInstance()->getItemIDs) {
        getItemIDs.push_back(itemID);
    }
    
    //アイテム取得あり
    if (getItemIDs.size()>0)
    {//アイテム取得がある場合の削除チェック
        Common::playSE("p.mp3");
        
        //選択アイテムを削除して良いかチェック
        AnswerManager::getInstance()->manageRemoveItem(map, true);
        
        //アイテム変化
        auto fromID=DataManager::sharedManager()->getShowingItemID();
        auto showingID=getItemIDs[0];
        
        DataManager::sharedManager()->changeItemWithIDs(fromID, showingID);
        
        //表示画像IDを変更後、データをロードし直す
        DataManager::sharedManager()->setShowingItemID(showingID);
        DataManager::sharedManager()->setShowingAngle(map["setAngle"].asInt());
                
        itemImage->removeAllChildrenWithCleanup(true);
        itemImage->setOpacity(255);
        clip->removeAllChildren();
        
        this->loadData();
        
        //再表示
        backImage->removeFromParent();
        showItemImage();
    }
    //--ここまで変化処理
    
    
    //フラグ処理
    //保持されたフラグを立てる
    AnswerManager::getInstance()->manageEnFlagIDs(map);
    
    if (map["needReload"].asBool()) {
        DataManager::sharedManager()->setNowBack(DataManager::sharedManager()->getNowBack(), ReplaceTypeNone);
    }
    
    //間違い探し処理
    AnswerManager::getInstance()->manageMysteryData();
    
    if (map["removeAfterUse"].asBool()) {
        DataManager::sharedManager()->removeItemWithID(DataManager::sharedManager()->getShowingItemID());
        DataManager::sharedManager()->setShowingItemID(0);
        this->removeFromParent();
    }
    
    //warp処理
    if (WarpManager::getInstance()->warps.size()>0) {
        auto warp=WarpManager::getInstance()->warps.at(0);
        DataManager::sharedManager()->setNowBack(warp.warpID, warp.replaceType);
        WarpManager::getInstance()->warps.clear();
    }
    
    AnswerManager::getInstance()->clear();
}

std::string PopUpLayer::getShowingFileName()
{
    auto name=itemMap["name"].asString();
    if (DataManager::sharedManager()->getShowingAngle()>0&&
        !itemMap["angles"].isNull()) {
        auto angles=itemMap["angles"].asValueMap();
        auto images=angles["images"].asValueVector();
        auto showIndex=DataManager::sharedManager()->getShowingAngle()-1;
        if (images.size()>showIndex) {
            return images.at(showIndex).asString();
        }
    }
    
    return name;
}

#pragma mark- 
PopUpLayer::~PopUpLayer()
{
    log("PopUp解放");
}
