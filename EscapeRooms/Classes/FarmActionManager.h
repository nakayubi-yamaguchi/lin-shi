//
//  ChocolateShopActionManager.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/11/29.
//
//

#ifndef __EscapeContainer__FarmActionManager__
#define __EscapeContainer__FarmActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class FarmActionManager:public CustomActionManager
{    
public:
    static FarmActionManager* manager;
    static FarmActionManager* getInstance();
   
    //over ride
    int getDefaultBackNum();
    void backAction(std::string key,int backID,Node*parent,ShowType type);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void touchBeganAction(std::string key,Node*parent);
    
    void dragAction(std::string key,Node*parent,Vec2 pos);
    
    void itemBackAction(std::string key,PopUpLayer*parent);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    
    void customAction(std::string key,Node*parent);
    void storyAction(std::string key,Node*parent,const onFinished& callback);

    
    void showAnswerAction(std::string key, Node *parent, ShowType showType);
    void cameraShotAction(int backID);
    void arrowAction(std::string key,Node*parent,const onFinished& callback);


private:
    FiniteTimeAction* birdsAction(int backID, Node*parent, ShowType type);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
