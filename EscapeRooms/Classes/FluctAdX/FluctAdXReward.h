//
// Created by RyoKosuge on 2019/04/02.
//

#ifndef PROJ_ANDROID_STUDIO_FLUCTADXREWARD_H
#define PROJ_ANDROID_STUDIO_FLUCTADXREWARD_H

#include "FluctRewardBridge.h"
#include "../AdX/AdXRewardBridge.h"

#include <mutex>
#include <cassert>

class FluctAdXRewardDelegate {
public:
    virtual void didStartFluctAdXReward() {};

    virtual void didCloseFluctAdXReward(bool completed) {};
};

class FluctAdXReward : public FluctRewardBridgeDelegate, public AdXRewardBridgeDelegate {
public:
    static FluctAdXReward* getInstance() {
        std::call_once(initFlag, create);
        assert(instance);
        return instance;
    }

    FluctAdXReward(const FluctAdXReward&) = delete;

    FluctAdXReward& operator=(const FluctAdXReward&) = delete;

    FluctAdXReward(FluctAdXReward&&) = delete;

    FluctAdXReward& operator=(FluctAdXReward&&) = delete;

    void load();

    bool isReady();

    void show();

    void setDelegate(FluctAdXRewardDelegate* delegate);

private:
    static std::once_flag initFlag;
    static FluctAdXReward* instance;

    static void create() {
        instance = new FluctAdXReward;
    }

    FluctAdXReward();

    ~FluctAdXReward() = default;

    FluctAdXRewardDelegate* delegate;

    bool failedAdX;

    // AdXRewardBridgeDelegate
    void didStartAdXReward();

    void didCloseAdXReward(bool completed);

    void didFailAdXReward(int errorCode);

    // FluctRewardBridgeDelegate
    void didStartFluctReward();

    void didCloseFluctReward(bool completed);

};

#endif //PROJ_ANDROID_STUDIO_FLUCTADXREWARD_H
