//
// Created by RyoKosuge on 2019/04/02.
//

#include "FluctAdXReward.h"

std::once_flag FluctAdXReward::initFlag;
FluctAdXReward* FluctAdXReward::instance = nullptr;

FluctAdXReward::FluctAdXReward() {
    this->delegate = nullptr;
    this->failedAdX = false;
}

void FluctAdXReward::setDelegate(FluctAdXRewardDelegate* delegate) {
    this->delegate = delegate;
}

void FluctAdXReward::load() {
    // フラグを初期化
    this->failedAdX = false;

    // まずは素直にAdXを読み込む
    AdXRewardBridge::setDelegate(this);
    AdXRewardBridge::loadReward();
}

bool FluctAdXReward::isReady() {
    if (this->failedAdX) {
        return FluctRewardBridge::isReady();
    }

    return AdXRewardBridge::isReady();
}

void FluctAdXReward::show() {
    if (this->failedAdX) {
        FluctRewardBridge::show();
        return;
    }

    AdXRewardBridge::show();
}

// AdXRewardBridgeDelegate
void FluctAdXReward::didStartAdXReward() {
    log("FluctAdXReward::didStartAdXReward()");
    if (this->delegate != nullptr) {
        this->delegate->didStartFluctAdXReward();
    }
}

void FluctAdXReward::didCloseAdXReward(bool completed) {
    log("FluctAdXReward::didCloseAdXReward");
    if (this->delegate != nullptr) {
        this->delegate->didCloseFluctAdXReward(completed);
    }

    // 再生完了したら次の読み込み
    AdXRewardBridge::loadReward();
}

void FluctAdXReward::didFailAdXReward(int errorCode) {
    this->failedAdX = true;

    // AdXが失敗したらFluctをloadする
    FluctRewardBridge::setDelegate(this);
    FluctRewardBridge::loadReward();
}

// FluctRewardBridgeDelegate
void FluctAdXReward::didStartFluctReward() {
    log("FluctAdXReward::didStartFluctReward()");
    if (this->delegate != nullptr) {
        this->delegate->didStartFluctAdXReward();
    }
}

void FluctAdXReward::didCloseFluctReward(bool completed) {
    log("FluctAdXReward::didCloseFluctReward");
    if (this->delegate != nullptr) {
        this->delegate->didCloseFluctAdXReward(completed);
    }

    // 再生完了したら次の読み込み
    FluctRewardBridge::loadReward();
}
