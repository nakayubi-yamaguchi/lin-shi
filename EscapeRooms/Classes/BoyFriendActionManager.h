//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__BoyFriendActionManager__
#define __EscapeContainer__BoyFriendActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class BoyFriendActionManager:public CustomActionManager
{
public:
    static BoyFriendActionManager* manager;
    static BoyFriendActionManager* getInstance();
    
    //over ride
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void itemBackAction(std::string key,PopUpLayer*parent);
    void storyAction(std::string key, cocos2d::Node *parent, const onFinished &callback);
    void customAction(std::string key,Node*parent);

    /**answer image上で呼び出される*/
    void showAnswerAction(std::string key,Node*parent);
private:
    FiniteTimeAction*armAction(Node*parent,int index);
    FiniteTimeAction*popCornAction(Node*parent);
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
