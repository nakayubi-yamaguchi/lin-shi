//
//  FingerSprite.h
//  Exit
//
//  Created by 成田凌平 on 2019/02/16.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FingerSprite_____
#define _____PROJECTNAMEASIDENTIFIER________FingerSprite_____

#include "cocos2d.h"

typedef enum{
    FingerDirectionX_Left,
    FingerDirectionX_Right
}FingerDirectionX;


typedef enum{
    FingerDirectionY_Bottom,
    FingerDirectionY_Top
}FingerDirectionY;

USING_NS_CC;
class FingerSprite :public Sprite
{
public:
    static FingerSprite* create(std::string fileName,Vec2 vec);
    CC_SYNTHESIZE(FingerDirectionX, directionX, DirectionX);
    CC_SYNTHESIZE(FingerDirectionY, directionY, DirectionY);

    /**アニメ 通常*/
    void pointAnimate();
    /**ドラッグするアニメ*/
    void dragAnimate(ValueVector vector,Node*parent);
private:
    bool init(std::string fileName,Vec2 vec);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
