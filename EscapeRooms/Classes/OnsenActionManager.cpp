//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "OnsenActionManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"

using namespace cocos2d;

OnsenActionManager* OnsenActionManager::manager =NULL;

OnsenActionManager* OnsenActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new OnsenActionManager();
    }
    return manager;
}

#pragma mark - Back
void OnsenActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="daruma")
    {
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(4)) {
            auto eye_0=parent->getChildByName("sup_42_eye_0.png");
            auto eye_1=parent->getChildByName("sup_42_eye_1.png");

            auto duration=.7;
            
            Vector<FiniteTimeAction*>actions;
            for (int i=0; i<4; i++) {
                auto distance_x=parent->getContentSize().width*.02;
                auto distance_y=distance_x;
                
                if (i>=2) {
                    distance_x=-distance_x;
                }
                if (i==1||i==2) {
                    distance_y=-distance_y;
                }
                auto moves=Sequence::create(EaseInOut::create(MoveBy::create(duration, Vec2(distance_x, distance_y)), 1.5),
                                            DelayTime::create(.5),
                                            EaseInOut::create(MoveBy::create(duration, Vec2(-distance_x, -distance_y)), 1.5),NULL);
                
                actions.pushBack(Spawn::create(TargetedAction::create(eye_0, moves),
                                               TargetedAction::create(eye_1, moves->clone()), NULL));
            }
            
            parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),actions.at(0),actions.at(2),actions.at(3),actions.at(2)->clone(),actions.at(1),actions.at(0)->clone(), NULL), UINT_MAX));
        }
    }
    else if(key=="racoon"){
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(29)&&DataManager::sharedManager()->getEnableFlagWithFlagID(30))
        {//たぬき移動後
            for (int i=0;i<4;i++) {
                auto spr=createSpriteToCenter(StringUtils::format("sup_26_racoon_%d_1.png",i), false, parent);
                parent->addChild(spr);
            }
        }
        else{
            std::vector<std::string>names={"sup_26_racoon_0.png","sup_26_racoon_1.png"};
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(29)) {
                names.push_back("sup_26_racoon_2.png");
            }
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(30)) {
                names.push_back("sup_26_racoon_3.png");
            }
            
            for (auto name :names) {
                auto spr=createSpriteToCenter(name, false, parent);
                parent->addChild(spr);
            }
        }
    }
    else if(key=="fox"&&DataManager::sharedManager()->getEnableFlagWithFlagID(21)){
        Vector<FiniteTimeAction*>actions;
        for(int i=0;i<4;i++){
            auto fox=createSpriteToCenter(StringUtils::format("anim_11_%d.png",i), true, parent);
            parent->addChild(fox);
            
            actions.pushBack(TargetedAction::create(fox, Sequence::create(FadeIn::create(.5),
                                                                          DelayTime::create(1),
                                                                          FadeOut::create(.5), NULL)));
        }
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),actions.at(0),actions.at(3),actions.at(2),actions.at(1),actions.at(1)->clone(),actions.at(3)->clone(), NULL), UINT_MAX));
        
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            auto kappa=parent->getChildByName("sup_11_mistake.png");
            kappa->setOpacity(0);
            
            auto delay=DelayTime::create(1.5);
            auto sound=createSoundAction("bashaa.mp3");
            auto appear=FadeIn::create(.2);
            auto call=CallFunc::create([]{DataManager::sharedManager()->setTemporaryFlag("kappa", 1);});
            
            parent->runAction(TargetedAction::create(kappa, Sequence::create(delay,sound,appear,call, NULL)));
        }
    }
    else if(key=="zzz"){
        Vector<FiniteTimeAction*>actions;
        Vector<FiniteTimeAction*>actions_1;

        for(int i=0;i<5;i++){
            auto zzz=createSpriteToCenter(StringUtils::format("sup_44_%d.png",i), true, parent);
            parent->addChild(zzz);
            
            if(i%2==1){
                actions.pushBack(createSoundAction("snoring.mp3"));
            }
            
            actions.pushBack(TargetedAction::create(zzz, Sequence::create(FadeIn::create(.5),
                                                                          DelayTime::create(1),
                                                                          NULL)));
            
            if (!DataManager::sharedManager()->isPlayingMinigame()) {
                actions_1.pushBack(TargetedAction::create(zzz, Sequence::create(FadeOut::create(.5), NULL)));
            }
            else{
                if (i==0) {
                    auto mistake=parent->getChildByName("sup_44_mistake.png");
                    mistake->setOpacity(0);
                    
                    actions_1.pushBack(TargetedAction::create(mistake, Sequence::create(FadeIn::create(.5),
                                                                                        CallFunc::create([]{
                        DataManager::sharedManager()->setTemporaryFlag("zzz", 1);
                    }), NULL)));
                }
                else if(i==3){
                    break;
                }
            }
        }
        
        
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),Sequence::create(actions),Spawn::create(actions_1), NULL), UINT_MAX));
    }
    else if(key=="clear"){
        if (!DataManager::sharedManager()->isPlayingMinigame()) {
            auto story=StoryLayer::create("ending", [parent](Ref*ref){
                EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

                auto story_1=StoryLayer::create("ending1", [parent](Ref*ref){
                    Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
                });
                Director::getInstance()->getRunningScene()->addChild(story_1);
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        }
        else{
            
        }
    }
}

#pragma mark - Touch
void OnsenActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="brushEye")
    {
        Common::playSE("p.mp3");
        auto brush=createSpriteToCenter("anim_42_brush.png", true, parent);
        parent->addChild(brush);
        
        auto eye=createSpriteToCenter("sup_42_eye_1.png", true, parent);
        parent->addChild(eye);
        
        auto distance=parent->getContentSize().width*.02;
        auto fadein_0=TargetedAction::create(brush, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto moves=TargetedAction::create(brush, Repeat::create(Sequence::create(MoveBy::create(.2, Vec2(distance, 0)),MoveBy::create(.2, Vec2(-distance, 0)) ,NULL), 3));
        auto fadein_1=TargetedAction::create(eye, FadeIn::create(1));
        auto spawn=Spawn::create(moves,fadein_1, NULL);
        auto fadeout=TargetedAction::create(brush, FadeOut::create(.5));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_0,delay,spawn,fadeout,call, NULL));
    }
    else if(key=="brush")
    {
        Common::playSE("p.mp3");
        auto brush=createSpriteToCenter("anim_32_0.png", true, parent);
        parent->addChild(brush);
        
        auto distance=parent->getContentSize().width*.02;

        auto fadein_0=TargetedAction::create(brush, FadeIn::create(1));
        auto delay_0=DelayTime::create(1);
        auto moves=TargetedAction::create(brush, Repeat::create(Sequence::create(MoveBy::create(.2, Vec2(0, -distance)),MoveBy::create(.2, Vec2(0, distance)) ,NULL), 3));
        auto fadeout=TargetedAction::create(brush, FadeOut::create(.5));

        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_0,delay_0,moves,fadeout,call,NULL));
    }
    else if(key=="sake")
    {
        Common::playSE("p.mp3");
        auto monkey=parent->getChildByName("sup_16_monkey_0.png");
        
        auto anim=createSpriteToCenter("anim_16_sake.png", true, parent);
        parent->addChild(anim);
        
        auto anim_1=createSpriteToCenter("anim_16_drink.png", true, parent);
        parent->addChild(anim_1);
        
        
        Vector<FiniteTimeAction*>actions;
        
        auto fadein_0=TargetedAction::create(anim, FadeIn::create(1));
        actions.pushBack(fadein_0);
        auto sound=CallFunc::create([parent]{
            Common::playSE("jobojobo.mp3");
        });
        actions.pushBack(sound);
        auto delay_0=DelayTime::create(1);
        actions.pushBack(delay_0);
        
        auto change=Spawn::create(TargetedAction::create(anim, FadeOut::create(.7)),
                                  TargetedAction::create(monkey, FadeOut::create(.7)),
                                  TargetedAction::create(anim_1, FadeIn::create(.7)),
                                  createSoundAction("drink.mp3"), NULL);
        actions.pushBack(change);

        auto delay_1=DelayTime::create(1.5);
        actions.pushBack(delay_1);
        
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(17))
        {//drunk
            auto drunk=createSpriteToCenter("anim_16_drunk.png", true, parent);
            drunk->setCascadeOpacityEnabled(true);
            parent->addChild(drunk);
            
            auto but=createSpriteToCenter("sup_16_button.png", true, parent);
            parent->addChild(but);
            
            auto cheek=createSpriteToCenter("sup_16_cheek.png", false, parent);
            drunk->addChild(cheek);
            
            auto change_1=Spawn::create(TargetedAction::create(anim_1, FadeOut::create(.7)),
                                        TargetedAction::create(drunk, Sequence::create(createSoundAction("monkey.mp3")
                                                                                       ,FadeIn::create(.7),DelayTime::create(1.5),TargetedAction::create(but, FadeIn::create(.1)),
                                                                                       FadeOut::create(.7), NULL)),
                                        NULL);

            actions.pushBack(change_1);
        }
        else{
            auto cheek=createSpriteToCenter("sup_16_cheek.png", true, parent);
            parent->addChild(cheek);

            auto change_1=Spawn::create(TargetedAction::create(anim_1, FadeOut::create(.7)),
                                        TargetedAction::create(monkey, FadeIn::create(.7)),
                                        TargetedAction::create(cheek, Sequence::create(createSoundAction("monkey.mp3"),FadeIn::create(.7),DelayTime::create(.5),FadeOut::create(.5), NULL)),
                                        NULL);
            actions.pushBack(change_1);
        }
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="cloud"){
        auto num=DataManager::sharedManager()->getNowBack();
        auto cloud=AnchorSprite::create(Vec2(.54, .674), parent->getContentSize(), StringUtils::format("sup_%d_cloud.png",num));
        cloud->setOpacity(0);
        parent->addChild(cloud);
        
        auto original_scale=cloud->getScale();
        cloud->setScale(.1);
        
        auto appear=TargetedAction::create(cloud, EaseIn::create(Spawn::create(ScaleTo::create(.3, original_scale),
                                                                                 FadeIn::create(.3), NULL), 1.5));
        auto sound=createSoundAction("nyu.mp3");
        auto delay=DelayTime::create(1);
        auto disappear=TargetedAction::create(cloud, EaseIn::create(Spawn::create(ScaleTo::create(.3, .1),
                                                                                  FadeOut::create(.3), NULL), 1.5));
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(appear,sound,delay,disappear,call, NULL));
    }
#pragma mark りんごをあげる
    else if(key=="apple"){
        Common::playSE("paku.mp3");
        
        auto apple=createSpriteToCenter("sup_13_apple.png", true, parent);
        apple->setLocalZOrder(2);
        parent->addChild(apple);
        
        auto sake=parent->getChildByName("sup_13_sake.png");

        auto fadein_1=TargetedAction::create(apple, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto sound=createSoundAction("pinpon.mp3");
        auto fadeout=TargetedAction::create(sake, FadeOut::create(.7));
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_1,delay,sound,fadeout,delay->clone(),call, NULL));
    }
#pragma mark 豚クラウド
    else if(key=="pig"){
        Common::playSE("pig.mp3");
        
        auto arm_0=parent->getChildByName("sup_13_arm_0.png");
        auto arm_1=parent->getChildByName("sup_13_arm_1.png");
        auto body=parent->getChildByName("sup_13_pig.png");
       
        auto createRotates=[](bool isLeft){
            auto angle=-10;
            auto duration_rotate=.2;
            if(!isLeft)
            {
                angle=-angle;
            }
            
            auto seq=Repeat::create(Sequence::create(RotateBy::create(duration_rotate, angle),
                                                     RotateBy::create(duration_rotate, -angle), NULL), 2) ;
            
            return seq;
        };
        
        auto cloudFile = "sup_13_cloud.png";
        auto cloud = cloudAction(parent, cloudFile, Vec2(.653, .736));
        
        auto bounce = Sequence::create(ScaleBy::create(.3, 1.05,.95),
                                       ScaleBy::create(.3, 1/1.05,1/.95), NULL);
        auto rotates=Spawn::create(TargetedAction::create(arm_0, createRotates(true)),
                                   TargetedAction::create(arm_1, createRotates(false)),
                                   TargetedAction::create(body, bounce),
                                   TargetedAction::create(arm_0, bounce->clone()),
                                   TargetedAction::create(arm_1, bounce->clone()),
                                   cloud,
                                   NULL);

        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(rotates,call, NULL));
    }
    else if(key=="water"){
        Common::playSE("jaa.mp3");
        
        auto water=createSpriteToCenter("anim_12.png", true, parent);
        parent->addChild(water);
        auto water_1=createSpriteToCenter("sup_12_water.png", true, parent);
        parent->addChild(water_1);
        
        auto fadein_0=TargetedAction::create(water,Sequence::create(FadeIn::create(.5),DelayTime::create(1.5),FadeOut::create(1), NULL));
        auto fadein_1=TargetedAction::create(water_1, Sequence::create(DelayTime::create(1),FadeIn::create(1), NULL));
        
        auto spawn=Spawn::create(fadein_0,fadein_1, NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,call, NULL));
    }
    else if(key=="dice"){
        Common::playSE("p.mp3");
        auto anim=createSpriteToCenter("anim_29.png", true, parent);
        parent->addChild(anim);
        
        auto back=createSpriteToCenter("back_30.png", true, parent);
        parent->addChild(back);
        
        auto sup=createSpriteToCenter("sup_30_dice.png", true, parent);
        parent->addChild(sup);
        
        auto fadein=TargetedAction::create(anim, FadeIn::create(1));
        auto sound_0=createSoundAction("monkey.mp3");
        auto delay=DelayTime::create(1);
        auto fadein_1=TargetedAction::create(back, FadeIn::create(2));
        auto sound=createSoundAction("dice.mp3");
        auto fadein_2 =TargetedAction::create(sup, FadeIn::create(.5));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,sound_0,delay,fadein_1,delay->clone(),sound,fadein_2,delay->clone(),call, NULL));
    }
    else if(key.compare(0, 6, "racoon")==0){
        Common::playSE("kacha.mp3");
        
        auto index=atoi(key.substr(7,1).c_str());
        
        //
        auto sup=createSpriteToCenter(StringUtils::format("sup_26_racoon_%d.png",index), true, parent);
        parent->addChild(sup);
        
        Vector<FiniteTimeAction*>actions;
        
        auto fadein_0=TargetedAction::create(sup, FadeIn::create(.7));
        actions.pushBack(fadein_0);
        actions.pushBack(DelayTime::create(.5));
        
        //animate or not?
        auto clear=((DataManager::sharedManager()->getEnableFlagWithFlagID(29)&&index==3)||
                    (DataManager::sharedManager()->getEnableFlagWithFlagID(30)&&index==2));
        
        if (clear) {
            auto delay=DelayTime::create(1);
            actions.pushBack(delay);
            
            Vector<FiniteTimeAction*>actions_1;
            for (int i=0; i<4; i++) {
                auto spr_before=parent->getChildByName(StringUtils::format("sup_26_racoon_%d.png",i));
                auto spr=createSpriteToCenter(StringUtils::format("sup_26_racoon_%d_1.png",i), true, parent);
                parent->addChild(spr);
                
                actions_1.pushBack(TargetedAction::create(spr, FadeIn::create(.7)));
                actions_1.pushBack(TargetedAction::create(spr_before, FadeOut::create(.7)));
            }
            
            auto sound=createSoundAction("kacha.mp3");
            actions.pushBack(sound);
            auto spawn=Spawn::create(actions_1);
            actions.pushBack(spawn);
        }
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void OnsenActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{
    if (key=="") {
        
    }
}
