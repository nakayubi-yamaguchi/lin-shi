//
//  SettingScene.hpp
//  Princess
//
//  Created by ナカユビ on 2017/09/09.
//
//

#ifndef SettingScene_hpp
#define SettingScene_hpp

#include "cocos2d.h"
#include "Utils/RewardMovieManager.h"
#include "Utils/PurchaseLayer.h"


USING_NS_CC;

class SettingScene : public cocos2d::LayerColor
{
public:
    static Scene *createScene();
    virtual bool init();
    void onEnterTransitionDidFinish();
    CREATE_FUNC(SettingScene);
        
    ~SettingScene();
    
private:
    Sprite *title_back;
    Sprite *menues_back;

    void createMenues();
    
    void showPanelAd();
    
    //ヒント関連
    void showHintLayer();
    //
    void showDescriptionLayer();
};

#endif /* SettingScene_hpp */
