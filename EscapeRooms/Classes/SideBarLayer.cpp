//
//  SideBarLayer.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/07/26.
//
//

#include "SideBarLayer.h"
#include "UIParts/MenuItemScale.h"
#include "DataManager.h"
#include "ConfirmAlertLayer.h"
#include "EscapeLoadingScene.h"
#include "SupplementsManager.h"

using namespace cocos2d;

SideBarLayer* SideBarLayer::create(SideBarType type, const SideBarCloseCallback &callback) {
    auto layer = new SideBarLayer();
    if (layer&&layer->init(type, callback)) {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_RELEASE(layer);
    return layer;
}

bool SideBarLayer::init(SideBarType type, const SideBarCloseCallback &callback){
    if (!LayerColor::initWithColor(Color4B(0, 0, 0, 0))) {
        return false;
    }
    
    DataManager::sharedManager()->setIsShowingSideBar(true);
    setSideBarType(type);
    m_callback=callback;
    menu_width=getContentSize().width*.175;
    auto layer=createDisableLayer();
    addChild(layer);
    
    createMenu();
    if (DataManager::sharedManager()->getShowingPhotoID()>0) {
        createPhotoSprite(DataManager::sharedManager()->getShowingPhotoID(), false,nullptr);
    }
    show();
    
    return true;
}

#pragma mark - UI
void SideBarLayer::createMenu()
{
    std::vector<std::string>menus={"home.png","side_camera.png","side_bgm_on.png","side_se_on.png","side_vibe_on.png"};
    Vector<MenuItem*>menuItems;
    auto item_width=menu_width-6;
    
    for (int i=0; i<menus.size(); i++) {
        if (i==0||i==1) {
            auto spr=Sprite::createWithSpriteFrameName(menus.at(i));
            auto select=Sprite::createWithSpriteFrameName(menus.at(i));
            
            auto item=MenuItemScale::create(spr, select, [this,i](Ref*ref){
                Common::playClick();
                if (i==0) {
                    this->pushHome();
                }
                else if(i==1){
                    this->pushCamera();
                }
            });
            item->setScale(item_width/item->getContentSize().width);
            menuItems.pushBack(item);
        }
        else{
            //toggle
            std::vector<std::string>offs={"","","side_bgm_off.png","side_se_off.png","side_vibe_off.png"};
            auto spr=Sprite::createWithSpriteFrameName(menus.at(i));
            auto select=Sprite::createWithSpriteFrameName(offs.at(i));
            
            auto onMenuItem = MenuItemScale::create(spr, spr,nullptr);
            auto offMenuItem = MenuItemScale::create(select, select,nullptr);
            
            auto soundMenuItem = MenuItemToggle::createWithCallback([i](Ref*sender){
                
                auto toggle = (MenuItemToggle*)sender;
                auto stopSound = toggle->getSelectedIndex();
                

                if(i==2){
                    //bgm
                    SettingDataManager::sharedManager()->setStopBgm(stopSound);

                    if (stopSound) {
                        Common::stopBgm();
                    }
                    else{                        
                        EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(false).c_str(), true);
                    }
                }
                else if(i==3){
                    //se
                    SettingDataManager::sharedManager()->setStopSe(stopSound);

                    if (stopSound) {
                        Common::stopAllSE();
                    }
                }
                else{
                    //vibe
                    SettingDataManager::sharedManager()->setStopVibe(stopSound);

                    NativeBridge::vibration3D();
                }
                
                //末尾で
                Common::playClick();

            }, onMenuItem, offMenuItem, NULL);
            soundMenuItem->setScale(item_width/soundMenuItem->getContentSize().width);
            
            if(i==2){
                soundMenuItem->setSelectedIndex(SettingDataManager::sharedManager()->getStopBgm());
            }
            else if(i==3){
                soundMenuItem->setSelectedIndex(SettingDataManager::sharedManager()->getStopSe());
            }
            else {
                soundMenuItem->setSelectedIndex(SettingDataManager::sharedManager()->getStopVibe());
            }
            
            menuItems.pushBack(soundMenuItem);
        }
    }
    
    auto menu_height=DataManager::sharedManager()->getMainSpriteRect().size.height;
    auto pos_x=(getSideBarType()==SideBarType_FromLeft)?-menu_width/2:getContentSize().width+menu_width/2;
    menuBack=Sprite::create();
    menuBack->setColor(Color3B(10, 10, 10));
    menuBack->setTextureRect(Rect(0, 0, menu_width, getContentSize().height));
    menuBack->setPosition(pos_x,getContentSize().height/2);
    addChild(menuBack);
    
    menu=Menu::createWithArray(menuItems);
    menu->alignItemsVerticallyWithPadding((menu_height-item_width*menus.size())/(menus.size()+1));
    menu->setPosition(menu_width/2,DataManager::sharedManager()->getMainSpriteRect().origin.y+DataManager::sharedManager()->getMainSpriteRect().size.height/2);
    menuBack->addChild(menu);
}

void SideBarLayer::createPhotoSprite(int ID, bool animate, const cocos2d::ccMenuCallback &callback)
{
    auto photo_back_size=Size((getContentSize().width-menu_width)*.9,DataManager::sharedManager()->getMainSpriteRect().size.height*.9);
    auto photo_width=photo_back_size.width*.9;
    setIsAnimated(false);
    auto pos_x=(getSideBarType()==SideBarType_FromLeft)?menu_width+(getContentSize().width-menu_width)/2:(getContentSize().width-menu_width)/2;
    
    photoBack=Sprite::create();
    photoBack->setTextureRect(Rect(0,0,photo_back_size.width,photo_back_size.height));
    photoBack->setPosition(pos_x, DataManager::sharedManager()->getMainSpriteRect().origin.y+DataManager::sharedManager()->getMainSpriteRect().size.height/2);
    photoBack->setOpacity(0);
    photoBack->setCascadeOpacityEnabled(true);
    addChild(photoBack);
    
    //clip
    auto stencil=Sprite::create();
    stencil->setTextureRect(Rect(0, 0, photo_width, photo_width*1.1));
    stencil->setPosition(photoBack->getContentSize()/2);
    
    clip=ClippingNode::create();
    clip->setCascadeOpacityEnabled(true);
    clip->setStencil(stencil);
    clip->setAlphaThreshold(0);
    clip->setInverted(false);
    photoBack->addChild(clip);
    
    photoSprite=EscapeStageSprite::createWithSpriteFileName(StringUtils::format("back_%d.png",ID));
    photoSprite->setPosition(clip->getStencil()->getPosition());
    photoSprite->setScale(photo_width/photoSprite->getContentSize().width);
    photoSprite->setCascadeOpacityEnabled(true);
    clip->addChild(photoSprite);
    
    
    SupplementsManager::getInstance()->addSupplementToMain(photoSprite, ID, true,ShowType_OnPhoto);
    SupplementsManager::getInstance()->customBackAction(photoSprite,ID,DataManager::sharedManager()->getBackData(ID)["customActionKey"].asString(),ShowType_OnPhoto);
    
    if (animate) {
        auto original_scale=photoBack->getScale();
        photoBack->setScale(1.5);
        
        runAction(Sequence::create(TargetedAction::create(photoBack, Spawn::create(FadeIn::create(.3),
                                                                                   ScaleTo::create(.3, original_scale), NULL)),
                                   CallFunc::create([this,callback]{
            this->setIsAnimated(true);
            if (callback) {
                callback(NULL);
            }
        }),
                                   NULL));
    }
    else{
        if (callback) {
            callback(NULL);
        }
    }
}

#pragma mark - アニメーション
void SideBarLayer::show()
{
    auto distance=(getSideBarType()==SideBarType_FromLeft)?menu_width:-menu_width;
    auto duration=.2;
    Vector<FiniteTimeAction*>actions;//spawn

    auto fadein_back=FadeTo::create(duration, 100);
    actions.pushBack(fadein_back);
    auto movein=TargetedAction::create(menuBack, EaseOut::create(MoveBy::create(duration, Vec2(distance, 0)), 1.5));
    actions.pushBack(movein);
    if (photoBack) {
        actions.pushBack(TargetedAction::create(photoBack, FadeIn::create(duration)));
    }
    auto spawn=Spawn::create(actions);
    auto call=CallFunc::create([this]{
        this->setIsAnimated(true);
    });
    runAction(Sequence::create(spawn,call, NULL));
}

void SideBarLayer::close()
{
    auto distance=(getSideBarType()==SideBarType_FromLeft)?-menu_width:menu_width;
    auto duration=.2;
    Vector<FiniteTimeAction*>actions;//spawn
    auto fadeout_back=FadeOut::create(duration);
    actions.pushBack(fadeout_back);
    if (photoBack) {
        actions.pushBack(TargetedAction::create(photoBack, FadeOut::create(duration)));
    }
    auto moveout=TargetedAction::create(menuBack, EaseOut::create(MoveBy::create(duration, Vec2(distance, 0)), 1.5));
    actions.pushBack(moveout);
    auto spawn=Spawn::create(actions);
    auto call=CallFunc::create([this]{
        DataManager::sharedManager()->setIsShowingSideBar(false);

        if (m_callback) {
            m_callback();
        }
        this->removeFromParent();
    });
    runAction(Sequence::create(spawn,call, NULL));
}

#pragma mark - 各種ボタン
void SideBarLayer::pushHome()
{
    auto message = DataManager::sharedManager()->getSystemMessage("interrupt_game");
    auto alert = ConfirmAlertLayer::create(ConfirmAlertType_Twice, message, [this](Ref* sender){
        auto alert = (ConfirmAlertLayer*)sender;
        int selectedButton = alert->getSelectedButtonNum();
        
        if (selectedButton != 0)
        {//遷移する
            Common::stopBgm();

            Director::getInstance()->replaceScene(TransitionFade::create(.5, EscapeLoadingScene::createScene(LoadingType_RemoveCathe),Color3B::WHITE));
        }
    });
    alert->showAlert(this);
}

void SideBarLayer::pushCamera()
{
    if (!getIsAnimated()) {
        return;
    }
    Common::playSE("camera.mp3");
    
    //撮影時、カスタムアクションを呼ぶ。
    auto manager=SupplementsManager::getInstance()->getCustomActionManager();
    manager->cameraShotAction(DataManager::sharedManager()->getNowBack());
    
    //switch,sliderは別keyで保持する(扉のタイプを考慮)
    DataManager::sharedManager()->setValueToPhotoMap(DataManager::sharedManager()->getNowBack());
    
    if (!photoSprite) {
        DataManager::sharedManager()->setShowingPhotoID(DataManager::sharedManager()->getNowBack());
        createPhotoSprite(DataManager::sharedManager()->getNowBack(),true,nullptr);
    }
    else{
        //破棄してから再表示
        setIsAnimated(false);
        DataManager::sharedManager()->setShowingPhotoID(DataManager::sharedManager()->getNowBack());
        auto distance=(getSideBarType()==SideBarType_FromLeft)?getContentSize().width:-getContentSize().width;
        auto remove=TargetedAction::create(photoBack, Sequence::create(Spawn::create(FadeOut::create(.2),
                                                                                     MoveBy::create(.2, Vec2(distance, 0)),
                                                                                     RotateBy::create(.2, 180), NULL),
                                                                       RemoveSelf::create(),
                                                                       NULL));
        auto call=CallFunc::create([this]{
            this->createPhotoSprite(DataManager::sharedManager()->getNowBack(), true,[this](Ref*ref){
                this->setIsAnimated(true);
            });
        });
        runAction(Sequence::create(remove,call, NULL));
    }
}

#pragma mark -
LayerColor* SideBarLayer::createDisableLayer()
{
    auto layer=LayerColor::create(Color4B(0, 0, 0, 0));
    
    auto listner=EventListenerTouchOneByOne::create();
    listner->setSwallowTouches(true);//透過させない
    listner->onTouchBegan=[this](Touch*touch,Event*event)
    {
        if (this->isAnimated) {
            this->setIsAnimated(false);
            this->close();
        }
        return true;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listner, layer);
    
    return layer;
}
