
#ifndef __Kebab__DataManager__
#define __Kebab__DataManager__

#include "cocos2d.h"
#include "CustomActionKeys.h"
#include "EscapeDataManager.h"
#include "ShowType.h"

USING_NS_CC;

typedef enum {//タイトルのデータごとに持つべきデータ
    RecordType_StartDate,
    RecordType_StopDate,
    RecordType_Hint,
    //RecordType_ClearGame,
    //RecordType_ClearMiniGame,
    //RecordType_Reset//クリアデータ以外の情報をリセット。
}RecordType;

//コイン使用箇所と必要コイン数。増えたらここに記載すること.
//金額が被る場合は他の手段を検討すること
typedef enum {//コインを使用できる箇所
    CoinSpot_Pre = 50,
    CoinSpot_Hint = 2,
    CoinSpot_Minigame = 5,
    CoinSpot_Unlocked = 100//アンロックに必要なコイン
}CoinSpot;

typedef enum {//デイリープレゼントパック
    DailyPresentPack_None= 0,
    DailyPresentPack_Small,
    DailyPresentPack_Big
}DailyPresentPack;

typedef enum {//アチーブメント種類
    Achievement_Login,
    Achievement_TotalStampByMovie,
    Achievement_StageClear,
    Achievement_MinigameClear,
    Achievement_PrePlay,
    Achievement_BuyBeginnersPack
}Achievement;

//タイトル毎に保持されるデータ。[START]で再プレイしても消えないデータ
#define RecordKey_PreMovie "recordpremovie"//bool
#define RecordKey_UnlockedCoin "recordunlockedcoin"//bool:公開終了したやつをコイン払って買い戻したかどうか。
#define RecordKey_UnlockedTimeStamp "recordunlockedtimestamp"//longlong:アンロックしたタイムスタンプ。ver5.0から導入。
#define RecordKey_UnlockedCount "recordunlockedcount"//int:アンロックした回数を保存。ver5.0から導入

#define RecordKey_PlayCount "recordplaycount"//int タイトル表示完了ごとにincrement
#define RecordKey_LastPlayVersion "recordlastplayversion"//string
#define RecordKey_Review "recordreview"//int つけた星の数
#define RecordKey_ClearGame "cleargame"//bool 本編クリアしたか.version5.0でマイグレーション
#define RecordKey_ClearMiniGame "clearminigame"//bool ミニゲームクリアしたか v.3.3から搭載. version5.0でマイグレーション

//タイトル毎に保存されるデータ。[START]で再プレイしたら、消える。
#define RecordKey_Date "recorddate" //プレイ時間を計測するための基準日時
#define RecordKey_Time "recordtime" //int プレイ秒数
#define RecordKey_Hint "recordhint" //int 総ヒント視聴回数

//全体で使うデータ
#define UserKey_Coin "Coin"//コイン
#define UserKey_LoginBonusDate "LoginBonusDate" //ログインボーナス受け取り日時
//#define UserKey_LoginBonusCount "LoginBonusCount"//ログインボーナス受け取り数
#define UserKey_GetStamp "GetStamp_%08lld"//日付を代入 日毎のスタンプ入手数
#define UserKey_GetRoulette "GetRoulette_%08lld"//日付を代入 日毎のルーレット回転数
#define UserKey_GetRouletteTotal "loginBonusCount"//トータル抽選回数
#define UserKey_LaunchCountDate "LaunchCount" //起動日数分析用 1日一回のみ、起動回数をfirebaseでカウントする(分析用)v.4.0~
#define UserKey_RegistNotification "RegistNotification"
#define UserKey_RegistNotification_Again "RegistNotification_Again" //analyticsTypeAgainの時。

#define UserKey_IsPaidUser "isPaiduser"

#define UserKey_NowStampCount "NowStampCount"//現在の溜まってるスタンプ数

#define UserKey_PigTalkIndex "pigtalkindex"//サムのトーク

//ログインボーナスパワーアップのキー
#define UserKey_GotDailyBonusDate "GotDailyBonusDate"//デイリーボーナスを受け取った日
#define UserKey_DailyBonusCoin "DailyBonusCoin"//デイリーボーナスで受け取れるコイン数
#define UserKey_DailyBonusCount "DailyBonusCount"//デイリーボーナスを受け取れる残り回数。

//アチーブメント
#define UserKey_LoginDayCount "LoginDayCount"//Achievement用 ログイン日数を記録
#define UserKey_TotalStampByMovie "TotalStampByMovie"//動画によるトータルスタンプ取得数
#define UserKey_PrePlayCount "PrePlayCount"//先行リリースプレイ総数

//ユーザー
#define UserKey_UserID "userID"

#define StartGameKey "StartGame"//ストーリー読了タイミングで記憶 otherFlagとして

typedef enum {
    HintType_Hint = 0,//通常のヒント
    HintType_Hint_Mystery,//謎解きのためのヒント
    HintType_Hint_Location,//解答箇所のためのヒント
    HintType_Hint_GetItem,//アイテム取得のためのヒント
    HintType_Hint_UseItem,//アイテム使用のためのヒント
    HintType_Hint_Mistake,//間違い探しのためのヒント
    HintType_Answer,//解答
    HintType_Commentary,//解説
}
HintType;

class DataManager
{
public:
    static DataManager* manager;
    static DataManager* sharedManager();

#pragma mark back,touch
    ValueMap nowBackMap;//背景が変わるごとに保持するbackData
    void reloadNowBack();
    ValueMap getBackData(int num);//背景データを取得 遷移先情報
    ValueMap getBackData();//背景データを取得 遷移先情報
    std::string getBackName();
    std::string getBackName(int backID);

    ValueVector getTouchDataArray();//タッチ領域を格納した配列を取得
    Value getAnimationData(int animationID);//アニメーションを取得
    
#pragma mark - Load
    /**一括ロード GameScene遷移前に呼ばれる*/
    void loadAllData();
    
#pragma mark - Item
    ValueMap nowItemMap;//showItemが変わるごとに保持するbackData
    void reloadNowItem();
    ValueMap getItemData(int itemID);//アイテム情報を取得 ファイル名とタップ情報
    std::vector<int>items;
    void addNewItem(int itemID);
    bool findItemWithItemID(int itemID);
    void changeItemWithIDs(int fromID,int toID);//アイテムを変化させる
    void separateItemWithIDs(int fromID,ValueVector ids);//アイテムを分離させる
    /**アイテムを消去 0のときはreturn*/
    void removeItemWithID(int itemID);
    bool isExistItemData();//アイテムデータがあるかどうか。
    void loadItemData();//アイテムデータをローディングします。
    void saveItemData();//アイテムデータをセーブします。
    
#pragma mark - Flag
    ValueMap flagMap;//セーブ対象 フラグ管理
    ValueMap releasedHintMap;//解放したヒント管理
    /**フラグを立てる 0ならreturn*/
    void setEnableFlag(float flagID);//saveも行う
    void setDisableFlag(float flagID);

    
    /**指定範囲のフラグを全て立てる*/
    void setEnableFlags(float min ,float max);

    bool getEnableFlagWithFlagID(float flagID);
    bool getEnableFlagWithFlagIDs(ValueVector flagIDs);
    /**min-max範囲内のフラグをカウント*/
    int getFlagCount(float min,float max);
    /**min-max範囲内の立ってるフラグをカウント*/
    int getEnableFlagCount(float min,float max);
    /**記録されたフラグがあるかどうか。セーブされてればプレイ履歴があるということ*/
    bool isExistFlagData();
    /**進捗率取得*/
    float getProgress();
    void loadFlagData();
    void saveFlagData();
    
    /*EscapeDataManagerからロード後、保持するデータ**/
    ValueMap flagFileData;
    ValueMap getFlagFileData();
    
    //一時的な(セーブしない)フラグ ゲーム終了でリセットされる
    ValueMap temporaryFlagMap;
    ValueVector getTemporarySupplementsVec(ValueMap map);
    void setTemporaryFlag(std::string key,int value);
    Value getTemporaryFlag(std::string key);
    
    //その他フラグ
    ValueMap otherFlagMap;//セーブ対象
    ValueVector getOtherFlagSupplementsVec(ValueMap map);
    void loadOtherFlagData();
    void setOtherFlag(std::string key,int value);
    Value getOtherFlag(std::string key);
    void saveOtherFlagData();

#pragma mark Hint
    float flagIDforHint;

    std::string getHintTitle(int hintIndex);
    ValueVector getHintArray(bool includeUseAnswerImage);
    int getHintCount();
    float getFlagIDForHint();
    void setFlagIDForHint();

    void loadReleasedHintData();
    void saveReleasedHintData();
    void setReleasedHintIndex(int hintIndex);
    /**該当フラグのフラグマップを取得*/
    ValueMap getFlagMapForFlag(float flagID);
    /**答えを画像で表示する際のValueMapを返す*/
    ValueMap getUseImageInAnswerValueMap(ValueMap map);
    /**ヒントを画像で表示する際のIndexを返す*/
    ValueMap getUseImageInHintValueMap(ValueMap map);
    /**ヒントの開放状態を返す*/
    bool isReleasedHint(int hintIndex);
    /**フラグを指定し、ヒントの開放状態を返す*/
    bool isReleasedHint(int hintIndex,float flagID);
    /**何番目のヒントまで解放されているか。-1なら一個も解放されていない。*/
    int getReleasedHintNum();
    /**FlagID,ヒント数を渡し、何番目のヒントまで解放されているか。-1なら一個も解放されていない。*/
    int getReleasedHintNum(float flagID,int hintCount);
    
    /**ヒント解放されている番号の配列*/
    ValueVector getReleaseHintVector();
    /**フラグIDをMapのキーの形に変換する flagIDが整数の場合は整数、小数点以下がある場合は小数のキーを作成 小数点第一位までのみ対応*/
    std::string getFlagIDKey(float flagID);
    
    /**ヒントタイプを取得*/
    HintType getHintType(ValueMap hintMap);
    
    /**flag.plistの書き方がEscapeRooms用かどうか*/
    bool isEscapeRoomsGrammerForFlag();
    
    CC_SYNTHESIZE(int, temporaryHintIndex, TemporaryHintIndex);
    
#pragma mark - ShowType
    bool isShowTypeAnswer(ShowType type);
    bool isShowTypeHint(ShowType type);

#pragma mark - Story
    ValueVector getStoryData(std::string storyName);
    std::string getColorCodeData(std::string colorName);
    ValueMap getSpeakerData(const char* speakerName);
    /**すでにストーリーを流したかどうか*/
    bool isShowedOpeningStory();
    
#pragma mark Minigame
    ValueMap mysteryMap;//{backID:[mysteryIDs]}
    ValueMap mysteryMap_onItem;//{itemID:[mysteryIDs]}

    void loadMysteryMap();
    ValueMap getMiniGameData();//ミニゲームデータをplistから取得
    ValueMap getMysteryMap(int mysteryID);//謎ごとのマップを取得
    int getFoundMysteryCount();//正解済みの謎の個数を取得
    std::string getMysteryCorrectSupName(int mysteryID);
    bool isPlayingMinigame();

#pragma mark - Panel
    ValueMap getPanelData(int num);//パネル広告の表示データを取得
    
    //その他システム的な
    std::string getAppName();
    std::string getHashTag();
    /**~~~から脱出しよう の文字列を返す other.stringsに"Notification"が設定されていない場合、"HashTag"を参照して返す*/
    std::string getNotificationString();
    std::string getTitleSE();
    std::string getBgmName(bool isEnding);
    ValueMap getAppTextMap(std::string key);
    std::string getSystemMessage(std::string key);
    std::string getTalkMessage();

#pragma mark Supplements
    //補完画像 カスタムアクション
    ValueMap getSupplementsMap(ValueMap backMap);
    /**back,itemのマップを渡す*/
    std::vector<std::string> getRemoveSupplementsNames(ValueMap map,bool onItem);
    /**CustomActionNameOnBack_を渡すこと*/
    ValueMap getCustomSupplementsMap(std::string customActionKey);
    /**CustomActionNameOnBack_を渡すこと backならbackMap,itemならItemmapを*/
    ValueMap getCustomSupplementsMap(std::string customActionKey,ValueMap map);
    /**CustomActionNameOnBack_を渡すこと backならbackMap,itemならItemmapを*/
    ValueVector getCustomSupplementsVector(std::string customActionKey,ValueMap map);
    

    int getZorderBySupName(std::string supName,int original,int backNum);

    ValueVector getParticleVector(bool onItem,int num);
    
    /**OnPhoto で読み込むtemporaryKeyを専用keyで保持 */
    void setValueToPhotoMap(int backID);
    void setValueToPhotoMap(int backID,std::string actionKey);
    
    /**カスタムパーツのタッチ判定*/
    bool checkCustomActionTouchable(ValueMap map);
    
#pragma mark - アチーブメント
/**並べるAchievementタイプの配列を返す*/
    std::vector<Achievement> getAchievements();
    ValueMap getAchievementMap();
    /**達成項目サムネイル名を返す*/
    std::string getAchievementThumbnailName(Achievement achievement);
    /**達成項目説明stringを返す*/
    std::string getAchievementString(Achievement achievement);
    /**達成に必要なポイントを取得*/
    int getAchievementPoint(Achievement achievement);
    /**現在溜まっているポイントを取得*/
    int getNowPoint(Achievement achievement);
    /**達成を受け取った項目を記録*/
    void setAchievementPoint(Achievement achievement);
    /**達成項目をplistから読み込むキーを取得*/
    std::string getAchievementKey(Achievement achievement);
    /**達成を受け取った項目のキーを取得*/
    std::string getGotAchievementKey(Achievement achievement);
    /**達成できる項目が存在するか*/
    bool existAchievableItems();
#pragma mark - 時間やヒントなどのデータ管理.タイトルのデータ毎に持つ情報
    ValueMap recordData;
    std::map<std::string, ValueMap> allRecordData;
    std::map<std::string, ValueMap> allEternalRecordData;//リスタートしても消えないデータ。
    /**全てのタイトルのプレイデータを取得。version.plistダウンロード完了後に呼び出さないといけない。展開処理は重いから非同期処理する。*/
    void loadAllRecordData();
    
    /**レコードデータを展開時にマイグレーション処理をする。*/
    void migrationRecordData();
    
    /**クリアしているステージ総数などを取得。引数はRecordKey_ClearGame,RecordKey_ClearMiniGame*/
    int getTotalStageRecord(const char* recordKey);
    /**引数インデックスのステージをクリアしているかどうか。recordKeyはRecordKey_ClearGame,RecordKey_ClearMiniGame*/
    bool isClearStage(int index, const char* recordKey);
    
    /**引数インデックスのタイトルデータのクリア状態を確認。TitleCondition_CLEAR or TitleCondition_CLEAR_MINIGAME or TitleCondition_ISPLAYING or TitleCondition_UNPLAYED or TitleCondition_NONE*/
    TitleCondition getClearTitleCondition(int index);
    
    /**RecordTypeをRecordKeyに変換*/
    std::string transformRecordTypeToKey(RecordType recordType);
    
    /**テスト用＊全てのステージをクリア状態にする。*/
    void testAllClear();
    
    void loadRecordData();
    void record(RecordType recordType);
    Value getRecordData(RecordType recordType);
    
    //タイトル毎に持つデータ。プレ動画とか、
    //マイグレーション用に修正済み
    /**引数インデックスタイトルのレコードキーデータを取り出す*/
    Value getEternalRecordDataWithIndex(int index, const char* recordKey);

    /**プレイタイトルのレコードキーデータを取り出す。*/
    Value getEternalRecordDataOnPlay(const char* recordKey);

    /**プレイタイトルのレコードキーに値をセットする*/
    void recordEternalDataOnPlay(const char* recordKey, Value value);
    
    /**プレイタイトルのエターナルデータにsavedMapの中身だけ上書き保存する。*/
    void recordEternalDataMapOnPlay(ValueMap savedMap);
        
    /**時間関連とデータを検証して有効なTitleConditionを返す。戻り値は期間関連のTitleCondition or TitleCondition_None*/
    TitleCondition getIsValidTimeCondition(int index);

    /**時間関連のタイトルコンディションが有効かどうかチェック*/
    bool isValidTimeCondition(int index, TitleCondition condition);

    /**プレ動画視聴済み等の条件をチェック。期間コンディションとデータを照合して有効かどうか判定。*/
    bool checkTimeCondition(int index, const char* key);
    
    /**アップデート帯をセルに表示するかどうか。*/
    bool isUpdateCondition(int index);
    
    /**引数インデックス番目のステージが推奨に値するかどうか。*/
    bool getPlayableWithNoCoins(int index);

    /**ロックまでの残り日数を取得*/
    int getDaysToLocked(int index);
    
#pragma mark - データ消去
    //reset
    void resetAllData(bool deleteData);
    void resetMinigameData();
    
#pragma mark - アクセサ
    int nowBack;//表示中の背景index
    void setNowBack(int var,ReplaceType replaceType);
    int getNowBack();
    int selectedItemID;
    int getSelectedItemID();
    void setSelectedItemID(int _selectedItemID);
    
    CC_SYNTHESIZE(int, showingItemID, ShowingItemID);
    CC_SYNTHESIZE(int, showingAngle, ShowingAngle);//表示中のアイテム画像インデックス
    CC_SYNTHESIZE(bool, stopPanelAd, StopPanelAd);//ストーリー表示中とかにパネル表示しないようにするやつ
    CC_SYNTHESIZE(int, showingPhotoID, ShowingPhotoID);//撮影backID
    CC_SYNTHESIZE(bool, isShowingSideBar, IsShowingSideBar);//サイドバーを表示しているか
    CC_SYNTHESIZE(bool, isShowingPromoteHint, IsShowingPromoteHint);//ヒントレイヤを表示しているか

#pragma mark ユーザー
    std::string userID;
    std::string getUserID();
    void setUserID(std::string var);
    void loadUserID();
    bool getIsPaidUser();
    
    /**Userネストにデータをポストする.データ量が小さいのでネストまるまる上書きしている coin変更時、clear時に送信*/
    void postUserData();
#pragma mark コイン
    /**コインを付与*/
    void addCoin(int added);
    /**コインが足りるかどうか*/
    bool isEnoughCoin(int used);
    /**現在のコイン所持枚数*/
    int getCoin();
    /**コインを使用*/
    void useCoin(int used);
    /**現在保持しているコインを3点カンマ付きで取得*/
    std::string getCoinSt();
    /**所持コイン数が変動した時の通知登録*/
    void addChangeCoinNotification(Node *parent, const std::function<void (EventCustom *)> &callback);
    
#pragma mark 日付
    /**サーバ日付とkeyに保存されているを比較。最新の日付ならtrueを返す*/
    bool isSavedDateNew(std::string key);
    /**サーバ日付をセーブ。(ログインボーナス自動受け取り時など)*/
    void saveDate(std::string key);
    /**ログイン日数をincrement*/
    void incrementLoginCount();
    
#pragma mark 初心者パック
    /**ビギナーズパックを購入した際に呼ぶ*/
    void purchasedBeginnersPack(int dailyBonusCoin, int dailyPeriod);
    /**デイリーボーナス(課金アイテム)を受け取れるかどうか*/
    bool canGotDailyCoin();
    /**デイリーボーナス(課金アイテム)を受け取った*/
    void gotDailyBonus();
#pragma mark ヒント促進
    /**ヒント促進アラートチェック*/
    bool checkAutoPromoteHint();
    CC_SYNTHESIZE(long, enFlagTime, EnFlagTime);
    CC_SYNTHESIZE(bool, isPromoteHint, IsPromoteHint);

#pragma mark - UI
    /**UI情報*/
    CC_SYNTHESIZE(float, itemBoxWidth, ItemBoxWidth);
    CC_SYNTHESIZE(Rect, mainSpriteRect, MainSpriteRect);
    CC_SYNTHESIZE(bool, hideTopBanner, HideTopBanner);
    
#pragma mark - Private
private:
    std::map<std::string, ValueMap> plistDataMap;
    
    int m_coin;
    std::function<void()> m_hintPromoteCallback;
    
    void init();

    ValueMap getPlistMapWithKey(const char* key);
    void loadingCoin();
};

#endif /* defined(__Kebab__SettingDataManager__) */
