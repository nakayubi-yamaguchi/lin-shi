//
//  VoteLayer.h
//  EscapeRooms
//
//  Created by yamaguchi on 2019/02/13.
//
//

#ifndef _____EscapeRooms________VoteLayer_____
#define _____EscapeRooms________VoteLayer_____

#include "cocos2d.h"
#include "Utils/ContentsAlert.h"
#include "EscapeDataManager.h"
#include "UIParts/GaugeSprite.h"

typedef enum {//投票タイプ、動画で投票かコインで投票か。
    VoteType_Video = 0,
    VoteType_Coin
}VoteType;


class VoteLayer : public ContentsAlert
{
public:
    static Scene *createScene();
    CREATE_FUNC(VoteLayer);
    
private:
    int m_selectedCandidate;//選択した候補者
    
    virtual bool init() override;
    void createContents();
    
    /**投票の詳細*/
    ValueMap m_voteInfo;
    
    /**投票結果*/
    Value m_VoteResultValue;
    
    /**選択投票数*/
    int m_voteCount;
    
    /**候補者メニューアイテム配列*/
    Vector<MenuItem*> m_candidateMenuItems;
    
    /**キー名.投票の識別ID.*/
    std::string m_key_name;
    
    /**ゲージスプライト*/
    GaugeSprite* m_gaugeSprite;
    
    /**投票期間*/
    VoteTerm m_voteTerm;
    
    /**今日は投票済みかどうか*/
    bool m_isVotedToday;
    
    /**候補者の詳細。投票の確認アラート。*/
    void showSpeachAlert();
    
    /**コインで投票の確認アラート*/
    void showVoteAlertWithCoins();
    
    /**ビデオ視聴で投票の確認アラート*/
    void showVoteAlertWithVideo();
    
    /**結果データを読み込み*/
    void loadResultValue();
    
    /**結果表示用UI*/
    void showResult();
    
    /**投票処理*/
    void vote(VoteType voteType);
    
    /**中間発表。データベースにデータがなかったら、最初にアクセスしたユーザーが、保存する。*/
    void interimReport();

    /**結果発表。*/
    //void resultReport();
    
    /**データ読み込み完了*/
    void readVoteData(Value value);
    
    /**
     Firebaseデータベースのディレクトリを返す。
     @param isInterim 中間発表かどうか
     @param childkey チャイルドキー
     @return Firebaseデータベースのディレクトリ
     */
    std::string getFirVoteDir(bool isInterim, const char* childkey);
    
    
    /**投票用にレイアウトを整えます。*/
    virtual void arrange() override;
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
