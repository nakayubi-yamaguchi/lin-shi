//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__SportsGymActionManager__
#define __EscapeContainer__SportsGymActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class SportsGymActionManager:public CustomActionManager
{
public:
    static SportsGymActionManager* manager;
    static SportsGymActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void customAction(std::string key,Node*parent);
    void arrowAction(std::string key,Node*parent,const onFinished& callback);

    //over ride
    void showAnswerAction(std::string key,Node*parent);

private:
    FiniteTimeAction*buttonAction(int index,Node*parent);
    Spawn* balloonAction(Node*parent,int backID,int index);
    Sequence* danceAction(Node*parent,int backID,bool complete);
    FiniteTimeAction* slideAction(Node*parent,Node*node,bool animate,int backID);
    void addWaveParticles(Node*parent);
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
