//
//  CountUpLabel.cpp
//  ObjectRoom
//
//  Created by 成田凌平 on 2017/02/01.
//
//

#include "CountUpLabel.h"
#include "DataManager.h"
#include "Utils/Common.h"
#include "Utils/ValueHelper.h"
using namespace cocos2d;

CountUpLabel*CountUpLabel::create(cocos2d::ValueMap map, cocos2d::Value answerMapV, cocos2d::Size parentSize, int number,int backNum, std::string fontPath,float fontSize,bool useBM)
{
    auto node=new CountUpLabel();
    if (node&&node->init(map,answerMapV,parentSize,number,backNum,fontPath,fontSize,useBM)) {
        node->autorelease();
        return node;
    }
    CC_SAFE_RELEASE(node);

    return node;
}

bool CountUpLabel::init(cocos2d::ValueMap map, cocos2d::Value answerMapV, cocos2d::Size parentSize, int number,int backNum, std::string _fontPath,float _fontSize,bool useBM)
{
    //spriteごとに個別に設定されているパラメータを優先してセット
    auto fontSize=map["fontSize"].asFloat()*parentSize.width;
    if (fontSize==0) {
        fontSize=_fontSize;
    }
    
    std::string fontPath;
    if (!map["fontPath"].isNull()) {
        fontPath=map["fontPath"].asString();
    }
    else if (_fontPath.size()>0){
        fontPath=_fontPath;
    }
    else{
        fontPath=MyLightFont;
    }
    
    //init
    if (!Label::initWithTTF("0", fontPath, fontSize)) {
        return false;
    }
    
    if (useBM) {
        setBMFontFilePath(fontPath);
        auto bmfontSize=64.0;//bmフォントは64pxでつくられていることとする
        auto scale=fontSize/bmfontSize;
        setScale(scale);
    }
    
    //貼り付けるラベルのプロパティチェック
    auto posX=map["x"].asFloat()*parentSize.width;
    float posY;
    if(map["posFix"].asBool()){
        log("ポジション修正");
        posY=map["y"].asFloat()*parentSize.height;
    }
    else{
        posY=map["y"].asFloat()*parentSize.width;
    }
    setPosition(posX, posY);

    //
    setBackNum(backNum);
    //num
    setNumber(number);
    
    //回転
    setRotation(map["angle"].asFloat());
    
    //textcolorがあれば
    setTextColor(Color4B::BLACK);
    if(!map["textColor"].isNull()){
        auto colorcode=DataManager::sharedManager()->getColorCodeData(map["textColor"].asString());
        setTextColor(Color4B(Common::getColorFromHex(colorcode)));
    }
    
    //outlineがあれば
    setOutline(map);
    
    //固定する値があるかチェック
    bool secure=false;
    if (!map["secure"].isNull()) {
        auto secureMap=map["secure"].asValueMap();
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(secureMap["flag"].asInt())) {
            setString(secureMap["text"].asString());
            secure=true;
        }
    }
    else if (!answerMapV.isNull()) {
        if (answerMapV.asValueMap()["secure"].asBool()) {
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(answerMapV.asValueMap()["enFlagID"].asInt())) {
                auto answer=answerMapV.asValueMap()["answer"].asString();
                setString(ValueHelper::substr(answer, number));
                secure=true;
            }
        }
    }

    
    //固定しない場合処理を続行
    if (!secure) {
        //strings(数字以外の表示項目配列)があれば
        std::vector<std::string>strings;
        //最優先でflag指定をチェック
        if (!map["stringsWithFlag"].isNull()) {
            auto flagMap=map["stringsWithFlag"].asValueMap();
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(flagMap["flag"].asInt())) {
                auto valueVec=flagMap["values"].asValueVector();
                for (auto value : valueVec) {
                    strings.push_back(value.asString());
                }
                setStrings(strings);
            }
        }
        
        if (strings.size()==0)
        {//フラグによるセットをされていない
            if (!map["strings"].isNull()) {
                auto valueVec=map["strings"].asValueVector();
                for (auto value : valueVec) {
                    strings.push_back(value.asString());
                }
                setStrings(strings);
            }
            else{
                //文字列を格納(defaultは数値)
                for (int i=0; i<10; i++) {
                    strings.push_back(StringUtils::format("%d",i));
                }
                setStrings(strings);
            }
        }
        
        //記憶を使用するか
        auto memory=map["memory"].asBool();
        setMemory(memory);
        if (memory) {
            checkMemory();
        }
    }
    
    return true;
}


#pragma mark- Public
void CountUpLabel::setStrings(std::vector<std::string> vec)
{
    strings=vec;
    setString(strings[0]);
}

void CountUpLabel::checkMemory()
{
    if(!DataManager::sharedManager()->getTemporaryFlag(getTemporaryKey()).isNull()){
        auto index=DataManager::sharedManager()->getTemporaryFlag(getTemporaryKey()).asInt();
        if (strings.size()>index) {
            setString(strings[index]);
        }
    }
}

void CountUpLabel::countUp()
{//現在の文字indexを取得->カウントアップ
    auto iter=std::find(strings.begin(), strings.end(), getString());
    auto index=std::distance(strings.begin(), iter);

    auto newIndex=(index+1)%strings.size();
    setString(strings[newIndex]);
    
    if(getPlaySE().size()>0){
        Common::playSE(getPlaySE().c_str());
    }
    
    if (getMemory()) {
        DataManager::sharedManager()->setTemporaryFlag(getTemporaryKey(),(int)newIndex);
    }
}

void CountUpLabel::setOutline(ValueMap map)
{
    if (map["enableOutline"].asBool()) {
        auto code=map["outlineColor"].asString();
        auto size=getContentSize().height*.02;
        if (code.size()>0) {
            enableOutline(Color4B(Common::getColorFromHex(DataManager::sharedManager()->getColorCodeData(code))),size);
        }
        else{
            enableOutline(Color4B::BLACK,size);
        }
    }
}

#pragma mark- Private
std::string CountUpLabel::getTemporaryKey()
{
    auto index=getBackNum();

    if(DataManager::sharedManager()->getShowingItemID()>0) {
        index=DataManager::sharedManager()->getShowingItemID();
    }
    
    auto key=StringUtils::format("label_%d_%d",index,getNumber());
    
    return key;
}
