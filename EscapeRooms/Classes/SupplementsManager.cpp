//
//  SupplementsManager.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/01/17.
//
//

#include "SupplementsManager.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "ZOrders.h"
#include "Utils/ValueHelper.h"
#include "Utils/Common.h"
#include "AnswerManager.h"
#include "EscapeStageSprite.h"
#include "EscapeDataManager.h"
#include "Utils/AdConstants.h"
#include "Utils/NativeAdManager.h"

#include "TempleActionManager.h"//寺
#include "HalloweenActionManager.h"//ハロウィン
#include "KidsRoomActionManager.h"//子供部屋
#include "TokyoActionManager.h"//東京
#include "GuestHouseActionManager.h"//ゲストハウス
#include "ChristmasActionManager.h"//クリスマス
#include "OnsenActionManager.h"//温泉旅館
#include "CatCafeActionManager.h"//ねこカフェ
#include "BabyRoomActionManager.h"//赤ちゃん
#include "SweetHouseActionManager.h"//お菓子の家

#include "SakuraActionManager.h"//桜
#include "YashikiActionManager.h"//屋敷
#include "LiveHouseActionManager.h"
#include "PublicBathActionManager.h"
#include "CandyHouseActionManager.h"
#include "WeddingActionManager.h"
#include "NakayubiCorporationActionManager.h"
#include "PigHatActionManager.h"
#include "PigHat2ActionManager.h"
#include "LaboratoryActionManager.h"
#include "TheaterActionManager.h"
#include "GhostHouseActionManager.h"
#include "CabaretActionManager.h"
#include "CircusActionManager.h"
#include "TutorialActionManager.h"//チュートリアル
#include "WhiteDayActionManager.h"
#include "SportsGymActionManager.h"
#include "MomijiActionManager.h"
#include "BoyFriendActionManager.h"
#include "IzakayaActionManager.h"
#include "BeachSideActionManager.h"
#include "SchoolFesActionManager.h"
#include "TwoThousandNineteenActionManager.h"
#include "ChocolateShopActionManager.h"
#include "GirlFriendActionManager.h"
#include "AmusementParkActionManager.h"
#include "OhanamiActionManager.h"
#include "OmoideActionManager.h"
#include "FarmActionManager.h"

using namespace cocos2d;

SupplementsManager* SupplementsManager::manager =NULL;

SupplementsManager* SupplementsManager::getInstance()
{
    if (manager==NULL) {
        manager=new SupplementsManager();
    }
    return manager;
}

#pragma mark - Supplement
void SupplementsManager::addSupplementToMain(cocos2d::Node *parent, int backNum)
{
    addSupplementToMain(parent, backNum,false);
}

void SupplementsManager::addSupplementToMain(cocos2d::Node *parent, int backNum, bool addAllsup)
{
    addSupplementToMain(parent, backNum, addAllsup, ShowType_Normal);
}

void SupplementsManager::addSupplementToMain(cocos2d::Node *parent, int backNum, bool addAllsup, ShowType showType)
{
    //backDataを取得.使い回す.
    auto backMap=DataManager::sharedManager()->getBackData(backNum);
    //補完画像データを取得
    auto supplementsMap=DataManager::sharedManager()->getSupplementsMap(backMap);
    auto removes=DataManager::sharedManager()->getRemoveSupplementsNames(backMap, false);
    Vector<Sprite*>supplements;
    
    //フラグが立っている箇所の補完画像を追加
    //順番をフラグIDで手動ソートする
    std::vector<float>vec_key;
    for (auto& value :supplementsMap)
    {//allkeys,allvaluesをしゅとくしますよ
        auto key=atof(value.first.c_str());//キーとなるフラグ(float)
        if (key==0||DataManager::sharedManager()->getEnableFlagWithFlagID(key))
        {//対象フラグが立っている。(指定フラグ0の場合は通す)対象フラグの補完画像配列をチェック
            auto fileNames=value.second.asValueVector();
            for (auto nameValue: fileNames) {
                auto fileName=nameValue.asString();
                
                if (!ValueHelper::isContainsValue(removes, fileName)) {
                    //追加していい
                    int addIndex=0;
                    for(auto compareKey:vec_key)
                    {
                        if (key>=compareKey) {
                            addIndex++;
                        }
                        else{
                            break;
                        }
                    }
                    
                    auto spr=addSupplementToCenter(parent, fileName, false,false);//getCustomActionManager()->createSpriteToCenter(fileName, false, parent, false);
                    spr->setLocalZOrder(DataManager::sharedManager()->getZorderBySupName(fileName, Zorder_Main,backNum));

                    //スプライトとキーをを格納
                    vec_key.insert(vec_key.begin()+addIndex, key);
                    supplements.insert(addIndex, spr);
                }
            }
        }
    }
    
    //順番にaddchildする
    for (auto spr:supplements) {
        parent->addChild(spr);
    }
    
    //temporaryフラグをチェック
    auto temporaryVec=DataManager::sharedManager()->getTemporarySupplementsVec(backMap);
    for (auto v:temporaryVec) {
        auto temporaryMap=v.asValueMap();
        auto key_temporary=temporaryMap["key"].asString();
        auto temporaryFlag=DataManager::sharedManager()->getTemporaryFlag(key_temporary).asInt();
        auto values=temporaryMap["values"].asValueVector();
        bool add=false;
        
        auto enable=checkAddable(temporaryMap);
        if (!enable) {
            continue;
        }
        
        //指定フラグが指定されたValueと一致していればaddする
        for (auto v :values) {
            if (temporaryFlag==v.asInt()) {
                add=true;
                break;
            }
        }
        
        
        if (add) {
            auto fileName=temporaryMap["name"].asString();
            if (temporaryMap["dynamic"].asBool())
            {//動的にfile名を生成する
                fileName.append(StringUtils::format("_%d.png",temporaryFlag));
            }
            
            addSupplementToCenter(parent, fileName, false, true);
        }
    }
    
    //その他フラグのチェック
    auto otherFlagVec=DataManager::sharedManager()->getOtherFlagSupplementsVec(backMap);
    for(auto v:otherFlagVec){
        auto otherMap=v.asValueMap();
        auto key_other=otherMap["key"].asString();
        if (DataManager::sharedManager()->getOtherFlag(key_other).asInt()==otherMap["value"].asInt()) {
            if (checkAddable(otherMap)) {
                auto sup =addSupplementToCenter(parent, otherMap["name"].asString(), false, true);
                sup->setLocalZOrder(otherMap["zOrder"].asFloat());
            }
        }
    }

    if (addAllsup) {
        //

        addAnchorSpritesToMain(parent, backMap);
        
        addClippingNode(parent, false, backMap, showType);

        
        //間違い探し用スプライト
        addMysterySup(parent, false, backNum);
        
        //LabelSpriteを使用
        addLabelSupplement(parent, false, backMap, showType);
        //switchSpriteを使用
        addSwitchSupplements(parent, false, backMap, showType);
        //rotateSpriteを使用
        addRotateSupplements(parent, false, backMap, showType);
        //changing
        addChangingSupplement(parent, false, backMap, showType);

        //useInputを使用
        addInputSupplement(parent, false, backMap,showType);
        addInputRotateSupplement(parent, false, backMap,showType);
        
        //並び替え
        addOrderSupplements(parent, false, backMap, showType);
        
        //
        addDragSupplements(parent, false, backMap, showType);
        
        //
        addSliderSupplements(parent, false, backMap, showType);

        
        //
        addSpineSupplement(parent, backMap, showType);
        
        //
        addParticle(parent,backNum, false,showType);
    }
}

#pragma mark - Item
Sprite* SupplementsManager::createItemBox(int itemID)
{
    auto itemData=DataManager::sharedManager()->getItemData(itemID);
    if (itemData.size()==0) {
        return NULL;
    }
    auto spr = EscapeStageSprite::createWithSpriteFileName(itemData["name"].asString());
    spr->setPosition(spr->getContentSize().width/2, spr->getContentSize().height/2);
    spr->setCascadeOpacityEnabled(true);
    auto white=Sprite::create();
    white->setTextureRect(Rect(0, 0, spr->getContentSize().width, spr->getContentSize().height));
    white->setCascadeOpacityEnabled(true);
    white->addChild(spr);
    
    addSupplementToPopUp(spr, itemID, 0, true, ShowType_Normal);
    addMysterySup(spr, true,itemID);

    return white;
}

#pragma mark - Common
Sprite* SupplementsManager::addSupplementToCenter(Node *parent, std::string name,bool invisible,bool addChild)
{
    auto spr = EscapeStageSprite::createWithSpriteFileName(name);
    spr->setPosition(parent->getContentSize()/2);
    spr->setCascadeOpacityEnabled(true);
    spr->setScale(parent->getContentSize().width/spr->getContentSize().width);
    spr->setName(name);
    
    if (invisible) {
        spr->setOpacity(0);
    }
    
    if (addChild) {
        parent->addChild(spr);
    }
    
    return spr;
}

void SupplementsManager::transformToAnchorSprite(Node *parent, Node *targetNode, Vec2 anchorPoint)
{
    targetNode->setAnchorPoint(anchorPoint);
    targetNode->setPosition(anchorPoint.x*parent->getContentSize().width, anchorPoint.y*parent->getContentSize().height);
}

#pragma mark popup
void SupplementsManager::addSupplementToPopUp(Node *parent, int itemID, int angle, bool addAllsup, ShowType showType)
{
    auto itemMap=DataManager::sharedManager()->getItemData(itemID);
    auto value=itemMap["supplements"];
    if (!value.isNull()) {
        auto supMap=value.asValueMap();
        auto filenamesValue=supMap[StringUtils::format("%d",angle)];
        if (!filenamesValue.isNull()) {
            auto fileNames=filenamesValue.asValueVector();
            auto removes=DataManager::sharedManager()->getRemoveSupplementsNames(itemMap, true);

            for (auto name : fileNames) {
                if (!ValueHelper::isContainsValue(removes, name.asString())) {
                    //removesにnameが含まれていない
                    auto spr = EscapeStageSprite::createWithSpriteFileName(name.asString());
                    spr->setPosition(parent->getContentSize()/2);
                    spr->setName(name.asString());
                    parent->addChild(spr);
                }
            }
        }
    }
    
    //より応用の効く指定方法
    auto value_custom=itemMap["supplements_custom"];
    if (!value_custom.isNull()) {
        auto sup_vec=value_custom.asValueVector();
        for (auto v : sup_vec) {
            auto s_map=v.asValueMap();
            if (checkAddable(s_map)) {
                addSupplementToCenter(parent, s_map["fileName"].asString(), false, true);
            }
        }
    }
    
    if (addAllsup) {
        //間違い探し用スプライト
        addMysterySup(parent, true, itemID);
        //LabelSpriteを使用
        addLabelSupplement(parent, true,angle, itemMap, showType);
        //switchSpriteを使用
        addSwitchSupplements(parent, true,angle, itemMap, showType);
        //input
        addInputSupplement(parent, true,angle, itemMap, showType);

        //
        addParticle(parent,itemID, true,showType);
        //
    }
}

#pragma mark - Anchor
void SupplementsManager::addAnchorSpritesToMain(Node *parent, ValueMap backMap)
{
    auto map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Anchor,backMap);
    if (map.size()==0) {
        return;
    }
    
    auto fileName_head=map["name"].asString();
    auto counter=0;
    for (auto v : map["sprites"].asValueVector()) {
        auto sprMap=v.asValueMap();
        auto x=sprMap["x"].asFloat();
        auto y=sprMap["y"].asFloat();
        auto zOrder=sprMap["zOrder"].asInt();
        auto fileName=sprMap["name"].asString();
        auto enable=checkAddable(sprMap);
        
        if (fileName.size()==0){
            fileName=StringUtils::format("%s_%d.png",fileName_head.c_str(),counter);
        }
        
        if (!enable) {
            counter++;
            continue;
        }
        
        auto sup=AnchorSprite::create(Vec2(x, y), parent->getContentSize(), fileName);
        sup->setLocalZOrder(zOrder);
        parent->addChild(sup);
        
        counter++;
    }
}

#pragma mark - Spine
void SupplementsManager::addSpineSupplement(Node *parent, ValueMap backMap, ShowType showType)
{
    auto map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNaameOnBack_Spine, backMap);
    if (map.size()==0) {
        return;
    }
    
    if (!checkAddable(map)) {
        return;
    }
    
    auto spine=addSpine(parent, map["name"].asString());
    spine->update(0);
    auto x=.5;
    auto y=.5;
    if (!map["x"].isNull()) {
        x=map["x"].asFloat();
    }
    if (!map["y"].isNull()) {
        y=map["y"].asFloat();
    }
    
    //set position
    spine->setPosition(parent->getContentSize().width*x, parent->getContentSize().height*y);
    //set scale
    if (!map["width"].isNull()) {
        spine->setScale(parent->getContentSize().width*map["width"].asFloat()/spine->getBoundingBox().size.width);
    }
}

spine::SkeletonAnimation* SupplementsManager::addSpine(Node *parent, std::string name)
{
    auto path = EscapeDataManager::getInstance()->getPlayTitleResourceDirWithFile(name.c_str());

    auto spine=spine::SkeletonAnimation::createWithJsonFile(path+".json",path+".atlas");
    spine->setName(name);
    parent->addChild(spine);
    
    return spine;
}

#pragma mark - ClippingNode
void SupplementsManager::addClippingNode(Node *parent, bool onItem, ValueMap backMap, ShowType showType)
{
    auto clipVec=DataManager::sharedManager()->getCustomSupplementsVector(CustomActionNameOnBack_Clipping,backMap);
    if (clipVec.size()==0) {
        return;
    }
    
    for (auto mapv : clipVec) {
        auto clipMap=mapv.asValueMap();
        
        auto name=clipMap["name"].asString();
        auto clip=ClippingNode::create(addSupplementToCenter(parent, name, false, false));
        clip->setAlphaThreshold(0);
        clip->setInverted(false);
        clip->setName(name);
        clip->setCascadeOpacityEnabled(true);
        parent->addChild(clip);
        
        auto supplements=clipMap["supplements"];
        if (!supplements.isNull()) {
            
            auto supVec=supplements.asValueVector();
            for (auto v : supVec) {
                auto supMap=v.asValueMap();
                if (checkAddable(supMap)) {
                    auto supName=supMap["name"].asString();
                    auto sup=addSupplementToCenter(parent, supName, false, false);
                    clip->addChild(sup);
                }
            }
        }
    }
}

#pragma mark - Input
void SupplementsManager::addInputSupplement(Node *parent, bool onItem, ValueMap backMap, ShowType showType)
{
    addInputSupplement(parent, onItem,DataManager::sharedManager()->getShowingAngle(), backMap, showType);
}

void SupplementsManager::addInputSupplement(Node *parent, bool onItem,int angle, ValueMap backMap, ShowType showType)
{
    auto inputMap=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Input,backMap);
    if (inputMap.size()==0) {
        return;
    }
    
    if (!checkAddable(inputMap,angle)&&
        !DataManager::sharedManager()->isShowTypeAnswer(showType)) {
        return;
    }
    
    auto defaultAdd=inputMap["defaultAdd"].asBool();
    int addType=0;
    if (defaultAdd) {
        addType=1;
    }
    if (addType==0) {
        addType=inputMap["addType"].asInt();
    }
    
    if (addType>0) {
        auto map=inputMap["animateSupplements"].asValueMap();
        for (auto v : map) {
            auto fileName=v.second.asString();
            auto sup = EscapeStageSprite::createWithSpriteFileName(fileName);
            sup->setName(fileName);
            sup->setLocalZOrder(inputMap["zOrder"].asInt());
            sup->setScale(parent->getContentSize().width/sup->getContentSize().width);

            sup->setPosition(parent->getContentSize()/2);
            parent->addChild(sup);
        }
    }
    
    if (DataManager::sharedManager()->isShowTypeAnswer(showType)) {//答えを強制アニメーション
        auto answer=inputMap["answer"].asValueMap()["answer"].asString();
        auto duration=MAX(inputMap["duration"].asFloat(), .1);//最低でも.1s
        
        log("答えは %s",answer.c_str());
        Vector<FiniteTimeAction*>actions;
        int i=0;
        while (i<answer.size()) {
            auto index=atoi(answer.substr(i,1).c_str());
            auto sprmap=inputMap["animateSupplements"].asValueMap();
            auto fileName=sprmap[StringUtils::format("%d",index)].asString();
            
            auto playSE=(showType==ShowType_Answer_Big)? inputMap["playSE"].asString():"";
            if (addType==1) {
                Node*spr=parent->getChildByName(fileName);
                actions.pushBack(TargetedAction::create(spr, Sequence::create(FadeOut::create(0),
                                                                              Common::createSoundAction(playSE.c_str()),
                                                                              DelayTime::create(duration),
                                                                              FadeIn::create(0), NULL)));
                
            }
            else if(addType==2){
                Node*spr=parent->getChildByName(fileName);
                actions.pushBack(TargetedAction::create(spr, Sequence::create(FadeOut::create(0),
                                                                              Common::createSoundAction(playSE.c_str()),
                                                                              DelayTime::create(duration),
                                                                              NULL)));
            }
            else{
                auto spr=getCustomActionManager()
                ->createSpriteToCenter(fileName, true, parent, true);
                actions.pushBack(TargetedAction::create(spr, Sequence::create(FadeIn::create(0.1),
                                                                              Common::createSoundAction(playSE.c_str()),
                                                                              DelayTime::create(duration),
                                                                              FadeOut::create(0.1), NULL)));
            }
            actions.pushBack(DelayTime::create(.3));
            
            i++;
        }
        
        auto repCount=-1;
        if (addType==2) {
            repCount=1;
        }
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),
                                                          Sequence::create(actions),
                                                          DelayTime::create(1), NULL), repCount));
    }
}

void SupplementsManager::addInputRotateSupplement(Node *parent, bool onItem, ValueMap backMap, ShowType showType)
{
    auto inputMap=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_InputRotate,backMap);
    if (inputMap.size()==0) {
        return;
    }
    
    if (!checkAddable(inputMap)) {
        return;
    }
    
    auto map=inputMap["supplement"].asValueMap();
    auto fileName=map["name"].asString();
    auto angles=map["angles"].asValueVector();
    auto sup=AnchorSprite::create(Vec2(map["x"].asFloat(), map["y"].asFloat()), parent->getContentSize(), fileName);
    sup->setLocalZOrder(inputMap["zOrder"].asInt());
    sup->setScale(parent->getContentSize().width/sup->getContentSize().width);
    parent->addChild(sup);
    
    if (DataManager::sharedManager()->isShowTypeAnswer(showType)) {//答えを強制アニメーション
        auto answer=inputMap["answer"].asValueMap()["answer"].asString();
        auto duration=MAX(inputMap["duration"].asFloat(), .1);//最低でも.1s
        
        log("答えは %s",answer.c_str());
        Vector<FiniteTimeAction*>actions;
        int i=0;
        while (i<answer.size()) {
            auto index=atoi(answer.substr(i,1).c_str());
            auto angle=angles.at(index).asFloat();
            auto playSE=(showType==ShowType_Answer_Big)?inputMap["playSE"].asString():"";
            actions.pushBack(swingAction(sup, angle, duration, playSE));
            actions.pushBack(DelayTime::create(.3));
            
            i++;
        }
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),
                                                          Sequence::create(actions),
                                                          DelayTime::create(1), NULL), -1));
    }
}

#pragma mark - Changing
void SupplementsManager::addChangingSupplement(Node *parent, bool onItem, ValueMap backMap, ShowType showType)
{
    auto map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Changing,backMap);

    if (!onItem) {
        if (showType==ShowType_Normal) {
            changingSprites.clear();
        }
    }
    
    if (map.size()==0) {
        return;
    }
    
    //追加して良いか
    if (!DataManager::sharedManager()->isShowTypeAnswer(showType)) {
        if (!checkAddable(map)) {
            return;
        }
    }
    
    auto answerMap=map["answer"];
    auto name=map["name"].asString();
    
    auto sprs=map["sprites"].asValueVector();
    auto counter=0;
    for (auto value: sprs) {
        auto sprMap=value.asValueMap();
        if (!sprMap["name"].isNull()) {
            name=sprMap["name"].asString();
        }
        
        auto spr=ChangingSprite::create(name, sprMap, answerMap, counter);
        spr->setPosition(parent->getContentSize()/2);
        if (!sprMap["x"].isNull()) {
            SupplementsManager::getInstance()->transformToAnchorSprite(parent, spr, Vec2(sprMap["x"].asFloat(), sprMap["y"].asFloat()));
        }
        
        spr->setDeflection(map["deflection"].asFloat());
        spr->setPlaySE(map["playSE"].asString());
        spr->setDelay(1.0);
        if (!map["delay"].isNull()) {
            spr->setDelay(map["delay"].asFloat());
        }
        spr->setDuration(1);
        if (!map["duration"].isNull()) {
            spr->setDuration(map["duration"].asFloat());
        }
        spr->setLocalZOrder(map["zOrder"].asInt());
        parent->addChild(spr);
        
        if (DataManager::sharedManager()->isShowTypeAnswer(showType)) {
            auto answer=answerMap.asValueMap()["answer"].asString();
            auto index=atoi(answer.substr(counter,1).c_str());
            spr->setSpriteFrame(EscapeStageSprite::createWithSpriteFileName(StringUtils::format("%s_%d_%d.png",name.c_str(),counter,index))->getSpriteFrame());
        }
        else if(showType==ShowType_Normal){
            if (!onItem) {
                changingSprites.pushBack(spr);
            }
        }
        
        
        auto startable=true;
        if (!map["startFlags"].isNull()) {
            auto flags=map["startFlags"].asValueVector();
            for (auto v : flags) {
                if (!DataManager::sharedManager()->getEnableFlagWithFlagID(v.asFloat())) {
                    startable=false;
                    break;
                }
            }
        }
        
        if (startable&&
            !DataManager::sharedManager()->isShowTypeAnswer(showType)) {
            spr->startAnimate();
        }
        
        counter++;
    }
}

#pragma mark - Label
void SupplementsManager::addLabelSupplement(Node *parent, bool onItem, ValueMap backMap, ShowType showType)
{
    addLabelSupplement(parent, onItem,DataManager::sharedManager()->getShowingAngle(), backMap, showType);
}

void SupplementsManager::addLabelSupplement(Node *parent, bool onItem,int angle, ValueMap backMap, ShowType showType)
{
    addLabelSupplement(parent, onItem,DataManager::sharedManager()->getShowingAngle(), backMap, showType,{});
}

void SupplementsManager::addLabelSupplement(Node *parent, bool onItem,int angle, ValueMap backMap, ShowType showType, std::vector<std::string> strings)
{
    auto map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Label,backMap);
    
    if (!onItem) {
        if (showType==ShowType_Normal) {
            labelSupplements.clear();
        }
    }
    else{
        if (showType==ShowType_Normal) {
            labelSupplements_onItem.clear();
        }
    }
    
    if (map.size()==0) {
        return;
    }
    
    auto answerMap=map["answer"];
    auto labelSupplementsVec=map["labels"].asValueVector();
    auto counter=0;
    auto backNum=DataManager::sharedManager()->getNowBack();
    if (showType==ShowType_OnPhoto) {
        backNum=DataManager::sharedManager()->getShowingPhotoID();
    }
    
    for (auto value: labelSupplementsVec) {
        auto labelMap=value.asValueMap();
        
        if (!DataManager::sharedManager()->isShowTypeAnswer(showType))
        {//ラベルを出すかチェック
            if (!checkAddable(map,angle)) {
                counter++;
                continue;
            }
            if (!checkAddable(labelMap,angle)) {
                counter++;
                continue;
            }
        }
        
        auto label=CountUpLabel::create(labelMap,answerMap,parent->getContentSize(),counter,backNum,map["fontPath"].asString(),map["fontSize"].asFloat()*parent->getContentSize().width,map["useBM"].asBool());
        if (label->getOutlineSize()==0) {
            label->setOutline(map);
        }
        label->setPlaySE(map["playSE"].asString());
        label->setLocalZOrder(labelMap["zOrder"].asInt());
        auto textColor=map["textColor"].asString();
        
        if (textColor.size()>0&&
            labelMap["textColor"].isNull()) {
            auto colorcode=DataManager::sharedManager()->getColorCodeData(map["textColor"].asString());
            label->setTextColor(Color4B(Common::getColorFromHex(colorcode)));
        }
        
        parent->addChild(label);
        
        if (DataManager::sharedManager()->isShowTypeAnswer(showType)) {//答えを強制セット
            auto answer=answerMap.asValueMap()["answer"].asString();
            if(counter<strings.size()){
                label->setString(strings.at(counter));
            }
            else{
                label->setString(ValueHelper::substr(answer, counter));
            }
        }
        else if(showType==ShowType_Normal)
        {//ラベル補完画像として格納
            if (!onItem) {
                labelSupplements.pushBack(label);
            }
            else{
                labelSupplements_onItem.pushBack(label);
            }
        }
        
        counter++;
    }
}


#pragma mark - Switch
void SupplementsManager::addSwitchSupplements(Node *parent, bool onItem, ValueMap backMap, ShowType showType)
{
    addSwitchSupplements(parent, onItem, DataManager::sharedManager()->getShowingAngle(), backMap, showType);
}

void SupplementsManager::addSwitchSupplements(Node *parent, bool onItem,int angle, ValueMap backMap, ShowType showType)
{
    ValueMap switchMap;
    if (!onItem) {
        if (showType==ShowType_Normal) {
            switchSprites.clear();
        }
    }
    else{
        if (showType==ShowType_Normal) {
            switchSprites_onItem.clear();
        }
    }
    switchMap=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Switch,backMap);
    
    //switchの指定がない場合空のmap
    if (switchMap.size()==0) {
        return;
    }
    
    //追加して良いか
    if (!DataManager::sharedManager()->isShowTypeAnswer(showType)) {
        if (!checkAddable(switchMap,angle)) {
            return;
        }
    }
    
    auto answerMap=switchMap["answer"];
    auto name=switchMap["name"].asString();
    auto temporary=switchMap["temporaryFlagKey"].asString();
    
    auto switchSprs=switchMap["sprites"].asValueVector();
    auto counter=0;
    for (auto value: switchSprs) {
        auto sprMap=value.asValueMap();
        
        //onPhotoの場合は読み込むkeyが変わる
        auto switchSpr=SwitchSprite::create(sprMap,answerMap,name,counter,temporary,showType);
        switchSpr->setPosition(parent->getContentSize()/2);
        switchSpr->setScale(parent->getContentSize().width/switchSpr->getContentSize().width);

        if(switchSpr->getPlaySE().size()==0){
            switchSpr->setPlaySE(switchMap["playSE"].asString());
        }
        
        if(!switchMap["zOrder"].isNull()){
            switchSpr->setLocalZOrder(switchMap["zOrder"].asInt());
        }
        
        parent->addChild(switchSpr);
        
        if (DataManager::sharedManager()->isShowTypeAnswer(showType)) {
            auto answerV=answerMap.asValueMap()["answer"];
            if (answerV.getType()==Value::Type::STRING) {
                auto answer=answerV.asString();
                auto index=atoi(answer.substr(counter,1).c_str());
                switchSpr->changeTexture([](Ref*ref){}, index);
            }
            else if (answerV.getType()==Value::Type::VECTOR){
                auto answerVec=answerV.asValueVector();
                //強制的にフラグをセットしてからリロードしている
                if (counter==0) {
                    for (auto v : answerVec) {
                        auto vMap=v.asValueMap();
                        auto key=vMap["key"].asString();
                        key=SupplementsManager::getInstance()->appendKey(key, showType);//answer用のkeyに
                        log("立てるkey %s",key.c_str());
                        DataManager::sharedManager()->setTemporaryFlag(key, vMap["value"].asInt());
                    }
                    
                    log("読むkey %s",switchSpr->getTemporaryKey(showType).c_str());
                    auto index=DataManager::sharedManager()->getTemporaryFlag(switchSpr->getTemporaryKey(showType)).asInt();
                    switchSpr->changeTexture(nullptr, index);
                }
            }
        }
        else if(showType==ShowType_Normal){
            if (!onItem) {
                switchSprites.pushBack(switchSpr);
            }
            else{
                switchSprites_onItem.pushBack(switchSpr);
            }
        }
        
        counter++;
    }
}

#pragma mark - Rotate
void SupplementsManager::addRotateSupplements(Node *parent, bool onItem, ValueMap backMap, ShowType showType)
{
    ValueMap rotateMap;
    if (!onItem) {
        if (showType==ShowType_Normal) {
            rotateSprites.clear();
        }
    }
    rotateMap=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Rotate,backMap);

    
    if (rotateMap.size()>0) {
        auto rotates=rotateMap["sprites"].asValueVector();
        auto temporary=rotateMap["temporaryFlagKey"].asString();

        auto answerV=rotateMap["answer"];
        auto counter=0;
        for(auto v:rotates){
            auto name=StringUtils::format("%s_%d.png",rotateMap["name"].asString().c_str(),counter);
            
            auto vmap=v.asValueMap();
            auto rotate=RotateSprite::create(name,parent->getContentSize(),vmap,answerV,counter,temporary);
            parent->addChild(rotate);
            
            if(rotate->getSE().size()==0){
                rotate->setSE(rotateMap["playSE"].asString());
            }
            
            if (DataManager::sharedManager()->isShowTypeAnswer(showType)) {
                auto answer=answerV.asValueMap()["answer"].asString();
                auto index=atoi(answer.substr(counter*2,2).c_str());//答えは1ボタンあたり2桁で記憶されてる
                rotate->setRotation(rotate->getAngle()*index);
            }
            else if(showType==ShowType_Normal){
                if (!onItem) {
                    rotateSprites.pushBack(rotate);
                }
            }
            
            counter++;
        }
    }
}

#pragma mark - Order
void SupplementsManager::addOrderSupplements(Node *parent, bool onItem, ValueMap backMap, ShowType showType)
{
    if (!onItem) {
        if (showType==ShowType_Normal) {
            orderSprites.clear();
        }
    }
    auto map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Order,backMap);

    
    if (map.size()==0) {
        return;
    }
    
    //追加して良いか
    if (!checkAddable(map)) {
        return;
    }
    
    auto seName=map["playSE"].asString();
    auto name=map["name"].asString();
    //auto selectedZorder=map["selectedZorder"].asInt();
    auto answerMap=map["answer"];
    auto animate=map["animate"].asInt();
    auto temporaryKey=map["temporaryFlagKey"].asString();
    ValueVector setItemVec;
    if (!map["setItem"].isNull()) {
        setItemVec=map["setItem"].asValueVector();
    }
    auto vec=map["sprites"].asValueVector();
    
    auto counter=0;
    for (auto v:vec) {
        auto vmap=v.asValueMap();
        
        auto spr=OrderSprite::create(vmap,answerMap,name,temporaryKey, counter,parent->getContentSize(),setItemVec);
        spr->setPosition(parent->getContentSize().width*spr->getAnchorPoint().x,parent->getContentSize().height*spr->getAnchorPoint().y);
        spr->setDefaultPos(spr->getPosition());
        spr->setPlaySE(seName);
        //spr->setSelectedZOrder(selectedZorder);
        spr->setOrderAnimateType((OrderAnimateType)animate);
        parent->addChild(spr);
        
        if (DataManager::sharedManager()->isShowTypeAnswer(showType)) {
            auto answer=answerMap.asValueMap()["answer"].asString();
            auto index=atoi(answer.substr(counter,1).c_str());
            spr->change(index,parent->getContentSize(),false,false);
        }
        else if(showType==ShowType_Normal){
            orderSprites.pushBack(spr);
        }
        
        counter++;
    }
}

#pragma mark - Slider
void SupplementsManager::addSliderSupplements(Node *parent, bool onItem, ValueMap backMap, ShowType showType)
{
    if (!onItem) {
        if (showType==ShowType_Normal) {
            sliderSprites.clear();
        }
    }
    auto map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Slider,backMap);

    
    if (map.size()==0) {
        return;
    }
    
    //追加して良いか
    if (!checkAddable(map)) {
        return;
    }
    
    auto answerMap=map["answer"];
    
    auto name=map["name"].asString();
    
    auto temporary=map["temporaryFlagKey"].asString();
    auto duration=map["duration"].asFloat();

    auto sprs=map["sprites"].asValueVector();
    int counter=0;
    for (auto value: sprs) {
        auto sprMap=value.asValueMap();
        if (!checkAddable(sprMap)) {
            counter++;
            continue;
        }
        
        //onPhotoの場合は読み込むkeyが変わる
        auto spr=SliderSprite::create(sprMap,answerMap,name,counter,parent->getContentSize(),temporary,(showType==ShowType_OnPhoto));

        if(spr->getPlaySE().size()==0){
            spr->setPlaySE(map["playSE"].asString());
        }
        
        if (duration>0) {
            spr->setDuration(duration);
        }
        
        parent->addChild(spr);
        
        if (DataManager::sharedManager()->isShowTypeAnswer(showType)) {
            auto answer=answerMap.asValueMap()["answer"].asString();
            spr->setPosition(spr->positions.at(atoi(answer.substr(counter,1).c_str())));
        }
        else if(showType==ShowType_Normal){
            if (!onItem) {
                sliderSprites.pushBack(spr);
            }
        }
        
        counter++;
    }
}

#pragma mark - Drag
void SupplementsManager::addDragSupplements(Node *parent, bool onItem, ValueMap backMap, ShowType showType)
{
    if (!onItem) {
        if (showType==ShowType_Normal) {
            dragSprites.clear();
        }
    }
    
   auto map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Drag,backMap);

    
    if (map.size()==0) {
        return;
    }
    
    auto answer=map["answer"];
    
    for (auto v : map["sprites"].asValueVector()) {
        auto sprMap=v.asValueMap();
        if (!checkAddable(sprMap)) {
            continue;
        }
        
        auto sup=DragSprite::create(sprMap,answer, parent->getContentSize());
        if (showType==ShowType_Normal) {
            dragSprites.pushBack(sup);
        }
        parent->addChild(sup);
    }
}

#pragma mark - Pattern
void SupplementsManager::clearPatternSprites(bool onItem)
{
    if (!onItem) {
        patternSprites.clear();
    }
    else{
        if (patternSprites_onItem.size()>0) {
            patternSprites_onItem.at(0)->removeFromParent();
        }
        patternSprites_onItem.clear();
    }
}

void SupplementsManager::addPatternSprite(Node *parent, bool onItem, ValueMap backMap, ShowType showType)
{
    auto map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Pattern,backMap);
    
    auto radius=parent->getContentSize().height*.01;
    if (!map["radius"].isNull()) {
        radius=map["radius"].asFloat()*parent->getContentSize().width;
    }
    auto startAreaWidth=map["startAreaWidth"].asFloat()*parent->getContentSize().width;//タッチ範囲判定
    auto endAreaWidth=startAreaWidth*.65;//終点範囲判定
    auto lineColor=Color4F::WHITE;
    
    std::vector<Vec2> startAreas;
    for (auto value : map["startPoses"].asValueVector()) {
        auto value_map=value.asValueMap();
        startAreas.push_back(Vec2(value_map["x"].asFloat(), value_map["y"].asFloat()));
    }
    
    auto pattern=PatternSprite::create(parent);
    pattern->setPosition(parent->getContentSize()/2);
    pattern->setStartAreas(startAreas);
    pattern->setCenteringAnchor(map["centeringAnchor"].asBool());
    pattern->setLineWidth(radius);
    pattern->setStartAreaWidth(startAreaWidth);
    pattern->setEndAreaWidth(endAreaWidth);
    pattern->setLineColor(lineColor);
    pattern->setPlaySE(map["playSE"].asString());
    parent->addChild(pattern);
    
    
    if (DataManager::sharedManager()->isShowTypeAnswer(showType)) {
        //答えをセットして表示
        auto answer=map["answer"].asValueMap()["answer"].asString();
        for (int i=0; i<answer.size()-1; i++) {
            auto index_start=AnswerManager::getInstance()->transformStringToNumber(answer.substr(i,1));
            auto index_end=AnswerManager::getInstance()->transformStringToNumber(answer.substr(i+1,1));
            auto pos_start=pattern->getCenterOnStartArea(index_start);
            auto pos_end=pattern->getCenterOnStartArea(index_end);
            //dotを描画
            pattern->drawDotOnPattern(pattern->getCenterOnStartArea(index_start));
            if (i==answer.size()-2) {
                pattern->drawDotOnPattern(pos_end);
            }
            
            //線分を描画
            pattern->drawLineOnPattern(pos_start, pos_end);
        }
    }
    else if(showType==ShowType_Normal){
        if (!onItem) {
            patternSprites.pushBack(pattern);
        }
        else{
            patternSprites_onItem.pushBack(pattern);
        }
    }
}

#pragma mark - Mystery
void SupplementsManager::addMysterySup(cocos2d::Node *parent, bool onItem,int ID)
{
    if (!DataManager::sharedManager()->isPlayingMinigame()) {//minigame はじまってない
        return;
    }
    
    ValueVector mysteryIDs;
    if (!onItem) {
        auto v=DataManager::sharedManager()->mysteryMap[StringUtils::format("%d",ID)];
        if (!v.isNull()) {
            mysteryIDs=v.asValueVector();
        }
    }
    else{
        auto v=DataManager::sharedManager()->mysteryMap_onItem[StringUtils::format("%d",ID)];
        if (!v.isNull()) {
            mysteryIDs=v.asValueVector();
        }
    }
    
    //mysteryIDごとに処理を実行
    for (auto _mysteryID :mysteryIDs)
    {
        
        auto mysteryID=_mysteryID.asInt();
        log("間違い 表示するmystery:%d",mysteryID);
        
        ValueMap supplementsMap=getMysterySupMap(mysteryID,onItem,ID);
        
        //flagCheck
        auto otherFlag=supplementsMap["needOtherFlag"];
        if (!otherFlag.isNull()) {
            auto otherFlagMap=otherFlag.asValueMap();
            if (DataManager::sharedManager()->getOtherFlag(otherFlagMap["key"].asString()).asInt()!=otherFlagMap["value"].asInt()) {
                log("間違いsup 必要flag不足");
                continue;
            }
        }
        
        auto zOrder=supplementsMap["zOrder"].asFloat();
        auto opacity=255.0;
        if (!supplementsMap["opacity"].isNull()) {
            opacity=supplementsMap["opacity"].asFloat();
        }
        
        //mistakeを貼っていく
        std::vector<std::string>fileNames;
        auto fileName=supplementsMap["name"].asString();
        if (fileName.size()>0) {
            fileNames.push_back(fileName);
        }
        
        if (!supplementsMap["names"].isNull()) {
            for (auto v: supplementsMap["names"].asValueVector()) {
                fileNames.push_back(v.asString());
            }
        }
        
        for (auto name : fileNames) {
            auto sup = EscapeStageSprite::createWithSpriteFileName(name);
            if (sup->getReferenceCount()==0 || sup == NULL) {//エラー回避 cc_safe_releaseされるので
                log("間違い 存在しないファイル名です %s",name.c_str());
                return;
            }
            auto x=.5;
            auto y=.5;
            if (!supplementsMap["x"].isNull()) {
                x=supplementsMap["x"].asFloat();
            }
            if (!supplementsMap["y"].isNull()) {
                y=supplementsMap["y"].asFloat();
            }
            
            sup->setLocalZOrder(zOrder);
            sup->setAnchorPoint(Vec2(x, y));
            sup->setPosition(parent->getContentSize().width*x,parent->getContentSize().height*y);
            sup->setName(name);
            sup->setOpacity(opacity);
            sup->setCascadeOpacityEnabled(true);
            parent->addChild(sup);
        }
        
        //correct
        if (!supplementsMap["zOrderCorrect"].isNull()) {
            zOrder=supplementsMap["zOrderCorrect"].asInt();
        }
        
        //正解していたら
        auto flag=DataManager::sharedManager()->getMysteryMap(mysteryID)["enFlagID"].asInt();
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(flag)) {
            auto fileName_correct=supplementsMap["correctName"].asString();
            if (fileName_correct.size()>0) {
                auto correct = EscapeStageSprite::createWithSpriteFileName(fileName_correct);
                correct->setPosition(parent->getContentSize()/2);
                correct->setLocalZOrder(zOrder);
                parent->addChild(correct);
            }
        }
    }
}

void SupplementsManager::correctAction(cocos2d::Node *parent, bool onItem, int ID, const onFinished &callback)
{
    //タッチひとうか
    auto layer=createDisableLayer();
    Director::getInstance()->getRunningScene()->addChild(layer);
    
    auto mysteryID=ID;
    auto mysteryMap=DataManager::sharedManager()->getMysteryMap(mysteryID);
    auto enFlagID=mysteryMap["enFlagID"].asInt();
    auto backNum=onItem?DataManager::sharedManager()->getShowingItemID():DataManager::sharedManager()->getNowBack();
    auto supplementsMap=SupplementsManager::getInstance()->getMysterySupMap(mysteryID, onItem,backNum);
    
    //間違いsprのファイル名を格納していく
    std::vector<std::string>fileNames;
    if (supplementsMap["name"].asString().size()>0) {
        fileNames.push_back(supplementsMap["name"].asString());
    }
    if (!supplementsMap["names"].isNull()) {
        for (auto name : supplementsMap["names"].asValueVector()) {
            fileNames.push_back(name.asString());
        }
    }
    
    Vector<FiniteTimeAction*> spawn_actions;
    for (auto n:fileNames) {
        auto sup=parent->getChildByName<Sprite*>(n);
        spawn_actions.pushBack(TargetedAction::create(sup, Repeat::create(Sequence::create(FadeOut::create(.2),FadeIn::create(.3), NULL), 2)));
    }
    
    
    //corerct
    auto correctName=supplementsMap["correctName"].asString();
    //flash
    std::vector<std::string>flashNames;
    if (!supplementsMap["flashName"].isNull()) {
        flashNames.push_back(supplementsMap["flashName"].asString());
    }
    if (!supplementsMap["flashNames"].isNull()) {
        for (auto name : supplementsMap["flashNames"].asValueVector()) {
            flashNames.push_back(name.asString());
        }
    }
    
    for (std::string flashName : flashNames) {
        Vector<FiniteTimeAction*>flashActions;
        auto flashAction=Repeat::create(Sequence::create(FadeIn::create(.2),FadeOut::create(.3), NULL),2);
        flashActions.pushBack(flashAction);
        
        //対象がすでにaddChildされているかどうかで分岐する
        auto node=parent->getChildByName(flashName);
        if (!node) {
            log("flashSpriteを新規追加");
            //点滅スプライトを新規追加
            node = EscapeStageSprite::createWithSpriteFileName(flashName);
            node->setName(flashName);
            node->setPosition(parent->getContentSize()/2);
            node->setOpacity(0);
            
            auto parentName=supplementsMap["flashParent"].asString();
            if (parentName.size()>0) {
                parent->getChildByName(parentName)->addChild(node);
            }
            else{
                parent->addChild(node);
            }
            
            flashActions.pushBack(RemoveSelf::create());
        }
        
        spawn_actions.pushBack(TargetedAction::create(node, Sequence::create(flashActions)));
    }
    
    
    Vector<FiniteTimeAction*>actions;
    //点滅まとめ
    auto spawn_flash=Spawn::create(spawn_actions);
    actions.pushBack(spawn_flash);
    
    if (!DataManager::sharedManager()->getEnableFlagWithFlagID(enFlagID))
    {//新発見の間違い
        log("間違いを発見");
        AnswerManager::getInstance()->setFoundMystery(true);
        Common::playSE("pinpon.mp3");
        AnswerManager::getInstance()->enFlagIDs.push_back(enFlagID);
        
        auto zorder=(int)Zorder_Main;
        if (!supplementsMap["zOrderCorrect"].isNull()) {
            zorder=supplementsMap["zOrderCorrect"].asInt();
        }else if (!supplementsMap["zOrder"].isNull()){
            zorder=supplementsMap["zOrder"].asInt();
        }
        
        auto delay_2=DelayTime::create(.3);
        actions.pushBack(delay_2);
        
        auto call=CallFunc::create([parent,layer,callback,correctName,zorder](){
            layer->removeFromParent();
            
            //correctSupを追加
            if (!parent->getChildByName(correctName)) {
                auto spr = EscapeStageSprite::createWithSpriteFileName(correctName);
                spr->setPosition(parent->getContentSize()/2);
                spr->setName(correctName);
                spr->setLocalZOrder(zorder);
                parent->addChild(spr);
            }
            
            callback(NULL);
        });
        actions.pushBack(call);
    }
    else
    {//発見済みの謎 透明度のみいじる
        auto call=CallFunc::create([this,layer,callback](){
            layer->removeFromParent();
            if (callback) {
                callback(true);
            }
        });
        actions.pushBack(call);
    }
    
    parent->runAction(Sequence::create(actions));
}

ValueMap SupplementsManager::getMysterySupMap(int mysteryID,bool onItem,int ID)
{
    auto mysteryMap=DataManager::sharedManager()->getMysteryMap(mysteryID);
    
    if (!onItem) {
        log("現在のidは%d",ID);
        ValueHelper::dumpValue(mysteryMap["supplements"]);
        ValueHelper::dumpValue(mysteryMap["supplements"].asValueMap()[StringUtils::format("%d",ID)]);
        return mysteryMap["supplements"].asValueMap()[StringUtils::format("%d",ID)].asValueMap();//現在のbackに表示すべき情報
    }
    else{
        auto supVector=mysteryMap["supplements"].asValueMap()[StringUtils::format("%d",ID)].asValueVector();
        auto supMapValue=supVector.at(DataManager::sharedManager()->getShowingAngle());
        return supMapValue.asValueMap();
    }
}

#pragma mark - Particle
void SupplementsManager::addParticle(Node *parent, int backID, bool onItem, ShowType type)
{
    if (type==ShowType_Normal) {
        particles.clear();
    }
    
    ValueVector particleVec;
    particleVec=DataManager::sharedManager()->getParticleVector(onItem,backID);
    
    if (particleVec.size()>0) {
        for (auto v : particleVec) {
            auto particleMap=v.asValueMap();
            
            auto enable=checkAddable(particleMap);
            if(!particleMap["disableFlagID"].isNull()){
                if (DataManager::sharedManager()->getEnableFlagWithFlagID(particleMap["disableFlagID"].asFloat()))
                {
                    enable=false;
                }
            }
            
            if (enable) {
                //可能ならbatchNodeに貼り付ける処理
                std::string fileName=particleMap["name"].asString();
                auto mapFromPlist=FileUtils::getInstance()->getValueMapFromFile(fileName);
                std::string batchName="";
                if (!particleMap["texture"].isNull()) {
                    batchName=particleMap["texture"].asString();
                }
                else if(mapFromPlist["textureImageData"].asString().size()==0)
                {//使ってないと思うけど一応pngを使わないパターンを除外
                    batchName=mapFromPlist["textureFileName"].asString();
                }
                
                Node* batch=NULL;
                if (batchName.size()>0) {
                    batch=ParticleBatchNode::create(batchName);
                    batch->setCascadeOpacityEnabled(true);
                    parent->addChild(batch);
                }
                
                auto particle=getParticle(parent,particleMap);
                if(batch)
                {
                    batch->setLocalZOrder(particle->getLocalZOrder());
                    batch->addChild(particle);
                }
                else{
                    parent->addChild(particle);
                }
                
                if (type==ShowType_Normal) {
                    particles.pushBack(particle);
                }
            }
        }
    }
}

ParticleSystemQuad* SupplementsManager::getParticle(cocos2d::Node *parent, ValueMap particleMap)
{
    auto fileName=particleMap["name"].asString();
    auto particle=ParticleSystemQuad::create(fileName);
    particle->setAutoRemoveOnFinish(true);
    particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
    particle->setPosition(particleMap["x"].asFloat()*parent->getContentSize().width,particleMap["y"].asFloat()*parent->getContentSize().height);
    particle->setLocalZOrder(particleMap["zOrder"].asInt());
    if (!particleMap["texture"].isNull()) {
        auto texture=Sprite::create(particleMap["texture"].asString())->getTexture();        
        particle->setTexture(texture);
    }
    if (!particleMap["duration"].isNull()) {
        particle->setDuration(particleMap["duration"].asFloat());
    }
    if (!particleMap["max"].isNull()) {
        particle->setTotalParticles(particleMap["max"].asInt());
    }
    if (!particleMap["duration"].isNull()) {
        particle->setDuration(particleMap["duration"].asFloat());
    }
    if (!particleMap["startSize"].isNull()) {
        particle->setStartSize(parent->getContentSize().width*particleMap["startSize"].asFloat());
    }
    if (!particleMap["var_startSize"].isNull()) {
        particle->setStartSizeVar(parent->getContentSize().width*particleMap["var_startSize"].asFloat());
    }
    if (!particleMap["finishSize"].isNull()) {
        particle->setEndSize(parent->getContentSize().width*particleMap["finishSize"].asFloat());
    }
    if (!particleMap["var_finishSize"].isNull()) {
        particle->setEndSizeVar(parent->getContentSize().width*particleMap["var_finishSize"].asFloat());
    }
    if (!particleMap["lifeSpan"].isNull()) {
        particle->setLife(particleMap["lifeSpan"].asFloat());
    }
    if (!particleMap["var_life"].isNull()) {
        particle->setLifeVar(particleMap["var_life"].asFloat());
    }
    if (!particleMap["speed"].isNull()) {
        particle->setSpeed(parent->getContentSize().width*particleMap["speed"].asFloat());
    }
    if (!particleMap["var_speed"].isNull()) {
        particle->setSpeedVar(parent->getContentSize().width*particleMap["var_speed"].asFloat());
    }
    
    if (!particleMap["var_x"].isNull()) {
        particle->setPosVar(Vec2(parent->getContentSize().width*particleMap["var_x"].asFloat(),parent->getContentSize().width*particleMap["var_y"].asFloat()));
    }
    if (!particleMap["gravity_x"].isNull()) {
        particle->setGravity(Vec2(parent->getContentSize().width*particleMap["gravity_x"].asFloat(),-parent->getContentSize().width*particleMap["gravity_y"].asFloat()));
    }
    if (!particleMap["startColorAlpha"].isNull()) {
        particle->setStartColor(Color4F(particleMap["startColorRed"].asFloat(), particleMap["startColorGreen"].asFloat(), particleMap["startColorBlue"].asFloat(), particleMap["startColorAlpha"].asFloat()));
    }
    if (!particleMap["finishColorAlpha"].isNull()) {
        particle->setEndColor(Color4F(particleMap["finishColorRed"].asFloat(), particleMap["finishColorGreen"].asFloat(), particleMap["finishColorBlue"].asFloat(), particleMap["finishColorAlpha"].asFloat()));
    }
    if (!particleMap["angle"].isNull()) {
        particle->setAngle(particleMap["angle"].asFloat());
    }
    if (!particleMap["var_angle"].isNull()) {
        particle->setAngleVar(particleMap["var_angle"].asFloat());
    }
    if (!particleMap["startSpin"].isNull()) {
        particle->setStartSpin(particleMap["startSpin"].asFloat());
    }
    if (!particleMap["var_startSpin"].isNull()) {
        particle->setStartSpinVar(particleMap["var_startSpin"].asFloat());
    }
    if (!particleMap["endSpin"].isNull()) {
        particle->setEndSpin(particleMap["endSpin"].asFloat());
    }
    if (!particleMap["var_endSpin"].isNull()) {
        particle->setEndSpinVar(particleMap["var_endSpin"].asFloat());
    }
    if (!particleMap["blendFuncDestination"].isNull()) {
        BlendFunc blend;
        blend.dst=particleMap["blendFuncDestination"].asInt();
        blend.src=particleMap["blendFuncSource"].asInt();
        particle->setBlendFunc(blend);
    }
    
    return particle;
}

ParticleSystemQuad* SupplementsManager::getParticleFromPlist(std::string fileName)
{
    auto particle=ParticleSystemQuad::create(fileName);
    particle->setName(fileName);
    particle->setAutoRemoveOnFinish(true);
    particle->setPositionType(ParticleSystemQuad::PositionType::RELATIVE);
    
    return particle;
}


#pragma mark - パネル
void SupplementsManager::showPanel(cocos2d::Node *parent)
{
    showPanel(parent, DataManager::sharedManager()->getNowBack());
}

void SupplementsManager::showPanel(cocos2d::Node *parent, int num)
{
    if(DataManager::sharedManager()->getStopPanelAd()){
        return;
    }
    if(DataManager::sharedManager()->getShowingItemID()>0){
        return;
    }
    
    auto panelMap=DataManager::sharedManager()->getPanelData(num);
    
    if (panelMap.size()==0){
        return;
    }
    
    auto name = panelMap["name"].asString();
    
    auto origin_x = panelMap["x"].asFloat();
    auto origin_y = panelMap["y"].asFloat();
    auto width = panelMap["width"].asFloat();
    
    auto needFlags = panelMap["needFlags"];//指定フラグが全て立っていないなら処理しない
    if (!needFlags.isNull()) {
        auto vector=needFlags.asValueVector();
        
        for (auto value :vector) {
            
            //log("下記フラグが立っているかチェック%d",value.asInt());
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(value.asInt())==false) {
                log("必要フラグ不足によるパネル広告表示不可");
                return ;
            }
        }
    }
    
    auto removeFlags = panelMap["removeFlags"];
    if (!removeFlags.isNull()) {
        auto vector = removeFlags.asValueVector();
        
        for (auto value :vector) {
            //log("下記フラグが立っているかチェック%d",value.asInt());
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(value.asInt())==true) {
                log("広告表示終了のフラグが立っているのでパネル広告表示不可");
                return ;
            }
        }
    }
    
    float scale = parent->getScale();
    
    //表示するパネル広告サイズ(5:3になるように)
    auto panelSize =Size(width*parent->getContentSize().width, width*parent->getContentSize().width*.6)*scale;
    
    //表示位置を絶対座標に変換
    auto absolutePoint = parent->convertToWorldSpace(Vec2(origin_x*parent->getContentSize().width, origin_y*parent->getContentSize().height));
    
    //バナーは左上から
    auto bannerPoint = Vec2(absolutePoint.x, Director::getInstance()->getRunningScene()->getContentSize().height-absolutePoint.y);
    
    //広告IDをplistに書いた場合。
    std::string id;
    
    if (!panelMap["IDKey"].isNull()) {//広告IDが挿入されていたら。
        auto idMap = panelMap["IDKey"].asString();
        
        auto idKey = StringUtils::format("%s_%s", EscapeDataManager::getInstance()->getPlayTitleName().c_str(),idMap.c_str());
        auto bannerID = AdConstants::getPanelID(idKey.c_str());
        
        if (bannerID.size() == 0) {//該当IDなし。共通IDの呼び出し。
            bannerID = AdConstants::getPanelID(idMap.c_str());
        }
        
        id = bannerID;
    }
    else {
        id = "";
    }
    
    for (auto showingID : showPanelIDs) {
        if (id==showingID) {
            log("指定したpanelはすでに表示中");
            return;
        }
    }
    
    NativeAdManager::getInstance()->showNativeAd(id.c_str(), bannerPoint, panelSize);
    showPanelIDs.push_back(id);
}

void SupplementsManager::hidePanel()
{
    NativeAdManager::getInstance()->hideNativeAd();
    showPanelIDs.clear();
}

#pragma mark - Finger
FingerSprite* SupplementsManager::addFinger(Node *parent, Vec2 vec, bool animate)
{
    auto finger=FingerSprite::create("finger_hint.png",vec);
    
    auto width=parent->getContentSize().width*.2;
    
    finger->setPosition(Vec2(parent->getContentSize().width*vec.x+width/2*pow(-1, (int)finger->getDirectionX()), parent->getContentSize().height*vec.y+width/2*pow(-1, (int)finger->getDirectionY())));
    finger->setLocalZOrder(999);
    finger->setScale(width/finger->getContentSize().width);
    finger->setName(fingerName);
    parent->addChild(finger);
    
    if (animate) {
        finger->pointAnimate();
    }
    
    return finger;
}

#pragma mark - 汎用
bool SupplementsManager::checkAddable(ValueMap map)
{
    return checkAddable(map,DataManager::sharedManager()->getShowingAngle());
}

bool SupplementsManager::checkAddable(cocos2d::ValueMap map,int angle)
{
    //&検索
    std::vector<float>needFlags;
    if(!map["needFlagID"].isNull()){
        needFlags.push_back(map["needFlagID"].asFloat());
    }
    if (!map["needFlagIDs"].isNull()) {
        auto vec=map["needFlagIDs"].asValueVector();
        for (auto needflag : vec) {
            needFlags.push_back(needflag.asFloat());
        }
    }
    
    for (auto needFlag : needFlags) {
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(needFlag))
        {//必要なフラグが立っていない
            return false;
        }
    }
    
    //angle これは論理和
    std::vector<int>needAngles;
    if (!map["needAngle"].isNull()) {
        needAngles.push_back(map["needAngle"].asInt());
    }
    
    if (needAngles.size()>0) {
        auto angleOK=false;
        for (auto v : needAngles) {
            if (angle==v) {
                angleOK=true;
                break;
            }
        }
        
        if (!angleOK) {
            return false;
        }
    }
    
    
    //remove
    if(!map["removeFlagID"].isNull()){
        log("removeFlag %f",map["removeFlagID"].asFloat());
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(map["removeFlagID"].asFloat()))
        {
            return false;
        }
    }
    
    return true;
}

std::string SupplementsManager::appendKey(std::string key, ShowType type)
{
    if (type==ShowType_OnPhoto) {
        return key.append("photo");
    }
    else if (DataManager::sharedManager()->isShowTypeHint(type)){
        return key.append("hint");
    }
    else if (DataManager::sharedManager()->isShowTypeAnswer(type)){
        return key.append("answer");
    }
    return key;
}

void SupplementsManager::addDebugSprite(Node *parent, Rect rect)
{//rectはboundingboxで取得してるよ
    auto spr=Sprite::create();
    spr->setTextureRect(Rect(0, 0, rect.size.width/parent->getScale(), rect.size.height/parent->getScale()));
    spr->setPosition((rect.origin.x+rect.size.width/2)/parent->getScale(),(rect.origin.y+rect.size.height/2)/parent->getScale());//親スケールを考慮
    spr->setColor(Color3B::RED);
    spr->setOpacity(150);
    parent->addChild(spr);
    
    /*log("DEBUG: X:%f Y:%f W:%f H:%f",spr->getPosition().x/getContentSize().width,spr->getPosition().y/getContentSize().height,spr->getContentSize().width/getContentSize().width,spr->getContentSize().height/getContentSize().height);
     log("DEBUG: W:%f H:%f",spr->getBoundingBox().size.width,spr->getBoundingBox().size.height);
     log("DEBUG: W:%f H:%f",getBoundingBox().size.width,getBoundingBox().size.height);*/
    
    auto fade=FadeOut::create(.3);
    auto remove=RemoveSelf::create();
    
    spr->runAction(Sequence::create(fade,remove, NULL));
}

#pragma mark- タッチ不透過
LayerColor* SupplementsManager::createDisableLayer()
{
    auto layer=LayerColor::create(Color4B(0, 0, 0, 0));
    layer->setLocalZOrder(999);
    
    auto listner=EventListenerTouchOneByOne::create();
    listner->setSwallowTouches(true);//透過させない
    listner->onTouchBegan=[this](Touch*touch,Event*event)
    {
        return true;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listner, layer);
    
    return layer;
}

#pragma mark 補完画像アニメーション
void SupplementsManager::animateSupplement(Node* parent,std::string fileName,SupAnimateType type, float duration,float value, ccMenuCallback callback)
{
    if (fileName.size()==0) {
        callback(NULL);
        return;
    }
    
    //タッチ透過させない(親に貼って全面に)
    auto layer=SupplementsManager::getInstance()->createDisableLayer();
    Director::getInstance()->getRunningScene()->addChild(layer);
    
    
    if (type==SupAnimateType_Flash) {
        auto spr=parent->getChildByName<Sprite*>(fileName);
        if (!spr) {

            log("flashファイルをaddしました");
            spr=addSupplementToCenter(parent, fileName, false, true);
        }
        else{
            spr->setOpacity(0);
        }
        
        //アニメーション
        auto wait=DelayTime::create(duration);
        auto callfunc=CallFunc::create([layer,callback,spr](){
            layer->removeFromParent();
            if (spr->getOpacity()==0) {
                spr->setOpacity(255);
            }
            else{
                spr->removeFromParent();
            }
            
            if (callback) {
                callback(NULL);
            }
        });
        
        parent->runAction(Sequence::create(wait,callfunc,NULL));
    }
    else if (type==SupAnimateType_FadeOut) {
        auto spr=parent->getChildByName(fileName);
        
        //アニメーション
        auto fadeout=TargetedAction::create(spr,FadeOut::create(duration));
        auto callfunc=CallFunc::create([layer,callback,spr](){
            layer->removeFromParent();
            spr->removeFromParent();
            
            if (callback) {
                callback(NULL);
            }
        });
        
        parent->runAction(Sequence::create(fadeout,callfunc,NULL));
    }
    else if(type==SupAnimateType_SwingRotate){
        auto spr=parent->getChildByName(fileName);
        //アニメーション
        auto swing=swingAction(spr, value, duration,"");
        auto callfunc=CallFunc::create([layer,callback,spr](){
            layer->removeFromParent();
            
            if (callback) {
                callback(NULL);
            }
        });
        
        parent->runAction(Sequence::create(swing,callfunc,NULL));
    }
    else
    {//add
        auto spr = EscapeStageSprite::createWithSpriteFileName(fileName);
        if (spr->getReferenceCount()==0) {
            log("input file名が存在しません %s",fileName.c_str());
            layer->removeFromParent();
            callback(NULL);
            return;
        }
        
        spr->setPosition(parent->getContentSize()/2);
        spr->setScale(parent->getContentSize().width/spr->getContentSize().width);
        parent->addChild(spr);
        
        //アニメーション
        auto wait=DelayTime::create(duration);
        auto callfunc=CallFunc::create([layer,callback,spr](){
            layer->removeFromParent();
            spr->removeFromParent();
            
            if (callback) {
                callback(NULL);
            }
        });
        
        parent->runAction(Sequence::create(wait,callfunc,NULL));
    }
}

FiniteTimeAction* SupplementsManager::swingAction(Node *node, float angle, float duration, std::string soundName)
{
    return TargetedAction::create(node,Sequence::create(Common::createSoundAction(soundName),
                                                        EaseInOut::create(RotateBy::create(duration, angle), 1.5),
                                                EaseInOut::create(RotateBy::create(duration, -angle), 1.5), NULL));
}

#pragma mark - カスタムアクション
void SupplementsManager::customBackAction(cocos2d::Node *parent,int backID, std::string key,ShowType type)
{
    CustomActionManager*manager=getCustomActionManager();
    
    //実行
    manager->backAction(key, backID, parent);
    manager->backAction(key, backID, parent, type);
}

void SupplementsManager::customTouchAction(cocos2d::Node *parent, std::string key, const actionCallback &callback)
{
    auto manager=getCustomActionManager();
    
    //実行
    manager->touchAction(key, parent,[callback](bool success){
        callback(success);
    });
}

CustomActionManager* SupplementsManager::getCustomActionManager()
{
    auto title=EscapeDataManager::getInstance()->getPlayTitleName();
    return getCustomActionManager(title);
}


CustomActionManager* SupplementsManager::getCustomActionManager(std::string title)
{
    CustomActionManager*manager;
    
    if (title=="tutorial") {
        manager=TutorialActionManager::getInstance();
    }
    else if (title=="farm") {
        manager=FarmActionManager::getInstance();
    }
    else if (title=="omoide") {
        manager=OmoideActionManager::getInstance();
    }
    else if (title=="ohanami") {
        manager=OhanamiActionManager::getInstance();
    }
    else if (title=="amusementpark") {
        manager=AmusementParkActionManager::getInstance();
    }
    else if (title=="girlfriend") {
        manager=GirlFriendActionManager::getInstance();
    }
    else if (title=="chocolateshop") {
        manager=ChocolateShopActionManager::getInstance();
    }
    else if (title=="twothousandnineteen") {
        manager=TwoThousandNineteenActionManagerManager::getInstance();
    }
    else if (title=="sweethouse") {
        manager=SweetHouseActionManager::getInstance();
    }
    else if (title=="schoolfes") {
        manager=SchoolFesActionManager::getInstance();
    }
    else if(title=="beachside") {
        manager=BeachSideActionManager::getInstance();
    }
    else if(title=="izakaya") {
        manager=IzakayaActionManager::getInstance();
    }
    else if(title=="boyfriend") {
        manager=BoyFriendActionManager::getInstance();
    }
    else if(title=="circus") {
        manager=CircusActionManager::getInstance();
    }
    else if(title=="momiji") {
        manager=MomijiActionManager::getInstance();
    }
    else if(title=="sportsgym") {
        manager=SportsGymActionManager::getInstance();
    }
    else if(title=="cabaret"){
        manager=CabaretActionManager::getInstance();
    }
    else if(title=="ghosthouse"){
        manager=GhostHouseActionManager::getInstance();
    }
    else if(title=="theater"){
        manager=TheaterActionManager::getInstance();
    }
    else if(title=="laboratory"){
        manager=LaboratoryActionManager::getInstance();
    }
    else if(title=="pighat"){
        manager=PigHatActionManager::getInstance();
    }
    else if(title=="pighat2"){
        manager=PigHat2ActionManager::getInstance();
    }
    else if(title=="nakayubicorporation"){
        manager=NakayubiCorporationActionManager::getInstance();
    }
    else if (title=="wedding") {
        manager=WeddingActionManager::getInstance();
    }
    else if (title=="candyhouse") {
        manager=CandyHouseActionManager::getInstance();
    }
    else if (title=="publicbath") {
        manager=PublicBathActionManager::getInstance();
    }
    else if (title=="livehouse") {
        manager=LiveHouseActionManager::getInstance();
    }
    else if (title=="yashiki") {
        manager=YashikiActionManager::getInstance();
    }
    else if (title=="sakura") {
        manager=SakuraActionManager::getInstance();
    }
    else if (title=="whiteday") {
        manager=WhiteDayActionManager::getInstance();
    }
    else if (title=="babyroom") {
        manager=BabyRoomActionManager::getInstance();
    }
    else if (title=="catcafe") {
        manager=CatCafeActionManager::getInstance();
    }
    else if (title=="onsen") {
        manager=OnsenActionManager::getInstance();
    }
    else if (title=="christmas") {
        manager=ChristmasActionManager::getInstance();
    }
    else if (title=="guesthouse") {
        manager=GuestHouseActionManager::getInstance();
    }
    else if (title=="tokyo") {
        manager=TokyoActionManager::getInstance();
    }
    else if (title=="kidsroom") {
        manager=KidsRoomActionManager::getInstance();
    }
    else if (title=="temple") {
        manager=TempleActionManager::getInstance();
    }
    else if(title=="halloween") {
        manager=HalloweenActionManager::getInstance();
    }
    
    
    return manager;
}
