//
//  DragSprite.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2017/11/20.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________DragSprite_____
#define _____PROJECTNAMEASIDENTIFIER________DragSprite_____

#include "cocos2d.h"
#include "EscapeStageSprite.h"
USING_NS_CC;

class DragSprite:public EscapeStageSprite
{
public:
    static DragSprite* create(ValueMap map,Value answerMapV,Size parentSize);

    bool init(ValueMap map,Value answerMapV ,Size parentSize);
    /**parent基準のタッチ座標を渡す isInBeganがtrueのときtouchBeganで呼び出されている*/
    void drag(Vec2 pos,Vec2 touchStartPos,bool onItem,bool isInBegan);
    
    CC_SYNTHESIZE(Vec2, defaultPosition, DefaultPosition)
    CC_SYNTHESIZE(Vec2, selectedDistance, SelectedDistance);
    CC_SYNTHESIZE(Rect, endRect, EndRect);
    CC_SYNTHESIZE(float, borderLeft, BorderLeft);
    CC_SYNTHESIZE(float, borderRight, BorderRight);
    CC_SYNTHESIZE(float, borderTop, BorderTop);
    CC_SYNTHESIZE(float, borderBottom, BorderBottom);
    CC_SYNTHESIZE(std::string, endFileName, EndFileName);
private:

};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
