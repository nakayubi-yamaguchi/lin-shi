//
//  SchoolFesActionManager.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2018/11/29.
//
//

#include "OmoideActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "GameScene.h"
#include "TouchManager.h"

using namespace cocos2d;

OmoideActionManager* OmoideActionManager::manager =NULL;

OmoideActionManager* OmoideActionManager::getInstance()
{
    if (manager==NULL) {
        manager = new OmoideActionManager();
    }
        
    return manager;
}

#pragma mark - 背景
int OmoideActionManager::getDefaultBackNum()
{
    /*
    if(!DataManager::sharedManager()->isShowedOpeningStory()) {
        return 0;
    }*/
    
    int backNum = 1;
    
    Common::performProcessForDebug([&backNum](){

        backNum = 3;
    }, nullptr);
    
    return backNum;
}

#pragma mark - Back
void OmoideActionManager::backAction(std::string key, int backID, Node *parent, ShowType type)
{
#pragma mark 仏壇
    if(key == "butsudan") {
        std::vector<Vec2> poses;
        float sizeRatio = 0;
        float life = 0;
        float speed = 0;
        
        if (backID == 64) {//仏壇アップ
            speed = .06;
            life = .4;
            sizeRatio = .025;
            poses = {Vec2(.3, .467), Vec2(.695, .467)};
        }
        else if (backID == 3) {//引き
            speed = .015;
            life = .2;
            sizeRatio = .003;
            poses = {Vec2(.343, .509), Vec2(.422, .509)};
        }
        
        for (auto pos : poses) {
            auto particle=ParticleSystemQuad::create("fire.plist");
            particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width*pos.x, parent->getContentSize().height*pos.y);
            particle->setTotalParticles(40);
            particle->setStartColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 150));
            particle->setStartSize(parent->getContentSize().width*sizeRatio);
            particle->setSpeed(parent->getContentSize().height*speed);
            
            particle->setTotalParticles(120);
            particle->setLife(life);
            particle->setLifeVar(.05);
            
            parent->addChild(particle);
        }
    }
#pragma mark 線香の煙
    else if(key == "senko") {
        showSmokeParticle(parent, backID);
    }
#pragma mark 将棋のコマ
    else if(key == "syogi_end" && DataManager::sharedManager()->getEnableFlagWithFlagID(33)) {
        
        for (int i = 0; i < 6; i++) {
            auto file = StringUtils::format("sup_68_koma_%d.png", i);
            auto koma = parent->getChildByName<Sprite*>(file);
            transformToAnchorSprite(parent, koma, komaAnchors.at(i));
            
            koma->setPosition(getMovedSyogiPos(parent, i));
        }
    }
#pragma mark ツイスター
    else if(key == "twister") {
        Vector<FiniteTimeAction*> actions;
        std::vector<Vec2> anchorPoses;
        std::string fileName;
        if (backID == 51) {//子供部屋
            fileName = "sup_%d_twisterpig_%d.png";
            anchorPoses = {Vec2(.8, .202), Vec2(.897, .109)};
        }
        else if (backID == 60) {//ツイスターアップ
            fileName = "sup_%d_pig_%d.png";
            anchorPoses = {Vec2(.29, .368), Vec2(.728, .259)};
        }
       
        //全員フラフラ
        for (int i = 0; i < anchorPoses.size(); i++) {
            auto pig = parent->getChildByName(StringUtils::format(fileName.c_str(), backID, i));
            transformToAnchorSprite(parent, pig, anchorPoses.at(i));
            pig->setLocalZOrder(2);
            
            float random = (float)RandomHelper::random_int(0, 10);
            float duration = 1+random/10;
            actions.pushBack(swingAction(2.5, duration, pig, 2, true, -1));
        }
        
        parent->runAction(Spawn::create(actions));
    }
#pragma mark ネズミが震える
    else if(key == "mouse") {
        //ネズミが震える
        auto mouse = parent->getChildByName<Sprite*>(StringUtils::format("sup_%d_mouse.png", backID));
        
        if (mouse) {
            
            createSpriteToCenter(StringUtils::format("sup_%d_trembling.png", backID), false, mouse, true);
            parent->runAction(swingAction(Vec2(parent->getContentSize().width*.01, 0), .1, mouse, 1.5, false, -1));
        }
    }
#pragma mark 鯉のぼり
    else if(key == "carp") {
        for (int i=0; i<5; i++) {
            createWaveSprite(parent, StringUtils::format("sup_%d_carp_%d.png",backID,i), 1.0,.1*(float)i, Size(.025, .025), 1, .003, true, false, -1);
        }
    }
#pragma mark いけ
    else if(key == "pond") {
        auto grid_width=.1;
        auto amplitude=0.05;
        auto radius=.5;
        createRipple3DSprite(parent, "sup_23_water.png", 3.0, Size(grid_width, grid_width), Vec2(.5, .525), radius, 4, amplitude, -1);
    }
#pragma mark 金魚
    else if(key == "fish") {
        auto grid_width=.25;
         auto amplitude=0.01;
         auto radius=.5;
         createRipple3DSprite(parent, "sup_12_fish.png", 3.0, Size(grid_width, grid_width), Vec2(.5, .35), radius, 2, amplitude, -1);
        auto correct=DataManager::sharedManager()->getEnableFlagWithFlagID(9);
        showBubbles(parent, backID, correct);
    }
#pragma mark 眠る豚
    else if(key == "bed"&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(30)) {
        
        std::vector<int>indexs={4,0,2,3,1,3,1,0};
        Vector<FiniteTimeAction*>actions;
        
        for (int i=0; i<5; i++) {
            createSpriteToCenter(StringUtils::format("sup_%d_pig_%d.png",backID,i), i==4? false:true, parent,true);
        }
        
        for (int i=1; i<indexs.size(); i++) {
            auto nowIndex=indexs.at(i-1);
            auto nextIndex=indexs.at(i);
            auto pig_now=parent->getChildByName(StringUtils::format("sup_%d_pig_%d.png",backID,nowIndex));
            auto pig_next=parent->getChildByName(StringUtils::format("sup_%d_pig_%d.png",backID,nextIndex));
            actions.pushBack(DelayTime::create(1));
            
            if (i==1) {
                pig_now->setOpacity(255);
            }
            
            if (backID==58) {
                actions.pushBack(createSoundAction("pop_short.mp3", type));
            }
            actions.pushBack(createChangeAction(1.0, .7, pig_now, pig_next));
            
            //デフォルトに戻す
            if (i == indexs.size()-1) {
                actions.pushBack(DelayTime::create(1));
                actions.pushBack(createChangeAction(1.0, .7, pig_next, parent->getChildByName(StringUtils::format("sup_%d_pig_%d.png",backID,indexs.at(0)))));
            }
        }
        
        actions.pushBack(DelayTime::create(2));
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
#pragma mark まちがい 花壇
    else if(key == "plant"&&
            DataManager::sharedManager()->isPlayingMinigame()) {
        Vector<FiniteTimeAction*>actions;

        auto mistake=parent->getChildByName("sup_17_mistake.png");
        mistake->setPositionY(parent->getContentSize().height*.45);
        createSpriteToCenter("sup_17_cover.png", false, parent,true);
        
        actions.pushBack(DelayTime::create(1.5));
        actions.pushBack(createSoundAction("pop.mp3"));
        actions.pushBack(TargetedAction::create(mistake, Sequence::create(FadeIn::create(0),
                                                                          MoveTo::create(.2, parent->getContentSize()/2),
                                                                          NULL)));
        actions.pushBack(CallFunc::create([]{
            DataManager::sharedManager()->setTemporaryFlag("mistake_17", 1);
        }));
        
        parent->runAction(Sequence::create(actions));
    }
}

#pragma mark - Touch
void OmoideActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    Vector<FiniteTimeAction*> actions;
    //コールバック
    auto call = CallFunc::create([callback]{
        callback(true);
    });
    
    log("タッチキーは%s",key.c_str());

#pragma mark 吹き出し（クラウド）
    if(key.substr(0, 5) == "cloud") {
        
        int num = DataManager::sharedManager()->getNowBack();
        if (num == 31) {//だんご
            actions.pushBack(cloudBounceAction(parent, parent->getChildByName("sup_31_pig.png"), "sup_31_cloud.png", Vec2(.435, .66), "pig.mp3"));
        }
        else if (num == 67) {//将棋
            actions.pushBack(cloudBounceAction(parent, {"sup_67_pig_1.png"}, Vec2(.763, .377), "sup_67_cloud.png", Vec2(.662, .632), "pig.mp3"));
        }
        else if (num == 12) {//ビール欲しがる。
            actions.pushBack(cloudBounceAction(parent, {"sup_12_beer_pig_0.png"}, Vec2(.5, .0), "sup_12_cloud.png", Vec2(.638, .641), "pig.mp3"));
        }
        else if (num == 59) {//スティック
            actions.pushBack(createSoundAction("monkey.mp3"));
            auto angle=5;
            actions.pushBack(Spawn::create(cloudAction(parent, "sup_59_cloud.png", Vec2(.435, .66)),
                                                       swingAction(angle, .25, parent->getChildByName("sup_59_hand_0_0.png"), 1.5,false,2),
                                                       swingAction(-angle, .25, parent->getChildByName("sup_59_hand_1_1.png"), 1.5,false,2),
                                                       NULL));
        }
        
        actions.pushBack(call);
    }
#pragma mark - アイテム使用系
    else if (key.substr(0, 4) == "use_") {
        
        auto key_split = Common::splitString(key, "_");
        auto item = key_split.at(1);//key.substr(4);
        
        log("アイテムをあげる%s",item.c_str());
        actions.pushBack(useItemSoundAction());
#pragma mark マッチ使用
        if (item == "match") {
            auto match=createSpriteToCenter("sup_65_match.png", true, parent, true);
            transformToAnchorSprite(parent, match, Vec2(.347, .671));
            actions.pushBack(TargetedAction::create(match, FadeIn::create(1)));
            
            auto particles=DataManager::sharedManager()->getParticleVector(false, 65);
            std::vector<float> distances={.445, .555};
            Vector<FiniteTimeAction*>fireActions;
            
            for (int i=0; i<distances.size(); i++) {
                auto pos_x=parent->getContentSize().width*distances.at(i);
                
                auto move=TargetedAction::create(match, EaseInOut::create(MoveTo::create(.5, Vec2(pos_x, match->getPositionY())), 1.2));
                fireActions.pushBack(move);
                
                if (i==0) {
                    auto shake=jumpAction(parent->getContentSize().height*.01, .1, match, 1.2, 2);
                    fireActions.pushBack(shake);
                }
                
                fireActions.pushBack(createSoundAction("huo.mp3"));
                
                auto delay_short=DelayTime::create(.4);
                fireActions.pushBack(delay_short);
                
                auto fire=CallFunc::create([parent,particles,pos_x,i,match]{
                    auto particleMap=particles.at(i).asValueMap();
                    auto particle=SupplementsManager::getInstance()->getParticle(parent, particleMap);
                    particle->setName(StringUtils::format("fire_%d",i));
                    particle->setPosition(match->getPosition());
                    parent->addChild(particle);
                    
                    if (i==0) {
                        auto particle_match=SupplementsManager::getInstance()->getParticle(parent, particleMap);
                        particle_match->setPositionType(ParticleSystem::PositionType::FREE);
                        particle_match->setPosition(parent->getContentSize().width*match->getAnchorPoint().x, parent->getContentSize().height*match->getAnchorPoint().y);
                        particle_match->setName("fire");
                        match->addChild(particle_match);
                    }
                });
                fireActions.pushBack(fire);
                fireActions.pushBack(DelayTime::create(.3));
            }
            
            actions.pushBack(Sequence::create(fireActions));
            
            //上に行きながら消える
            auto fadeout_match=TargetedAction::create(match, Spawn::create(FadeOut::create(1),
                                                                           MoveBy::create(1, Vec2(0, parent->getContentSize().height*.05)),
                                                                           CallFunc::create([match]{
                auto particle = match->getChildByName<ParticleSystemQuad*>("fire");
                if (particle) {
                    particle->setDuration(0);
                }
            }), NULL));
            actions.pushBack(fadeout_match);
            actions.pushBack(DelayTime::create(.6));
            
            //火が消える。& 線香焼ける
            Vector<FiniteTimeAction*> fadeoutFireActions;
            for (int i = 0; i < distances.size(); i++) {
                fadeoutFireActions.pushBack(CallFunc::create([parent, i](){
                    auto particle = parent->getChildByName<ParticleSystemQuad*>(StringUtils::format("fire_%d",i));
                    if (particle) {
                        particle->setDuration(.3);
                    }
                }));
            }
            auto fadeoutFire = Spawn::create(fadeoutFireActions);
            
            auto senko = createSpriteToCenter("sup_65_senko.png", true, parent, true);
            senko->setLocalZOrder(1);
            actions.pushBack(Spawn::create(fadeoutFire,
                                           TargetedAction::create(senko, FadeIn::create(.3)), NULL));
            
            
            actions.pushBack(DelayTime::create(1.5));
        }
#pragma mark 将棋のコマあげる
        else if (item == "koma") {
            
            //コマあげる
            auto koma = createSpriteToCenter("sup_67_koma.png", true, parent, true);
            actions.pushBack(giveAction(koma, 1, .8, true));
            actions.pushBack(DelayTime::create(.5));
            
            //将棋を配置する
            auto syogi = createSpriteToCenter("sup_67_syogi_1.png", true, parent, true);
            auto syogi_2 = createSpriteToCenter("sup_67_syogi_2.png", true, parent, true);

            actions.pushBack(TargetedAction::create(syogi, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(.5));
            
            //将棋盤アップに遷移
            auto back68 = createSpriteToCenter("back_68.png", true, parent, true);
            SupplementsManager::getInstance()->addSupplementToMain(back68, 68, true);
            createSpriteToCenter("sup_68_others_1.png", false, back68, true);
            Vector<Sprite*> syogiKomas;
            for (int i = 0; i < 6; i++) {
                auto file = StringUtils::format("sup_68_koma_%d.png", i);
                auto sp = back68->getChildByName<Sprite*>(file);
                
                log("{%s}::%d", file.c_str(), sp != NULL);
                if (!sp) {
                    sp = createSpriteToCenter(file.c_str(), false, back68, true);
                }
                syogiKomas.pushBack(sp);
            }
            Vector<Sprite*> pigHands;
            for (int i = 0; i < 2; i++) {
                auto hand = createSpriteToCenter(StringUtils::format("sup_68_hand_%d.png", i), false, back68, true);
                transformToAnchorSprite(back68, hand, (i==0)? Vec2(.747, .509) : Vec2(.113, .447));
                hand->setPosition(parent->getContentSize().width*.05*pow(-1, i+1)+i*parent->getContentSize().width, parent->getContentSize().height/2);
                pigHands.pushBack(hand);
            }
            actions.pushBack(TargetedAction::create(back68, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(.5));
            
            //back67の将棋を入れ替え
            actions.pushBack(createChangeAction(.1, .1, syogi, syogi_2));
            
            //将棋のコマを動かす
            int k = 0;
            for (auto pos : komaAnchors) {
                bool isRight = k%2 == 1;
                auto hand = pigHands.at(isRight);
                auto originalHandPos = hand->getPosition();
                auto koma = syogiKomas.at(k);
                transformToAnchorSprite(back68, koma, pos);
                
                //腕動かし、コマを持つ
                actions.pushBack(TargetedAction::create(hand, EaseOut::create(MoveTo::create(.4, Vec2(parent->getContentSize().width*pos.x, parent->getContentSize().height*pos.y)), 1.5)));
                actions.pushBack(TargetedAction::create(hand, EaseOut::create(ScaleTo::create(.25, .9), 1.5)));
                actions.pushBack(Spawn::create(TargetedAction::create(hand, EaseOut::create(ScaleTo::create(.25, 1), 1.5)),
                                               TargetedAction::create(koma, EaseOut::create(ScaleTo::create(.25, 1.1), 1.5)), NULL));
                
                //将棋を持った腕を動かす。
                /*
                auto moveUnit = moveUnits.at(k);
                auto vec = Vec2(parent->getContentSize().width*moveUnit.x*moveUnitVec.x, parent->getContentSize().height*moveUnit.y*moveUnitVec.y);
                syogiPoses.push_back(Vec2(koma->getPositionX() + vec.x, koma->getPositionY() + vec.y));*/

                auto moveTo = EaseOut::create(MoveTo::create(.4, getMovedSyogiPos(parent, k)), 1.5);
                
                auto spawn = Spawn::create(TargetedAction::create(hand, moveTo),
                                           TargetedAction::create(koma, moveTo->clone()), NULL);
                actions.pushBack(spawn);
                
                //コマおく
                actions.pushBack(Spawn::create(TargetedAction::create(hand, EaseOut::create(ScaleTo::create(.25, .9), 1.5)),
                                               TargetedAction::create(koma, EaseOut::create(ScaleTo::create(.25, 1), 1.5)), NULL));
                actions.pushBack(createSoundAction("pisi.mp3"));
                actions.pushBack(TargetedAction::create(hand, EaseOut::create(ScaleTo::create(.25, 1), 1.5)));
                
                //引っ込む
                actions.pushBack(TargetedAction::create(hand, EaseOut::create(MoveTo::create(.4, originalHandPos), 1.5)));
                
                k++;
            }
            actions.pushBack(DelayTime::create(.6));
            
            actions.pushBack(TargetedAction::create(back68, FadeOut::create(.6)));
            actions.pushBack(DelayTime::create(.6));
        }
#pragma mark 招き猫&魚
        else if(item == "fishToCat") {
            auto fish = createSpriteToCenter("sup_87_fish.png", true, parent, true);
            actions.pushBack(TargetedAction::create(fish, FadeIn::create(1)));
            actions.pushBack(DelayTime::create(.7));
            
            auto arm = parent->getChildByName("sup_87_hand.png");
            Vec2 anchorPoint = Vec2(.397, .56);
            transformToAnchorSprite(parent, arm, anchorPoint);
            
            //アニメーション
            actions.pushBack(createSoundAction("cat_0.mp3"));

            auto move_distance = -parent->getContentSize().height*.02;
            auto duration_move = .4;
            auto move_0 = MoveBy::create(duration_move, Vec2(0, move_distance));
            auto scale_0 = EaseIn::create(ScaleTo::create(duration_move, 1.2, .5),2);
            auto spawn_0 = Spawn::create(move_0,scale_0, NULL);
            auto move_1 = MoveBy::create(duration_move, Vec2(0, -move_distance));
            auto scale_1 = EaseOut::create(ScaleTo::create(duration_move, 1,1), 2);
            auto spawn_1 = Spawn::create(move_1,scale_1, NULL);
            auto seq = TargetedAction::create(arm, Sequence::create(spawn_0,spawn_1,NULL));
            actions.pushBack(seq);
            
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark ボムマッチ
        else if(item == "bombmatch") {
            
            auto match=createSpriteToCenter("sup_55_match.png", true, parent, true);
            transformToAnchorSprite(parent, match, Vec2(.562, .736));
            actions.pushBack(TargetedAction::create(match, FadeIn::create(1)));
            
            auto particles=DataManager::sharedManager()->getParticleVector(false, 55);
            Vec2 vec =  Vec2(parent->getContentSize().width * .5, parent->getContentSize().height * .702);
            Vector<FiniteTimeAction*>fireActions;
            
            auto move=TargetedAction::create(match, EaseInOut::create(MoveTo::create(.5, vec), 1.2));
            fireActions.pushBack(move);
            
            auto shake=jumpAction(parent->getContentSize().height*.01, .1, match, 1.2, 2);
            fireActions.pushBack(shake);
            
            fireActions.pushBack(createSoundAction("huo.mp3"));
            
            auto delay_short=DelayTime::create(.4);
            fireActions.pushBack(delay_short);
            
            auto bomb = parent->getChildByName<Sprite*>("sup_55_bomb_0.png");
            auto fire=CallFunc::create([parent,particles,match,bomb]{
                auto particleMap=particles.at(0).asValueMap();
                auto particle=SupplementsManager::getInstance()->getParticle(parent, particleMap);
                particle->setName("fire_0");
                particle->setPosition(match->getPosition());
                bomb->addChild(particle);
            });
            fireActions.pushBack(fire);
            fireActions.pushBack(DelayTime::create(.3));
            
            actions.pushBack(Sequence::create(fireActions));
            
            //上に行きながら消える
            auto fadeout_match=TargetedAction::create(match, Spawn::create(FadeOut::create(1),
                                                                           MoveBy::create(1, Vec2(0, parent->getContentSize().height*.05)),
                                                                           /*
                                                                            CallFunc::create([match]{
                                                                            
                                                                            auto particle = match->getChildByName<ParticleSystemQuad*>("fire");
                                                                            if (particle) {
                                                                            particle->setDuration(0);
                                                                            }
                                                                            }),*/ NULL));
            actions.pushBack(fadeout_match);
            actions.pushBack(DelayTime::create(.6));
            
            //目の色が変わる
            auto eye = createSpriteToCenter("sup_55_bomb_eye.png", true, bomb, true);
            actions.pushBack(createSoundAction("emission.mp3"));
            actions.pushBack(TargetedAction::create(eye, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(.4));
            
            //back66に遷移
            auto back66 = createSpriteToCenter("back_66.png", true, parent, true);
            actions.pushBack(TargetedAction::create(back66, FadeIn::create(.7)));
            actions.pushBack(DelayTime::create(.5));
            
            //ボムを消す
            actions.pushBack(CallFunc::create([bomb](){
                auto par = bomb->getChildByName<ParticleSystemQuad*>("fire_0");
                if (par) {
                    par->setDuration(.1);
                }
            }));
            actions.pushBack(TargetedAction::create(bomb, FadeOut::create(.1)));
            
            //爆弾乱入
            auto bomb_66 = createSpriteToCenter("sup_66_bomb.png", false, back66, true);
            bomb_66->setPosition(Vec2(parent->getContentSize().width*-.16, parent->getContentSize().height*.6));
            auto particleMap=particles.at(0).asValueMap();
            auto particle=SupplementsManager::getInstance()->getParticle(parent, particleMap);
            particle->setName("fire");
            particle->setPosition(parent->getContentSize().width*.337, parent->getContentSize().height*.544);

            bomb_66->addChild(particle);
            
            actions.pushBack(createSoundAction("timer.mp3"));
            actions.pushBack(TargetedAction::create(bomb_66, JumpTo::create(1.5, parent->getContentSize()/2, parent->getContentSize().height*.02, 6)));
            actions.pushBack(TargetedAction::create(bomb_66, JumpBy::create(1.5, Vec2::ZERO, parent->getContentSize().height*.02, 6)));
            actions.pushBack(DelayTime::create(.7));
            
            
            //爆発
            actions.pushBack(createSoundAction("cannon.mp3"));
            auto smoke=CallFunc::create([back66,bomb_66]{
                BlendFunc func;
                func.dst=771;
                func.src=770;
                
                auto particle=ParticleSystemQuad::create("smoke.plist");
                particle->setAutoRemoveOnFinish(true);
                particle->setDuration(.2);
                particle->setPosition(Vec2(back66->getContentSize().width*.335, back66->getContentSize().height*.412));
                particle->setPosVar(Vec2(back66->getContentSize().width/8, back66->getContentSize().height/8));
                particle->setBlendFunc(func);
                particle->setStartColor(Color4F::BLACK);
                particle->setEndColor(Color4F(0, 0, 0, 0));
                particle->setSpeed(back66->getContentSize().height*.1);
                particle->setGravity(Vec2(0, -back66->getContentSize().height*.025));
                particle->setTotalParticles(200);
                particle->setAngle(90);
                particle->setAngleVar(10);
                particle->setLife(2);
                particle->setLifeVar(.5);
                particle->setStartSize(back66->getContentSize().width*.1);
                particle->setEndSize(back66->getContentSize().width*.2);
                back66->addChild(particle);
            });
            actions.pushBack(smoke);
            
            //壁に穴が開く
            auto hole = createSpriteToCenter("sup_66_hole.png", true, back66, true);
            std::string mouseName = "sup_66_mouse.png";
            auto mouse = createSpriteToCenter(mouseName, true, back66, true);
            actions.pushBack(createChangeAction(.1, .1, {bomb_66}, {hole, mouse}));
            actions.pushBack(DelayTime::create(.6));
            
            //ネズミびっくり
            actions.pushBack(attentionAction(back66, mouseName, Vec2(.493, .302), "sup_66_attention.png"));
            actions.pushBack(createSoundAction("squirre.mp3"));
            actions.pushBack(DelayTime::create(1));
            
            //back66を削除
            actions.pushBack(CallFunc::create([bomb_66](){
                auto par = bomb_66->getChildByName<ParticleSystemQuad*>("fire");
                if (par) {
                    par->setDuration(.1);
                }
            }));
            actions.pushBack(TargetedAction::create(back66, FadeOut::create(.5)));
            actions.pushBack(DelayTime::create(.7));
        }

#pragma mark じょうろ+池
        if (item == "pond") {
            auto pot = createAnchorSprite("sup_23_pot_0.png", .6, .5, true, parent, true);
            //沈める
            actions.pushBack(TargetedAction::create(pot, Sequence::create(FadeIn::create(1),
                                                                          DelayTime::create(.5),
                                                                          EaseOut::create(Spawn::create(MoveBy::create(1.0, Vec2(0,-parent->getContentSize().height*.1)),
                                                                                                        ScaleBy::create(1.0, .8),
                                                                                        NULL), 1.5),
                                                                          NULL)));
            
            //change
            auto pot_1=createSpriteToCenter("sup_23_pot_1.png", true, parent,true);
            actions.pushBack(createChangeAction(1, 1, pot, pot_1,"bubble.mp3"));
            actions.pushBack(DelayTime::create(.5));
            actions.pushBack(TargetedAction::create(pot_1, FadeOut::create(1)));
        }
#pragma mark じょうろ+花壇
        else if (item == "pot") {
            auto pot = createAnchorSprite("sup_17_pot.png", .5, .5, true, parent, true);
            pot->setLocalZOrder(1);
            //じょうろ
            actions.pushBack(TargetedAction::create(pot, Sequence::create(FadeIn::create(1),
                                                                          DelayTime::create(.5),
                                                                          MoveTo::create(1.0, Vec2(parent->getContentSize().width*.35,parent->getContentSize().height*.8)),
                                                                          EaseInOut::create(RotateBy::create(1.0, -60), 1.5),
                                                                          NULL)));
            
            //移動 bezier
            auto duration=3.0;
            ccBezierConfig config;
            config.controlPoint_1 = Vec2(parent->getContentSize().width*.9, parent->getContentSize().height*.72);
            config.controlPoint_2 = Vec2(parent->getContentSize().width*.9, parent->getContentSize().height*.55);
            config.endPosition = Vec2(parent->getContentSize().width*.35, parent->getContentSize().height*.55);
            
            //water
            auto water=CallFunc::create([pot,duration]{
                Common::playSE("jaa.mp3");
                
                BlendFunc func;
                func.dst=1;
                func.src=770;
                
                auto particle=ParticleSystemQuad::create("particle_shower.plist");
                particle->setTexture(Sprite::create("particle_fire.png")->getTexture());
                particle->setStartColor(Color4F(.5, .5, 1, 1));
                particle->setEndColor(Color4F(.5,.5,1,0));
                particle->setBlendFunc(func);
                particle->setAutoRemoveOnFinish(true);
                particle->setDuration(duration);
                particle->setPosition(pot->getContentSize().width*.34,pot->getContentSize().height*.53);
                particle->setSpeed(pot->getContentSize().width*.125);
                particle->setSpeedVar(particle->getSpeed()*.33);
                particle->setGravity(Vec2(0, -pot->getContentSize().height*.5));
                particle->setTotalParticles(100);
                particle->setAngle(180);
                particle->setAngleVar(10);
                particle->setLife(.75);
                particle->setLifeVar(.25);
                particle->setStartSize(pot->getContentSize().width*.04);
                particle->setStartSizeVar(pot->getContentSize().width*.02);
                particle->setEndSize(pot->getContentSize().width*.08);
                particle->setEndSizeVar(pot->getContentSize().width*.02);
                particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
                
                pot->addChild(particle);
            });
            actions.pushBack(water);
            actions.pushBack(DelayTime::create(.5));
            //fadeout pot
            actions.pushBack(TargetedAction::create(pot, Sequence::create(EaseInOut::create(BezierTo::create(duration, config), 1.5),
                                                                          Spawn::create(RotateBy::create(1.0, 60),
                                                                                        MoveBy::create(1.0, Vec2(0, parent->getContentSize().height*.1)),
                                                                                        FadeOut::create(1),
                                                                                        NULL),
                                                                          NULL)));

            //leaf
            auto clip=createClippingNodeToCenter("sup_17_clip.png", parent);
            for (int i=0; i<5; i++) {
                auto distance=parent->getContentSize().height*.05;
                if (i==4) {
                    distance=parent->getContentSize().height*.03;
                }
                auto leaf=createAnchorSprite(StringUtils::format("sup_17_leaf_%d.png",i), .5, .5, false, parent, false);
                leaf->setPositionY(leaf->getPositionY()-distance);
                clip->addChild(leaf);
                auto shade=createSpriteToCenter(StringUtils::format("sup_17_shade_%d.png",i), true, parent,true);
                
                actions.pushBack(Spawn::create(TargetedAction::create(leaf, EaseInOut::create(MoveBy::create(.3, Vec2(0, distance)), 1.5)),
                                               TargetedAction::create(shade, Sequence::create(DelayTime::create(.1),
                                                                                              FadeIn::create(.2),
                                                                                              NULL)),
                                               createSoundAction("pop_short.mp3"),
                                               NULL));
            }
        }
#pragma mark はさみ
        else if (item == "scissors") {
            auto clip=createClippingNodeToCenter("sup_5_clip.png", parent);
            auto scissors=createAnchorSprite("sup_5_scissors.png",.8,.525, true, parent,false);
            clip->addChild(scissors);
            
            auto close=parent->getChildByName("sup_5_close.png");
            auto tape=parent->getChildByName("sup_5_tape.png");

            auto distance=-parent->getContentSize().width*.47;
            actions.pushBack(TargetedAction::create(scissors, Sequence::create(FadeIn::create(1),
                                                                           DelayTime::create(1),
                                                                               createSoundAction("chokichoki.mp3"),
                                                                               Spawn::create(EaseInOut::create(MoveBy::create(1, Vec2(distance, 0)), 1.2),
                                                                                             Sequence::create(DelayTime::create(.3),
                                                                                                              TargetedAction::create(tape, FadeOut::create(.7)),
                                                                                                              NULL),
                                                                                             NULL),
                                                                               MoveBy::create(1, Vec2(0, parent->getContentSize().height*.05)),
                                                                               NULL)));
            
            //fadouts
            auto open=createSpriteToCenter("sup_5_open.png", true, parent,true);
            createSpriteToCenter("sup_5_caset.png", false, open,true);
            
            actions.pushBack(Spawn::create(TargetedAction::create(scissors, FadeOut::create(1)),
                                           createChangeAction(1, .7, close, open,"pop.mp3"),
                                           NULL));
            
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark きんぎょのえさ
        else if (item == "fish") {
            auto pack=createAnchorSprite("sup_12_pack.png", .5, .5, true, parent, true);
            pack->setPositionY(parent->getContentSize().height*.75);
            
            actions.pushBack(TargetedAction::create(pack, Sequence::create(FadeIn::create(1),
                                                                           createSoundAction("sa.mp3"),
                                                                           jumpAction(parent->getContentSize().height*.05, .2, pack, 1.2, 2),
                                                                           DelayTime::create(1),
                                                                           FadeOut::create(1),
                                                                           NULL)));
            
            actions.pushBack(DelayTime::create(.3));
            
            auto fish=parent->getChildByName("sup_12_fish.png")->getChildByName("sup_12_fish.png");
            auto pos=fish->getPosition();
            
            actions.pushBack(createSoundAction("bubble.mp3"));
            actions.pushBack(jumpAction(parent->getContentSize().height*.02, .3, fish, 1.5));
            //移動 bezier
            auto duration=2.0;
            ccBezierConfig config;
            config.controlPoint_1 = Vec2(parent->getContentSize().width*.75, parent->getContentSize().height*.35);
            config.controlPoint_2 = Vec2(parent->getContentSize().width*.75, parent->getContentSize().height*.5);
            config.endPosition = pos;
            auto bezier_0=BezierTo::create(duration, config);
            actions.pushBack(TargetedAction::create(fish, Spawn::create(bezier_0,
                                                                        Sequence::create(DelayTime::create(duration*.4),
                                                                                         ScaleBy::create(duration*.2, -1,1),
                                                                                         NULL),
                                                                        NULL)));
            ccBezierConfig config_1;
            config_1.controlPoint_1 = Vec2(parent->getContentSize().width*.25, parent->getContentSize().height*.35);
            config_1.controlPoint_2 = Vec2(parent->getContentSize().width*.25, parent->getContentSize().height*.5);
            config_1.endPosition = pos;
            auto bezier_1=BezierTo::create(duration, config_1);

            actions.pushBack(TargetedAction::create(fish, Spawn::create(bezier_1,
                                                                        Sequence::create(DelayTime::create(duration*.4),
                                                                                         ScaleBy::create(duration*.2, -1,1),
                                                                                         NULL),
                                                                        NULL)));
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(CallFunc::create([this,parent]{
                Common::createSoundAction("bubble.mp3");
                this->showBubbles(parent, 12, true);
            }));
            actions.pushBack(DelayTime::create(2));
        }
#pragma mark ぶた+だんご
        else if (item == "dango") {
            auto dango = createSpriteToCenter("sup_31_dumpling.png", true, parent, true);

            actions.pushBack(giveAction(dango, 1.0, .8, true));
            auto pig=parent->getChildByName("sup_31_pig.png");
            actions.pushBack(createSoundAction("pig.mp3"));
            actions.pushBack(createBounceAction(pig, .3, .1));

            //移動
            actions.pushBack(createSoundAction("run.mp3"));
            actions.pushBack(TargetedAction::create(pig, JumpBy::create(1.25, Vec2(parent->getContentSize().width*.5, 0), parent->getContentSize().height*.02, 6)));
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark カセット
        else if (item == "caset") {
            auto clip=createClippingNodeToCenter("sup_83_clip.png", parent);
            auto caset = createSpriteToCenter("sup_83_game.png", true, parent, false);
            caset->setPositionY(parent->getContentSize().height*.6);
            clip->addChild(caset);
            actions.pushBack(TargetedAction::create(caset, Sequence::create(FadeIn::create(1),
                                                                            DelayTime::create(.5),
                                                                            EaseOut::create(MoveTo::create(1.0, parent->getContentSize()/2), 1.5),
                                                                            createSoundAction("kacha.mp3"),
                                                                            NULL)));
            actions.pushBack(DelayTime::create(.7));
            
            //replace
            auto back=createSpriteToCenter("back_82.png", true, parent,true);
            actions.pushBack(TargetedAction::create(back, FadeIn::create(1)));
            auto screen=createSpriteToCenter("sup_82_title.png", true, back,true);
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(createSoundAction("robot_start.mp3"));
            actions.pushBack(TargetedAction::create(screen, FadeIn::create(1)));
            
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(TargetedAction::create(back, FadeOut::create(1)));
            actions.pushBack(DelayTime::create(1));

        }
#pragma mark はしごを置く
        else if (item == "ladders") {
            auto ladders = createSpriteToCenter("sup_56_ladders.png", true, parent, true);
            actions.pushBack(TargetedAction::create(ladders, FadeIn::create(1)));
            actions.pushBack(createSoundAction("goto.mp3"));
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark スティック
        else if (item == "stick") {
            auto stick = createSpriteToCenter("sup_59_stick.png", true, parent, true);
            actions.pushBack(giveAction(stick, 1, .8, true));
            actions.pushBack(DelayTime::create(.5));
            
            auto arm_0_0 = parent->getChildByName<Sprite*>("sup_59_hand_0_0.png");
            auto anchorMap=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Anchor);
            auto sprMap=anchorMap["sprites"].asValueVector().at(0).asValueMap();
            auto arm_0_1 = createAnchorSprite(sprMap["name"].asString(), sprMap["x"].asFloat(), sprMap["y"].asFloat(), true, parent, true);
            actions.pushBack(createChangeAction(.5, .3, arm_0_0, arm_0_1 ,"monkey.mp3"));
            actions.pushBack(DelayTime::create(.6));
            
            auto arm_1_1=parent->getChildByName("sup_59_hand_1_1.png");
            auto snare=parent->getChildByName("sup_59_snare.png");
            transformToAnchorSprite(parent, snare, Vec2(.5, .3));
            //beat
            auto distance=-parent->getContentSize().height*.02;
            auto angle=10;
            auto duration=.1;
            auto ease=1.5;
            auto snare_action=createBounceAction(snare, duration, .05);
            auto beat=Repeat::create(Sequence::create(Spawn::create(swingAction(angle, duration, arm_0_1, ease,true),
                                                                    jumpAction(distance, duration, arm_0_1, ease),
                                                                    Sequence::create(DelayTime::create(duration),
                                                                                     createSoundAction("snare.mp3"),
                                                                                     snare_action,
                                                                                     NULL),
                                                                    NULL),
                                                      Spawn::create(swingAction(-angle, duration, arm_1_1, ease,true),
                                                                    jumpAction(distance, duration, arm_1_1, ease),
                                                                    Sequence::create(DelayTime::create(duration),
                                                                                     createSoundAction("snare.mp3"),
                                                                                     snare_action->clone(),
                                                                                     NULL),
                                                                    NULL),
                                                      NULL), 5);
            
            //replace
            auto back=createSpriteToCenter("back_57.png", true, parent,true);
            createSpriteToCenter("sup_57_stick.png", false, back,true);
            auto pig_0=createSpriteToCenter("sup_57_pig_0.png", false, back,true);
            auto wake=createSpriteToCenter("sup_57_wake.png", true, pig_0, true);
            actions.pushBack(Spawn::create(beat,
                                           Sequence::create(DelayTime::create(3),
                                                            TargetedAction::create(back, FadeIn::create(1)),
                                                            NULL),
                                           NULL));
            actions.pushBack(DelayTime::create(.5));
            actions.pushBack(createSoundAction("surprise.mp3"));
            actions.pushBack(TargetedAction::create(wake, FadeIn::create(.1)));
            actions.pushBack(DelayTime::create(1));

            //replace
            auto back_51=createSpriteToCenter("back_51.png", true, parent,true);
            SupplementsManager::getInstance()->addSupplementToMain(back_51, 51);
            auto pig_ladder=createSpriteToCenter("sup_51_pig_0.png", true, back_51,true);
            auto pig_run=createAnchorSprite("sup_51_pig_1.png",.5,.25, true, back_51,true);
            actions.pushBack(TargetedAction::create(back_51, FadeIn::create(1)));
            actions.pushBack(TargetedAction::create(back, RemoveSelf::create()));
            actions.pushBack(DelayTime::create(1));

            actions.pushBack(createSoundAction("stair.mp3"));
            actions.pushBack(TargetedAction::create(pig_ladder, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(createChangeAction(1, .7, pig_ladder, pig_run, "stair.mp3"));
            actions.pushBack(DelayTime::create(1));
            
            actions.pushBack(TargetedAction::create(pig_run, Spawn::create(JumpBy::create(1.5, Vec2(-parent->getContentSize().width*.2, -parent->getContentSize().height*.3), parent->getContentSize().height*.02, 5),
                                                                           Sequence::create(DelayTime::create(1),
                                                                                            FadeOut::create(1),
                                                                                            NULL),
                                                                           ScaleBy::create(1.5, 2.0),
                                                                           createSoundAction("pig.mp3"),
                                                                           NULL)));
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(TargetedAction::create(back_51, FadeOut::create(1)));
        }
#pragma mark かぎ
        else if (item == "key") {
            actions.pushBack(keyAction(parent, DataManager::sharedManager()->getNowBack(),false));
        }
        
        //完了
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark - 布団叩き
    else if (key.compare(0,6,"beater")==0) {
        auto index=atoi(key.substr(6).c_str());
        
        actions.pushBack(hitFutonAction(parent, index, ShowType_Normal));
        
        actions.pushBack(call);
    }
#pragma mark だんごが落ちる
    else if(key == "fallDango"){
        auto clip=createClippingNodeToCenter("sup_21_clip.png", parent);
        auto dango=createAnchorSprite("sup_21_dango.png", .33, .5, false, parent, false);
        clip->addChild(dango);
        
        auto duration=1.0;
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createSoundAction("fall.mp3"));
        actions.pushBack(TargetedAction::create(dango, EaseIn::create(Spawn::create(ScaleBy::create(duration, .8),
                                                                                    MoveBy::create(duration, Vec2(0, -parent->getContentSize().height*.3)),
                                                                                    NULL), 1.5)));
        actions.pushBack(TargetedAction::create(dango, Sequence::create(jumpAction(parent->getContentSize().height*.01, .1, dango, 1.2, 1),
                                                                        DelayTime::create(1),
                                                                        FadeOut::create(1), NULL)));
        
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark コントローラ正解後
    else if (key == "controller") {
        //replace
        auto back=createSpriteToCenter("back_82.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 82,true);
        actions.pushBack(TargetedAction::create(back, FadeIn::create(1)));
        auto screen=createSpriteToCenter("sup_82_play.png", true, back,true);
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createSoundAction("robot_start.mp3"));
        actions.pushBack(TargetedAction::create(screen, FadeIn::create(1)));
        
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(TargetedAction::create(back, FadeOut::create(1)));
        actions.pushBack(DelayTime::create(1));
        
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark 屋台くじ正解後
    else if(key.substr(0, 6) == "answer"){
        
        auto index = atoi(key.substr(7).c_str());
        
        //back46に遷移
        auto back46 = createSpriteToCenter("back_46.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back46, 46, true);
        actions.pushBack(TargetedAction::create(back46, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.5));
        
        //
        auto robot_0 = back46->getChildByName<Sprite*>("sup_46_robot_0.png");
        transformToAnchorSprite(back46, robot_0, Vec2(.5, .367));
        robot_0->setPositionY(parent->getContentSize().height*.3);
        auto robot_1 = createSpriteToCenter(StringUtils::format("sup_46_robot_%d.png", index), true, back46, true);
        transformToAnchorSprite(back46, robot_1, robot_0->getAnchorPoint());
        robot_1->setPositionY(robot_0->getPositionY());
        createSpriteToCenter("sup_46_cover.png", false, back46, true);
        actions.pushBack(TargetedAction::create(back46, FadeIn::create(.6)));
        actions.pushBack(DelayTime::create(.7));
        
        //入れ替わり
        actions.pushBack(createBounceAction(robot_0, .3, .3));
        actions.pushBack(createChangeAction(.3, .3, robot_0, robot_1, "robot_move.mp3"));
        actions.pushBack(DelayTime::create(.6));
        
        //上がる
        actions.pushBack(TargetedAction::create(robot_1, EaseOut::create(MoveTo::create(.6, Vec2(robot_1->getPositionX(), parent->getContentSize().height*robot_1->getAnchorPoint().y)), 2)));
        actions.pushBack(DelayTime::create(1.2));
        
        actions.pushBack(call);
    }
#pragma mark お守り並び替え正解後
    else if(key == "complete_amulet"){
        auto pig_0 = parent->getChildByName<Sprite*>("sup_87_pig_0.png");
        auto pig_1 = createSpriteToCenter("sup_87_pig_1.png", true, parent, true);
        auto pig_2 = createSpriteToCenter("sup_87_pig_2.png", true, parent, true);
        
        actions.pushBack(createSoundAction("pig.mp3"));
        actions.pushBack(DelayTime::create(.6));
        actions.pushBack(createChangeAction(.4, .4, pig_0, pig_1, "pop_1.mp3"));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createChangeAction(.5, .5, pig_1, pig_2));
        actions.pushBack(DelayTime::create(.9));
        
        //エンディング
        actions.pushBack(CallFunc::create([parent]{
            auto story=StoryLayer::create("ending", []{
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        }));
    }
#pragma mark スワンボートで移動
    else if(key=="swanboat"){
        
        auto black = Sprite::create();
        black->setColor(Color3B::BLACK);
        black->setPosition(parent->getContentSize()/2);
        black->setOpacity(0);
        black->setTextureRect(Rect(0, 0, parent->getContentSize().width, parent->getContentSize().height));
        parent->addChild(black);
        
        actions.pushBack(TargetedAction::create(black, FadeIn::create(1)));
        actions.pushBack(createSoundAction("wave.mp3"));
        actions.pushBack(DelayTime::create(.5));
        
        actions.pushBack(call);
    }
#pragma mark - 指定無し
    else{
        actions.pushBack(call);
    }
    parent->runAction(Sequence::create(actions));
}

#pragma mark - ヒント機能のAnswer
void OmoideActionManager::showAnswerAction(std::string key, Node *parent, ShowType showType)
{
#pragma mark 布団叩き回答
    if (key=="futontataki") {
        
        int backNum = 21;
        SupplementsManager::getInstance()->addSupplementToMain(parent, backNum, true);
        
        auto answer = DataManager::sharedManager()->getBackData(backNum)["useInput"].asValueMap()["answer"].asValueMap()["answer"].asString();
        
        log("回答です%s",answer.c_str());
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(1));
        for (int i=0; i<answer.size(); i++) {
            
            auto index=atoi(answer.substr(i,1).c_str());
            
            actions.pushBack(hitFutonAction(parent, index, showType));
            actions.pushBack(DelayTime::create(.3));
        }
        actions.pushBack(DelayTime::create(1));
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
}

#pragma mark - touch began
void OmoideActionManager::touchBeganAction(std::string key, Node *parent)
{
}


#pragma mark - Drag
void OmoideActionManager::dragAction(std::string key, Node *parent, Vec2 pos)
{
    
}

#pragma mark - Item
void OmoideActionManager::itemBackAction(std::string key, PopUpLayer *parent)
{
}

void OmoideActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造 addする際はcreateSpriteOnClipを使用するとよい
    Vector<FiniteTimeAction*>actions;
    log("アイテムアクション発動 : %s",key.c_str());
    actions.pushBack(useItemSoundAction());
#pragma mark スクラッチカードを削る
    if (key == "scratchcard") {
        auto  item = createSpriteToCenter("item_anim_coin.png", true, parent->itemImage, true);
        transformToAnchorSprite(parent->itemImage, item, Vec2(.504, .514));
        
        //fadein
        actions.pushBack(TargetedAction::create(item, FadeIn::create(1)));
        actions.pushBack(createSoundAction("pen.mp3"));
        
        //move
        std::vector<Vec2> movePoses = {Vec2(.008, .268), Vec2(.248, .55), Vec2(.248, .268), Vec2(.368, .55), Vec2(.368, .268), Vec2(.484, .55), Vec2(.484, .268), Vec2(.608, .55), Vec2(.608, .268),Vec2(.692, .55), Vec2(.692, .268),Vec2(.782, .55), Vec2(.782, .268),Vec2(.888, .55), Vec2(.888, .268)};
        
        Vector<FiniteTimeAction*> moves;
        int i = 0;
        for (auto movePos : movePoses) {
            auto move = MoveTo::create(i == 0? .7 : .3, Vec2(parent->itemImage->getContentSize().width*movePos.x, parent->itemImage->getContentSize().height*movePos.y));
            moves.pushBack(move);
            i++;
        }
        auto moveSeq = Sequence::create(moves);
        actions.pushBack(TargetedAction::create(item, moveSeq));
        actions.pushBack(DelayTime::create(.5));
        
        //上に行きながら消える
        auto fadeout_match=TargetedAction::create(item, Spawn::create(FadeOut::create(1),
                                                                      MoveBy::create(1, Vec2(0, parent->getContentSize().height*.05)), NULL));
        actions.pushBack(fadeout_match);
        actions.pushBack(DelayTime::create(.6));
        
        
        auto scratch_1 = createSpriteToCenter("scratchcard_1.png", true, parent->itemImage, true);
        actions.pushBack(TargetedAction::create(scratch_1, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.5));
    }
#pragma mark オルゴール
    else if (key == "musicbox") {
        for (int i=0; i<4; i++) {
            createSpriteToCenter(StringUtils::format("musicbox_screw_%d.png",i), true, parent->itemImage,true);
            if (i>0) {
                createSpriteToCenter(StringUtils::format("musicbox_stage_%d.png",i), true, parent->itemImage,true);
            }
        }
        
        //put
        actions.pushBack(TargetedAction::create(parent->itemImage->getChildByName("musicbox_screw_0.png"), Sequence::create(FadeIn::create(1),
                                                                                                                            createSoundAction("kacha.mp3"), NULL)));
        
        Vector<FiniteTimeAction*>screwActions;
        Vector<FiniteTimeAction*>stageActions;
        stageActions.pushBack(CallFunc::create([]{
            auto name=DataManager::sharedManager()->getBgmName(true);
            EscapeDataManager::getInstance()->preloadSound(name.c_str());
            EscapeDataManager::getInstance()->playSound(name.c_str(), true);
        }));
        for (int i=0; i<4; i++) {
            auto before=parent->itemImage->getChildByName(StringUtils::format("musicbox_screw_%d.png",i));
            auto after=parent->itemImage->getChildByName(StringUtils::format("musicbox_screw_%d.png",(i+1)%4));
            screwActions.pushBack(createChangeAction(1.0, .7, before, after,"gigigi.mp3"));
            
            auto before_stage=parent->itemImage->getChildByName(StringUtils::format("musicbox_stage_%d.png",i));
            auto after_stage=parent->itemImage->getChildByName(StringUtils::format("musicbox_stage_%d.png",(i+1)%4));
            stageActions.pushBack(createChangeAction(1.0, .7, before_stage, after_stage));
        }
        actions.pushBack(Sequence::create(screwActions));
        actions.pushBack(Sequence::create(stageActions));
        actions.pushBack(parent->fadeOutAll(1));
        actions.pushBack(CallFunc::create([parent]{
            auto story=StoryLayer::create("ending", []{
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            parent->addChild(story);
        }));
    }
#pragma mark 木箱open
    else if (key == "openBox") {
        auto close=createSpriteToCenter("box.png", true, parent->backImage,true);
        auto open=createSpriteToCenter("box_anim.png", true, parent->backImage,true);
        actions.pushBack(createChangeAction(1, .7, parent->itemImage, close));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createChangeAction(1, .7, close, open,"gacha.mp3"));
        actions.pushBack(DelayTime::create(1));
    }
    //call
    auto call=CallFunc::create([callback](){
        callback(true);
    });
    actions.pushBack(call);
    
    parent->runAction(Sequence::create(actions));
}

#pragma mark - Custom
void OmoideActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    
   
}

#pragma mark - Story
void OmoideActionManager::storyAction(std::string key,Node*parent,const onFinished& callback)
{
    if (key == "getMusicbox"){
        DataManager::sharedManager()->addNewItem(24);
    }
    
    callback(true);
    log("ストーリのカスタムアクションが呼ばれます[%s]",key.c_str());
};


#pragma mark - Camera Shot
void OmoideActionManager::cameraShotAction(int backID)
{
}

#pragma mark - Arrow Action
void OmoideActionManager::arrowAction(std::string key, Node *parent, const onFinished &callback)
{
    callback(true);
}

#pragma mark - Private
ParticleSystemQuad* OmoideActionManager::showFireParticle(cocos2d::Node *parent, int backID)
{
    auto clipName = "clip";
    auto fireName = "fire";
    auto clipNode = parent->getChildByName<ClippingNode*>(clipName);
    
    if (clipNode != NULL) {
        auto particle = clipNode->getChildByName<ParticleSystemQuad*>(fireName);
        if (particle != NULL) {//あるなら貼らずに返す。
            log("炎パーティクルあるから返すよ。");
            return particle;
        }
    }
    
    auto particle=ParticleSystemQuad::create("fire.plist");
    particle->setName(fireName);
    particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
    particle->setAutoRemoveOnFinish(true);
    particle->setPosition(parent->getContentSize().width*.5, parent->getContentSize().height*.55);
    particle->setTotalParticles(150);
    particle->setStartColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 150));
    particle->setPosVar(Vec2(parent->getContentSize().width*.075, parent->getContentSize().height*.05));
    particle->setStartSize(parent->getContentSize().width*.075);
    particle->setSpeed(parent->getContentSize().height*.08);
    
    parent->addChild(particle);
    
    return particle;
}

void OmoideActionManager::showSmokeParticle(cocos2d::Node *parent, int backID)
{
    if (!DataManager::sharedManager()->getEnableFlagWithFlagID(12))
        return;
    
    Vector<FiniteTimeAction*> actions;
    float smokeDuration = 1.5;
    
    std::vector<int> answer = {0, 1, 0, 0, 0, 1, 0, 1, 0};//答えは自由に決められるで.最後の答えは0にすること。
    
    std::vector<Vec2> poses = {Vec2(.445, .674), Vec2(.555, .674)};
    
    for (int num : answer) {
        auto showSmoke = CallFunc::create([parent, num, smokeDuration, poses](){
            auto particle=ParticleSystemQuad::create("smoke.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width*poses.at(num).x,parent->getContentSize().height*poses.at(num).y);
            particle->setStartSize(parent->getContentSize().width*.015);
            particle->setStartSizeVar(particle->getStartSize()*.05);
            particle->setEndSize(particle->getStartSize()*4);
            particle->setSpeed(parent->getContentSize().height*.05);
            //particle->setSpeedVar(particle->getSpeed()*.01);
            particle->setGravity(Vec2(0, particle->getSpeed()*3));
            particle->setLife(2.5);
            particle->setTotalParticles(200);
            particle->setPosVar(Vec2(parent->getContentSize().width*.005, parent->getContentSize().height*.02));
            //particle->setAngle(270);
            particle->setAngleVar(5);
            particle->setDuration(smokeDuration);
            parent->addChild(particle);
        });
        actions.pushBack(showSmoke);
        actions.pushBack(DelayTime::create(smokeDuration*1.5));
    }
    
    auto seq = Sequence::create(actions);
    
    parent->runAction(Repeat::create(Sequence::create(seq, DelayTime::create(smokeDuration*2), NULL), -1));
}

void OmoideActionManager::showBubbles(cocos2d::Node *parent, int backID, bool correct)
{
    std::vector<float>xs={.6,.4};
    std::vector<float>ys={.66,.61,.56,.51,.44,.39};

    for (int i=0;i<ys.size();i++) {
        auto name=StringUtils::format("bubble_%d",i);
        auto scale=.15;
        if (correct) {
            scale=bubble_scales.at(i);
        }
        auto bubble=parent->getChildByName(name);
        if (!bubble) {
            bubble=createSpriteToCenter("sup_12_bubble.png", false, parent,true);
            bubble->setName(name);
            bubble->setOpacity(150);
            bubble->setScale(parent->getContentSize().width*scale/bubble->getContentSize().width);
            bubble->setPosition(parent->getContentSize().width*xs.at(i%2), parent->getContentSize().height*ys.at(i));
        }

        bubble->stopAllActions();
        
        auto distance=parent->getContentSize().height*.01;
        bubble->runAction(Repeat::create(Sequence::create(Spawn::create(createBounceAction(bubble, 1.0, .1, parent->getContentSize().width*scale/bubble->getContentSize().width),
                                                                        Sequence::create(EaseInOut::create(MoveBy::create(1.25, Vec2(0, distance)), 1.5),
                                                                                         EaseInOut::create(MoveBy::create(1.25, Vec2(0, -distance)), 1.5),
                                                                                         NULL),
                                                                        NULL),
                                                          NULL), -1));
    }
}

FiniteTimeAction* OmoideActionManager::hitFutonAction(cocos2d::Node *parent, int index, ShowType type)
{
    Vector<FiniteTimeAction*> actions;
    auto cushion_0=parent->getChildByName(StringUtils::format("sup_21_cushion_%d_0.png",index));
    auto cushion_1=createSpriteToCenter(StringUtils::format("sup_21_cushion_%d_1.png",index), true, parent,true);
    auto beater = createAnchorSprite("sup_21_beater.png", .5, .37, true, parent, true);
    if (index==1) {
        beater->setPositionX(parent->getContentSize().width*.9);
    }
    
    auto scaleRatio=1.2;
    auto angle=10;
    beater->setScale(scaleRatio*beater->getScale());
    beater->setRotation(angle);
    
    //hit
    auto duration=.2;
    actions.pushBack(TargetedAction::create(beater, Sequence::create(FadeIn::create(.2),
                                                                     EaseIn::create(Spawn::create(ScaleBy::create(duration, 1.0/scaleRatio),
                                                                                                  RotateBy::create(duration, -angle),
                                                                                                  NULL), 1.5),
                                                                     createSoundAction("bohu.mp3", type), Spawn::create(Sequence::create(createChangeAction(0, 0, cushion_0, cushion_1),
                                                                                                    DelayTime::create(.1),
                                                                                                    createChangeAction(0, 0, cushion_1, cushion_0),
                                                                                                    NULL),
                                                                                   EaseOut::create(Spawn::create(ScaleBy::create(duration, scaleRatio),
                                                                                                                 RotateBy::create(duration, angle),
                                                                                                                 NULL), 1.5),
                                                                                   NULL),
                                                                     
                                                                     FadeOut::create(duration),
                                                                     //RemoveSelf::create(),//remove入れると、リピートが止まるので、コメントアウトします。
                                                                     NULL)));
    //actions.pushBack(TargetedAction::create(cushion_1, RemoveSelf::create()));//remove入れると、リピートが止まるので、コメントアウトします。
    
    return Sequence::create(actions);
}

Vec2 OmoideActionManager::getMovedSyogiPos(cocos2d::Node *parent, int index)
{
    std::vector<Vec2> moveUnits = {Vec2(1, 0), Vec2(-1, 0), Vec2(1, 1), Vec2(-1, -1), Vec2(0, -1), Vec2(0, 1)};
    auto moveUnit = moveUnits.at(index);
    
    auto komaAnchor = komaAnchors.at(index);
    
    auto vec = Vec2(parent->getContentSize().width*(komaAnchor.x+moveUnit.x*moveUnitVec.x), parent->getContentSize().height*(komaAnchor.y+moveUnit.y*moveUnitVec.y));
    
    return vec;
}
