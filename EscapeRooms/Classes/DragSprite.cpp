//
//  DragSprite.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2017/11/20.
//
//

#include "DragSprite.h"
#include "DataManager.h"

using namespace cocos2d;

DragSprite* DragSprite::create(ValueMap map, Value answerMapV, Size parentSize)
{
    auto node=new DragSprite();
    if(node&&node->init(map,answerMapV,parentSize)){
        node->autorelease();
        return node;
    }
    CC_SAFE_RELEASE(node);
    return node;
}

bool DragSprite::init(ValueMap map, Value answerMapV, Size parentSize)
{
    //すでに正しく繋いでいるかをによって分岐する
    std::string fileName="";
    auto answered=false;
    if (!answerMapV.isNull()) {
        answered=DataManager::sharedManager()->getEnableFlagWithFlagID(answerMapV.asValueMap()["enFlagID"].asFloat());
    }

    if (!map["end_filename"].isNull()&&answered) {
        fileName=map["end_filename"].asString();
    }
    else{
        answered=false;
        fileName=map["name"].asString();
    }
    
    if(!EscapeStageSprite::init(fileName))
    {
        return false;
    }
    
    if (answered) {
        setPosition(parentSize/2);
    }
    else{
        setPosition(parentSize.width*map["x"].asFloat(),parentSize.height*map["y"].asFloat());
        setDefaultPosition(getPosition());
        setEndRect(Rect(parentSize.width*map["end_x"].asFloat(), parentSize.height*map["end_y"].asFloat(), parentSize.width*map["end_width"].asFloat(), parentSize.height*map["end_height"].asFloat()));
        setBorderRight(parentSize.width);
        setBorderLeft(0);
        setBorderTop(parentSize.height);
        setBorderBottom(0);
        setEndFileName(map["end_filename"].asString());
        setSelectedDistance(Vec2(0, 0));
        if(!map["border_right"].isNull())
        {
            setBorderRight(map["border_right"].asFloat()*parentSize.width);
        }
        if(!map["border_left"].isNull())
        {
            setBorderLeft(map["border_left"].asFloat()*parentSize.width);
        }
        if(!map["border_top"].isNull())
        {
            setBorderTop(map["border_top"].asFloat()*parentSize.height);
        }
        if(!map["border_bottom"].isNull())
        {
            setBorderBottom(map["border_bottom"].asFloat()*parentSize.height);
        }
        if(!map["selected_distance"].isNull())
        {
            auto dis_map=map["selected_distance"].asValueMap();
            setSelectedDistance(Vec2(parentSize.width*dis_map["x"].asFloat(), parentSize.height*dis_map["y"].asFloat()));
        }
    }
    
    
    return true;
}

void DragSprite::drag(cocos2d::Vec2 pos,Vec2 touchStartPos, bool onItem, bool isInBegan)
{
    stopAllActions();
    
    if (isInBegan) {
        runAction(Sequence::create(MoveBy::create(.1, getSelectedDistance()), NULL));
    }
    else{
        auto x=getDefaultPosition().x+(pos.x-touchStartPos.x)/getScale()+getSelectedDistance().x;//初期位置+変位
        auto y=getDefaultPosition().y+(pos.y-touchStartPos.y)/getScale()+getSelectedDistance().y;//初期位置+変位

        //right check
        x=MIN(x, getBorderRight());
        //left check
        x=MAX(x, getBorderLeft());
        //top check
        y=MIN(y, getBorderTop());
        //bottom check
        y=MAX(y, getBorderBottom());
        
        setPosition(x, y);
    }
}
