//
//  SwitchSprite.h
//  BalentienRoom
//
//  Created by 成田凌平 on 2017/02/10.
//
//

#ifndef __BalentienRoom__SwitchSprite__
#define __BalentienRoom__SwitchSprite__

#include "cocos2d.h"
#include "EscapeStageSprite.h"
#include "ShowType.h"

USING_NS_CC;
class SwitchSprite:public EscapeStageSprite
{
public:
    static SwitchSprite* create(ValueMap map,Value answerMapV,std::string name,int number,std::string temporaryFlagKey);
    static SwitchSprite* create(ValueMap map,Value answerMapV,std::string name,int number,std::string temporaryFlagKey,ShowType type);

    bool init(ValueMap map ,Value answerMapV,std::string name,int number,std::string temporaryFlagKey,ShowType type);
    
    /**indexを一つ進め、タッチ可能判定、セーブ処理を行う*/
    void switching(ccMenuCallback callback);
    
    /**indexを進め、タッチ可能判定、セーブ処理を行う*/
    void switching(ccMenuCallback callback,int index);

    /**indexを一つ進め、タッチ可能判定、セーブ処理を行う*/
    void switching(ccMenuCallback callback,int index,bool sound);
    
    /**画像のみ差し替える*/
    void changeTexture(ccMenuCallback callback,int index);
    /**キーを自動生成して保存*/
    std::string save();
    /**キーを自動生成して保存*/
    std::string save(ShowType type);
    /**キーを完全指定して保存*/
    std::string save(std::string key);

private:
    CC_SYNTHESIZE(int, minIndex, MinIndex);//最初のインデックス
    CC_SYNTHESIZE(int, index, Index);//表示中の画像インデックス _%d部分
    CC_SYNTHESIZE(std::string,fileName , FileName);//画像ファイル名 %s_%d.pngの%s部分
    CC_SYNTHESIZE(int, maxCount, MaxCount);
    CC_SYNTHESIZE(bool, defaultEmpty, DefaultEmpty);//表示インデックス0のとき画像表示しない
    CC_SYNTHESIZE(bool, memory, Memory);//インデックスの変更をプレイ中に保持するか
    CC_SYNTHESIZE(std::string, playSE, PlaySE);//タッチ時の効果音
    CC_SYNTHESIZE(std::string, CustomFlagKey, CustumFlagKey);//フラグを読み込むキーを指定。(Optional)
    CC_SYNTHESIZE(bool, enabled, Enabled);//falseのとき非表示+answer処理をしない
    
    ValueMap animateMap;
    ValueMap supplementsMap;

    std::string getTemporaryKey(ShowType type);
    std::string getTemporaryKey(std::string name,ShowType type);

    void addSupplements();
};

#endif /* defined(__BalentienRoom__SwitchSprite__) */
