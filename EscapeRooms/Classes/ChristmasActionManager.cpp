//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "ChristmasActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "GameScene.h"

using namespace cocos2d;

ChristmasActionManager* ChristmasActionManager::manager =NULL;

ChristmasActionManager* ChristmasActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new ChristmasActionManager();
    }
    return manager;
}

#pragma mark - オープニング
void ChristmasActionManager::openingAction(cocos2d::Node *node, const onFinished &callback)
{
    auto scene=(GameScene*)node;
    
    auto storyLayer = StoryLayer::create("opening1", [scene,callback,this](Ref* sen){
        auto back=createSpriteToCenter("back_4.png", true, scene->mainSprite);
        scene->mainSprite->addChild(back);
        
        auto layer=SupplementsManager::getInstance()->createDisableLayer();
        scene->addChild(layer);
        
        auto sup=createSpriteToCenter("sup_4_letters.png",true, scene->mainSprite);
        back->addChild(sup);
        
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        auto fadein_sup=TargetedAction::create(sup, FadeIn::create(1));
        auto call=CallFunc::create([scene,callback,layer,this,back]{
            auto story = StoryLayer::create("opening2", [scene,callback,layer,this,back](Ref* sen){
                
                //back4を消す
                auto fadeout = TargetedAction::create(back, FadeOut::create(1));
                auto remove = TargetedAction::create(back, RemoveSelf::create());
                auto back4Action = Sequence::create(fadeout, remove, NULL);
                
                //back61を表示
                auto back61=createSpriteToCenter("back_61.png", true, scene->mainSprite);
                scene->mainSprite->addChild(back61);
                auto sup=createSpriteToCenter("sup_61_letters.png",true, scene->mainSprite);
                back61->addChild(sup);
                auto fadein = TargetedAction::create(back61, FadeIn::create(1));
                auto fadein_sup = TargetedAction::create(sup, FadeIn::create(1));
                auto fadeinSpawn = Spawn::create(fadein, fadein_sup, NULL);

                //back4とback61を同時アクション
                auto backSpawn = Spawn::create(back4Action, fadeinSpawn, NULL);
                
                //ストーリーレイヤーを表示
                auto callStory = CallFunc::create([scene,callback,layer,this,back61,sup](){
                    auto story = StoryLayer::create("opening3", [scene,callback,layer,this,back61,sup](){
                        
                        auto fadeout = TargetedAction::create(sup, FadeOut::create(1));
                        auto call = CallFunc::create([scene,layer,back61,callback](){
                            
                            auto story = StoryLayer::create("opening4", [scene,layer,back61,callback](){
                                //back61を消す
                                auto fadeout = TargetedAction::create(back61, FadeOut::create(1));
                                auto remove = TargetedAction::create(back61, RemoveSelf::create());
                                auto call = CallFunc::create([layer,callback](){
                                    layer->removeFromParent();
                                    callback(true);
                                });
                                auto back61Action = Sequence::create(fadeout, remove, call, NULL);
                                
                                scene->runAction(back61Action);
                            });
                           
                            scene->addChild(story);
                        });
                        scene->runAction(Sequence::create(fadeout, call, NULL));
                        
                    });
                    scene->addChild(story);
                });
                
                scene->runAction(Sequence::create(backSpawn, callStory, NULL));
            });
            scene->addChild(story);
        });
        
        scene->runAction(Sequence::create(Spawn::create(fadein,fadein_sup, NULL),call, NULL));
    });
    scene->addChild(storyLayer);
}

#pragma mark - Back
void ChristmasActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="horce")
    {//木馬
        auto vec=Vec2(.5, .227);
        auto angle=-10;
        auto horce=createAnchorSprite(StringUtils::format("sup_%d_horce.png",backID), vec.x, vec.y, false, parent, true);
        horce->setLocalZOrder(1);
        if (backID==10) {
            angle=-5;
            vec=Vec2(.5, 34.0/660);
        }
        
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(14)) {
            createSpriteToCenter(StringUtils::format("sup_%d_doll.png",backID), false, horce,true);
            
            std::vector<int>indexs={0,1,0,1,0,0,1,1};
            Vector<FiniteTimeAction*>actions;
            actions.pushBack(DelayTime::create(1));
            for (auto index : indexs) {
                actions.pushBack(createSoundAction("olddoor_1.mp3"));
                
                auto swing=swingAction(angle*pow(-1, index), .6, horce, 1.3);
                if (backID==10) {
                    actions.pushBack(Spawn::create(skewAction(horce, .6*2/2.5, -angle*.8*pow(-1, index), 1),
                                                   swing, NULL));
                }
                else{
                    actions.pushBack(swing);
                }
            }
            actions.pushBack(DelayTime::create(1));
            parent->runAction(Repeat::create(Sequence::create(actions), -1));
        }
    }
    else if(key=="board"&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(15))
    {//
        pictureAction(backID, parent, false);
    }
    else if(key=="snow")
    {
        if (backID==73&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(11)) {
            return;
        }
        fallSnow(parent, backID);
    }
    else if(key=="deer")
    {
        auto deer=createAnchorSprite("sup_94_deer.png", .5, 0, false, parent, true);
        auto hone=createSpriteToCenter("sup_94_hone.png", false, deer,true);
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            hone->setOpacity(0);
        }
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(44)) {
            createAnchorSprite("sup_94_bell_0.png", .416, 230.0/660, false, deer, true);
        }
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(45)) {
            auto bell_0=deer->getChildByName("sup_94_bell_0.png");
            auto bell_1=createAnchorSprite("sup_94_bell_1.png", .58, 230.0/660, false, deer, true);
            
            auto scale_original=deer->getScale();
            auto duration=.3;
            auto bounce=TargetedAction::create(deer, Sequence::create(EaseInOut::create(ScaleTo::create(duration, scale_original*1.2,scale_original*1.1), 1.25),
                                                                      EaseInOut::create(ScaleTo::create(duration, scale_original*1.075,scale_original*1.15), 1.25),
                                                                      ScaleTo::create(duration, scale_original), NULL));
            
            Vector<FiniteTimeAction*>actions;
            actions.pushBack(DelayTime::create(1));
            std::vector<int>indexs={1,0,1,1,0,0,1,0};
            for (int index : indexs) {
                FiniteTimeAction*action;
                if (index==0) {
                    action=swingAction(10, duration, bell_0, 1.3, true);
                }
                else{
                    action=swingAction(10, duration, bell_1, 1.3, true);
                }
                actions.pushBack(Spawn::create(action,bounce->clone(),createSoundAction("handbell.mp3"), NULL));
                actions.pushBack(DelayTime::create(.3));
            }
            
            actions.pushBack(DelayTime::create(1));
            
            parent->runAction(Repeat::create(Sequence::create(actions), UINT_MAX));
        }
    }
    else if(key=="piano"&&DataManager::sharedManager()->getEnableFlagWithFlagID(6)){
        Vector<FiniteTimeAction*>actions;
        std::vector<int>counts={2,5,3};
        std::vector<Vec2>vecs={Vec2(.333, .5),Vec2(.5, .5),Vec2(.666, .5)};
        for (int i=0;i<3;i++) {
            auto vec=vecs.at(i);
            auto music=CallFunc::create([this,parent,i,vec]{
                auto pos=Vec2(parent->getContentSize().width*vec.x, parent->getContentSize().height*vec.y);
                auto size=parent->getContentSize().width*.15;
                std::vector<Color4F> startColors={Color4F(1, .6, .6, 1),Color4F(.46, .95, .46, 1),Color4F::YELLOW};
                
                auto particle=ParticleSystemQuad::create("particle_music.plist");
                particle->setTexture(Sprite::create(StringUtils::format("particle_music_%d.png",i+2))->getTexture());
                particle->setAutoRemoveOnFinish(true);
                particle->setPosition(pos);
                particle->setLife(3);
                particle->resetSystem();
                particle->setStartSize(size);
                particle->setEndSize(size);
                particle->setStartColor(startColors.at(i));
                particle->setEndColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 0));
                particle->setAngle(120-30*i);
                particle->setSpeed(size*2);
                
                if (i==0) {
                    Common::playSE("si.mp3");
                    particle->setGravity(Vec2(size*.3, -size*1));
                }
                else if (i==1){
                    Common::playSE("fa.mp3");
                    particle->setGravity(Vec2(size*0, -size*1));
                }
                else{
                    Common::playSE("mi_1.mp3");
                    particle->setGravity(Vec2(-size*.3, -size*1));
                }
                
                parent->addChild(particle);
            });
            
            auto seq=Repeat::create(Sequence::create(music,DelayTime::create(2), NULL), counts.at(i));

            actions.pushBack(seq);
        }
        
        auto spawn=Spawn::create(actions);
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),spawn,DelayTime::create(1), NULL), -1));
    }
    else if(!DataManager::sharedManager()->isPlayingMinigame()&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(48)&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(33)&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(25)&&key=="clear")
    {
        auto story=StoryLayer::create("ending1", []{
            
            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

            auto story = StoryLayer::create("ending2", [](){
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
}

#pragma mark - Touch
void ChristmasActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="back_32"){
        //回転解凍後
        Vector<FiniteTimeAction*>actions;
        
        auto back_30=createSpriteToCenter("back_30.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back_30, 30,true);

        auto door=createSpriteToCenter("sup_30_0_1.png", true, back_30, true);
        createSpriteToCenter("sup_30_match.png", false,door, true);
        
        auto fadein_back_30=TargetedAction::create(back_30, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        actions.pushBack(fadein_back_30);
        actions.pushBack(delay);
        auto fadein_door=TargetedAction::create(door, Spawn::create(FadeIn::create(.5),
                                                                    createSoundAction("su.mp3"), NULL));
        actions.pushBack(fadein_door);
        actions.pushBack(delay->clone());
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if (key=="back_31") {
        Vector<FiniteTimeAction*>actions;
        
        auto back_30=createSpriteToCenter("back_30.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back_30, 30,true);
        auto door=createSpriteToCenter("sup_30_1_1.png", true, back_30, true);
        createSpriteToCenter("sup_30_radder.png", false,door, true);
        
        auto fadein_back_30=TargetedAction::create(back_30, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        actions.pushBack(fadein_back_30);
        actions.pushBack(delay);
        auto fadein_door=TargetedAction::create(door, createChangeAction(1, .5, SupplementsManager::getInstance()->switchSprites.at(1), door,"gacha.mp3"));
        actions.pushBack(fadein_door);
        actions.pushBack(delay->clone());
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="match")
    {//マッチアニメ
        Common::playSE("p.mp3");
        Vector<FiniteTimeAction*>actions;
        
        auto match=createSpriteToCenter("sup_9_match.png", true, parent, true);
        
        auto fadein_item=TargetedAction::create(match, FadeIn::create(1));
        actions.pushBack(fadein_item);
        
        auto particles=DataManager::sharedManager()->getParticleVector(false, 9);
        
        std::vector<float> pos_xs={135,245,355,465,86,229,371,513};//火付けposx 長い時
        Vector<FiniteTimeAction*>fireActions;

        for (int i=0; i<8; i++) {
            auto pos_y=(i<4)?parent->getContentSize().height*.656:parent->getContentSize().height*.59;
            auto pos_x=parent->getContentSize().width*(pos_xs.at(i)/600);
            
            auto move=TargetedAction::create(match, EaseInOut::create(MoveTo::create(.7, Vec2(pos_x, pos_y)), 1.2));
            fireActions.pushBack(move);
            
            if (i==0) {
                match->setPosition(parent->getContentSize().width*.2366, parent->getContentSize().height*.578);
                auto shake=jumpAction(parent->getContentSize().height*.01, .1, match, 1.2, 2);
                fireActions.pushBack(shake);
            }
            
            fireActions.pushBack(createSoundAction("huo.mp3"));
            
            auto delay_short=DelayTime::create(.2);
            fireActions.pushBack(delay_short);
            
            auto fire=CallFunc::create([parent,particles,pos_x,pos_y,i,match]{
                auto particleMap=particles.at(i).asValueMap();
                auto particle=SupplementsManager::getInstance()->getParticle(parent, particleMap);
                particle->setName(StringUtils::format("fire_%d",i));
                particle->setPosition(pos_x,pos_y);
                parent->addChild(particle);
                
                if (i==0) {
                    auto particle_match=SupplementsManager::getInstance()->getParticle(parent, particleMap);
                    particle_match->setPositionType(ParticleSystem::PositionType::FREE);
                    particle_match->setPosition(match->getContentSize()/2);
                    particle_match->setName("fire");
                    match->addChild(particle_match);
                }
            });
            fireActions.pushBack(fire);
            fireActions.pushBack(DelayTime::create(.4));
        }
        
        actions.pushBack(Sequence::create(fireActions));
        
        auto fadeout_match=TargetedAction::create(match, Spawn::create(FadeOut::create(1),
                                                                       MoveBy::create(1, Vec2(0, parent->getContentSize().height*.05)),
                                                                       CallFunc::create([match]{
            ((ParticleSystemQuad*)(match->getChildByName("fire")))->setDuration(0);
        }), NULL));
        actions.pushBack(fadeout_match);
        
        
        //change_candle
        auto candle_long=parent->getChildByName("sup_9_candles.png");
        
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);

        Vector<FiniteTimeAction*>changes;
        changes.pushBack(TargetedAction::create(candle_long, FadeOut::create(1)));
        for (int i=0; i<8; i++) {
            changes.pushBack(CallFunc::create([match,particles,parent,i]{
                auto fire=((ParticleSystemQuad*)parent->getChildByName(StringUtils::format("fire_%d",i)));
                if (fire) {
                    fire->setDuration(0.5);
                }
                
                auto particle=SupplementsManager::getInstance()->getParticle(parent, particles.at(i).asValueMap());
                particle->setName(StringUtils::format("fire_1_%d",i));
                parent->addChild(particle);
            }));
            
            auto candle_short=createSpriteToCenter(StringUtils::format("sup_9_candle_%d.png",i), true, parent, true);
            changes.pushBack(TargetedAction::create(candle_short, FadeIn::create(.7)));
        }
        changes.pushBack(createSoundAction("pop.mp3"));
        actions.pushBack(Spawn::create(changes));
        actions.pushBack(delay->clone());
        
        //remove
        std::vector<int>removeIndex={1,4,6};
        Vector<FiniteTimeAction*>removeActions;
        for (auto i:removeIndex) {
            auto candle=parent->getChildByName(StringUtils::format("sup_9_candle_%d.png",i));
            removeActions.pushBack(TargetedAction::create(candle, Sequence::create(FadeOut::create(1),RemoveSelf::create(), NULL)));
            removeActions.pushBack(CallFunc::create([parent,i]{
                auto fire=((ParticleSystemQuad*)(parent->getChildByName(StringUtils::format("fire_1_%d",i))));
                if (fire) {
                    fire->setDuration(.5);
                }
                
            }));
        }
        removeActions.pushBack(createSoundAction("pop.mp3"));
        actions.pushBack(Spawn::create(removeActions));
        actions.pushBack(delay->clone());

        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="back_43"||
            key=="back_45"||
            key=="back_7"||
            key=="back_63"||
            key=="back_12"||
            key=="back_44"){
        //棚正解後
        Vector<FiniteTimeAction*>actions;
        
        int backID=0;
        
        std::string back;
        std::string door;
        std::string sup_name;
        std::string soundName="gacha.mp3";
        if (key=="back_43") {
            back="back_41.png";
            backID=41;
            door="sup_41_2_1.png";
            sup_name="sup_41_handle.png";
        }
        else if (key=="back_44") {
            back="back_41.png";
            backID=41;
            door="sup_41_1_1.png";
            sup_name="sup_41_robo.png";
        }
        else if (key=="back_7") {
            back="back_6.png";
            backID=6;
            door="sup_6_1_1.png";
            sup_name="sup_6_stick.png";
            soundName="su.mp3";
        }
        else if (key=="back_12") {
            back="";
            backID=12;
            door="sup_12_open.png";
            sup_name="";
            soundName = "tissue.mp3";
        }
        else if(key=="back_63"){
            back="back_62.png";
            backID=62;
            door="sup_62_0_1.png";
            sup_name="sup_62_pen.png";
            soundName = "kacha.mp3";
        }
        else{
            back="back_41.png";
            backID=41;
            door="sup_41_0_1.png";
            sup_name="sup_41_ball.png";
        }
        
       
        auto open=openDoorAction(parent, back, backID, door, sup_name, soundName);
        actions.pushBack(open);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="back_80"){
        //地球儀棚解凍後
        Vector<FiniteTimeAction*>actions;
        
        auto back=createSpriteToCenter("back_79.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 79, true);
        auto door=createSpriteToCenter("sup_79_0_1.png", true, back, true);
        createSpriteToCenter("sup_79_scissors.png", false,door, true);
        
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        actions.pushBack(fadein_back);
        actions.pushBack(delay);
        auto fadein_door=TargetedAction::create(door, Spawn::create(FadeIn::create(.5),
                                                                    createSoundAction("su.mp3"), NULL));
        actions.pushBack(fadein_door);
        actions.pushBack(delay->clone());
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="scissors")
    {
        Common::playSE("p.mp3");
        auto item=createAnchorSprite("sup_47_scissors.png", .5, .5, true, parent, true);
        
        auto seq=TargetedAction::create(item, Sequence::create(FadeIn::create(1),
                                                               createSoundAction("chokichoki.mp3"),
                                                                DelayTime::create(2),
                                                                NULL));
        
        //back
        auto back=createSpriteToCenter("back_46.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 46, true);
        auto seq_back=TargetedAction::create(back, Spawn::create(FadeIn::create(.3),
                                                                 createSoundAction("sa.mp3"), NULL));
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq,seq_back,DelayTime::create(1),call, NULL));
    }
    else if(key=="board")
    {//絵画正解後
        Vector<FiniteTimeAction*>actions;
        
        createSpriteToCenter("sup_46_knob.png", false, parent,true);
        
        auto action=pictureAction(DataManager::sharedManager()->getNowBack(), parent,true);
        actions.pushBack(action);
        
        actions.pushBack(DelayTime::create(1));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="knob"){
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(TargetedAction::create(createSpriteToCenter("sup_60_knob.png", true, parent,true), FadeIn::create(1)));
        actions.pushBack(createSoundAction("kacha.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if (key=="handle"){
        //ピアノの
        Common::playSE("p.mp3");
        
        auto handle_0=createSpriteToCenter("sup_21_handle_0.png", true, parent,true);
        auto fadein_handle=TargetedAction::create(handle_0, Sequence::create(FadeIn::create(1),
                                                                         createSoundAction("kacha.mp3"), NULL));
        auto delay=DelayTime::create(1);
        
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<10; i++) {
            auto before=parent->getChildByName(StringUtils::format("sup_21_handle_%d.png",i%4));
            auto after_handle_name=StringUtils::format("sup_21_handle_%d.png",(i+1)%4);
            auto after=parent->getChildByName(after_handle_name);
            if (after==NULL) {
                after=createSpriteToCenter(after_handle_name, true, parent, true);
            }
            
            if (i%4==0) {
                actions.pushBack(createSoundAction("gigigi.mp3"));
            }
            actions.pushBack(createChangeAction(.7, .5, before, after));
        }
        auto handle_action=Sequence::create(actions);
        
        auto open=createSpriteToCenter("sup_21_open.png", true, parent,true);
        auto fadein_open=TargetedAction::create(open, Spawn::create(FadeIn::create(1),
                                                                       createSoundAction("gacha.mp3"), NULL));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_handle,delay,handle_action,delay->clone(),fadein_open,delay->clone(),call, NULL));
    }
    else if (key=="back_78"){
        //並び替え正解後
        auto back=createSpriteToCenter("back_81.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 81, true);
        
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,call, NULL));
    }
    else if(key=="radder"){
        //はしごセット
        Vector<FiniteTimeAction*>actions;
        auto delay=DelayTime::create(1);
        auto radder=createSpriteToCenter("sup_10_radder.png", true, parent,true);
        auto put=putAction(radder, "kacha.mp3", 1,0);
        actions.pushBack(put);
        actions.pushBack(delay);
        
        auto doll=parent->getChildByName("sup_10_doll_0.png");
        auto doll_anim=createSpriteToCenter("sup_10_doll_anim.png", true, parent, true);
        auto doll_horce=createSpriteToCenter("sup_10_doll.png", true, parent, true);
        doll_horce->setLocalZOrder(1);
        auto change=createChangeAction(1, .7, doll, doll_anim, "pop.mp3");
        actions.pushBack(change);
        actions.pushBack(delay->clone());
        auto change_1=createChangeAction(1, .7, doll_anim, doll_horce, "pop.mp3");
        actions.pushBack(change_1);
        actions.pushBack(delay->clone());

        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="driver"){
        Common::playSE("p.mp3");
        Vector<FiniteTimeAction*>actions;
        
        auto item=createAnchorSprite("sup_73_driver.png", .5, .5, true, parent, true);
        auto plate=parent->getChildByName("sup_73_plate.png");
        item->setPosition(parent->getContentSize().width*230.0/600,parent->getContentSize().height*132.0/660);
        auto fadein_item=TargetedAction::create(item, FadeIn::create(1));
        actions.pushBack(fadein_item);
        
        std::vector<Vec2>vecs={Vec2(230.0/600, 100.0/660),Vec2(368.0/600, 100.0/660)};
        for (int i=0; i<3; i++) {
            auto shake=Spawn::create(jumpAction(parent->getContentSize().height*.005, .1, item, 1.5, 3),
                                     createSoundAction("kacha_2.mp3"), NULL);
            actions.pushBack(shake);
            auto remove_screw=TargetedAction::create(parent->getChildByName(StringUtils::format("sup_73_screw_%d.png",i)), Sequence::create(FadeOut::create(.3),
                                                                                                                                            RemoveSelf::create(), NULL));
            actions.pushBack(remove_screw);
            auto delay=DelayTime::create(1);
            actions.pushBack(delay);
            
            if (i==2) {
                auto fadeout=TargetedAction::create(item, Spawn::create(MoveBy::create(1, Vec2(0, -parent->getContentSize().height*.1)),
                                                                        FadeOut::create(1), NULL));
                actions.pushBack(fadeout);
            }
            else{
                auto vec=vecs.at(i);
                auto move=TargetedAction::create(item, Spawn::create(MoveTo::create(.7, Vec2(parent->getContentSize().width*vec.x, parent->getContentSize().height*vec.y)), NULL));
                actions.pushBack(move);
            }
            actions.pushBack(DelayTime::create(.3));
        }

        actions.pushBack(DelayTime::create(1));
        
        auto change=createChangeAction(1, .7, plate, createSpriteToCenter("sup_73_mark.png", true, parent,true),"kacha.mp3");
        actions.pushBack(change);
        actions.pushBack(DelayTime::create(1));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="shake")
    {//shakeActionから呼び出し
        Common::playSE("bell_1.mp3");
        auto delay=DelayTime::create(1);
        auto fall=CallFunc::create([this,parent]{
            this->fallSnow(parent, DataManager::sharedManager()->getNowBack());
        });
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(delay,fall,DelayTime::create(3),call, NULL));
    }
    else if(key=="cloud"){
        Vector<FiniteTimeAction*>actions_spawn;
        
        auto num=DataManager::sharedManager()->getNowBack();
        //std::string soundName="pig_1.mp3";
        
        auto cloud_anchor=Vec2(250.0/600.0,538.0/660.0);
        if (num==92) {
            cloud_anchor=Vec2(.635,.72);
            actions_spawn.pushBack(createSoundAction("pig.mp3"));
        }
        
        auto cloud=cloudAction(parent, StringUtils::format("sup_%d_cloud.png",num), cloud_anchor);
        actions_spawn.pushBack(cloud);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(Spawn::create(actions_spawn),call, NULL));
    }
    else if(key=="stick"){
        Common::playSE("p.mp3");
        
        auto stick=createAnchorSprite("sup_29_stick.png", .5, .25, true, parent, true);
        auto original_pos_stick=stick->getPosition();
        auto original_scale_stick=stick->getScale();
        stick->setScale(original_scale_stick*1.5);
        stick->setPositionY(original_pos_stick.y-parent->getContentSize().height*.1);
        auto cover=createSpriteToCenter("sup_29_cover.png", true, parent,true);
        auto apple=createAnchorSprite("sup_29_apple.png", .5, .25, true, parent, true);
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(TargetedAction::create(stick, Sequence::create(FadeIn::create(1),
                                                                        EaseOut::create(Spawn::create(MoveTo::create(1, original_pos_stick),
                                                                                        ScaleTo::create(1, original_scale_stick),
                                                                                        NULL), 1.5),
                                                                        NULL)));
        actions.pushBack(TargetedAction::create(cover, FadeIn::create(1)));
        actions.pushBack(TargetedAction::create(stick, Sequence::create(MoveBy::create(.5, Vec2(0, -parent->getContentSize().height*.025)),
                                                                        Spawn::create(jumpAction(parent->getContentSize().height*.01, .25, stick, 1.2,2),
                                                                                      createSoundAction("za.mp3"),
                                                                                      TargetedAction::create(apple, FadeIn::create(1)),
                                                                                      NULL)
                                                                        , NULL)));
        
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(Spawn::create(TargetedAction::create(stick, Spawn::create(MoveBy::create(1, Vec2(0, parent->getContentSize().height*.1)),
                                                                                   FadeOut::create(1),
                                                                                   NULL)),
                                       TargetedAction::create(apple, Spawn::create(Sequence::create(DelayTime::create(1),
                                                                                                    FadeOut::create(.5), NULL),
                                                                                   RotateBy::create(1.5, 270),
                                                                                   EaseIn::create(JumpBy::create(1.5, Vec2(0, -parent->getContentSize().height*.05), parent->getContentSize().height*.1, 1), 1.5),
                                                                                   ScaleBy::create(1.5, 2.5), NULL)),
                                       createSoundAction("pop.mp3"),
                                       NULL));
       
        actions.pushBack(DelayTime::create(1));
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="putpen"){
        Common::playSE("p.mp3");
        auto item=createSpriteToCenter("sup_77_wingpen.png", true, parent, true);
        auto delay=DelayTime::create(1);
        auto fadein_item=TargetedAction::create(item, Sequence::create(FadeIn::create(1),
                                                                      createSoundAction("kacha.mp3"), NULL));
        auto door=createSpriteToCenter("sup_77_open.png", true, parent,true);
        createSpriteToCenter("sup_77_key.png", false, door,true);
        auto fadein_door=TargetedAction::create(door, Spawn::create(createSoundAction("kacha.mp3"),
                                                                    FadeIn::create(1), NULL));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_item,delay,fadein_door,delay->clone(),call, NULL));
    }
    else if(key=="ball"){
        Common::playSE("p.mp3");
        
        auto ball=createSpriteToCenter("sup_12_ball_2_1.png", true, parent,true);
        auto fadein=TargetedAction::create(ball, Sequence::create(FadeIn::create(1),
                                                                  createSoundAction("kacha.mp3"), NULL));
        auto delay=DelayTime::create(1);
        
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<3; i++) {
            auto spr=createSpriteToCenter(StringUtils::format("sup_12_ball_%d_2.png",i), true, parent, true);
            auto before=parent->getChildByName(StringUtils::format("sup_12_ball_%d_1.png",i));
            actions.pushBack(createChangeAction(1, .7, before, spr));
        }
        actions.pushBack(createSoundAction("pop.mp3"));
        
        auto call=CallFunc::create([callback,parent]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein,delay,Spawn::create(actions),delay->clone(),call, NULL));
    }
    else if(key=="globe"){
        Common::playSE("p.mp3");
        auto globe=createSpriteToCenter("sup_79_globe.png", true, parent, true);
        auto open=createSpriteToCenter("sup_79_open.png", true, parent, true);
        auto ball=createSpriteToCenter("sup_79_soccer.png", false, open,true);
        auto fadein_globe=TargetedAction::create(globe, Sequence::create(FadeIn::create(1),
                                                                  createSoundAction("kacha.mp3"), NULL));
        auto delay=DelayTime::create(1);
        
        auto fadein_open=createChangeAction(1, .7, globe, open,"kapa.mp3");
        
        auto fadeout_ball=TargetedAction::create(ball, FadeOut::create(1));
        
        auto call=CallFunc::create([callback,parent]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_globe,delay,fadein_open,delay->clone(),fadeout_ball,delay->clone(),call, NULL));
    }
    else if(key=="give")
    {//present を箱に
        Common::playSE("p.mp3");
        typedef enum {
            ItemType_Soccer=0,
            ItemType_Bear,
            ItemType_Robot
        }ItemType;
        
        auto index=ItemType_Soccer;
        if (DataManager::sharedManager()->getSelectedItemID()==9) {
            index=ItemType_Bear;
        }
        else if (DataManager::sharedManager()->getSelectedItemID()==19){
            index=ItemType_Robot;
        }
        
        Node* item;
        if (index==ItemType_Bear) {
            item=createSpriteToCenter("sup_91_bear.png", true, parent, true);
        }
        else if(index==ItemType_Robot){
            item=createSpriteToCenter("sup_91_robo.png", true, parent, true);
        }
        else{
            item=createSpriteToCenter("sup_91_soccer.png", true, parent, true);
        }
        
        auto fadein_item=TargetedAction::create(item, Sequence::create(FadeIn::create(1),
                                                                         createSoundAction("koto.mp3"), NULL));
        auto delay=DelayTime::create(1);

        auto call=CallFunc::create([callback,parent]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_item,delay,call, NULL));
    }
    else if(key=="apple")
    {//りんごをあげる
        Common::playSE("p.mp3");
        
        auto item=createAnchorSprite("sup_92_apple.png", 350.0/600, .6, true, parent, true);
        auto pig_0=parent->getChildByName("sup_92_pig_0.png");
        auto item_1=createAnchorSprite("sup_92_apple_1.png", 350.0/600, .6, true, parent, true);

        auto pig_1=createSpriteToCenter("sup_92_pig_1.png", true, parent, true);
        auto pig_2=createSpriteToCenter("sup_92_pig_2.png", true, parent, true);
        auto pig_3=createSpriteToCenter("sup_92_pig_3.png", true, parent, true);


        auto seq_item=giveAction(item, 1, .8, true);
        auto eat=TargetedAction::create(item_1, Spawn::create(FadeIn::create(1),
                                                              createSoundAction("paku.mp3"), NULL));
        auto delay=DelayTime::create(1);
        auto change_0=Spawn::create(createChangeAction(1, .7, pig_0, pig_1,"pop.mp3"),
                                    TargetedAction::create(item_1, FadeOut::create(1)), NULL);
        auto change_1=createChangeAction(1, .7, pig_1, pig_2,"pop.mp3");
        auto change_2=createChangeAction(1, .7, pig_2, pig_3,"pig.mp3");

        auto call=CallFunc::create([callback,parent]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(seq_item,eat,delay,change_0,delay->clone(),change_1,delay->clone(),change_2,delay->clone(),
                                           call, NULL));
    }
    else if(key.compare(0,4,"bell")==0)
    {
        Common::playSE("p.mp3");
        
        auto index=atoi(key.substr(5,1).c_str());
        auto bell=createSpriteToCenter(StringUtils::format("sup_94_bell_%d.png",index), true, parent,true);
        auto fadein_bell=TargetedAction::create(bell, FadeIn::create(1));
        auto delay=Spawn::create(createSoundAction("kacha.mp3"),
                                 DelayTime::create(1), NULL);
        auto call=CallFunc::create([callback,parent]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_bell,delay,call, NULL));
    }
    else if(key.compare(0,6,"monkey")==0)
    {
        
        auto index=atoi(key.substr(7,1).c_str());
        Common::playSE("handbell.mp3");
        auto action=monkeyAction(index, parent);

        auto call=CallFunc::create([callback,parent]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(action,call, NULL));
    }
    else if(key=="back_6")
    {
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            auto animateType=DataManager::sharedManager()->getTemporaryFlag("mistake_6").asInt();
            auto cover=parent->getChildByName("sup_6_cover.png");
            auto mistake=parent->getChildByName("sup_6_mistake.png");

            if (animateType>0) {
                //animationを中断
                mistake->stopAllActions();
                cover->setOpacity(0);
                mistake->setOpacity(0);
                mistake->setPositionY(parent->getContentSize().height*.5);
                
                DataManager::sharedManager()->setTemporaryFlag("mistake_6", 0);
            }
            else{
                DataManager::sharedManager()->setTemporaryFlag("mistake_6", 1);

                mistake->setPositionY(parent->getContentSize().height*.4);
                mistake->setOpacity(255);
                if (!cover) {
                    cover=createSpriteToCenter("sup_6_cover.png", false, parent, true);
                    cover->setLocalZOrder(1);
                }
                else{
                    cover->setOpacity(255);
                }
                
                auto seq=TargetedAction::create(mistake, Sequence::create(DelayTime::create(1),
                                                                          MoveTo::create(1, parent->getContentSize()/2),
                                                                          createSoundAction("nyu.mp3"),
                                                                          CallFunc::create([this]{
                    DataManager::sharedManager()->setTemporaryFlag("mistake_6", 2);
                }), NULL));
                
                mistake->runAction(seq);
            }
        }
        
        callback(true);
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void ChristmasActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造
    if (key=="key") {
        Common::playSE("p.mp3");
        
        auto key_0=createSpriteOnClip("diary_up_key_0.png", true, parent, true);
        auto key_1=createSpriteOnClip("diary_up_key_1.png", true, parent, true);

        auto fadein_key=TargetedAction::create(key_0, Sequence::create(FadeIn::create(1),
                                                                       createSoundAction("kacha.mp3"), NULL));
        auto delay=DelayTime::create(1);
        auto change=createChangeAction(1, .7, key_0, key_1,"key_1.mp3");
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_key,delay,change,delay->clone(),call, NULL));
    }
}

#pragma mark - Custom
void ChristmasActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    
}

#pragma mark - Show Answer
void ChristmasActionManager::showAnswerAction(std::string key, cocos2d::Node *parent)
{
    if (key=="monkey") {
        auto answerMap=DataManager::sharedManager()->getBackData(44)["useInput"].asValueMap()["answer"].asValueMap();
        auto answer=answerMap["answer"].asString();
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(.3));
        for (int i=0; i<answer.size(); i++) {
            actions.pushBack(monkeyAction(atoi(answer.substr(i,1).c_str()), parent));
            actions.pushBack(DelayTime::create(.2));
        }
        actions.pushBack(DelayTime::create(1));
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
}

#pragma mark - Private
FiniteTimeAction* ChristmasActionManager::pictureAction(int backID, cocos2d::Node *parent, bool animate)
{
    Node* picture;
    if (DataManager::sharedManager()->isPlayingMinigame()) {
        picture=parent->getChildByName(StringUtils::format("sup_%d_mistake.png",backID));
    }
    else{
        picture=parent->getChildByName(StringUtils::format("sup_%d_board.png",backID));
    }
    
    auto vec=Vec2(.5, .8);
    if (backID==3) {
        vec=Vec2(.5, .55);
    }
    auto pos=Vec2(parent->getContentSize().width*vec.x, parent->getContentSize().height*vec.y);
    
    
    if (animate) {
        return TargetedAction::create(picture, Spawn::create(EaseInOut::create(MoveTo::create(1, pos), 1.5),
                                                             createSoundAction("su.mp3"), NULL));
    }
    else{
        picture->setPosition(pos);
        return NULL;
    }
}

FiniteTimeAction* ChristmasActionManager::monkeyAction(int index, Node *parent)
{
    return Spawn::create(swingAction(-25*pow(-1, index), .4, parent->getChildByName(StringUtils::format("sup_44_monkey_hand_%d.png",index)), 1.1, false),
                         createSoundAction("handbell.mp3"), NULL);
}

void ChristmasActionManager::fallSnow(cocos2d::Node *parent, int backID)
{
    auto clipImageName = (backID == 72)? "sup_72_snow_clip.png":"sup_snow_clip.png";
    auto stencil=createSpriteToCenter(clipImageName, false, parent);
    auto clip=ClippingNode::create(stencil);
    clip->setInverted(false);
    clip->setAlphaThreshold(0.0f);
    
    std::vector<Color4F>colors={Color4F(0, 150.0/255, 162.0/255, 1),//Color4F(0, 90.0/255, 102.0/255, 1),
        Color4F::WHITE,
        Color4F::YELLOW,
        Color4F(255.0/255, 90.0/255, 60.0/255, 1)//Color4F(184.0/255, 46.0/255, 26.0/255, 1)
    };
    
    auto createParticle = [parent](ClippingNode* clip, Color4F color, Vec2 position)->ParticleSystemQuad*{
        auto particle=ParticleSystemQuad::create("particle_snow.plist");
        particle->setAutoRemoveOnFinish(true);
        particle->setPosition(position);
        particle->setPosVar(Vec2(parent->getContentSize().width*.4, parent->getContentSize().height*.0));
        particle->setTotalParticles(200);
        particle->setDuration(-1);
        particle->setStartSize(parent->getContentSize().width*.05);
        particle->setStartSizeVar(particle->getStartSize()*.2);
        particle->setEndSize(particle->getEndSize());
        particle->setStartColor(color);
        particle->setLife(3);
        particle->setSpeed(parent->getContentSize().height*.07);
        particle->setSpeedVar(particle->getSpeed()*.2);
        particle->setEndColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 0));
        clip->addChild(particle);
        
        return particle;
    };
    
    if (backID == 72) {//スノードーム4つ
        std::vector<float> posXRatios = {.3, .7};
        std::vector<float> posYRatios = {.66, .33};
        
        for (int i = 0; i < colors.size(); i++) {
            if (i == 0 && !DataManager::sharedManager()->getEnableFlagWithFlagID(11)) {
                //左上のやつでかつ、まだ降っていない。
            }
            else {
                auto color = colors.at(i);
                
                auto xIndex = i%2;
                auto yIndex = i/2;
                
                auto particle = createParticle(clip, color, Vec2(posXRatios.at(xIndex)*parent->getContentSize().width,posYRatios.at(yIndex)*parent->getContentSize().height));
                particle->setStartSize(particle->getStartSize()/3);
                particle->setStartSizeVar(particle->getStartSizeVar()/3);
                particle->setEndSize(particle->getEndSize()/3);
                particle->setPosVar(particle->getPosVar()/3);
                particle->setLife(particle->getLife()/1.5);
                particle->setSpeed(particle->getSpeed()/3);
                particle->setSpeedVar(particle->getSpeed()*.2);
            }
        }
        
    }
    else {
        auto index=0;
        if (backID==73) {
            index=0;
        }
        else if (backID==74){
            index=1;
        }
        else if (backID==75){
            index=2;
        }
        else if (backID==76){
            index=3;
        }
        
        createParticle(clip, colors.at(index), Vec2(parent->getContentSize().width*.5,parent->getContentSize().height*.825));
    }
    
    parent->addChild(clip);
}
