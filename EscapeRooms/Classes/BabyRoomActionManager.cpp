//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "BabyRoomActionManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "GameScene.h"
#include "SupplementsManager.h"

using namespace cocos2d;

BabyRoomActionManager* BabyRoomActionManager::manager =NULL;

BabyRoomActionManager* BabyRoomActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new BabyRoomActionManager();
    }
    return manager;
}

std::vector<float>pandas={.0225,.03,.02,.045,0.005};

#pragma mark - 万能
void BabyRoomActionManager::openingAction(cocos2d::Node *node, const onFinished &callback)
{
    auto scene=(GameScene*)node;
    
    auto storyLayer = StoryLayer::create("opening1", [scene,callback,this](Ref* sen){
        auto back=createSpriteToCenter("back_5.png", true, scene->mainSprite);
        scene->mainSprite->addChild(back);
        
        auto layer=SupplementsManager::getInstance()->createDisableLayer();
        scene->addChild(layer);
        
        Vector<FiniteTimeAction*>actions;
        Vector<FiniteTimeAction*>actions_fade;
        for (int i=0; i<3; i++) {
            auto sup=createSpriteToCenter(StringUtils::format("sup_5_light_%d.png",i),true, scene->mainSprite);
            back->addChild(sup);
            
            actions.pushBack(TargetedAction::create(sup,Sequence::create(FadeIn::create(1),FadeOut::create(.5),FadeIn::create(1), NULL)));
            actions_fade.pushBack(TargetedAction::create(sup, FadeOut::create(1)));
        }
        
        actions_fade.pushBack(TargetedAction::create(back, FadeOut::create(1)));
        
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        auto sound=createSoundAction("alarm.mp3");
        auto flash=Spawn::create(actions);
        auto call=CallFunc::create([scene,callback,actions_fade,layer]{
            auto story = StoryLayer::create("opening2", [scene,callback,actions_fade,layer](Ref* sen){
                auto fadeout=Spawn::create(actions_fade);
                auto call=CallFunc::create([scene,callback,layer]{
                    layer->removeFromParent();
                    auto story = StoryLayer::create("opening3", [callback](Ref* sen){
                        callback(true);
                    });
                    scene->addChild(story);
                });
                
                scene->runAction(Sequence::create(fadeout,call, NULL));
            });
            scene->addChild(story);
        });
        
        scene->runAction(Sequence::create(fadein,sound,flash,call, NULL));
    });
    scene->addChild(storyLayer);
}

#pragma mark - Back
void BabyRoomActionManager::backAction(std::string key,int backID, Node *parent)
{
    if(key=="drum")
    {
        if(DataManager::sharedManager()->getEnableFlagWithFlagID(17)){
            auto left=parent->getChildByName("sup_31_0.png");
            
            auto right=parent->getChildByName("sup_31_1.png");
            
            
            
            auto seq=[parent,this](bool isLeft){
                auto distance=parent->getContentSize().height*.01;
                auto angle=-25.0*pow(-1, isLeft);
                auto duration_up=.3;
                auto duration_down=.15;
                
                std::string soundName="snare.mp3";
                if (DataManager::sharedManager()->isPlayingMinigame()) {
                    soundName="poku.mp3";
                }
                auto sequence=Sequence::create(Spawn::create(MoveBy::create(duration_up, Vec2(0, distance)),
                                                             RotateBy::create(duration_up, -angle), NULL),
                                               DelayTime::create(.15), Spawn::create(MoveBy::create(duration_down, Vec2(0, -distance)),
                                                                                    RotateBy::create(duration_down, angle), NULL),
                                               RotateBy::create(duration_down/2, -angle/4),
                                               RotateBy::create(duration_down/2, angle/4),
                                               this->createSoundAction(soundName),
                                               DelayTime::create(.7), NULL);
                
                return sequence;
            };

            auto action_left=TargetedAction::create(left, seq(true));
            auto action_right=TargetedAction::create(right, seq(false));

            parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),action_left,action_right,action_left->clone(),action_right->clone(),action_left->clone(),action_left->clone(),action_right->clone(),action_right->clone(),DelayTime::create(1), NULL), UINT_MAX));
        }
    }
    else if(key=="butterfly")
    {
        std::vector<Vec2>poses;
        if (backID==62) {
            poses.push_back(Vec2(.9, .6));
            poses.push_back(Vec2(.1, .3));
        }
        else{
            poses.push_back(Vec2(.9, .6));
            poses.push_back(Vec2(.1, .6));
        }
        
        Vector<FiniteTimeAction*>actions;
        
        for (auto pos: poses) {
            auto fly=createSpriteToCenter(StringUtils::format("anim_%d_0.png",backID), false, parent);
            fly->setPosition(parent->getContentSize().width*pos.x, parent->getContentSize().height*pos.y);
            parent->addChild(fly);
            
            Vector<SpriteFrame*>frames;
            for(int i=0;i<2;i++){
                auto s=Sprite::createWithSpriteFrameName(StringUtils::format("anim_%d_%d.png",backID,i));
                frames.pushBack(s->getSpriteFrame());
            }
            
            //
            auto animation=Animation::createWithSpriteFrames(frames);
            animation->setDelayPerUnit(10.0f/60.0f);//??フレームぶん表示
            animation->setRestoreOriginalFrame(true);
            animation->setLoops(UINT_MAX);
            
            //
            auto distance=parent->getContentSize().width*.25;
            auto height=parent->getContentSize().height*.02;
            if (pos.x>.5) {
                distance=-distance;
            }
            
            auto hover=Repeat::create(Sequence::create(EaseInOut::create(MoveBy::create(1, Vec2(0, height)), 2),
                                                       EaseInOut::create(MoveBy::create(1, Vec2(0, -height)), 2), NULL) , 2);
            auto move=Repeat::create(Sequence::create(JumpBy::create(5, Vec2(distance, 0), height, 3),
                                                      hover,
                                                      JumpBy::create(5, Vec2(-distance, 0), height, 3),
                                                      DelayTime::create(1),NULL), 1);
            auto repeat_move=Repeat::create(move, UINT_MAX);
            //animation
            auto action=TargetedAction::create(fly,Spawn::create(Animate::create(animation),
                                                                 repeat_move,
                                                                 NULL));
            actions.pushBack(action);
        }
        
        parent->runAction(Spawn::create(actions));
    }
#pragma mark パンダ
    else if(key=="panda"){
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(9)) {
            auto index=0;
            for(auto sup: SupplementsManager::getInstance()->orderSprites){
                sup->setPosition(sup->getPosition().x, parent->getContentSize().height/2-pandas.at(index)*parent->getContentSize().height);
                index++;
            }
        }
    }
    else if(key=="mouse"){
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            auto mouse=parent->getChildByName("sup_67_mistake.png");
            mouse->setPosition(parent->getContentSize().width*.75,mouse->getPosition().y);
            
            auto fadein=FadeIn::create(2);
            auto move=MoveTo::create(.5, parent->getContentSize()/2);
            auto call=CallFunc::create([]{
                Common::playSE("nyu.mp3");
                DataManager::sharedManager()->setTemporaryFlag("mouse", 1);
            });
            
            mouse->runAction(Sequence::create(fadein,move,call, NULL));
        }
    }
}

#pragma mark - Touch
void BabyRoomActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key.compare(0,6,"flower")==0){
        auto index=atoi(key.substr(7,1).c_str());
        AnswerManager::getInstance()->appendPasscode(StringUtils::format("%d",index));
        bool correct=false;
        
        //答え判定
        if (AnswerManager::getInstance()->getPasscode().size()==5) {
            if (AnswerManager::getInstance()->getPasscode()=="01012"&&
                DataManager::sharedManager()->getEnableFlagWithFlagID(28)) {
                correct=true;
            }
            else{
                AnswerManager::getInstance()->resetPasscode();
            }
        }
        
        Common::playSE("hyu.mp3");
        
        auto flower=parent->getChildByName(StringUtils::format("sup_20_%d.png",index));
        
        auto rotate=TargetedAction::create(flower, EaseInOut::create(RotateBy::create(1.5, 720), 2));
        
        auto call=CallFunc::create([callback,correct]{
            if(correct){
                Common::playSE("pinpon.mp3");
            }
            callback(correct);
        });
        
        parent->runAction(Sequence::create(rotate,call, NULL));
    }
    else if(key=="stick"){
        Common::playSE("p.mp3");
        
        auto stick=createSpriteToCenter("anim_31_stick.png",true, parent);
        parent->addChild(stick);
        
        auto stick_1=createSpriteToCenter("sup_31_0.png", true, parent);
        parent->addChild(stick_1);
        
        auto spawn=TargetedAction::create(stick, Spawn::create(DelayTime::create(.5),FadeIn::create(1),ScaleBy::create(2, .9),DelayTime::create(1), NULL));
        auto change=createChangeAction(1,.7, stick, stick_1);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,createSoundAction("monkey.mp3"),change,DelayTime::create(1),call, NULL));
    }
    else if(key=="carrot"){
        Common::playSE("p.mp3");
        
        auto carrot=AnchorSprite::create(Vec2(.75, .5), parent->getContentSize(), "anim_45_carrot.png");
        carrot->setOpacity(0);
        parent->addChild(carrot);
        
        auto anim=createSpriteToCenter("anim_45_rabbit.png",true, parent);
        parent->addChild(anim);
        
        auto spawn=TargetedAction::create(carrot, Spawn::create(DelayTime::create(.5),FadeIn::create(1),ScaleBy::create(2, .9),DelayTime::create(1), NULL));
        auto change=createChangeAction(1, .7, carrot, anim);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,DelayTime::create(1),change,DelayTime::create(1),call, NULL));
    }
    else if(key=="elephant"){
        Common::playSE("p.mp3");
        
        auto elephant=createSpriteToCenter("anim_10_elephant.png", true, parent);
        elephant->setLocalZOrder(1);
        parent->addChild(elephant);
        
        auto duck_0=parent->getChildByName<Sprite*>("sup_10_duck_0.png");
        auto anim_0=createSpriteToCenter("anim_10_duck.png", true, parent);
        parent->addChild(anim_0);
        
        auto anim_1=createSpriteToCenter("sup_10_duck_1.png", true, parent);
        parent->addChild(anim_1);
        
        //
        auto fadeins=TargetedAction::create(elephant, Sequence::create(FadeIn::create(1), NULL));
        auto delay=DelayTime::create(.7);
        auto sound=createSoundAction("jaa.mp3");
        auto change_0=createChangeAction(1, .7, duck_0, anim_0);
        auto rotate=TargetedAction::create(elephant,Sequence::create(RotateBy::create(.5, -5),RotateBy::create(.5, 5), NULL));
        auto change_1=Spawn::create(createChangeAction(1, .7, anim_0, anim_1),
                                    TargetedAction::create(elephant, RotateBy::create(1, -10)),
                                    TargetedAction::create(elephant, MoveBy::create(1, Vec2(parent->getContentSize().width*.1,0))), NULL) ;
        auto fadeout=TargetedAction::create(elephant, FadeOut::create(1));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadeins,delay,sound,Spawn::create(change_0,rotate, NULL),rotate,delay->clone(),change_1,fadeout,call, NULL));
    }
    else if(key=="pig")
    {
        Common::playSE("pig_1.mp3");
        auto num=DataManager::sharedManager()->getNowBack();
        auto cloud_anchor=Vec2(.585,.68);
        if (num==23) {
            cloud_anchor=Vec2(.38,.6);
        }
        
        auto cloud=AnchorSprite::create(cloud_anchor, parent->getContentSize(), StringUtils::format("anim_%d_cloud.png",num));
        cloud->setOpacity(0);
        parent->addChild(cloud);
        auto original_scale=cloud->getScale();
        cloud->setScale(.1);
        
        auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(.5, original_scale),
                                                                                    FadeIn::create(.5), NULL),
                                                                      createSoundAction("nyu.mp3"),
                                                                      DelayTime::create(.5),
                                                                      Spawn::create(ScaleTo::create(.5, .1),
                                                                                    FadeOut::create(.5), NULL), NULL));
        
        
        Vector<FiniteTimeAction*>actions;
        for(int i=0;i<4;i++){
            auto part=parent->getChildByName(StringUtils::format("sup_%d_%d.png",num,i));
            auto angle=20;
            if (i%2==1) {
                angle=-angle;
            }
            
            auto seq=Sequence::create(EaseInOut::create(RotateBy::create(.4, angle), 1.5),
                                      EaseInOut::create(RotateBy::create(.4, -angle), 1.5), NULL);
            
            actions.pushBack(TargetedAction::create(part,Repeat::create(seq, 2)));
        }
        
        auto spawn=Spawn::create(seq_cloud,Spawn::create(actions), NULL);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,call, NULL));
    }
    else if(key=="diaper")
    {
        Common::playSE("p.mp3");
        auto pig_0=parent->getChildByName("sup_59_pig_0.png");
        Vector<FiniteTimeAction*>fadeouts;
        for(int i=0;i<4;i++){
            auto part=parent->getChildByName(StringUtils::format("sup_59_%d.png",i));
            fadeouts.pushBack(TargetedAction::create(part,FadeOut::create(1)));
        }
        fadeouts.pushBack(TargetedAction::create(pig_0, FadeOut::create(1)));
        
        auto anim_0=createSpriteToCenter("anim_59_0.png", true, parent);
        parent->addChild(anim_0);
        auto anim_1=createSpriteToCenter("anim_59_1.png", true, parent);
        parent->addChild(anim_1);
        //auto pig_1=createSpriteToCenter("sup_59_pig_1.png", true, parent);
        //parent->addChild(pig_1);
        
        auto change_0=Spawn::create(Spawn::create(fadeouts),
                                    TargetedAction::create(anim_0, FadeIn::create(.7)), NULL);
        
        auto delay=DelayTime::create(2);
        auto sound=createSoundAction("pig_1.mp3");
        auto change_1=createChangeAction(1, .7, anim_0, anim_1);
        //auto sound_1=createSoundAction("snoring.mp3");
        //auto change_2=createChangeAction(1, .7, anim_1, pig_1);

        auto light_action=createLightAction(0, parent);
        
        auto call=CallFunc::create([callback]{
            auto story=StoryLayer::create("diaper", [callback](Ref*ref){
                Common::playSE("pinpon.mp3");
                callback(true);
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        parent->runAction(Sequence::create(change_0,delay,sound,change_1,delay->clone()/*,sound_1,change_2*/,light_action,call,NULL));
    }
    else if(key=="milk"){
        Common::playSE("p.mp3");
        
        auto anim_0=createSpriteToCenter("anim_69_0.png", true, parent);
        anim_0->setLocalZOrder(1);
        parent->addChild(anim_0);
        auto anim_1=createSpriteToCenter("anim_69_1.png", true, parent);
        anim_1->setLocalZOrder(1);
        parent->addChild(anim_1);
        
        auto fadein_0=TargetedAction::create(anim_0, FadeIn::create(1));
        auto fadein_1=TargetedAction::create(anim_1, FadeIn::create(1));
        auto light_action=createLightAction(1, parent);
        auto call=CallFunc::create([callback]{
            auto story=StoryLayer::create("milk", [callback](Ref*ref){
                Common::playSE("pinpon.mp3");
                callback(true);
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        parent->runAction(Sequence::create(DelayTime::create(.7),fadein_0,DelayTime::create(1),createSoundAction("drink.mp3"),fadein_1,DelayTime::create(1),light_action,call, NULL));
    }
    else if(key=="crayon"){
        Common::playSE("p.mp3");
        
        auto character=createSpriteToCenter("sup_26_crayon.png", true, parent);
        parent->addChild(character);
        
        auto crayon=createSpriteToCenter("anim_26_crayon.png", true, parent);
        parent->addChild(crayon);
        
        //fadein
        auto fadein=FadeIn::create(1);
        auto sound=CallFunc::create([this]{
            Common::playSE("pen.mp3");
        });
        //move
        auto distance=parent->getContentSize().width*.05;
        auto duration=.2;
        Vector<FiniteTimeAction*>moves;
        for (int i=1; i<6; i++) {
            auto move=MoveBy::create(duration+i*.025, Vec2(distance*i*pow(-1, i)+distance*1*(i/2), distance*i*pow(-1, i)-distance*.4*(i/2)));
            moves.pushBack(move);
        }
        
        auto seq=TargetedAction::create(crayon, Sequence::create(fadein,sound,Sequence::create(moves),FadeOut::create(.7), NULL));
        
        //fadein_chara
        auto fadein_1=TargetedAction::create(character, Sequence::create(DelayTime::create(2.25),FadeIn::create(1), NULL));
        auto spawn=Spawn::create(seq,fadein_1, NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,call,NULL));
    }
    else if(key=="driver")
    {
        Common::playSE("p.mp3");
        
        auto driver=Sprite::createWithSpriteFrameName("anim_8_driver.png");
        driver->setPosition(parent->getContentSize()/2);
        driver->setOpacity(0);
        parent->addChild(driver);
        
        auto screw_0=parent->getChildByName("sup_8_screw_0.png");
        auto screw_1=parent->getChildByName("sup_8_screw_1.png");
        
        auto distance=parent->getContentSize().width*.01;
        auto fadein= FadeIn::create(.7);
        auto shake=Repeat::create(Sequence::create(MoveBy::create(.1,Vec2(distance, distance)),
                                                   CallFunc::create([]{Common::playSE("kacha_1.mp3");}),
                                                   MoveBy::create(.1,Vec2(-distance, -distance)), NULL),3);
        
        auto seq_0=TargetedAction::create(driver, Sequence::create(fadein,shake, NULL));
        auto remove_screw_0=TargetedAction::create(screw_1, FadeOut::create(0));
        
        auto move=MoveBy::create(1, Vec2(-parent->getContentSize().width*.22, -parent->getContentSize().height*.195));
        auto seq_1=TargetedAction::create(driver, Sequence::create(move,shake->clone(), NULL));
        auto remove_screw_1=TargetedAction::create(screw_0, FadeOut::create(0));
        
        auto spawn=Spawn::create(TargetedAction::create(driver, FadeOut::create(.5)), NULL);
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_0,remove_screw_0,seq_1,remove_screw_1,DelayTime::create(.5),spawn,DelayTime::create(.3),call, NULL));
    }
    else if(key=="musicbox"){
        Common::playSE("kacha.mp3");
        
        auto handle=createSpriteToCenter("sup_24_handle_0.png", true, parent);
        parent->addChild(handle);
        
        auto fadein=TargetedAction::create(handle, FadeIn::create(.7));
        
        Vector<FiniteTimeAction*>actions;
        Vector<FiniteTimeAction*>actions_doll;

        for(int i=0;i<3;i++){
            auto handle_after=createSpriteToCenter(StringUtils::format("sup_24_handle_%d.png",i+1), true, parent);
            parent->addChild(handle_after);
            auto handle_anim=createChangeAction(.5, .3, parent->getChildByName<Sprite*>(StringUtils::format("sup_24_handle_%d.png",i)),handle_after );
            
            if (i==0) {
                actions.pushBack(createSoundAction("gigigi.mp3"));
            }
            actions.pushBack(handle_anim);
            
            auto doll_after=createSpriteToCenter(StringUtils::format("sup_24_doll_%d.png",i+1), true, parent);
            parent->addChild(doll_after);
            auto doll_anim=createChangeAction(.5, .3, parent->getChildByName<Sprite*>(StringUtils::format("sup_24_doll_%d.png",i)),doll_after );

            actions_doll.pushBack(doll_anim);
        }
        actions.pushBack(createChangeAction(.5, .3, parent->getChildByName<Sprite*>("sup_24_handle_3.png"), handle));
        actions_doll.pushBack(createChangeAction(.5, .3, parent->getChildByName<Sprite*>("sup_24_doll_3.png"), parent->getChildByName<Sprite*>("sup_24_doll_0.png")));
        auto handle_seq=Repeat::create(Sequence::create(actions), 2);
        auto doll_seq=Sequence::create(actions_doll);
        auto particle=CallFunc::create([parent]{
            auto pos=Vec2(parent->getContentSize().width*.25, parent->getContentSize().height*.55);
            auto size=parent->getContentSize().width*.15;
            
            auto particle=ParticleSystemQuad::create("particle_music.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(pos);
            particle->setLife(3);
            particle->resetSystem();
            particle->setStartSize(size);
            particle->setEndSize(size);
            particle->setGravity(Vec2(size*1, -size*.3));
            particle->setStartColor(Color4F::RED);
            particle->setEndColor(Color4F(255, 0, 0, 0));
            
            particle->setAngle(135);
            particle->setSpeed(size*2);
            parent->addChild(particle);
        });
        auto spawn_doll=Repeat::create(Spawn::create(doll_seq,
                                                     particle, NULL), 2);
        
        //scene change
        auto back=createSpriteToCenter("back_23.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        auto pig_1=createSpriteToCenter("sup_23_pig_1.png",true, parent);
        parent->addChild(pig_1);
        
        auto sups={"sup_23_0.png","sup_23_2.png","sup_23_3.png","sup_23_pig_0.png","sup_23_1.png","sup_23_handle.png"};
        for(auto name:sups){
            auto sup=createSpriteToCenter(name, false, back);
            sup->setName(name);
            back->addChild(sup);
        }
        
        auto change_scene=Sequence::create(DelayTime::create(3),TargetedAction::create(back, FadeIn::create(2)), NULL);
        auto change_pig=Spawn::create(TargetedAction::create(back->getChildByName("sup_23_0.png"), FadeOut::create(.7)),
                                      TargetedAction::create(back->getChildByName("sup_23_1.png"), FadeOut::create(1.5)),
                                      TargetedAction::create(back->getChildByName("sup_23_2.png"), FadeOut::create(1.5)),
                                      TargetedAction::create(back->getChildByName("sup_23_3.png"), FadeOut::create(1.5)),
                                      TargetedAction::create(back->getChildByName("sup_23_pig_0.png"), FadeOut::create(1.5)),
                                      TargetedAction::create(pig_1, FadeIn::create(1)),
                                      createSoundAction("snoring.mp3"),NULL);
        
        auto light_action=createLightAction(2, parent);
        
        auto call=CallFunc::create([callback](){
            auto story=StoryLayer::create("musicbox", [callback](Ref*ref){
                Common::playSE("pinpon.mp3");
                callback(true);
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        parent->runAction(Sequence::create(fadein,handle_seq,createSoundAction("music_box.mp3"),Spawn::create(spawn_doll,change_scene, NULL),DelayTime::create(1),change_pig,DelayTime::create(1),light_action,call, NULL));

    }
    else if(key=="panda"){
        Vector<FiniteTimeAction*>actions;
        auto index=0;
        for (auto sup:SupplementsManager::getInstance()->orderSprites) {
            actions.pushBack(TargetedAction::create(sup, EaseInOut::create(MoveBy::create(.5, Vec2(0, -parent->getContentSize().height*pandas.at(index))), 2)));
            
            index++;
        }
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(Spawn::create(actions),DelayTime::create(1),call, NULL));
    }
    else if(key=="check"){
        auto call= CallFunc::create([callback]{
            auto num=DataManager::sharedManager()->getNowBack();
            if((num==59&&DataManager::sharedManager()->getEnableFlagWithFlagID(36)&&DataManager::sharedManager()->getEnableFlagWithFlagID(40))||
               (num==24&&DataManager::sharedManager()->getEnableFlagWithFlagID(16)&&DataManager::sharedManager()->getEnableFlagWithFlagID(40))||
               (num==69&&DataManager::sharedManager()->getEnableFlagWithFlagID(16)&&DataManager::sharedManager()->getEnableFlagWithFlagID(36))){
                auto story=StoryLayer::create("ending1", [callback](Ref*ref){
                    Common::playSE("pinpon.mp3");
                    callback(true);
                });
                Director::getInstance()->getRunningScene()->addChild(story);
            }
            else{
                callback(true);
            }
        });
        
        parent->runAction(call);
    }
    else if(key=="clear"){
        auto open=createSpriteToCenter("sup_5_open.png", true, parent);
        parent->addChild(open);
        
        Common::playSE("gacha.mp3");
        
        auto fadein=TargetedAction::create(open, FadeIn::create(.3));
        auto call=CallFunc::create([]{
            
            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);
            
            auto story=StoryLayer::create("ending2", [](Ref*sender){
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        parent->runAction(Sequence::create(fadein,call, NULL));
    }
    else if(key=="peri"&&DataManager::sharedManager()->isPlayingMinigame()){
        auto mistake=parent->getChildByName("sup_21_mistake.png");
        if (mistake->getOpacity()>0) {
            mistake->setOpacity(0);
        }
        else{
            mistake->setOpacity(255);
        }
        
        callback(true);
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void BabyRoomActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{
    
}

#pragma mark -
Sequence* BabyRoomActionManager::createLightAction(int num, Node *parent)
{
    auto back=createSpriteToCenter("back_5.png", true, parent);
    back->setCascadeOpacityEnabled(true);
    back->setLocalZOrder(1);
    parent->addChild(back);
    
    std::vector<std::string>names;
    if(!DataManager::sharedManager()->getEnableFlagWithFlagID(16)){
        names.push_back("sup_5_light_0.png");
    }
    if(!DataManager::sharedManager()->getEnableFlagWithFlagID(40)){
        names.push_back("sup_5_light_1.png");
    }
    if(!DataManager::sharedManager()->getEnableFlagWithFlagID(36)){
        names.push_back("sup_5_light_2.png");
    }
    
    for (auto name : names) {
        auto sup=createSpriteToCenter(name, false, back);
        back->addChild(sup);
    }
    
    auto fadein=TargetedAction::create(back, FadeIn::create(1));
    auto delay=DelayTime::create(.7);
    auto fadeout=TargetedAction::create(back->getChildByName(StringUtils::format("sup_5_light_%d.png",num)), FadeOut::create(.5));
    auto finish=TargetedAction::create(back, FadeOut::create(2));
    
    return Sequence::create(fadein,delay,createSoundAction("pinpon.mp3"),fadeout,delay->clone(),finish, NULL);
}

#pragma mark - Private
Sprite* BabyRoomActionManager::createSpriteToCenter(std::string name, bool invisible, cocos2d::Node *parent)
{
    auto spr=Sprite::createWithSpriteFrameName(name);
    spr->setPosition(parent->getContentSize()/2);
    spr->setName(name);
    if (invisible) {
        spr->setOpacity(0);
    }
    return spr;
}

CallFunc* BabyRoomActionManager::createSoundAction(std::string fileName)
{
    auto call=CallFunc::create([fileName]{Common::playSE(fileName.c_str());});
    return call;
}

Spawn* BabyRoomActionManager::createChangeAction(float duration_fadeout,float duration_fadein, cocos2d::Sprite *before, cocos2d::Sprite *after)
{
    auto spawn=Spawn::create(TargetedAction::create(before, FadeOut::create(duration_fadeout)),
                             TargetedAction::create(after, FadeIn::create(duration_fadein)), NULL);
    
    return spawn;
}
