//
//  TouchManager.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/07/12.
//
//

#include "TouchManager.h"
#include "CustomActionKeys.h"
#include "SupplementsManager.h"
#include "DataManager.h"
#include "AnswerManager.h"
#include "PopUpLayer.h"
#include "Utils/NativeBridge.h"
#include "Utils/Common.h"
#include "Utils/AudioManager.h"
#include "Utils/ValueHelper.h"
#include "ZOrders.h"
#include "GameScene.h"

using namespace cocos2d;

TouchManager* TouchManager::manager =NULL;

TouchManager* TouchManager::getInstance()
{
    if (manager==NULL) {
        manager=new TouchManager();
    }
    return manager;
}

#pragma mark - カスタムアクション
void TouchManager::customAction(std::string key, cocos2d::Node *parent, bool onItem, const onFinished &callback)
{
#pragma mark Switch
    if(key.compare(0,6,CustomAction_SwitchSprite)==0)
    {//スイッチスプライトを使用
        ValueMap switchMap;
        if (onItem) {
            switchMap=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Switch, DataManager::sharedManager()->nowItemMap);
        }
        else{
            switchMap=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Switch,DataManager::sharedManager()->nowBackMap);
        }
        
        if (!DataManager::sharedManager()->checkCustomActionTouchable(switchMap)) {
            callback(false);
            return;
        }
        
        int index=atoi(key.substr(7,key.length()-6).c_str());//判別id
        //インクリメントする
        Vector<SwitchSprite*> switchSprites;
        if (onItem) {
            switchSprites=SupplementsManager::getInstance()->switchSprites_onItem;
        }
        else{
            switchSprites=SupplementsManager::getInstance()->switchSprites;
        }
        
        if(switchSprites.size()>index){
            //アニメーション対策
            auto layer=createDisableLayer();
            Director::getInstance()->getRunningScene()->addChild(layer);
            
            auto answer=switchMap["answer"];//答えのvalue
            
            auto s_spr=switchSprites.at(index);
            s_spr->switching([this,callback,layer,answer,onItem](Ref*ref){//スイッチの切り替え
                layer->removeFromParent();
                
                AnswerManager::getInstance()->checkAnswer(CustomAction_SwitchSprite, answer,onItem, [callback,answer](bool success){
                    callback(success);
                });
            });
        }
    }
#pragma mark Changing
    else if (key.compare(0,8,CustomAction_ChangingSprite)==0)
    {//自動変化
        ValueMap changeMap;
        if (onItem) {
            changeMap=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Changing,DataManager::sharedManager()->nowItemMap);
        }
        else{
            changeMap=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Changing,DataManager::sharedManager()->nowBackMap);
        }
        
        if (!DataManager::sharedManager()->checkCustomActionTouchable(changeMap)) {
            callback(false);
            return;
        }
        
        int index=atoi(key.substr(9,key.length()-8).c_str());//判別id
        Vector<ChangingSprite*> changingSprites;
        if (onItem) {
            //changeingSprites=SupplementsManager::getInstance()->changingSprites_onItem;
        }
        else{
            changingSprites=SupplementsManager::getInstance()->changingSprites;
        }
        
        auto layer=SupplementsManager::getInstance()->createDisableLayer();
        Director::getInstance()->getRunningScene()->addChild(layer);
        
        auto answer=changeMap["answer"];

        
        auto spr=changingSprites.at(index);
        spr->stopAnimate([this,callback,onItem,answer,layer](Ref*ref){//スイッチの切り替え
            layer->removeFromParent();
            callback(false);
            AnswerManager::getInstance()->checkAnswer(CustomAction_ChangingSprite, answer,onItem, [callback](bool success){
                callback(success);
            });
        });
    }
#pragma mark Label
    else if (key.compare(0,5,CustomAction_Label)==0)
    {//加算ラベルを使用
        ValueMap labelMap;
        if (onItem) {
            labelMap=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Label,DataManager::sharedManager()->nowItemMap);
        }
        else{
            labelMap=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Label,DataManager::sharedManager()->nowBackMap);
        }
        
        if (!DataManager::sharedManager()->checkCustomActionTouchable(labelMap)) {
            callback(false);
            return;
        }
        
        int labelIndex=atoi(key.substr(key.length()-1,1).c_str());//タップしたラベル判別id
        
        //インクリメントする
        Vector<CountUpLabel*> labelSupplements;

        if (onItem) {
            labelSupplements=SupplementsManager::getInstance()->labelSupplements_onItem;
        }
        else{
            labelSupplements=SupplementsManager::getInstance()->labelSupplements;
        }
        
        if(labelSupplements.size()>labelIndex){
            auto countLabel=labelSupplements.at(labelIndex);
            countLabel->countUp();
        }
        
        //答えチェック
        auto answer=labelMap["answer"];
        AnswerManager::getInstance()->checkAnswer(CustomAction_Label, answer,onItem, [callback](bool success){
            callback(success);
        });
    }
#pragma mark Rotate
    else if(key.compare(0,6,CustomAction_RotateSprite)==0)
    {//回転を使用
        auto map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Rotate,DataManager::sharedManager()->nowBackMap);
        auto answer=map["answer"];
        
        if (!DataManager::sharedManager()->checkCustomActionTouchable(map)) {
            callback(false);
            return;
        }
        
        int index=atoi(key.substr(7,key.length()-6).c_str());//判別id
        
        auto layer=SupplementsManager::getInstance()->createDisableLayer();
        Director::getInstance()->getRunningScene()->addChild(layer);
        
        auto rotateSprites=SupplementsManager::getInstance()->rotateSprites;
        auto rotate=rotateSprites.at(index);
        
        if (rotate->getDisable()) {
            log("非表示のrotate");
            layer->removeFromParent();
            callback(false);
            return;
        }
        
        if (rotate->getFlashFileName().size()>0) {
            SupplementsManager::getInstance()->animateSupplement(parent,rotate->getFlashFileName(), SupAnimateType_Flash, .1,0, NULL);
        }
        
        rotate->rotate([this,callback,layer,answer,rotateSprites](Ref*ref){
            layer->removeFromParent();
            
            AnswerManager::getInstance()->checkAnswer(CustomAction_RotateSprite, answer,false, [this,answer,callback](bool success){
                callback(success);
            });
        });
    }
#pragma mark Input InputRotate

    else if (key.compare(0,5,CustomAction_Input)==0)
    {//順番問題に使用
        //input or input_rotate mapkey
        auto mapKey=StringUtils::format("%s",(key.compare(0,12,CustomAction_InputRotate)==0) ? CustomActionNameOnBack_InputRotate:CustomActionNameOnBack_Input);
        
        //append character
        auto actionKey=StringUtils::format("%s",(key.compare(0,12,CustomAction_InputRotate)==0) ? CustomAction_InputRotate:CustomAction_Input);
        auto character=key.substr(actionKey.length()+1,key.length()-(actionKey.length()+1));
        
        ValueMap inputMap;
        if (onItem) {
            inputMap=DataManager::sharedManager()->getCustomSupplementsMap(mapKey,DataManager::sharedManager()->nowItemMap);
        }
        else{
            inputMap=DataManager::sharedManager()->getCustomSupplementsMap(mapKey,DataManager::sharedManager()->nowBackMap);
        }
        auto disable=!checkTouchEnable(inputMap);
        auto playSE=inputMap["playSE"].asString();
        auto duration=inputMap["duration"].asFloat();
        auto answerMapV=inputMap["answer"];
        
        if (duration==0) {
            duration=.1;
        }
        
        //アニメーション画像読み込み
        std::string fileName="";
        
        auto addType=inputMap["defaultAdd"].asInt();
        if (!inputMap["addType"].isNull()) {
            addType=inputMap["addType"].asInt();
        }
        
        SupAnimateType type=SupAnimateType_Add;
        auto value=0.0;
        if (mapKey==CustomActionNameOnBack_Input) {
            if (!inputMap["animateSupplements"].isNull()) {
                auto supMap=inputMap["animateSupplements"].asValueMap();
                fileName=supMap[character].asString();
            }
            
            if (addType==1) {
                type=SupAnimateType_Flash;
            }
            else if (addType==2) {
                type=SupAnimateType_FadeOut;
                
                //すでに入力されているかcheck
                auto pass=AnswerManager::getInstance()->getPasscode(onItem);
                bool found= (pass.find(character)!=std::string::npos);
                if (found) {
                    log("すでに入力されているinputです");
                    callback(false);
                    return;
                }
            }
        }
        else if(mapKey==CustomActionNameOnBack_InputRotate){
            fileName=inputMap["supplement"].asValueMap()["name"].asString();
            type=SupAnimateType_SwingRotate;
            value=inputMap["supplement"].asValueMap()["angles"].asValueVector().at(atoi(character.c_str())).asFloat();
        }
        
        
        //run
        Common::playSE(playSE.c_str());
        AnswerManager::getInstance()->appendPasscode(character,onItem);
        
        Node* parent_animate;
        if (onItem) {
            parent_animate=((PopUpLayer*)parent)->itemImage;
        }
        else{
            parent_animate=parent;
        }
        
        SupplementsManager::getInstance()->animateSupplement(parent_animate,fileName,type,duration,value,[this,callback,disable,answerMapV,onItem,addType](Ref*ref){
            auto answerMap=answerMapV.asValueMap();
            auto answer=answerMap["answer"].asString();
            //判定
            if (disable) {
                log("disable input");
                callback(false);
                return;
            }
            
            auto pass=AnswerManager::getInstance()->getPasscode(onItem);
            
            if(AnswerManager::getInstance()->checkCustomActionAnswerable(answerMap)&&
               pass==answer)
            {//正解
                AnswerManager::getInstance()->manageAnswer(answerMap, [callback](){
                    callback(true);
                });
            }
            else
            {//不正解
                //addTypeが2のときはりせっとしない
                if (pass.length()==answer.length()&&
                    addType!=2) {
                    AnswerManager::getInstance()->resetPasscode(onItem);
                }
                callback(false);
            }
        });
    }
#pragma mark Order

    else if(key.compare(0,5,CustomAction_OrderSprite)==0)
    {//並び替えを使用
        auto orderSprites=SupplementsManager::getInstance()->orderSprites;
        if(orderSprites.size()==0){
            callback(false);
            return;
        }
        
        auto map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Order,DataManager::sharedManager()->nowBackMap);
        auto answer=map["answer"];
        int spriteIndex=atoi(key.substr(6,key.length()-5).c_str());//判別id
        auto touchSpr=orderSprites.at(spriteIndex);
        
        int selectedIndex=-1;//すでに選択されているものがあるかチェックする
        for (OrderSprite* spr :orderSprites) {
            if (spr->getIsSelected()) {
                selectedIndex=spr->getNumber();
                break;
            }
        }
        
        if (!checkTouchEnable(map)) {
            callback(false);
            return;
        }
        
        if (selectedIndex<0)
        {//選択されているspriteはない
            auto layer=SupplementsManager::getInstance()->createDisableLayer();
            Director::getInstance()->getRunningScene()->addChild(layer);
            //アイテム使用チェック
            touchSpr->useItem(map["setItem"],parent->getContentSize(), [this,touchSpr,callback,answer,layer](bool success){
                if (success) {
                    layer->removeFromParent();
                    //アイテムセット->答えチェック
                    AnswerManager::getInstance()->checkAnswer(CustomAction_OrderSprite, answer,false, [callback](bool success){
                        if (success) {
                            callback(true);
                        }
                    });
                }
                else{
                    //アイテムセットなし->選択状態にする
                    touchSpr->up([layer,callback](bool success){
                        layer->removeFromParent();
                        callback(false);
                    });
                }
            });
        }
        else
        {//順番チェンジ処理
            if (spriteIndex==selectedIndex)
            {//同じのを選択
                touchSpr->down();
            }
            else
            {//画像インデックスをチェンジ
                auto selectedSprite=orderSprites.at(selectedIndex);//選択状態のsprite
                auto selectedImageIndex=selectedSprite->getIndex();
                selectedSprite->change(touchSpr->getIndex(),parent->getContentSize(),true,false);
                touchSpr->change(selectedImageIndex,parent->getContentSize(),true,true);//soundは一回のみ鳴らす touchSprは空欄以外になるので基本的にsound指定がある
            }
            
            //答えチェック
            AnswerManager::getInstance()->checkAnswer(CustomAction_OrderSprite, answer,false, [callback](bool success){
                callback(success);
            });
        }
    }
#pragma mark Slider
    else if(key.compare(0,6,CustomAction_SliderSprite)==0)
    {
        auto map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Slider,DataManager::sharedManager()->nowBackMap);
        auto answer=map["answer"];
        
        if (!DataManager::sharedManager()->checkCustomActionTouchable(map)) {
            callback(false);
            return;
        }
        
        if (SupplementsManager::getInstance()->sliderSprites.size()>0) {
            auto index=atoi(key.substr(7,key.length()-6).c_str());
            auto layer=SupplementsManager::getInstance()->createDisableLayer();
            Director::getInstance()->getRunningScene()->addChild(layer);
            
            auto spr=SupplementsManager::getInstance()->sliderSprites.at(index);
            spr->slide([this,index,callback,layer,answer,onItem](Ref*ref){
                layer->removeFromParent();
                
                if(!answer.isNull()){
                    auto answerMap=answer.asValueMap();
                    //必要なフラグが立っているか
                    if (!AnswerManager::getInstance()->checkCustomActionAnswerable(answerMap)) {
                        callback(false);
                        return ;
                    }
                    
                    AnswerManager::getInstance()->checkAnswer(CustomAction_SliderSprite, answer, onItem, [callback](bool success){
                        callback(success);
                    });
                }
            });
        }
    }
#pragma mark Drag
    else if(key.compare(0,4,CustomAction_DragSprite)==0)
    {//ドラッグを使用
        if(SupplementsManager::getInstance()->dragSprites.size()==0){
            return;
        }
        auto index=atoi(key.substr(5,1).c_str());
        auto spr=SupplementsManager::getInstance()->dragSprites.at(index);
        
        if (spr->getEndRect().containsPoint(spr->getPosition())) {
            auto map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Drag,DataManager::sharedManager()->nowBackMap);
            auto answer=map["answer"];
            if(!answer.isNull()){
                log("せいかいdrag");
                
                auto answerMap=answer.asValueMap();
                if (!answerMap["needFlagIDs"].isNull()) {
                    if (!DataManager::sharedManager()->getEnableFlagWithFlagIDs(answerMap["needFlagIDs"].asValueVector())) {
                        spr->stopAllActions();
                        spr->setPosition(spr->getDefaultPosition());
                        return ;
                    }
                }
                
                Common::playSE(map["playSE"].asString().c_str());
                
                if (spr->getEndFileName().size()>0) {
                    //reloadされるまでの間あたらしいspriteをadd
                    auto sup = EscapeStageSprite::createWithSpriteFileName(spr->getEndFileName());
                    sup->setPosition(parent->getContentSize()/2);
                    parent->addChild(sup);
                    
                    spr->setOpacity(0);
                }
                
                AnswerManager::getInstance()->manageAnswer(answerMap, [callback](){
                    callback(true);
                });
            }
        }
        else{
            spr->stopAllActions();
            spr->setPosition(spr->getDefaultPosition());
        }
    }

#pragma mark Pattern
    else if(key==CustomAction_PatternSprite)
    {//touchEndで呼び出し
        if (SupplementsManager::getInstance()->patternSprites_onItem.size()==0&&
            onItem) {
            log("ドラッグがされていません");
            callback(false);
            return;
        }
        else if (SupplementsManager::getInstance()->patternSprites.size()==0&&
                 !onItem) {
            log("ドラッグがされていません");
            callback(false);
            return;
        }
        
        PatternSprite* pattern;
        ValueMap map;
        Node*pattern_parent;
        if (onItem) {
            pattern=SupplementsManager::getInstance()->patternSprites_onItem.at(0);
            map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Pattern,DataManager::sharedManager()->nowItemMap);
            auto popup=(PopUpLayer*)parent;
            pattern_parent=popup->itemImage;
        }
        else{
            pattern=SupplementsManager::getInstance()->patternSprites.at(0);
            map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Pattern,DataManager::sharedManager()->nowBackMap);
            pattern_parent=parent;
        }
        
        
        pattern->end(true);

        auto answerMap=map["answer"].asValueMap();
        
        auto layer=SupplementsManager::getInstance()->createDisableLayer();
        parent->addChild(layer);
        
        /**正解判定*/
        pattern->check(pattern_parent,answerMap,map,onItem,[callback,answerMap,layer,this](bool success){
            layer->removeFromParent();
            
            if (success) {
                log("パターン正解");
                AnswerManager::getInstance()->manageAnswer(answerMap, [callback](){
                    callback(true);
                });
            }
            else{
                log("パターンふ正解");

                callback(false);
            }
        });
    }
    else{
        //その他のアクション
        auto layer=SupplementsManager::getInstance()->createDisableLayer();
        Director::getInstance()->getRunningScene()->addChild(layer);
        
        auto manager=SupplementsManager::getInstance()->getCustomActionManager();
        if (onItem) {
            manager->itemAction(key, (PopUpLayer*)parent, [callback,layer](bool success){
                layer->removeFromParent();
                
                callback(success);
            });
        }
        else{
            //実行
            manager->touchAction(key, parent,[callback,layer](bool success){
                layer->removeFromParent();
                callback(success);
            });
        }
    }
}

#pragma mark - Touch 処理
#pragma mark - タッチ可能判定
bool TouchManager::checkTouchEnable(cocos2d::ValueMap map)
{
    //changeAngleをスルーするか
    if (map["changeAngle"].asBool()) {
        auto itemMap=DataManager::sharedManager()->getItemData(DataManager::sharedManager()->getShowingItemID());
        auto angleMap=itemMap["angles"].asValueMap();
        auto angle=DataManager::sharedManager()->getShowingAngle();//現在の視点
        auto vec=angleMap["images"].asValueVector();
        if (angle==vec.size()) {
            log("最後のangle によるタッチ不可");
            return false;
        }
    }
    
    //必要なアングルをチェック
    std::vector<int>needAngles;
    if (!map["needAngle"].isNull()) {
        needAngles.push_back(map["needAngle"].asInt());
    }
    if (!map["needAngles"].isNull()) {
        for (auto v : map["needAngles"].asValueVector()) {
            needAngles.push_back(v.asInt());
        }
    }
    
    if (needAngles.size()>0) {
        if (!ValueHelper::isContainsValue(needAngles, DataManager::sharedManager()->getShowingAngle())) {
            //発見できなかった
            log("angle不正によるタッチ不可");
            return false;
        }
    }
    
    
    auto disableFlag=map["disableFlagID"].asFloat();//このフラグがtrueなら処理しない
    if (disableFlag>0&&DataManager::sharedManager()->getEnableFlagWithFlagID(disableFlag)==true)
    {
        log("disableフラグによるタッチ不可");
        disableAction(map);
        
        return false;
    }
    
    auto disableFlagIDs=map["disableFlagIDs"];
    if (!disableFlagIDs.isNull()) {
        auto vec=disableFlagIDs.asValueVector();
        auto disable=true;
        for (auto v:vec) {
            if (!DataManager::sharedManager()->getEnableFlagWithFlagID(v.asFloat())) {
                disable=false;
                break;
            }
        }
        
        if (disable) {
            log("disableフラグsによるタッチ不可");
            return false;
        }
    }
    
    auto needFlags=map["needFlagIDs"];//指定フラグが全て立っていないなら処理しない
    if (!needFlags.isNull()) {
        auto vector=needFlags.asValueVector();
        
        for (auto value :vector) {
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(value.asFloat())==false) {
                log("必要フラグ不足によるタッチ不可 :%f",value.asFloat());
                disableAction(map);
                
                return false;
            }
        }
    }
    
    auto needFlag=map["needFlagID"].asFloat();
    if (needFlag>0&&!DataManager::sharedManager()->getEnableFlagWithFlagID(needFlag)) {
        log("必要フラグ不足によるタッチ不可 :%f",needFlag);
        disableAction(map);
        
        return false;
    }
    
    //特定のアイテムを選択していなかったら処理しない
    std::vector<int>needItemIDs;
    
    if (!map["needItemID"].isNull()) {
        needItemIDs.push_back(map["needItemID"].asInt());
    }
    
    if (!map["needItemIDs"].isNull()) {
        auto IDs=map["needItemIDs"].asValueVector();
        for (auto id :IDs) {
            needItemIDs.push_back(id.asInt());
        }
    }
    
    if (needItemIDs.size()>0) {
        auto itr_item=std::find(needItemIDs.begin(), needItemIDs.end(), DataManager::sharedManager()->getSelectedItemID());
        auto found_item=(needItemIDs.end()!=itr_item);
        if (!found_item) {
            log("選択アイテム不良によるタッチ不可");
            return false;
        }
    }
    
    //一時的なフラグが指定した値でなければ処理しない
    if (!map["needTemporaryFlag"].isNull()) {
        auto temporaryMap=map["needTemporaryFlag"].asValueMap();
        auto flag=DataManager::sharedManager()->getTemporaryFlag(temporaryMap["key"].asString()).asInt();
        std::vector<int>flags;
        if (!temporaryMap["values"].isNull()) {
            auto needFlags=temporaryMap["values"].asValueVector();
            for (Value v : needFlags) {
                flags.push_back(v.asInt());
            }
        }
        
        if (!temporaryMap["value"].isNull()) {
            flags.push_back(temporaryMap["value"].asInt());
        }
        
        bool ok=false;
        for (int i:flags) {
            if (i==flag) {
                ok=true;
                break;
            }
        }
        
        if (!ok) {
            log("temporaryフラグ不良によるタッチ不可 %s",temporaryMap["key"].asString().c_str());
            disableAction(map);
            return false;
        }
    }
    
    if (!map["needTemporaryFlags"].isNull()) {
        auto temporaryVector=map["needTemporaryFlags"].asValueVector();
        
        bool ok = true;
        for (auto temporaryValue : temporaryVector) {
            auto temporaryMap = temporaryValue.asValueMap();
            auto flag=DataManager::sharedManager()->getTemporaryFlag(temporaryMap["key"].asString()).asInt();
            auto needFlag = temporaryMap["value"].asInt();
            if (flag != needFlag) {//論理積。needTemporaryFlagsの中身は全て満たしている必要あり
                ok = false;
                break;
            }
        }
        
        if (!ok) {
            log("temporaryフラグス不良によるタッチ不可");
            disableAction(map);
            return false;
        }
    }
    
    //その他のフラグが指定した値でなければ処理しない
    if (!map["needOtherFlag"].isNull()) {
        auto otherMap=map["needOtherFlag"].asValueMap();
        auto flag=DataManager::sharedManager()->getOtherFlag(otherMap["key"].asString()).asInt();
        std::vector<int>flags;
        
        bool ok=false;
        if (!otherMap["values"].isNull()) {
            for (Value v : otherMap["values"].asValueVector()) {
                flags.push_back(v.asInt());
            }
        }
        
        if (!otherMap["value"].isNull()) {
            flags.push_back(otherMap["value"].asInt());
        }
        
        for (int i:flags) {
            if (i==flag) {
                ok=true;
                break;
            }
        }
        
        if (!ok) {
            log("otherフラグ不良によるタッチ不可");
            disableAction(map);
            return false;
        }
    }
    
    //間違い探し
    if (!map["mysteryID"].isNull()&&!DataManager::sharedManager()->isPlayingMinigame()) {
        log("間違い探し中でない。によるタッチ不可");
        return false;
    }
    
    //バイブ判定を最後尾で行う アイテム使用時のみ
    if (needItemIDs.size()>0&&!map["noVibration"].asBool()) {
        NativeBridge::vibration3D();
    }
    
    return true;
}

#pragma mark - タッチ座標判定
bool TouchManager::isContained(Node *parent, ValueMap map, Vec2 pos)
{
    auto origin_x=map["x"].asFloat()*parent->getBoundingBox().size.width;
    auto origin_y=map["y"].asFloat()*parent->getBoundingBox().size.height;
    auto width=map["width"].asFloat()*parent->getBoundingBox().size.width;
    auto height=map["height"].asFloat()*parent->getBoundingBox().size.height;
    Rect area;
    auto radius=map["radius"].asFloat()*parent->getBoundingBox().size.width;
    bool isContain=false;
    if (radius>0) {
        auto distance=sqrt(pow(origin_x-pos.x, 2)+pow(origin_y-pos.y, 2));
        isContain=radius>distance;
        area=Rect(origin_x-radius, origin_y-radius, radius*2, radius*2);
    }
    else{
        //タッチ領域のrect
        area=Rect(origin_x, origin_y, width,height);
        isContain=area.containsPoint(pos);
    }
        
    //デバッグ用 タッチ領域を可視化
    if (isContain) {
        Common::performProcessForDebug([parent, area](){
            SupplementsManager::getInstance()->addDebugSprite(parent, area);
        }, nullptr);
    }
    
    return isContain;
}

#pragma mark Reset
void TouchManager::resetTouchInfo()
{
    if(touchMap.size()>0){
        touchMap.clear();
    }
    if(priorityWarpMap.size()>0){
        priorityWarpMap.clear();
    }
    
    AnswerManager::getInstance()->setCustomActionKey_1("");
    AnswerManager::getInstance()->setCustomActionKey_2("");
}

#pragma mark タッチ直後処理
TouchType TouchManager::manageTouchStart(ValueMap map, Touch *touch, Node *parent, bool onItem)
{
    //処理を行うかチェック
    //優先してワープを行うかをチェック(他の処理を飛ばす)
    if (!map["priorityWarp"].isNull())
    {
        auto pri_map=map["priorityWarp"].asValueMap();
        std::vector<float>flags;
        if (!pri_map["flagIDs"].isNull()) {
            for (auto v : pri_map["flagIDs"].asValueVector()) {
                flags.push_back(v.asFloat());
            }
        }
        auto flag=pri_map["flag"].asFloat();
        if (flag>0) {
            flags.push_back(flag);
        }
        
        auto canWarp=true;
        for (auto f : flags) {
            if (!DataManager::sharedManager()->getEnableFlagWithFlagID(f)) {
                canWarp=false;
                break;
            }
        }
        if (canWarp) {
            TouchManager::getInstance()->priorityWarpMap=pri_map;
            
            return TouchType_True;
        }
    }
    
    if (map["stopSE"].asBool()) {
        Common::stopAllSE();
    }

    //フラグ、選択アイテムによるタッチ可能をチェック
    if (!checkTouchEnable(map)) {
        if (map["touchThrough"].asBool()) {
            log("タッチ検索続行");
            return TouchType_Continue;
        }
        //touchMapを保持せずtrueを返す
        return TouchType_True;
    }
    
    //保持。endで処理
    TouchManager::getInstance()->touchMap=map;
    
    //beganのcustomAction
    SupplementsManager::getInstance()->getCustomActionManager()->touchBeganAction(map["customActionKey"].asString(), parent);
    
    //drag開始処理

    TouchManager::getInstance()->dragAction(true,touch,parent,onItem);
    return TouchType_True;
}

#pragma mark Drag
void TouchManager::dragAction(bool isInBegan, Touch *touch, Node *parent, bool onItem)
{
    if (touch==NULL) {
        return;
    }
    
    auto touchMap=TouchManager::getInstance()->touchMap;
    
    auto key=touchMap["customActionKey"].asString();//空の場合もあります
    Vec2 pos;
    if (onItem) {
        //posをbackImageに基準に変換
        pos=Vec2(touch->getLocation().x-parent->getBoundingBox().origin.x, touch->getLocation().y-parent->getBoundingBox().origin.y);
    }
    else{
        pos=Vec2(touch->getLocation().x, touch->getLocation().y-(parent->getPosition().y-parent->getBoundingBox().size.height/2));
    }
    
    auto touchStartPos=TouchManager::getInstance()->touchStartPos;
    
    //drag
    if (key.compare(0,4,CustomAction_DragSprite)==0) {
        if (SupplementsManager::getInstance()->dragSprites.size()>0) {
            auto index=atoi(key.substr(5,1).c_str());
            
            SupplementsManager::getInstance()->dragSprites.at(index)->drag(Vec2(pos.x/parent->getScale(), pos.y/parent->getScale()),Vec2(touchStartPos.x/parent->getScale(), touchStartPos.y/parent->getScale()),onItem, isInBegan);
        }
    }
    else if (key==CustomAction_PatternSprite) {
        //pattern
        if (onItem) {
            if (SupplementsManager::getInstance()->patternSprites_onItem.size()==0) {
                //新規作成
                SupplementsManager::getInstance()->addPatternSprite(parent, true, DataManager::sharedManager()->nowItemMap, ShowType_Normal);
            }
            SupplementsManager::getInstance()->patternSprites_onItem.at(0)->drag(pos,true);
        }
        else{
            if (SupplementsManager::getInstance()->patternSprites.size()==0) {
                //新規作成
                SupplementsManager::getInstance()->addPatternSprite(parent, false, DataManager::sharedManager()->nowBackMap, ShowType_Normal);
            }
            SupplementsManager::getInstance()->patternSprites.at(0)->drag(Vec2(pos.x/parent->getScale(), pos.y/parent->getScale()),false);
        }
    }
    else if(touchMap["useDrag"].asBool()){
        //drag,pattern以外のカスタムを使用
        SupplementsManager::getInstance()->getCustomActionManager()->dragAction(key, parent,pos);
    }
    else if (EscapeDataManager::getInstance()->getTypePlayTitleCondition()!=TitleCondition_TUTORIAL&&
             !onItem){
        //サイドバー表示処理
        auto area_start=.05;//タッチ開始範囲
        auto start_x=touchStartPos.x/parent->getScale();
        if (start_x>parent->getContentSize().width*(1-area_start)||
            start_x<parent->getContentSize().width*area_start) {
            auto warp_distance=.1;//遷移するとみなすスワイプ距離
            auto distance=abs(start_x-pos.x/parent->getScale());
            
            if (distance>parent->getContentSize().width*warp_distance&&
                !DataManager::sharedManager()->getIsShowingSideBar()) {
                resetTouchInfo();
                
                if (touchStartPos.x<0) {
                    return;
                }
                
                //遷移を実行
                SideBarType type=SideBarType_FromRight;
                if (start_x<parent->getContentSize().width*.5) {
                    type=SideBarType_FromLeft;
                }
                
                showSideBar(type);
            }
        }
    }
}

#pragma mark - SideBar
void TouchManager::showSideBar(SideBarType type)
{
    SupplementsManager::getInstance()->hidePanel();
    auto gameScene=((GameScene*)(Director::getInstance()->getRunningScene()->getChildren().at(1)));//0はCamera*が入ってるらしい

    auto layer=SideBarLayer::create(type,[this,gameScene]{

        SupplementsManager::getInstance()->showPanel(gameScene->mainSprite);
    });
    layer->setLocalZOrder(Zorder_TopMenu);
    gameScene->addChild(layer);
}

#pragma mark - Private
#pragma mark - disableAction
void TouchManager::disableAction(cocos2d::ValueMap map)
{
    //se
    if (!map["disableSE"].isNull()) {
        AudioManager::getInstance()->playSE(map["disableSE"].asString().c_str(), true);
    }
}

#pragma mark - タッチ非透過
LayerColor* TouchManager::createDisableLayer()
{
    auto layer=LayerColor::create(Color4B(0, 0, 0, 0));
    layer->setLocalZOrder(999);
    
    auto listner=EventListenerTouchOneByOne::create();
    listner->setSwallowTouches(true);//透過させない
    listner->onTouchBegan=[this](Touch*touch,Event*event)
    {
        return true;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listner, layer);
    
    return layer;
}
