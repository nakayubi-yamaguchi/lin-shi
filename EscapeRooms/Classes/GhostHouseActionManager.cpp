//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "GhostHouseActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"

using namespace cocos2d;

GhostHouseActionManager* GhostHouseActionManager::manager =NULL;

GhostHouseActionManager* GhostHouseActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new GhostHouseActionManager();
    }
    return manager;
}

#pragma mark - Back
void GhostHouseActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="pendulum"&&DataManager::sharedManager()->getEnableFlagWithFlagID(3))
    {//フリこ
        auto pendulum=parent->getChildByName(StringUtils::format("sup_%d_pendulum.png",backID));
        auto angle=10;
        if (backID==35) {
            angle=5;
        }
        //action
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<2; i++) {
            actions.pushBack(Spawn::create(swingAction(angle*pow(-1, i), .5, pendulum, .9),
                                           CallFunc::create([backID]{if(backID==36){
                Common::playSE("clock.mp3");
            }}), NULL));
        }

        std::vector<int>indexs={1,0,0,1,1,0};
        Vector<FiniteTimeAction*>actions_1;
        actions_1.pushBack(DelayTime::create(1));
        for (auto index : indexs) {
            actions_1.pushBack(actions.at(index)->clone());
        }
        actions_1.pushBack(DelayTime::create(1));
        
        parent->runAction(Repeat::create(Sequence::create(actions_1), UINT_MAX));
    }
    else if(key=="ghost"&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(6)&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(7)){//飛び出すおばけ
        auto ghost=parent->getChildByName("sup_17_ghost.png");

        auto delay=DelayTime::create(7);
        auto call_0=CallFunc::create([parent,this,ghost,backID]{
            //表示状態をチェック。
            if (DataManager::sharedManager()->getShowingItemID()>0||
                DataManager::sharedManager()->getIsShowingPromoteHint()||
                DataManager::sharedManager()->getIsShowingSideBar()) {
                this->backAction("ghost",backID, parent);
                log("表示状態エラー item:%d hint:%d side:%d",
                    DataManager::sharedManager()->getShowingItemID(),
                    DataManager::sharedManager()->getIsShowingPromoteHint(),
                    DataManager::sharedManager()->getIsShowingSideBar());
                return ;
            }
            
            Common::playSE("ghost.mp3");
            auto layer=SupplementsManager::getInstance()->createDisableLayer();
            Director::getInstance()->getRunningScene()->addChild(layer);
            
            auto fadeout=TargetedAction::create(ghost, FadeOut::create(2));
            auto scale_1=TargetedAction::create(ghost, ScaleBy::create(1, -1, 1));
            
            ccBezierConfig config;
            config.controlPoint_1 = Vec2(-parent->getContentSize().width*.1, parent->getContentSize().height*.05);
            config.controlPoint_2 = Vec2(-parent->getContentSize().width*.2, parent->getContentSize().height*.1);
            config.endPosition = Vec2(parent->getContentSize().width*.7, parent->getContentSize().height*.5);
            
            auto spawn=TargetedAction::create(ghost, Spawn::create(FadeTo::create(1, 150),
                                                                   createSoundAction("ghost.mp3"),
                                                                   EaseIn::create(BezierBy::create(4, config), 1.5),
                                                                   Sequence::create(DelayTime::create(1.5),
                                                                                    EaseIn::create(RotateBy::create(2.5, 20), 2), NULL),
                                                                   Repeat::create(Sequence::create(ScaleBy::create(1, 1.1,.95),
                                                                                                   ScaleBy::create(1, .95,1.1),
                                                                                                   NULL), 2)
                                                                   , NULL));
            
            auto call_1=CallFunc::create([layer]{
                Common::playSE("pinpon.mp3");
                layer->removeFromParent();
                DataManager::sharedManager()->setEnableFlag(7);
            });
            
            parent->runAction(Sequence::create(fadeout,scale_1,spawn,DelayTime::create(1),call_1, NULL));
        });
        
        parent->runAction(Sequence::create(delay,call_0, NULL));
    }
    else if(key=="skull"&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(19)&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(20)){//がいこつ
        skullAction(parent,backID, false, nullptr);
    }
    else if(key=="paper"){//切り絵
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(.5));
        std::vector<int>indexs={1,2,0,5,4,3};

        for (auto index:indexs) {
            auto paper=parent->getChildByName(StringUtils::format("sup_%d_%d.png",backID,index));
            
            if (DataManager::sharedManager()->isPlayingMinigame()&&index==4) {
                auto mistake=parent->getChildByName("sup_51_mistake.png");
                mistake->setAnchorPoint(paper->getAnchorPoint());
                mistake->setPosition(parent->getContentSize().width*mistake->getAnchorPoint().x, parent->getContentSize().height*mistake->getAnchorPoint().y);

                paper->removeFromParent();
                actions.pushBack(Spawn::create(swingAction(20, .3, mistake, 1.5, true, 1),
                                               createSoundAction("pig_1.mp3"), NULL));
            }
            else{
                actions.pushBack(Spawn::create(swingAction(20, .3, paper, 1.5, true, 1),
                                               createSoundAction("ghost.mp3"), NULL));
            }
            
        }
        
        actions.pushBack(DelayTime::create(.5));
        
        parent->runAction(Repeat::create(Sequence::create(actions),UINT_MAX));
    }
    else if(key=="photo"&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(34)&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(35)){//写真
        photoAction(parent,backID, false, nullptr);
    }
    else if(key=="fire"&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(42)){//暖炉
        parent->addChild(getFireParticle(parent,backID));
    }
    else if(key=="bed"&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(46)){
        Vector<FiniteTimeAction*>actions;
        auto height=parent->getContentSize().height*.05;

        for (int i=0; i<4; i++) {
            auto ghost=createSpriteToCenter("sup_17_ghost.png", true, parent,true);
            auto alpha=125;
            if (i!=0) {
                alpha=15;
            }
            
            auto seq_ghost=TargetedAction::create(ghost,Sequence::create(DelayTime::create(.15*i),
                                                                         Spawn::create(FadeTo::create(1, alpha),
                                                                                       CallFunc::create([i]{
                                                                             if (i==0) {
                                                                                 Common::playSE("ghost.mp3");
                                                                             }
                                                                             ;}),
                                                                                       swingActionForever(Vec2(0, height), 1, ghost, 1.25), NULL),
                                                                         NULL));
            
            actions.pushBack(seq_ghost);
        }
       
        
        parent->runAction(Spawn::create(actions));
    }
    else if(key=="exit"&&DataManager::sharedManager()->isPlayingMinigame()){
        auto ghost=parent->getChildByName("sup_8_mistake.png");
        auto distance=parent->getContentSize().width*.3;
        ghost->setPositionX(ghost->getPositionX()+distance);
        auto delay=DelayTime::create(.5);
        
        ccBezierConfig config;
        config.controlPoint_1=Vec2(-distance/3, -parent->getContentSize().height*.1);
        config.controlPoint_2=Vec2(-distance/3*2, parent->getContentSize().height*.2);
        config.endPosition=Vec2(-distance, 0);

        auto seq=TargetedAction::create(ghost, Sequence::create(createSoundAction("ghost.mp3"),
                                                                FadeIn::create(1),
                                                                EaseOut::create(BezierBy::create(2, config), 2),
                                                                 NULL));
        auto call=CallFunc::create([]{
            DataManager::sharedManager()->setTemporaryFlag("mistake_8", 1);
        });
        auto swing=swingActionForever(Vec2(0, parent->getContentSize().height*.025), 2, ghost, 1.5);
        parent->runAction(Sequence::create(delay,seq,call,swing, NULL));
    }
    else if(key=="doll"&&DataManager::sharedManager()->isPlayingMinigame()){
        auto doll=parent->getChildByName("sup_16_mistake.png");
        auto distance=parent->getContentSize().width*.1;
        doll->setPositionX(doll->getPositionX()+distance);
        auto delay=DelayTime::create(.5);

        auto seq=TargetedAction::create(doll, Sequence::create(
                                                                FadeIn::create(.5),
                                                                JumpBy::create(.5, Vec2(-distance, 0), parent->getContentSize().height*.05, 1),
                                                               createSoundAction("pop_1.mp3"),
                                                                NULL));
        auto call=CallFunc::create([]{
            DataManager::sharedManager()->setTemporaryFlag("mistake_16", 1);
        });
        parent->runAction(Sequence::create(delay,seq,call, NULL));
    }
}

#pragma mark - Touch
void GhostHouseActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="fireplace")
    {//暖炉open
        auto back=createSpriteToCenter("back_18.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 18);
        auto fence=back->getChildByName<Sprite*>("sup_18_fence.png");
        

        //fadein
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto slide=Spawn::create(TargetedAction::create(fence, EaseInOut::create(MoveBy::create(3, Vec2(0, -parent->getContentSize().height*.5)), 1.5)),
                                  createSoundAction("gii.mp3"), NULL);
        auto fadeout=TargetedAction::create(back, FadeOut::create(1));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein,delay,slide,delay->clone(),fadeout,delay->clone(),call,NULL));
    }
    else if(key=="wrench"){
        Common::playSE("p.mp3");
        //ボルト外し
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<2; i++) {
            Vec2 vec=Vec2(.133, .439);
            if (i==1) {
                vec=Vec2(.883, .439);
            }
            auto wrench=AnchorSprite::create(vec, parent->getContentSize(),true, StringUtils::format("sup_29_anime_%d.png",i));
            parent->addChild(wrench);
            auto bolt=parent->getChildByName(StringUtils::format("sup_29_bolt_%d.png",i));
            
            auto fadein=TargetedAction::create(wrench, FadeIn::create(1));
            auto angle=20*pow(-1, i);
            auto seq=TargetedAction::create(wrench, Sequence::create(EaseInOut::create(RotateBy::create(.5, angle), 1.5),
                                                                     createSoundAction("kacha_1.mp3"),
                                                                     Spawn::create(EaseInOut::create(RotateBy::create(.5, angle), 1.5),
                                                                                   TargetedAction::create(bolt, FadeOut::create(.5)), NULL),
                                                                     DelayTime::create(.3),
                                                                     FadeOut::create(.5), NULL));
            
            actions.pushBack(Sequence::create(fadein,seq, NULL));
        }
        
        auto board=parent->getChildByName("sup_29_board.png");
        actions.pushBack(createSoundAction("goto.mp3"));
        actions.pushBack(TargetedAction::create(board, FadeOut::create(.5)));
        actions.pushBack(DelayTime::create(1));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="hammer"){
        Common::playSE("p.mp3");
        
        auto hammer=AnchorSprite::create(Vec2(.1, .1), parent->getContentSize(),true, "sup_23_hammer.png");
        parent->addChild(hammer);
        auto B=parent->getChildByName("sup_23_crackB.png");
        
        auto fadein=TargetedAction::create(hammer, FadeIn::create(1));
        
        auto smoke=CallFunc::create([parent]{
            auto size=parent->getContentSize().width*.1;
            
            auto particle=ParticleSystemQuad::create("smoke.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setTotalParticles(100);
            particle->setPosition(parent->getContentSize().width*(.283),parent->getContentSize().height*.565);
            particle->setPosVar(Vec2(parent->getContentSize().width*.1,parent->getContentSize().height*.1));
            particle->setLife(2);
            particle->setDuration(.5);
            particle->resetSystem();
            particle->setStartSize(size);
            particle->setStartSizeVar(size*.5);
            particle->setEndSize(size*2);
            particle->setStartColor(Color4F(1, 1, 1, .6));
            particle->setSpeed(size*.5);
            particle->setGravity(Vec2(0, size*1));
            particle->setEndSpin(30);
            particle->setEndSpinVar(30);
            particle->setAngle(90);
            particle->setAngleVar(270);
            parent->addChild(particle);
        });
        
        auto angle=-25;
        auto scale=hammer->getScale();
        auto seq=TargetedAction::create(hammer, Sequence::create(Spawn::create(
                                                                               RotateBy::create(1, angle), NULL),
                                                                 DelayTime::create(1),
                                                                 Spawn::create(ScaleBy::create(.3, .8,1),
                                                                               RotateBy::create(.3, -angle*1.5),
                                                                               createSoundAction("swing_0.mp3"),
                                                                               NULL),
                                                                 createSoundAction("hit.mp3"),
                                                                 Spawn::create(ScaleTo::create(.3, scale),
                                                                               RotateBy::create(.3, angle),
                                                                               FadeOut::create(.3),
                                                                                NULL),
                                                                 smoke,
                                                                 TargetedAction::create(B, Spawn::create(JumpTo::create(2, Vec2(parent->getContentSize().width*1.25, 0), parent->getContentSize().height*.75, 1),
                                                                                                         RotateBy::create(2, 360*6),
                                                                                                         createSoundAction("rolling.mp3"),
                                                                                                         NULL)), NULL));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,seq,call,NULL));
    }
    else if(key=="shake_b"){
        auto crack=parent->getChildByName("sup_23_crackB.png");
        
        auto shake=TargetedAction::create(crack, Spawn::create(swingAction(10, .1, crack, 1.5, true),
                                                               createSoundAction("paki.mp3"),
                                                               NULL));
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(shake,call, NULL));
    }
    else if(key.compare(0,5,"skull")==0){
        //骸骨をおく
        Common::playSE("koto.mp3");
        auto index=atoi(key.substr(6,1).c_str());
        
        auto skull=createSpriteToCenter(StringUtils::format("sup_25_%d_1.png",index), true, parent, true);
        
        Vector<FiniteTimeAction*>actions;
        
        auto fadein_skull=TargetedAction::create(skull, Sequence::create(FadeIn::create(1),
                                                                          NULL));
        actions.pushBack(fadein_skull);

        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        
        auto call=CallFunc::create([callback,index,this,parent]{
            if ((index==0&&DataManager::sharedManager()->getEnableFlagWithFlagID(20))||
                 (index!=0&&DataManager::sharedManager()->getEnableFlagWithFlagID(19))) {
                this->skullAction(parent,DataManager::sharedManager()->getNowBack(),true, callback);
            }
            else{
                Common::playSE("pinpon.mp3");
                callback(true);
            }
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="knife"){
        Common::playSE("p.mp3");
        auto knife=createSpriteToCenter("sup_13_knife.png", true, parent,true);
        auto fadein=TargetedAction::create(knife, FadeIn::create(1));
        
        auto key=parent->getChildByName(StringUtils::format("sup_13_key.png"));
        
        auto seq=TargetedAction::create(knife, Sequence::create(DelayTime::create(.7),
                                                                createSoundAction("sword.mp3"),
                                                                EaseIn::create(MoveBy::create(.3, Vec2(-parent->getContentSize().width*.5, 0)), 2),
                                                                FadeOut::create(1),
                                                                NULL));
        auto spawn=Spawn::create(TargetedAction::create(key, FadeOut::create(.5))
                                 , NULL);
        auto call=CallFunc::create([callback](){
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,seq,spawn,DelayTime::create(1),call, NULL));
    }
    else if(key.compare(0,7,"picture")==0)
    {//かたかた揺らす
        Common::playSE("kacha.mp3");
        
        auto index=atoi(key.substr(8,1).c_str());
        
        auto picture=SupplementsManager::getInstance()->switchSprites.at(0);
        auto swing=swingAction(20*pow(-1, index), .1, picture, 2);
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(swing,call, NULL));
    }
    else if (key=="clock"){
        //とけい正解後
        auto back=createSpriteToCenter("back_27.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 27);
        auto hint=createSpriteToCenter("sup_27_softcream.png", true, back,true);
        
        auto delay=DelayTime::create(1);
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto fadein_hint=TargetedAction::create(hint, Spawn::create(FadeIn::create(1),
                                                                    createSoundAction("robot_start.mp3"), NULL));
        auto fadeout_back=TargetedAction::create(back, FadeOut::create(1));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(delay,fadein_back,delay->clone(),fadein_hint,delay->clone(),fadeout_back,delay->clone(),call, NULL));
    }
    else if(key.compare(0,5,"spray")==0){
        //噴射
        Common::playSE("p.mp3");
        Vector<FiniteTimeAction*>actions;
        
        auto backNum=DataManager::sharedManager()->getNowBack();
        
        //
        auto spray=createSpriteToCenter(StringUtils::format("sup_%d_spray.png",backNum), true, parent,true);
        auto seq_spray=TargetedAction::create(spray, Sequence::create(FadeIn::create(1),
                                                                      jumpAction(parent->getContentSize().height*.05, .2, spray, 1.5, 2),
                                                                          NULL));
        actions.pushBack(seq_spray);
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        //
        auto jet=CallFunc::create([parent]{
            Common::playSE("spray.mp3");
            auto size=parent->getContentSize().width*.05;
            
            auto particle=ParticleSystemQuad::create("smoke.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setTotalParticles(600);
            particle->setPosition(parent->getContentSize().width*(.72),parent->getContentSize().height*.62);
            particle->setPosVar(Vec2(0,0));
            particle->setLife(2.5);
            particle->setDuration(1);
            particle->resetSystem();
            particle->setStartSize(size);
            particle->setStartSizeVar(size*.5);
            particle->setEndSize(size*2);
            particle->setStartColor(Color4F(1, 1, 1, .5));
            particle->setSpeed(size*6);
            particle->setSpeedVar(size*2.5);
            particle->setGravity(Vec2(size*3, -size*1));
            particle->setEndSpin(150);
            particle->setEndSpinVar(30);
            particle->setAngle(180);
            particle->setAngleVar(30);
            parent->addChild(particle);
        });
        actions.pushBack(jet);
        
        auto delay_1=DelayTime::create(3);
        actions.pushBack(delay_1);
        
        if (backNum==21) {
            auto spider=parent->getChildByName("sup_21_spider.png");
            actions.pushBack(Spawn::create(TargetedAction::create(spider, FadeOut::create(1)),
                                            NULL));
        }
        else{
            Vector<FiniteTimeAction*>spawns;
            for (auto v : parent->getChildren()) {
                if (v->getName()=="sup_17_ghost.png") {
                    auto spawn=Spawn::create(TargetedAction::create(v, FadeOut::create(1)),
                                                   CallFunc::create([spawns]{
                        if (spawns.size()==0) {
                            Common::playSE("ghost.mp3");
                        }
                    }),
                                                   NULL);
                    spawns.pushBack(spawn);
                }
            }
            actions.pushBack(Spawn::create(spawns));
        }
        
        actions.pushBack(TargetedAction::create(spray, FadeOut::create(1)));
        actions.pushBack(delay->clone());
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key.compare(0,5,"photo")==0){
        Common::playSE("p.mp3");
        auto backNum=DataManager::sharedManager()->getNowBack();
        auto index=atoi(key.substr(6,1).c_str());
        auto photo=createSpriteToCenter(StringUtils::format("sup_%d_photo_%d.png",backNum,index), true, parent,true);

        auto seq_photo=TargetedAction::create(photo, Sequence::create(FadeIn::create(1),
                                                                      createSoundAction("kacha.mp3"),
                                                                    DelayTime::create(.7),
                                                                    NULL));
        
        auto call=CallFunc::create([callback,index,this,parent,backNum]{
            if ((index==0&&DataManager::sharedManager()->getEnableFlagWithFlagID(35))||
                (index!=0&&DataManager::sharedManager()->getEnableFlagWithFlagID(34))) {
                this->photoAction(parent,backNum,true, callback);
            }
            else{
                Common::playSE("pinpon.mp3");
                callback(true);
            }
        });
        
        parent->runAction(Sequence::create(seq_photo,DelayTime::create(1),call, NULL));
    }
    else if(key=="train"){
        auto back=createSpriteToCenter("back_46.png", true, parent,true);
        
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto train_0=createSpriteToCenter("sup_46_train_0.png", true, back,true);
        auto train_1=createSpriteToCenter("sup_46_train_1.png", true, back,true);
        createSpriteToCenter("sup_46_match.png", false, train_1,true);

        auto fadein_0=TargetedAction::create(train_0, Spawn::create(FadeIn::create(1),
                                                                       createSoundAction("train.mp3"), NULL));
        
        auto fadein_1=createChangeAction(1, .7, train_0, train_1);
        
        auto delay=DelayTime::create(1);
        auto fadeout=TargetedAction::create(back, FadeOut::create(1));
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_back,delay,fadein_0,delay->clone(),fadein_1,delay->clone(),fadeout,delay->clone(),call, NULL));
    }
    else if(key=="match"){
        Common::playSE("p.mp3");
        
        auto match=createSpriteToCenter("sup_18_match.png", true, parent,true);
        
        auto seq_match=TargetedAction::create(match, Sequence::create(FadeIn::create(1),
                                                                      Repeat::create(swingAction(Vec2(0, parent->getContentSize().height*.02), .1, match, 1.51, false), 2),
                                                                      DelayTime::create(.3),
                                                                      CallFunc::create([this,parent]{
            Common::playSE("huo.mp3");
            auto particle=getFireParticle(parent,DataManager::sharedManager()->getNowBack());
            parent->addChild(particle);
        }),
                                                                      FadeOut::create(1),
                                                                      NULL));
        
        auto delay=DelayTime::create(1);
        
        //
        auto back=createSpriteToCenter("back_11.png", true, parent,true);
        back->setLocalZOrder(1);
        SupplementsManager::getInstance()->addInputRotateSupplement(back, false, DataManager::sharedManager()->getBackData(11), ShowType_Normal);
        
        auto paper=AnchorSprite::create(Vec2(.5, .2), back->getContentSize(), true,"sup_11_hint.png");
        back->addChild(paper);
        
        auto pos_paper=paper->getPosition();
        paper->setPositionY(pos_paper.y+parent->getContentSize().height*.48);

        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto spawn_paper=TargetedAction::create(paper, Spawn::create(FadeIn::create(1),
                                                                      swingAction(20, .3, paper, 1.5, true,2),
                                                                     createSoundAction("pop.mp3"),
                                                                     MoveTo::create(2.4, pos_paper),
                                                                      NULL));
        auto fadeout_back=TargetedAction::create(back, FadeOut::create(1));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_match,delay,fadein_back,delay->clone(),spawn_paper,delay->clone(),fadeout_back,delay->clone(),call, NULL));
    }
    else if(key=="screw"){
        Common::playSE("p.mp3");
        
        auto anim_0=createSpriteToCenter("sup_37_0.png", true, parent,true);
        auto anim_1=createSpriteToCenter("sup_37_1.png", true, parent,true);

        auto fadein_0=TargetedAction::create(anim_0, Spawn::create(FadeIn::create(1), NULL));
        auto changes=Sequence::create(createSoundAction("gigigi.mp3"),
                                      createChangeAction(.7, .5, anim_0, anim_1),
                                      DelayTime::create(.3),
                                      
                                      createChangeAction(.7, .5, anim_1, anim_0),
                                      DelayTime::create(.3), NULL);
        
        auto back=createSpriteToCenter("back_36.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 36,true);
        //SupplementsManager::getInstance()->addAnchorSpritesToMain(back, 36);
        
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto delay=DelayTime::create(1);

        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_0,Repeat::create(changes, 2),fadein_back,delay,call, NULL));
    }
    else if(key=="baby"){
            auto story=StoryLayer::create("cantget", [callback]{
                callback(false);
            });
            Director::getInstance()->getRunningScene()->addChild(story);
    }
    else if (key=="getbaby"){
        auto baby=parent->getChildByName("sup_44_baby.png");
        auto fadeout_baby=TargetedAction::create(baby, FadeOut::create(1));
        auto delay=DelayTime::create(1);
        auto call=CallFunc::create([callback,parent]{
            auto story=StoryLayer::create("getbaby", [callback,parent]{
                parent->runAction(Sequence::create(DelayTime::create(.7),
                                                   CallFunc::create([callback]{
                    Common::playSE("pinpon.mp3");
                    callback(true);}),
                                                   NULL));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        parent->runAction(Sequence::create(fadeout_baby,delay,call, NULL));
    }
    else if(key=="cantopen"){

        auto addStory=CallFunc::create([parent,callback]{
            auto story=StoryLayer::create("cantopen", [callback]{
                callback(true);
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        parent->runAction(addStory);
    }
    else if(key=="clear"){
        Common::playSE("gacha.mp3");
        createSpriteToCenter("sup_8_open.png", false, parent,true);
        
        auto delay=DelayTime::create(1);
        auto addStory=CallFunc::create([parent]{
            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

            auto story=StoryLayer::create("ending", [parent]{
                
                auto call=CallFunc::create([parent](){
                    Director::getInstance()->replaceScene(TransitionFade::create(8, ClearScene::createScene(), Color3B::WHITE));
                });
                
                parent->runAction(Sequence::create(call, NULL));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        parent->runAction(Sequence::create(delay,addStory, NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void GhostHouseActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造
    if (key=="key") {//鍵の組み合わせ
        Common::playSE("p.mp3");
        auto key_head=createSpriteOnClip("key_head_anim.png", true, parent,true);
        //
        auto key=createSpriteOnClip("key.png", true, parent, true);
        
        auto change=createChangeAction(1, .7, parent->itemImage, key_head);
        auto delay=DelayTime::create(1);
        auto get_key=TargetedAction::create(key, FadeIn::create(1));
        auto sound=createSoundAction("kacha.mp3");
        auto fadeout_head=TargetedAction::create(key_head, FadeOut::create(1));
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(change,delay,get_key,sound,fadeout_head,call, NULL));
    }
    else if(key=="device")
    {
        auto box=parent->itemImage;
        auto open=createSpriteOnClip("box_open.png", true, parent, true);

        auto delay=DelayTime::create(1);
        auto change=Sequence::create(createChangeAction(1, .7, box, open),
                                     createSoundAction("kacha.mp3"), NULL);

        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(delay,change,delay->clone(),call, NULL));
    }
}

#pragma mark - Clear
void GhostHouseActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    if (key == CustomAction_Clear) {

        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<4; i++) {
            auto ghost=createSpriteToCenter(StringUtils::format("room_image_ghost_%d.png",i), true, parent,true);
            auto fades=Repeat::create(Sequence::create(FadeIn::create(2),DelayTime::create(2),FadeOut::create(2),DelayTime::create(1), NULL), UINT_MAX);
            auto seq=TargetedAction::create(ghost, Sequence::create(DelayTime::create(1*i+1),
                                                                    Spawn::create(swingActionForever(Vec2(0, parent->getContentSize().height*.1), 2, ghost, 1.5),
                                                                                  fades, NULL)
                                                                    , NULL));
            actions.pushBack(seq);
        }
        
        parent->runAction(Spawn::create(actions));
    }
}

#pragma mark - Private
void GhostHouseActionManager::skullAction(cocos2d::Node *parent,int backID, bool animate, const onFinished &callback)
{//目を
    Vector<FiniteTimeAction*>actions;
    for (int i=0; i<5; i++) {
        auto eye=createSpriteToCenter(StringUtils::format("sup_%d_%d_2.png",backID,i), animate, parent,true);
        actions.pushBack(TargetedAction::create(eye, FadeIn::create(1)));
    }
    
    if (animate) {
        Common::playSE("bones.mp3");
        parent->runAction(Sequence::create(Spawn::create(actions),DelayTime::create(1),CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        }), NULL));
    }
    else{
        if (callback) {
            callback(true);
        }
    }
}

void GhostHouseActionManager::photoAction(cocos2d::Node *parent,int backID, bool animate, const onFinished &callback)
{//写真
    auto color=createSpriteToCenter(StringUtils::format("sup_%d_color.png",backID), animate, parent,true);
    
    if (animate) {
        Common::playSE("ghost.mp3");
        parent->runAction(Sequence::create(TargetedAction::create(color, FadeIn::create(1)),DelayTime::create(1),CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        }), NULL));
    }
    else{
        if (callback) {
            callback(true);
        }
    }
}

ParticleSystemQuad* GhostHouseActionManager::getFireParticle(cocos2d::Node *parent,int backID)
{
    auto particle=ParticleSystemQuad::create("fire.plist");
    particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
    particle->setAutoRemoveOnFinish(true);
    particle->setPosition(parent->getContentSize().width*.48, parent->getContentSize().height*.5);
    particle->setTotalParticles(150);
    particle->setStartColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 150));
    particle->setPosVar(Vec2(parent->getContentSize().width*.075, parent->getContentSize().height*.05));
    particle->setStartSize(parent->getContentSize().width*.075);
    particle->setSpeed(parent->getContentSize().height*.08);
    
    if(backID==2){
        particle->setPosition(parent->getContentSize().width/2, parent->getContentSize().height*.37);
        particle->setPosVar(Vec2(parent->getContentSize().width*.025, 0));
        particle->setStartSize(parent->getContentSize().width*.04);
        particle->setSpeed(parent->getContentSize().height*.025);
        particle->setSpeedVar(particle->getSpeed()/3);
        particle->setTotalParticles(30);
    }
    
    return particle;
}
