//
//  ChangingSprites.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/12/13.
//
//

#include "ChangingSprite.h"
#include "DataManager.h"
#include "SupplementsManager.h"

using namespace cocos2d;

ChangingSprite* ChangingSprite::create(std::string name,ValueMap map,Value answerV, int number)
{
    auto node=new ChangingSprite();
    if (node&&node->init(name,map,answerV,number)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    
    return node;
}

bool ChangingSprite::init(std::string name, ValueMap map, Value answerV, int number)
{
    auto indexs=map["indexs"].asValueVector();
    auto defIndex=indexs.at(0).asInt();

    auto _name=StringUtils::format("%s_%d_%d.png",name.c_str(),number,defIndex);
    if (!EscapeStageSprite::init(_name)) {
        return false;
    }
    setName(_name);
    setNameCreated(name);
    setNumber(number);//何番目のspriteか
    setIndexs(indexs);
    setIndex(defIndex);
    //setIndexPosition(0);
    setStoped(true);
    setCascadeOpacityEnabled(false);
    setDeflection(map["deflection"].asFloat());
    
    //表示していいか
    auto needFlag=map["needFlagID"].asInt();
    if (!DataManager::sharedManager()->getEnableFlagWithFlagID(needFlag)&&needFlag>0) {
        setDisabled(true);
        setOpacity(0);
    }
    
    //secure
    if (!answerV.isNull()) {
        if (answerV.asValueMap()["secure"].asBool()) {
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(answerV.asValueMap()["enFlagID"].asInt())) {
                auto answer=answerV.asValueMap()["answer"].asString();
                
                auto _index=atoi(answer.substr(number,1).c_str());
                setIndex(_index);
                setDisabled(true);
            setSpriteFrame(EscapeStageSprite::createWithSpriteFileName(StringUtils::format("%s_%d_%d.png",name.c_str(),number,_index))->getSpriteFrame());
            }
        }
    }
    
    return true;
}

#pragma mark Animate
void ChangingSprite::startAnimate()
{
    if (!getDisabled()) {
        setStoped(false);

        Vector<FiniteTimeAction*>actions;
        
        for (int i=0; i<getIndexs().size(); i++) {
            auto nextIndex=(getIndexs().at((getIndex()+1+i)%getIndexs().size())).asInt();
            auto nextName=StringUtils::format("%s_%d_%d.png",getNameCreated().c_str(),getNumber(),nextIndex);
            auto nextSpr=getChildByName<Sprite*>(nextName);
            if (!nextSpr) {
                nextSpr=SupplementsManager::getInstance()->addSupplementToCenter(this, nextName,true,true);
            }
            
            //delay
            auto duration_fadein=getDuration()*.7;
            actions.pushBack(DelayTime::create(getDelay()));
            
            //setup この時点でindexは変更となる
            actions.pushBack(CallFunc::create([this,nextName,nextIndex]{
                this->setChildSprName(nextName);
                this->setIndex(nextIndex);
            }));
            
            //change
            actions.pushBack(Spawn::create(TargetedAction::create(nextSpr, FadeIn::create(duration_fadein)),
                                           TargetedAction::create(this, FadeOut::create(getDuration())), NULL));
            
            //apply to this
            actions.pushBack(CallFunc::create([this,nextName,nextSpr]{
                this->setSpriteFrame(nextSpr->getSpriteFrame());
                this->setOpacity(255);
                nextSpr->setOpacity(0);
            }));
            
        }
        
        
        runAction(Repeat::create(Sequence::create(actions), -1));
    }
}

void ChangingSprite::stopAnimate(ccMenuCallback callback)
{
    if (getStoped()) {
        callback(NULL);
        return;
    }
    
    stopAllActions();
    setStoped(true);
    
    Vector<FiniteTimeAction*>actions;
    auto duration=.3;
    
    Common::playSE(getPlaySE().c_str());
    
    auto child=getChildByName<Sprite*>(getChildSprName());
    if (child) {
        child->setOpacity(0);
        this->setSpriteFrame(child->getSpriteFrame());
    }
    

    if (getDeflection()>0) {
        auto scale=getScale();
        actions.pushBack(Spawn::create(Sequence::create(ScaleTo::create(duration*.33, scale*1.05,scale*.95),
                                                        ScaleTo::create(duration*.33, scale*.95,scale*1.025),
                                                        ScaleTo::create(duration*.33, scale), NULL),
                                       FadeIn::create(duration),
                                       NULL));
    }
    else{
        actions.pushBack(FadeIn::create(duration));
    }
    
    actions.pushBack(CallFunc::create([callback]{
        callback(NULL);
    }));
    
    runAction(Sequence::create(actions));
}
