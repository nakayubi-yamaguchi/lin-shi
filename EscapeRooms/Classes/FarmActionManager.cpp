//
//  SchoolFesActionManager.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2018/11/29.
//
//

#include "FarmActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "GameScene.h"
#include "TouchManager.h"

using namespace cocos2d;

FarmActionManager* FarmActionManager::manager =NULL;

FarmActionManager* FarmActionManager::getInstance()
{
    if (manager==NULL) {
        manager = new FarmActionManager();
    }
        
    return manager;
}

#pragma mark - 背景
int FarmActionManager::getDefaultBackNum()
{
    /*
    if(!DataManager::sharedManager()->isShowedOpeningStory()) {
        return 0;
    }*/
    
    int backNum = 1;
    
    Common::performProcessForDebug([&backNum](){

        backNum = 1;
    }, nullptr);
    
    return backNum;
}

#pragma mark - Back
void FarmActionManager::backAction(std::string key, int backID, Node *parent, ShowType type)
{
#pragma mark にわとり餌食べる
    if(key == "bird" && DataManager::sharedManager()->getEnableFlagWithFlagID(4)) {
        parent->runAction(birdsAction(backID, parent, type));
    }
#pragma mark 風車
    else if(key == "windmill") {
        std::vector<int> answer = {0, 1, 1};
        std::vector<Vec2> poses = {Vec2(.0817, .629), Vec2(.182, .629), Vec2(.282, .629)};
        if (backID == 60) {
            answer = {0, 0, 1, 1};
            poses = {Vec2(.515, .627), Vec2(.615, .629), Vec2(.715, .629), Vec2(.813, .629)};
        }
        
        for (int i = 0; i < answer.size(); i++) {
            if (answer.at(i) == 0) {
                continue;
            }
            
            auto windmillSp = parent->getChildByName<Sprite*>(StringUtils::format("sup_%d_%s_%d.png", backID, key.c_str(), i));
            transformToAnchorSprite(parent, windmillSp, poses.at(i));
            windmillSp->runAction(Repeat::create(RotateBy::create(2, 360), -1));
        }
    }
}

#pragma mark - Touch
void FarmActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    Vector<FiniteTimeAction*> actions;
    //コールバック
    auto call = CallFunc::create([callback]{
        callback(true);
    });
    
    log("タッチキーは%s",key.c_str());

#pragma mark 吹き出し（クラウド）
    if(key.substr(0, 5) == "cloud") {
        
        int num = DataManager::sharedManager()->getNowBack();
        if (num == 31) {
            actions.pushBack(cloudBounceAction(parent, parent->getChildByName("sup_31_pig.png"), "sup_31_cloud.png", Vec2(.435, .66), "pig.mp3"));
        }
        
        actions.pushBack(call);
    }
#pragma mark - アイテム使用系
    else if (key.substr(0, 4) == "use_") {
        
        auto key_split = Common::splitString(key, "_");
        auto item = key_split.at(1);//key.substr(4);
        
        log("アイテムをあげる%s",item.c_str());
        actions.pushBack(useItemSoundAction());
#pragma mark にわとりに餌使用
        if (item == "food") {
            auto anim_food = createAnchorSprite("sup_93_food.png", .628, .67, true, parent, true);
            anim_food->setLocalZOrder(2);
            actions.pushBack(TargetedAction::create(anim_food, FadeIn::create(1)));
            actions.pushBack(DelayTime::create(.6));
            
            std::vector<Vec2> poses = {Vec2(.16, .517), Vec2(.398, .517), Vec2(.628, .517), Vec2(.858, .517)};
            int i = 0;
            for (auto pos : poses) {
                actions.pushBack(TargetedAction::create(anim_food, EaseOut::create(MoveTo::create(.7, Vec2(parent->getContentSize().width*pos.x, parent->getContentSize().height*pos.y)), 1.5)));
                actions.pushBack(DelayTime::create(.2));
                
                //ドッグフードそそぐ
                auto angle = 20;
                actions.pushBack(createSoundAction("gasagoso.mp3"));
                actions.pushBack(TargetedAction::create(anim_food, EaseInOut::create(RotateBy::create(.7, angle), 1.5)));
                actions.pushBack(jumpAction(parent->getContentSize().height*.05, .15, anim_food, .7, 2));
                
                //餌を落とす
                auto food = createSpriteToCenter(StringUtils::format("sup_93_food_%d.png", i), true, parent, true);
                actions.pushBack(TargetedAction::create(food, FadeIn::create(.2)));
                
                actions.pushBack(TargetedAction::create(anim_food, EaseInOut::create(RotateBy::create(.7, -angle), 1.5)));
                
                i++;
            }
            
            //上に行きながら消える
            auto fadeout_food = TargetedAction::create(anim_food, Spawn::create(FadeOut::create(1),
                                                                           MoveBy::create(1, Vec2(0, parent->getContentSize().height*.05)), NULL));
            actions.pushBack(fadeout_food);
        }
        
        //完了
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark - 指定無し
    else{
        actions.pushBack(call);
    }
    parent->runAction(Sequence::create(actions));
}

#pragma mark - ヒント機能のAnswer
void FarmActionManager::showAnswerAction(std::string key, Node *parent, ShowType showType)
{
}

#pragma mark - touch began
void FarmActionManager::touchBeganAction(std::string key, Node *parent)
{
}


#pragma mark - Drag
void FarmActionManager::dragAction(std::string key, Node *parent, Vec2 pos)
{
    
}

#pragma mark - Item
void FarmActionManager::itemBackAction(std::string key, PopUpLayer *parent)
{
}

void FarmActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造 addする際はcreateSpriteOnClipを使用するとよい
    Vector<FiniteTimeAction*>actions;
    actions.pushBack(useItemSoundAction());
    
#pragma mark スクラッチカードを削る
    if (key == "scratchcard") {
        
    }
    
    //call
    auto call=CallFunc::create([callback](){
        callback(true);
    });
    actions.pushBack(call);
    
    parent->runAction(Sequence::create(actions));
}

#pragma mark - Custom
void FarmActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    
   
}

#pragma mark - Story
void FarmActionManager::storyAction(std::string key,Node*parent,const onFinished& callback)
{
  
    callback(true);
};


#pragma mark - Camera Shot
void FarmActionManager::cameraShotAction(int backID)
{
}

#pragma mark - Arrow Action
void FarmActionManager::arrowAction(std::string key, Node *parent, const onFinished &callback)
{
    callback(true);
}

#pragma mark - Private
FiniteTimeAction* FarmActionManager::birdsAction(int backID, Node *parent, ShowType type)
{
    Vector<Sprite*> beforeBirds;
    Vector<Sprite*> afterBirds;
    for (int i = 0; i < 4; i++) {
        auto before = StringUtils::format("sup_%d_%d_1.png", backID, i);
        beforeBirds.pushBack(parent->getChildByName<Sprite*>(before));
        auto after = StringUtils::format("sup_%d_%d_2.png", backID, i);
        afterBirds.pushBack(createSpriteToCenter(after, true, parent, true));
    }
    
    std::vector<int> answer = {0, 1, 2, 3, 0, 2, 3};
    Vector<FiniteTimeAction*> actions;
    actions.pushBack(DelayTime::create(.7));
    for (int num : answer) {
        auto beforeBird = beforeBirds.at(num);
        auto afterBird = afterBirds.at(num);
        
        actions.pushBack(createChangeAction(.5, .5, beforeBird, afterBird, "chicken.mp3", type));
        actions.pushBack(DelayTime::create(.8));
        actions.pushBack(createChangeAction(.4, .4, afterBird, beforeBird));
        actions.pushBack(DelayTime::create(.6));
    }
    actions.pushBack(DelayTime::create(1));
    
    return Repeat::create(Sequence::create(actions), -1);
}
