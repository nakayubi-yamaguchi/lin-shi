//
//  OrderSprite.cpp
//  Wedding
//
//  Created by 成田凌平 on 2017/04/27.
//
//

#include "OrderSprite.h"
#include "Utils/Common.h"
#include "DataManager.h"

using namespace cocos2d;

OrderSprite* OrderSprite::create(cocos2d::ValueMap map, cocos2d::Value answerV, std::string name, std::string temporaryKey, int number, cocos2d::Size parentSize,ValueVector setItemVec)
{
    auto node=new OrderSprite();
    if (node&&node->init(map,answerV,name,temporaryKey,number,parentSize,setItemVec)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    
    return node;
}

bool OrderSprite::init(cocos2d::ValueMap map, cocos2d::Value answerV, std::string name, std::string temporaryKey, int number, cocos2d::Size parentSize,ValueVector setItemVec)
{
    auto indexMap=map["index"].asValueMap();
    auto index=indexMap["default"].asInt();
    

    if(!indexMap["withFlag"].isNull())
    {//フラグが立っていたらの指定があればこちらを優先
        auto withFlagMap=indexMap["withFlag"].asValueMap();
        auto targetFlag=withFlagMap["flag"].asFloat();
        log("targetFlag %f",targetFlag);
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(targetFlag)) {
            if (!withFlagMap["index"].isNull()) {
                //indexの指定がある
                index=withFlagMap["index"].asInt();
            }
            else{
                //index指定がないとき、setItemから取得する
                for (auto v:setItemVec) {
                    auto setItemMap=v.asValueMap();
                    if (setItemMap["enFlagID"].asFloat()==targetFlag) {
                        
                        index=setItemMap["setIndex"].asInt();
                        //log("before number %d : index %d",number,index);
                        
                        break;
                    }
                }
            }
        }
    }
    
    //log("after number %d : index %d",number,index);

    
    //連動させるフラグがある時はセット(セットしておけば優先して使われます)
    auto flagKey=map["temporaryFlagKey"].asString();
    if (temporaryKey.size()>0&&flagKey.size()==0) {
        flagKey=StringUtils::format("%s_%d",temporaryKey.c_str(),number);
    }
    setCustumFlagKey(flagKey);
    
    //nameを編集 ~~~_%d
    name=StringUtils::format("%s_%d",name.c_str(),number);
    
    auto savedValue=DataManager::sharedManager()->getTemporaryFlag(getTemporaryKey(name));
    if(!savedValue.isNull())
    {//indexがせーぶされてるよ
        index=savedValue.asInt();
    }
    
    //フラグによる固定チェック
    if (!answerV.isNull()) {
        if (answerV.asValueMap()["secure"].asBool()) {
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(answerV.asValueMap()["enFlagID"].asInt())) {
                auto answer=answerV.asValueMap()["answer"].asString();
                index=atoi(answer.substr(number,1).c_str());
            }
        }
    }
    

    
    if(index==0){
        //この場合まだ画像をセットしない
        if (!Sprite::init()){
            return false;
        }
    }
    else{
        if (!EscapeStageSprite::init(StringUtils::format("%s_%d.png",name.c_str(),index))) {
            return false;
        }
    }
    
    if (!map["x"].isNull()) {
        setAnchorPoint(Vec2(map["x"].asFloat(), getAnchorPoint().y));
    }
    if (!map["y"].isNull()) {
        setAnchorPoint(Vec2(getAnchorPoint().x, map["y"].asFloat()));
    }
    
    setIndex(index);
    setName(name);
    setScale(parentSize.width/getContentSize().width);
    setNumber(number);//何番目のspriteか
    setDistance(Vec2(0, parentSize.height*.1));//選択時の変位
    
    if(!map["zOrder"].isNull()){
        setLocalZOrder(map["zOrder"].asFloat());
    }
    
    if (!map["distance"].isNull()) {
        auto distanceMap=map["distance"].asValueMap();
        setDistance(Vec2(parentSize.width*distanceMap["x"].asFloat(),parentSize.height*distanceMap["y"].asFloat()));
    }
    
    log("%d : zorder %d",number, getLocalZOrder());
    
    saveIndex();
    
    return true;
}

#pragma mark -
void OrderSprite::up(const onFinished &callback)
{
    if(getIndex()==0)
    {//からの時は選択状態にできません
        callback(false);
        return;
    }
    
    if(getPlaySE().size()>0){
        Common::playSE(getPlaySE().c_str());
    }
    
    setIsSelected(true);
    
    auto pos=Vec2(getDefaultPos().x+getDistance().x, getDefaultPos().y+getDistance().y);
    
    if(getOrderAnimateType()==OrderAnimateType_Move){
        auto move=MoveTo::create(.15, pos);
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        runAction(Sequence::create(move,call, NULL));
    }
    else if (getOrderAnimateType()==OrderAnimateType_Scale){
        auto scale=ScaleBy::create(.15, 1.2);
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        runAction(Sequence::create(scale,call, NULL));
    }
    else{
        setPosition(getDefaultPos().x+getDistance().x, getDefaultPos().y+getDistance().y);
        callback(true);
    }
}

void OrderSprite::down()
{
    setIsSelected(false);
    setPosition(getDefaultPos());
    
    if(getPlaySE().size()>0){
        Common::playSE(getPlaySE().c_str());
    }
}

void OrderSprite::change(int imageIndex, Size parentSize, bool save, bool sound)
{
    setIsSelected(false);
    setPosition(getDefaultPos());
    
    if(imageIndex==0){
        setOpacity(0);
    }
    else{
        auto spr_next = EscapeStageSprite::createWithSpriteFileName(StringUtils::format("%s_%d.png",getName().c_str(),imageIndex));
        setSpriteFrame(spr_next->getSpriteFrame());
        setOpacity(255);

        if(getPlaySE().size()>0&&sound){
            Common::playSE(getPlaySE().c_str());
        }
    }

    setScale(parentSize.width/getContentSize().width);
    setIndex(imageIndex);

    if (save) {
        saveIndex();
    }
}

void OrderSprite::saveIndex()
{
    //save
    DataManager::sharedManager()->setTemporaryFlag(getTemporaryKey(getName()), getIndex());
}

#pragma mark - アイテム使用時
void OrderSprite::useItem(Value setItemVec, Size parentSize, const onFinished &callback)
{//アイテム使用チェック
    if(!setItemVec.isNull()&&getIndex()==0)
    {
        auto vec=setItemVec.asValueVector();
        auto use=false;
        for (auto v :vec) {
            auto map=v.asValueMap();
            if (DataManager::sharedManager()->getSelectedItemID()==map["needItemID"].asInt()) {
                //アイテムを使用
                Common::playSE(getPlaySE().c_str());
                use=true;

                //
                auto index=map["setIndex"].asInt();
                auto spr_next = EscapeStageSprite::createWithSpriteFileName(StringUtils::format("%s_%d.png",getName().c_str(),index));
                setSpriteFrame(spr_next->getSpriteFrame());
                setScale(parentSize.width/getContentSize().width);
                setOpacity(0);
                
                auto enFlagID=map["enFlagID"].asFloat();
                
                //fadeinしてフラグ処理
                runAction(Sequence::create(FadeIn::create(.7),DelayTime::create(.7),CallFunc::create([callback,enFlagID,index,this]{
                    setIndex(index);
                    saveIndex();
                    //特例的にここでフラグ処理
                    Common::playSE("pinpon.mp3");
                    DataManager::sharedManager()->setEnableFlag(enFlagID);
                    DataManager::sharedManager()->removeItemWithID(DataManager::sharedManager()->getSelectedItemID());
                    callback(true);
                    
                }), NULL));
                
                break;
            }
        }
        
        if (!use) {
            //選択アイテムが不正だった
            callback(false);
        }
    }
    else{
        callback(false);
    }
}

#pragma mark -
std::string OrderSprite::getTemporaryKey(std::string name)
{
    if(getCustumFlagKey().size()>0)
    {//指定のキーがある
        return getCustumFlagKey();
    }
    
    auto key=StringUtils::format("order_%s",name.c_str());
    
    return key;
}
