//
//  WarpManager.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/03/24.
//
//

#include "WarpManager.h"
#include "DataManager.h"

using namespace cocos2d;

WarpManager* WarpManager::manager =NULL;

WarpManager* WarpManager::getInstance()
{
    if (manager==NULL) {
        manager=new WarpManager();
    }
    return manager;
}

Warp WarpManager::getWarpStruct(ValueMap map)
{
    Warp warpStruct;
    
    warpStruct.warpID=map["warpID"].asInt();
    warpStruct.replaceType=ReplaceTypeFadeIn;
    
    //flag
    if (!map["warpWithFlag"].isNull()) {
        auto f_map=map["warpWithFlag"].asValueMap();
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(f_map["flagID"].asInt())) {
            log("flagによるwarp指定");
            warpStruct.warpID=f_map["warpID"].asInt();
        }
    }
    
    //temporary
    if (!map["warpWithTemporary"].isNull()) {
        auto t_map=map["warpWithTemporary"].asValueMap();
        if (DataManager::sharedManager()->getTemporaryFlag(t_map["key"].asString()).asInt()==t_map["value"].asInt()) {
            log("temporaryflagによるwarp指定");
            warpStruct.warpID=t_map["warpID"].asInt();
        }
    }
    
    //priority
    auto priority=map["priorityWarp"];
    if (!priority.isNull()) {
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(priority.asValueMap()["flag"].asFloat())) {
            warpStruct.warpID=priority.asValueMap()["warp"].asInt();
        }
    }
    
    //アイテム取得があるときはNoneに。
    if (!map["getItemID"].isNull()) {
        warpStruct.replaceType=ReplaceTypeNone;
    }
    else if (!map["replaceType"].isNull()) {
        warpStruct.replaceType=(ReplaceType)map["replaceType"].asInt();
    }
    
    return warpStruct;
}

void WarpManager::clear()
{
    warps.clear();
}
