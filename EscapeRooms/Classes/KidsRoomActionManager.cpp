//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "KidsRoomActionManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"

#define OtherFlagKey_PropellerStop "propeller_stop"

using namespace cocos2d;

KidsRoomActionManager* KidsRoomActionManager::manager =NULL;

KidsRoomActionManager* KidsRoomActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new KidsRoomActionManager();
    }
    return manager;
}

#pragma mark - Back
void KidsRoomActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if (key=="propeller")
    {//ぷろぺらと時計
        auto spr=Sprite::createWithSpriteFrameName("propeller_0.png");
        spr->setPosition(parent->getContentSize()/2);
        parent->addChild(spr);
        
        //isStoping?
        if (!DataManager::sharedManager()->getOtherFlag(OtherFlagKey_PropellerStop).asBool()) {
            Vector<SpriteFrame*>frames;
            for(int i=1;i<6;i++){
                auto s=Sprite::createWithSpriteFrameName(StringUtils::format("propeller_%d.png",i));
                frames.pushBack(s->getSpriteFrame());
            }
            
            auto animation=Animation::createWithSpriteFrames(frames);
            animation->setDelayPerUnit(2.0f/60.0f);//??フレームぶん表示
            animation->setRestoreOriginalFrame(true);
            animation->setLoops(UINT_MAX);
            
            spr->runAction(Animate::create(animation));
        }
        
        //isStoping?
        std::vector<std::string>names={"sup_4_m.png","sup_4_h.png"};
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(20)) {
            Vector<FiniteTimeAction*>actions;
            int counter=0;
            std::vector<float>angles={360*3,360};
            for (auto name : names) {
                auto spr=parent->getChildByName<Sprite*>(name);
                actions.pushBack(TargetedAction::create(spr, Repeat::create(RotateBy::create(6,angles.at(counter)), UINT_MAX)));
                counter++;
            }
            
            parent->runAction(Spawn::create(actions));
        }
        else{
            std::vector<float>angles={0,180};
            int counter=0;
            for (auto name : names) {
                auto needle=parent->getChildByName(name);
                needle->setRotation(angles.at(counter));
                counter++;
            }
        }
    }
    else if (key=="whiteboard"){
        auto character=Sprite::createWithSpriteFrameName("sup_26_board.png");
        character->setPosition(parent->getContentSize()/2);
        
        auto mask=ClippingNode::create();
        
        auto clippingShape=Sprite::create();
        clippingShape->setTextureRect(Rect(0, 0, parent->getContentSize().width*.6, parent->getContentSize().height/4));
        clippingShape->setColor(Color3B::BLACK);
        clippingShape->setPosition(0,parent->getContentSize().height*.4);
        
        mask->setStencil(clippingShape);
        mask->setAlphaThreshold(0);
        mask->setInverted(false);
        mask->addChild(character);
        mask->setName("clip");
        
        parent->addChild(mask);
        mask->addChild(character);
    }
    else if(key=="flower")
    {
        if(!DataManager::sharedManager()->getEnableFlagWithFlagID(19)){
            return;
        }
        auto duration=1.5f;
        auto angle=10.0f;
        auto angle_head=25.0f;
        auto distance_head=parent->getContentSize().height*.01;
        
        auto isMinigeme=DataManager::sharedManager()->isPlayingMinigame();
        
        Sprite*mistake;
        if (isMinigeme) {
            mistake=parent->getChildByName<Sprite*>(StringUtils::format("sup_%d_mistake.png",backID));
        }
        
        Vector<FiniteTimeAction*>actions_left;
        Vector<FiniteTimeAction*>actions_center;
        Vector<FiniteTimeAction*>actions_right;

        if(backID==34)
        {
            auto stalk=parent->getChildByName("sup_34_stalk.png");
            auto head=parent->getChildByName("sup_34_head.png");
            auto scale_original=stalk->getScale();
            
            auto left_stalk=EaseInOut::create(Spawn::create(RotateTo::create(duration,-angle),
                                                            ScaleTo::create(duration, scale_original*1.1,scale_original*.95), NULL),1.5);
            auto center_stalk=EaseInOut::create(Spawn::create(RotateTo::create(duration,0),
                                                              ScaleTo::create(duration, scale_original), NULL),1.5);
            auto right_stalk=EaseInOut::create(Spawn::create(RotateTo::create(duration,angle),
                                                             ScaleTo::create(duration, scale_original*1.1,scale_original*.95), NULL),1.5);
            auto left_head=EaseInOut::create(Spawn::create(RotateTo::create(duration,-angle_head),
                                                           MoveBy::create(duration, Vec2(0, -distance_head)), NULL),1.5);
            auto center_head=EaseInOut::create(Spawn::create(RotateTo::create(duration,0),
                                                             MoveBy::create(duration, Vec2(0, distance_head)), NULL),1.5);
            auto right_head=EaseInOut::create(Spawn::create(RotateTo::create(duration,angle_head),
                                                            MoveBy::create(duration, Vec2(0, -distance_head)), NULL),1.5);
            
            //
            actions_center.pushBack(TargetedAction::create(stalk,center_stalk));
            actions_center.pushBack(TargetedAction::create(head, center_head));
            if(isMinigeme){
                mistake->setAnchorPoint(stalk->getAnchorPoint());
                mistake->setPosition(stalk->getPosition());
                actions_center.pushBack(TargetedAction::create(mistake, center_stalk->clone()));
            }
            auto center=Spawn::create(actions_center);
            
            actions_left.pushBack(TargetedAction::create(stalk,left_stalk));
            actions_left.pushBack(TargetedAction::create(head, left_head));
            if(isMinigeme){
                actions_left.pushBack(TargetedAction::create(mistake, left_stalk->clone()));
            }
            auto left=Sequence::create(Spawn::create(actions_left),center,NULL);
            
            actions_right.pushBack(TargetedAction::create(stalk,right_stalk));
            actions_right.pushBack(TargetedAction::create(head, right_head));
            if(isMinigeme){
                actions_right.pushBack(TargetedAction::create(mistake, right_stalk->clone()));
            }
            auto right=Sequence::create(Spawn::create(actions_right),center->clone(),NULL);
            
            
            parent->runAction(Repeat::create(Sequence::create(left,right,right->clone(),right->clone(),left->clone(),right->clone(),DelayTime::create(1), NULL), 100));
        }
        else{
            auto flower=parent->getChildByName("sup_3_flower.png");
            if (isMinigeme) {
                //parentの入れ替え
                mistake->retain();
                mistake->removeFromParent();
                flower->addChild(mistake);
            }
            
            auto center=EaseInOut::create(RotateTo::create(duration, 0),1.5);
            auto left=Sequence::create(EaseInOut::create(RotateTo::create(duration, -angle),1.5),center, NULL);
            auto right=Sequence::create(EaseInOut::create(RotateTo::create(duration, angle),1.5),center->clone(), NULL);

            flower->runAction(Repeat::create(Sequence::create(left,right,right->clone(),right->clone(),left->clone(),right->clone(),DelayTime::create(1), NULL), 100));
            
        }
    }
    else if(key=="propeller_up"){
        if (!DataManager::sharedManager()->getOtherFlag(OtherFlagKey_PropellerStop).asBool()) {
            auto propeller=parent->getChildByName<Sprite*>("sup_38_propeller.png");
            auto rotate=RotateBy::create(.4, 360);
            propeller->runAction(Repeat::create(rotate, UINT_MAX));
        }
    }
    else if(key=="clock"){
        if(!DataManager::sharedManager()->getEnableFlagWithFlagID(20)){
            clockAnimate(parent,false,NULL);
        }
        else{
            std::vector<std::string>names={"sup_36_m.png","sup_36_h.png"};
            std::vector<float>angles={60,210};
            int counter=0;
            for (auto name : names) {
                auto needle=AnchorSprite::create(Vec2(.5,.535), parent->getContentSize(), name);
                needle->setName(name);
                needle->setRotation(angles.at(counter));
                parent->addChild(needle);
                counter++;
            }
        }
    }
    else if(key=="train"){
        trainAnimate(parent,backID);
    }
    else if(key=="clear"){
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            return;
        }
        
        auto storyLayer = StoryLayer::create(StoryKey_Ending, [this](Ref* sen){
            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

            auto story1=StoryLayer::create(StoryKey_Ending2, [this](Ref*ref){
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story1);
        });
        Director::getInstance()->getRunningScene()->addChild(storyLayer);
    }
}

void KidsRoomActionManager::trainAnimate(Node *parent,int backID)
{
    //上レール
    std::vector<double>stopPosXs_0={parent->getContentSize().width*.285,parent->getContentSize().width*.5,parent->getContentSize().width*.715};
    std::vector<double>stopPosXs_1={parent->getContentSize().width*.25,parent->getContentSize().width*.5,parent->getContentSize().width*.8};
    auto left_edge_0=0.0;
    auto right_edge_0=parent->getContentSize().width;
    auto left_edge_1=-parent->getContentSize().width*.25;
    auto right_edge_1=parent->getContentSize().width*1.2;
    
    if (backID==2) {
        stopPosXs_0={parent->getContentSize().width*.32,parent->getContentSize().width*.4,parent->getContentSize().width*.5};
        stopPosXs_1={parent->getContentSize().width*.575,parent->getContentSize().width*.675,parent->getContentSize().width*.775};
        left_edge_0=parent->getContentSize().width*.25;
        right_edge_0=parent->getContentSize().width*.625;
        left_edge_1=parent->getContentSize().width*.5;
        right_edge_1=parent->getContentSize().width*.9;
    }
    
    auto train_0=Sprite::createWithSpriteFrameName(StringUtils::format("sup_%d_train_0.png",backID));
    train_0->setPosition(right_edge_0, parent->getContentSize().height/2);
    parent->addChild(train_0);
    
    //フラグによる
    if(!DataManager::sharedManager()->getEnableFlagWithFlagID(22)){
        return;
    }
    
    auto isMinigame=DataManager::sharedManager()->isPlayingMinigame();
    
    auto train_1=Sprite::createWithSpriteFrameName(StringUtils::format("sup_%d_train_1.png",backID));
    train_1->setPosition(Vec2(left_edge_1, parent->getContentSize().height/2));
    parent->addChild(train_1);
    
    Sprite* mistake;
    if (isMinigame) {
        mistake=parent->getChildByName<Sprite*>(StringUtils::format("sup_%d_mistake.png",backID));
        mistake->setPosition(train_1->getPosition());
    }
    
    auto duration=1.5;//1マス進むぶん
    auto duration_delay=1.0;
    
    auto delay=DelayTime::create(duration_delay);
    //右上
    auto move_0=EaseInOut::create(MoveTo::create(duration, Vec2(stopPosXs_0.at(2), train_0->getPosition().y)),1.5);
    auto sound=CallFunc::create([backID]{
        if (backID!=2) {
            Common::playSE("train.mp3");
        }});
    //左上
    auto move_1=EaseInOut::create(MoveTo::create(duration*2, Vec2(stopPosXs_0.at(0), train_0->getPosition().y)),1.5);
    //画面外へ
    auto move_2=EaseInOut::create(MoveTo::create(duration, Vec2(left_edge_0, train_0->getPosition().y)),1.5);
    auto call=CallFunc::create([train_0,right_edge_0]{train_0->setPosition(right_edge_0, train_0->getPosition().y);});
    //上アニメ
    auto seq_0=TargetedAction::create(train_0, Sequence::create(move_0,sound,DelayTime::create(duration_delay*2),move_1,delay,move_2,call,delay->clone(), NULL));
    
    //中下
    auto move_3=EaseInOut::create(MoveTo::create(duration*2.5, Vec2(stopPosXs_1.at(1), train_1->getPosition().y)),1.5);
    auto move_4=EaseInOut::create(MoveTo::create(duration, Vec2(stopPosXs_1.at(2), train_1->getPosition().y)),1.5);
    auto move_5=EaseInOut::create(MoveTo::create(duration, Vec2(right_edge_1, train_1->getPosition().y)),1.5);
    
    auto call_1=CallFunc::create([train_1,left_edge_1]{train_1->setPosition(left_edge_1, train_1->getPosition().y);});
    
    //下アニメ
    if (isMinigame&&backID==12) {
        auto seq_1=Sequence::create(move_3,CallFunc::create([this]{DataManager::sharedManager()->setTemporaryFlag("train_mistake", 1);}), NULL);
        auto spawn=Spawn::create(TargetedAction::create(train_1, seq_1),TargetedAction::create(mistake, seq_1->clone()), NULL);
        parent->runAction(Sequence::create(seq_0,spawn, NULL));
    }
    else{
        auto seq_1=TargetedAction::create(train_1, Sequence::create(move_3,delay->clone(),move_4,delay->clone(),move_5,call_1,delay->clone(), NULL));
        
        parent->runAction(Repeat::create(Sequence::create(seq_0,seq_1, NULL),UINT_MAX));
    }
}

#pragma mark - Touch
void KidsRoomActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key.compare(0, 6, "handle")==0)
    {
        Common::playSE("kacha.mp3");
        bool toLeft=atoi(key.substr(7,1).c_str());
        auto distance=parent->getContentSize().width*.6;
        
        auto mask=(ClippingNode*)parent->getChildByName("clip");
        auto clippingShape=(Sprite*)mask->getStencil();
        auto handle=parent->getChildByName("sup_26_handle.png");
        if (toLeft) {
            distance*=(-1);
        }

        auto move=Spawn::create(TargetedAction::create(handle, MoveBy::create(.5,Vec2(distance,0))),
                                TargetedAction::create(clippingShape, MoveBy::create(.5, Vec2(distance, 0))), NULL);
        auto call=CallFunc::create([callback,toLeft]{
            DataManager::sharedManager()->setTemporaryFlag("whiteboard",!toLeft);
            callback(true);
        });
        parent->runAction(Sequence::create(move,call, NULL));
    }
    else if(key=="propeller_switch"){
        Common::playSE("kacha.mp3");
        
        auto isStoping=DataManager::sharedManager()->getOtherFlag(OtherFlagKey_PropellerStop).asBool();
        DataManager::sharedManager()->setOtherFlag(OtherFlagKey_PropellerStop, !isStoping);
        
        Vector<FiniteTimeAction*>actions;
        if (isStoping) {
            auto sup=(Sprite*)parent->getChildByName("sup_30_off.png");
            actions.pushBack(TargetedAction::create(sup, FadeOut::create(.1)));
        }
        else{
            auto sup=Sprite::createWithSpriteFrameName("sup_30_off.png");
            sup->setPosition(parent->getContentSize()/2);
            sup->setOpacity(0);
            parent->addChild(sup);
            
            actions.pushBack(TargetedAction::create(sup, FadeIn::create(.1)));
        }
        
        
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="marker")
    {
        Common::playSE("p.mp3");
        
        auto stain=Sprite::createWithSpriteFrameName("sup_6_stain.png");
        stain->setPosition(parent->getContentSize()/2);
        stain->setOpacity(0);
        parent->addChild(stain);
        
        auto marker=Sprite::createWithSpriteFrameName("sup_6_marker.png");
        marker->setPosition(parent->getContentSize()/2);
        marker->setOpacity(0);
        parent->addChild(marker);
        
        //fadein
        auto fadein=FadeIn::create(1);
        auto sound=CallFunc::create([this]{
            Common::playSE("pen.mp3");
        });
        //move
        auto distance=parent->getContentSize().width*.02;
        auto duration=.2;
        Vector<FiniteTimeAction*>moves;
        for (int i=1; i<7; i++) {
            auto move=MoveBy::create(duration+i*.025, Vec2(distance*i*pow(-1, i)+distance*.8*(i/2), distance*i*pow(-1, i)-distance*(i/2)));
            moves.pushBack(move);
        }
        
        auto seq_marker=TargetedAction::create(marker, Sequence::create(fadein,sound,Sequence::create(moves),FadeOut::create(.7), NULL));
        
        //fadin_stain
        auto fadein_1=TargetedAction::create(stain, Sequence::create(DelayTime::create(2.25),FadeIn::create(1), NULL));
        auto spawn=Spawn::create(seq_marker,fadein_1, NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,call,NULL));
    }
    else if(key=="monkey"){
        Common::playSE("p.mp3");
        
        //give balls
        auto anim_0=Sprite::createWithSpriteFrameName("anim_42_0.png");
        anim_0->setPosition(parent->getContentSize()/2);
        anim_0->setOpacity(0);
        parent->addChild(anim_0);
        
        auto fadein=TargetedAction::create(anim_0,FadeIn::create(1));
        
        //scene change
        auto anim_1=Sprite::createWithSpriteFrameName("anim_42_1.png");
        anim_1->setPosition(parent->getContentSize()/2);
        anim_1->setOpacity(0);
        parent->addChild(anim_1);
        
        auto arm=Sprite::createWithSpriteFrameName("anim_42_arm.png");
        arm->setPosition(parent->getContentSize()/2);
        arm->setOpacity(0);
        anim_1->addChild(arm);
        
        Vector<Sprite*>balls;
        for (int i=0; i<5; i++) {
            auto ball=AnchorSprite::create(Vec2(.71, .575), parent->getContentSize(), "anim_42_ball.png");
            ball->setOpacity(0);
            
            anim_1->addChild(ball);
            balls.pushBack(ball);
        }
        
        auto fadein_1=TargetedAction::create(anim_1, FadeIn::create(1));
        auto remove_0=TargetedAction::create(anim_0, RemoveSelf::create());
        
        auto fadein_2=Spawn::create(TargetedAction::create(arm, FadeIn::create(1)),TargetedAction::create(balls.at(0), FadeIn::create(1)), NULL);
        
        //throw
        auto move_ball=[balls,parent](int index){
            std::vector<Vec2>vecs={Vec2(-.2, .15),Vec2(-.2, .05),Vec2(-.25, .05),Vec2(-.2, .2),Vec2(-.1, .0)};
            auto distanceX=parent->getContentSize().width*vecs.at(index).x;
            auto distanceY=parent->getContentSize().height*vecs.at(index).y;
            
            auto spawn=TargetedAction::create(balls.at(index), Spawn::create(MoveBy::create(.5,Vec2(distanceX, distanceY)),
                                                                             FadeOut::create(.5),
                                                                             ScaleBy::create(.5, .1),
                                                                             RotateBy::create(.5, 360),
                                                                             CallFunc::create([]{
                Common::playSE("hyu.mp3");
            }), NULL));
            return spawn;
        };
        
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<5; i++) {
            if (i>0) {
                auto setup=Spawn::create(TargetedAction::create(balls.at(i), FadeIn::create(.1)),
                                         TargetedAction::create(arm, FadeIn::create(.1)), NULL);
                actions.pushBack(setup);
            }
            auto arm_action=TargetedAction::create(arm,FadeOut::create(.2));
            auto spawn=Spawn::create(arm_action,move_ball(i), NULL);
            actions.pushBack(spawn);
        }
        
        auto throwSeq=Sequence::create(actions);
        
        //scene change
        auto anim_2=Sprite::createWithSpriteFrameName("anim_42_2.png");
        anim_2->setPosition(parent->getContentSize()/2);
        anim_2->setOpacity(0);
        anim_2->setCascadeOpacityEnabled(true);
        parent->addChild(anim_2);
        
        Vector<FiniteTimeAction*>actions_1;
        std::vector<Vec2>anchors={Vec2(.5, .62),Vec2(.233,.378),Vec2(.5, .378),Vec2(.233, .134),Vec2(.766, .134)};
        for (int i=0; i<5; i++) {
            auto spr=AnchorSprite::create(anchors.at(i), parent->getContentSize(),StringUtils::format("anim_42_plate_%d.png",i));
            anim_2->addChild(spr);
            
            actions_1.pushBack(TargetedAction::create(spr, Spawn::create(ScaleBy::create(.2, 1,.2),
                                                                         FadeOut::create(.2),
                                                                          CallFunc::create([i]{
                Common::playSE("koto.mp3");
                if (i==4) {
                    Common::playSE("goal.mp3");
                }
            }), NULL)));
        }
        
        auto frame=Sprite::createWithSpriteFrameName("anim_42_frame.png");
        frame->setPosition(parent->getContentSize()/2);
        anim_2->addChild(frame);
        
        auto seq_plate=Sequence::create(actions_1);
        auto fadein_3=TargetedAction::create(anim_2, FadeIn::create(1));
        auto remove_1=TargetedAction::create(anim_1, RemoveSelf::create());
        
        auto call=CallFunc::create([this,parent,callback]{
            this->lampAnimate(parent,DataManager::sharedManager()->getNowBack(), [callback](bool finish){
                callback(true);
            });
        });
        
        parent->runAction(Sequence::create(fadein,DelayTime::create(1),fadein_1,remove_0,fadein_2,DelayTime::create(.5),throwSeq,fadein_3,remove_1,DelayTime::create(1),seq_plate,DelayTime::create(.5),call, NULL));
    }
    else if(key.compare(0, 3, "egg")==0)
    {//マトリョーシカ
        auto index=atoi(key.substr(4,1).c_str());
        Vector<FiniteTimeAction*>actions;
        if (index==1&&!DataManager::sharedManager()->getEnableFlagWithFlagID(17)) {
            Common::playSE("koto.mp3");
            auto doll=parent->getChildByName("sup_15_1_0.png");
            doll->setAnchorPoint(Vec2(.716,.412));
            doll->setPosition(parent->getContentSize().width*doll->getAnchorPoint().x, parent->getContentSize().height*doll->getAnchorPoint().y);
            auto rotate=TargetedAction::create(doll,Sequence::create(RotateBy::create(.15, 10),RotateBy::create(.15, -10), NULL));
            actions.pushBack(rotate);
        }
        else{
            Common::playSE("kacha.mp3");
            DataManager::sharedManager()->setTemporaryFlag("egg", index+1);
        }
        
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="pendulum"){
        //振り子正解後
        auto back=Sprite::createWithSpriteFrameName("back_36.png");
        back->setCascadeOpacityEnabled(true);
        back->setPosition(parent->getContentSize()/2);
        back->setOpacity(0);
        parent->addChild(back);
        
        auto sup=Sprite::createWithSpriteFrameName("sup_36_close.png");
        sup->setPosition(parent->getContentSize()/2);
        back->addChild(sup);
        
        auto sup_1=Sprite::createWithSpriteFrameName("sup_36_flower.png");
        sup_1->setPosition(parent->getContentSize()/2);
        back->addChild(sup_1);
        
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        auto clock=CallFunc::create([this,parent,callback]{
            this->clockAnimate(parent,true,[callback](bool onfinish){
                Common::playSE("pinpon.mp3");
                callback(true);
            });
        });
        
        parent->runAction(Spawn::create(fadein,clock, NULL));
    }
    else if(key=="basket")
    {
        Common::playSE("p.mp3");
        
        auto ball=Sprite::createWithSpriteFrameName("anim_46_0.png");
        ball->setOpacity(0);
        ball->setPosition(parent->getContentSize()/2);
        parent->addChild(ball);
        
        auto fadein_0=TargetedAction::create(ball, FadeIn::create(1));
        
        //scene change
        auto back=Sprite::createWithSpriteFrameName("anim_46_1.png");
        back->setOpacity(0);
        back->setCascadeOpacityEnabled(true);
        back->setPosition(parent->getContentSize()/2);
        parent->addChild(back);
        
        auto ball_1=AnchorSprite::create(Vec2(.5, .3), parent->getContentSize(),"anim_46_ball.png");
        ball_1->setPosition(parent->getContentSize().width/2, 0);
        back->addChild(ball_1);
        
        auto rabbit=AnchorSprite::create(Vec2(.5,0), parent->getContentSize(), "anim_46_rabbit.png");
        back->addChild(rabbit);
        
        auto net=Sprite::createWithSpriteFrameName("anim_46_net.png");
        net->setOpacity(0);
        net->setPosition(parent->getContentSize()/2);
        net->setLocalZOrder(parent->getLocalZOrder()+1);
        parent->addChild(net);
        
        //fadein
        auto fadein_1=TargetedAction::create(back, FadeIn::create(1));
        
        //ボールを上に
        auto moveup=TargetedAction::create(ball_1,MoveTo::create(2, Vec2(parent->getContentSize().width/2, parent->getContentSize().height*.3)));
        
        //sit
        auto scale_rabbit=rabbit->getScale();
        auto distance=parent->getContentSize().height*.05;
        auto duration_sit=1.0;
        auto duration_throw=.3;//rabbit
        auto sit=Spawn::create(TargetedAction::create(rabbit, Spawn::create(ScaleTo::create(duration_sit, 1.1*scale_rabbit,.9*scale_rabbit),MoveBy::create(duration_sit, Vec2(0, -distance)), NULL)),
                               TargetedAction::create(ball_1, MoveBy::create(1, Vec2(0, -distance*2))),NULL);
        auto throw_rabbit=TargetedAction::create(rabbit,EaseIn::create( Spawn::create(Sequence::create(ScaleTo::create(duration_throw/2, scale_rabbit*.95,scale_rabbit*1.05),ScaleTo::create(duration_throw, scale_rabbit), NULL),MoveBy::create(duration_throw, Vec2(0, distance)), NULL),1.5));
        auto throw_ball=TargetedAction::create(ball_1,Spawn::create(JumpTo::create(2, Vec2(parent->getContentSize().width/2, parent->getContentSize().height*.8), parent->getContentSize().height, 1),
                                                                    ScaleTo::create(2, scale_rabbit*.25),
                                                                    RotateBy::create(2, 360), NULL));
        auto add_net=TargetedAction::create(net,Sequence::create(DelayTime::create(1.5),
                                                                 FadeIn::create(.1), NULL));
        
        auto spawn_throw=Spawn::create(throw_ball,throw_rabbit,add_net,CallFunc::create([]{Common::playSE("hyu.mp3");}), NULL);
        
        auto sound_net=CallFunc::create([]{Common::playSE("basket.mp3");
            Common::playSE("goal.mp3");
        });
        
        //fadeout
        auto fadeouts=Spawn::create(TargetedAction::create(ball_1, Spawn::create(FadeOut::create(1),EaseIn::create(MoveBy::create(1, Vec2(0, -parent->getContentSize().height*.25)), 1.5), NULL)),
                                    TargetedAction::create(net, FadeOut::create(1)), NULL);
        
        auto call=CallFunc::create([this,parent,callback]{
            this->lampAnimate(parent,DataManager::sharedManager()->getNowBack(), [callback](bool finish){
                callback(true);
            });
        });
        
        parent->runAction(Sequence::create(fadein_0,DelayTime::create(1),fadein_1,moveup,DelayTime::create(.7),sit,DelayTime::create(.3),spawn_throw,sound_net,fadeouts,call, NULL));
    }
    else if(key=="soccer"){
        Common::playSE("p.mp3");
        
        auto ball=Sprite::createWithSpriteFrameName("anim_45_0.png");
        ball->setOpacity(0);
        ball->setPosition(parent->getContentSize()/2);
        parent->addChild(ball);
        
        auto fadein_0=TargetedAction::create(ball, FadeIn::create(1));
        
        //scene change
        auto back=Sprite::createWithSpriteFrameName("anim_45_1.png");
        back->setOpacity(0);
        back->setCascadeOpacityEnabled(true);
        back->setPosition(parent->getContentSize()/2);
        parent->addChild(back);
        
        auto ball_1=AnchorSprite::create(Vec2(.5, .2), parent->getContentSize(),"anim_45_ball.png");
        back->addChild(ball_1);
        
        auto pig=AnchorSprite::create(Vec2(.375,0), parent->getContentSize(), "anim_45_pig.png");
        back->addChild(pig);
        
        //fadein
        auto fadein_1=TargetedAction::create(back, FadeIn::create(1));
        
        //sit
        auto scale_pig=pig->getScale();
        auto distance=parent->getContentSize().height*.1;
        auto duration_sit=1.0;
        auto duration_throw=.3;//pig
        auto duration_ball=1.0;
        
        auto sit=TargetedAction::create(pig, Spawn::create(MoveBy::create(duration_sit, Vec2(0, -distance)), NULL));
        auto kick_pig=TargetedAction::create(pig,EaseIn::create( Spawn::create(Sequence::create(ScaleTo::create(duration_throw/2, scale_pig*.95,scale_pig*1.05),ScaleTo::create(duration_throw, scale_pig), NULL),MoveBy::create(duration_throw, Vec2(0, distance)), NULL),1.5));
        
        //ball
        auto kick_ball=TargetedAction::create(ball_1,Spawn::create(JumpTo::create(duration_ball, Vec2(parent->getContentSize().width/2, parent->getContentSize().height*.55), parent->getContentSize().height*.3, 1),
                                                                    ScaleTo::create(duration_ball, scale_pig*.25),
                                                                    RotateBy::create(duration_ball, 360), NULL));
        
        auto spawn_kick=Spawn::create(kick_ball,kick_pig,CallFunc::create([]{Common::playSE("hit.mp3");}), NULL);
        
        auto sound_net=CallFunc::create([]{Common::playSE("basket.mp3");
            Common::playSE("goal.mp3");
        });
        
        //fadeout
        auto fadeouts=TargetedAction::create(ball_1,Sequence::create(DelayTime::create(.7),Spawn::create(FadeOut::create(1),EaseIn::create(MoveBy::create(1, Vec2(0, -parent->getContentSize().height*.1)), 1.5), NULL), NULL) );
        
        auto call=CallFunc::create([this,parent,callback]{
            this->lampAnimate(parent,DataManager::sharedManager()->getNowBack(), [callback](bool finish){
                callback(true);
            });
        });
        
        parent->runAction(Sequence::create(fadein_0,DelayTime::create(1),fadein_1,DelayTime::create(.7),sit,DelayTime::create(1),spawn_kick,sound_net,fadeouts,call, NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark - Private
void KidsRoomActionManager::clockAnimate(Node *parent, bool stop,const onFinished &callback)
{
    std::vector<std::string>names={"sup_36_m.png","sup_36_h.png"};
    Vector<Sprite*>needles;
    for (auto name : names) {
        auto needle=AnchorSprite::create(Vec2(.5,.535), parent->getContentSize(), name);
        needle->setName(name);
        parent->addChild(needle);
    }
    
    auto needle_m=parent->getChildByName<Sprite*>(names.at(0));
    auto needle_h=parent->getChildByName<Sprite*>(names.at(1));
    
    auto duration=6;
    auto angle_m=360*3;
    auto angle_h=360;
    if (stop) {
        duration=3;
        angle_m/=2;
        angle_h/=2;
    }
    
    auto rotate_m=TargetedAction::create(needle_m, RotateBy::create(duration, angle_m));
    auto rotate_h=TargetedAction::create(needle_h, RotateBy::create(duration, angle_h));

    auto spawn=Spawn::create(rotate_m,rotate_h, NULL);
    
    if (!stop) {
        auto repeat=Repeat::create(spawn, UINT_MAX);
        parent->runAction(repeat);
    }
    else{
        //徐々に止める
        auto rotate_m_1=TargetedAction::create(needle_m, EaseOut::create(RotateBy::create(duration, 240),1.25));
        auto rotate_h_1=TargetedAction::create(needle_h, EaseOut::create(RotateBy::create(duration, 30),1.25));
        auto sound=CallFunc::create([]{
            Common::playSE("kacha.mp3");
        });
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,Spawn::create(rotate_m_1,rotate_h_1, NULL),sound,DelayTime::create(1),call, NULL));
    }
}

void KidsRoomActionManager::lampAnimate(Node *parent,int backID, const onFinished &callback)
{
    auto back=Sprite::createWithSpriteFrameName("back_9.png");
    back->setPosition(parent->getContentSize()/2);
    back->setCascadeOpacityEnabled(true);
    back->setOpacity(0);
    parent->addChild(back);
    
    std::vector<std::string>sup_names;
    if (DataManager::sharedManager()->getEnableFlagWithFlagID(35)) {
        sup_names.push_back("sup_9_basket.png");
    }
    if (DataManager::sharedManager()->getEnableFlagWithFlagID(36)) {
        sup_names.push_back("sup_9_soccer.png");
    }
    if (DataManager::sharedManager()->getEnableFlagWithFlagID(37)) {
        sup_names.push_back("sup_9_baseball.png");
    }
    
    for (auto name:sup_names) {
        auto spr=EscapeStageSprite::createWithSpriteFileName(name);
        spr->setPosition(parent->getContentSize()/2);
        back->addChild(spr);
    }
    
    std::string add_sup_name;
    auto nowBack=DataManager::sharedManager()->getNowBack();
    if (nowBack==46) {
        add_sup_name="sup_9_basket.png";
    }
    else if (nowBack==45) {
        add_sup_name="sup_9_soccer.png";
    }
    else{
        add_sup_name="sup_9_baseball.png";
    }
    
    auto add=EscapeStageSprite::createWithSpriteFileName(add_sup_name);
    add->setPosition(parent->getContentSize()/2);
    add->setOpacity(0);
    parent->addChild(add);
    
    //fadein
    auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
    auto fadein_add=TargetedAction::create(add, FadeIn::create(1));
    auto sound=CallFunc::create([]{Common::playSE("pinpon.mp3");});
    
    //scene change
    auto back_1=Sprite::createWithSpriteFrameName(StringUtils::format("back_%d.png",nowBack));
    back_1->setPosition(parent->getContentSize()/2);
    back_1->setOpacity(0);
    back_1->setLocalZOrder(9);
    parent->addChild(back_1);
    
    auto fadein_back_1=TargetedAction::create(back_1, FadeIn::create(1));
    
    auto call=CallFunc::create([this,callback,parent]{
        this->addStoryAfterGoal(callback);
    });
    
    parent->runAction(Sequence::create(fadein_back,DelayTime::create(1),fadein_add,sound,DelayTime::create(.5),fadein_back_1,call, NULL));
}

void KidsRoomActionManager::addStoryAfterGoal(const onFinished &callback)
{
    std::string key;
    auto nowBack=DataManager::sharedManager()->getNowBack();
    if (nowBack==46) {
        key="basket";
    }
    else if (nowBack==45) {
        key="soccer";
    }
    else{
        key="baseball";
    }
    
    auto story=StoryLayer::create(key, [callback](Ref*ref){
        callback(NULL);
    });
    
    Director::getInstance()->getRunningScene()->addChild(story);
}
