//
//  SettingLayer.h
//  EscapeRooms
//
//  Created by yamaguchinarita on 2017/11/22.
//
//

#ifndef _____EscapeContainer________SettingLayer_____
#define _____EscapeContainer________SettingLayer_____

#include "cocos2d.h"
#include "Utils/ContentsAlert.h"

typedef enum {
    SettingType_OnCollection,
    SettingType_OnTitleScene
}SettingType;

class SettingLayer : public ContentsAlert
{
public:
    static SettingLayer*create(SettingType type);
    virtual bool init(SettingType type);
    CC_SYNTHESIZE(SettingType, settingType, SettingType);
    
    float button_width;
    float button_space;
    ~SettingLayer(){log("SettingLayerが解放されました。");};
    
private:
    void createMenues();
    void setTextToSortLabel();
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
