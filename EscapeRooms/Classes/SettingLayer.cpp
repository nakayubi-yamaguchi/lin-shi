//
//  SettingLayer.cpp
//  EscapeRooms
//
//  Created by yamaguchinarita on 2017/11/22.
//
//

#include "SettingLayer.h"
#include "Utils/Common.h"
#include "UIParts/MenuItemScale.h"
#include "SettingDataManager.h"
#include "DataManager.h"
#include "EscapeDataManager.h"
#include "ConfirmAlertLayer.h"


using namespace cocos2d;

SettingLayer* SettingLayer::create(SettingType type)
{
    auto node=new SettingLayer();
    if (node&&node->init(type)) {
        node->autorelease();
        return node;
    }
    CC_SAFE_RELEASE(node);
    return node;
}

bool SettingLayer::init(SettingType type)
{
    auto screen_size=Director::getInstance()->getRunningScene()->getContentSize();
    button_width=screen_size.width*.225;
    button_space=screen_size.width*.04;
    auto height=(type==SettingType_OnTitleScene)?button_width*1.5:button_width*3;
    auto spr=Sprite::create();
    spr->setTextureRect(Rect(0, 0, screen_size.width*.8, height));
    spr->setOpacity(0);
    
    if (!ContentsAlert::init(spr, "SETTING", "", {"OK"}, nullptr)) {
        return false;
    }
    setSettingType(type);
    
    //タイトルラベルの
    //titleLabel->setTextColor(Color4B(Common::getColorFromHex(LightBlackColor)));

    //displayCloseButton();
    createMenues();
    
    return true;
}

void SettingLayer::createMenues()
{
    //背景画像作成用のラムダ式
    auto backSprite = [this](const char* fileName)->Sprite*{
        
        auto backSprite = Sprite::createWithSpriteFrameName("circle.png");
        backSprite->setColor(Common::getColorFromHex(BlueColor));
        backSprite->setCascadeOpacityEnabled(true);
        
        auto itemSprite = Sprite::createWithSpriteFrameName(fileName);
        itemSprite->setPosition(backSprite->getContentSize()/2);
        itemSprite->setColor(Common::getColorFromHex(WhiteColor));
        itemSprite->setScale(backSprite->getContentSize().width*.53/itemSprite->getContentSize().width);
        backSprite->addChild(itemSprite);
        
        return backSprite;
    };
    
    //ラベルを作成する。
    auto createMenuLabel = [this](const char*text, MenuItem* menuItem,std::string name){
        auto menuLabel = Label::createWithSystemFont(text, "", button_width/5);
        menuLabel->setTextColor(Color4B(Common::getColorFromHex(LightBlackColor)));
        menuLabel->setName(name);
        menuLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        menuLabel->setPosition(menuItem->getPositionX(), menuItem->getPositionY()-menuItem->getBoundingBox().size.height/2-menuLabel->getContentSize().height/2);
        mainSprite->addChild(menuLabel);
    };
    
    auto createToggleMenuItem = [this, backSprite](std::vector<const char*> fileNames,const ccMenuCallback &callback)->MenuItemToggle*{
        
        Vector<MenuItem*> toggleItems;
        for (auto fileName : fileNames) {
            auto onMenuItem = MenuItemScale::create(backSprite(fileName), backSprite(fileName), nullptr);
            toggleItems.pushBack(onMenuItem);
        }
        
        auto sortToggleMenuItem = MenuItemToggle::createWithCallback(callback, toggleItems);
        sortToggleMenuItem->setScale(button_width/sortToggleMenuItem->getContentSize().height);

        sortToggleMenuItem->setCascadeOpacityEnabled(true);
        
        return sortToggleMenuItem;
    };
    
    //行列を渡すと、メニューアイテムのポジションを教えてくれる。
    auto decideMenuItemPosition = [this](int row, int column)->Vec2{
        
        auto positionY = m_contentsNode->getPositionY()+button_width*.15+pow(-1, row)*button_width*.7;
        
        auto positionX = mainSprite->getContentSize().width/2-button_width-button_space+(column-1)*(button_width+button_space);
        
        return Vec2(positionX, positionY);
    };
    
    Vector<MenuItem*> menuItems;
    
    if (getSettingType()!=SettingType_OnTitleScene) {
        //ゴミ箱ボタン
        auto trashMenuItem = MenuItemScale::create(backSprite("trash.png"), backSprite("trash.png"), [this](Ref* sender){
            auto title = DataManager::sharedManager()->getSystemMessage("delete_data");
            auto message = DataManager::sharedManager()->getSystemMessage("delete_data_text");
            
            Common::playClick();
            
            auto alert = CustomAlert::create(title, message, {"Cancel","OK"}, [this](Ref* sender){
                
                auto alert = (CustomAlert*)sender;
                auto selectedNum = alert->getSelectedButtonNum();
                
                if (selectedNum == 1) {//ok
                    log("削除します");
                    EscapeDataManager::getInstance()->deleteAllResourceData();
                    
                    auto completeMessage = DataManager::sharedManager()->getSystemMessage("delete_complete");
                    auto completeAlert = ConfirmAlertLayer::create(ConfirmAlertType_Single , completeMessage, nullptr);
                    completeAlert->showAlert(this);
                }
            });
            alert->setCloseDuration(.3);
            alert->showAlert(this);
        });
        trashMenuItem->setScale(button_width/trashMenuItem->getContentSize().height);
        trashMenuItem->setPosition(decideMenuItemPosition(1, 1));
        
        menuItems.pushBack(trashMenuItem);
        
        //下部ラベル
        createMenuLabel(DataManager::sharedManager()->getSystemMessage("delete_data").c_str(), trashMenuItem,"");
        
        //ソートメニュー
        auto fileNames = {"calendar.png", "difficulty.png", "starframe.png"};
        auto sortToggleMenuItem = createToggleMenuItem(fileNames, [this](Ref*sender){
            Common::playClick();
            
            auto toggle = (MenuItemToggle*)sender;
            auto selectedSort = (SortType)toggle->getSelectedIndex();
            
            SettingDataManager::sharedManager()->setSortType(selectedSort);
            this->setTextToSortLabel();
        });
        sortToggleMenuItem->setPosition(decideMenuItemPosition(1, 2));
        sortToggleMenuItem->setSelectedIndex(SettingDataManager::sharedManager()->getSortType());

        menuItems.pushBack(sortToggleMenuItem);
        
        //ラベルを表示
        createMenuLabel(DataManager::sharedManager()->getSystemMessage("sort").c_str(), sortToggleMenuItem,"sort");//仮入れ
        setTextToSortLabel();
        
        //順序メニュー
        auto orderFileNames = {"descend.png", "ascend.png"};
        auto orderToggleMenuItem = createToggleMenuItem(orderFileNames, [](Ref*sender){
            Common::playClick();
            
            auto toggle = (MenuItemToggle*)sender;
            auto selectedOrder = (SortType)toggle->getSelectedIndex();
            
            SettingDataManager::sharedManager()->setIsAscend(selectedOrder);
            
        });
        orderToggleMenuItem->setPosition(decideMenuItemPosition(1, 3));
        orderToggleMenuItem->setSelectedIndex(SettingDataManager::sharedManager()->getIsAscend());
        
        menuItems.pushBack(orderToggleMenuItem);
        
        //ラベルを表示
        createMenuLabel(DataManager::sharedManager()->getSystemMessage("order").c_str(), orderToggleMenuItem,"");
    }
    
    //サウンドボタン
    std::vector<std::string>labelNames={"BGM","SE","VIBE"};
    std::vector<const char*> fileNames = {"on.png","off.png"};

    for (int i=0; i<labelNames.size(); i++) {
        if (i==2) {
            fileNames = {"on_vibe.png","off_vibe.png"};
        }
        
        auto soundMenuItem = createToggleMenuItem(fileNames,[i](Ref*sender){
            Common::playClick();
            
            auto toggle = (MenuItemToggle*)sender;
            auto stopSound = toggle->getSelectedIndex();
            
            if(i==0){
                SettingDataManager::sharedManager()->setStopBgm(stopSound);
            }
            else if(i==1){
                SettingDataManager::sharedManager()->setStopSe(stopSound);
            }
            else{
                SettingDataManager::sharedManager()->setStopVibe(stopSound);
            }
        });
        
        auto pos = decideMenuItemPosition(0,i+1);
        auto soundPos = Vec2(pos.x,(getSettingType()==SettingType_OnTitleScene)?m_contentsNode->getPosition().y+button_width*.15:pos.y);
        soundMenuItem->setPosition(soundPos);
        
        if(i==0){
            soundMenuItem->setSelectedIndex(SettingDataManager::sharedManager()->getStopBgm());
        }
        else if(i==1){
            soundMenuItem->setSelectedIndex(SettingDataManager::sharedManager()->getStopSe());
        }
        else{
            soundMenuItem->setSelectedIndex(SettingDataManager::sharedManager()->getStopVibe());
        }
        menuItems.pushBack(soundMenuItem);
        
        createMenuLabel(labelNames.at(i).c_str(), soundMenuItem,"");
    }
    
    //メニュー
    auto settingMenu = Menu::createWithArray(menuItems);
    settingMenu->setPosition(Vec2::ZERO);
    mainSprite->addChild(settingMenu);
}

void SettingLayer::setTextToSortLabel()
{
    return;
    
    auto label=mainSprite->getChildByName<Label*>("sort");
    auto sortKey="sort_star";
    if (SettingDataManager::sharedManager()->getSortType()==SortType_Date) {
        sortKey="sort_date";
    }
    else if (SettingDataManager::sharedManager()->getSortType()==SortType_Difficulty){
        sortKey="sort_difficulty";
    }
    
    label->setString(StringUtils::format("%s\n(%s)",DataManager::sharedManager()->getSystemMessage("sort").c_str(),DataManager::sharedManager()->getSystemMessage(sortKey).c_str()));
    //createMenuLabel(StringUtils::format("%s\n(%s)",DataManager::sharedManager()->getSystemMessage("sort").c_str(),DataManager::sharedManager()->getSystemMessage(sortKey).c_str()).c_str(), sortToggleMenuItem,"sort");
}
