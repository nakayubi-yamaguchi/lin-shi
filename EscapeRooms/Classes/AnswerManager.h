//
//  AnswerManager.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/06/04.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________AnswerManager_____
#define _____PROJECTNAMEASIDENTIFIER________AnswerManager_____

#include "cocos2d.h"
typedef std::function<void()> answerCallback;
typedef std::function<void(bool success)> answerCallback2;

USING_NS_CC;
class AnswerManager
{
public:
    static AnswerManager* manager;
    static AnswerManager* getInstance();
    std::vector<float>enFlagIDs;
    std::vector<int>getItemIDs;
    std::vector<int>animationIDs;
    std::vector<int>removeItemIDs;
    std::vector<ValueMap>temporaryFlags;
    CC_SYNTHESIZE(bool, foundMystery, FoundMystery);
    CC_SYNTHESIZE(std::string, customActionKey_1, CustomActionKey_1);
    CC_SYNTHESIZE(std::string, customActionKey_2, CustomActionKey_2);

    /**パスワード入力記憶用*/
    std::string passcode;
    std::string passcode_onItem;

    void appendPasscode(std::string append);
    void appendPasscode(std::string append,bool onItem);
    void resetPasscode();
    void resetPasscode(bool onItem);
    
#pragma mark 2桁対応
    /**2桁の数字を1桁の文字に変換する*/
    std::string transformNumberToString(int num);
    int transformStringToNumber(std::string string);
    std::vector<std::string>getStringsForTransform();

#pragma mark 正解判定
    std::string getPasscode();
    std::string getPasscode(bool onItem);

    /**正解判定 answerがNULLの場合trueを返します*/
    void checkAnswer(std::string customKey,Value answerValue,bool onItem,const answerCallback2& callback);
    /**正解後のデータ処理 一時的に格納する*/
    void manageAnswer(ValueMap answerMap,const answerCallback& callback);
    
#pragma mark manage data
    /**フラグ立て temporary,otherも処理*/
    void manageEnFlagIDs(ValueMap map);
    /**間違い探しデータ処理*/
    void manageMysteryData();
    /**カメラ処理*/
    void manageCameraData(ValueMap map);

    /**カスタムアクションが実行可能かフラグをチェック*/
    bool checkCustomActionAnswerable(ValueMap answerMap);
    /**アイテム使用後データ処理*/
    void manageRemoveItem(ValueMap map,bool onItem);

#pragma mark clear
    /**一時格納した配列をclear*/
    void clear();
    
private:
    void delayAndCallBack(float duration, const answerCallback& callback);
    LayerColor* createDisableLayer();
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
