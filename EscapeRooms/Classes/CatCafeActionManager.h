//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__CatCafeActionManager__
#define __EscapeContainer__CatCafeActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class CatCafeActionManager:public CustomActionManager
{
public:
    static CatCafeActionManager* manager;
    static CatCafeActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);

private:
    Sprite*createSpriteToCenter(std::string name,bool invisible,Node* parent);
    CallFunc*createSoundAction(std::string fileName);
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
