//
//  AnswerManager.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/06/04.
//
//

#include "AnswerManager.h"
#include "WarpManager.h"
#include "DataManager.h"
#include "Utils/Common.h"
#include "ClearScene.h"
#include "NotificationKeys.h"
#include "SupplementsManager.h"

using namespace cocos2d;

AnswerManager*AnswerManager::manager=NULL;
AnswerManager* AnswerManager::getInstance()
{
    if (manager==NULL) {
        manager=new AnswerManager();
    }
    return manager;
}

#pragma mark - 正解判定
void AnswerManager::checkAnswer(std::string customKey, cocos2d::Value answerValue,bool onItem, const answerCallback2 &callback)
{
    if (answerValue.isNull()) {
        callback(true);
        return;
    }
    
    auto answerMap=answerValue.asValueMap();
    
    //必要なフラグが立っているか
    if (!checkCustomActionAnswerable(answerMap)) {
        callback(false);
        return;
    }
    
    //
    resetPasscode(onItem);
    
    if (customKey==CustomAction_OrderSprite) {
        for (auto spr :SupplementsManager::getInstance()->orderSprites) {
            AnswerManager::getInstance()->appendPasscode(StringUtils::format("%d",spr->getIndex()));
        }
    }
    else if (customKey==CustomAction_Label) {
        Vector<CountUpLabel*>vec;
        if (onItem) {
            vec=SupplementsManager::getInstance()->labelSupplements_onItem;
        }
        else{
            vec=SupplementsManager::getInstance()->labelSupplements;
        }
        
        for (auto spr :vec) {
            AnswerManager::getInstance()->appendPasscode(StringUtils::format("%s",spr->getString().c_str()));
        }
    }
    else if (customKey==CustomAction_SwitchSprite) {
        Vector<SwitchSprite*>vec;
        if (onItem) {
            vec=SupplementsManager::getInstance()->switchSprites_onItem;
        }
        else{
            vec=SupplementsManager::getInstance()->switchSprites;
        }
        
        for (auto spr :vec) {
            if (spr->getEnabled()) {
                AnswerManager::getInstance()->appendPasscode(StringUtils::format("%d",spr->getIndex()));
            }
        }
    }
    else if (customKey==CustomAction_RotateSprite) {
        for (auto spr :SupplementsManager::getInstance()->rotateSprites) {
            AnswerManager::getInstance()->appendPasscode(StringUtils::format("%02d",spr->getIndex()));
        }
    }
    else if(customKey==CustomAction_SliderSprite){
        for (auto spr :SupplementsManager::getInstance()->sliderSprites) {
            AnswerManager::getInstance()->appendPasscode(StringUtils::format("%d",spr->getPosIndex()));
        }
    }
    else if (customKey==CustomAction_ChangingSprite) {
        for (auto spr :SupplementsManager::getInstance()->changingSprites) {
            if (spr->getStoped()) {
                AnswerManager::getInstance()->appendPasscode(StringUtils::format("%d",spr->getIndex()));
            }
        }
    }
    
    //答え判定
    auto passcode = AnswerManager::getInstance()->getPasscode();
    if (passcode.size() > 0 && (answerMap["answer"].asString()==passcode||
        answerMap["answer_1"].asString()==passcode)) {
        log("正解とします。");
        AnswerManager::getInstance()->manageAnswer(answerMap, [callback](){
            callback(true);
        });
    }
    else{
        log("不正解とします。");

        callback(false);
    }
}

#pragma mark - 正解後処理
void AnswerManager::manageAnswer(cocos2d::ValueMap answerMap, const answerCallback &callback)
{
    auto enFlagID=answerMap["enFlagID"].asFloat();
    if (enFlagID) {
        enFlagIDs.push_back(enFlagID);
    }
    
    if (!answerMap["getItemID"].isNull()) {
        getItemIDs.push_back(answerMap["getItemID"].asInt());
    }
    
    auto playSE=answerMap["playSE"].asString();
    auto warpID=answerMap["warpID"].asInt();
    if(warpID){
        Warp warp;
        warp.warpID=warpID;
        
        auto replaceType=ReplaceTypeFadeIn;
        if (!answerMap["replaceType"].isNull()) {
            replaceType=(ReplaceType)answerMap["replaceType"].asInt();
        }
        warp.replaceType=replaceType;
        
        WarpManager::getInstance()->warps.push_back(warp);
    }
    
    auto setTemporaryFlag=answerMap["setTemporaryFlag"];
    if (!setTemporaryFlag.isNull()) {
        auto m=setTemporaryFlag.asValueMap();
        
        temporaryFlags.push_back(m);
    }
    
    auto setTemporaryFlags=answerMap["setTemporaryFlags"];
    if (!setTemporaryFlags.isNull()) {
        auto vec=setTemporaryFlags.asValueVector();
        
        for (auto v : vec) {
            temporaryFlags.push_back(v.asValueMap());
        }
    }
    
    auto removeItemValue=answerMap["removeItemID"];
    if (!removeItemValue.isNull()) {
        removeItemIDs.push_back(removeItemValue.asInt());
    }
    
    auto removeItemsValue=answerMap["removeItemIDs"];
    if (!removeItemsValue.isNull()) {
        auto vec=removeItemsValue.asValueVector();
        for (auto v : vec) {
            removeItemIDs.push_back(v.asInt());
        }
    }
    
    auto animationID=answerMap["animationID"].asInt();
    if(animationID>0){
        animationIDs.push_back(animationID);
    }
    
    auto duration=answerMap["delay"].asFloat();
    if (duration<=0) {
        duration=.7;
    }
    
    if (!answerMap["customActionKey_1"].isNull()) {
        setCustomActionKey_1(answerMap["customActionKey_1"].asString());
    }
    
    if (!answerMap["customActionKey_2"].isNull()) {
        setCustomActionKey_2(answerMap["customActionKey_2"].asString());
    }
    
    //cameraは保存がないのでここでchangeしちゃう
    manageCameraData(answerMap);
    
    delayAndCallBack(duration, [callback,playSE](){
        Common::playSE(playSE.c_str());
        callback();
    });
}

void AnswerManager::manageEnFlagIDs(ValueMap map)
{
    auto enFlagID=map["enFlagID"].asFloat();
    if (enFlagID>0)
    {//フラグを立てる
        DataManager::sharedManager()->setEnableFlag(enFlagID);
    }
    
    //立てるべきフラグが格納されていれば
    for(float flag :enFlagIDs){
        DataManager::sharedManager()->setEnableFlag(flag);
    }
    
    //一時的なフラグ指定があれば立てる
    if(!map["setTemporaryFlags"].isNull()){
        auto temporaryVec=map["setTemporaryFlags"].asValueVector();
        for (auto v : temporaryVec) {
            auto m=v.asValueMap();
            DataManager::sharedManager()->setTemporaryFlag(m["key"].asString(), m["value"].asInt());
        }
    }
    else if (!map["setTemporaryFlag"].isNull()) {
        auto temporaryMap=map["setTemporaryFlag"].asValueMap();
        DataManager::sharedManager()->setTemporaryFlag(temporaryMap["key"].asString(), temporaryMap["value"].asInt());
    }
    
    if (temporaryFlags.size()>0) {
        for (auto m: AnswerManager::getInstance()->temporaryFlags) {
            DataManager::sharedManager()->setTemporaryFlag(m["key"].asString(), m["value"].asInt());
        }
    }
    
    //その他フラグがあれば立てる
    if (!map["setOtherFlag"].isNull()) {
        auto otherMap=map["setOtherFlag"].asValueMap();
        DataManager::sharedManager()->setOtherFlag(otherMap["key"].asString(), otherMap["value"].asInt());
    }
}

void AnswerManager::manageMysteryData()
{
    if (getFoundMystery()) {
        setFoundMystery(false);

        //通知を発行
        auto event=EventCustom(NotificationKeyFoundMystery);
        Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
        
        //
        if (DataManager::sharedManager()->getFoundMysteryCount()==DataManager::sharedManager()->getMiniGameData()["mysteryCount"].asInt()) {
            //くりあ
            auto filename=DataManager::sharedManager()->getBgmName(true);
            EscapeDataManager::getInstance()->playSound(filename.c_str(), true);
            
            //minigameデータを消去する
            DataManager::sharedManager()->resetMinigameData();
            
            Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
        }
    }
}

#pragma mark camera
void AnswerManager::manageCameraData(ValueMap map)
{
    auto v=map["setCamera"];
    if (!v.isNull()) {
        auto cameraMap=v.asValueMap();
        if (cameraMap["from"].asInt()==DataManager::sharedManager()->getShowingPhotoID()) {
            DataManager::sharedManager()->setShowingPhotoID(cameraMap["to"].asInt());
        }
    }
}

#pragma mark - アイテム使用後
void AnswerManager::manageRemoveItem(ValueMap map, bool onItem)
{
    if (map["retainItemWithFlag"].isNull()&&
        map["retainItemAfterUse"].asBool()==false&&
        (!map["needItemIDs"].isNull()||!map["needItemID"].isNull()))
    {//アイテムの使用指定があり、削除ルールもない
        DataManager::sharedManager()->removeItemWithID(DataManager::sharedManager()->getSelectedItemID());
    }
    
    //cacheされていれば
    for (auto r:AnswerManager::getInstance()->removeItemIDs) {
        DataManager::sharedManager()->removeItemWithID(r);
    }
    
    //保持ルールがない
    if (!map["retainItemWithFlag"].isNull()) {
        auto itemMap=map["retainItemWithFlag"].asValueMap();
        bool remove=true;
        auto flags=itemMap["flagIDs"].asValueVector();
        for (auto v :flags) {
            if (!DataManager::sharedManager()->getEnableFlagWithFlagID(v.asInt())) {
                remove=false;
                log("フラグ不足:%d アイテムをretain",v.asInt());
                break;
            }
        }
        
        if (remove) {
            auto itemIDs=itemMap["itemIDs"].asValueVector();
            for (auto v : itemIDs) {
                DataManager::sharedManager()->removeItemWithID(v.asInt());
            }
        }
    }
    
    //指定IDが明示されてれば
    if (!map["removeItemID"].isNull()) {
        DataManager::sharedManager()->removeItemWithID(map["removeItemID"].asInt());
    }
}

#pragma mark -
void AnswerManager::clear()
{
    enFlagIDs.clear();
    getItemIDs.clear();
    animationIDs.clear();
    removeItemIDs.clear();
    temporaryFlags.clear();
}

#pragma mark delay
void AnswerManager::delayAndCallBack(float duration, const answerCallback &callback)
{
    auto layer=createDisableLayer();
    Director::getInstance()->getRunningScene()->addChild(layer);
    
    auto wait=DelayTime::create(duration);
    auto call=CallFunc::create([callback,layer](){
        layer->removeFromParent();
        callback();
    });
    
    Director::getInstance()->getRunningScene()->runAction(Sequence::create(wait,call, NULL));
}

#pragma mark- パスコード
void AnswerManager::appendPasscode(std::string append)
{
    bool onItem=(DataManager::sharedManager()->getShowingItemID()>0);
    appendPasscode(append,onItem);
}

void AnswerManager::appendPasscode(std::string append, bool onItem)
{
    if (onItem) {
        passcode_onItem+=append;
    }
    else{
        passcode+=append;
    }
}

void AnswerManager::resetPasscode()
{
    passcode="";
    passcode_onItem="";
}

void AnswerManager::resetPasscode(bool onItem)
{
    if (onItem) {
        passcode_onItem="";
    }
    else{
        passcode="";
    }
}

std::string AnswerManager::getPasscode()
{
    bool onItem=(DataManager::sharedManager()->getShowingItemID()>0);
    return getPasscode(onItem);
}

std::string AnswerManager::getPasscode(bool onItem)
{
    std::string str=passcode;
    if (onItem) {
        str= passcode_onItem;
    }
    log("入力中パス %s onItem ? %d",str.c_str(),onItem);

    return str;
}

#pragma mark - 文字列変換
std::string AnswerManager::transformNumberToString(int num)
{
    if (num>9) {
        
        return getStringsForTransform().at(num-10);
    }
    return StringUtils::format("%d",num);
}

int AnswerManager::transformStringToNumber(std::string string)
{
    auto vec=getStringsForTransform();
    int counter=10;
    for (auto str : vec) {
        if (str==string) {
            return counter;
        }
        counter++;
    }
    
    return atoi(string.c_str());
}

std::vector<std::string> AnswerManager::getStringsForTransform()
{
    std::vector<std::string>strings={"a","b","c","d","e","f","g","h"};
    return strings;
}

#pragma mark- タッチ非透過
LayerColor* AnswerManager::createDisableLayer()
{
    auto layer=LayerColor::create(Color4B(0, 0, 0, 0));
    layer->setLocalZOrder(999);
    
    auto listner=EventListenerTouchOneByOne::create();
    listner->setSwallowTouches(true);//透過させない
    listner->onTouchBegan=[this](Touch*touch,Event*event)
    {
        return true;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listner, layer);
    
    return layer;
}

#pragma mark -
bool AnswerManager::checkCustomActionAnswerable(cocos2d::ValueMap answerMap)
{
    if (!answerMap["needFlagIDs"].isNull()) {
        if (!DataManager::sharedManager()->getEnableFlagWithFlagIDs(answerMap["needFlagIDs"].asValueVector())) {
            return false;
        }
    }
    if (!answerMap["needFlagID"].isNull()) {
        if(!DataManager::sharedManager()->getEnableFlagWithFlagID(answerMap["needFlagID"].asFloat())){
            log("フラグ不足 %f",answerMap["needFlagID"].asFloat());
            return false;
        }
    }
    
    if(DataManager::sharedManager()->getEnableFlagWithFlagID(answerMap["disableFlagID"].asFloat())){
        log("disableフラグがたっています");
        return false;
    }
    
    return true;
}
