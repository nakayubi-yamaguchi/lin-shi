//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__KidsRoomActionManager__
#define __EscapeContainer__KidsRoomActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class KidsRoomActionManager : public CustomActionManager
{
public:
    static KidsRoomActionManager* manager;
    static KidsRoomActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void trainAnimate(Node*parent,int backID);

    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void clockAnimate(Node*parent,bool stop,const onFinished& callback);
    void lampAnimate(Node*parent,int backID,const onFinished& callback);
    void addStoryAfterGoal(const onFinished& callback);
private:
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
