//
//  ChocolateShopActionManager.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/11/29.
//
//

#ifndef __EscapeContainer__GirlFriendActionManager__
#define __EscapeContainer__GirlFriendActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;
#define MoguraActionTag 2
#define Temporarykey_Attention "attention_count"
#define Temporarykey_Enable_Hammer "enable_hammer"

class GirlFriendActionManager:public CustomActionManager
{
private:
    
public:
    static GirlFriendActionManager* manager;
    static GirlFriendActionManager* getInstance();
   
    
    //over ride
    int getDefaultBackNum();
    void backAction(std::string key,int backID,Node*parent,ShowType type);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void touchBeganAction(std::string key,Node*parent);
    
    void dragAction(std::string key,Node*parent,Vec2 pos);
    
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    
    void customAction(std::string key,Node*parent);
    
    void showAnswerAction(std::string key, Node *parent, ShowType showType);

    
private:
    
    void createHumidificationParticle(Node *parent, int backID, ShowType type);
    FiniteTimeAction* showLight(Node *parent, int backID, ShowType type);

    /**モグラのアニメーション継続可能か判定*/
    bool enableMoguraAction();
    FiniteTimeAction *hamsterAction(Node* parent);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
