//
//  IzakayaActionManager.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2018/10/17.
//
//

#include "IzakayaActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "GameScene.h"
#include "TouchManager.h"

using namespace cocos2d;

IzakayaActionManager* IzakayaActionManager::manager =NULL;

IzakayaActionManager* IzakayaActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new IzakayaActionManager();
    }
    
    return manager;
}


#pragma mark - Back
void IzakayaActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
#pragma mark 鼻ちょうちん
    if(key=="sleep"&&!DataManager::sharedManager()->getEnableFlagWithFlagID(3))
    {
        auto balloonFile = "sup_5_balloon.png";
        auto balloonAct = balloonAction(parent, balloonFile, Vec2(.483, .365), 2);
        auto balloon = parent->getChildByName(balloonFile);
        
        balloon->runAction(balloonAct);//バルーンでrunActionしないと、あとでrepeatの音消せない。
    }
#pragma mark レジ
    else if(key=="register")
    {
        auto labelSupplements = SupplementsManager::getInstance()->labelSupplements;

        for (auto label : labelSupplements) {
            if (!DataManager::sharedManager()->getTemporaryFlag(label->getTemporaryKey()).isNull()) {
                auto index = DataManager::sharedManager()->getTemporaryFlag(label->getTemporaryKey()).asInt();
                AnswerManager::getInstance()->appendPasscode(StringUtils::format("%d",index));
                log("いんでっくす%d",index);
                log("いんでっくすぱす%s",AnswerManager::getInstance()->getPasscode().c_str());
            }
        }
        setRegiText("");
    }
#pragma mark 湯気鍋
    else if (key == "zashikinabe") {
        
        Vec2 posRate = Vec2(.5, .5);
        Vec2 posVar = Vec2(parent->getContentSize().width*.2, parent->getContentSize().height*.15);
        int totalPaticle = 150;
        if (backID == 73) {//ちょいひき
            posRate = Vec2(.17, .626);
            posVar = posVar/2;
            totalPaticle = totalPaticle/2;
        }
        
        auto particle=ParticleSystemQuad::create("smoke.plist");
        particle->setAutoRemoveOnFinish(true);
        particle->setPosition(parent->getContentSize().width*posRate.x, parent->getContentSize().height*posRate.y);
        particle->setPosVar(posVar);
        particle->setSpeed(parent->getContentSize().height*.1);
        particle->setTotalParticles(totalPaticle);
        particle->setAngle(90);
        particle->setAngleVar(10);
        particle->setLife(2);
        particle->setStartSize(parent->getContentSize().width*.1);
        particle->setEndSize(parent->getContentSize().width*.2);
        particle->setLocalZOrder(2);
        parent->addChild(particle);
    }
#pragma mark ガスコンロ
    else if (key == "gas") {
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(42)) return;
        
        //火をつける
        showFireParticle(parent,backID);
        
        Vector<FiniteTimeAction*> actions;
        actions.pushBack(DelayTime::create(1));

        //蓋カタカタ
        auto lid = parent->getChildByName<Sprite*>(StringUtils::format("sup_%d_lid.png", backID));
        int rotate = 7;
        float duration = .1;
        
        if (backID == 33) {//引き
            rotate = 4;
            
            lid->setAnchorPoint(Vec2(.475, .7));
            lid->setPosition(parent->getContentSize().width*lid->getAnchorPoint().x, parent->getContentSize().height*lid->getAnchorPoint().y);
        }
        else {
            actions.pushBack(createSoundAction("gatagata.mp3"));
        }
        
        
        for (int i=0; i<3; i++) {
            actions.pushBack(TargetedAction::create(lid, RotateTo::create(duration, rotate)));
            actions.pushBack(TargetedAction::create(lid, RotateTo::create(duration, -1*rotate)));
        }
        actions.pushBack(TargetedAction::create(lid, RotateTo::create(duration, 0)));
        
        actions.pushBack(DelayTime::create(.8));
        
        //煙出る
        std::vector<int> smokeAnswer = {0, 2, 1, 2, 0};
        float hunsyaDuration = 1.5;
        for (int n = 0; n < smokeAnswer.size(); n++) {
            
            int num = smokeAnswer[n];
            
            //回転
            actions.pushBack(TargetedAction::create(lid, RotateTo::create(duration, rotate*-(num-1))));
            
            //噴射
            if (backID == 35) {//引きでは音鳴らさない。
                actions.pushBack(createSoundAction("spray.mp3"));
            }
            
            auto showParticleCall = CallFunc::create([this, parent, num, hunsyaDuration,backID](){
                this->showNabeParticle(parent,backID, num, hunsyaDuration);
            });
            actions.pushBack(showParticleCall);
            actions.pushBack(DelayTime::create(hunsyaDuration));
            
            //回転を戻す
            actions.pushBack(TargetedAction::create(lid, RotateTo::create(duration, 0)));
            
            actions.pushBack(DelayTime::create(.5));
        }
        
        actions.pushBack(DelayTime::create(1));
        
        parent->runAction(Repeat::create(Sequence::create(Sequence::create(actions), NULL), -1));
    }
#pragma mark 間違い探し：雌豚
    else if (key == "mistake_pig") {
        if (!DataManager::sharedManager()->isPlayingMinigame()) return;
        
        std::string fileName;
        if (backID == 6) {
            fileName = "sup_6_mistake_pig.png";
        }
        else {
            fileName = StringUtils::format("sup_%d_mistake.png", backID);
        }
        
        auto pig = parent->getChildByName<Sprite*>(fileName);

        auto seq = Sequence::create(MoveTo::create(.06, Vec2(pig->getPositionX()-parent->getContentSize().width*.005,pig->getPositionY())),
                                    MoveTo::create(.06, Vec2(pig->getPositionX()+parent->getContentSize().width*.005,pig->getPositionY())), NULL);
        pig->runAction(Repeat::create(seq, -1));
    }
#pragma mark 間違い探し：カッパ
    else if (key == "sleep" && DataManager::sharedManager()->isPlayingMinigame()) {
        
        Vector<FiniteTimeAction*> actions;
        actions.pushBack(DelayTime::create(.5));
        
        //カッパ驚く
        auto kappaFile = "sup_5_mistake_kappa.png";
        actions.pushBack(attentionAction(parent, kappaFile, Vec2(.5, .167), "sup_5_attention.png"));
        actions.pushBack(DelayTime::create(.3));
        
        //頰を赤らめる
        auto kappa = parent->getChildByName<Sprite*>(kappaFile);
        auto red = createSpriteToCenter("sup_5_red.png", true, kappa, true);
        actions.pushBack(createSoundAction("kyuu.mp3"));
        actions.pushBack(TargetedAction::create(red, FadeIn::create(.4)));
        
        parent->runAction(Sequence::create(actions));
        
    }
}

#pragma mark - Touch
void IzakayaActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    Vector<FiniteTimeAction*> actions;
#pragma mark 串で起こす。
    if(key=="skewer"){
        Common::playSE("p.mp3");
        auto balloon=parent->getChildByName("sup_5_balloon.png");
        auto pin=createSpriteToCenter("sup_5_skewer.png", true, parent, true);
        pin->setLocalZOrder(1);
        
        //串動く
        auto seq_skewer = Sequence::create(FadeIn::create(1),
                                           DelayTime::create(1),
                                           Spawn::create(MoveBy::create(.2, Vec2(parent->getContentSize().width*.07, parent->getContentSize().height*.13)),
                                                         RotateTo::create(.2, -10), NULL),
                                           DelayTime::create(.1),
                                           MoveBy::create(.1, Vec2(parent->getContentSize().width*.07, -parent->getContentSize().height*.13)), NULL);
        actions.pushBack(TargetedAction::create(pin, seq_skewer));
        
        //風船割れる
        auto call_stop = CallFunc::create([balloon]{
            balloon->stopAllActions();
            Common::stopAllSE();
        });
        auto spawn_balloon =   Sequence::create(call_stop,
                                                createSoundAction("pan.mp3"),
                                                TargetedAction::create(balloon, RemoveSelf::create()), NULL);
        actions.pushBack(spawn_balloon);
        
        //串消す
        actions.pushBack(TargetedAction::create(pin, Sequence::create(DelayTime::create(1),
                                                                      FadeOut::create(1), NULL)));
     
        //目を覚ます
        auto pig=parent->getChildByName("sup_5_pig_0.png");
        auto pig_awake = createSpriteToCenter("sup_5_pig_1.png", true, parent, true);
        actions.pushBack(createChangeAction(.5, .5, pig, pig_awake));
        
        auto delay=DelayTime::create(.6);
        actions.pushBack(delay);
        
        //豚驚く
        auto attentionPigFileName = "sup_5_pig_2.png";
        auto pig_attention = createSpriteToCenter(attentionPigFileName, true, parent, true);
        actions.pushBack(createChangeAction(.1, .1, pig_awake, pig_attention));
        actions.pushBack(attentionAction(parent, attentionPigFileName, Vec2(.5, .167), "sup_5_attention.png"));
        
        actions.pushBack(DelayTime::create(.3));
        
        //走ってどっか行く
        auto pig_run = createSpriteToCenter("sup_5_pig_3.png", true, parent, true);
        actions.pushBack(createChangeAction(.2, .2, pig_attention, pig_run));
        actions.pushBack(createSoundAction("pig.mp3"));
        
        actions.pushBack(DelayTime::create(1.5));
        
        //豚消える
        actions.pushBack(TargetedAction::create(pig_run, FadeOut::create(.4)));

        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark クラウド。欲しがる。
    else if (key == "cloud") {
        int num = DataManager::sharedManager()->getNowBack();
        if (num == 18) {//オレンジジュース豚のとこ
            auto cloudBounce = cloudBounceAction(parent, {"sup_18_pig_0.png"}, Vec2(.5, .364), "sup_18_cloud.png", Vec2(.318, .742), "pig.mp3");
            actions.pushBack(cloudBounce);
        }
        else if (num == 27) {//うさぎ
            Vec2 charaAnchor = Vec2(.5, .359);
            Vec2 cloudAnchor = Vec2(.389, .688);
            FiniteTimeAction *cloudBounce = nullptr;
            if (!DataManager::sharedManager()->getEnableFlagWithFlagID(45)) {
                cloudBounce = cloudBounceAction(parent, {"sup_27_rabbit_1.png"}, charaAnchor, "sup_27_cloud_1.png", cloudAnchor, "");
            }
            else if (!DataManager::sharedManager()->getEnableFlagWithFlagID(46)) {
                cloudBounce = cloudBounceAction(parent, {"sup_27_rabbit_2.png", "sup_27_dice_1.png"}, charaAnchor, "sup_27_cloud_2.png", cloudAnchor, "");
            }
            else if (!DataManager::sharedManager()->getEnableFlagWithFlagID(47)) {
                cloudBounce = cloudBounceAction(parent, {"sup_27_rabbit_2.png", "sup_27_dice_1.png", "sup_27_dice_2.png"}, charaAnchor, "sup_27_cloud_3.png", cloudAnchor, "");
            }
            
            if (cloudBounce != nullptr) {
                actions.pushBack(cloudBounce);
            }
        }
        else if (num == 72) {
            Vec2 charaAnchor = Vec2(.5, .121);
            Vec2 cloudAnchor = Vec2(.389, .688);
            actions.pushBack(cloudBounceAction(parent, {"sup_72_pig_1.png"}, charaAnchor, "sup_72_cloud.png", cloudAnchor, "pig.mp3"));
        }
        
        //コールバック
        auto call = CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 栓抜きあげる
    else if (key == "opener") {
        Common::playSE("p.mp3");

        //栓抜きを渡す
        auto opener = createSpriteToCenter("sup_18_opener.png", true, parent, true);
        actions.pushBack(giveAction(opener, 1, .7, 1));
        
        //ジュースを持つ
        auto juice = parent->getChildByName("sup_18_orange.png");
        auto pig = parent->getChildByName("sup_18_pig_0.png");
        auto pig_have = createSpriteToCenter("sup_18_pig_1.png", true, parent, true);
        actions.pushBack(Spawn::create(TargetedAction::create(juice, FadeOut::create(.5)),
                                       createChangeAction(.5, .5, pig, pig_have),NULL));
        actions.pushBack(DelayTime::create(.4));
        
        //栓抜きをジュースにかける & 鳴く
        auto pig_useOpener = createSpriteToCenter("sup_18_pig_2.png", true, parent, true);
        actions.pushBack(createChangeAction(.5, .5, pig_have, pig_useOpener, "pig.mp3"));
        
        //プルプル
        auto seq = Sequence::create(MoveTo::create(.1, Vec2(pig_useOpener->getPositionX()-parent->getContentSize().width*.007,pig_useOpener->getPositionY())),
                                    MoveTo::create(.1, Vec2(pig_useOpener->getPositionX()+parent->getContentSize().width*.007,pig_useOpener->getPositionY())), NULL);
        actions.pushBack(TargetedAction::create(pig_useOpener, Repeat::create(seq, 10)));
        actions.pushBack(DelayTime::create(.7));
        
        //栓抜ける
        auto pig_usedOpener = createSpriteToCenter("sup_18_pig_3.png", true, parent, true);
        actions.pushBack(createChangeAction(.2, .2, pig_useOpener, pig_usedOpener, "po.mp3"));
        actions.pushBack(DelayTime::create(1.5));
        
        //ジュースをつぐ演出を入れる。, "beer.mp3"
        auto pig_juice = createAnchorSprite("sup_18_pig_4.png", .5, .367, true, parent, true);// createSpriteToCenter("sup_18_pig_4.png", true, parent, true);
        actions.pushBack(createChangeAction(.2, .2, pig_usedOpener, pig_juice));
        
        std::vector<float> x_poses = {.267, .483, .698, .902};
        int i = 0;
        for (auto x_pos : x_poses) {
            auto juice_glass = createSpriteToCenter(StringUtils::format("sup_18_juiceglass_%d.png",i), true, parent, true);
            
            //ジュースを入れる
            auto move = MoveTo::create(.4, Vec2(parent->getContentSize().width*x_pos, pig_juice->getPositionY()));
            auto repeatBounce = Repeat::create(createBounceAction(pig_juice, .4, .1), 3);
            auto spawn = Spawn::create(repeatBounce, createSoundAction("beer.mp3"), NULL);
            actions.pushBack(TargetedAction::create(pig_juice, Sequence::create(move, spawn, NULL)));
            
            //ジュース入れた。フェードイン
            actions.pushBack(TargetedAction::create(juice_glass, FadeIn::create(.4)));
            actions.pushBack(DelayTime::create(.5));
            
            i++;
        }

        //通常の豚に戻す
        actions.pushBack(createChangeAction(.4, .4, pig_juice, pig));
        actions.pushBack(DelayTime::create(.7));

        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark レジ入力
    else if(key.compare(0,4,"regi")==0)
    {
        Common::playSE("keyboard.mp3");
        int index = atoi(key.substr(5,key.length()-4).c_str());//判別id
        
        //ボタンを押下
        auto spr = parent->getChildByName<Sprite*>(StringUtils::format("sup_13_regi_%d.png",index));
        Vector<FiniteTimeAction*>actions;
        auto fadeout=FadeOut::create(0);
        auto delay=DelayTime::create(.1);
        auto fadein=FadeIn::create(0);
        auto seq=TargetedAction::create(spr, Sequence::create(fadeout,delay,fadein, NULL));
        actions.pushBack(seq);
        
        //鍵を刺した後かどうか
        bool condition = DataManager::sharedManager()->getEnableFlagWithFlagID(39.5);
        
        //正解判定
        auto call=CallFunc::create([this, parent, callback, index, condition](){
            if (index == 10) {
                log("レジ;;;;;;%s_%d",AnswerManager::getInstance()->getPasscode().c_str(), index);
                auto labelMap=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Label);
                //答えチェック
                auto answer=labelMap["answer"];
                AnswerManager::getInstance()->checkAnswer(CustomAction_Label, answer,false, [this,callback](bool success){
                    if (!success) {
                        AnswerManager::getInstance()->resetPasscode();
                        setRegiText("");
                        
                        if (DataManager::sharedManager()->getEnableFlagWithFlagID(39.5)) {//レジ入力モードなら
                            Common::playSE("boo.mp3");
                        }
                    }
                    else {//正解
                        Common::playSE("pinpon.mp3");
                    }
                   
                    callback(success);
                });
            }
            else {
                if (condition) {
                    setRegiText(StringUtils::format("%d",index));
                }
                else {
                    setRegiText("");
                }
                
                callback(false);
            }
        });
        actions.pushBack(call);
        parent->runAction(Spawn::create(actions));
    }
#pragma mark 黒板消し
    else if(key == "eraser") {
        Common::playSE("p.mp3");

        auto eraser = createSpriteToCenter("sup_14_eraser.png", true, parent, true);
        
        //黒板配置
        auto fadein_eraser = TargetedAction::create(eraser, FadeIn::create(.4));
        actions.pushBack(fadein_eraser);
        actions.pushBack(DelayTime::create(.7));

        //ゴシゴシ&黒板切り替え
        float duration = .4;
        Vector<FiniteTimeAction* >spawnActions;
        Vector<FiniteTimeAction* >goshigoshiActions;
        std::vector<Vec2> positions = {Vec2(.2, .4), Vec2(.32, .75), Vec2(.44, .4), Vec2(.56, .75), Vec2(.68, .4), Vec2(.8, .8)};
        for (int i = 0; i < positions.size(); i++) {
            auto pos = positions[i];
            if (i == 0) {
                eraser->setPosition(parent->getContentSize().width*pos.x, parent->getContentSize().height*pos.y);
                goshigoshiActions.pushBack(createSoundAction("goshigoshi.mp3"));
            }
            else {
                auto easeIn = EaseIn::create(MoveTo::create(.4, Vec2(parent->getContentSize().width*pos.x, parent->getContentSize().height*pos.y)), 1);
                goshigoshiActions.pushBack(TargetedAction::create(eraser, easeIn));
            }
        }
        goshigoshiActions.pushBack(DelayTime::create(.5));
        goshigoshiActions.pushBack(TargetedAction::create(eraser, FadeOut::create(.3)));
        spawnActions.pushBack(Sequence::create(goshigoshiActions));
        
        Vector<FiniteTimeAction* >boardChangeActions;
        auto board_0 = createSpriteToCenter("sup_14_board_0.png", true, parent, true);
        auto board_1 = createSpriteToCenter("sup_14_board_1.png", true, parent, true);
        boardChangeActions.pushBack(DelayTime::create(duration*3));
        boardChangeActions.pushBack(TargetedAction::create(board_0, FadeIn::create(duration)));
        boardChangeActions.pushBack(DelayTime::create(duration*1.5));
        boardChangeActions.pushBack(createChangeAction(duration, duration, board_0, board_1));
        spawnActions.pushBack(Sequence::create(boardChangeActions));
        actions.pushBack(Spawn::create(spawnActions));
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 猫の手
    else if (key.compare(0,10,"cat_input_")==0) {
        Common::playSE("cat_0.mp3");
        
        auto inputKey = key.substr(4, 7);
        log("いんぷっと%s",inputKey.c_str());
        
        int index=atoi(key.substr(10, 1).c_str());
        auto arm = parent->getChildByName(StringUtils::format("sup_17_cathand_%d.png",index));
        Vec2 anchorPoint = (index == 0) ? Vec2(.193, .565) : Vec2(.612, .565);
        arm->setAnchorPoint(anchorPoint);
        arm->setPosition(anchorPoint.x*parent->getContentSize().width, anchorPoint.y*parent->getContentSize().height);
        
        //アニメーション
        Vector<FiniteTimeAction*>actions;
        auto move_distance = -parent->getContentSize().height*.02;
        auto duration_move = .4;
        auto move_0 = MoveBy::create(duration_move, Vec2(0, move_distance));
        auto scale_0 = EaseIn::create(ScaleTo::create(duration_move, 1.2, .5),2);
        auto spawn_0 = Spawn::create(move_0,scale_0, NULL);
        auto move_1 = MoveBy::create(duration_move, Vec2(0, -move_distance));
        auto scale_1 = EaseOut::create(ScaleTo::create(duration_move, 1,1), 2);
        auto spawn_1 = Spawn::create(move_1,scale_1, NULL);
        auto seq = TargetedAction::create(arm, Sequence::create(spawn_0,spawn_1,NULL));
        actions.pushBack(seq);
        
        //コールバック.TouchManagerに正解判定任す。
        auto callFunc = CallFunc::create([inputKey, parent, callback](){
            TouchManager::getInstance()->customAction(inputKey, parent, false, callback);
        });
        actions.pushBack(callFunc);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark タイヤセット
    else if (key == "tire") {
        Common::playSE("p.mp3");
 
        //タイヤをセット
        auto tireSup = createSpriteToCenter("sup_53_wheel.png", true, parent, true);
        actions.pushBack(TargetedAction::create(tireSup, FadeIn::create(.4)));
        actions.pushBack(DelayTime::create(1.2));
        
        //back_52に遷移
        auto back52 = createSpriteToCenter("back_52.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back52, 52);
        auto pig_wait = back52->getChildByName("sup_52_pig_1.png");
        pig_wait->setCascadeOpacityEnabled(true);
        auto wheel = createSpriteToCenter("sup_52_wheel.png", true, pig_wait, true);
        actions.pushBack(Spawn::create(TargetedAction::create(back52, FadeIn::create(.5)),
                                       TargetedAction::create(wheel, FadeIn::create(.5)), NULL));
        actions.pushBack(DelayTime::create(1));

        //豚走る画像に切り替え
        actions.pushBack(createSoundAction("pig.mp3"));
        auto pig_run = createSpriteToCenter("sup_52_pig_2.png", true, back52, true);
        actions.pushBack(createChangeAction(.5, .5, pig_wait, pig_run));
        actions.pushBack(DelayTime::create(.7));
        
        //豚アップ&移動
        float spawnDuration = 1.6;
        Vector<FiniteTimeAction*> pigSpawnActions;
        pigSpawnActions.pushBack(createSoundAction("chain.mp3"));
        pigSpawnActions.pushBack(ScaleTo::create(spawnDuration, 1.8));
        int jumpCount = 13;
        pigSpawnActions.pushBack(jumpAction(parent->getContentSize().height*.03, spawnDuration/jumpCount, pig_run, 1.5, jumpCount));
        pigSpawnActions.pushBack(MoveTo::create(spawnDuration, pig_run->getContentSize()*-.5));
        actions.pushBack(TargetedAction::create(pig_run, Spawn::create(pigSpawnActions)));
        
        //暗転
        auto black = Sprite::create();
        black->setTextureRect(Rect(0, 0, parent->getContentSize().width, parent->getContentSize().height));
        black->setPosition(parent->getContentSize()/2);
        black->setColor(Color3B::BLACK);
        black->setOpacity(0);
        parent->addChild(black);
        actions.pushBack(TargetedAction::create(black, FadeIn::create(.5)));
        actions.pushBack(createSoundAction("gasagoso.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        //back1
        auto back1 = createSpriteToCenter("back_1.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back1, 1);
        actions.pushBack(createChangeAction(.3, .3, black, back1));
        
        //豚表示
        Vec2 pig_run_anchor = Vec2(.492, .152);
        auto boggie_1 = AnchorSprite::create(pig_run_anchor, parent->getContentSize(), true, "sup_1_bogie_1.png");
        boggie_1->setPositionX(parent->getContentSize().width*1.5);
        parent->addChild(boggie_1);
        auto pig_0 = AnchorSprite::create(pig_run_anchor, parent->getContentSize(), true, "sup_1_pig_0.png");
        pig_0->setPositionX(boggie_1->getPositionX());
        parent->addChild(pig_0);
        auto pig_1 = AnchorSprite::create(pig_run_anchor, parent->getContentSize(), true, "sup_1_pig_1.png");
        pig_1->setPositionX(boggie_1->getPositionX());
        parent->addChild(pig_1);
        
        auto shownPigCall = CallFunc::create([boggie_1, pig_0](){
            boggie_1->setOpacity(255);
            pig_0->setOpacity(boggie_1->getOpacity());
        });
        actions.pushBack(shownPigCall);
        
        //豚ガタガタ言いながら走って来る
        actions.pushBack(createSoundAction("chain.mp3"));
        Vector<FiniteTimeAction*> pigRunActions;
        float pig_run_duration = 1.5;
        float pig_run_jumpCount = 6;
        
        auto pig_run_jump = JumpTo::create(pig_run_duration, Vec2(parent->getContentSize().width/2, pig_1->getPositionY()), parent->getContentSize().height*.01, pig_run_jumpCount);
        auto pig_run_spawn = Spawn::create(TargetedAction::create(boggie_1, pig_run_jump),
                                           TargetedAction::create(pig_0, pig_run_jump->clone()),
                                           TargetedAction::create(pig_1, pig_run_jump->clone()), NULL);
        
        float changeCount = 3;
        float changeDuration = pig_run_duration/changeCount/4;
        
        
        auto pig_change = Repeat::create(Sequence::create(DelayTime::create(changeDuration),
                                                          createChangeAction(changeDuration, changeDuration, pig_0, pig_1),
                                                          DelayTime::create(changeDuration),
                                                          createChangeAction(changeDuration, changeDuration, pig_1, pig_0),
                                                          NULL), (int)changeCount);
        
        actions.pushBack(Spawn::create(pig_run_spawn,
                                       pig_change, NULL));
        
        //ブレーキ
        int skewX = 18;
        auto skew = EaseOut::create(SkewBy::create(.3, -skewX, 0), 1);
        auto skewBack = EaseInOut::create(SkewBy::create(.12, skewX, 0), 1);
        auto skew_seq = Sequence::create(skew, skewBack, NULL);
        actions.pushBack(Spawn::create(TargetedAction::create(boggie_1,  skew_seq),
                                       TargetedAction::create(pig_0, skew_seq->clone()),
                                       TargetedAction::create(pig_1, skew_seq->clone()), NULL));
        
        //<荷物降ろす豚>
        auto beercase_1 = createSpriteToCenter("sup_1_beercase_1.png", true, parent, true);
        auto beercase_2 = createSpriteToCenter("sup_1_beercase_2.png", true, parent, true);
        auto pig_2 = createSpriteToCenter("sup_1_pig_2.png", true, parent, true);
        auto pig_3 = createSpriteToCenter("sup_1_pig_3.png", true, parent, true);
        auto pig_4 = AnchorSprite::create(Vec2(.177, .158), parent->getContentSize(), true, "sup_1_pig_4.png");
        parent->addChild(pig_4);
        auto pig_5 = createSpriteToCenter("sup_1_pig_5.png", true, parent, true);
        auto boggie_2 = createSpriteToCenter("sup_1_bogie_2.png", true, parent, true);
        auto boggie_3 = createSpriteToCenter("sup_1_bogie_3.png", true, parent, true);

        float pig_convey_duration = 1;
        //豚荷物の前に移動
        actions.pushBack(createChangeAction(.3, .3, {pig_0, pig_1}, {pig_2}));
        actions.pushBack(DelayTime::create(pig_convey_duration));
        
        //荷物を置く
        actions.pushBack(createSoundAction("goto.mp3"));
        actions.pushBack(createChangeAction(.3, .3, {pig_2, boggie_1}, {pig_3, boggie_2}));
        actions.pushBack(DelayTime::create(pig_convey_duration));

        //荷物を持つ
        actions.pushBack(createChangeAction(.3, .3, {pig_3}, {pig_2, beercase_1}));
        actions.pushBack(DelayTime::create(pig_convey_duration));
        
        //荷物を置く
        actions.pushBack(createSoundAction("goto.mp3"));
        actions.pushBack(createChangeAction(.3, .3, {pig_2, boggie_2}, {pig_3, boggie_3}));
        actions.pushBack(DelayTime::create(pig_convey_duration));
        
        //手ぶら
        float teburaDuration = pig_convey_duration*2;
        int tiredCount = 3;
        actions.pushBack(createChangeAction(.3, .3, {pig_3}, {pig_4, beercase_2}));
        actions.pushBack(Repeat::create(createBounceAction(pig_4, teburaDuration/tiredCount, .1), tiredCount));
        
        //台車を持つ
        actions.pushBack(createChangeAction(.3, .3, {pig_4, boggie_3}, {pig_5}));
        actions.pushBack(DelayTime::create(pig_convey_duration));
        
        //台車走り去る
        actions.pushBack(createSoundAction("pig.mp3"));
        actions.pushBack(createSoundAction("chain.mp3"));
        Vector<FiniteTimeAction*> pig_runaway_Actions;
        pig_runaway_Actions.pushBack(ScaleTo::create(3, 2));
        pig_runaway_Actions.pushBack(JumpTo::create(3, -1*pig_5->getContentSize(), parent->getContentSize().height*.05, 12));
        actions.pushBack(TargetedAction::create(pig_5, Spawn::create(pig_runaway_Actions)));
        
        //コールバック
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 木札セット
    else if (key == "sticker") {
        Common::playSE("p.mp3");

        //木札配置
        auto stickerSup = createSpriteToCenter("sup_51_sticker.png", true, parent, true);
        actions.pushBack(TargetedAction::create(stickerSup, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.5));
        
        //コールバック
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 木槌
    else if (key == "hammer") {
        Common::playSE("p.mp3");
        auto hammer=AnchorSprite::create(Vec2(.877, .611), parent->getContentSize(), "sup_68_hammer.png");
        hammer->setOpacity(0);
        hammer->setLocalZOrder(1);
        parent->addChild(hammer);
        
        auto lid=parent->getChildByName<Sprite*>("sup_68_lid.png");
        
        auto bottle=createSpriteToCenter("sup_68_handle.png", true, parent);
        parent->addChild(bottle);
        
        auto open=createSpriteToCenter("sup_68_lid_1.png", true, parent);
        parent->addChild(open);
        
        
        auto openAction=Spawn::create(createSoundAction("paki.mp3"),
                                      createChangeAction(.2, .2, lid, open), NULL);
        
        auto seq_hammer=TargetedAction::create(hammer, Sequence::create(FadeIn::create(1),
                                                                        DelayTime::create(.5),
                                                                        MoveBy::create(.5, Vec2(-parent->getContentSize().width*.025, parent->getContentSize().height*.02)),
                                                                        RotateBy::create(.3, 10),
                                                                        DelayTime::create(1),
                                                                        Spawn::create(EaseIn::create(RotateBy::create(.2, -45), 1.3),
                                                                                      ScaleBy::create(.2, .8), NULL),
                                                                        openAction,
                                                                        Spawn::create(EaseOut::create(RotateBy::create(.2, 45), 1.3),
                                                                                      ScaleBy::create(.2, 1.0/.8), NULL),
                                                                        DelayTime::create(.5), NULL));
        
        auto fadeouts=Spawn::create(TargetedAction::create(open, FadeOut::create(.7)),
                                    TargetedAction::create(hammer, FadeOut::create(.7)),
                                    TargetedAction::create(bottle, FadeIn::create(.3))
                                    , NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(seq_hammer,fadeouts,DelayTime::create(.7),call, NULL));
    }
#pragma mark ガスはめる
    else if (key == "gas") {
        Common::playSE("p.mp3");
        
        //ガスをはめる
        auto open = parent->getChildByName<Sprite*>("sup_35_open.png");
        auto gas = createSpriteToCenter("sup_35_gas.png", true, parent, true);
        actions.pushBack(createSoundAction("koto.mp3"));
        actions.pushBack(createChangeAction(.5, .5, open, gas));
        actions.pushBack(DelayTime::create(.5));
        
        //蓋閉じる
        auto close = createSpriteToCenter("sup_35_close.png", true, parent, true);
        actions.pushBack(createSoundAction("door_close.mp3"));
        actions.pushBack(createChangeAction(.5, .5, gas, close));
        actions.pushBack(DelayTime::create(1.5));
        
        //着火
        auto fireCall = CallFunc::create([this, parent](){
            this->showFireParticle(parent,DataManager::sharedManager()->getNowBack());
        });
        actions.pushBack(Spawn::create(fireCall,
                                       createSoundAction("huo.mp3"), NULL));
        actions.pushBack(DelayTime::create(1));
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark サイコロをうさぎにあげる
    else if (key == "setDice") {
        Common::playSE("p.mp3");
        
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(45)) {//バブルガム
            auto rabbit_default = parent->getChildByName<Sprite*>("sup_27_rabbit_1.png");
            auto rabbit_have = createSpriteToCenter("sup_27_rabbit_2.png", true, parent, true);
            createSpriteToCenter("sup_27_dice_1.png", false, rabbit_have, true);
            
            actions.pushBack(createChangeAction(.5, .5, rabbit_default, rabbit_have));
            actions.pushBack(DelayTime::create(.5));
        }
        else if (!DataManager::sharedManager()->getEnableFlagWithFlagID(46)) {//レモン
            auto dice = createSpriteToCenter("sup_27_dice_2.png", true, parent, true);
            actions.pushBack(TargetedAction::create(dice, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(.5));
        }
        else if (!DataManager::sharedManager()->getEnableFlagWithFlagID(47)) {//スプリング
            //サイコロあげる
            auto dice_3 = createAnchorSprite("sup_27_dice_3.png", .515, .539, true, parent, true);
            
            actions.pushBack(TargetedAction::create(dice_3, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(.5));
            
            //サイコロを振りかぶる
            float changeDuration = .5;
            auto rabbit = parent->getChildByName<Sprite*>("sup_27_rabbit_2.png");
            auto dice_1 = parent->getChildByName<Sprite*>("sup_27_dice_1.png");
            dice_1->setAnchorPoint(Vec2(.473, .539));
            dice_1->setPosition(parent->getContentSize().width*dice_1->getAnchorPoint().x, parent->getContentSize().height*dice_1->getAnchorPoint().y);
            auto dice_2 = parent->getChildByName<Sprite*>("sup_27_dice_2.png");
            dice_2->setAnchorPoint(Vec2(.555, .539));
            dice_2->setPosition(parent->getContentSize().width*dice_2->getAnchorPoint().x, parent->getContentSize().height*dice_2->getAnchorPoint().y);

            auto rabbit_upper = createSpriteToCenter("sup_27_rabbit_3.png", true, parent, true);

            actions.pushBack(createChangeAction(changeDuration, changeDuration, {rabbit, dice_1, dice_2, dice_3}, {rabbit_upper}));
            actions.pushBack(DelayTime::create(.6));
            
            //ジャンプ
            Vec2 charaAnchor = Vec2(.5, .359);
            actions.pushBack(bounceJumpAction(rabbit_upper, charaAnchor, parent, "pop_1.mp3", 2));
            actions.pushBack(DelayTime::create(1));
            
            //サイコロをふる
            actions.pushBack(createChangeAction(.2, .2, {rabbit_upper}, {rabbit, dice_1, dice_2, dice_3}, "dice.mp3"));
            
            //サイコロ落下
            Vector<Sprite*> dices = {dice_1, dice_2, dice_3};
            std::vector<Vec2> dice_poses = {Vec2(.5, .202), Vec2(.535, .158), Vec2(.463, .158)};
            Vector<FiniteTimeAction*> falls;
            int l = 0;
            for (auto dice : dices) {
                auto pos = dice_poses[l];
                auto fall = EaseIn::create(MoveTo::create(.9, Vec2(parent->getContentSize().width*pos.x, parent->getContentSize().height*pos.y)), 3);
                auto fall_se = createSoundAction("fall.mp3");
                auto fall_scale = ScaleTo::create(.9, .95);
                auto fall_spawn = Spawn::create(fall, fall_se, fall_scale, NULL);
                
                //ジャンプしながら回転する
                auto rotate = RotateBy::create(.9, 360*(l+1));
                auto jump = jumpAction(parent->getContentSize().height*.05, .3, dice, 2, 3);
                auto round_spawn = Spawn::create(rotate,
                                                 jump,
                                                 createSoundAction("dice.mp3"), NULL);
                
                falls.pushBack(TargetedAction::create(dice, Sequence::create(fall_spawn, round_spawn, NULL)));
                l++;
            }
            
            //ダイスを切り替え。
            auto allDice = createSpriteToCenter("sup_27_dice_all.png", true, parent, true);
            falls.pushBack(Sequence::create(DelayTime::create(1.9),
                                            createChangeAction(.1, .1, {dice_1, dice_2, dice_3}, {allDice}), NULL));
            
            actions.pushBack(Spawn::create(falls));
            actions.pushBack(DelayTime::create(.6));
            
            //うさぎ入れ替え
            auto rabbit_default = createSpriteToCenter("sup_27_rabbit_1.png", true, parent, true);
            actions.pushBack(createChangeAction(.3, .3, rabbit, rabbit_default));
        }
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark ドライバー
    else if (key == "driver") {
        
        //ドライバーカタカタ
        std::vector<Vec2> positions = {Vec2(.117, .741), Vec2(.878, .741), Vec2(.118, .538), Vec2(.878, .538)};
        auto driverAct = driverAction(DataManager::sharedManager()->getNowBack(), positions, parent, 2);
        actions.pushBack(driverAct);
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 延長コード
    else if (key == "extension") {
        
        auto extension = createSpriteToCenter("sup_10_extension.png", true, parent, true);
        runSetItemAction(parent, extension, callback);
    }
#pragma mark コンセント
    else if (key == "consent") {
        //back11に遷移
        auto back11 = createSpriteToCenter("back_11.png", true, parent, true);
        actions.pushBack(TargetedAction::create(back11, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(1));
        
        //ディスプレイ表示。
        auto display = createSpriteToCenter("sup_11_locked.png", true, back11, true);
        actions.pushBack(TargetedAction::create(display, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(2));
        
        //backに戻る
        actions.pushBack(TargetedAction::create(back11, FadeOut::create(.5)));

        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark グラスセット
    else if (key.compare(0,8,"setGlass")==0)
    {
        //ぐらすを生成
        auto glass = createSpriteToCenter("sup_19_glass.png", true, parent, true);
        auto glassAction = setItemAction(parent, glass);
        actions.pushBack(glassAction);
        
        bool isPour = (bool)atoi(key.substr(9, 1).c_str());

        if (isPour) {//ハンドルがセットされているなら注ぐアクション
            actions.pushBack(pourAction(parent));
        }
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark ハンドルセット
    else if (key.compare(0,9,"setHandle")==0)
    {
        //ハンドルアクションを生成
        auto handle = createSpriteToCenter("sup_23_handle.png", true, parent, true);
        auto handleAction = setItemAction(parent, handle);
        actions.pushBack(handleAction);
        
        bool isPour = (bool)atoi(key.substr(10, 1).c_str());
        //ハンドルがセットされているなら注ぐアクション
        if (isPour) {
            //back19に遷移
            auto back19 = createSpriteToCenter("back_19.png", true, parent, true);
            SupplementsManager::getInstance()->addSupplementToMain(back19, 19);
            createSpriteToCenter("sup_19_handle.png", false, back19, true);
            
            actions.pushBack(TargetedAction::create(back19, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(.6));

            actions.pushBack(pourAction(back19));
        }
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 河童にビール
    else if (key == "kappa") {
        Common::playSE("p.mp3");
        
        //ビールを置く
        auto beer = createSpriteToCenter("sup_32_beer.png", true, parent, true);
        beer->setLocalZOrder(2);
        actions.pushBack(TargetedAction::create(beer, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(1));
        
        //ビールを持つ
        auto kappa1 = parent->getChildByName<Sprite*>("sup_32_kappa_1.png");
        auto kappa2 = createSpriteToCenter("sup_32_kappa_2.png", true, parent, true);
        actions.pushBack(createChangeAction(.5, .5, {beer, kappa1}, {kappa2}));
        actions.pushBack(DelayTime::create(.4));

        //ぐびぐび
        actions.pushBack(Repeat::create(Spawn::create(createSoundAction("drink.mp3"),
                                                      createBounceAction(kappa2, .3, .05), NULL), 3));
        
        //ビールを空っぽに
        auto kappa3 = createSpriteToCenter("sup_32_kappa_3.png", true, parent, true);
        actions.pushBack(createChangeAction(.3, .3, kappa2, kappa3));
        actions.pushBack(DelayTime::create(.7));
        
        //目を回す
        auto key = createSpriteToCenter("sup_32_key.png", true, parent, true);
        auto kappa4 = AnchorSprite::create(Vec2(.628, .236), parent->getContentSize(), true, "sup_32_kappa_4.png");
        parent->addChild(kappa4);
        actions.pushBack(createChangeAction(.5, .5, kappa3, kappa4, "kyuu.mp3"));
        actions.pushBack(TargetedAction::create(key, FadeIn::create(.1)));

        //フラフラ
        auto kappa5 = createSpriteToCenter("sup_32_kappa_5.png", true, parent, true);
        auto hurahuraSeq = Sequence::create(EaseInOut::create(Sequence::create(RotateTo::create(1, 30),
                                                                               createSoundAction("pop_1.mp3"),
                                                                               NULL), 3),
                                            EaseInOut::create(Sequence::create(RotateTo::create(1, -30),
                                                                               createSoundAction("pop_1.mp3"),
                                                                               NULL), 3),
                                            NULL);
        
        auto hurahuraSeq2 = Sequence::create(EaseInOut::create(Sequence::create(RotateTo::create(1, 30),
                                                                                createSoundAction("pop_1.mp3"),
                                                                                NULL), 3),
                                             EaseInOut::create(Sequence::create(RotateTo::create(1, -40),
                                                                                createSoundAction("pop_2.mp3"),
                                                                                NULL), 3), NULL);
        actions.pushBack(Sequence::create(TargetedAction::create(kappa4, hurahuraSeq),
                                          Spawn::create(TargetedAction::create(kappa4, hurahuraSeq2),
                                                        Sequence::create(DelayTime::create(1.8),
                                                                         createChangeAction(.2, .2, kappa4, kappa5),
                                                                         NULL),
                                                        NULL),
                                          NULL));
        actions.pushBack(DelayTime::create(.5));
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark トングでサイコロ取得
    else if (key == "tong") {
        Common::playSE("p.mp3");
        
        //トング表示
        auto tong = createSpriteToCenter("sup_76_tong.png", true, parent, true);
        actions.pushBack(TargetedAction::create(tong, FadeIn::create(.5)));
        
        //トング移動
        actions.pushBack(TargetedAction::create(tong, MoveTo::create(.7, Vec2(parent->getContentSize().width*.35, parent->getContentSize().height*.55))));
        
        //くねくね
        auto kunekuneSeq = Sequence::create(RotateTo::create(.3, 10),
                                            RotateTo::create(.3, -10), NULL);
        actions.pushBack(TargetedAction::create(tong, Repeat::create(kunekuneSeq, 2)));
        actions.pushBack(DelayTime::create(.8));
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark お盆あげる
    else if (key == "giveobon") {
        Common::playSE("p.mp3");
        
        //お盆あげる
        auto obon = createSpriteToCenter("sup_72_obon.png", true, parent, true);
        actions.pushBack(giveAction(obon, .7, .95, .7));
        
        //お盆所持状態に切り替え
        auto pig1 = parent->getChildByName<Sprite*>("sup_72_pig_1.png");
        auto pig2 = createSpriteToCenter("sup_72_pig_2.png", true, parent, true);
        actions.pushBack(createSoundAction("pop_1.mp3"));
        actions.pushBack(createChangeAction(.5, .5, pig1, pig2));
        actions.pushBack(DelayTime::create(1.2));
        
        //back73に遷移
        auto back73 = createSpriteToCenter("back_73.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back73, 73);
        actions.pushBack(createChangeAction(.5, .5, pig2, back73));
        actions.pushBack(DelayTime::create(1.1));
        
        //豚登場
        auto pig3 = AnchorSprite::create(Vec2(.725, .715), parent->getContentSize(), true, "sup_73_pig.png");
        pig3->setCascadeOpacityEnabled(true);
        back73->addChild(pig3);
        //createSpriteToCenter("sup_73_pig.png", true, back73, true);
        auto pig3_lefthand = AnchorSprite::create(Vec2(.878, .88), pig3->getContentSize(), "sup_73_hand_left.png");
        pig3->addChild(pig3_lefthand);
        auto pig3_righthand = AnchorSprite::create(Vec2(.662, .903), pig3->getContentSize(), "sup_73_hand_right.png");
        pig3->addChild(pig3_righthand);
        actions.pushBack(TargetedAction::create(pig3, FadeIn::create(.5)));
        
        //グニョグニョ&手を動かす
        int rotate = 15;
        float handDuration = .15;
        
        auto gunyo = createBounceAction(pig3, handDuration, .1);
        
        auto moveRightHandAction = TargetedAction::create(pig3_righthand, Sequence::create(RotateTo::create(handDuration, -rotate),
                                                                                           RotateTo::create(handDuration, 0),
                                                                                           NULL));
        auto moveLeftHandAction = TargetedAction::create(pig3_lefthand, Sequence::create(RotateTo::create(handDuration, rotate),
                                                                                          RotateTo::create(handDuration, 0),
                                                                                          NULL));
        auto movehandSpawn = Spawn::create(moveRightHandAction, moveLeftHandAction, gunyo, NULL);
        actions.pushBack(Spawn::create(Repeat::create(movehandSpawn, 10),
                                       createSoundAction("pig.mp3"),
                                       NULL));

        //back73フェードアウト
        auto pig4 = createSpriteToCenter("sup_72_pig_3.png", true, parent, true);
        auto dishes = parent->getChildByName<Sprite*>("sup_72_dishes.png");
        actions.pushBack(createChangeAction(1, 1, {back73, dishes}, {pig4}));
        actions.pushBack(DelayTime::create(.5));

        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark レジに鍵をセット
    else if (key == "key_regi") {
        //鍵をさす
        auto key_1 = createSpriteToCenter("sup_13_key_1.png", true, parent, true);
        actions.pushBack(createSoundAction("pisi.mp3"));
        actions.pushBack(TargetedAction::create(key_1, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(1));
        
        //鍵をひねる
        auto key_2 = createSpriteToCenter("sup_13_key_2.png", true, parent, true);
        actions.pushBack(createChangeAction(.5, .5, key_1, key_2, "cha.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        //画面点灯
        auto display = createSpriteToCenter("sup_13_display.png", true, parent, true);
        actions.pushBack(TargetedAction::create(display, FadeIn::create(.6)));

        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark タッチパネル解錠
    else if (key == "unlock") {
        //オーダー表表示
        auto order = createSpriteToCenter("sup_11_order.png", true, parent, true);
        actions.pushBack(TargetedAction::create(order, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.5));
        
        //猿の手表示
        auto monkeyHand = AnchorSprite::create(Vec2(.5, .918), parent->getContentSize(), true, "sup_11_monkey.png");
        monkeyHand->setPositionY(.5*parent->getContentSize().height);
        monkeyHand->setLocalZOrder(2);
        parent->addChild(monkeyHand);
        actions.pushBack(TargetedAction::create(monkeyHand, FadeIn::create(.5)));

        //移動、ボタン押しループ
        std::vector<Vec2> poses = {Vec2(.275, .546), Vec2(.717, .546), Vec2(.1, .309), Vec2(.625, .309), Vec2(.8, .773)};
        Vector<FiniteTimeAction*> roopActions;
        
        int i = 0;
        for (auto pos : poses) {
            auto countSp = createSpriteToCenter(StringUtils::format("sup_11_order_%d.png",i+1), true, parent, true);
            
            roopActions.pushBack(MoveTo::create(.8, Vec2(pos.x*parent->getContentSize().width, pos.y*parent->getContentSize().height)));
            
            roopActions.pushBack(ScaleTo::create(.2, .9));
            roopActions.pushBack(createSoundAction("pi.mp3"));
            roopActions.pushBack(TargetedAction::create(countSp, FadeIn::create(0.01)));
            roopActions.pushBack(ScaleTo::create(.2, 1));

            
            if (i == poses.size()-2) {
                roopActions.pushBack(DelayTime::create(.8));
            }
            else {
                roopActions.pushBack(DelayTime::create(.5));
            }
            
            i++;
        }
        roopActions.pushBack(DelayTime::create(1));
        
        //猿の手が下に行きながら消える
        auto fadeoutMonkey = Spawn::create(MoveBy::create(.3, Vec2(0, -parent->getContentSize().height*.1)),
                                           FadeOut::create(.3),
                                           NULL);
        roopActions.pushBack(fadeoutMonkey);
        actions.pushBack(TargetedAction::create(monkeyHand, Sequence::create(roopActions)));        
        actions.pushBack(DelayTime::create(.5));
        
        //back9に遷移
        auto back9 = createSpriteToCenter("back_9.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back9, 9);
        createSpriteToCenter("sup_9_display_2.png", false, back9, true);
        actions.pushBack(TargetedAction::create(back9, FadeIn::create(.3)));
        actions.pushBack(DelayTime::create(.5));
        
        //豚配達
        auto pig = createSpriteToCenter("sup_9_pig.png", true, parent, true);
        actions.pushBack(createSoundAction("pig.mp3"));
        actions.pushBack(TargetedAction::create(pig, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(1.5));

        //猿の歓喜の声
        actions.pushBack(createSoundAction("monkey.mp3"));
        
        //ドリンク配達
        auto drink = createSpriteToCenter("sup_9_drink.png", true, parent, true);
        actions.pushBack(createChangeAction(.4, .4, pig, drink));
        actions.pushBack(DelayTime::create(.5));
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark トイレに鍵をセット
    else if (key == "key_toilet") {
        //鍵をさす
        auto key_1 = createSpriteToCenter("sup_45_key_0.png", true, parent, true);
        actions.pushBack(createSoundAction("pisi.mp3"));
        actions.pushBack(TargetedAction::create(key_1, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(1));
        
        //鍵をひねる
        auto key_2 = createSpriteToCenter("sup_45_key_1.png", true, parent, true);
        actions.pushBack(createChangeAction(.5, .5, key_1, key_2, "cha.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 猫をセット
    else if (key == "setCat") {
        //猫をセット
        auto cat = createSpriteToCenter("sup_17_cat.png", true, parent, true);
        createSpriteToCenter("sup_17_cathand_0.png", false, cat, true);
        actions.pushBack(createSoundAction("koto.mp3"));
        actions.pushBack(TargetedAction::create(cat, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(1));
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 手紙発見！クリア
    else if (key == "clear") {
        
        
        auto storyLayer = StoryLayer::create(StoryKey_Ending, [](Ref* sen){
            
            Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            
        });
        Director::getInstance()->getRunningScene()->addChild(storyLayer);
        
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        actions.pushBack(call);
        
        
        parent->runAction(Sequence::create(actions));
    }
}

#pragma mark - Item
void IzakayaActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造 addする際はcreateSpriteOnClipを使用するとよい
    
}


#pragma mark - Custom
void IzakayaActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    
}

#pragma mark - Private
void IzakayaActionManager::setRegiText(std::string appendText)
{
    if (appendText.size() > 0) {
        AnswerManager::getInstance()->appendPasscode(appendText);
    }
    
    auto answer = AnswerManager::getInstance()->getPasscode();

    //回答を調整
    auto labelSups = SupplementsManager::getInstance()->labelSupplements;
    int labelCount = (int)labelSups.size();
    if (answer.size() > labelCount) {//3文字以上なら後ろ3文字を
        answer = answer.substr(answer.size()-labelCount, labelCount);
        AnswerManager::getInstance()->passcode = answer;
    }
  
    int i = 0;
    
    int start = labelCount-(int)answer.size();
    for (auto label : labelSups) {
        auto text = (i>=start)? answer.substr(i-start, 1) : "";
        label->setString(text);
        
        log("レジ入力用のテキストは%s",text.c_str());
        if (text == "") {
            label->setOpacity(0);
        }
        else {
            label->setOpacity(255);
            log("店舗オラリ%s;;;%d",label->getTemporaryKey().c_str(), atoi(text.c_str()));
            DataManager::sharedManager()->setTemporaryFlag(label->getTemporaryKey(),atoi(text.c_str()));
        }
        i++;
    }
}

void IzakayaActionManager::showNabeParticle(Node *parent, int backID, int num, float duration)
{
    auto smokeName = "smokeParticle";

    auto particle=ParticleSystemQuad::create("smoke.plist");
    particle->setAutoRemoveOnFinish(true);
    particle->setName(smokeName);
    
    float xPosRatio = 0;
    float yPosRatio = 0;
    float posVar = 0;
    float startSize = 0;
    float speed = 0;
    
    if (backID == 33) {//引き
        yPosRatio = .606;

        if (num == 0) {
            xPosRatio = .426;
        }
        else if (num == 1) {
            xPosRatio = .477;
        }
        else {
            xPosRatio = .528;
        }
        
        posVar = parent->getContentSize().width*.03;
        startSize = parent->getContentSize().width*.045;
        speed = parent->getContentSize().height*.35;
    }
    else if (backID == 35) {//アップ
        yPosRatio = .435;
        
        if (num == 0) {
            xPosRatio = .293;
        }
        else if (num == 1) {
            xPosRatio = .5;
        }
        else {
            xPosRatio = .644;
        }
        
        posVar = parent->getContentSize().width*.1;
        startSize = parent->getContentSize().width*.1;
        speed = parent->getContentSize().height*.5;
    }
    
    particle->setPosition(parent->getContentSize().width*xPosRatio, parent->getContentSize().height*yPosRatio);
    particle->setPosVar(Vec2(posVar, 0));
    particle->setSpeed(speed);
    particle->setTotalParticles(200);
    particle->setAngle(135-45*num);
    particle->setAngleVar(10);
    particle->setLife(2);
    particle->setStartSize(startSize);
    particle->setEndSize(particle->getStartSize()*2);
    particle->setLocalZOrder(1);
    particle->setDuration(duration);
    parent->addChild(particle);
}

FiniteTimeAction* IzakayaActionManager::pourAction(Node* parent)
{
    Vector<FiniteTimeAction*> pourActions;

    Vec2 handleAnchor = Vec2(.5, .685);
    auto handle = parent->getChildByName("sup_19_handle.png");
    handle->setAnchorPoint(handleAnchor);
    handle->setPosition(handleAnchor.x*parent->getContentSize().width, handleAnchor.y*parent->getContentSize().height);
    
    //ビールを浮かす&入れ替え
    auto beer = parent->getChildByName<Sprite*>("sup_19_glass.png");
    Vec2 beerAnchor = Vec2(.43, .523);
    auto beer_1 = AnchorSprite::create(beerAnchor, parent->getContentSize(), true, "sup_19_glass_0.png");
    parent->addChild(beer_1);
    auto beer_2 = AnchorSprite::create(beerAnchor, parent->getContentSize(), true, "sup_19_glass_1.png");
    parent->addChild(beer_2);
    
    float beerDuration = 1;
    pourActions.pushBack(Spawn::create(TargetedAction::create(beer, MoveBy::create(beerDuration, Vec2(0, parent->getContentSize().height*.1))),
                                   Sequence::create(DelayTime::create(.4),
                                                    createChangeAction(beerDuration-.4, beerDuration-.4, beer, beer_1), NULL), NULL));
    pourActions.pushBack(DelayTime::create(.4));
    
    //ハンドル傾け。
    pourActions.pushBack(TargetedAction::create(handle, RotateTo::create(.3, -15)));
    
    //グラス傾け
    pourActions.pushBack(createSoundAction("beer.mp3"));
    auto rotateGlass = Sequence::create(EaseOut::create(RotateTo::create(.6, -15), 1), EaseOut::create(RotateTo::create(.6, 15), 1), NULL);
    auto rotateRepeat = Repeat::create(rotateGlass, 3);
    pourActions.pushBack(Spawn::create(TargetedAction::create(beer_1, rotateRepeat),
                                   TargetedAction::create(beer_2, rotateRepeat->clone()),
                                   Sequence::create(DelayTime::create(1.5),
                                                    createChangeAction(.5, .5, beer_1, beer_2), NULL), NULL));
    //グラス入れ替え
    pourActions.pushBack(DelayTime::create(.5));
    
    //ハンドルを戻す
    pourActions.pushBack(TargetedAction::create(handle, RotateTo::create(.4, 0)));
    
    //グラス消す
    pourActions.pushBack(TargetedAction::create(beer_2, FadeOut::create(.5)));
    pourActions.pushBack(DelayTime::create(.6));
    
    return Sequence::create(pourActions);
}

void IzakayaActionManager::showFireParticle(Node *parent,int backNum)
{
    auto particleBatch = ParticleBatchNode::create("particle_fire.png");
    parent->addChild(particleBatch);
        
    Vec2 pos;
    Vec2 posVar;
    float startSize = 0;
    
    if (backNum == 33) {//引き
        pos = Vec2(.473, .552);
        posVar = Vec2(parent->getContentSize().width*.045, parent->getContentSize().height*.005);
        startSize = parent->getContentSize().width*.045;
    }
    else if (backNum == 35) {//アップ
        pos = Vec2(.5, .27);
        posVar = Vec2(parent->getContentSize().width*.075, parent->getContentSize().height*.01);
        startSize = parent->getContentSize().width*.075;
    }
    
    auto particle = ParticleSystemQuad::create("fire.plist");
    particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
    particle->setAutoRemoveOnFinish(true);
    particle->setPosition(parent->getContentSize().width*pos.x, parent->getContentSize().height*pos.y);
    particle->setTotalParticles(150);
    particle->setStartColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 150));
    particle->setPosVar(posVar);
    particle->setStartSize(startSize);
    particle->setSpeed(parent->getContentSize().height*.04);
    particle->setLife(.3);
    particleBatch->addChild(particle);
}
