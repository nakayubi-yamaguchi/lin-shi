//
//  AnchorSprite.hpp
//  Airplane-mobile
//
//  Created by 成田凌平 on 2017/09/16.
//

#ifndef AnchorSprite_h
#define AnchorSprite_h

#include "cocos2d.h"
#include "EscapeStageSprite.h"

USING_NS_CC;

class AnchorSprite:public EscapeStageSprite
{
public:
    static AnchorSprite* create(Vec2 anchor,Size parentSize,std::string fileName);
    static AnchorSprite* create(Vec2 anchor,Size parentSize,bool invisible,std::string fileName);

    bool init(Vec2 anchor,Size parentSize,bool invisible,std::string fileName);
    
private:
    
};

#endif /* AnchorSprite_hpp */
