//
//  EscapeCollectionCell.h
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/09/26.


#ifndef __EscapeContainer__EscapeCollectionCell__
#define __EscapeContainer__EscapeCollectionCell__

#include "cocos2d.h"
#include "UIParts/CollectionViewCell.h"
#include "EscapeDataManager.h"

#define IconName "IconName"

USING_NS_CC;

//const char* backColorCode, const char* text, const char* textColorCode, const Vec2 position, TitleCondition condition
struct BandDetail{
    std::string backColorCode;
    std::string textColorCode;
    std::string text;
    bool isRight;
    bool isWide;
    float fontSizeRatio;
};

class EscapeCollectionCell: public CollectionViewCell
{
public:
    static EscapeCollectionCell*create(int cellIndex);
    bool init(int cellIndex);
        
    /**アイコン画像を読み込んで、ローディングが完了したら反映。*/
    static void loadingIconImage();
    
    /**希望のTitleConditionを追加。showBandを呼ぶとバンドが表示される。*/
    void addCondition(TitleCondition condition);
    
    /**配列を生成してバンドを表示*/
    void showBand();
    
    /**アプリ名やレビュー内容を表示*/
    void showDetail(const char* titleName);
private:
    std::map<TitleCondition, BandDetail> bandDetailMap;
    std::vector<TitleCondition> titleConditions;
    int m_cellIndex;
    
    void addNotification();
    void createBandDetail();
    void showBandTypes();
    
    /**時間関連のバンドを決定。BandType_UNPLAYED、BandType_NEW、BandType_PRE*/
    void addPeriodBand(int index);
    
    /**その他のバンドタイプを決定*/
    void addOtherBand(int index);
};

#endif /* defined(__EscapeContainer__EscapeCollectionCell__) */
