//
//  StampLayer.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/06/29.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________StampLayer_____
#define _____PROJECTNAMEASIDENTIFIER________StampLayer_____

#include "cocos2d.h"
typedef std::function<void()> onFinish;
typedef std::function<void(bool byMovie)> onSaved;
#define MaxGetStampCount 3//１日の最大抽選回数

USING_NS_CC;
class StampLayer : public cocos2d::LayerColor
{
public:
    static StampLayer* create(const onFinish& callback);
    /**addStampは0~5まで対応*/
    static StampLayer* create(int addStampCount,const onFinish& callback);
    /**save時のcallback付き*/
    static StampLayer* create(int addStampCount,const onFinish&callback,const onSaved& callback_onsaved);

    /**今日のスタンプ獲得数のキー*/
    static std::string getKeyOfStampCountToday();

    virtual bool init(int addStampCount,const onFinish& callback,const onSaved& callback_onsaved);
private:
    /**close時のcallback*/
    onFinish m_callback;
    /**save時のcallback*/
    onSaved m_callback_saved;
    /**アラート表示 stamp押すカウント、動画によるstampかどうか*/
    void showAlert(int addStampCount,bool byMovie);
    /**現在押されているスタンプの数*/
    int stamp_count;
    /**今日押したスタンプ数 */
    int stamp_count_today;
    /**動画読み込み失敗をカウント*/
    int movie_error_count;
    /**ボーナス情報 ボーナスがもらえるindex、枚数を*/
    std::vector<int>bonusIndexs={4,9};
    std::vector<int>bonusCounts={5,15};
    
    float stamp_width=.14;
    /**マス目の座標情報*/
    std::vector<float>xs={.134,.317,.5,.683,.866};
    std::vector<float>ys={.594,.276};
    
    Sprite*createStampCard();
    /**スタンプspriteを指定indexの箇所に作成*/
    Sprite* addStamp(Node*parent,int index);
    /**コインの画像をはる*/
    void addBonusSprite(Node*parent,int stamp_count_now,bool animate);

    /**スタンプ押すアニメーション データセーブも行う*/
    void pushStamp(Node*parent,int addStampCount,bool byMovie,const onFinish& callback);
    /**現在のスタンプ数と日の受け取り数を記録 インクリメントも行う*/
    void saveStampCounts(int getBonusCount,int addStampCount,bool byMovie);
    
    void checkConditionOfReward();
    
    void close();
    void addTouchListener();
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
