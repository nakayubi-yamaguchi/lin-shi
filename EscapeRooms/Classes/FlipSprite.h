//
//  FlipSprite.h
//  EscapeRooms
//
//  Created by yamaguchi on 2019/02/01.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FlipSprite_____
#define _____PROJECTNAMEASIDENTIFIER________FlipSprite_____

#include "cocos2d.h"
#include "AnchorSprite.h"
USING_NS_CC;

class FlipSprite:public Sprite
{
public:
    CC_SYNTHESIZE(std::string, BaseFileName, BaseFileName);
    bool m_isFlipped;
    bool getIsFlipped();
    CC_SYNTHESIZE(Vec2, FileAnchorPoint, FileAnchorPoint);
    Vector<AnchorSprite*> flipSps;
    
    /**
     検索用の名前を返す。flip_~
     @param name flip_に続く名前
     @return 検索用のキーを返す。
     */
    static std::string getSerchName(std::string name);
    /**
     ファイル名とアンカーポイントを元にフリップスプライトを生成。ファイル名はsup_~_(位置)_(0[手前] or 1[奥])
     @param fileName フリップさせるファイル名（例：sup_79_0）
     @param anchorPoint カードの中央アンカーポイント
     @return フリップスプライト
     */
    static FlipSprite* create(std::string fileName, Vec2 anchorPoint, Node* parent);

    bool init(std::string fileName, Vec2 anchorPoint, Node* parent);


    /**
     フリップアクションを取得
     @param flipDuration フリップの時間
     @return フリップアクションを取得
     */
    FiniteTimeAction* getFlipAction(float flipDuration);
    
    
    /**
     カードをターゲットにしたアクションを取得
     @param action カードに適用するアクション
     @return フリップアクションを取得
     */
    FiniteTimeAction* getActionToFlips(FiniteTimeAction* action);
    
private:
    void createCard(Node *parent);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
