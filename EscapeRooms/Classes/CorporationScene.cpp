//
//  CorporationScene.cpp
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/06/29.
//
//

#include "CorporationScene.h"
#include "TitleScene.h"
#include "EscapeCollectionScene.h"
#include "UIParts/NCIndicator.h"
#include "EscapeDataManager.h"
#include "EscapeLoadingScene.h"
#include "Utils/CustomAlert.h"
#include "Utils/FireBaseBridge.h"
#include "ConfirmAlertLayer.h"
#include "DataManager.h"
#include "UIParts/NCLoadingScene.h"
#include "Utils/BannerBridge.h"
#include "RewardPlayer/RewardPlayer.h"
#include "Utils/RewardMovieManager.h"

#include "Utils/BannerManager.h"

using namespace cocos2d;

Scene* CorporationScene::createScene() {
	auto scene = Scene::create();
	auto layer = CorporationScene::create();
	scene->addChild(layer);
	
    return scene;
}

bool CorporationScene::init() {
    if (!LayerColor::initWithColor(Color4B(255, 255, 255, 255))) {
        return false;
    }
    // 初期化
    createLogo();
    
    createIndicator();
    
    
    return true;
}

void CorporationScene::onEnterTransitionDidFinish()
{
    log("コーポレートシーンの表示が完了しました。");
    
    Common::performProcessForDebug([this](){
        //デバッグ時はポイントあげちゃう
        //DataManager::sharedManager()->addCoin(100);
        //ネイティブアドバンス広告を全て読み込み。
        BannerBridge::loadingAllNativeAdvanceAd();
        //ローディングスタート
        startLoadingData();
        
        //BannerManager::getInstance()->createBanner(BannerPosition_Bottom);
        
    }, [this](){
        
        //ネイティブアドバンス広告を全て読み込み。
        BannerBridge::loadingAllNativeAdvanceAd();
        
        //ローディングスタート
        startLoadingData();
        
        //バナー
        FireBaseBridge::getInstance()->fetchRemoteConfig("banner_type", [](bool success, Value value){
            BannerType type = (BannerType)value.asInt();
            BannerManager::getInstance()->setBannerType(type);
            BannerManager::getInstance()->createBanner(BannerPosition_Bottom);
        });
        
        //動画を読み込みします。
        FireBaseBridge::getInstance()->fetchRemoteConfig("reward_movie_type", [](bool success, Value value){
            
            RewardMovieType rewardType = MIN(RewardMovieType::ADMOB, (RewardMovieType)value.asInt());
            
            RewardPlayer::getInstance()->refresh(rewardType);
            RewardMovieManager::sharedManager()->loading();
        });
    });
}

void CorporationScene::createLogo()
{
    auto logo=Sprite::createWithSpriteFrameName("logo.png");
    logo->setScale(getContentSize().width/logo->getContentSize().width);
    logo->setPosition(getContentSize()/2);
    
    addChild(logo);
}

void CorporationScene::createListener()
{//タッチ透過させない
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [this](Touch *touch,Event*event)->bool{
        this->transition(0);
        
        return true;
    };
    auto dip = Director::getInstance()->getEventDispatcher();
    dip->addEventListenerWithSceneGraphPriority(listener, this);
}

void CorporationScene::createIndicator()
{
    auto indicator = NCIndicator::create();
    indicator->setScale(MIN(getContentSize().width*.08/indicator->getContentSize().width, 1));//元のサイズより大きくしない
    indicator->setPosition(getContentSize()-indicator->getContentSize()*.7);//右上に表示
    addChild(indicator);
}

void CorporationScene::startLoadingData()
{
    /**ネットワークエラーのアラート表示*/
    auto showNetworkErrorAlert = [this](){
        auto message = DataManager::sharedManager()->getSystemMessage("network_error_text");
        auto alert = ConfirmAlertLayer::create(ConfirmAlertType_Single, message, [this](Ref* sender){
            this->startLoadingData();
        });
        alert->showAlert(this);
    };
    
    /**強制アップデートのアラート表示*/
    auto showUpdateAlert = [this](const ccMenuCallback &callback){
        auto message = DataManager::sharedManager()->getSystemMessage("needUpdate");
        auto alert = CustomAlert::create(AlertTitle_INFORMATION, message, {"OK"}, callback); 
        alert->showAlert(this);
    };
    
    static int tryCount = 0;
    bool isConnected = FireBaseBridge::getInstance()->getIsConnected() || NativeBridge::isOnline();
    log("firebaseの接続状況%d,nativeの接続状況%d",FireBaseBridge::getInstance()->getIsConnected() , NativeBridge::isOnline());
    
    if (isConnected) {
        EscapeDataManager::getInstance()->loadingAppData([this, showNetworkErrorAlert, showUpdateAlert](bool success){
            if (!success) {
                log("not さくせす");
                showNetworkErrorAlert();
            }
            else {
                //強制アプデの必要があるかどうか、確認する。
                auto needUpdate = EscapeDataManager::getInstance()->checkNeedAppVersion();
                
                if (needUpdate) {
                    showUpdateAlert([this](Ref* sender){
                        
                        NativeBridge::openUrl(MyURL);
                        
                        //ストアに飛ばした後、再読み込み。
                        this->startLoadingData();
                    });
                }
                else {//強制アップデートの必要がないのでバージョン等をチェックします。
                    EscapeDataManager::getInstance()->checkVersion([this](bool isNewVersion, std::string errorDescription){
                        
                        auto versionMap = EscapeDataManager::getInstance()->getAppDataMap();
                        auto versionMapSize = versionMap.size();
                        
                        if (versionMapSize == 0) {//ローカルにversion.plistがないのに、新バージョンはない。→表示できるデータがない。
                            auto title = DataManager::sharedManager()->getSystemMessage("network_error");
                            auto message = DataManager::sharedManager()->getSystemMessage("network_new");
                            auto reload = DataManager::sharedManager()->getSystemMessage("reload");
                            
                            auto customAlert = CustomAlert::create(title, message, {reload}, [this](Ref* sender){
                                log("再読み込みする。");
                                
                                this->startLoadingData();
                            });
                            customAlert->showAlert(this);
                        }
                        else {//新規データの更新なし
                            Common::performAsyncTask([](){
                                DataManager::sharedManager()->loadAllRecordData();
                            }, [](){
                                //新規データの更新はないので、コレクション画面を表示します。
                                Director::getInstance()->replaceScene(TransitionFade::create(.5, EscapeLoadingScene::createScene(LoadingType_Icons),Color3B::WHITE));
                            });
                        }
                    });
                }
            }
        });
    }
    else{
        tryCount++;
        log("トライカウントは%d",tryCount);
        if (tryCount%5 == 0) {//5回トライしても失敗
            showNetworkErrorAlert();
        }
        else {
            auto delay = DelayTime::create(2);
            auto callback = CallFunc::create([this](){
                this->startLoadingData();
            });
            
            this->runAction(Sequence::create(delay, callback, NULL));
        }
    }
}

void CorporationScene::transition(float frame)
{
    unschedule(schedule_selector(CorporationScene::transition));

    Director::getInstance()->replaceScene(TransitionFade::create(.5, EscapeCollectionScene::createScene(),Color3B::WHITE));
}

