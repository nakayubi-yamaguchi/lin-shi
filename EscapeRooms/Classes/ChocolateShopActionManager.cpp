//
//  SchoolFesActionManager.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2018/11/29.
//
//

#include "ChocolateShopActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "GameScene.h"
#include "TouchManager.h"
#include "FlipSprite.h"

using namespace cocos2d;

ChocolateShopActionManager* ChocolateShopActionManager::manager =NULL;

ChocolateShopActionManager* ChocolateShopActionManager::getInstance()
{
    if (manager==NULL) {
        manager = new ChocolateShopActionManager();
    }
    
    return manager;
}

#pragma mark - 背景
int ChocolateShopActionManager::getDefaultBackNum()
{
    /*
    if(!DataManager::sharedManager()->isShowedOpeningStory()) {
        return 0;
    }*/
    
    return 1;
}

#pragma mark - Back
void ChocolateShopActionManager::backAction(std::string key, int backID, Node *parent, ShowType type)
{
#pragma mark 神経衰弱カード配置
    if(key == "trump_arrange"){

        std::vector<Vec2> anchorPoints = {Vec2(.21, .69), Vec2(.498, .69), Vec2(.81, .69), Vec2(.13, .317), Vec2(.377, .317), Vec2(0.622, .317), Vec2(0.87, .317)};
        
        int i = 0;
        
        for (auto anchorPoint : anchorPoints) {
            auto fileName = StringUtils::format("sup_79_trump_%d",i);

            auto invisibleKey = "invisible_" + fileName;
            auto isInvisible = DataManager::sharedManager()->getTemporaryFlag(invisibleKey).asBool();

            if (!isInvisible) {
                auto flipSprite = FlipSprite::create(fileName, anchorPoint, parent);
                parent->addChild(flipSprite);
            }
            
            i++;
        }
    }
#pragma mark ピンクケーキ火
    else if(key == "fire"){
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(8)) return;
        
        std::vector<Vec2> poses = getMatchPoses(parent, backID);
        for (int i=0; i<poses.size(); i++) {
            if (i != 0 && i != poses.size()-1) {
                this->showFireParticle(parent, backID, i);
            }
        }
    }
#pragma mark 居眠り
    else if(key == "sleep" && !DataManager::sharedManager()->getEnableFlagWithFlagID(20.1)){
        auto balloonFile = "sup_29_balloon.png";
        auto balloonAct = balloonAction(parent, balloonFile, Vec2(.475, .341), 2);
        auto balloon = parent->getChildByName(balloonFile);
        
        balloon->runAction(balloonAct);
    }
#pragma mark 時計
    else if (key == "clock" && DataManager::sharedManager()->getEnableFlagWithFlagID(20.1)) {
        auto hour = parent->getChildByName<Sprite*>("sup_57_hour.png");
        auto minute = parent->getChildByName<Sprite*>("sup_57_minute.png");
        
        //15時に合わせる。
        hour->setRotation(30);
        minute->setRotation(240);
    }
#pragma mark 絵画回答後
    else if (key == "picture" && DataManager::sharedManager()->getEnableFlagWithFlagID(35)) {
        auto picture = parent->getChildByName<Sprite*>("sup_27_picture.png");
        picture->setPositionY(picture->getPositionY()+parent->getContentSize().height*.2);
    }
#pragma mark わんちゃん
    else if(key == "dog"){
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(34)) return;
        Vector<FiniteTimeAction*>actions;
        
        std::vector<int>indexs={1,1,1,0,0,0,1,0};
        auto tongue_0=createAnchorSprite("sup_12_tongue_0.png", .5, .428, true, parent, true);
        auto tongue_1=createAnchorSprite("sup_12_tongue_1.png", .5, .428, true, parent, true);
        auto original_scale=tongue_0->getScale();
        
        for (auto index : indexs) {
            actions.pushBack(DelayTime::create(1));
            
            auto node=(index==0)?tongue_0:tongue_1;
            auto set_zero=CallFunc::create([node]{
                node->setScale(.01);
                node->setOpacity(0);
            });
            actions.pushBack(set_zero);
            
            //ぺろり
            auto seq=TargetedAction::create(node, Sequence::create(Spawn::create(FadeIn::create(.2),
                                                                                 ScaleTo::create(.2, original_scale*.5,original_scale*.25), NULL),
                                                                   createSoundAction("pop_4.mp3",type),
                                                                   ScaleTo::create(.2, original_scale),
                                                                   ScaleTo::create(.3, original_scale*.75,original_scale*.5),
                                                                   Spawn::create(ScaleTo::create(.3, .01),
                                                                                 FadeOut::create(.3),
                                                                                 NULL),
                                                                   NULL));
            actions.pushBack(seq);
        }
        actions.pushBack(DelayTime::create(2));
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
#pragma mark コルク人形の間違い探し
    else if(key == "mistake_cork" && DataManager::sharedManager()->isPlayingMinigame()){
        
        float moveDistance = parent->getContentSize().width*.087;
        auto mistake = parent->getChildByName<Sprite*>("sup_6_mistake.png");
        //createSpriteToCenter("sup_6_mistake.png", false, parent, true);
        mistake->setPositionX(mistake->getPositionX()-moveDistance);
        
        createSpriteToCenter("sup_6_cover_mistake.png", false, parent, true);
        
        Vector<FiniteTimeAction*> actions;
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createSoundAction("pop_1.mp3",type));
        actions.pushBack(TargetedAction::create(mistake, MoveBy::create(.5, Vec2(moveDistance, 0))));
        actions.pushBack(CallFunc::create([](){
            DataManager::sharedManager()->setTemporaryFlag("sup_6_mistake", 1);
        }));
        
        parent->runAction(Sequence::create(actions));
    }
}

#pragma mark - Touch
void ChocolateShopActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    Vector<FiniteTimeAction*> actions;
    //コールバック
    auto call = CallFunc::create([callback]{
        callback(true);
    });

#pragma mark 吹き出し（クラウド）
    if(key.substr(0, 5) == "cloud") {
        int num = DataManager::sharedManager()->getNowBack();
        
        if (num == 56) {
            auto fileName = "sup_56_pig_0.png";
            auto trumpName = "sup_56_trump.png";
            auto charaAnchor = parent->getChildByName<Sprite*>(fileName)->getAnchorPoint();
            auto cloudBounce = cloudBounceAction(parent, {fileName, trumpName}, charaAnchor, "sup_56_cloud.png", Vec2(.62, .72), "pig.mp3");
            actions.pushBack(cloudBounce);
            actions.pushBack(call);
        }
        else if (num == 41) {
            actions.pushBack(cloudBounceAction(parent, {"sup_41_pig_0.png"}, Vec2(.5, .418), "sup_41_cloud.png", Vec2(.405, .635), "pig.mp3"));
            actions.pushBack(call);
        }
        else if (num == 52) {
            actions.pushBack(cloudBounceAction(parent, {"sup_52_pig_0.png"}, Vec2(.5, .16), "sup_52_cloud.png", Vec2(.375, .68), "pig.mp3"));
            actions.pushBack(call);
        }else if (num == 7) {
            actions.pushBack(cloudBounceAction(parent, {"sup_7_monkey_0.png"}, Vec2(.5, .16), "sup_7_cloud.png", Vec2(.425, .59), "monkey.mp3"));
            actions.pushBack(call);
        }
        else if (num == 43) {
            actions.pushBack(cloudBounceAction(parent, {"sup_43_pig_0.png"}, Vec2(.5, .291), "sup_43_cloud.png", Vec2(.418, .574), "pig.mp3"));
            actions.pushBack(call);
        }
    }
#pragma mark アイテムあげる
    else if (key.substr(0, 5) == "give_") {
        
        actions.pushBack(useItemSoundAction());

        auto item = key.substr(5);
        log("アイテムをあげる%s",item.c_str());
        if (item == "strawberry") {//いちご
            //いちごを渡す
            auto strawberry = createSpriteToCenter("sup_56_strawberry.png", true, parent, true);
            actions.pushBack(giveAction(strawberry, .5, .8, .7));
            
            //いちごを受け取る
            auto pig_0 = parent->getChildByName<Sprite*>("sup_56_pig_0.png");
            auto charaAnchor = pig_0->getAnchorPoint();
            auto pig_1 = createAnchorSprite("sup_56_pig_1.png", charaAnchor.x, charaAnchor.y, true, parent, true);
            pig_1->setAnchorPoint(charaAnchor);
            actions.pushBack(createChangeAction(.5, .2, pig_0, pig_1));
            actions.pushBack(DelayTime::create(1));
            
            //いちごを食べる。
            auto pig_2 = createAnchorSprite("sup_56_pig_2.png", charaAnchor.x, charaAnchor.y, true, parent, true);
            actions.pushBack(createSoundAction("paku.mp3"));
            actions.pushBack(createChangeAction(.5, .2, pig_1, pig_2));
            actions.pushBack(DelayTime::create(1.5));
            
            //おいしぃぃぃー！
            actions.pushBack(createSoundAction("pig.mp3"));
            auto repeatBounce = Repeat::create(createBounceAction(pig_2, .2, .1), 5);
            actions.pushBack(repeatBounce);
            
            //懐中電灯に手をかける
            auto pig_3 = createSpriteToCenter("sup_56_pig_3.png", true, parent, true);
            actions.pushBack(createChangeAction(0, 0, pig_2, pig_3));
            actions.pushBack(swingAction(Vec2(parent->getContentSize().width*.03, 0), .3, pig_3, 1.5, false, 5));
            actions.pushBack(DelayTime::create(.6));
            
            //懐中電灯くれる
            auto pig_4 = createSpriteToCenter("sup_56_pig_4.png", true, parent, true);
            actions.pushBack(createChangeAction(0, .0, pig_3, pig_4, "poku.mp3"));
            actions.pushBack(DelayTime::create(1.5));

            //正解音
            actions.pushBack(correctSoundAction());
            actions.pushBack(call);
        }
    }
#pragma mark 神経衰弱
    else if (key.substr(0, 5) == "trump") {
        //トランプの配置
        static std::vector<int> trumpNumbers = {11, 8, 5, 2, 5, 2, 8};
        
        //現在選択したトランプの絵柄。
        int selectedIndex = atoi(key.substr(6).c_str());
        int selectedNum = trumpNumbers.at(selectedIndex);
        
        auto fileName = FlipSprite::getSerchName(StringUtils::format("sup_79_%s", key.c_str()));
        auto flipSprite = parent->getChildByName<FlipSprite*>(fileName.c_str());
        
        if (!flipSprite || flipSprite->getIsFlipped()) {//既に表なら何もしない
            actions.pushBack(call);
        }
        else {
            actions.pushBack(createSoundAction("wind_short.mp3"));
            actions.pushBack(flipSprite->getFlipAction(.7));
            actions.pushBack(DelayTime::create(.25));
            
            //検証する。
            static const char* selectedCardIndexKey = "selectedCardIndex";

            auto selectedBeforeValue = DataManager::sharedManager()->getTemporaryFlag(selectedCardIndexKey);
            int selectedBeforeIndex = selectedBeforeValue.asInt();
            
            if (selectedBeforeValue.isNull() || selectedBeforeIndex == -1) {
                log("一枚目を選択しました。%d",selectedIndex);
                
                DataManager::sharedManager()->setTemporaryFlag(selectedCardIndexKey, selectedIndex);
                actions.pushBack(call);
            }
            else {
                int selectedBeforeNum = trumpNumbers.at(selectedBeforeIndex);
                log("二枚名を選択しました。。%d::::%d", selectedIndex, selectedBeforeIndex);
                
                auto beforeFileName = FlipSprite::getSerchName(StringUtils::format("sup_79_trump_%d", selectedBeforeIndex));
                auto beforeTrump = parent->getChildByName<FlipSprite*>(beforeFileName);
                if (selectedNum == selectedBeforeNum) {//同じ数字揃えました！二枚弾く
                    auto spawn = Spawn::create(ScaleBy::create(.4, 1.2), FadeOut::create(.4), NULL);
                    actions.pushBack(correctSoundAction());
                    actions.pushBack(Spawn::create(flipSprite->getActionToFlips(spawn),
                                                   beforeTrump->getActionToFlips(spawn->clone()), NULL));
                    
                    
                    auto invisibleKey = "invisible_sup_79_trump_%d";
                    DataManager::sharedManager()->setTemporaryFlag(StringUtils::format(invisibleKey, selectedIndex), 1);
                    DataManager::sharedManager()->setTemporaryFlag(StringUtils::format(invisibleKey, selectedBeforeIndex), 1);
                    
                    
                    //最後の一枚かどうかを検証します。
                    int trumpCount = 7;//トランプは7枚です。
                    int invisibleCount = 0;
                    for (int i = 0; i < 7; i++) {
                        bool invisible = DataManager::sharedManager()->getTemporaryFlag(StringUtils::format(invisibleKey, i)).asBool();
                        
                        if (invisible) {
                            invisibleCount++;
                        }
                    }
                    
                    if (invisibleCount == trumpCount-1) {//全ペア揃えました！
                        
                        AnswerManager::getInstance()->getItemIDs.push_back(18);
                        AnswerManager::getInstance()->enFlagIDs.push_back(40);
                        DataManager::sharedManager()->setTemporaryFlag(StringUtils::format(invisibleKey, 0), 1);

                        auto elevenCardName = FlipSprite::getSerchName(StringUtils::format("sup_79_trump_%d", 0));
                        auto elevenTrump = parent->getChildByName<FlipSprite*>(elevenCardName);
            
                        actions.pushBack(DelayTime::create(.4));
                        actions.pushBack(elevenTrump->getFlipAction(.8));
                        actions.pushBack(DelayTime::create(1));
                    }
                    
                    actions.pushBack(call);
                }
                else {//ミスってます。両方裏返す。
                    //元からひっくり返っているやつ
                    auto beforeTrumpFlipAction = beforeTrump->getFlipAction(.5);
                    
                    //さっきひっくり返したやつ
                    auto trumpFlipAction = flipSprite->getFlipAction(.5);
                    
                    auto flipSpawn = Spawn::create(beforeTrumpFlipAction, trumpFlipAction, NULL);
                    
                    actions.pushBack(createSoundAction("boo.mp3"));
                    actions.pushBack(flipSpawn);
                    actions.pushBack(call);
                }
                
                //何も選択していない状態にするぜ
                DataManager::sharedManager()->setTemporaryFlag(selectedCardIndexKey, -1);
            }
        }
    }
#pragma mark マッチ
    else if(key=="match")
    {//マッチアニメ
        actions.pushBack(useItemSoundAction());
        
        auto match = createAnchorSprite("sup_36_match.png", .275, .755, true, parent, true);
        auto fadein_item=TargetedAction::create(match, FadeIn::create(1));
        actions.pushBack(fadein_item);
        
        std::vector<Vec2> poses = getMatchPoses(parent, 36);
        Vector<FiniteTimeAction*>fireActions;
        
        Vector<ParticleSystemQuad*> particles;

        for (int i=0; i<poses.size(); i++) {
            Vec2 pos = poses[i];
            auto move=TargetedAction::create(match, EaseInOut::create(MoveTo::create(.7, pos), 1.2));
            fireActions.pushBack(move);
            
            if (i==0) {
                auto shake=jumpAction(parent->getContentSize().height*.01, .1, match, 1.2, 2);
                fireActions.pushBack(shake);
            }
            fireActions.pushBack(createSoundAction("huo.mp3"));
            fireActions.pushBack(DelayTime::create(.2));
            
            //パーティクルを追加
            auto particle = this->showFireParticle(parent, 36, i);
            int totalParticle = particle->getTotalParticles();
            particle->setTotalParticles(0);
            particles.pushBack(particle);
            
            auto fire=CallFunc::create([particle, i ,match, this, totalParticle]{
                particle->setTotalParticles(totalParticle);
            });
            fireActions.pushBack(fire);
            fireActions.pushBack(DelayTime::create(.4));
        }
        actions.pushBack(Sequence::create(fireActions));
        
        auto fadeout_match=TargetedAction::create(match, Spawn::create(FadeOut::create(1),
                                                                       MoveBy::create(1, Vec2(0, parent->getContentSize().height*.05)), NULL));
        actions.pushBack(fadeout_match);
        actions.pushBack(DelayTime::create(.5));
        
        //番号を消す。
        auto numbers = parent->getChildByName<Sprite*>("sup_36_numbers.png");
        auto fadeout_numbers = TargetedAction::create(numbers, FadeOut::create(.5));
        auto fadeout_particle = CallFunc::create([particles](){
            
            auto leftNumber = particles.at(0);
            auto rightNumber = particles.at(particles.size()-1);
            
            leftNumber->setDuration(.5);
            rightNumber->setDuration(.5);
        });
        actions.pushBack(Spawn::create(fadeout_numbers, fadeout_particle, NULL));
        actions.pushBack(DelayTime::create(.5));
        
        actions.pushBack(correctSoundAction());

        actions.pushBack(call);
    }
#pragma mark コルク人形
    else if (key == "cork") {
        actions.pushBack(useItemSoundAction());
        
        //青コルクをセットします。
        auto cork_blue_0 = createSpriteToCenter("sup_55_cork_blue_0.png", true, parent, true);
        actions.pushBack(createSoundAction("kata.mp3"));
        actions.pushBack(TargetedAction::create(cork_blue_0, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.4));
        
        //青と赤を回転
        auto cork_red_0 = parent->getChildByName<Sprite*>("sup_55_cork_red_0.png");
        auto cork_red_1 = createSpriteToCenter("sup_55_cork_red_1.png", true, parent, true);
        auto cork_blue_1 = createSpriteToCenter("sup_55_cork_blue_1.png", true, parent, true);
        actions.pushBack(createChangeAction(.5, .5, {cork_blue_0, cork_red_0}, {cork_blue_1, cork_red_1}));
        actions.pushBack(DelayTime::create(.5));
        
        //両者歩み寄る
        float moveDistance = parent->getContentSize().width * .028;
        auto moveByToRight = MoveBy::create(.9, Vec2(moveDistance, 0));
        auto moveByToLeft = MoveBy::create(.9, Vec2(-moveDistance, 0));
        auto blueMoveAction = TargetedAction::create(cork_blue_1, moveByToRight);
        auto redMoveAction = TargetedAction::create(cork_red_1, moveByToLeft);
        auto corkSpawnAction = Sequence::create(Spawn::create(blueMoveAction, redMoveAction, NULL),
                                                DelayTime::create(.4), NULL) ;
        auto repeatAction = Repeat::create(EaseInOut::create(corkSpawnAction, 2), 3);
        actions.pushBack(createSoundAction("music_box.mp3"));
        actions.pushBack(repeatAction);
        actions.pushBack(DelayTime::create(1.2));
        
        //扉開く
        auto sup_open = createSpriteToCenter("sup_55_box_1.png", true, parent, true);
        actions.pushBack(createSoundAction("gii.mp3"));
        actions.pushBack(TargetedAction::create(sup_open, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.5));
        
        //扉open
        auto back_60 = createSpriteToCenter("back_60.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back_60, 60);
        actions.pushBack(createSoundAction("gii.mp3"));
        actions.pushBack(TargetedAction::create(back_60, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.5));

        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark アイス機械にアイテムセット
    else if (key == "stick" || key == "ice") {
        actions.pushBack(useItemSoundAction());
        actions.pushBack(DelayTime::create(.1));
        
        auto itemFileName = StringUtils::format("sup_74_%s_0.png", key.c_str());
        auto item = createSpriteToCenter(itemFileName, true, parent, true);
        actions.pushBack(TargetedAction::create(item, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.4));

        int needFlag = (key == "stick")? 37:38;
        log("フラグチェック%d",DataManager::sharedManager()->getEnableFlagWithFlagID(needFlag));
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(needFlag)) {
            actions.pushBack(iceMachineAction(parent));
        }
        
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark 文字盤セット
    else if (key.substr(0, 5) == "clock") {
        
        actions.pushBack(useItemSoundAction());
        actions.pushBack(DelayTime::create(.1));
        
        //文字をセット
        auto chara = key.substr(6);
        auto itemFileName = StringUtils::format("sup_57_%s.png", chara.c_str());
        auto item = createSpriteToCenter(itemFileName, true, parent, true);
        actions.pushBack(TargetedAction::create(item, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.4));
        
        int needFlag = (chara == "x")? 19:20;
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(needFlag)) {
            AnswerManager::getInstance()->enFlagIDs.push_back(20.1);

            auto hour = parent->getChildByName<Sprite*>("sup_57_hour.png");
            auto minute = parent->getChildByName<Sprite*>("sup_57_minute.png");
            
            //時計が回る
            auto hourRotate = TargetedAction::create(hour, RotateBy::create(1, 30));
            auto minuteRotate = TargetedAction::create(minute, RotateBy::create(1, 240));
            actions.pushBack(Spawn::create(hourRotate, minuteRotate, NULL));
            actions.pushBack(DelayTime::create(.5));
            
            //時計がなる
            actions.pushBack(createSoundAction("gooon.mp3"));
            auto particle=CallFunc::create([parent]{
                auto pos=Vec2(parent->getContentSize().width*.33, parent->getContentSize().height*.55);
                auto size=parent->getContentSize().width*.15;
                
                auto particle=ParticleSystemQuad::create("particle_music.plist");
                particle->setAutoRemoveOnFinish(true);
                particle->setDuration(6);
                particle->setPosition(pos);
                particle->setLife(6);
                particle->setLifeVar(0);
                particle->resetSystem();
                particle->setStartSize(size);
                particle->setEndSize(size);
                particle->setTotalParticles(6);
                particle->setGravity(Vec2(size*1, -size*.4));
                particle->setStartColor(Color4F::RED);
                particle->setEndColor(Color4F(255, 0, 0, 0));
                
                particle->setAngle(135);
                particle->setSpeed(size*2);
                parent->addChild(particle);
            });
            actions.pushBack(particle);
            actions.pushBack(DelayTime::create(3));
            
            //居眠り豚に遷移
            auto back_29 = createSpriteToCenter("back_29.png", true, parent, true);
            back_29->setLocalZOrder(1);
            SupplementsManager::getInstance()->addSupplementToMain(back_29, 29);
            actions.pushBack(TargetedAction::create(back_29, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(.2));
            actions.pushBack(createSoundAction("gooon.mp3"));
            actions.pushBack(DelayTime::create(1));
            
            //豚驚く
            auto sleepingPig = back_29->getChildByName<Sprite*>("sup_29_pig_0.png");
            auto attentionPigFileName = "sup_29_pig_1.png";
            auto pig_attention = createSpriteToCenter(attentionPigFileName, true, back_29, true);
            actions.pushBack(createChangeAction(.1, .1, sleepingPig, pig_attention));
            actions.pushBack(attentionAction(back_29, attentionPigFileName, Vec2(.5, .167), "sup_29_attention.png"));
            actions.pushBack(DelayTime::create(.7));
            
            //豚走っていく
            auto pig_run = createSpriteToCenter("sup_29_pig_2.png", true, back_29, true);
            actions.pushBack(createChangeAction(.5, .5, pig_attention, pig_run, "pig.mp3"));
            actions.pushBack(DelayTime::create(.8));
            
            //back43をセット。
            auto back_43 = createSpriteToCenter("back_43.png", true, parent, true);
            back_43->setLocalZOrder(1);
            SupplementsManager::getInstance()->addSupplementToMain(back_43, 43);
            actions.pushBack(TargetedAction::create(back_43, FadeIn::create(.5)));
            actions.pushBack(TargetedAction::create(back_29, FadeOut::create(0)));
            actions.pushBack(DelayTime::create(1.5));
            
            //豚登場
            auto pig_43 = createSpriteToCenter("sup_43_pig_0.png", true, back_43, true);
            actions.pushBack(createSoundAction("pig.mp3"));
            actions.pushBack(TargetedAction::create(pig_43, FadeIn::create(.4)));
            actions.pushBack(DelayTime::create(1));
            
            //時計に戻ってくる
            actions.pushBack(TargetedAction::create(back_43, FadeOut::create(.5)));
            actions.pushBack(DelayTime::create(1));
        }
        
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark 正解後にポスターを上にずらす
    else if (key == "openPoster") {
        auto picture = parent->getChildByName<Sprite*>("sup_27_picture.png");
        actions.pushBack(createSoundAction("su.mp3"));
        actions.pushBack(TargetedAction::create(picture, EaseInOut::create(MoveBy::create(.5, Vec2(0, parent->getContentSize().height*.2)), 2)));
        actions.pushBack(DelayTime::create(.3));
        
        actions.pushBack(call);
    }
#pragma mark トランプ回答後リス出現
    else if (key == "squirre") {
        
        auto back_28 = createSpriteToCenter("back_28.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back_28, 28, true);
        
        actions.pushBack(TargetedAction::create(back_28, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.6));
        
        //引き出しオープン
        auto sup_28_open = createSpriteToCenter("sup_28_tana_open.png", true, back_28, true);
        auto sup_28_squirre_before = createSpriteToCenter("sup_28_squirre_before.png", true, back_28, true);
        auto open_fadein = Spawn::create(TargetedAction::create(sup_28_open, FadeIn::create(.6)),
                                         TargetedAction::create(sup_28_squirre_before, FadeIn::create(.6)), NULL);
        actions.pushBack(open_fadein);
        actions.pushBack(createSoundAction("squirre.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        //リスが横を向く
        Vector<Node*> sup_30_squirres;
        for (int i = 0; i < 4; i++) {
            auto sup_30_squirre_after = createSpriteToCenter(StringUtils::format("sup_28_squirre_after_%d.png", i), true, back_28, true);
            sup_30_squirres.pushBack(sup_30_squirre_after);
        }
        actions.pushBack(createChangeAction(.6, .6, {sup_28_squirre_before}, sup_30_squirres, "pop_1.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        //リス消える
        Vector<FiniteTimeAction*> fadeoutActions;
        for (auto squirre : sup_30_squirres) {
            fadeoutActions.pushBack(TargetedAction::create(squirre, FadeOut::create(.5)));
        }
        actions.pushBack(Spawn::create(fadeoutActions));
        
        //back42に遷移
        auto back_42 = createSpriteToCenter("back_42.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back_42, 42);
        actions.pushBack(TargetedAction::create(back_42, FadeIn::create(.7)));
        
        //back28を非表示に
        actions.pushBack(TargetedAction::create(back_28, FadeOut::create(.1)));
        
        //横からリスが走ってくる。
        Vector<Node*> sup_42_squirres_0;//横向き
        float moveDistance = parent->getContentSize().width*.7;
        Vector<FiniteTimeAction*> jumpInActions;
        float jumpDuration = 4;
        int jumpCount = 7;
        for (int i = 0; i < 4; i++) {
            auto sup_42_squirre = createSpriteToCenter(StringUtils::format("sup_42_squirre_%d_1.png", i), false, back_42, true);
            sup_42_squirre->setPositionX(sup_42_squirre->getPositionX()-moveDistance);
            sup_42_squirres_0.pushBack(sup_42_squirre);
            
            jumpInActions.pushBack(TargetedAction::create(sup_42_squirre, JumpBy::create(jumpDuration, Vec2(moveDistance, 0), parent->getContentSize().height*.05, jumpCount)));
        }
        
        Vector<FiniteTimeAction*> jumpSoundActions;
        for (int i = 0; i < jumpCount; i++) {
            jumpSoundActions.pushBack(DelayTime::create(jumpDuration/jumpCount));
            jumpSoundActions.pushBack(createSoundAction("pop_1.mp3"));
        }
        jumpInActions.pushBack(Sequence::create(jumpSoundActions));
        
        actions.pushBack(Spawn::create(jumpInActions));
        actions.pushBack(DelayTime::create(.7));
        
        //リス正面向く
        Vector<Node*> sup_42_squirres_1;//横向き
        for (int i = 0; i < 4; i++) {
            auto sup_42_squirre = createSpriteToCenter(StringUtils::format("sup_42_squirre_%d_2.png", i), true, back_42, true);
            sup_42_squirre->setLocalZOrder(1);
            sup_42_squirres_1.pushBack(sup_42_squirre);
        }
        actions.pushBack(createChangeAction(.5, .5, sup_42_squirres_0, sup_42_squirres_1, "squirre.mp3"));
        actions.pushBack(DelayTime::create(.5));

        //ジャンプジャンプ&ほこりたつ
        Vector<FiniteTimeAction*> jumpSpawnActions;
        for (auto squirre : sup_42_squirres_1) {
            jumpSpawnActions.pushBack(TargetedAction::create(squirre, JumpBy::create(jumpDuration, Vec2(0, -parent->getContentSize().height*.2), parent->getContentSize().height*.1, 7)));
        }
        
        Vector<FiniteTimeAction*> bombSeqActions;
        float waitDuration = 1;
        bombSeqActions.pushBack(DelayTime::create(waitDuration));
        
        //煙
        auto particle=ParticleSystemQuad::create("bomb.plist");
        particle->setPosVar(Vec2(parent->getContentSize().width*.3, particle->getPosVar().y*.7));
        particle->setRadialAccel(particle->getRadialAccel()/2);
        particle->setSpeed(particle->getSpeed()/3);
        particle->setTotalParticles(0);
        particle->setStartSize(particle->getStartSize());
        particle->setDuration(-1);
        particle->setPosition(parent->getContentSize()/2);
        parent->addChild(particle);
        
        auto smoke = CallFunc::create([parent, particle](){
            particle->setTotalParticles(75);
        });
        bombSeqActions.pushBack(smoke);
        bombSeqActions.pushBack(DelayTime::create(1));
        
        //チョコ並び替え
        auto choco_1 = createSpriteToCenter("sup_42_choco_1.png", true, back_42, true);
        bombSeqActions.pushBack(TargetedAction::create(choco_1, FadeIn::create(.1)));
        
        //スモーク消す
        bombSeqActions.pushBack(CallFunc::create([particle](){
            particle->setDuration(3);
        }));
        
        jumpSpawnActions.pushBack(Sequence::create(bombSeqActions));
        actions.pushBack(Spawn::create(jumpSpawnActions));
        
        actions.pushBack(DelayTime::create(.8));
        
        //戻る
        actions.pushBack(TargetedAction::create(back_42, FadeOut::create(.5)));
        actions.pushBack(DelayTime::create(.6));
        
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark チョコをセット
    else if(key=="setchoco"){//back70
        actions.pushBack(useItemSoundAction());
        
        auto choco = createSpriteToCenter("sup_82_0.png", true, parent, true);
        actions.pushBack(putAction(choco, "koto.mp3", .7, .2));
        actions.pushBack(DelayTime::create(.4));
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark ケーキカット
    else if(key=="cakecut"){
        actions.pushBack(useItemSoundAction());
        
        //包丁渡す
        auto knife=createSpriteToCenter("sup_41_knife.png", true, parent,true);
        actions.pushBack(giveAction(knife, 1, .7, true));
        
        //包丁持たせる
        auto pig_0 = parent->getChildByName<Sprite*>("sup_41_pig_0.png");
        auto pig_1 = createSpriteToCenter("sup_41_pig_1.png", true, parent, true);
        auto arm=createAnchorSprite("sup_41_arm.png", .428, .536, false, pig_1, true);
        actions.pushBack(createChangeAction(.5, .5, pig_0, pig_1,"pop.mp3"));
        
        
        //投げるりんごと置いてあるりんご入れ替え
        auto throwCake = AnchorSprite::create(Vec2(311.0f/600.0f, 200.0f/660.0f), parent->getContentSize(), true, "sup_41_cake_throw.png");
        parent->addChild(throwCake);
        auto cake_0 = parent->getChildByName<Sprite*>("sup_41_cake_0_0.png");
        actions.pushBack(DelayTime::create(.3));
        actions.pushBack(createChangeAction(.5, .5, cake_0, throwCake));
        
        //投げる & 包丁振り回す
        Vector<FiniteTimeAction*> spawnActions;
        float totalDuration = 2;
        spawnActions.pushBack(createSoundAction("fall.mp3"));
        
        //上下
        float upDuration = 1;
        auto moveVec = Vec2(0, parent->getContentSize().height*.4);
        auto moveTop = MoveBy::create(upDuration, moveVec);
        auto moveFall = MoveBy::create(totalDuration-upDuration, Vec2(0, -moveVec.y*.8));
        auto move = Sequence::create(moveTop, moveFall, NULL);
        spawnActions.pushBack(move);
        
        //ピンキー包丁を振るアニメ
        spawnActions.pushBack(Spawn::create(swingAction(-60, totalDuration/8, arm, 1.5, false, 4,"sword.mp3"),
                                            Repeat::create(createBounceAction(pig_1, totalDuration/4, .05), 2),
                                            NULL));
        
        //////小さく
        auto scale = ScaleTo::create(totalDuration, .6);
        spawnActions.pushBack(scale);
        
        //////フェードアウト
        float fadeoutDuration = .5;
        auto fadeout = FadeOut::create(fadeoutDuration);
        auto fadeoutSeq = Sequence::create(DelayTime::create(totalDuration-fadeoutDuration), fadeout, NULL);
        spawnActions.pushBack(fadeoutSeq);
        
        //////並べる。
        Vector<FiniteTimeAction*> cutAppleActions;
        for (int i=0; i<8; i++) {
            auto cut = createSpriteToCenter(StringUtils::format("sup_41_cake_1_%d.png",i), true, parent, true);
            float originY = cut->getPositionY();
            cut->setPositionY(originY+parent->getContentSize().height*.1);
            
            auto cutSpawn = Spawn::create(MoveTo::create(.2, Vec2(cut->getPositionX(), originY)),
                                          FadeIn::create(.2),
                                          createSoundAction("koto.mp3"), NULL);
            cutAppleActions.pushBack(Sequence::create(TargetedAction::create(cut, cutSpawn),
                                                      DelayTime::create(.05), NULL));
        }
        spawnActions.pushBack(Sequence::create(DelayTime::create(2),
                                               Sequence::create(cutAppleActions), NULL));
        actions.pushBack(TargetedAction::create(throwCake, Spawn::create(spawnActions)));
        
        actions.pushBack(createChangeAction(.7, .3, pig_1, pig_0));
        actions.pushBack(createSoundAction("pinpon.mp3"));
        actions.pushBack(call);
    }
#pragma mark シャンパン注ぐ
    else if(key=="champagne"){
        actions.pushBack(useItemSoundAction());
        
        auto angle=60;
        auto up_distance=parent->getContentSize().height*.35;
        auto champagne=createAnchorSprite("sup_51_champagne.png", .25, .605, true, parent, true);
        champagne->setRotation(angle);
        champagne->setPositionY(up_distance+champagne->getPositionY());
        champagne->setLocalZOrder(1);
        
        actions.pushBack(TargetedAction::create(champagne, FadeIn::create(1)));
        
        for (int i=0; i<4; i++) {
            auto wine=createSpriteToCenter(StringUtils::format("sup_51_%d.png",i), true, parent, true);
            
            actions.pushBack(TargetedAction::create(champagne, Spawn::create(EaseInOut::create(RotateBy::create(1, -angle), 1.5),
                                                                             EaseInOut::create(MoveBy::create(1, Vec2(0, -up_distance)), 1.5)
                                                                             , NULL)));
            actions.pushBack(createSoundAction("glass-wine1.mp3"));
            actions.pushBack(TargetedAction::create(wine, FadeIn::create(1)));
            actions.pushBack(TargetedAction::create(champagne, Spawn::create(EaseInOut::create(RotateBy::create(.7, angle), 1.5),
                                                                             EaseInOut::create(MoveBy::create(1, Vec2(0, up_distance)), 1.5), NULL)));
            actions.pushBack(DelayTime::create(.5));
            
            //move
            if (i==3) {
                actions.pushBack(TargetedAction::create(champagne, FadeOut::create(1)));
            }
            else{
                actions.pushBack(TargetedAction::create(champagne, EaseInOut::create(MoveBy::create(1, Vec2(parent->getContentSize().width*.16, 0)), 1.5)));
            }
        }
        
        actions.pushBack(createSoundAction("pinpon.mp3"));
        actions.pushBack(call);
    }
#pragma mark チョコフォンデュ
    else if(key=="strawberry"){
        Common::playSE("p.mp3");
        
        auto strawberry=createSpriteToCenter("sup_6_strawberry.png", true, parent,true);
        auto choco=createSpriteToCenter("sup_6_choco.png", true, strawberry,true);
        createSpriteToCenter("sup_6_cover.png", false, parent,true);
        
        auto distance=parent->getContentSize().width*.15;
        
        actions.pushBack(TargetedAction::create(strawberry, Sequence::create(FadeIn::create(1),
                                                                             DelayTime::create(.5),
                                                                             EaseInOut::create(MoveBy::create(1, Vec2(distance, 0)), 1.5),
                                                                             createSoundAction("bubble.mp3"),
                                                                             TargetedAction::create(choco, FadeIn::create(1)),
                                                                             EaseInOut::create(MoveBy::create(1, Vec2(-distance, 0)), 1.5),
                                                                             FadeOut::create(1),
                                                                             NULL)));
        
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark クッキー+オーブン
    else if(key=="putcookie"){
        actions.pushBack(useItemSoundAction());
        
        auto cookie_0=createSpriteToCenter("sup_71_cookie_0.png", true, parent,true);
        auto cookie_2=createSpriteToCenter("sup_71_cookie_2.png", true, parent,true);//baked
        
        auto close=createSpriteToCenter("back_71.png", true, parent,true);
        createSpriteToCenter("sup_71_cookie_1.png", false, close,true);
        
        //put
        auto fadein=TargetedAction::create(cookie_0, FadeIn::create(.7));
        actions.pushBack(fadein);
        actions.pushBack(createSoundAction("koto.mp3"));
        actions.pushBack(DelayTime::create(.7));
        auto bake_action=TargetedAction::create(close, Spawn::create(Common::createSoundAction("ban.mp3"),
                                                                     FadeIn::create(.2), NULL));
        actions.pushBack(bake_action);
        actions.pushBack(createSoundAction("timer.mp3"));
        actions.pushBack(DelayTime::create(4));
        actions.pushBack(createSoundAction("tin.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        auto open=Spawn::create(TargetedAction::create(close, FadeOut::create(.2)),
                                Common::createSoundAction("gacha.mp3"),
                                createChangeAction(.3, .1, cookie_0, cookie_2),
                                CallFunc::create([parent]{
            auto particle=ParticleSystemQuad::create("smoke.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width/2,parent->getContentSize().height*.5);
            particle->setDuration(.2);
            particle->setStartSize(parent->getContentSize().width*.1);
            particle->setStartSizeVar(particle->getStartSize()*.2);
            particle->setEndSize(particle->getStartSize()*4);
            particle->setSpeed(parent->getContentSize().height*.075);
            particle->setSpeedVar(particle->getSpeed()*.2);
            particle->setGravity(Vec2(0, particle->getSpeed()*1));
            particle->setLife(2.5);
            particle->setTotalParticles(1500);
            particle->setPosVar(Vec2(parent->getContentSize().width*.275, parent->getContentSize().height*.24));
            particle->setAngle(270);
            particle->setAngleVar(5);
            parent->addChild(particle);
        }),
                                NULL);
        actions.pushBack(open);
        actions.pushBack(DelayTime::create(1));
        
        //actions.pushBack(TargetedAction::create(cookie_2, FadeOut::create(.7)));
        //actions.pushBack(DelayTime::create(.5));
        actions.pushBack(correctSoundAction());

        actions.pushBack(call);
    }
#pragma mark 骨+わんちゃん
    else if(key=="putbone"){
        actions.pushBack(useItemSoundAction());
        
        auto bone=createSpriteToCenter("sup_12_bone.png", true, parent,true);
        actions.pushBack(putAction(bone, "koto.mp3", 1, .5));
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark givesoft
    else if(key=="givesoft"){
        actions.pushBack(useItemSoundAction());

        auto pig_0=parent->getChildByName("sup_52_pig_0.png");
        transformToAnchorSprite(parent, pig_0, Vec2(.5, .16));
        auto pig_1=createSpriteToCenter("sup_52_pig_1.png", true, pig_0,true);
        
        auto soft=createSpriteToCenter("sup_52_softonhand.png", true, pig_0,true);
        auto soft_1=createSpriteToCenter("sup_52_softonmouth.png", true, pig_0,true);
        
        auto pig_2=createSpriteToCenter("sup_52_pig_2.png", true, parent,true);
        auto pig_3=createSpriteToCenter("sup_52_pig_3.png", true, parent,true);
        auto trump=createSpriteToCenter("sup_52_trump.png", false,pig_3,true);
        
        actions.pushBack(TargetedAction::create(pig_1, FadeIn::create(1)));
        actions.pushBack(putAction(soft, "pop.mp3", 1, .5));
        actions.pushBack(DelayTime::create(1));
        //eat
        actions.pushBack(Spawn::create(createBounceAction(pig_0, .4, .1),
                                       createChangeAction(.5, .3, soft, soft_1,"paku.mp3"), NULL));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(Spawn::create(Repeat::create(createBounceAction(pig_0, .1, .05), 4),
                                       TargetedAction::create(soft_1, FadeOut::create(1)),
                                       createSoundAction("pig.mp3"),
                                       NULL));
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(createChangeAction(.7, .3, pig_0, pig_2,"pop.mp3"));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createChangeAction(.7, .3, pig_2, pig_3,"pop.mp3"));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(TargetedAction::create(trump, FadeOut::create(.5)));
        actions.pushBack(DelayTime::create(1));
        
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark givearrow
    else if(key=="givearrow"){
        actions.pushBack(useItemSoundAction());
        
        auto monkey_0=parent->getChildByName("sup_7_monkey_0.png");
        transformToAnchorSprite(parent, monkey_0, Vec2(.5, .16));
        
        auto arrow_0=createSpriteToCenter("sup_7_arrow_0.png", true, parent,true);
        actions.pushBack(giveAction(arrow_0, 1, .8, true));
        
        //かまえ
        auto distance_shake=parent->getContentSize().width*.002;
        auto monkey_1=createSpriteToCenter("sup_7_monkey_1.png", true, parent,true);
        actions.pushBack(Spawn::create(createChangeAction(1, .5, monkey_0, monkey_1,"monkey.mp3"),
                                       swingAction(Vec2(distance_shake, -distance_shake), .1, monkey_1, 1.5, true, 5), NULL));
        
        //はなつ
        auto move_distance_0=parent->getContentSize().width;
        auto monkey_2=createSpriteToCenter("sup_7_monkey_2.png", true, parent,true);
        auto arrow_1=createAnchorSprite("sup_7_arrow_1.png",1,0, false, monkey_2,true);
        actions.pushBack(Spawn::create(createChangeAction(.5, .2, monkey_1, monkey_2,"arrow.mp3"),
                                       TargetedAction::create(arrow_1, EaseOut::create(Spawn::create(MoveBy::create(1, Vec2(move_distance_0, -move_distance_0)),
                                                                                                     ScaleBy::create(1, 1.2), NULL), 1.5)),
                                       
                                       NULL));
        
        //replace
        actions.pushBack(DelayTime::create(1));
        auto anim=createSpriteToCenter("anim_7_0.png", true, parent,true);
        auto arrow_2=createSpriteToCenter("sup_7_arrow_2.png", false, anim, true);
        arrow_2->setPositionX(parent->getContentSize().width*2);
        createSpriteToCenter("anim_7_cover.png", false, anim,true);
        
        actions.pushBack(TargetedAction::create(anim, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(.3));
        actions.pushBack(TargetedAction::create(arrow_2, EaseOut::create(MoveTo::create(.3, parent->getContentSize()/2), 2.0)));
        actions.pushBack(createSoundAction("sword_1.mp3"));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createChangeAction(0, 0, monkey_2, monkey_0));
        actions.pushBack(TargetedAction::create(anim, FadeOut::create(1)));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createSoundAction("pinpon.mp3"));
        actions.pushBack(call);
    }
#pragma mark ホットチョコ
    else if(key=="hotchoco"){
        actions.pushBack(useItemSoundAction());
        
        auto pig_0=parent->getChildByName("sup_43_pig_0.png");
        auto pig_1=createSpriteToCenter("sup_43_pig_1.png", true, parent, true);
        auto pig_2=createAnchorSprite("sup_43_pig_2.png",.5,.16, true, parent, true);
        
        auto choco_0=parent->getChildByName("sup_43_choco_0.png");
        auto choco_1=createAnchorSprite("sup_43_choco_1.png",.5,.1, true, parent,true);
        auto choco_2=createSpriteToCenter("sup_43_key.png", true, parent,true);
        
        auto pot=createSpriteToCenter("sup_43_pot.png", true, parent,true);
        pot->setLocalZOrder(1);
        
        actions.pushBack(giveAction(pot, 1, .8, true, "pop.mp3"));
        actions.pushBack(createChangeAction(.7, .5, pig_0, pig_1,"pop.mp3"));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createChangeAction(.5, .3, pig_1, pig_2,"pop.mp3"));
        
        //注ぐ
        auto clip=createClippingNodeToCenter("sup_43_clip.png", choco_0);
        auto hole=createSpriteToCenter("sup_43_hole.png", false, parent);
        hole->setPositionY(parent->getContentSize().height*.55);
        clip->addChild(hole);
        
        auto change_1=Spawn::create(createChangeAction(.7, .3, choco_0, choco_1,"pop.mp3"),
                                    TargetedAction::create(hole, FadeOut::create(.7)),
                                    NULL);
        auto change_2=Spawn::create(TargetedAction::create(choco_1, ScaleBy::create(2, 1.1,.9)),
                                    Sequence::create(DelayTime::create(1),
                                                     createChangeAction(.7, .3, choco_1, choco_2,"pop.mp3"),
                                                     NULL),NULL);
        
        actions.pushBack(Spawn::create(TargetedAction::create(hole, MoveTo::create(4, parent->getContentSize()/2)),
                                       Repeat::create(createBounceAction(pig_2, .5, .05), 4),//bounce
                                       
                                       createSoundAction("glass-wine1.mp3"),
                                       Sequence::create(DelayTime::create(1),
                                                        CallFunc::create([parent]{
                                           auto particle=ParticleSystemQuad::create("smoke.plist");
                                           particle->setAutoRemoveOnFinish(true);
                                           particle->setDuration(4.5);
                                           particle->setPosition(parent->getContentSize().width/2,parent->getContentSize().height*.25);
                                           particle->setSpeed(parent->getContentSize().height*.05);
                                           particle->setGravity(Vec2(0, -parent->getContentSize().height*.025));
                                           particle->setTotalParticles(20);
                                           particle->setAngle(90);
                                           particle->setAngleVar(10);
                                           particle->setLife(2);
                                           particle->setLocalZOrder(1);
                                           particle->setLifeVar(.5);
                                           particle->setStartSize(parent->getContentSize().width*.07);
                                           particle->setEndSize(parent->getContentSize().width*.125);
                                           parent->addChild(particle);
                                       }),
                                                        DelayTime::create(2),
                                                        change_1,
                                                        DelayTime::create(.7),
                                                        change_2,
                                                        NULL),
                                       NULL));
        actions.pushBack(createChangeAction(.7, .3, pig_2, pig_0));
        actions.pushBack(createSoundAction("pinpon.mp3"));
        actions.pushBack(call);
    }
#pragma mark conro
    else if(key=="conro"){
        //コンロ正解
        auto back=createSpriteToCenter("back_69.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 69);
        
        auto kettle=back->getChildByName("sup_69_pot.png");
        actions.pushBack(TargetedAction::create(back, FadeIn::create(1)));
        
        //fire
        auto batch=ParticleBatchNode::create("particle_fire.png");
        batch->setCascadeOpacityEnabled(true);
        back->addChild(batch);
        actions.pushBack(DelayTime::create(1));
        auto fire=CallFunc::create([batch,back]{
            Common::playSE("huo.mp3");
            
            auto fire=ParticleSystemQuad::create("fire.plist");
            fire->setName("particle");
            fire->setAutoRemoveOnFinish(true);
            fire->setTotalParticles(200);
            fire->setStartColor(Color4F(.2, .2, .8, 1));
            fire->setEndColor(Color4F(.2, .2, .8, 0));
            fire->setStartSize(back->getContentSize().width*.05);
            fire->setEndSize(back->getContentSize().width*.075);
            fire->setLife(.2);
            fire->setLifeVar(0);
            fire->setSpeed(back->getContentSize().height*.1);
            fire->setAngle(90);
            fire->setAngleVar(45);
            fire->setPosVar(Vec2(back->getContentSize().width*.05, back->getContentSize().height*.02));
            fire->setPosition(back->getContentSize().width*.316,back->getContentSize().height*.4);
            batch->addChild(fire);
        });
        actions.pushBack(fire);
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(TargetedAction::create(kettle, FadeOut::create(1)));
        actions.pushBack(DelayTime::create(1));
        
        auto stopfire=CallFunc::create([batch]{
            auto fire=batch->getChildByName<ParticleSystemQuad*>("particle");
            if (fire) {
                fire->setDuration(.5);
            }
        });
        actions.pushBack(stopfire);
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(call);
    }
#pragma mark クリア
    else if(key=="clear"){
        actions.pushBack(keyAction(parent, 81));
        auto open=createSpriteToCenter("sup_81_0_1.png", true, parent,true);
        auto recipe=createSpriteToCenter("sup_81_recipe.png", false, open,true);
        actions.pushBack(createSoundAction("gacha.mp3"));
        actions.pushBack(TargetedAction::create(open, FadeIn::create(.1)));
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(TargetedAction::create(recipe, FadeOut::create(1)));
        actions.pushBack(DelayTime::create(.5));
        
        actions.pushBack(CallFunc::create([parent]{
            auto story=StoryLayer::create("ending", []{
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        }));
    }
#pragma mark - 指定無し
    else{
        actions.pushBack(call);
    }
    parent->runAction(Sequence::create(actions));
    
   
}

#pragma mark - touch began
void ChocolateShopActionManager::touchBeganAction(std::string key, Node *parent)
{
}


#pragma mark - Drag
void ChocolateShopActionManager::dragAction(std::string key, Node *parent, Vec2 pos)
{
    
}

#pragma mark - Item
void ChocolateShopActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造 addする際はcreateSpriteOnClipを使用するとよい
    Vector<FiniteTimeAction*>actions;
    
#pragma mark opener
    if (key=="opener") {
        auto opener=createSpriteToCenter("champagne_opener.png", true, parent->backImage);
        parent->clip->addChild(opener);
        
        auto fadein_opener=TargetedAction::create(opener, FadeIn::create(1));
        actions.pushBack(fadein_opener);
        actions.pushBack(createSoundAction("kacha.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        auto cap=parent->itemImage->getChildByName("champagne_up_cap.png");
        auto cover=parent->itemImage->getChildByName("champagne_up_cover.png");
        auto moveout=Spawn::create(EaseOut::create(MoveBy::create(.5, Vec2(0, parent->itemImage->getContentSize().height*.5)), 1.5),
                                   FadeOut::create(1),
                                   createSoundAction("po.mp3"),
                                   NULL);
        actions.pushBack(Spawn::create(TargetedAction::create(opener, moveout),
                                       TargetedAction::create(cap, moveout->clone()),
                                       TargetedAction::create(cover, FadeOut::create(.1)), NULL));
        
        actions.pushBack(correctSoundAction());
    }
    
    //call
    auto call=CallFunc::create([callback](){
        callback(true);
    });
    actions.pushBack(call);
    
    parent->runAction(Sequence::create(actions));
}

#pragma mark - Custom
void ChocolateShopActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    
}

#pragma mark - Private
std::vector<Vec2> ChocolateShopActionManager::getMatchPoses(Node* parent, int backID)
{
    std::vector<Vec2> poses;
    if (backID == 36) {
        
        std::vector<float> pos_xs={.277, .39, .513, .613, .725};
        float pos_y = .635 * parent->getContentSize().height;
        
        for (auto pos_x : pos_xs) {
            poses.push_back(Vec2(pos_x*parent->getContentSize().width, pos_y));
        }
    }
    else if (backID == 35) {
        std::vector<float> pos_xs={.395, .447, .503, .552, .605};
        float pos_y = .836 * parent->getContentSize().height;
        
        for (auto pos_x : pos_xs) {
            poses.push_back(Vec2(pos_x*parent->getContentSize().width, pos_y));
        }
    }
    
    return poses;
}

ParticleSystemQuad* ChocolateShopActionManager::showFireParticle(cocos2d::Node *parent, int backID, int num)
{
    auto matchPoses = getMatchPoses(parent, backID);
    auto matchPos = matchPoses[num];
    
    auto particle=ParticleSystemQuad::create("fire.plist");
    particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
    particle->setAutoRemoveOnFinish(true);
    particle->setPosition(matchPos);
    particle->setTotalParticles(35);
    particle->setLife(.5);
    particle->setStartColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 150));
    
    //particle->setPosVar(Vec2(parent->getContentSize().width*.075, parent->getContentSize().height*.05));
    particle->setStartSize(parent->getContentSize().width*.03);

    particle->setSpeed(parent->getContentSize().height*.08);
    
    parent->addChild(particle);
    
    return particle;
}

FiniteTimeAction* ChocolateShopActionManager::iceMachineAction(Node *parent)
{
    AnswerManager::getInstance()->getItemIDs.push_back(7);
    AnswerManager::getInstance()->enFlagIDs.push_back(38.1);
    
    Vector<FiniteTimeAction*> actions;
    //ハンドルひねる
    auto item_1 = parent->getChildByName<Sprite*>("sup_74_stick_0.png");
    auto item_2 = createSpriteToCenter("sup_74_stick_1.png", true, parent, true);
    actions.pushBack(createChangeAction(1, .7, item_1, item_2, "weeen.mp3"));
    
    //アイス出てくる
    auto ice = createSpriteToCenter("sup_74_ice_1.png", true, parent, true);
    actions.pushBack(TargetedAction::create(ice, FadeIn::create(2)));
    actions.pushBack(DelayTime::create(.4));
    
    //チョコアイス取得
    
    auto getChoco = CallFunc::create([](){
        DataManager::sharedManager()->addNewItem(7);
    });
    actions.pushBack(getChoco);
    
    return Sequence::create(actions);
}
