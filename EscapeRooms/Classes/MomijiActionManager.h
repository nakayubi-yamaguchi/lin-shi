//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__MomijiActionManager__
#define __EscapeContainer__MomijiActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class MomijiActionManager:public CustomActionManager
{
public:
    static MomijiActionManager* manager;
    static MomijiActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void customAction(std::string key,Node*parent);

    //over ride
    void showAnswerAction(std::string key,Node*parent);

    void clearParticleAction(Node*parent);
private:
    ParticleSystemQuad* showFireParticle(cocos2d::Node *parent,int backID);
    ParticleSystemQuad* showDefaultSmokeParticle(cocos2d::Node *parent, bool isNew);
    ParticleSystemQuad* showBakedSmokeParticle(cocos2d::Node *parent, bool isNew);

    FiniteTimeAction* fanAction(Node*parent, ParticleSystemQuad* smoke);

};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
