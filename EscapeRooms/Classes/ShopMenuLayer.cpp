//
//  ContentsAlert.cpp
//  EscapeRooms
//
//  Created by yamaguchinarita on 2017/11/22.
//
//

#include "ShopMenuLayer.h"
#include "Utils/Common.h"
#include "DataManager.h"
#include "Utils/RewardMovieManager.h"
#include "Utils/ContentsAlert.h"
#include "Utils/UtilsMethods.h"
#include "UIParts/MenuItemScale.h"
#include "UIParts/MenuForScroll.h"
#include "UIParts/RoundedBoxSprite.h"
#include "AnalyticsManager.h"
#include "NotificationKeys.h"

using namespace cocos2d;

ShopMenuLayer* ShopMenuLayer::create(const ccMenuCallback &callback)
{
    auto node =new ShopMenuLayer();
    if (node && node->init(callback)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool ShopMenuLayer::init(const ccMenuCallback &callback) {
    if (!CustomAlert::init(Common::localize("SHOP", "ショップ"), "", {"CLOSE"}, callback)) {
        return false;
    }
    
    setTitleColor(LightBlackColor);
    dailyBonusCount = 10;

    createItemData();
    createPremiumMenu();
    
    AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_ShopOpen);

    
    return true;
}

#pragma mark- Private
void ShopMenuLayer::createItemData()
{
    //価格配列
    prices = {
    };
    
    //得られるコイン数配列
    items = {
        
        "126",
        "165",
        "330",
        "690",
        "1035",
        "1800",
        "3540",
        "30",
        "90"
    };
    
    auto enableCoin_min=30.0;//最小枚数
    auto basePrice = atof(ItemStore_plugin::ItemStore::getPrice(ItemID_30Coin).c_str());//基準アイテムの獲得枚数
    
    beginnersItemsMap ={
        //UI Bigが上
        {ItemID_PresentPackBig, {{"first", 50} , {"day", 13}}},
        {ItemID_PresentPackSmall ,{{"first", 25} , {"day", 5}}}
    };
    
    //お得率配列
    goodValues = {
    };
    
    itemIds = ItemConstants::getItemIds();
    
    //items itemIDsをセットアップ 初心者パックの有無
    bool haveBeginnersPack = (UserDefault::getInstance()->getIntegerForKey(UserKey_DailyBonusCoin, 0) != 0);
    if (!haveBeginnersPack) {//初心者パックの購入なし
        auto begin = items.begin();
        std::vector<std::string> dailyPresentPacks;
        dailyPresentPacks = {DataManager::sharedManager()->getSystemMessage("beginnersPackBig"), DataManager::sharedManager()->getSystemMessage("beginnersPackSmall")};
        
        for (auto dailyPresentPack : dailyPresentPacks)
        {
            begin = items.insert(begin, dailyPresentPack);
            begin++;  // ここでインクリメントしないと同じ位置に挿入し続けてしまう
        }
    }
    else{
        //購入履歴があるときはidを削除(defaultで存在しているので)
        itemIds.erase(itemIds.begin()++);
        itemIds.erase(itemIds.begin());
    }
    
    
    for (int i = 0; i < itemIds.size(); i++) {
        auto itemId = itemIds.at(i);
        
        log("アイテムIDは%s",itemId.c_str());

        auto localPriceSt = ItemStore_plugin::ItemStore::getLocalPrice(itemId);
        
        std::string goodValue = "";
        if (localPriceSt == "") {
            localPriceSt = "????";
        }
        else if(isCoinItem(itemId)) {//ちゃんと金額がある。コインのアイテム
            //単価計算する。
            auto priceSt = ItemStore_plugin::ItemStore::getPrice(itemId);
            
            float price = atof(priceSt.c_str());
            float enableCoin = atof(items.at(i).c_str());

            //使用金額分を最小メニューで購入した場合と比較する
            auto ratio=price/basePrice;
            float priceValue = round((enableCoin-enableCoin_min*ratio)/(enableCoin_min*ratio)*100);
            log("お得率は %f",priceValue);
            if (priceValue > 0) {//海外になると0%を下回る場合も出てくる。???
                auto bonusSt = DataManager::sharedManager()->getSystemMessage("bonus");
                goodValue = StringUtils::format(bonusSt.c_str(), (int)priceValue);
            }
            
        }
        
        prices.push_back(localPriceSt);
        goodValues.push_back(goodValue);
    }
}


void ShopMenuLayer::createPremiumMenu()
{
    minSize = Size(getContentSize().width*.9, getContentSize().height*.7);

    //メインスプライトとタイトルラベルの位置を変更
    mainSprite->setTextureRect(Rect(0, 0, minSize.width, minSize.height));
    arrange();

    titleLabel->setPositionY(mainSprite->getContentSize().height-space);
    
    //現在のポイント総数表示スプライト
    m_pointSprite = Sprite::create();
    m_pointSprite->setTextureRect(Rect(0, 0, mainSprite->getContentSize().width*.95, mainSprite->getContentSize().height*.1));
    m_pointSprite->setColor(Common::getColorFromHex(WhiteColor2));
    m_pointSprite->setPosition(mainSprite->getContentSize().width/2, titleLabel->getPositionY()-titleLabel->getContentSize().height/2-m_pointSprite->getContentSize().height/2);
    m_pointSprite->setCascadeOpacityEnabled(true);
    mainSprite->addChild(m_pointSprite);
    
    //所持コイン数
    auto coinSt = DataManager::sharedManager()->getCoinSt();
    
    TTFConfig ttfConfig(Common::getUsableFontPath(LocalizeFont),
                        m_pointSprite->getContentSize().height*.6,
                        GlyphCollection::DYNAMIC);
    auto pointLabel = Label::createWithTTF(ttfConfig, coinSt.c_str());
    pointLabel->setTextColor(Color4B(Common::getColorFromHex(BlackColor)));
    pointLabel->setPositionY(m_pointSprite->getContentSize().height/2);
    pointLabel->enableBold();
    m_pointSprite->addChild(pointLabel);
    
    //コイン画像
    auto coinSprite = Sprite::createWithSpriteFrameName("coin_image.png");
    coinSprite->setScale(ttfConfig.fontSize/coinSprite->getContentSize().height);
    coinSprite->setPositionY(pointLabel->getPositionY());
    m_pointSprite->addChild(coinSprite);

    //[コイン]ラベル
    TTFConfig coinTtfConfig(Common::getUsableFontPath(LocalizeFont),
                        ttfConfig.fontSize/2,
                        GlyphCollection::DYNAMIC);
    auto coinLabel = Label::createWithTTF(coinTtfConfig, Common::localize("coin", "コイン"));
    coinLabel->setTextColor(pointLabel->getTextColor());
    coinLabel->setPositionY(pointLabel->getPositionY()-pointLabel->getContentSize().height/2+coinLabel->getContentSize().height/2);
    m_pointSprite->addChild(coinLabel);
    
    //所持コインの位置を調整
    auto calculatePosition = [this, coinSprite, pointLabel, coinLabel](){
        auto coinSt = DataManager::sharedManager()->getCoinSt();
        pointLabel->setString(coinSt);
        pointLabel->setPositionX(m_pointSprite->getContentSize().width/2);
        
        coinSprite->setPositionX(pointLabel->getPositionX()-pointLabel->getContentSize().width/2-coinSprite->getBoundingBox().size.width*.6);
        coinLabel->setPositionX(pointLabel->getPositionX()+pointLabel->getContentSize().width/2+coinLabel->getContentSize().width*.6);

    };
    
    //ポジション合わせ
    calculatePosition();
    
    //所持コイン変動通知登録
    DataManager::sharedManager()->addChangeCoinNotification(this, [calculatePosition](EventCustom*event)
                                                            {
                                                                log("所持しているコインが更新されました");
                                                                calculatePosition();
                                                            });
    
    //tableview
    //テーブルビューを生成
    auto tableViewSize=Size(mainSprite->getContentSize().width, mainSprite->getContentSize().height*.68-space);
    auto width=tableViewSize.width;
    cellSize=Size(width,width/5.5);
    
    m_tableView =TableView::create(this, tableViewSize);
    m_tableView->setDirection(TableView::Direction::VERTICAL);
    m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
    m_tableView->setDelegate(this);
    m_tableView->setPosition((mainSprite->getContentSize().width-tableViewSize.width)/2, m_menuItems.at(0)->getPositionY()+space*1.5);
    //m_tableView->setPosition((mainSprite->getContentSize()-tableViewSize)/2);
    
    //アラートを削除する際にオパシティが透過するように。
    m_tableView->setCascadeOpacityEnabled(true);
    for (auto subView : m_tableView->getChildren()) {
        subView->setCascadeOpacityEnabled(true);
    }
    
    mainSprite->addChild(m_tableView);
}

bool ShopMenuLayer::isCoinItem(std::string itemId)
{
    return itemId.find("coin") != std::string::npos || itemId.find("Coin") != std::string::npos;
}

#pragma mark - 初心者パック
int ShopMenuLayer::getTotalCoinsByBeginnersPack(bool isBig)
{
    auto key=(isBig ? ItemID_PresentPackBig: ItemID_PresentPackSmall);
    auto map=beginnersItemsMap[key];
    
    return map["first"]+map["day"]*10;
}

float ShopMenuLayer::getDiscountRateByBeginnersPack(bool isBig)
{
    auto key=(isBig ? ItemID_PresentPackBig: ItemID_PresentPackSmall);
    auto price_pack = atof(ItemStore_plugin::ItemStore::getPrice(key).c_str());
    float price_pack_per=(float)getTotalCoinsByBeginnersPack(isBig)/price_pack;
    
    auto price_cheapItem=atof(ItemStore_plugin::ItemStore::getPrice(ItemID_30Coin).c_str());
    float price_cheapItem_per=atof(items.at(3).c_str())/price_cheapItem;//1,2は初心者パック？
    
    return price_pack_per/price_cheapItem_per*100;
}

#pragma mark !!購入ボタン選択時!!
void ShopMenuLayer::showPurchaseAlert(ssize_t idx)
{
    //
    auto itemId = itemIds.at(idx);
    bool isCoinType = isCoinItem(itemId);
    bool isUntreated = ItemStore_plugin::ItemStore::isUntreatedTransaction(itemId);
    
    std::string title = (isCoinType)? StringUtils::format("%s%s",NativeBridge::transformThreeComma(atoi(items.at(idx).c_str())).c_str(),Common::localize("coin", "コイン").c_str()):items.at(idx);
    
    std::string message;
    std::string coin_count;
    auto sprite=Sprite::create();
    sprite->setCascadeOpacityEnabled(true);
    sprite->setTextureRect(Rect(0, 0, getContentSize().width, getContentSize().width/3));
    
    auto untreatedText = DataManager::sharedManager()->getSystemMessage("untreatedText");

    if (isCoinType) {
        message = (isUntreated)? untreatedText : DataManager::sharedManager()->getSystemMessage("confirmPurchased");
        coin_count=NativeBridge::transformThreeComma(atoi(items.at(idx).c_str()));
    }
    else
    {//コイン以外
        int firstBonus = beginnersItemsMap.at(itemId).at("first");
        int dailyBonus = beginnersItemsMap.at(itemId).at("day");
        
        coin_count=NativeBridge::transformThreeComma(getTotalCoinsByBeginnersPack((itemId==ItemID_PresentPackBig)));
        auto addTxt = (isUntreated)? untreatedText:DataManager::sharedManager()->getSystemMessage("confirmPurchased");
        auto beginnersPackSt = DataManager::sharedManager()->getSystemMessage("beginnersPackDetailText") + "\n\n" + addTxt;
        message = StringUtils::format(beginnersPackSt.c_str(), firstBonus , dailyBonus*dailyBonusCount);
        
        //クリックイベントを通知
        UserDefault::getInstance()->setBoolForKey(UserKey_ClickBeginnersPack, true);
        auto event=EventCustom(NotificationKeyClickBeginners);
        Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
    }
    
    
    auto image_height=sprite->getContentSize().height*.5;
    auto coin_image=Sprite::createWithSpriteFrameName("coin_image.png");
    coin_image->setScale(image_height/coin_image->getContentSize().height);
    sprite->addChild(coin_image);
    
    auto coin_label=Label::createWithTTF(StringUtils::format("x %s",coin_count.c_str()), LocalizeFont, coin_image->getBoundingBox().size.height*.6);
    coin_label->setTextColor(Color4B::BLACK);
    sprite->addChild(coin_label);
    
    auto width_coin_container=coin_image->getBoundingBox().size.width+coin_label->getContentSize().width;
    auto coin_container_pos=Vec2(sprite->getContentSize().width*.5, sprite->getContentSize().height*.5);
    
    //position 調整
    if (!isCoinType) {
        auto total_label=Label::createWithTTF("Total", LocalizeFont, coin_label->getContentSize().height*.6);
        total_label->setTextColor(Color4B::RED);
        sprite->addChild(total_label);
        coin_container_pos=coin_container_pos=Vec2(coin_container_pos.x, coin_container_pos.y-total_label->getContentSize().height/2);
        coin_image->setPosition(coin_container_pos.x-width_coin_container/2+coin_image->getBoundingBox().size.width/2,coin_container_pos.y);
        coin_label->setPosition(coin_container_pos.x+width_coin_container/2-coin_label->getContentSize().width/2,coin_container_pos.y-coin_image->getBoundingBox().size.height*.4+coin_label->getContentSize().height/2);
        
        total_label->setPosition(coin_container_pos.x,coin_image->getPositionY()+coin_image->getBoundingBox().size.height/2+total_label->getContentSize().height/2);
    }
    else{
        coin_image->setPosition(coin_container_pos.x-width_coin_container/2+coin_image->getBoundingBox().size.width/2,coin_container_pos.y);
        coin_label->setPosition(coin_container_pos.x+width_coin_container/2-coin_label->getContentSize().width/2,coin_container_pos.y-coin_image->getBoundingBox().size.height*.4+coin_label->getContentSize().height/2);
    }
    
    //購入アラート
    std::vector<std::string>buttons={"NO",prices.at(idx)};
    bool addPackButton=false;
    if (UserDefault::getInstance()->getBoolForKey(UserKey_DailyBonusCount)==0) {
        if (isCoinType) {
            if (atoi(items.at(idx).c_str())<=126) {
                //初心者パック推し 126枚以下のメニュー選択時
                buttons.insert(++buttons.begin(),DataManager::sharedManager()->getSystemMessage("beginnersPackRecommendButton"));
                addPackButton=true;
            }
        }
        else if (itemId==ItemID_PresentPackSmall){
            //初心者パック大推し
            buttons.insert(++buttons.begin(),DataManager::sharedManager()->getSystemMessage("beginnersPackBigRecommendButton"));
            addPackButton=true;
        }
    }
    
    if (isUntreated) {
        buttons = {DataManager::sharedManager()->getSystemMessage("getItem")};
        addPackButton = false;
    }
    
    auto alert = ContentsAlert::create(sprite, title, message, buttons,addPackButton, [this, idx,addPackButton, title, isUntreated, itemId](Ref*sender){
        
        auto al = (ContentsAlert*)sender;
        int index = al->getSelectedButtonNum();
        
        if (isUntreated) {
            this->showPurchaseSuccessAlert(idx);
            return;
        }
        else
        {
            if (index == 0){
                return;//NOなら何もしない。
            }
            else if (index==1&&addPackButton)
            {//初心者パックに誘導
                tappedPackRecommendButton=true;
                this->showPurchaseAlert(0);
                
                return ;
            }
        }
        
        
        auto purchaseLayer = PurchaseLayer::create();
        
        auto purchaseID = itemIds.at(idx);
        purchaseLayer->startPurchase(purchaseID, false, PurchaseType_Consumable, [this, title,idx](std::string itemID, bool isRestore, ErrorType happenedError){
            
            if (happenedError == ErrorType_None) {//エラーなし。購入完了！
                this->showPurchaseSuccessAlert(idx);
            }
            else {
                auto failedPurchaseSt = DataManager::sharedManager()->getSystemMessage("failedPurchase");
                auto alert = ContentsAlert::create("failed.png", title.c_str(), failedPurchaseSt.c_str(), {"OK"}, NULL);
                alert->showAlert(this);
            }
            
        });//購入ならbuttonindex0,リストアなら1
        
        addChild(purchaseLayer);
    },nullptr);
    
    if (addPackButton) {
        alert->setButtonColor(Color3B::ORANGE, 1);
    }
    
    if (isUntreated) {
        alert->displayCloseButton();
    }
    
    alert->showAlert(this);
    
    
    if (itemId==ItemID_PresentPackSmall) {
        AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_ClickBeginnersPackSmall);
    }
    else if (itemId==ItemID_PresentPackBig){
        AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_ClickBeginnersPackBig);
    }
}

void ShopMenuLayer::showPurchaseSuccessAlert(ssize_t idx)
{//購入成功時
    auto itemId = itemIds.at(idx);
    bool thisCoinItem = this->isCoinItem(itemId);
    bool isUntreated = ItemStore_plugin::ItemStore::isUntreatedTransaction(itemId);
    
    if (isUntreated) {//未処理のトランザクションなら、完了処理を行う
        log("トランザクションを開始");
        ItemStore_plugin::ItemStore::treatTransaction(itemId);
        log("トランザクションを終了");
    }
    
    int firstBonus = (thisCoinItem)? 0:beginnersItemsMap.at(itemId).at("first");
    int dailyBonus = (thisCoinItem)? 0:beginnersItemsMap.at(itemId).at("day");
    
    int gotCoin = (thisCoinItem)? atoi(items.at(idx).c_str()): firstBonus;
    auto purchasedSt = DataManager::sharedManager()->getSystemMessage("purchasedCoin");
    auto bonusMessage=StringUtils::format(purchasedSt.c_str(), NativeBridge::transformThreeComma(gotCoin).c_str());

    bool needReload = isUntreated;
    if (!thisCoinItem) {//コインアイテムではない。→デイリーボーナス
        DataManager::sharedManager()->purchasedBeginnersPack(dailyBonus, dailyBonusCount);
        if (itemId==ItemID_PresentPackBig) {

            if (tappedPackRecommendButton) {
                AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_BuyBeginnersPackBig_ByReccomendButton);
            }
            else{
                AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_BuyBeginnersPackBig);
            }
        }
        else{
            AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_BuyBeginnersPackSmall);
        }
        
        bonusMessage.append(StringUtils::format(DataManager::sharedManager()->getSystemMessage("purchasedBeginners").c_str(),NativeBridge::transformThreeComma(dailyBonus).c_str()));
        
        //アイテムデータを作り直してリロード
        this->createItemData();
        
        needReload = true;
    }
    else if(itemId==ItemID_126Coin){
        AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_Buy126Coin);
    }
    
    //お礼アラート
    UtilsMethods::showThanksAlert(bonusMessage.c_str(), [this, needReload](Ref* sender){
        if (needReload) {
            log("テーブルをの内容をリロードしあす");
            m_tableView->reloadData();
        }
    });
    
    
    //paid userであることを記憶
    UserDefault::getInstance()->setBoolForKey(UserKey_IsPaidUser, true);
    
    DataManager::sharedManager()->addCoin(gotCoin);
};

#pragma mark - Table
#pragma mark data source
ssize_t ShopMenuLayer::numberOfCellsInTableView(TableView *table)
{
    int count = (int)prices.size();
    
    return count;
}

Size ShopMenuLayer::cellSizeForTable(TableView* table)
{
    return cellSize;
}

TableViewCell* ShopMenuLayer::tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx)
{
    auto cell = table->dequeueCell();
    cell = new TableViewCell();
    cell->setCascadeOpacityEnabled(true);
    cell->autorelease();
    
    //白背景
    auto backSprite = Sprite::create();
    backSprite->setTextureRect(Rect(0, 0, cellSize.width, cellSize.height));
    backSprite->setPosition(cellSize/2);
    backSprite->setColor(Common::getColorFromHex(WhiteColor2));
    cell->addChild(backSprite);
    
    //選択状態
    auto selectedSprite = Sprite::create();
    selectedSprite->setTextureRect(backSprite->getTextureRect());
    selectedSprite->setColor(Common::getColorFromHex(LightBlackColor));
    selectedSprite->setOpacity(0);
    selectedSprite->setName("Selected");
    selectedSprite->setPosition(backSprite->getPosition());
    cell->addChild(selectedSprite);
    
    // ボーダーライン
    Sprite* line = Sprite::create();
    line->setAnchorPoint(Point(0, 0));
    line->setTextureRect(Rect(0, 0, cellSize.width, 1));
    line->setColor(Common::getColorFromHex(LightBlackColor));
    cell->addChild(line);
    
    bool availableCoinItem =  isCoinItem(itemIds.at((int)idx));
    //サムネイル画像：コインかパワーアップか
    auto thumbnailSpace=cellSize.width*.02;
    auto thumbnailWidth=cellSize.height*.6;
    auto thumbnailSprite = Sprite::createWithSpriteFrameName((availableCoinItem)? "coin_image.png":"present.png");
    thumbnailSprite->setScale(thumbnailWidth/thumbnailSprite->getContentSize().height);
    thumbnailSprite->setPosition(thumbnailSpace+thumbnailWidth/2, cellSize.height/2);
    cell->addChild(thumbnailSprite);
    
    //アイテムラベル
    auto itemText = (availableCoinItem)? NativeBridge::transformThreeComma(atoi(items.at(idx).c_str())):items.at(idx);
    auto itemFontSize = thumbnailWidth*.5;
    
    auto itemLabel = Label::createWithTTF(itemText, Common::getUsableFontPath(HiraginoMaruFont), itemFontSize*pow(.8, (Common::localize("en", "", "", "", "")=="en"&&
                                                                                                                       !availableCoinItem)));//英語の場合のみ縮小
    
    itemLabel->setPosition(thumbnailWidth+thumbnailSpace*2+itemLabel->getContentSize().width/2,cellSize.height*.5+itemFontSize/2);
    itemLabel->enableBold();
    itemLabel->setTextColor(Color4B(Common::getColorFromHex(LightBlackColor)));
    cell->addChild(itemLabel);
    
    std::string specialString="";
    if (availableCoinItem) {//コインなら
        auto coinLabel = Label::createWithTTF(Common::localize("coin", "コイン"), Common::getUsableFontPath(HiraginoMaruFont), itemLabel->getTTFConfig().fontSize*.6);
        coinLabel->setPosition(itemLabel->getPositionX()+itemLabel->getContentSize().width/2+coinLabel->getContentSize().width/2, itemLabel->getPositionY()-itemLabel->getContentSize().height/2+coinLabel->getBoundingBox().size.height/2);
        coinLabel->enableBold();
        coinLabel->setTextColor(itemLabel->getTextColor());
        cell->addChild(coinLabel);
        
        //お得ラベル
        specialString=goodValues[idx];
    }
    else{
        //限定ラベルを
        specialString=StringUtils::format("(%s)",DataManager::sharedManager()->getSystemMessage("limited").c_str());
    }
    
    //お得な文字表記
    if (specialString.size()>0) {
        auto label=Label::createWithTTF(specialString, LocalizeFont, itemFontSize*.8);
        label->setTextColor(Color4B::RED);
        label->setPosition(thumbnailWidth+thumbnailSpace*2+label->getContentSize().width*1/2, cellSize.height/2/2);
        cell->addChild(label);
    }
    
    //完了していないトランザクション
    bool isUntreatedTransaction = ItemStore_plugin::ItemStore::isUntreatedTransaction(itemIds.at((int)idx));

    //購入ボタン
    Sprite* p_sprite;
    float p_height = cellSize.height/2;
    auto shade_height=p_height*.04;
    if (AnalyticsManager::getInstance()->getAnalyticsShopButtonType()==AnalyticsShopButtonType_Normal) {
        p_sprite = Sprite::create();
        p_sprite->setTextureRect(Rect(0, 0, p_height*3, p_height));
        
        //立体的に見せるよう
        auto p_sprite_back = Sprite::create();
        p_sprite_back->setTextureRect(Rect(0, 0, p_sprite->getContentSize().width, shade_height));
        p_sprite_back->setColor(Common::getColorFromHex(DarkGrayColor));
        p_sprite_back->setPosition(p_sprite->getContentSize().width/2, -p_sprite_back->getContentSize().height/2);
        p_sprite->addChild(p_sprite_back);
        
        if (isUntreatedTransaction || !availableCoinItem) {
            p_sprite->setColor(Color3B::ORANGE);
        }
        else{
            p_sprite->setColor(Common::getColorFromHex(MyColor));
        }
    }
    else{
        //丸パターン
        p_sprite = Sprite::createWithSpriteFrameName("rounded.png");
        p_sprite->setColor(Common::getColorFromHex(DarkGrayColor));
        
        auto top_p_sprite = Sprite::createWithSpriteFrameName("rounded.png");
        top_p_sprite->setPosition(p_sprite->getContentSize().width/2, p_sprite->getContentSize().height/2+shade_height);
        p_sprite->addChild(top_p_sprite);
        
        if (isUntreatedTransaction || !availableCoinItem) {
            top_p_sprite->setColor(Color3B::ORANGE);
        }
        else{
            top_p_sprite->setColor(Common::getColorFromHex(MyColor));
        }
    }
    
    p_sprite->setCascadeOpacityEnabled(true);
    
    //金額ラベル
    auto priceLabel = Label::createWithTTF(prices[idx].c_str(), Common::getUsableFontPath(LocalizeFont), p_sprite->getContentSize().height*.45);
    
    if (isUntreatedTransaction) {
        priceLabel->setString(DataManager::sharedManager()->getSystemMessage("getItem"));
    }
    
    priceLabel->setPosition(p_sprite->getContentSize()/2);
    priceLabel->enableBold();
    p_sprite->addChild(priceLabel);
    
    //ボタン作成
    auto purchaseButton = MenuItemScale::create(p_sprite, p_sprite, [this, idx](Ref*sender){
        log("購入ボタンが押されました。");
        Common::playClick();
        showPurchaseAlert(idx);
    });
    purchaseButton->setScale(p_height/p_sprite->getContentSize().height);
    purchaseButton->setTappedScale(.9);
    purchaseButton->setPosition(cellSize.width-purchaseButton->getBoundingBox().size.width/2*1.2, cellSize.height/2);
    
    auto purchaseMenu = MenuForScroll::createWithArray({purchaseButton});
    //purchaseMenu->setScrollView((ui::ScrollView*)m_tableView);
    purchaseMenu->setPosition(Vec2::ZERO);

    cell->addChild(purchaseMenu);
    
    return cell;
}

#pragma mark delegate
void ShopMenuLayer::tableCellHighlight(TableView* table, TableViewCell* cell)
{
    auto selectedSprite = cell->getChildByName("Selected");
    auto fadein = FadeTo::create(.1, 255/3);
    selectedSprite->runAction(fadein);
}

void ShopMenuLayer::tableCellUnhighlight(cocos2d::extension::TableView *table, cocos2d::extension::TableViewCell *cell)
{
    auto selectedSprite = cell->getChildByName("Selected");
    auto fadeout = FadeOut::create(.1);
    selectedSprite->runAction(fadeout);
}

void ShopMenuLayer::tableCellTouched(cocos2d::extension::TableView *table, cocos2d::extension::TableViewCell *cell)
{
    return;
    
    //選択されたアニメーション
    /*auto selectedSprite = Sprite::create();
    selectedSprite->setTextureRect(Rect(0, 0, cellSize.width ,cellSize.height));
    selectedSprite->setColor(Common::getColorFromHex(LightBlackColor));
    selectedSprite->setOpacity(100);
    selectedSprite->setPosition(cellSize/2);
    cell->addChild(selectedSprite);
    
    auto fadeout = FadeOut::create(.2);
    auto remove = RemoveSelf::create();
    selectedSprite->runAction(Sequence::create(fadeout, remove, NULL));
    */
}



