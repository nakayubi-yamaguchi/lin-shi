//
//  StarsSprites.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/08.
//
//

#ifndef __EscapeContainer__StarsSprites__
#define __EscapeContainer__StarsSprites__

#include "cocos2d.h"
USING_NS_CC;

#include "SettingDataManager.h"

class StarsSprite: public Sprite
{
public:
    static StarsSprite* create(float width);
    static StarsSprite* create(float width, SortType sortType);

    bool init(float width, SortType sortType);
    float starWidth;
    float starSpace;
    
    Vector<Sprite*> grayFrames;
    Vector<ProgressTimer*> stars;
    
    void createStarFrame();
    
    //アクセさ
    float rate;
    void setRate(float _rate);
    void setRate(float _rate,bool animate);//animationは少数のとき変になるかも
    float getRate();
    
    void setStarColor(Color3B color);
    Color3B getStarColor();
    
    //firebaseからの読み取り
    void readAverageRate(int index);
    
    /**ソートの種類によってセットする値変わる。コレクションセルで呼び出す推奨*/
    void setRateForSorted(int index);
    
    /**プログレスバーとStarSpriteのフェードアウトアクションを取得。なぜかStarsSpriteのフェードアウトだけでProgressbarが消えてくれない。*/
    Spawn* getFadeoutAction(float duration);
    
    //
    ~StarsSprite();
    
private:
    std::string m_firebaseStarDir;
    SortType m_sortType;

};

#endif /* defined(__EscapeContainer__StarsSprites__) */
