//
//  CorporationScene.h
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/06/29.
//
//

#ifndef __Regitaro2__CorporationScene__
#define __Regitaro2__CorporationScene__

#include "cocos2d.h"

class CorporationScene : public cocos2d::LayerColor
{
public:
    virtual bool init();
    static cocos2d::Scene *createScene();
    CREATE_FUNC(CorporationScene);
    
private:
    void extracted(const std::basic_string<char> &localResourcesPath, const std::basic_string<char> &packerName, std::vector<std::string> &searchPaths, const std::vector<std::string> &searchfolderNames);
    
    void onEnterTransitionDidFinish();

    void createLogo();
    void createListener();
    
    void startLoadingData();
    void transition(float frame);
    
    void createIndicator();
    
};

#endif /* defined(__Regitaro2__CorporationScene__) */
