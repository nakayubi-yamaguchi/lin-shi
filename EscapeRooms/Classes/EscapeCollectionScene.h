//
//  CustomerListScene.h
//  Regitaro2
//
//  Created by 成田凌平 on 2016/03/26.
//
//

#ifndef __Regitaro2__CustomerListScene__
#define __Regitaro2__CustomerListScene__

#include "cocos2d.h"
#include "UIParts/NCCollectionView.h"
#include "Utils/PurchaseLayer.h"


USING_NS_CC;

typedef enum{
    MenuType_Setting,
    MenuType_Achievement,
    MenuType_Shop,
    MenuType_Stamp
}MenuType;

class EscapeCollectionScene:public LayerColor, CollectionViewDataSource, CollectionViewDelegate, PurchaseLayerDelegate
{
public:
    virtual bool init();
    static Scene* createScene();
    CREATE_FUNC(EscapeCollectionScene);
    virtual void onEnterTransitionDidFinish();
    
private:    
    NCCollectionView *m_collectionView;
    Size cellSize;
    int playableMinIndex;
    
    /**トップバーの高さ*/
    float m_barHeight;

    static bool isLoadingTitleResource;//タイトルリソースをダウンロード中です。
    
    
    /**ボーナス ルーレット*/
    void showRouletteLayer();
    /**スタンプ表示*/
    void showStampLayer();
    /**ショップ表示*/
    void showShopAlert();
  
    void createUpperMenu();

    void createContents();
    void createScrollView();
    void showFirstOpenAlert();
    void transition(int index);

    //コレクションデータソース
    virtual ssize_t numberOfCellsInCollectionView(NCCollectionView *collection);
    virtual Size collectionCellSizeForIndex(NCCollectionView *collection, ssize_t idx);
    virtual CollectionViewCell* collectionCellAtIndex(NCCollectionView *collection, ssize_t idx);

    //コレクションデリゲート
    virtual void collectionCellTouched(NCCollectionView* collection, CollectionViewCell* cell);
    
    //購入レイヤーのデリゲート
    virtual void didFinishPurchase(std::string itemID, bool isRestore);
    virtual void didHappenedError(std::string itemID, bool isRestore, ErrorType happenedError);
    
    ~EscapeCollectionScene();
};

#endif /* defined(__Regitaro2__CustomerListScene__) */
