//
//  FingerSprite.cpp
//  Exit
//
//  Created by 成田凌平 on 2019/02/16.
//
//

#include "FingerSprite.h"

using namespace cocos2d;

FingerSprite* FingerSprite::create(std::string fileName, Vec2 vec)
{
    auto node=new FingerSprite();
    if (node&&node->init(fileName,vec)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool FingerSprite::init(std::string fileName, Vec2 vec)
{
    if (!Sprite::initWithSpriteFrameName(fileName)) {
        return false;
    }
    
    setDirectionX((FingerDirectionX)(vec.x>.25));//0 or 1(右)
    setDirectionY((FingerDirectionY)(vec.y>.75));//0 or 1(上)
    auto angle=45;
    if (getDirectionX()==FingerDirectionX_Left) {
        if (getDirectionY()==FingerDirectionY_Top) {
            angle=225;
        }
        else{
            angle=135;
        }
    }
    else{
        if (getDirectionY()==FingerDirectionY_Top) {
            angle=-45;
        }
    }
    
    setRotation(angle);
    
    return true;
}

#pragma mark - アニメーション
void FingerSprite::pointAnimate()
{
    setOpacity(0);

    auto move_duration=.5;
    auto move_distance=getBoundingBox().size.width*.1;
    
    auto fadein=TargetedAction::create(this, FadeIn::create(.5));
    auto move_0=EaseIn::create(MoveBy::create(move_duration, Vec2(-move_distance*pow(-1, (int)getDirectionX()), -move_distance*pow(-1, (int)getDirectionY()))), 1.5);
    auto move_1=EaseOut::create(MoveBy::create(move_duration, Vec2(move_distance*pow(-1, (int)getDirectionX()), move_distance*pow(-1, (int)getDirectionY()))), 1.5);
    auto rep=Repeat::create(Sequence::create(move_0,move_1, NULL), -1);
    
    this->runAction(Spawn::create(fadein,rep, NULL));
}

void FingerSprite::dragAnimate(ValueVector vector,Node*parent)
{
    setOpacity(0);
    Vector<FiniteTimeAction*>actions;
    auto fadein=FadeIn::create(.5);
    actions.pushBack(fadein);
    
    auto beginingPos=getPosition();
    auto beforePos=beginingPos;
    //差分を取り,movebyで動かす
    for (int i=1;i<vector.size();i++) {
        auto x=vector.at(i).asValueMap()["x"].asFloat()-vector.at(i-1).asValueMap()["x"].asFloat();
        auto y=vector.at(i).asValueMap()["y"].asFloat()-vector.at(i-1).asValueMap()["y"].asFloat();

        actions.pushBack(MoveBy::create(1, Vec2(parent->getContentSize().width*x, parent->getContentSize().height*y)));
    }
    actions.pushBack(FadeOut::create(.5));
    actions.pushBack(DelayTime::create(.5));
    actions.pushBack(CallFunc::create([this,beginingPos]{
        this->setPosition(beginingPos);
    }));

    runAction(Repeat::create(Sequence::create(actions), -1));
}
