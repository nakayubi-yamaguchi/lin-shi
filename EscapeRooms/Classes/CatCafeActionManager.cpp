//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "CatCafeActionManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"

using namespace cocos2d;

CatCafeActionManager* CatCafeActionManager::manager =NULL;

CatCafeActionManager* CatCafeActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new CatCafeActionManager();
    }
    return manager;
}

#pragma mark - Back
void CatCafeActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="counter")
    {
        auto pig=AnchorSprite::create(Vec2(.5, .318), parent->getContentSize(), "sup_21_pig.png");
        parent->addChild(pig);
        
        if(DataManager::sharedManager()->getEnableFlagWithFlagID(5)){
            auto apple=createSpriteToCenter("sup_21_apple.png", false, pig);
            pig->addChild(apple);
        }
    }
    else if(key=="back_22"&&DataManager::sharedManager()->getEnableFlagWithFlagID(12)){
        auto cat=parent->getChildByName("sup_22_cat.png");
        
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<2; i++) {
            auto anim_cat=createSpriteToCenter(StringUtils::format("sup_22_cat_%d.png",i), true, parent);
            parent->addChild(anim_cat);
            
            auto anim=Sequence::create(Spawn::create(TargetedAction::create(cat, FadeOut::create(.5)),
                                                     TargetedAction::create(anim_cat, FadeIn::create(.3)), NULL),
                                       createSoundAction("cat_0.mp3"),
                                       DelayTime::create(.5),
                                       Spawn::create(TargetedAction::create(cat, FadeIn::create(.3)),
                                                     TargetedAction::create(anim_cat, FadeOut::create(.5)), NULL), NULL);
            
            actions.pushBack(anim);
        }
        
        auto seq=Sequence::create(DelayTime::create(1),actions.at(0),actions.at(1),actions.at(0)->clone(),actions.at(1)->clone(),actions.at(1)->clone(),actions.at(0)->clone(), NULL);
        
        parent->runAction(Repeat::create(seq, UINT_MAX));
    }
    else if(key=="mouse"){
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            auto cover=createSpriteToCenter("sup_11_cover.png", false, parent);
            cover->setLocalZOrder(1);
            parent->addChild(cover);
            
            auto mouse=parent->getChildByName("sup_11_mistake.png");
            mouse->setOpacity(0);
            mouse->setPosition(mouse->getPosition().x, parent->getContentSize().height*.45);
            
            auto fadein=FadeIn::create(2);
            auto move=MoveTo::create(.5, parent->getContentSize()/2);
            auto call=CallFunc::create([]{
                Common::playSE("nyu.mp3");
                DataManager::sharedManager()->setTemporaryFlag("mouse", 1);
            });
            
            mouse->runAction(Sequence::create(fadein,move,call, NULL));
        }
    }
}

#pragma mark - Touch
void CatCafeActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="cloud")
    {
        Common::playSE("pig_1.mp3");
        std::string fileName="sup_21_cloud.png";
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(5)) {
            fileName="sup_21_cloud_1.png";
        }
        
        auto cloud=AnchorSprite::create(Vec2(.66,.74), parent->getContentSize(), fileName);
        cloud->setOpacity(0);
        parent->addChild(cloud);
        auto original_scale=cloud->getScale();
        cloud->setScale(.1);
        
        auto pig=parent->getChildByName("sup_21_pig.png");
        
        auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(.5, original_scale),
                                                                                    FadeIn::create(.5), NULL),
                                                                      createSoundAction("nyu.mp3"),
                                                                      DelayTime::create(.5),
                                                                      Spawn::create(ScaleTo::create(.5, .1),
                                                                                    FadeOut::create(.5), NULL), NULL));
        
        auto seq_pig=TargetedAction::create(pig, Sequence::create(ScaleTo::create(.3, 1.05, .95),
                                                                  ScaleTo::create(.3, .975, 1.025),
                                                                  ScaleTo::create(.2, original_scale), NULL));
        
        auto spawn=Spawn::create(seq_cloud,seq_pig, NULL);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,call, NULL));
    }
    else if(key=="cloud_cat")
    {
        Common::playSE("cat_0.mp3");
        auto cloud=AnchorSprite::create(Vec2(.6,.65), parent->getContentSize(), "anim_22_cloud.png");
        cloud->setOpacity(0);
        parent->addChild(cloud);
        auto original_scale=cloud->getScale();
        cloud->setScale(.1);
        
        auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(.5, original_scale),
                                                                                    FadeIn::create(.5), NULL),
                                                                      createSoundAction("nyu.mp3"),
                                                                      DelayTime::create(.5),
                                                                      Spawn::create(ScaleTo::create(.5, .1),
                                                                                    FadeOut::create(.5), NULL), NULL));
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_cloud,call,NULL));
    }
    else if(key=="apple")
    {
        Common::playSE("paku.mp3");
        auto apple=createSpriteToCenter("sup_21_apple.png", true, parent);
        parent->addChild(apple);
        auto card=parent->getChildByName("sup_21_card.png");
        
        auto fadein_0=TargetedAction::create(apple, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto fadeout=TargetedAction::create(card, FadeOut::create(.5));
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_0,delay,fadeout,call,NULL));
    }
    else if(key.compare(0,3,"cat")==0)
    {
        Common::playSE("cat_1.mp3");
        auto index=atoi(key.substr(4,1).c_str());
        
        auto before=parent->getChildByName(StringUtils::format("sup_5_%d_0.png",index));
        auto after=createSpriteToCenter(StringUtils::format("sup_5_%d_1.png",index), true, parent);
        parent->addChild(after);
        
        auto fadein_0=TargetedAction::create(after, FadeIn::create(.2));
        auto fadeout_0=TargetedAction::create(before, FadeOut::create(.5));
        auto spawn_0=Spawn::create(fadein_0,fadeout_0, NULL);
        auto delay=DelayTime::create(.2);
        auto fadein_1=TargetedAction::create(before, FadeIn::create(.2));
        auto fadeout_1=TargetedAction::create(after, FadeOut::create(.5));
        auto spawn_1=Spawn::create(fadein_1,fadeout_1, NULL);
        auto remove=TargetedAction::create(after, RemoveSelf::create());
        
        auto call=CallFunc::create([callback,index]{
            //正解判定
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(12)&&
                !DataManager::sharedManager()->getEnableFlagWithFlagID(13)) {
                std::string answer="010110";
                AnswerManager::getInstance()->appendPasscode(StringUtils::format("%d",index));
                auto inputing=AnswerManager::getInstance()->getPasscode();
                log("入力中%s",inputing.c_str());
                if (inputing.size()==answer.size()) {
                    if (inputing==answer) {
                        Common::playSE("pinpon.mp3");
                        callback(true);
                    }
                    else{
                        AnswerManager::getInstance()->resetPasscode();
                        callback(false);
                    }
                }
                else{
                    callback(false);
                }
            }
            else{
                log("フラグが不適切");
                callback(false);
            }
        });
        
        parent->runAction(Sequence::create(spawn_0,delay,spawn_1,remove,call,NULL));
    }
    else if(key.compare(0,4,"cage")==0){
        auto index=atoi(key.substr(5,1).c_str());
        
        auto back=createSpriteToCenter("back_32.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        std::vector<std::string>supnames;
        
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(28)) {
            supnames.push_back("sup_32_item_1.png");
            supnames.push_back("sup_32_cat_1.png");
        }
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(15)) {
            supnames.push_back("sup_32_item_2.png");
            supnames.push_back("sup_32_cat_2.png");
        }
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(31)) {
            supnames.push_back("sup_32_item_3.png");
            supnames.push_back("sup_32_cat_3.png");
        }
        
        for (auto name : supnames) {
            auto sup=createSpriteToCenter(name, false, back);
            sup->setName(name);
            back->addChild(sup);
        }
        
        auto open=createSpriteToCenter(StringUtils::format("anim_32_open_%d.png",index), true, parent);
        parent->addChild(open);
        auto cat=createSpriteToCenter(StringUtils::format("anim_32_cat_%d.png",index), true, parent);
        parent->addChild(cat);
        
        auto cat_before=back->getChildByName(StringUtils::format("sup_32_cat_%d.png",index));
        
        //
        auto fadein_0=TargetedAction::create(back, FadeIn::create(1));
        auto delay=DelayTime::create(.5);
        auto sound_0=createSoundAction("gacha.mp3");
        auto fadein_1=TargetedAction::create(open, FadeIn::create(.5));
        auto sound_1=createSoundAction("cat_0.mp3");
        auto move_cat=Spawn::create(TargetedAction::create(cat, FadeIn::create(1)),
                                    TargetedAction::create(cat_before, FadeOut::create(1)), NULL);
        auto fadeout=TargetedAction::create(cat, FadeOut::create(.5));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_0,delay,sound_0,fadein_1,delay->clone(),sound_1,move_cat,delay->clone(),fadeout,delay->clone(),call, NULL));
    }
    else if(key=="stirrer")
    {
        Common::playSE("p.mp3");
        //give
        auto stirrer=createSpriteToCenter("anim_21_stirrer.png", true, parent);
        stirrer->setLocalZOrder(1);
        parent->addChild(stirrer);
        
        //mix
        auto pig=createSpriteToCenter("sup_21_pig.png", true, parent);
        pig->setLocalZOrder(1);
        parent->addChild(pig);
        
        Vector<SpriteFrame*>frames;
        for(int i=0;i<2;i++){
            auto s=Sprite::createWithSpriteFrameName(StringUtils::format("anim_21_%d.png",i));
            frames.pushBack(s->getSpriteFrame());
        }
        
        auto animation=Animation::createWithSpriteFrames(frames);
        animation->setDelayPerUnit(4.0f/60.0f);//??フレームぶん表示
        animation->setRestoreOriginalFrame(true);
        animation->setLoops(30);
        
        //latte
        auto latte=createSpriteToCenter("sup_21_latte.png", true, parent);
        latte->setLocalZOrder(2);
        parent->addChild(latte);
        
        //animation
        auto seq_stirrer=TargetedAction::create(stirrer, Sequence::create(FadeIn::create(.5),DelayTime::create(.5),FadeOut::create(.5),
                                                                          DelayTime::create(.2),NULL));
        auto mix=Spawn::create(TargetedAction::create(pig, FadeIn::create(.5)),
                               TargetedAction::create(pig, Animate::create(animation)),
                               createSoundAction("pig.mp3"),
                               NULL);
        
        auto change=Spawn::create(TargetedAction::create(pig, Sequence::create(RemoveSelf::create(), NULL)),
                                  TargetedAction::create(latte, FadeIn::create(.5)),
                                  createSoundAction("pinpon.mp3"), NULL);
        
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_stirrer,mix,change,call,NULL));
    }
    else if(key=="doorman"){
        auto pig=parent->getChildByName("sup_24_pig.png");
        
        auto cloud=AnchorSprite::create(Vec2(.625, .715), parent->getContentSize(), StringUtils::format("anim_24_cloud.png"));
        cloud->setOpacity(0);
        parent->addChild(cloud);
        
        auto original_scale=cloud->getScale();
        cloud->setScale(.1);
        
        auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(.5, original_scale),
                                                                                    FadeIn::create(.5), NULL),
                                                                      createSoundAction("nyu.mp3"),
                                                                      DelayTime::create(.5),
                                                                      Spawn::create(ScaleTo::create(.5, .1),
                                                                                    FadeOut::create(.5), NULL), NULL));
        
        auto seq_pig=TargetedAction::create(pig, Sequence::create(ScaleTo::create(.3, 1.05, .95),
                                                                  ScaleTo::create(.3, .975, 1.025),
                                                                  ScaleTo::create(.2, original_scale), NULL));
        
        auto spawn=Spawn::create(seq_cloud,seq_pig, NULL);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,call, NULL));
    }
    else if(key=="card"){
        Common::playSE("p.mp3");
        
        auto card=createSpriteToCenter("anim_24_card.png", true, parent);
        parent->addChild(card);
        auto before=parent->getChildByName("sup_24_pig.png");
        
        auto after=createSpriteToCenter("anim_24_pig.png", true, parent);
        parent->addChild(after);

        auto fadein_0=TargetedAction::create(card, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto sound=createSoundAction("pig.mp3");
        auto change=Spawn::create(TargetedAction::create(card, FadeOut::create(.7)),
                                  TargetedAction::create(before, FadeOut::create(.7)),
                                  TargetedAction::create(after, FadeIn::create(.3)), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_0,delay,sound,change,delay->clone(),call, NULL));
    }
    else if(key.compare(0,4,"regi")==0){
        Common::playSE("keyboard.mp3");
        
        auto name=key.substr(5,key.size()-5);
        auto correct=false;
        
        if (name.size()==1)
        {//append
            if ((AnswerManager::getInstance()->getPasscode().size()==0&&name=="0")||
                AnswerManager::getInstance()->getPasscode().size()==6||
                !DataManager::sharedManager()->getEnableFlagWithFlagID(23)) {
                log("入力不可");
            }
            else{
                AnswerManager::getInstance()->appendPasscode(name);
                if (parent->getChildByName("regilabel")) {
                    parent->getChildByName<Label*>("regilabel")->removeFromParent();
                }
                //
                auto label=Label::createWithBMFont("register.fnt", AnswerManager::getInstance()->getPasscode());
                label->setName("regilabel");
                label->setScale(parent->getContentSize().height*.1/label->getContentSize().height);
                label->setPosition(parent->getContentSize().width/2,parent->getContentSize().height*.8);
                parent->addChild(label);
            }
        }
        else
        {//enter 正解判定
            if (AnswerManager::getInstance()->getPasscode()=="1650"&&
                DataManager::sharedManager()->getEnableFlagWithFlagID(25)&&
                !DataManager::sharedManager()->getEnableFlagWithFlagID(26)) {
                correct=true;
            }
            else{
                AnswerManager::getInstance()->resetPasscode();
                if (parent->getChildByName("regilabel")) {
                    parent->getChildByName<Label*>("regilabel")->removeFromParent();
                }
            }
        }
        
        //
        auto key=createSpriteToCenter(StringUtils::format("sup_45_%s.png",name.c_str()), false, parent);
        parent->addChild(key);
        
        auto seq=TargetedAction::create(key, Sequence::create(DelayTime::create(.1),RemoveSelf::create(), NULL));
        
        auto call=CallFunc::create([callback,correct]{
            if (correct) {
                Common::playSE("pinpon.mp3");
                callback(true);
            }
            else{
                callback(false);
            }
        });
        
        parent->runAction(Sequence::create(seq,call, NULL));
    }
    else if(key=="openregi"){
        auto back=createSpriteToCenter("back_44.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        auto on=createSpriteToCenter("sup_44_on.png", false, back);
        back->addChild(on);
        
        auto key=createSpriteToCenter("sup_44_key.png", false, back);
        back->addChild(key);
        
        auto open=createSpriteToCenter("sup_44_open.png", true, parent);
        parent->addChild(open);
        
        auto sup=createSpriteToCenter("sup_44_catfood.png", true, parent);
        parent->addChild(sup);
        
        auto delay=DelayTime::create(.7);
        auto fadein_0=TargetedAction::create(back, FadeIn::create(1));
        auto sound_0=createSoundAction("su.mp3");
        auto fadein_1=Spawn::create(TargetedAction::create(open, FadeIn::create(.5)),
                                    TargetedAction::create(sup, FadeIn::create(.5)), NULL);

        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(delay,fadein_0,delay->clone(),sound_0,fadein_1,delay->clone(),call, NULL));
    }
    else if(key=="food"){
        auto food=AnchorSprite::create(Vec2(543.0f/600, 110.0f/660), parent->getContentSize(), "anim_29_food.png");
        parent->addChild(food);
        
        //
        auto distance=parent->getContentSize().height*.02;
        auto angle=10;
        auto distance_x=-parent->getContentSize().width*.125;
        
        auto fadein_food=TargetedAction::create(food,FadeIn::create(1));
        auto shake_food=TargetedAction::create(food,Repeat::create(Sequence::create(EaseIn::create(Spawn::create(MoveBy::create(.2, Vec2(0,-distance)),
                                                                                     RotateBy::create(.2, angle), NULL), 1.5),
                                                        createSoundAction("sa.mp3"),
                                                        EaseIn::create(Spawn::create(MoveBy::create(.2, Vec2(0,distance)),
                                                                                     RotateBy::create(.2, -angle), NULL) , 1.5), NULL), 2));
        
        Vector<FiniteTimeAction*>food_actions;
        for (int i=6; i>=0; i--) {
            food_actions.pushBack(shake_food->clone());
            
            auto sup=createSpriteToCenter(StringUtils::format("sup_29_%d.png",i), true, parent);
            parent->addChild(sup);
            auto fadein=TargetedAction::create(sup, FadeIn::create(.2));
            food_actions.pushBack(fadein);
            
            auto move=TargetedAction::create(food,MoveBy::create(.5, Vec2(distance_x, 0)));
            food_actions.pushBack(move);
        }
        
        auto seq_food=Sequence::create(food_actions);
        auto fadeout=TargetedAction::create(food, FadeOut::create(.5));
        auto delay=DelayTime::create(.5);
        
        auto cat_on_toy=parent->getChildByName("sup_29_cat.png");
        
        auto cat_0=createSpriteToCenter(StringUtils::format("anim_29_cat_0.png"), true, parent);
        parent->addChild(cat_0);
        auto cat_1=createSpriteToCenter(StringUtils::format("anim_29_cat_1.png"), true, parent);
        parent->addChild(cat_1);
        auto cat_2=createSpriteToCenter(StringUtils::format("sup_29_cats.png"), true, parent);
        parent->addChild(cat_2);
        
        auto sound=createSoundAction("cat_0.mp3");
        auto fadein_cat_0=Spawn::create(TargetedAction::create(cat_0, FadeIn::create(1)),
                                        TargetedAction::create(cat_on_toy, FadeOut::create(.5)), NULL);
        auto fadein_cat_1=Spawn::create(TargetedAction::create(cat_1, FadeIn::create(1)),
                                        TargetedAction::create(cat_0, FadeOut::create(.5)), NULL);
        auto fadein_cat_2=Spawn::create(TargetedAction::create(cat_2, FadeIn::create(1)),
                                        TargetedAction::create(cat_1, FadeOut::create(.5)), NULL);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_food,seq_food,fadeout,delay,sound,fadein_cat_0,delay->clone(),sound->clone(),fadein_cat_1,delay->clone(),fadein_cat_2,call, NULL));
    }
    else if(key=="clear"){
        auto open=createSpriteToCenter("sup_1_open.png", true, parent);
        parent->addChild(open);
        
        Common::playSE("gacha.mp3");
        
        auto fadein=TargetedAction::create(open, FadeIn::create(.1));
        auto call=CallFunc::create([open]{
            auto story=StoryLayer::create("ending", [open](Ref*sender){
                EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

                auto story_1=StoryLayer::create("ending1", [open](Ref*sender){
                    open->removeFromParent();
                    auto story_2=StoryLayer::create("ending2", [](Ref*ref){
                        Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
                    });
                    Director::getInstance()->getRunningScene()->addChild(story_2);
                });
                Director::getInstance()->getRunningScene()->addChild(story_1);
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        parent->runAction(Sequence::create(fadein,call, NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void CatCafeActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{
    if (key.compare(0,5,"stamp")==0) {
        Common::playSE("p.mp3");
        
        auto index=atoi(key.substr(6,1).c_str());
        
        auto stamp_next=createSpriteToCenter(StringUtils::format("card_%d.png",index+1), true, parent->itemImage);
        parent->itemImage->addChild(stamp_next);
        
        auto stamp=createSpriteToCenter("anim_stamp.png", true, parent->itemImage);
        parent->itemImage->addChild(stamp);
        if (index==1) {
            stamp->setPosition(stamp->getContentSize().width*.75, stamp->getPosition().y);
        }
        else if (index==2) {
            stamp->setPosition(stamp->getContentSize().width, stamp->getPosition().y);
        }
        
        
        auto original_scale=stamp->getScale();
        
        auto scale=ScaleTo::create(.1, original_scale*1.5);
        auto fadein=FadeIn::create(1);
        auto scale_1=ScaleTo::create(.5, original_scale);
        auto sound=createSoundAction("koto.mp3");
        
        auto fadein_next=TargetedAction::create(stamp_next, FadeIn::create(.5));
        auto fadeout=Spawn::create(FadeOut::create(.5),ScaleTo::create(.5, original_scale*1.5),NULL);
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        auto seq=TargetedAction::create(stamp, Sequence::create(scale,fadein,scale_1,sound,fadein_next,fadeout,call, NULL));
        
        parent->runAction(seq);
    }
}

#pragma mark - Clear Scene


#pragma mark - Private
Sprite* CatCafeActionManager::createSpriteToCenter(std::string name, bool invisible, cocos2d::Node *parent)
{
    auto spr=Sprite::createWithSpriteFrameName(name);
    spr->setPosition(parent->getContentSize()/2);
    spr->setName(name);
    if (invisible) {
        spr->setOpacity(0);
    }
    return spr;
}

CallFunc* CatCafeActionManager::createSoundAction(std::string fileName)
{
    auto call=CallFunc::create([fileName]{Common::playSE(fileName.c_str());});
    return call;
}
