//
//  EscapeCollectionCell.cpp
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/09/26.
//

#include "EscapeCollectionCell.h"
#include "Utils/Common.h"
#include "UIParts/RoundedBoxSprite.h"
#include "StarsSprite.h"
#include "EscapeStageSprite.h"
#include "DataManager.h"
#include "SettingDataManager.h"

#include "Utils/ValueHelper.h"

#define TAG_Band "Band"
#define NotificationKey_LoadingIcon "NotificationKeyLoadingIcon"

EscapeCollectionCell*EscapeCollectionCell::create(int cellIndex)
{
    auto node=new EscapeCollectionCell();
    if (node&&node->init(cellIndex)) {
        node->autorelease();
        return node;
    }
    CC_SAFE_RELEASE(node);
    
    return node;
}

bool EscapeCollectionCell::init(int cellIndex)
{
    if (!CollectionViewCell::init(nullptr, nullptr)) {
        return false;
    }
    
    m_cellIndex = cellIndex;
    
    createBandDetail();
    
    addNotification();
    
    return true;
}

void EscapeCollectionCell::loadingIconImage()
{
    EscapeDataManager::getInstance()->loadingEachIconData([](bool successed, std::string fileName){
        
        if (!successed) return;
            
        auto evt = EventCustom(NotificationKey_LoadingIcon);
        auto data = Value(fileName);
        evt.setUserData(&data);
        Director::getInstance()->getEventDispatcher()->dispatchEvent(&evt);
    });
}

void EscapeCollectionCell::addNotification()
{
    auto loadedListner=EventListenerCustom::create(NotificationKey_LoadingIcon, [this](EventCustom*event)
                                                   {
                                                       auto data = (Value*)event->getUserData();
                                                       auto fileName = data->asString();
                                                       auto fileNameDir = EscapeDataManager::getInstance()->getIconDirWithFile(fileName);
                                                       
                                                       if (getName() == fileName) {//thisに表示するべき画像をダウンロードしたら
                                                           //読み込んだアイコン画像に更新。
                                                           auto iconImage = (Sprite*)getNormalImage()->getChildByName(IconName);
                                                           
                                                           float size = iconImage->getScale() * iconImage->getContentSize().width;
                                                           iconImage->setTexture(fileNameDir);
                                                           iconImage->setScale(size/iconImage->getContentSize().width);
                                                       }
                                                   });
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(loadedListner,this);
}

#pragma mark - パブリック
void EscapeCollectionCell::showDetail(const char *titleName)
{
    auto normalImage = getNormalImage();
    
    auto blackBar = Sprite::create();
    
    auto type = EscapeDataManager::getInstance()->getTypeTitleCondition(m_cellIndex);
    bool isNoStar = EscapeDataManager::getInstance()->conditionOfFixedTitle(type);
    
    auto cellSize = normalImage->getContentSize();
    blackBar->setTextureRect(Rect(0, 0, cellSize.width, (isNoStar)? cellSize.height*.2: cellSize.height*.25));
    blackBar->setPosition(getContentSize().width/2, blackBar->getContentSize().height/2);
    blackBar->setOpacity(155);
    blackBar->setColor(Color3B::BLACK);
    
    auto labelFontSize = cellSize.height*.1;
    auto space = cellSize.height*.04;
    auto appNameLabel = Label::createWithTTF(titleName, Common::getUsableFontPath(MyFont), labelFontSize);
    appNameLabel->setPosition(blackBar->getContentSize().width/2, (isNoStar)? blackBar->getContentSize().height/2: appNameLabel->getContentSize().height/2+space/2);
    //appNameLabel->enableBold();太字が良ければ。
    appNameLabel->setTextColor(Color4B::WHITE);
    blackBar->addChild(appNameLabel);
    
    if (!isNoStar){
        auto sortType = SettingDataManager::sharedManager()->getSortType();
        auto starSprite = StarsSprite::create(cellSize.width, sortType);
        starSprite->setPosition(getContentSize().width/2, blackBar->getContentSize().height-labelFontSize/2-space/2);
        starSprite->setScale(labelFontSize/starSprite->getContentSize().height);
        //starSprite->readAverageRate(index);
        starSprite->setRateForSorted(m_cellIndex);
        blackBar->addChild(starSprite);
    }
    normalImage->addChild(blackBar);
}

void EscapeCollectionCell::showBand()
{
    //属性バンドを判定。EscapeDataManager.hのTitleConditionを参照。
    auto condition = EscapeDataManager::getInstance()->getTypeTitleCondition(m_cellIndex);
    if (condition == TitleCondition_None) {//Noneが返ってきた場合は通常のタイトル
        //バンド配列に{期間バンド}を追加
        addPeriodBand(m_cellIndex);
    }
    else if (condition == TitleCondition_REMAKE || condition == TitleCondition_COLLABORATION) { //remakeまたはコラボ
        //バンド配列に{属性バンド}を追加
        addCondition(condition);
        
        //バンド配列に{期間バンド}を追加
        addPeriodBand(m_cellIndex);
    }
    else {//ad、チュートリアル
        //バンド配列に{属性バンド}を追加
        addCondition(condition);
    }
    
    //その他のバンドを追加
    addOtherBand(m_cellIndex);
    
    //バンドを表示します。
    showBandTypes();
}

#pragma mark- プライベート
void EscapeCollectionCell::createBandDetail()
{//{"背景色","文字色","テキスト","右かどうか（bool）","幅広かどうか（bool）","フォントサイズ（float）"}
    
    bandDetailMap[TitleCondition_Unplayed] = {"", BlueColor, "UNPLAYED", false, true, .6};
    bandDetailMap[TitleCondition_Pre] = {BlueColor, "", "PRE", true, false, 0};
    bandDetailMap[TitleCondition_New] = {RedColor, "", "NEW", false, false, 0};
    bandDetailMap[TitleCondition_UP] = {MagendaColor, "", "UP", true, false, 0};
    bandDetailMap[TitleCondition_AD] = {OrangeColor, "", "AD", false, false, 0};
    bandDetailMap[TitleCondition_REMAKE] = {GreenColor, "", "REMAKE", false, true, 0};
    bandDetailMap[TitleCondition_TUTORIAL] = {GreenColor, "", "TUTORIAL", false, true, 0};
    bandDetailMap[TitleCondition_LOCKED] = {RedColor, "", "LOCKED", true, true, 0};
    bandDetailMap[TitleCondition_CLEAR] = {BlueColor, "", "CLEAR", false, true, 0};
    bandDetailMap[TitleCondition_COLLABORATION] = {RedColor, "", "コラボ", false, true, 0};
    bandDetailMap[TitleCondition_CLEAR_MINIGAME] = bandDetailMap[TitleCondition_CLEAR];
    bandDetailMap[TitleCondition_ISPLAYING] = {"", RedColor, "NOW PLAYING", false, true, .6};
    bandDetailMap[TitleCondition_VOTE] = {GreenColor2, "", "投票結果", false, true, 0};

    int days = DataManager::sharedManager()->getDaysToLocked(m_cellIndex);
    auto enDays = Common::transformToPlural(days, "day", "days");

    bandDetailMap[TitleCondition_COUNTDOWN_TOLOCKED] = {RedColor, "", (days == 1)? Common::localize("Until today", "本日まで") : StringUtils::format(Common::localize("%d " + enDays, "あと%d日").c_str(), days) , true, true, 0.65};
    
    bandDetailMap[TitleCondition_IN_REPAIRING] = {YellowColor2, WhiteColor2, "IN REPAIRING", true, true, 0.65};
}

void EscapeCollectionCell::showBandTypes()
{
    std::vector<int> leftRightCounts = {0, 0};
    for (auto titleCondition :titleConditions) {
        auto bandDetail = bandDetailMap[titleCondition];
        //サイズを決定
        auto size = Size((bandDetail.isWide)?getContentSize().width*.4:getContentSize().width*.2, getContentSize().width*.1);
        
        //左右を決定
        int leftRightCount = leftRightCounts[bandDetail.isRight];
        float delta = getContentSize().width*.05;//表示位置を隅っこからどのくらい中央に寄せるか。
        float x = (!bandDetail.isRight)? size.width*.5 + delta:getContentSize().width - size.width*.5 - delta;//左右の位置
        auto position = Vec2(x, getContentSize().height - size.height/2 - delta - (delta/2 + size.height) * leftRightCount);
        
        auto normalImage = getNormalImage();
        //コラボなら背景ガウス
        if (titleCondition == TitleCondition_COLLABORATION) {
            auto gauss = Sprite::createWithSpriteFrameName("wideband_gauss.png");
            gauss->setScale(size.width*1.1/gauss->getContentSize().width, size.height*1.5/gauss->getContentSize().height);
            gauss->setPosition(position);
            normalImage->addChild(gauss);
        }
        
        //角丸画像のファイル名と背景色を決定
        auto roundedFileName = (!bandDetail.isWide)? "band.png": "wideband.png";
        auto backColorCode = bandDetail.backColorCode;
        auto rounded = Sprite::createWithSpriteFrameName(roundedFileName);
        rounded->setScale(size.width/rounded->getContentSize().width, size.height/rounded->getContentSize().height);
        rounded->setPosition(position);
        rounded->setColor((backColorCode == "") ? Color3B::WHITE : Common::getColorFromHex(backColorCode));
        rounded->setTag(titleCondition);
        normalImage->addChild(rounded);
        
        //ラベルテキストとテキスト文字色を決定
        auto back = Sprite::create();
        back->setTextureRect(Rect(0, 0, size.width, size.height));
        back->setPosition(rounded->getPosition());
        back->setOpacity(0);
        normalImage->addChild(back);

        log("タイトルコンディション%s",bandDetail.text.c_str());
        auto textColorCode = bandDetail.textColorCode;
        auto fontSizeRatio = (bandDetail.fontSizeRatio == 0)? .8 : bandDetail.fontSizeRatio;
        
        auto label = Label::createWithSystemFont(bandDetail.text, "", size.height*fontSizeRatio);
    
        //auto label = Label::createWithTTF(text, ArialFont, size.height*.8);
        label->enableBold();
        label->setTextColor((textColorCode == "") ? Color4B::WHITE :Color4B(Common::getColorFromHex(textColorCode)));
        label->setAlignment(TextHAlignment::CENTER);
        label->setPosition(back->getContentSize()/2);
        back->addChild(label);
        
        if (titleCondition == TitleCondition_CLEAR_MINIGAME) {//ミニゲームまでクリアしていたら、レイアウトを変える。
            auto crownSize = size.height*.7;
            
            float space = (size.width-crownSize-label->getContentSize().width)/5;//左右スペース*2+SpriteとLabelのスペースで合計5個分
            float hSpace = space*2;//左右のスペース。

            auto crown = Sprite::createWithSpriteFrameName("crown_icon.png");
            crown->setScale(crownSize/crown->getContentSize().height);
            crown->setPosition(hSpace+crownSize/2, back->getContentSize().height/2);
            crown->setColor(Color3B::YELLOW);
            back->addChild(crown);
            
            label->setPositionX(hSpace+crownSize+space+label->getContentSize().width/2);
        }
        
        //左右追加した方のカウントを増やす
        leftRightCounts.at(bandDetail.isRight) = leftRightCount+1;
    }
}

void EscapeCollectionCell::addCondition(TitleCondition condition)
{
    //バンド詳細mapに該当キーがあるなら
    if (bandDetailMap.count(condition) == 1)
        titleConditions.push_back(condition);
}

void EscapeCollectionCell::addPeriodBand(int index)
{
    auto releaseCondition = DataManager::sharedManager()->getIsValidTimeCondition(index);

    auto clearCondition = DataManager::sharedManager()->getClearTitleCondition(index);
    
    //バンド表示のための条件を満たしているかどうかをプレイ履歴などから確認.
    if (releaseCondition == TitleCondition_New) {
        //newでかつ、プレイの痕跡ある場合はnewのバンドは貼らない。
        if (clearCondition == TitleCondition_Unplayed) {
            addCondition(releaseCondition);
        }
    }
    else if (releaseCondition != TitleCondition_None) {
        addCondition(releaseCondition);
    }
}

void EscapeCollectionCell::addOtherBand(int index)
{
    auto timeCondition = DataManager::sharedManager()->getIsValidTimeCondition(index);
    auto clearCondition = DataManager::sharedManager()->getClearTitleCondition(index);
    
    //TitleCondition_Unplayed状態の時には下記状態を優先する
    auto priorityTitleCondition = timeCondition == TitleCondition_Waiting || timeCondition == TitleCondition_Pre || timeCondition == TitleCondition_New;
    
    if (clearCondition != TitleCondition_None && !(priorityTitleCondition && clearCondition == TitleCondition_Unplayed)) {//waiting,pre,newの時はNew優先です。
        addCondition(clearCondition);
    }
    
    if (EscapeDataManager::getInstance()->isRepairing(index)) {
        addCondition(TitleCondition_IN_REPAIRING);
    }
    
    //アップデートバンドを追加→
    //if(DataManager::sharedManager()->isUpdateCondition(index))
        //addCondition(TitleCondition_UP);
     
}



