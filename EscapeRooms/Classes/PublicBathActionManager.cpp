//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "PublicBathActionManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"

using namespace cocos2d;

PublicBathActionManager* PublicBathActionManager::manager =NULL;

PublicBathActionManager* PublicBathActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new PublicBathActionManager();
    }
    return manager;
}

#pragma mark - Back
void PublicBathActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="sweat"){
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(3))
        {//汗の猿
            Vector<FiniteTimeAction*>actions;
            auto distance=parent->getContentSize().height*.1;
            for (int i=0; i<2; i++) {
                auto sweat=createSpriteToCenter(StringUtils::format("sup_44_sweat_%d.png",i), true, parent);
                parent->addChild(sweat);
                
                actions.pushBack(TargetedAction::create(sweat, Sequence::create(FadeIn::create(1),
                                                                                Spawn::create(EaseInOut::create(MoveBy::create(2, Vec2(0, -distance)), 1.5),
                                                                                              Sequence::create(DelayTime::create(1),
                                                                                                               FadeOut::create(1),
                                                                                                               MoveTo::create(.1, parent->getContentSize()/2), NULL),
                                                                                              NULL),
                                                                               DelayTime::create(1), NULL)));
            }
            
            parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),actions.at(1),actions.at(0),actions.at(1)->clone(),actions.at(0)->clone(),actions.at(0)->clone(),actions.at(1)->clone(),DelayTime::create(1), NULL), UINT_MAX));
        }
    }
    else if(key=="akira"&&DataManager::sharedManager()->getEnableFlagWithFlagID(11))
    {//アキラ
        auto monkey=createSpriteToCenter("sup_39_monkey_1.png", false, parent);
        parent->addChild(monkey);
        
        auto monkey_inter=createSpriteToCenter("sup_39_monkey_2.png", true, parent);
        parent->addChild(monkey_inter);
        
        Vector<FiniteTimeAction*>actions;
        auto duration_out=.2;
        auto duration_in=.15;
        auto count=4;
        
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            count=5;
        }
        
        for (int i=0; i<count; i++) {
            Sprite* dish;
            
            if (DataManager::sharedManager()->isPlayingMinigame()&&i==count-1) {
                dish=parent->getChildByName<Sprite*>("sup_39_mistake.png");
                dish->setOpacity(0);
            }
            else{
                
                dish=createSpriteToCenter(StringUtils::format("sup_39_%d.png",i), true, parent);
                parent->addChild(dish);
            }
            
            //回す最中の絵をはさむ
            auto change=createChangeAction(duration_out, duration_in, monkey, monkey_inter);
            auto change_1=Spawn::create(createChangeAction(duration_out, duration_in, monkey_inter, monkey),
                                        TargetedAction::create(dish, FadeIn::create(duration_in)),
                                        createSoundAction("po.mp3"), NULL);
            auto delay=DelayTime::create(2);
            auto turn=Spawn::create(createChangeAction(duration_out, duration_in, monkey,monkey_inter),
                                    TargetedAction::create(dish, FadeOut::create(duration_out)),
                                    createSoundAction("po.mp3"), NULL);
            auto turn_1=createChangeAction(duration_out, duration_in, monkey_inter, monkey);

            if (DataManager::sharedManager()->isPlayingMinigame()&&i==count-1) {
                actions.pushBack(Sequence::create(change,change_1, NULL));
            }
            else{
                actions.pushBack(Sequence::create(change,change_1,delay,turn,turn_1,DelayTime::create(.7), NULL));
            }
        }
        
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            auto call=CallFunc::create([]{DataManager::sharedManager()->setTemporaryFlag("mistake_dish", 1);});
            parent->runAction(Sequence::create(DelayTime::create(.5),createSoundAction("monkey.mp3"),DelayTime::create(.5),actions.at(0),actions.at(1),actions.at(2),actions.at(3),actions.at(4),call, NULL));
        }
        else{
            parent->runAction(Repeat::create(Sequence::create(DelayTime::create(.5),createSoundAction("monkey.mp3"),DelayTime::create(.5),actions.at(0),actions.at(1),actions.at(2),actions.at(3),DelayTime::create(1), NULL),UINT_MAX));
        }
    }
    else if(key=="back_14"&&!DataManager::sharedManager()->getEnableFlagWithFlagID(27)){
        auto pig=parent->getChildByName("sup_14_pig.png");
        pig->setCascadeOpacityEnabled(true);
        auto head=AnchorSprite::create(Vec2(.468, .477), parent->getContentSize(), "sup_14_head.png");
        pig->addChild(head);
        auto glass=createSpriteToCenter("sup_14_glass.png", false, parent);
        head->addChild(glass);
    }
    else if(key=="back_33"&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(34)&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(27)){
        auto pig=parent->getChildByName<Sprite*>("sup_33_pig_0.png");
        auto distance=parent->getContentSize().height*.02;
        
        parent->runAction(Repeat::create(swingAction(Vec2(0, -distance), 2, pig, 1.5, false), UINT_MAX));
    }
    else if(key=="clear"){
        auto story=StoryLayer::create("ending", [](Ref*ref){
            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

            auto story_1=StoryLayer::create("ending1", [](Ref*ref){
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story_1);
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
}

#pragma mark - Touch
void PublicBathActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="towel")
    {
        Common::playSE("p.mp3");
        
        auto stain=parent->getChildByName<Sprite*>("sup_16_stain.png");
        auto mark=createSpriteToCenter("sup_16_mark.png", true, parent);
        parent->addChild(mark);
        
        auto item=createSpriteToCenter("sup_16_towel.png", true, parent);
        item->setOpacity(0);
        parent->addChild(item);
        
        //fadein
        auto fadein=FadeIn::create(1);
        auto sound=CallFunc::create([this]{
            Common::playSE("goshigoshi.mp3");
        });
        //move
        auto distance=parent->getContentSize().width*.025;
        auto duration=.2;
        Vector<FiniteTimeAction*>moves;
        for (int i=1; i<6; i++) {
            auto move=MoveBy::create(duration+i*.025, Vec2(distance*i*pow(-1, i)+distance*1*(i/2), distance*i*pow(-1, i)-distance*.4*(i/2)));
            moves.pushBack(move);
        }
        
        auto seq=TargetedAction::create(item, Sequence::create(fadein,sound,Sequence::create(moves), NULL));
        auto change=Spawn::create(TargetedAction::create(item, FadeOut::create(.7)),
                                  createChangeAction(1, .7, stain, mark), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq,change,DelayTime::create(.5),call,NULL));
    }
    else if(key=="water")
    {
        auto back=createSpriteToCenter("back_15.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        SupplementsManager::getInstance()->addSupplementToMain(back, 15);
        
        //
        auto water_0=createSpriteToCenter("sup_15_water_0.png", true, parent);
        back->addChild(water_0);
        
        auto water_1=createSpriteToCenter("sup_15_water_1.png", true, parent);
        water_1->setCascadeOpacityEnabled(true);
        back->addChild(water_1);
        
        auto racoon=createSpriteToCenter("sup_15_racoon_0.png", false, parent);
        water_1->addChild(racoon);
        
        auto monkey=createSpriteToCenter("sup_15_monkey.png", true, parent);
        water_1->addChild(monkey);
        
        auto coin=createSpriteToCenter("sup_15_coin.png", true, parent);
        water_1->addChild(coin);
        
        //animation
        auto fadein=TargetedAction::create(back, FadeIn::create(1.5));
        auto delay=DelayTime::create(1);
        auto fadein_water_0=Spawn::create(createSoundAction("jaa.mp3"),
                                          TargetedAction::create(water_0, FadeIn::create(1)), NULL);
        auto delay_0=DelayTime::create(.5);
        auto change_water=createChangeAction(1, .5, water_0, water_1);
        auto fadein_monkey=Spawn::create(TargetedAction::create(monkey, FadeIn::create(1)),
                                         TargetedAction::create(coin, FadeIn::create(1)),
                                         createSoundAction("monkey.mp3"), NULL);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,fadein_water_0,delay_0,change_water,delay->clone(),fadein_monkey,delay_0->clone(),call,NULL));
    }
    else if(key=="back_14"){
        Common::playSE("pig.mp3");
        auto head=parent->getChildByName("sup_14_pig.png")->getChildByName("sup_14_head.png");
        
        auto bounce=createBounceAction(head, .3);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(bounce,call, NULL));
    }
    else if(key=="board"){
        auto back=createSpriteToCenter("back_9.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        SupplementsManager::getInstance()->addSupplementToMain(back, 9);
        
        auto board=back->getChildByName<Sprite*>("sup_9_board.png");
        auto board_after=createSpriteToCenter("sup_9_board_1.png", true, parent);
        back->addChild(board_after);

        auto fadein=TargetedAction::create(back, FadeIn::create(1.5));
        auto delay=DelayTime::create(1);
        auto change=createChangeAction(1, .5, board, board_after);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,change,delay->clone(),call, NULL));
    }
    else if(key.compare(0,4,"coin")==0)
    {
        Common::playSE("p.mp3");
        
        Vector<FiniteTimeAction*>actions;
        auto index=atoi(key.substr(5,1).c_str());
        
        auto coin=createSpriteToCenter("sup_41_coin.png", true, parent);
        parent->addChild(coin);
        
        auto milk=createSpriteToCenter("sup_41_milk.png", true, parent);
        parent->addChild(milk);
        
        auto insert=TargetedAction::create(coin, Sequence::create(FadeIn::create(1),
                                                                  DelayTime::create(.5),
                                                                  createSoundAction("chalin.mp3"),
                                                                  FadeOut::create(.5), NULL));
        actions.pushBack(insert);
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        auto fall_milk=Spawn::create(TargetedAction::create(milk, FadeIn::create(.2)),
                                     createSoundAction("goto.mp3"), NULL);
        if (index==1) {
            actions.pushBack(fall_milk);
            actions.pushBack(delay->clone());
        }
        
        auto call=CallFunc::create([callback,index]{
            if (index==0) {
                Common::playSE("pinpon.mp3");
            }
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if (key=="cloud")
    {//吹きだし
        Vector<FiniteTimeAction*>actions;
        auto num=DataManager::sharedManager()->getNowBack();
        auto cloud_anchor=Vec2(.665,.74);
        
        if (num==53) {
            cloud_anchor=Vec2(.636,.655);
        }
        
        auto cloud=AnchorSprite::create(cloud_anchor, parent->getContentSize(), StringUtils::format("sup_%d_cloud.png",num));
        cloud->setOpacity(0);
        parent->addChild(cloud);
        auto original_scale=cloud->getScale();
        cloud->setScale(.1);
        
        if (num==40) {
            Common::playSE("pig_1.mp3");

            auto seq_pig=createBounceAction(parent->getChildByName("sup_40_pig.png"), .3);
            actions.pushBack(seq_pig);
        }
        else{
            Common::playSE("monkey.mp3");
        }
        
        auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(.5, original_scale),
                                                                                    FadeIn::create(.5), NULL),
                                                                      createSoundAction("nyu.mp3"),
                                                                      DelayTime::create(.5),
                                                                      Spawn::create(ScaleTo::create(.5, .1),
                                                                                    FadeOut::create(.5), NULL), NULL));
        actions.pushBack(seq_cloud);
        
        auto spawn=Spawn::create(actions);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,call, NULL));
    }
    else if(key=="dish")
    {//アキラにさら
        Common::playSE("p.mp3");
        
        auto dish=createSpriteToCenter("sup_39_dish.png", true, parent);
        parent->addChild(dish);
        
        auto monkey=parent->getChildByName<Sprite*>("sup_39_monkey_0.png");
        
        auto monkey_after=createSpriteToCenter("sup_39_monkey_1.png", true, parent);
        parent->addChild(monkey_after);
        auto dish_seq=TargetedAction::create(dish, Sequence::create(FadeIn::create(1),
                                                                    ScaleBy::create(1, .9),
                                                                    FadeOut::create(.7), NULL));
        
        auto delay=DelayTime::create(.7);
        auto change=createChangeAction(1, .7, monkey, monkey_after);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(dish_seq,delay,change ,DelayTime::create(1),call, NULL));
    }
    else if(key=="milk")
    {//みるく
        Common::playSE("p.mp3");
        
        auto milk=createSpriteToCenter("sup_40_milk.png", true, parent);
        parent->addChild(milk);
        
        auto pig=parent->getChildByName<Sprite*>("sup_40_pig.png");
        
        auto milk_seq=TargetedAction::create(milk, Sequence::create(FadeIn::create(1),
                                                                    ScaleBy::create(1, .9),
                                                                    FadeOut::create(.7), NULL));
        
        auto delay=DelayTime::create(1);
        
        Vector<FiniteTimeAction*>actions;
        auto duration=1.0f;
        for (int i=0; i<4; i++)
        {
            auto anim=createSpriteToCenter(StringUtils::format("anim_40_%d.png",i), true, parent);
            parent->addChild(anim);
            
            if (i==0) {
                actions.pushBack(createSoundAction("drink.mp3"));
                actions.pushBack(createChangeAction(duration, duration*.7, pig, anim));
            }
            else{
                if (i==1) {
                    actions.pushBack(createSoundAction("pig_1.mp3"));
                }
                else if (i==2){
                    actions.pushBack(createSoundAction("sa.mp3"));
                }
                else if (i==3){
                    actions.pushBack(createSoundAction("po.mp3"));
                }
                actions.pushBack(createChangeAction(duration, duration*.7, parent->getChildByName<Sprite*>(StringUtils::format("anim_40_%d.png",i-1)), anim));
            }
            
            actions.pushBack(DelayTime::create(1));
        }
        
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(milk_seq,delay,Sequence::create(actions),DelayTime::create(.5),call, NULL));
    }
    else if(key=="putboard"){
        Common::playSE("p.mp3");
        
        auto board=createSpriteToCenter("sup_52_board.png", true, parent);
        parent->addChild(board);
        
        auto back=createSpriteToCenter("back_13.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        
        SupplementsManager::getInstance()->addSupplementToMain(back, 13);
        
        auto water_anim=createSpriteToCenter("anim_13_0.png", true, back);
        back->addChild(water_anim);
        
        auto water_after=createSpriteToCenter("sup_13_water.png", true, parent);
        water_after->setCascadeOpacityEnabled(true);
        back->addChild(water_after);
        
        auto duck_0=back->getChildByName<Sprite*>("sup_13_duck_0.png");
        
        auto duck_1=createSpriteToCenter("sup_13_duck_1.png", false, parent);
        water_after->addChild(duck_1);
        
        
        auto fadein_board=TargetedAction::create(board, FadeIn::create(1));
        auto sound_board=createSoundAction("kacha.mp3");
        
        auto delay_1=DelayTime::create(1);
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1.5));
        auto fadein_water=Spawn::create(TargetedAction::create(water_anim, FadeIn::create(1)),
                                        createSoundAction("jaa.mp3"), NULL);
        auto change=Spawn::create(createChangeAction(1, .5, water_anim, water_after),
                                  TargetedAction::create(duck_0, FadeOut::create(1)), NULL);
        auto fadeout=TargetedAction::create(back, FadeOut::create(1.5));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(DelayTime::create(.5),fadein_board,sound_board,delay_1,fadein_back,delay_1->clone(),fadein_water,DelayTime::create(.5),change,delay_1->clone(),fadeout,call, NULL));
    }
    else if(key=="shell"){
        Common::playSE("p.mp3");
        auto shell=createSpriteToCenter("sup_25_shell.png", true, parent);
        parent->addChild(shell);
        
        auto hand_0=parent->getChildByName<Sprite*>("sup_25_hand_0.png");
        auto hand_1=createSpriteToCenter("sup_25_hand_1.png", true, parent);
        parent->addChild(hand_1);
        auto duck=createSpriteToCenter("sup_25_duck.png", true, parent);
        parent->addChild(duck);
        
        auto fadein=TargetedAction::create(shell, FadeIn::create(1));
        auto change=Spawn::create(createChangeAction(1, .5, hand_0, hand_1),
                                  createSoundAction("kyuu.mp3"),
                                  TargetedAction::create(duck, FadeIn::create(.5)), NULL);
        auto delay=DelayTime::create(.7);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,change,delay->clone(),call, NULL));
    }
    else if(key=="sake"){
        Common::playSE("p.mp3");
        
        auto cup_0=createSpriteToCenter("sup_53_cup_0.png", true, parent);
        parent->addChild(cup_0);
        auto bottle_0=createSpriteToCenter("sup_53_bottle_0.png", true, parent);
        parent->addChild(bottle_0);
        auto bottle_1=createSpriteToCenter("sup_53_bottle_1.png", true, parent);
        parent->addChild(bottle_1);
        
        auto monkey_0=parent->getChildByName<Sprite*>("sup_53_monkey_0.png");
        auto monkey_1=createSpriteToCenter("sup_53_monkey_1.png", true, parent);//drink
        parent->addChild(monkey_1);
        auto monkey_2=createSpriteToCenter("sup_53_monkey_2.png", true, parent);//stand
        parent->addChild(monkey_2);
        
        auto cheek=createSpriteToCenter("sup_53_cheek.png", true, parent);
        parent->addChild(cheek);
        
        auto fadein=Spawn::create(TargetedAction::create(cup_0, FadeIn::create(1)),
                                  TargetedAction::create(bottle_0, FadeIn::create(1)),NULL);
        
        //pour
        auto change=Spawn::create(createChangeAction(1, .5, bottle_0, bottle_1),
                                  createSoundAction("jobojobo.mp3"),
                                  NULL);
        auto delay=DelayTime::create(1);
        
        //drink
        auto drink=Spawn::create(TargetedAction::create(bottle_1, FadeOut::create(1)),
                                 TargetedAction::create(cup_0, FadeOut::create(1)),
                                 createChangeAction(1, .5, monkey_0, monkey_1),
                                 createSoundAction("drink.mp3"), NULL);
        
        //cheek
        auto cheek_action=Spawn::create(createChangeAction(1, .5, monkey_1, monkey_0),
                                        TargetedAction::create(cheek, FadeIn::create(.5)),
                                        createSoundAction("monkey.mp3"), NULL);
        
        //stand
        auto stand=Spawn::create(createChangeAction(1, .5, monkey_0, monkey_2),
                                 TargetedAction::create(cheek, FadeOut::create(1)),
                                 createSoundAction("nyu.mp3"), NULL);
        
        //out
        auto fadeout=Spawn::create(TargetedAction::create(monkey_2, FadeOut::create(1)), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,change,delay->clone(),drink,delay->clone(),cheek_action,delay->clone(),stand,delay->clone(),fadeout,delay->clone(),call, NULL));
    }
    else if(key=="ice"){
        Common::playSE("p.mp3");
        
        auto ice=createSpriteToCenter("sup_14_ice.png", true, parent);
        parent->addChild(ice);
        
        auto pig=parent->getChildByName<Sprite*>("sup_14_pig.png");
        auto head=pig->getChildByName("sup_14_head.png");
        auto glass=head->getChildByName("sup_14_glass.png");

        auto water=createSpriteToCenter("sup_14_water.png", true, parent);
        parent->addChild(water);
        auto paper=createSpriteToCenter("sup_14_paper.png", true, parent);
        parent->addChild(paper);
        auto anim_pig=createSpriteToCenter("anim_14_pig.png", true, parent);
        parent->addChild(anim_pig);
        
        auto partilce=SupplementsManager::getInstance()->particles.at(0);
        
        auto delay_0=DelayTime::create(.5);
        auto fadein=TargetedAction::create(ice, FadeIn::create(1));
        auto delay_1=DelayTime::create(1);
        auto melt=Spawn::create(TargetedAction::create(ice, FadeOut::create(1)),
                                createSoundAction("shuuu.mp3"), NULL);
        auto stop_particle=CallFunc::create([partilce]{
            if (partilce) {
                partilce->setDuration(.1);
            }
        });
        auto glass_action=Spawn::create(TargetedAction::create(glass, MoveBy::create(.5, Vec2(0, parent->getContentSize().height*.05))),
                                        createSoundAction("nyu.mp3"), NULL);
        auto pig_action=Sequence::create(Spawn::create(createChangeAction(1, .5, pig, anim_pig),
                                                       TargetedAction::create(glass, FadeOut::create(1)),
                                                       createSoundAction("pig.mp3"), NULL), NULL);
        auto fall=Spawn::create(createSoundAction("bashaa.mp3"),
                                createChangeAction(1, .5, anim_pig, water),
                                TargetedAction::create(paper, FadeIn::create(.5)), NULL);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(delay_0,fadein,delay_1,melt,stop_particle,delay_1->clone(),glass_action,delay_1->clone(),pig_action,delay_1->clone(),fall,delay_1->clone(),call, NULL));
    }
    else if(key=="harisen"){
        Common::playSE("p.mp3");
        
        auto pig_sleep=parent->getChildByName<Sprite*>("sup_33_pig_0.png");
        auto pig_awake=createSpriteToCenter("sup_33_pig_1.png", true, parent);
        parent->addChild(pig_awake);
        
        auto harisen=AnchorSprite::create(Vec2(.86, .5), parent->getContentSize(), "sup_33_harisen.png");
        harisen->setOpacity(0);
        parent->addChild(harisen);
        
        auto attack=TargetedAction::create(harisen,Sequence::create(FadeIn::create(1),
                                                                    MoveBy::create(1, Vec2(-parent->getContentSize().width*.1, parent->getContentSize().height*.1)),
                                                                    EaseOut::create(RotateBy::create(1, 10), 1.5),
                                                                    DelayTime::create(1),
                                                                    EaseIn::create(RotateBy::create(.2, -75), 1.5),
                                                                    createSoundAction("pan.mp3"),
                                                                    Spawn::create(EaseOut::create(RotateBy::create(.5, 30), 1.5),
                                                                                  FadeOut::create(.5), NULL),
                                                                    NULL));
        auto delay=DelayTime::create(1);
        auto wakeup=Spawn::create(createChangeAction(1, .5, pig_sleep, pig_awake),
                                  createSoundAction("pig.mp3"), NULL);
        auto fadeout=TargetedAction::create(pig_awake, FadeOut::create(1));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(attack,delay,wakeup,delay->clone(),fadeout,delay->clone(),call, NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void PublicBathActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{
    if (key.compare(0,5,"stamp")==0) {

    }
}

