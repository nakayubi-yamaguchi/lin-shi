//
//  AnalyticsManager.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/05/30.
//
//

#include "AnalyticsManager.h"
#include "Utils/FireBaseBridge.h"
#include "Utils/Common.h"
#include "EscapeDataManager.h"
#include "DataManager.h"

using namespace cocos2d;

AnalyticsManager*AnalyticsManager::manager=NULL;

AnalyticsManager*AnalyticsManager::getInstance()
{
    if (manager==NULL) {
        manager=new AnalyticsManager();
        //manager->setAnalyticsUserTypeLogin(AnalyticsUserTypeLogin_None);
    }
    return manager;
}

#pragma mark - 新規ユーザー判定
bool AnalyticsManager::isNewUser()
{
    return getInstallVersion()==Application::getInstance()->getVersion();
}

std::string AnalyticsManager::getInstallVersion()
{
    if (UserDefault::getInstance()->getStringForKey(UserKey_InstallVersion).size()==0)
    {//バージョン登録がされていない
        UserDefault::getInstance()->setStringForKey(UserKey_InstallVersion, Application::getInstance()->getVersion());
        log("インストールしたバージョンを保存:%s",Application::getInstance()->getVersion().c_str());
    }
    
    //log("分析 インストールしたバージョンは%s",UserDefault::getInstance()->getStringForKey(UserKey_InstallVersion).c_str());
    
    return UserDefault::getInstance()->getStringForKey(UserKey_InstallVersion);
}

#pragma mark - イベント送信
void AnalyticsManager::sendAnalytics(AnalyticsKeyType keyType)
{
    if (!canSendAnalytics(keyType)) {
        return;
    }
    
    
    std::string autoPromoteTemporaryKey="autoPromoteCount";
    int autoPromoteCount=DataManager::sharedManager()->getTemporaryFlag(autoPromoteTemporaryKey).asInt();
    
    bool appendVersion=true;
    
    //キーによる処理わけ
    std::string key="";
    switch (keyType) {
        case AnalyticsKeyType_TutorialAlert:
            appendVersion=false;
            key=StringUtils::format("Tutorial_FirstAlert");
            break;
        case AnalyticsKeyType_StartOnTitleScene:
            key=StringUtils::format("Tutorial_StartOnTitleScene");
            appendVersion=false;

            break;
        case AnalyticsKeyType_ClearRoom_1:
            appendVersion=false;

            key=StringUtils::format("Tutorial_ClearRoom_1");
            break;
        case AnalyticsKeyType_ClearRoom_2:
            appendVersion=false;

            key=StringUtils::format("Tutorial_ClearRoom_2");
            break;
        case AnalyticsKeyType_ClearRoom_3:
            appendVersion=false;

            key=StringUtils::format("Tutorial_ClearRoom_3");
            break;
        case AnalyticsKeyType_CloseClearScene:
            appendVersion=false;
            key=StringUtils::format("Tutorial_CloseClearScene");
            break;
        case AnalyticsKeyType_ChooseFirstStage:
            appendVersion=false;
            key=StringUtils::format("FirstStage_Choose");
            UserDefault::getInstance()->setBoolForKey(UserKey_SendChooseFirstStage, true);
            break;
        case AnalyticsKeyType_StartFirstStage:
            appendVersion=false;
            key=StringUtils::format("FirstStage_Start");
            UserDefault::getInstance()->setBoolForKey(UserKey_SendStartFirstStage, true);
            break;
        case AnalyticsKeyType_ClearFirstStage:
            appendVersion=false;
            key=StringUtils::format("FirstStage_Clear");
            UserDefault::getInstance()->setBoolForKey(UserKey_SendClearFirstStage, true);
            break;
        case AnalyticsKeyType_GetStamp_0:
            appendVersion=false;
            key=StringUtils::format("GetStamp_0");
            break;
        case AnalyticsKeyType_GetStamp_1:
            appendVersion=false;
            key=StringUtils::format("GetStamp_1");
            break;
        case AnalyticsKeyType_GetStamp_2:
            appendVersion=false;
            key=StringUtils::format("GetStamp_2");
            break;
        case AnalyticsKeyType_GetHint_ByCoin:
            appendVersion=false;
            key=StringUtils::format("GetHint_ByCoin");
            break;
        case AnalyticsKeyType_GetHint_ByMovie:
            appendVersion=false;
            key=StringUtils::format("GetHint_ByMovie");
            break;
        case AnalyticsKeyType_GetMinigame_ByCoin:
            appendVersion=false;
            key=StringUtils::format("GetMiniGame_ByCoin");
            break;
        case AnalyticsKeyType_GetMinigame_ByMovie:
            appendVersion=false;
            key=StringUtils::format("GetMiniGame_ByMovie");
            break;
        case AnalyticsKeyType_GetMinigameBonus:
            appendVersion=false;
            key=StringUtils::format("GetMiniGameBonus");
            break;
        case AnalyticsKeyType_LaunchCount:
            appendVersion=false;
            key=StringUtils::format("LaunchCount");
            break;
        case AnalyticsKeyType_ShopOpen:
            appendVersion=false;
            key=StringUtils::format("ShopOpen");
            break;
        case AnalyticsKeyType_ClickBeginnersPackSmall:
            appendVersion=false;
            key=StringUtils::format("ShopClickBeginnersPackSmall");
            break;
        case AnalyticsKeyType_ClickBeginnersPackBig:
            appendVersion=false;
            key=StringUtils::format("ShopClickBeginnersPackBig");
            break;
        case AnalyticsKeyType_BuyBeginnersPackSmall:
            appendVersion=false;
            key=StringUtils::format("ShopBuyBeginnersPackSmall");
            break;
        case AnalyticsKeyType_BuyBeginnersPackBig:
            appendVersion=false;
            key=StringUtils::format("ShopBuyBeginnersPackBig");
            break;
        case AnalyticsKeyType_BuyBeginnersPackBig_ByReccomendButton:
            appendVersion=false;
            key=StringUtils::format("ShopBuyBeginnersPackBigByReccomend");
            break;
        case AnalyticsKeyType_Buy126Coin:
            appendVersion=false;
            key=StringUtils::format("ShopBuy126Coin");
            break;
        case AnalyticsKeyType_AutoPromote_Show:
            autoPromoteCount++;
            appendVersion=false;
            DataManager::sharedManager()->setTemporaryFlag(autoPromoteTemporaryKey, autoPromoteCount);
            key=StringUtils::format("AutoPromoteShow_%d_%d",AutoPromoteHint,autoPromoteCount);
            break;
        case AnalyticsKeyType_AutoPromote_GotHint:
            appendVersion=false;
            key=StringUtils::format("AutoPromoteGotHint_%d_%d",AutoPromoteHint,autoPromoteCount);
            break;
        case AnalyticsKeyType_RegisterNotification:
            appendVersion=false;
            key=StringUtils::format("RegistNotification_%d",getAnalyticsNotificationType());
            break;
        default:
            break;
    }
    
    //versionをappend
    if (appendVersion) {
        auto version=atof(getInstallVersion().c_str());
        //4以下は分析にて統合する
        if (version<4) {
            version=4.0;
        }
        
        key.append(StringUtils::format("%.1f",version));
        //.を_に置き換える
        std::string from=".";
        std::string to="_";
        while (key.find(from)!=std::string::npos) {
            auto target_pos=key.find(from);
            key.replace(target_pos, from.length(), to);
        }
    }
    
    log("%s : 分析名は %s",__FUNCTION__,key.c_str());

    
    //debug
    sendFirebaseAnalyticsWithNameForDebug(key.c_str());
    //release
    sendFirebaseAnalyticsWithName(key.c_str());
}

bool AnalyticsManager::canSendAnalytics(AnalyticsKeyType keyType)
{
    if(keyType==AnalyticsKeyType_StartFirstStage)
    {
        if (UserDefault::getInstance()->getBoolForKey(UserKey_SendStartFirstStage)) {
            log("分析 送信済みのAnalyticsです StartFirstStage");
            return false;
        }
    }
    else if(keyType==AnalyticsKeyType_ChooseFirstStage)
    {
        if (UserDefault::getInstance()->getBoolForKey(UserKey_SendChooseFirstStage)) {
            log("分析 送信済みのAnalyticsです ChooseFirstStage");
            return false;
        }
    }
    else if(keyType==AnalyticsKeyType_ClearFirstStage)
    {
        if (UserDefault::getInstance()->getBoolForKey(UserKey_SendClearFirstStage)) {
            log("分析 送信済みのAnalyticsです ClearFirstStage");
            return false;
        }
    }
    
    return true;
}

void AnalyticsManager::sendFirebaseAnalyticsWithName(const char *eventName)
{
    Common::performProcessOnlyRelease([eventName](){
        FireBaseBridge::getInstance()->analyticsWithName(eventName);
    });
}

void AnalyticsManager::sendFirebaseAnalyticsWithNameForDebug(const char *eventName)
{
    auto key=StringUtils::format("Debug_%s",eventName);
    Common::performProcessForDebug_1([key]{
        FireBaseBridge::getInstance()->analyticsWithName(key.c_str());
    });
}

#pragma mark - 型変換
/*int AnalyticsManager::transformAnalytics(AnalyticsUserTypeTutorial type)
{
    return atoi(StringUtils::format("%d",type).c_str());
}*/

/*int AnalyticsManager::transformAnalytics(AnalyticsUserTypeLogin type)
{
    return atoi(StringUtils::format("%d",type).c_str());
}*/

