//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__CabaretActionManager__
#define __EscapeContainer__CabaretActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class CabaretActionManager:public CustomActionManager
{
public:
    static CabaretActionManager* manager;
    static CabaretActionManager* getInstance();
    
    int getDefaultBackNum();
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void customAction(std::string key,Node*parent);

private:
    void singerAction(Node*parent);
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
