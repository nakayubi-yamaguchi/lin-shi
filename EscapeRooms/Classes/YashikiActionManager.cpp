//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "YashikiActionManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"

using namespace cocos2d;

YashikiActionManager* YashikiActionManager::manager =NULL;

YashikiActionManager* YashikiActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new YashikiActionManager();
    }
    return manager;
}

float kakejiku_distance=.2;

#pragma mark - Back
void YashikiActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="back_7"&&!DataManager::sharedManager()->isPlayingMinigame()){
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(30)&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(31))
        {//手裏剣
            Vector<FiniteTimeAction*>actions;
            for (int i=0; i<2; i++) {
                auto mill=parent->getChildByName(StringUtils::format("sup_7_%d.png",i+1));
                
                actions.pushBack(TargetedAction::create(mill, Sequence::create(createSoundAction("hyu.mp3"),
                                                                               EaseInOut::create(RotateBy::create(2, 360), 2),
                                                                               DelayTime::create(1), NULL)));
            }
            
            parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),actions.at(1),actions.at(0),actions.at(1)->clone(),actions.at(0)->clone(),actions.at(0)->clone(),actions.at(1)->clone(),DelayTime::create(1), NULL), UINT_MAX));
        }
    }
    else if (key=="back_17")
    {//掛け軸移動
        auto kakejiku=parent->getChildByName(StringUtils::format("sup_%d_kakejiku_0.png",backID));
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(4)&&
            backID==17) {
            kakejiku->setPositionY(parent->getContentSize().height*(.5+kakejiku_distance));
        }
    }
    else if(key=="back_40"&&DataManager::sharedManager()->getEnableFlagWithFlagID(22))
    {//さの
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<3; i++) {
            auto sano_0=parent->getChildByName<Sprite*>(StringUtils::format("sup_40_%d_0.png",i));
            auto sano_1=createSpriteToCenter(StringUtils::format("sup_40_%d_1.png",i), true, parent);
            parent->addChild(sano_1);
            
            actions.pushBack(Sequence::create(createSoundAction("kacha.mp3"),
                                              createChangeAction(.7, .3, sano_0, sano_1),
                                              DelayTime::create(.5),
                                              createChangeAction(.7, .3, sano_1, sano_0), NULL));
        }
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(.5),actions.at(1),actions.at(2),actions.at(0),actions.at(1)->clone(),actions.at(0)->clone(),actions.at(2)->clone(),DelayTime::create(1), NULL), UINT_MAX));
    }
    else if (key=="eye")
    {//だるまの目
        auto sup=parent->getChildByName("sup_42_eye.png");
        auto distance=parent->getContentSize().width*.015;
        
        auto move=[sup,distance](int direction){
            auto x=distance;
            auto y=distance;
            if (direction==0) {
                x=0;
            }
            else if (direction==1){
                y=0;
            }
            else if (direction==2){
                x=0;
                y=-distance;
            }
            else{
                x=-distance;
                y=0;
            }
            auto moves=TargetedAction::create(sup, Sequence::create(MoveBy::create(.5, Vec2(x, y)),
                                                                    DelayTime::create(.5),
                                                                    MoveBy::create(.5, Vec2(-x, -y)),
                                                                    DelayTime::create(.3), NULL));
            
            return moves;
        };
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(.7),
                                                          move(0),
                                                          move(2),
                                                          move(3),
                                                          move(1),
                                                          DelayTime::create(1), NULL), UINT_MAX));
    }
    else if(key=="back_39"){
        auto mistake=parent->getChildByName("sup_39_mistake.png");
        mistake->setOpacity(0);
        mistake->setPositionY(parent->getContentSize().height*.35);
        
        auto fadein=TargetedAction::create(mistake, FadeIn::create(1));
        auto move=TargetedAction::create(mistake, MoveTo::create(1, parent->getContentSize()/2));
        auto call=CallFunc::create([]{
            Common::playSE("nyu.mp3");
            DataManager::sharedManager()->setTemporaryFlag("doll", 1);});
        
        parent->runAction(Sequence::create(fadein,move,call, NULL));
    }
    else if(key=="back_24"&&DataManager::sharedManager()->isPlayingMinigame()){
        Common::playSE("pig.mp3");
    }
    else if(key=="back_38"&&DataManager::sharedManager()->isPlayingMinigame()){
        Common::playSE("snoring.mp3");
    }
}

#pragma mark - Touch
void YashikiActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="match")
    {
        Common::playSE("p.mp3");
        
        auto match=createSpriteToCenter("sup_17_match.png", true, parent);
        match->setPositionX(parent->getContentSize().width*.6);
        parent->addChild(match);
        
        auto kakejiku=parent->getChildByName<Sprite*>("sup_17_kakejiku_0.png");
        kakejiku->setLocalZOrder(1);
        auto mark=createSpriteToCenter("sup_17_mark.png", false, parent);
        parent->addChild(mark);
        auto face=createSpriteToCenter("sup_17_face.png", true, parent);
        parent->addChild(face);
        auto back=createSpriteToCenter("sup_17_kakejiku_1.png", true, parent);
        parent->addChild(back);
        
        //animation
        auto seq_match=TargetedAction::create(match, Sequence::create(FadeIn::create(.5),
                                                                      MoveTo::create(1, Vec2(parent->getContentSize()/2)),
                                                                    DelayTime::create(.5),
                                                                      Repeat::create(jumpAction(parent->getContentSize().height*.02, .1, match, 1.5), 2),
                                                                      createSoundAction("huo.mp3"),NULL));
        
        //fire
        auto fire=CallFunc::create([this,parent]{
            auto particle=SupplementsManager::getInstance()->getParticle(parent, DataManager::sharedManager()->getParticleVector(false,DataManager::sharedManager()->getNowBack()).at(0).asValueMap());
            parent->addChild(particle);
        });
        
        auto fadeout=TargetedAction::create(match, FadeOut::create(.5));
        auto move=TargetedAction::create(kakejiku, Spawn::create(createSoundAction("weeen.mp3"),
                                                                 MoveBy::create(2, Vec2(0, parent->getContentSize().height*kakejiku_distance)), NULL));
        auto face_action=TargetedAction::create(face, FadeIn::create(.7)) ;
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_match,fire,fadeout,DelayTime::create(.7),move,face_action,DelayTime::create(1),call,NULL));
    }
    else if(key=="tea")
    {
        Common::playSE("p.mp3");
        
        auto item=AnchorSprite::create(Vec2(.51, .4), parent->getContentSize(),"sup_10_teapot.png");
        item->setOpacity(0);
        parent->addChild(item);

        auto face=createSpriteToCenter("sup_10_1.png", true, parent);
        parent->addChild(face);
        auto syuri=createSpriteToCenter("sup_10_open.png", true, parent);
        parent->addChild(syuri);
        auto syuri_1=createSpriteToCenter("sup_10_syuriken.png", true, parent);
        parent->addChild(syuri_1);
        
        //animation
        auto seq_item=TargetedAction::create(item, Sequence::create(FadeIn::create(.5),
                                                                    createSoundAction("jobojobo.mp3"),
                                                                    Repeat::create(swingAction(-10, .5, item, 1.2), 2),
                                                                      DelayTime::create(.2),
                                                                      NULL));
        
        auto fadeout=TargetedAction::create(item, FadeOut::create(.5));
        auto faces_action=Spawn::create(createSoundAction("su.mp3")
                                        ,TargetedAction::create(face, FadeIn::create(.7)),
                                        TargetedAction::create(syuri, FadeIn::create(.7)),
                                        TargetedAction::create(syuri_1, FadeIn::create(.7)), NULL) ;
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_item,DelayTime::create(.7),fadeout,faces_action,DelayTime::create(1),call,NULL));
    }
    else if(key=="sake")
    {
        Common::playSE("p.mp3");
        
        auto item=AnchorSprite::create(Vec2(.51, .2), parent->getContentSize(),"sup_19_sake.png");
        item->setOpacity(0);
        item->setLocalZOrder(1);
        parent->addChild(item);
        
        
        auto kakejiku=parent->getChildByName<Sprite*>("sup_19_kakejiku_0.png");
        auto anim_0=createSpriteToCenter("sup_19_0.png", true, parent);
        parent->addChild(anim_0);
        auto anim_1=createSpriteToCenter("sup_19_1.png", true, parent);
        parent->addChild(anim_1);
        auto anim_2=createSpriteToCenter("sup_19_mark.png", true, parent);
        parent->addChild(anim_2);
        
        auto face=createSpriteToCenter("sup_19_face.png", true, parent);
        face->setLocalZOrder(1);
        parent->addChild(face);
        
        //animation
        auto seq_item=TargetedAction::create(item, Sequence::create(FadeIn::create(.5),
                                                                    createSoundAction("jobojobo.mp3"),
                                                                    Repeat::create(swingAction(-10, .5, item, 1.2), 2),
                                                                    DelayTime::create(.2),
                                                                    NULL));
        
        auto fadeout=TargetedAction::create(item, FadeOut::create(.5));
        
        auto duration_fade=1;
        auto duration_in=.7;
        
        auto action=Sequence::create(createSoundAction("zuzuzu.mp3"),
                                     createChangeAction(duration_fade, duration_in, kakejiku, anim_0),
                                     createChangeAction(duration_fade, duration_in, anim_0, anim_1),
                                     createChangeAction(duration_fade, duration_in, anim_1, anim_2),
                                     createSoundAction("ban.mp3"),
                                     TargetedAction::create(face, FadeIn::create(.7))
                                     ,NULL);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_item,DelayTime::create(.7),fadeout,action,DelayTime::create(1),call,NULL));
    }
    else if(key=="daruma"){
        auto back=DataManager::sharedManager()->getNowBack();
        auto anchor=Vec2(.59,.45);
        if (back==10) {
            anchor=Vec2(.566, .7);
        }
        
        auto cloud=AnchorSprite::create(anchor, parent->getContentSize(), StringUtils::format("sup_%d_cloud.png",back));
        cloud->setOpacity(0);
        parent->addChild(cloud);
        auto original_scale=cloud->getScale();
        cloud->setScale(.1);
        
        auto daruma=parent->getChildByName(StringUtils::format("sup_%d_daruma.png",back));
        
        auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(.5, original_scale),
                                                                                    FadeIn::create(.5), NULL),
                                                                      createSoundAction("nyu.mp3"),
                                                                      DelayTime::create(.5),
                                                                      Spawn::create(ScaleTo::create(.5, .1),
                                                                                    FadeOut::create(.5), NULL), NULL));
        
        auto seq=Sequence::create(createSoundAction("goro.mp3")
                                  ,swingAction(12, .4, daruma, 1.5,true), NULL);
        
        auto spawn=Spawn::create(seq_cloud,seq, NULL);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,call, NULL));
    }
    else if(key=="clear"){
        auto open=SupplementsManager::getInstance()->sliderSprites.at(0);
        open->slide([](Ref*ref){
            auto story=StoryLayer::create("ending", [](Ref*ref){
                EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);
                
                auto story_1=StoryLayer::create("ending1", [](Ref*ref){
                    Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
                });
                Director::getInstance()->getRunningScene()->addChild(story_1);
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
    }
    else if(key=="handle")
    {
        Common::playSE("p.mp3");
        
        auto handle=AnchorSprite::create(Vec2(.5, .185), parent->getContentSize(), "sup_40_handle.png");
        handle->setOpacity(0);
        parent->addChild(handle);
        
        auto fadein=TargetedAction::create(handle, FadeIn::create(1));
        auto seq_handle=TargetedAction::create(handle,Repeat::create(Sequence::create(createSoundAction("gigigi.mp3"),
                                                                                      EaseInOut::create(RotateBy::create(2, 720), 1.5), NULL), 2));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,seq_handle,call, NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void YashikiActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{
    if (key.compare(0,5,"stamp")==0) {

    }
}

