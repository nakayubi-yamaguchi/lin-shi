//
//  AnalyticsManager.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/05/30.
//
//6/21 v3.4 チュートリアルステージでの離脱はほとんどないことがわかったので分析をやめる。
//          UserTypeによるFirstClearまでの指標にしぼり分析。Longの場合はTitleから一覧に戻る場合も対象ユーザーに入るようになった。
//          チュートリアルクリアまで、ではなく継続率が出るユーザー導線パターンをあぶり出す目的

//7/14 v.3.5 チュートリアルはShort Richタイプが両OSで数パーセントいい結果。どのパターンもボトルネックはステージ開始までなので、
//           ローディングにサムの一言を追加。ステージを選択時にAnalyticsポイントを追加。離脱をあぶり出す目的。v_3.6にアップ
//           ログインボーナスタイプによる総動画再生回数をテスト。

#ifndef _____PROJECTNAMEASIDENTIFIER________AnalyticsManager_____
#define _____PROJECTNAMEASIDENTIFIER________AnalyticsManager_____

#include "cocos2d.h"
#define UserKey_FirstOpen "FirstOpen"//初回起動キー Tutorialアラート表示時に保存
#define UserKey_InstallVersion "InstallVersion" //初回起動時にtrue 落としたバージョンを記憶する
#define UserKey_UserTypeTutorial "UserTypeTutorial"//チュートリアルタイプキー Tutorialアラート表示時に保存
#define UserKey_UserTypeLogin "UserTypeLogin"//ログインタイプキー Collection初回表示時にセット
#define UserKey_SetUserTypeLogin "SetUserTypeLogin_4.1"//ログインタイプキーをセットしたか判別用 4.0でスタンプに選定->4.1でスタンプ(Normalに統一)
//#define UserKey_ShopType "Shoptype"//
//#define UserKey_SetShopType "SetShopType"//4.2


#define UserKey_SendStartFirstStage "SendStartFirstStage"
#define UserKey_SendChooseFirstStage "SendChooseFirstStage"
#define UserKey_SendClearFirstStage "SendClearFirstStage"

typedef enum{
    AnalyticsNotificationType_Normal=0,
    AnalyticsNotificationType_Again,
    AnalyticsNotificationType_Again_HighLight
}AnalyticsNotificationType;

typedef enum{
    AnalyticsShopButtonType_Normal=0,
    AnalyticsShopButtonType_Rounded
}AnalyticsShopButtonType;

typedef enum{//firebaseのeventKey
    //sequence tutorial
    AnalyticsKeyType_TutorialAlert=0,
    AnalyticsKeyType_StartOnTitleScene,
    AnalyticsKeyType_ClearRoom_1,
    AnalyticsKeyType_ClearRoom_2,
    AnalyticsKeyType_ClearRoom_3,
    AnalyticsKeyType_CloseClearScene,
    AnalyticsKeyType_ChooseFirstStage,//ローディング画面に遷移前で送信
    
    AnalyticsKeyType_StartFirstStage,
    AnalyticsKeyType_ClearFirstStage,
    
    //sequence bonus
    AnalyticsKeyType_GetRoulette_0,//1回目のルーレット(動画なし)
    AnalyticsKeyType_GetRoulette_1,//2回目の促進
    AnalyticsKeyType_GetRoulette_2,//動画を２回見て3度のボーナスを受けとった
    
    //sequence stamp
    AnalyticsKeyType_GetStamp_0,//１個目のスタンプを取得
    AnalyticsKeyType_GetStamp_1,//２個目
    AnalyticsKeyType_GetStamp_2,//3個目
    //gotHint
    AnalyticsKeyType_GetHint_ByCoin,
    AnalyticsKeyType_GetHint_ByMovie,
    //release minigame
    AnalyticsKeyType_GetMinigame_ByCoin,
    AnalyticsKeyType_GetMinigame_ByMovie,
    //minigame bonus
    AnalyticsKeyType_GetMinigameBonus,
    //launch
    AnalyticsKeyType_LaunchCount,
    
    //shop
    AnalyticsKeyType_ShopOpen,
    AnalyticsKeyType_ClickBeginnersPackSmall,
    AnalyticsKeyType_ClickBeginnersPackBig,
    AnalyticsKeyType_BuyBeginnersPackSmall,
    AnalyticsKeyType_BuyBeginnersPackBig,
    AnalyticsKeyType_BuyBeginnersPackBig_ByReccomendButton,//単品メニューからの推薦より
    AnalyticsKeyType_Buy126Coin,
    
    //auto promote
    AnalyticsKeyType_AutoPromote_Show,
    AnalyticsKeyType_AutoPromote_GotHint,
    
    //notification 承認で送信
    AnalyticsKeyType_RegisterNotification

}AnalyticsKeyType;

class AnalyticsManager
{
public:
    static AnalyticsManager*manager;
    static AnalyticsManager*getInstance();
    
    /**新規ユーザー判定 */
    bool isNewUser();
    std::string getInstallVersion();
    /**FireBaseにイベントデータを送る*/
    void sendAnalytics(AnalyticsKeyType keyType);
    bool canSendAnalytics(AnalyticsKeyType keyType);
    /**Firebase経由でアナリティクスを送信。*/
    void sendFirebaseAnalyticsWithName(const char*eventName);
    /**Firebase経由でDebug用アナリティクスを送信。 debug==1のときのみ。Debug_をappend*/
    void sendFirebaseAnalyticsWithNameForDebug(const char*eventName);
    /**アナリティクス用*/
    /*CC_SYNTHESIZE(AnalyticsTypeTutorial, analyticsTypeTutorial, AnalyticsTypeTutorial);
    CC_SYNTHESIZE(AnalyticsPushType, analyticsPushType, AnalyticsPushType);
    CC_SYNTHESIZE(AnalyticsHintType, analyticsHintType, AnalyticsHintType);*/
    CC_SYNTHESIZE(AnalyticsNotificationType, analyticsNotificationType, AnalyticsNotificationType);
    CC_SYNTHESIZE(AnalyticsShopButtonType, analyticsShopButtonType, AnalyticsShopButtonType);

private:
};



#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
