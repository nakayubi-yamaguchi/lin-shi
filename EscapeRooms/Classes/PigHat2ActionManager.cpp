//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "PigHat2ActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "GameScene.h"
#include "Utils/BannerBridge.h"

using namespace cocos2d;

PigHat2ActionManager* PigHat2ActionManager::manager =NULL;

PigHat2ActionManager* PigHat2ActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new PigHat2ActionManager();
    }

    return manager;
}

#pragma mark - 背景
int PigHat2ActionManager::getDefaultBackNum()
{
    
    if(!DataManager::sharedManager()->isShowedOpeningStory()) {
        return 0;
    }
    
    return 1;
}

#pragma mark - 万能
void PigHat2ActionManager::openingAction(cocos2d::Node *node, const onFinished &callback)
{
    auto scene=(GameScene*)node;
    auto layer=SupplementsManager::getInstance()->createDisableLayer();
    scene->addChild(layer);
    
    auto storyLayer = StoryLayer::create("opening_0", [scene,callback,this,layer](Ref* sen){
        
        auto back=createSpriteToCenter("back_2.png", true, scene->mainSprite);
        scene->mainSprite->addChild(back);
        
        auto black=Sprite::create();
        black->setColor(Color3B::BLACK);
        black->setOpacity(0);
        black->setPosition(scene->mainSprite->getContentSize()/2);
        black->setTextureRect(Rect(0, 0, scene->mainSprite->getContentSize().width, scene->mainSprite->getContentSize().height));
        black->setLocalZOrder(100);
        scene->mainSprite->addChild(black);
        
        auto fadein = TargetedAction::create(black, EaseOut::create(FadeIn::create(.5), 1.5));
        auto call=CallFunc::create([scene, this]{
            
            Common::playSE("stair.mp3");
            
            auto back=createSpriteToCenter("back_2.png", false, scene->mainSprite);
            scene->mainSprite->addChild(back);
        });
        auto delay = DelayTime::create(1);
        auto fadeout = TargetedAction::create(black, EaseOut::create(FadeOut::create(.5), 1.5));
        auto call2=CallFunc::create([scene, this, callback,layer]{
            auto story = StoryLayer::create("opening_1", [scene, callback, layer](Ref* sen){
                layer->removeFromParent();
                
                if (callback) {
                    callback(true);
                }
                
            });
            scene->addChild(story);
            
        });
        
        scene->runAction(Sequence::create(fadein,call,delay,fadeout, call2, NULL));
    });
    scene->addChild(storyLayer);
}

#pragma mark - Back
void PigHat2ActionManager::backAction(std::string key,int backID, Node *parent)
{
    if(key=="balloon")
    {//気球アニメ
        auto distance=parent->getContentSize().height*.025;
        auto duration_move=2.0;

        if(backID==33){
            Vector<FiniteTimeAction*>actions;
            for (int i=0; i<4; i++) {
                auto balloon=createSpriteToCenter(StringUtils::format("sup_33_balloon_%d.png",i), false, parent,true);

                if (i==0&&
                    DataManager::sharedManager()->isPlayingMinigame()) {
                    balloon->addChild(parent->getChildByName("sup_33_mistake.png"));
                }
                
                auto delay=DelayTime::create(.15*i);
                auto moves=TargetedAction::create(balloon, Sequence::create(EaseInOut::create(MoveBy::create(duration_move, Vec2(0, distance)), 1.5),
                                                                            Repeat::create(Sequence::create(EaseInOut::create(MoveBy::create(duration_move*2, Vec2(0, -distance*2)), 1.5),
                                                                                                            EaseInOut::create(MoveBy::create(duration_move*2, Vec2(0, distance*2)), 1.5),
                                                                                                            NULL),UINT_MAX),
                                                                            NULL));
                
                actions.pushBack(Sequence::create(delay,moves, NULL));
            }
            parent->runAction(Spawn::create(actions));
        }
        else{
            Vector<FiniteTimeAction*>actions;
            auto balloon=createSpriteToCenter(StringUtils::format("sup_%d_balloon.png",backID), false, parent,true);
            actions.pushBack(Repeat::create(swingAction(Vec2(0, distance), duration_move, balloon, 1.5, false), UINT_MAX));
            
            if (backID==34) {
                if (DataManager::sharedManager()->isPlayingMinigame()) {
                    auto mistake=parent->getChildByName("sup_34_mistake.png");
                    actions.pushBack(Repeat::create(swingAction(Vec2(0, distance), duration_move, mistake, 1.5, false), UINT_MAX));
                }
                else{
                    createSpriteToCenter("sup_34_k.png", false, balloon,true);
                }
            }
            
            parent->runAction(Spawn::create(actions));
        }
    }
    else if(key=="pillow")
    {
        //action
        Vector<FiniteTimeAction*>actions;
        auto distance=.05*parent->getContentSize().height;
        std::vector<int>indexs={3,1,2,3,1,0};
        Vec2 anchor=Vec2(.12, .38);
        if (backID==7) {
            anchor=Vec2(.27, .25);
            distance=.12*parent->getContentSize().height;
        }
        
        auto pillow=AnchorSprite::create(anchor, parent->getContentSize(), StringUtils::format("sup_%d_pillow.png",backID));
        parent->addChild(pillow);
        auto original_height=pillow->getPositionY();

        actions.pushBack(DelayTime::create(1));
        actions.pushBack(Spawn::create(swingAction(3, .1, pillow, 1.5, true,2),
                                       CallFunc::create([backID]{
            if (backID==7) {
                Common::playSE("bohu.mp3");
            }
        }), NULL));
        
        for (int i=0; i<indexs.size(); i++) {
            actions.pushBack(TargetedAction::create(pillow, Sequence::create(
                                                                             Spawn::create(EaseInOut::create(MoveTo::create(1.5, Vec2(pillow->getPositionX(), original_height+distance*indexs.at(i))), 1.5),
                                                                                           CallFunc::create([backID]{
                                                                                 if (backID==7) {
                                                                                     Common::playSE("pop.mp3");
                                                                                 }
                                                                             }),
                                                                                                                NULL),
                                                                             swingAction(Vec2(0, distance*.1), 1, pillow, 1.2, false),
                                                                             NULL)));
        }
        
        parent->runAction(Repeat::create(Sequence::create(actions), UINT_MAX));
    }
    else if(key=="back_2"){
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(11)) {
            CustomActionManager::backAction("angry",backID, parent);
        }
        else if(!DataManager::sharedManager()->getEnableFlagWithFlagID(32)){
            auto hawk=parent->getChildByName("sup_2_hawk_search.png");
            parent->runAction(Repeat::create(Sequence::create(createBounceAction(hawk, .7,.05),
                                                              DelayTime::create(1),
                                                              NULL),
                                             UINT_MAX));
        }
    }
    else if(key=="angry"&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(11)){
        auto fileName="sup_19_hawk_1.png";
        if (backID==2) {
            fileName="sup_2_hawk_hang.png";
        }
        
        Vector<FiniteTimeAction*>actions;
        auto hawk=parent->getChildByName(fileName);
        if (backID==19) {
            for (int i=0; i<4; i++) {
                auto spr=AnchorSprite::create(Vec2(.42, .42), parent->getContentSize(),true, StringUtils::format("sup_19_angry_%d.png",i));
                hawk->addChild(spr);
                auto original_scale=spr->getScale();
                auto duration=.7;
                auto seq=TargetedAction::create(spr, Sequence::create(Spawn::create(FadeIn::create(duration),
                                                                                    ScaleBy::create(duration,.8), NULL),
                                                                      createSoundAction("surprise.mp3"),
                                                                      DelayTime::create(duration),
                                                                      Spawn::create(ScaleTo::create(duration, original_scale),
                                                                                    FadeOut::create(duration), NULL)
                                                                      , NULL));
                
                actions.pushBack(seq);
            }
            
            Vector<FiniteTimeAction*>actions_1;
            actions_1.pushBack(DelayTime::create(1.5));
            std::vector<int>indexs={0,0,2,1,3};
            for (int i=0; i<indexs.size(); i++) {
                actions_1.pushBack(actions.at(indexs.at(i))->clone());
                actions_1.pushBack(DelayTime::create(1));
            }
            auto rep_angry=Repeat::create(Sequence::create(actions_1), UINT_MAX);
            
            auto rep_hawk=Spawn::create(swingActionForever(2.5, 1, hawk, 1.5),
                                        swingActionForever(2.5, 1, parent->getChildByName("sup_19_hawk_0.png"), 1.5), NULL);
            parent->runAction(Spawn::create(rep_angry,rep_hawk, NULL));
        }
        else{
            auto rep_hawk=swingActionForever(2.5, 1, hawk, 1.5);
            parent->runAction(rep_hawk);
        }
    }
    else if(key=="lantern"&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(15)){//鼻ちょうちん
        Vector<FiniteTimeAction*>actions;
        
        std::vector<Vec2>vecs={Vec2(.43, .362),Vec2(.418, .331),Vec2(.41, .29),Vec2(.4, .236)};
        if (backID==32) {
            vecs={Vec2(.141, .586),Vec2(.381, .588),Vec2(.62, .588),Vec2(.86, .586)};
        }
        else if (backID==3) {
            vecs={Vec2(.371, .417),Vec2(.368, .41),Vec2(.362, .405),Vec2(.36, .398)};
        }
        
        for (int i=0; i<4; i++) {
            auto spr=AnchorSprite::create(vecs.at(i), parent->getContentSize(),false, StringUtils::format("sup_%d_lantern_%d.png",backID,i));
            parent->addChild(spr);
            auto original_scale=spr->getScale();
            spr->setScale(original_scale*.7);
            auto duration=3;
            auto seq=TargetedAction::create(spr, Sequence::create(CallFunc::create([i,backID]{
                if (i==0&&backID==32) {
                    Common::playSE("snoring.mp3");
                }
            }),
                                                                  ScaleTo::create(duration, original_scale),
                                                                  ScaleTo::create(duration, original_scale*.7),
                                                                  NULL));
            
            actions.pushBack(seq);
        }
        
        auto rep=Repeat::create(Spawn::create(actions), UINT_MAX);
        rep->setTag(1);
        
        parent->runAction(rep);
    }
    else if(key=="rabbit"&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(15)){//ベッドの上
        Vector<FiniteTimeAction*>actions;
        std::vector<int>counts={2,4,3,5};
        std::vector<Vec2>vecs={Vec2(.283, .42),Vec2(.43, .42),Vec2(.583, .42),Vec2(.725, .42)};
        for (int i=0; i<4; i++) {
            auto spr=AnchorSprite::create(vecs.at(i), parent->getContentSize(),false, StringUtils::format("sup_54_rabbit_%d.png",i));
            parent->addChild(spr);
            auto original_scale=spr->getScale();
            auto height=parent->getContentSize().height*.15;
            auto duration=.5;
            auto seq=TargetedAction::create(spr, Sequence::create(ScaleTo::create(.2, original_scale*1.05,original_scale*.95),
                                                                  ScaleTo::create(.2, original_scale),
                                                                  CallFunc::create([i]{
                if (i==3) {
                    Common::playSE("pop_1.mp3");
                }
            }),
                                                                  jumpAction(height, duration, spr, 1.75),
                                                                  
                                                                  NULL));
            
            actions.pushBack(Repeat::create(seq, counts.at(i)));
        }
        
        parent->runAction(Spawn::create(actions));
    }
    else if(key=="picture"&&
            DataManager::sharedManager()->isPlayingMinigame())
    {//ポスター間違い
        if (DataManager::sharedManager()->getOtherFlag("picture").asInt()==1) {
            auto mistake=parent->getChildByName(StringUtils::format("sup_%d_mistake.png",backID));
            mistake->removeFromParent();
        }
    }
    else if(key=="back_61"&&
            DataManager::sharedManager()->isPlayingMinigame()&&
            DataManager::sharedManager()->getTemporaryFlag("switch_sup_61_0").asInt()==1){
        auto mistake=parent->getChildByName(StringUtils::format("sup_61_mistake.png"));
        mistake->setOpacity(255);
    }
}

#pragma mark - Touch
void PigHat2ActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="treasure")
    {//宝箱正解
        auto back=createSpriteToCenter("back_16.png", true, parent,true);
        auto lid=createSpriteToCenter("sup_16_lid_0_0.png", false, back,true);
        auto open=createSpriteToCenter("sup_16_lid_0_1.png", true, back,true);
        auto sword=createSpriteToCenter("sup_16_sword.png", true, back,true);

        //fadein
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        
        auto change=Spawn::create(createChangeAction(1, .5, lid, open),
                                  TargetedAction::create(sword, FadeIn::create(.5)),
                                  createSoundAction("kapa.mp3"), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein,DelayTime::create(1),change,DelayTime::create(.7),call,NULL));
    }
#pragma mark - マトリョ謎
    else if(key=="back_41"&&
            DataManager::sharedManager()->isPlayingMinigame()){
        auto switchSpr=SupplementsManager::getInstance()->switchSprites.at(0);
        auto mystery=parent->getChildByName("sup_41_mistake.png");
        
        switchSpr->switching(nullptr);
        auto fadein=TargetedAction::create(mystery, FadeIn::create(.2));
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,call, NULL));
    }
    else if(key=="putbell"){
        auto map=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Anchor);
        auto sprMap=map["sprites"].asValueVector().at(3).asValueMap();
        auto bell=AnchorSprite::create(Vec2(sprMap["x"].asFloat(), sprMap["y"].asFloat()),parent->getContentSize(),true , "sup_25_bell_3.png");
        parent->addChild(bell);
        
        auto fadein_bell=TargetedAction::create(bell, Sequence::create(FadeIn::create(1),
                                                                       createSoundAction("kacha.mp3"), NULL));
        auto delay=DelayTime::create(1);
        //ベルを揺らすぜ
        Vector<FiniteTimeAction*> actions;
        for (int i=0; i<6; i++) {
            actions.pushBack(bellAction(parent, parent->getChildByName(StringUtils::format("sup_25_bell_%d.png",i)), i));
        }
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_bell,delay,Spawn::create(actions),delay->clone(),call,NULL));
    }
    else if(key.compare(0,4,"bell")==0){
        //bell
        callback(true);

        auto index=atoi(key.substr(5,1).c_str());
        auto bell=parent->getChildByName(StringUtils::format("sup_25_bell_%d.png",index));
        auto action=bellAction(parent,bell,index);
        
        if (bell->getRotation()==0) {
            parent->runAction(Sequence::create(action,NULL));
        }
    }
    else if(key=="pin"){
        Common::playSE("p.mp3");
        //鼻ちょうちんわる
        Vector<FiniteTimeAction*>actions;
        auto rabbits=parent->getChildByName("sup_32_rabbits.png");
        auto pin=createSpriteToCenter("sup_32_pin.png", true, parent,true);
        
        auto fadein_pin=TargetedAction::create(pin, FadeIn::create(1));
        actions.pushBack(fadein_pin);
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        
        auto distance=parent->getContentSize().width*.02;
        auto duration_shake=.1;
        
        for (int i=0; i<4; i++) {
            auto lantern=parent->getChildByName(StringUtils::format("sup_32_lantern_%d.png",i));
            if (i!=0) {
                //ちょうちんの位置まで移動
                auto move_pin=TargetedAction::create(pin, EaseInOut::create(MoveTo::create(1, Vec2(parent->getContentSize().width*(lantern->getAnchorPoint().x+.385), pin->getPositionY())), 1.5));
                actions.pushBack(move_pin);
                actions.pushBack(DelayTime::create(1));
            }
            
            //割るアクション
            auto shake_pin=swingAction(Vec2(-distance, -distance), duration_shake, pin, 1.5, false);
            if (i==3) {
                actions.pushBack(CallFunc::create([parent]{
                    Common::stopAllSE();
                    parent->stopActionByTag(1);//repeatで音を鳴らすアクションを止める
                }));
            }
            auto break_lantern=TargetedAction::create(lantern, Spawn::create(RemoveSelf::create(),
                                                                             createSoundAction("pan.mp3"), NULL));
            actions.pushBack(Spawn::create(shake_pin,break_lantern, NULL));
            
            actions.pushBack(DelayTime::create(.5));
        }
        
        //fadeout pin
        actions.pushBack(TargetedAction::create(pin, FadeOut::create(1)));
        
        //wakeup
        rabbits->setCascadeOpacityEnabled(true);
        auto knob=createSpriteToCenter("sup_32_knob.png", true, parent,true);
        
        auto wake=createSpriteToCenter("sup_32_wake.png", true, rabbits, true);
        actions.pushBack(Spawn::create(TargetedAction::create(wake, FadeIn::create(1)),
                                       createSoundAction("surprise.mp3"), NULL));
        actions.pushBack(delay->clone());
        
        //run away
        actions.pushBack(Spawn::create(TargetedAction::create(rabbits, FadeOut::create(1)),
                                       TargetedAction::create(knob, FadeIn::create(1)),
                                       createSoundAction("run.mp3"), NULL));
        actions.pushBack(delay->clone());
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="knob"){
        Common::playSE("p.mp3");
        //とってをつける
        auto knob=createSpriteToCenter("sup_11_knob.png", true, parent,true);
        
        auto seq_knob=TargetedAction::create(knob, Sequence::create(FadeIn::create(1),
                                                                    createSoundAction("kacha.mp3"), NULL));
        auto delay=DelayTime::create(1);
        
        auto back=createSpriteToCenter("back_5.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 5);
        createSpriteToCenter("sup_5_knob.png", false, back,true);
        auto open=createSpriteToCenter("sup_5_0_1.png", true, back,true);
        createSpriteToCenter("sup_5_book.png", false, open,true);
        
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto fadein_open=TargetedAction::create(open, Spawn::create(FadeIn::create(1),
                                                                    createSoundAction("gacha.mp3"), NULL));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_knob,delay,fadein_back,delay->clone(),fadein_open,delay->clone(),call,NULL));
    }
    else if(key.compare(0,6,"handle")==0){
        //棚上部入力
        auto index=atoi(key.substr(7,1).c_str());

        Node* picture_0;
        Node* picture_1;
        Node* anim_0;
        Node* anim_1;
        Vector<FiniteTimeAction*>turnAction;
        if (index==0||index==2) {
            if (index==0) {
                Common::playSE("p.mp3");
            }
            picture_0=parent->getChildByName<Sprite*>("sup_55_picture_0.png");
            picture_0->setCascadeOpacityEnabled(true);
            picture_1=createSpriteToCenter("sup_55_picture_1.png", true, parent,true);
            anim_1=createSpriteToCenter("anim_55_picture_1.png", true, parent,true);

            if (DataManager::sharedManager()->isPlayingMinigame()) {
                auto mistake=parent->getChildByName("sup_55_mistake.png");
                if (mistake) {
                    mistake->retain();
                    mistake->removeFromParent();
                    picture_0->addChild(mistake);
                }
                anim_0=createSpriteToCenter("anim_55_mistake_1.png", true, parent,true);
            }
            else{
                anim_0=createSpriteToCenter("anim_55_picture_0.png", true, parent,true);
            }
            
            turnAction.pushBack(createChangeAction(.7, .5, anim_1, picture_1));
        }
        else{
            picture_0=parent->getChildByName<Sprite*>("sup_55_picture_1.png");
            picture_1=createSpriteToCenter("sup_55_picture_0.png", true, parent, true);
            anim_0=createSpriteToCenter("anim_55_picture_2.png", true, parent,true);
            
            if (DataManager::sharedManager()->isPlayingMinigame()) {
                auto mistake=createSpriteToCenter("sup_55_mistake.png", false, picture_1, true);
                anim_1=createSpriteToCenter("anim_55_mistake_0.png", true, parent,true);
                
                turnAction.pushBack(Spawn::create(createChangeAction(.7, .5, anim_1, picture_1),
                                                  TargetedAction::create(mistake, FadeIn::create(.5)), NULL));
            }
            else{
                anim_1=createSpriteToCenter("anim_55_picture_3.png", true, parent,true);
                turnAction.pushBack(createChangeAction(.7, .5, anim_1, picture_1));
            }
        }
        

        Node* handle;
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(24)) {
            handle=parent->getChildByName("sup_55_handle.png");
        }
        else{
            handle=AnchorSprite::create(Vec2(.5, .142), parent->getContentSize(),true, "sup_55_handle.png");
            parent->addChild(handle);
        }
        handle->setLocalZOrder(1);
        
        Vector<FiniteTimeAction*>actions;
        auto fadein_handle=TargetedAction::create(handle, Sequence::create(FadeIn::create(1),
                                                                           createSoundAction("kacha.mp3"), NULL));
        auto delay=DelayTime::create(1);

        if (index==0) {
            actions.pushBack(fadein_handle);
            actions.pushBack(delay);
        }
        auto rotate_handle=TargetedAction::create(handle, Spawn::create(EaseInOut::create(RotateBy::create(6, 360*3), 1.5),
                                                                        createSoundAction("twist.mp3"), NULL));
        auto seq_picture=Sequence::create(delay->clone(),
                                          createSoundAction("gii.mp3"),
                                          createChangeAction(.7, .5, picture_0, anim_0),
                                          delay->clone(),
                                          createChangeAction(.7, .5, anim_0, anim_1),
                                          delay->clone(),
                                          Spawn::create(turnAction),
                                          createSoundAction("door.mp3"), NULL);
        actions.pushBack(Spawn::create(rotate_handle,seq_picture, NULL));
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="shelf2f")
    {//棚正解後
        auto back=createSpriteToCenter("back_61.png", true, parent,true);
        createSpriteToCenter("sup_61_0_0.png", false, back,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 61);
        
        auto open=createSpriteToCenter("sup_61_0_1.png", true, parent,true);
        
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto fadein_open=TargetedAction::create(open, Spawn::create(FadeIn::create(1),
                                                                    createSoundAction("gacha.mp3"), NULL));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,fadein_open,delay,call, NULL));
    }
    else if(key.compare(0,4,"dial")==0)
    {
        auto index=atoi(key.substr(5,1).c_str());
        Common::playSE("kacha_2.mp3");
        auto dial=createSpriteToCenter(StringUtils::format("sup_28_dial_%d.png",index), false, parent,true);
        
        auto seq=TargetedAction::create(dial, Sequence::create(DelayTime::create(.1),RemoveSelf::create(), NULL));
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(seq,call, NULL));
    }
    else if(key=="handcuffs")
    {
        auto back_19=createSpriteToCenter("back_19.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back_19, 19,true);
        //SupplementsManager::getInstance()->addAnchorSpritesToMain(back_19, 19);
        
        auto fadein_back=TargetedAction::create(back_19, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        
        auto fadeout_cuffs=Spawn::create(TargetedAction::create(back_19->getChildByName("sup_19_handcuffs_0.png"), FadeOut::create(.5)),
                                         TargetedAction::create(back_19->getChildByName("sup_19_handcuffs_1.png"), FadeOut::create(.5)),
                                         NULL);
        
        auto hawk_leg=back_19->getChildByName("sup_19_hawk_0.png");
        auto hawk_body=back_19->getChildByName<Sprite*>("sup_19_hawk_1.png");

        auto fall=EaseIn::create(MoveBy::create(.5, Vec2(0, -parent->getContentSize().height*.15)), 2);
        auto fall_hawk=Spawn::create(createSoundAction("fall.mp3"),
                                     TargetedAction::create(hawk_leg, fall),
                                     TargetedAction::create(hawk_body, fall->clone()),
                                     NULL);
        auto seq_fall=Spawn::create(fadeout_cuffs,fall_hawk, NULL);

        auto bounce_hawk=Sequence::create(CallFunc::create([this]{
            Common::stopAllSE();
            Common::playSE("hit.mp3");
        }),
                                          Spawn::create(jumpAction(parent->getContentSize().height*.05, .1, hawk_leg, 1.5),
                                                        jumpAction(parent->getContentSize().height*.05, .1, hawk_body, 1.5),
                                                        NULL), NULL);
        
        auto hawk_wake=createSpriteToCenter("sup_19_hawk_wake.png", true, back_19,true);
        auto delay_short=DelayTime::create(.5);
        auto wake_hawk=Spawn::create(TargetedAction::create(hawk_leg, FadeOut::create(1)),
                                     createChangeAction(1, .7, hawk_body, hawk_wake),
                                     NULL);
        
        
        auto call=CallFunc::create([callback,hawk_wake]{
            auto story=StoryLayer::create("hawk_wake", [callback,hawk_wake]{
                auto fadeout_hawk=TargetedAction::create(hawk_wake, FadeOut::create(1));
                auto call=CallFunc::create([callback]{
                    Common::playSE("pinpon.mp3");
                    callback(true);
                });
                hawk_wake->runAction(Sequence::create(fadeout_hawk,call, NULL));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        parent->runAction(Sequence::create(fadein_back,delay,seq_fall,bounce_hawk,delay_short,wake_hawk,delay->clone(),call, NULL));
    }
    else if(key=="thumb_appear"){
        
        auto back_67=createSpriteToCenter("back_67.png", true, parent,true);
        
        auto door=createSpriteToCenter("sup_67_0_1.png", true, back_67,true);
        createSpriteToCenter("sup_67_longstick.png", false, door,true);
        auto pig=AnchorSprite::create(Vec2(.5, .325), parent->getContentSize(),false, "sup_67_pig.png");
        door->addChild(pig);
        auto attention=createSpriteToCenter("sup_67_attention.png", true, door,true);
        auto pig_run=createSpriteToCenter("sup_67_pig_run.png", true, door,true);
        pig_run->setPositionY(parent->getContentSize().height*.45);
        
        auto fadein_back_67=TargetedAction::create(back_67, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto fadein_door=TargetedAction::create(door, Spawn::create(createSoundAction("gacha.mp3"),
                                                                    FadeIn::create(.3), NULL));
        auto attention_action=Spawn::create(createBounceAction(pig, .2),
                                            createSoundAction("surprise.mp3"),
                                            TargetedAction::create(attention, Spawn::create(FadeIn::create(.1),
                                                                                            jumpAction(parent->getContentSize().height*.1, .2, attention, 1.5), NULL)),
                                            NULL);
        auto run_action=Spawn::create(createSoundAction("pig.mp3"),
                                      createChangeAction(1, .5, pig, pig_run),
                                      TargetedAction::create(attention, FadeOut::create(1)), NULL);
        auto fadeout_run_pig=TargetedAction::create(pig_run, Spawn::create(FadeOut::create(1),
                                                                           ScaleBy::create(1, 1.2),
                                                                           createSoundAction("run.mp3"),
                                                                           JumpBy::create(1, Vec2(parent->getContentSize().width*.3, -parent->getContentSize().height*.3), parent->getContentSize().height*.05, 3), NULL));
        auto seq_67=Sequence::create(fadein_back_67,delay,fadein_door,attention_action,delay->clone(),run_action,DelayTime::create(.3),fadeout_run_pig, NULL);
        
        auto story_meliodas=CallFunc::create([parent,callback,this,back_67]{
            auto story=StoryLayer::create("thumb_escape_0", [parent,callback,this,back_67]{
                //3fへ                
                auto back_1=createSpriteToCenter("back_1.png", true, parent,true);
                SupplementsManager::getInstance()->addSupplementToMain(back_1, 1);
                auto open=createSpriteToCenter("sup_1_dooropen.png", true, back_1,true);
                
                auto delay=DelayTime::create(1);
                auto fadein_back_1=TargetedAction::create(back_1, FadeIn::create(1));
                auto fadein_open=TargetedAction::create(open, Spawn::create(FadeIn::create(.5),
                                                                            createSoundAction("gacha.mp3"),
                                                                            NULL));

                auto seq_1=Sequence::create(fadein_back_1,delay,fadein_open, NULL);
                
                //door up
                auto door=createSpriteToCenter("anim_door.png", true, back_1,true);
                auto pig_appear=createSpriteToCenter("anim_door_pig_0.png", true, door,true);
                pig_appear->setPositionX(parent->getContentSize().width*.4);
                createSpriteToCenter("anim_door_cover.png", false, door,true);//cover
                auto pig_run=createSpriteToCenter("anim_door_pig_1.png", true, door,true);

                auto fadein_door=TargetedAction::create(door, FadeIn::create(1));
                auto fadein_pig_appear=TargetedAction::create(pig_appear, FadeIn::create(1));
                auto move_pig_appear=TargetedAction::create(pig_appear, Spawn::create(createSoundAction("pig.mp3"),
                                                                                      MoveTo::create(.7, parent->getContentSize()/2), NULL));
                auto run_pig=Sequence::create(this->createChangeAction(1, .7, pig_appear, pig_run),
                                              DelayTime::create(.3),
                                              TargetedAction::create(pig_run, Spawn::create(JumpBy::create(1, Vec2(-parent->getContentSize().width*.2, -parent->getContentSize().height*.3), parent->getContentSize().height*.05, 3),
                                                                                            FadeOut::create(1),
                                                                                            this->createSoundAction("run.mp3"),
                                                                                            NULL)),
                                              NULL);
                auto seq_door=Sequence::create(fadein_door,fadein_pig_appear,move_pig_appear,DelayTime::create(1.5),run_pig, NULL);
                //hanmoc_サムcutin
                auto back_3=this->createSpriteToCenter("back_3.png", true, parent,true);
                SupplementsManager::getInstance()->addSupplementToMain(back_3, 3);
                
                auto pig_run_1=this->createSpriteToCenter("sup_3_pig_run.png", false, back_3,true);
                pig_run_1->setPosition(Vec2(parent->getContentSize().width*.75, parent->getContentSize().height*.25));
                auto pig_shake=this->createSpriteToCenter("sup_3_pig_shake.png", true, back_3,true);
                
                auto fadein_back_3=TargetedAction::create(back_3, FadeIn::create(1));
                auto remove_door=TargetedAction::create(door, RemoveSelf::create());
                auto remove_back_1=TargetedAction::create(back_1, RemoveSelf::create());

                auto run_pig_1=TargetedAction::create(pig_run_1, Spawn::create(JumpTo::create(1, parent->getContentSize()/2, parent->getContentSize().height*.05, 3),
                                                                               this->createSoundAction("run.mp3"), NULL));
                
                auto seq_3_0=Sequence::create(fadein_back_3,remove_back_1,remove_door,delay->clone(),run_pig_1, NULL);
                auto change_pig=this->createChangeAction(.1, .1, pig_run_1, pig_shake);

                //door側
                auto back_2=this->createSpriteToCenter("back_2.png", true, parent,true);
                SupplementsManager::getInstance()->addSupplementToMain(back_2, 2,true);
                //SupplementsManager::getInstance()->addAnchorSpritesToMain(back_2, 2);
                
                auto hawk_search=back_2->getChildByName<Sprite*>("sup_2_hawk_search.png");
                auto hawk_found=this->createSpriteToCenter("sup_2_hawk_found.png", true, back_2,true);
                auto attention=this->createSpriteToCenter("sup_2_attention.png", true, back_2,true);
                
                auto fadein_back_2=TargetedAction::create(back_2, FadeIn::create(1));
                auto found_action=createChangeAction(.7, .5, hawk_search, hawk_found);
                auto attention_action=TargetedAction::create(attention, Spawn::create(this->jumpAction(parent->getContentSize().height*.05, .3, attention, 1.5),
                                                                                      FadeIn::create(.2),
                                                                                      this->createSoundAction("surprise.mp3"), NULL));
                auto fadeout_attention=TargetedAction::create(attention, FadeOut::create(.5));
                
                //見つけたー！
                auto found_story=CallFunc::create([parent,back_2,back_3,pig_shake,hawk_found,callback,this]{
                    auto story=StoryLayer::create("hawk_attack_0", [parent,back_2,back_3,pig_shake,hawk_found,callback,this]{
                        //とっしん
                        auto run_pig=TargetedAction::create(hawk_found, Spawn::create(JumpBy::create(1, Vec2(parent->getContentSize().width*.25, -parent->getContentSize().height*.25), parent->getContentSize().height*.05, 3),
                                                                                      this->createSoundAction("horce_run.mp3"), NULL));
                        
                        auto story=CallFunc::create([parent,back_2,back_3,pig_shake,hawk_found,this,callback]{
                            auto story=StoryLayer::create("hawk_attack_1", [parent,back_2,back_3,pig_shake,this,callback]{
                                auto shake_pig=swingActionForever(Vec2(parent->getContentSize().width*.01, 0), .1, pig_shake, 1.2);
                                auto fadeout_back_2=TargetedAction::create(back_2, Sequence::create(FadeOut::create(1),
                                                                                                    RemoveSelf::create(),
                                                                                                    NULL));
                                
                                auto jumpPoint=Vec2(parent->getContentSize().width*.4, parent->getContentSize().width*.2);//ホークのjump開始
                                auto attackPoint=Vec2(parent->getContentSize().width*.635, parent->getContentSize().height*.25);//ホークのattack着地
                                auto hawk_run=createSpriteToCenter("sup_3_hawk_run.png", false, back_3,true);
                                hawk_run->setPosition(-parent->getContentSize().width*.2,-parent->getContentSize().height*.2);
                                auto hawk_down=createSpriteToCenter("sup_3_hawk_down.png", true, back_3,true);
                                auto pig_jump=createSpriteToCenter("sup_3_pig_jump.png", true, back_3,true);
                                auto pig_land=createSpriteToCenter("sup_3_pig_land.png", true, back_3,true);
                                auto pig_flee=createSpriteToCenter("sup_3_pig_flee.png", true, back_3,true);
                                auto hawk_wake=createSpriteToCenter("sup_3_hawk_wake.png", true, back_3,true);
                                
                                auto run_hawk=TargetedAction::create(hawk_run, Sequence::create(this->createSoundAction("horce_run.mp3"),
                                                                                                JumpTo::create(1.5, jumpPoint, parent->getContentSize().height*.025, 6),
                                                                                                 NULL));
                                auto attack_hawk=Sequence::create(TargetedAction::create(hawk_run, Spawn::create(RotateBy::create(1, 360*3+30),
                                                                                                                 this->createSoundAction("rolling.mp3"),
                                                                                                                 JumpTo::create(1, attackPoint, parent->getContentSize().height*.3, 1),
                                                                                                                 ScaleBy::create(1, .8),
                                                                                                                 NULL)),
                                                                  this->createSoundAction("hit.mp3"),
                                                                  TargetedAction::create(parent, Spawn::create(this->swingAction(2, .1, parent, 1.2),
                                                                                                               Sequence::create(ScaleBy::create(.1, 1.1),
                                                                                                                                ScaleBy::create(.1, 1.0/1.1), NULL), NULL)),
                                                                  this->createChangeAction(.4, .2, hawk_run, hawk_down),
                                                                  NULL);
                                auto jump_thumb=Sequence::create(DelayTime::create(.8),//1sでhawkは衝突
                                                                 Spawn::create(this->createChangeAction(.3, .2, pig_shake, pig_jump),
                                                                               this->createSoundAction("jump.mp3"),
                                                                               TargetedAction::create(pig_jump, JumpBy::create(1, Vec2(parent->getContentSize().width*.11,-parent->getContentSize().height*.17), parent->getContentSize().height*.2, 1)),
                                                                               NULL),
                                                                 this->createSoundAction("landing.mp3"),
                                                                 this->createChangeAction(.4, .2, pig_jump, pig_land),
                                                                 NULL);
                                
                                auto spawn_attack=Spawn::create(attack_hawk,jump_thumb, NULL);
                                auto delay=DelayTime::create(2);
                                auto flee_pig=TargetedAction::create(pig_flee, Spawn::create(this->createChangeAction(.7, .5, pig_land, pig_flee),
                                                                                             this->createSoundAction("run.mp3"),
                                                                                             JumpBy::create(3, Vec2(-parent->getContentSize().width,-parent->getContentSize().height*.1), parent->getContentSize().height*.05, 10), NULL));
                                
                                auto shake_down=swingAction(Vec2(parent->getContentSize().width*.01, 0), .1, hawk_down, 1.5, true);
                                auto wake_up=createChangeAction(.7, .5, hawk_down, hawk_wake);
                                
                                auto story=CallFunc::create([this,back_3,callback,parent]{
                                    auto story=StoryLayer::create("hawk_attack_after", [back_3,callback,this,parent]{
                                        auto fadeout_back_3=TargetedAction::create(back_3, FadeOut::create(1));
                                        auto remove_back_3=TargetedAction::create(back_3, RemoveSelf::create());
                                        auto call=CallFunc::create([callback]{
                                            
                                            Common::playSE("pinpon.mp3");
                                        
                                            //ドアが開いている状態にする。
                                            DataManager::sharedManager()->setTemporaryFlag("switch_sup_67_0", 1);
                                            callback(true);
                                        });
                                        
                                        parent->runAction(Sequence::create(fadeout_back_3, remove_back_3, call, NULL));
                                    });
                                    Director::getInstance()->getRunningScene()->addChild(story);
                                });
                                
                                auto seq_3=Sequence::create(fadeout_back_2,run_hawk,spawn_attack,delay,flee_pig,delay->clone(),shake_down,wake_up,DelayTime::create(.5),story, NULL);
                                
                                pig_shake->runAction(shake_pig);
                                parent->runAction(seq_3);
                            });
                            Director::getInstance()->getRunningScene()->addChild(story);
                        });
                        
                        //ストーリーと突進を同時に
                        auto spawn=Spawn::create(run_pig,story, NULL);
                        parent->runAction(spawn);
                    });
                    Director::getInstance()->getRunningScene()->addChild(story);
                });
                
                auto seq_2 = Sequence::create(fadein_back_2,change_pig,delay->clone(),found_action,attention_action,found_action,fadeout_attention,delay->clone(),found_story, NULL);
                
                parent->runAction(Sequence::create(seq_1,delay->clone(),seq_door,delay->clone(),seq_3_0,delay->clone(),seq_2, NULL));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
       
        parent->runAction(Sequence::create(seq_67,story_meliodas, NULL));
    }
    else if(key=="longstick"){
        Common::playSE("p.mp3");
        
        auto stick=createSpriteToCenter("sup_68_longstick.png", true, parent,true);
        stick->setPosition(Vec2(parent->getContentSize().width*.7, parent->getContentSize().height*.2));
        auto lamp=parent->getChildByName("sup_68_lamp.png");
        
        auto fadein_stick=TargetedAction::create(stick, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto move=TargetedAction::create(stick, MoveTo::create(1, parent->getContentSize()/2));
        auto delays=Spawn::create(createSoundAction("kacha.mp3"),
                                  DelayTime::create(1), NULL);
        auto moveby=Spawn::create(MoveBy::create(2, Vec2(0, -parent->getContentSize().height*.3)),
                                  FadeOut::create(2), NULL);
        auto getItem=Spawn::create(TargetedAction::create(stick, moveby),
                                   TargetedAction::create(lamp, moveby->clone()), NULL);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_stick,delay,move,delays,getItem,delay->clone(),call, NULL));
    }
    else if(key=="tea"){
        Common::playSE("p.mp3");
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<4; i++) {
            auto after=createSpriteToCenter(StringUtils::format("sup_52_%d_1.png",i), true, parent,true);
            actions.pushBack(TargetedAction::create(after, FadeIn::create(.1)));
        }
        auto anim=createSpriteToCenter("anim_52.png", true, parent,true);
        anim->setLocalZOrder(1);
        auto pot=AnchorSprite::create(Vec2(.56, .72), parent->getContentSize(),true,"anim_52_pot.png");
        pot->setLocalZOrder(1);
        anim->addChild(pot);
        auto fadein_back=TargetedAction::create(anim, FadeIn::create(1));
        auto fadein_afters=Spawn::create(actions);
        auto add_Particle=CallFunc::create([parent]{
            auto vec=DataManager::sharedManager()->getParticleVector(false,DataManager::sharedManager()->getNowBack());
            for (auto v : vec) {
                auto particleMap=v.asValueMap();
                auto particle=SupplementsManager::getInstance()->getParticle(parent,particleMap);
                parent->addChild(particle);
            }
        });
        auto fadein_pot=TargetedAction::create(pot, Spawn::create(createSoundAction("pop.mp3"),
                                                                  FadeIn::create(.7), NULL));
        
        auto delay=DelayTime::create(1);
        
        //moves
        Vector<FiniteTimeAction*>actions_pot;
        std::vector<Vec2>vecs={Vec2(0,-.06),Vec2(-.14,0-.02),Vec2(.33,-.08),Vec2(-.2,-.05)};
        for (int i=0; i<4; i++) {
            auto tea=createSpriteToCenter(StringUtils::format("anim_52_%d.png",i), true, anim,true);
            
            auto move_pot=TargetedAction::create(pot, MoveBy::create(1, Vec2(parent->getContentSize().width*vecs.at(i).x, parent->getContentSize().height*vecs.at(i).y)));
            auto rotate_pot=swingAction(-15, .75, pot, 1.5);
            auto fadein_tea=TargetedAction::create(tea, EaseIn::create(FadeIn::create(1), 1.5));
            
            actions_pot.pushBack(Sequence::create(move_pot,
                                                  DelayTime::create(.2),
                                                  Spawn::create(rotate_pot,
                                                                         fadein_tea,
                                                                         createSoundAction("jobojobo.mp3"),
                                                                         NULL),
                                                  DelayTime::create(.5),
                                                  NULL));
        }
        
        auto fadeout_anim=TargetedAction::create(anim, FadeOut::create(1));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_back,fadein_afters,add_Particle,fadein_pot,delay,Sequence::create(actions_pot),delay->clone(),fadeout_anim,call, NULL));
    }
    else if(key=="stair"){
        auto mainRect = DataManager::sharedManager()->getMainSpriteRect();
        auto black=Sprite::create();
        black->setColor(Color3B::BLACK);
        black->setOpacity(0);
        black->setPosition(mainRect.size.width/2, mainRect.origin.y+mainRect.size.height/2);
        black->setTextureRect(Rect(0, 0, mainRect.size.width, mainRect.size.height));
        black->setLocalZOrder(parent->getLocalZOrder()+1);//mainspriteは何回貼られてもzoder変更されないから現在表示中のzorderに+1してOK
        parent->getParent()->addChild(black);
        
        auto gachaAction = createSoundAction("gacha.mp3");
        auto fadein = TargetedAction::create(black, EaseOut::create(FadeIn::create(.3), 1.5));
        auto soundAction = createSoundAction("stair.mp3");
        auto delay = DelayTime::create(.1);
        auto call = CallFunc::create([callback]{
            callback(true);
        });
        auto delay_ = DelayTime::create(.2);
        auto fadeout = TargetedAction::create(black, EaseOut::create(FadeOut::create(.4), 1.5));
        auto removeBlack = TargetedAction::create(black, RemoveSelf::create());

        //コールバック呼んだら、mainspriteは解放されちゃうから、GameSceneでアニメーションをかける様にする。
        parent->getParent()->runAction(Sequence::create(gachaAction, fadein, soundAction, delay, call, delay_, fadeout, removeBlack, NULL));
    }
    else if(key=="clear"){
        auto barrel=parent->getChildByName("sup_69_barrel.png");
        auto thumb_barrel=createSpriteToCenter("sup_69_thumb_0.png", true, barrel,true);
        thumb_barrel->setPositionY(parent->getContentSize().height*.4);
        createSpriteToCenter("sup_69_cover.png", false, barrel,true);
        auto attention_barrel=createSpriteToCenter("sup_69_attention.png", true, barrel,true);

        auto thumb_run=createSpriteToCenter("sup_69_thumb_1.png", true, parent,true);
        thumb_run->setPositionY(parent->getContentSize().height*.45);
        
        auto shake_barrel=TargetedAction::create(barrel, Spawn::create(Repeat::create(swingAction(Vec2(-parent->getContentSize().width*.01, 0), .1, barrel, 1.5, true), 3),
                                                                       createSoundAction("gatagata.mp3")
                                                                       , NULL));
        auto delay=DelayTime::create(1);
        auto move_thumb_barrel=TargetedAction::create(thumb_barrel, Sequence::create(FadeIn::create(.1),
                                                                                     createSoundAction("nyu.mp3"),
                                                                                     MoveTo::create(.5, parent->getContentSize()/2),
                                                                                     NULL));
        auto move_attention=TargetedAction::create(attention_barrel, Spawn::create(FadeIn::create(.2),
                                                                                   createSoundAction("surprise.mp3"),
                                                                                   jumpAction(parent->getContentSize().height*.05, .2, attention_barrel, 1.5),
                                                                                   NULL));
        
        auto run_thumb=Sequence::create(Spawn::create(createChangeAction(1, .7, thumb_barrel, thumb_run,"pig.mp3"),
                                                      TargetedAction::create(attention_barrel, FadeOut::create(1)),
                                                      NULL),
                                        TargetedAction::create(thumb_run, Spawn::create(JumpBy::create(1, Vec2(parent->getContentSize().width*.2, -parent->getContentSize().height*.25), parent->getContentSize().height*.05, 3),
                                                                                        EaseIn::create(FadeOut::create(1), 1.5),
                                                                                        createSoundAction("run.mp3"),
                                                                                        NULL)),
                                        NULL);
        
        auto seq_barrel=Sequence::create(shake_barrel,delay,move_thumb_barrel,move_attention,delay->clone(),run_thumb, NULL);
        
        auto addStory_barrel=CallFunc::create([parent,this]{
            //Common::playBGM(DataManager::sharedManager()->getBgmName(true).c_str());

            auto story=StoryLayer::create("thumb_escape_1", [parent,this]{
                //back2
                auto back_2=createSpriteToCenter("back_2.png", true, parent,true);
                SupplementsManager::getInstance()->addSupplementToMain(back_2, 2);
                createSpriteToCenter("sup_2_hawk_final.png", false, back_2,true);
                auto door=createSpriteToCenter("sup_2_dooropen.png", true, back_2,true);
                auto thumb_back_2=createSpriteToCenter("sup_2_thumb_final.png", false, door,true);
                auto attention_0=createSpriteToCenter("sup_2_attention_final_0.png", true, back_2,true);
                createSpriteToCenter("sup_2_attention_final_1.png", false, attention_0,true);
                
                auto anim_final=createSpriteToCenter("anim_final_hawk.png", true, parent,true);

                auto fadein_back_2=TargetedAction::create(back_2, FadeIn::create(1));
                auto delay=DelayTime::create(1);
                auto open_door=TargetedAction::create(door, Spawn::create(FadeIn::create(.5),
                                                                          this->createSoundAction("gacha.mp3"),
                                                                          NULL));
                auto attention_action=TargetedAction::create(attention_0, Sequence::create(Spawn::create(this->jumpAction(parent->getContentSize().height*.05, .2, attention_0, 1.5),
                                                                                                         FadeIn::create(.2),
                                                                                                         this->createSoundAction("surprise.mp3"),
                                                                                                         NULL),
                                                                                           DelayTime::create(.3),
                                                                                           FadeOut::create(.3),
                                                                                           NULL));
                auto run_thumb=TargetedAction::create(thumb_back_2, Spawn::create(JumpBy::create(1.2, Vec2(parent->getContentSize().width*.3, -parent->getContentSize().height*.1), parent->getContentSize().height*.025, 4),
                                                                                  this->createSoundAction("run.mp3"),
                                                                                  NULL));
                
                //
                auto fadein_final=TargetedAction::create(anim_final, FadeIn::create(1));
                auto remove_back_2=TargetedAction::create(back_2, RemoveSelf::create());
                
                auto addStory_Hawk=CallFunc::create([parent,this]{
                    auto story=StoryLayer::create("ending", [parent,this]{
                        //走って行くシーン
                        auto back_final=createSpriteToCenter("anim_final.png", true, parent,true);
                        
                        auto thumb=AnchorSprite::create(Vec2(.173, .3), parent->getContentSize(), "anim_final_thumb_0.png");
                        back_final->addChild(thumb);
                        
                        auto hawk=AnchorSprite::create(Vec2(.426, .14), parent->getContentSize(), "anim_final_hawk_0.png");
                        back_final->addChild(hawk);
                        
                        auto distance_y=parent->getContentSize().height*.3;//最初の位置からの変位
                        hawk->setPositionY(hawk->getPositionY()-distance_y);
                        
                        auto smoke=ParticleSystemQuad::create("smoke.plist");
                        smoke->setAutoRemoveOnFinish(true);
                        smoke->setPositionType(ParticleSystem::PositionType::RELATIVE);
                        smoke->setStartSize(parent->getContentSize().width*.1);
                        smoke->setTotalParticles(30);
                        smoke->setEndSize(parent->getContentSize().width*.1);
                        smoke->setDuration(-1);
                        smoke->setAngle(270);
                        smoke->setAngleVar(10);
                        smoke->setSpeed(parent->getContentSize().height*.2);
                        smoke->setSpeedVar(parent->getContentSize().height*.05);
                        smoke->setPosition(parent->getContentSize().width*hawk->getAnchorPoint().x,parent->getContentSize().height*0.05);
                        smoke->setLife(2.5);
                        smoke->setPosVar(Vec2(parent->getContentSize().width*.05, 0));
                        smoke->setGravity(Vec2(0, parent->getContentSize().height*.3));
                        hawk->addChild(smoke);
                        
                        
                        auto jumpByPoint_thumb=Vec2(parent->getContentSize().width*.258,parent->getContentSize().height*.06);
                        auto jumpByPoint_hawk=Vec2(0, distance_y+parent->getContentSize().height*.167);
                        
                        auto fadein_back=TargetedAction::create(back_final, FadeIn::create(1));
                        auto delay=DelayTime::create(1);
                        auto scale_back=TargetedAction::create(back_final, EaseIn::create(ScaleBy::create(2, 1.5), 2));
                        auto run_thumb=TargetedAction::create(thumb, Sequence::create(JumpBy::create(2, jumpByPoint_thumb, parent->getContentSize().height*.02, 6),
                                                                                      this->createSoundAction("jump.mp3"),
                                                                                      Spawn::create(EaseOut::create(MoveBy::create(.5, Vec2(0, parent->getContentSize().height*.165)), 1.3),
                                                                                                    this->swingAction(5, .125, thumb, 1.2, true, 1),
                                                                                                    NULL),
                                                                                      NULL));
                        
                        auto remove_smoke=CallFunc::create([smoke]{
                            smoke->setDuration(0);
                        });
                        
                        auto run_hawk=TargetedAction::create(hawk, Sequence::create(Spawn::create(JumpBy::create(2.5, jumpByPoint_hawk, parent->getContentSize().height*.075, 7),
                                                                                                  ScaleBy::create(2.5, .6),
                                                                                                  this->createSoundAction("horce_run.mp3"),
                                                                                                  NULL),
                                                                                    remove_smoke, NULL));
                        auto spawn_run=Spawn::create(scale_back,
                                                     run_thumb,
                                                     run_hawk,
                                                     NULL);
                        
                        auto delay_1=DelayTime::create(.5);
                        //タックルかますぜ
                        auto add_final_story=CallFunc::create([this,parent,hawk,thumb,back_final]{
                            auto story=StoryLayer::create("ending_1", [this,parent,hawk,thumb,back_final]{
                                auto window=createSpriteToCenter("anim_final_window.png", true, back_final,true);
                                auto thumb_after=AnchorSprite::create(Vec2(.426, .53), parent->getContentSize(),true, "anim_final_thumb_1.png");
                                back_final->addChild(thumb_after);
                                auto hawk_after=AnchorSprite::create(Vec2(.43, .5), parent->getContentSize(),true, "anim_final_hawk_1.png");
                                back_final->addChild(hawk_after);
                                auto hawk_land=AnchorSprite::create(Vec2(.43, .5), parent->getContentSize(),true, "anim_final_hawk_2.png");
                                back_final->addChild(hawk_land);
                                
                                auto jump_hawk=TargetedAction::create(hawk, Spawn::create(JumpBy::create(.5, Vec2(0, parent->getContentSize().height*.15), parent->getContentSize().height*.175, 1),
                                                                                            ScaleBy::create(.5, .7),
                                                                                            this->createSoundAction("jump.mp3"),
                                                                                            NULL));
                                auto fall_thumb=TargetedAction::create(thumb, Spawn::create(this->swingAction(3, .1, thumb, 1.2),
                                                                                            MoveBy::create(.5, Vec2(0, -parent->getContentSize().height*.02)), NULL));
                                auto spawn_before_attack=Spawn::create(jump_hawk,fall_thumb, NULL);
                                
                                auto attack=Spawn::create(this->createSoundAction("hit.mp3"),
                                                          this->createChangeAction(.2, .1, hawk, hawk_after),
                                                          this->createChangeAction(.2, .1, thumb, thumb_after),
                                                          this->createBounceAction(hawk_after, .2),
                                                          NULL);
                                
                                auto glass_action=Sequence::create(DelayTime::create(1),
                                                                   this->createSoundAction("glass-break1.mp3"),
                                                                   TargetedAction::create(window, FadeIn::create(.1)),
                                                                   NULL);
                                
                                auto add_shine=CallFunc::create([back_final,thumb_after]{
                                    //キラーん
                                    Common::playSE("shine_1.mp3");
                                    
                                    auto shine=ParticleSystemQuad::create("kirakira.plist");
                                    shine->setAutoRemoveOnFinish(true);
                                    shine->setPositionType(ParticleSystem::PositionType::RELATIVE);
                                    shine->setStartSize(back_final->getContentSize().width*.05);
                                    shine->setEndSize(shine->getStartSize());
                                    shine->setTotalParticles(20);
                                    shine->setEndSize(back_final->getContentSize().width*.1);
                                    shine->setDuration(1);
                                    shine->setAngleVar(0);
                                    shine->setEndSpin(0);
                                    shine->setPosition(back_final->getContentSize().width*thumb_after->getAnchorPoint().x,back_final->getContentSize().height*thumb_after->getAnchorPoint().y);
                                    shine->setPosVar(Vec2::ZERO);
                                    shine->setLife(1);
                                    back_final->addChild(shine);
                                });
                                
                                auto land_hawk=Sequence::create(DelayTime::create(.8),
                                                             this->createChangeAction(.2, .2, hawk_after, hawk_land),
                                                             this->createSoundAction("landing.mp3"), NULL);
                                
                                auto spawn_after=Spawn::create(TargetedAction::create(thumb_after, Spawn::create(RotateBy::create(3, 360*3),
                                                                                                                 Sequence::create(DelayTime::create(.5),
                                                                                                                                  this->createSoundAction("pig.mp3"), NULL),
                                                                                                                 ScaleBy::create(3, .1),
                                                                                                                         NULL)),
                                                                      TargetedAction::create(hawk_after, Spawn::create(JumpBy::create(1, Vec2(0, -parent->getContentSize().height*.15), parent->getContentSize().height*.1, 1),
                                                                                                                       RotateBy::create(1, 360*2+180),
                                                                                                                       this->createSoundAction("rolling.mp3"),
                                                                                                                       land_hawk,
                                                                                                                       NULL)),
                                                               glass_action,
                                                               NULL);
                                
                                auto remove_thumb=Spawn::create(TargetedAction::create(thumb_after, FadeOut::create(1)),
                                                                add_shine,
                                                                NULL);
                                
                                //attackとafterは同時実行
                                auto spawn_attack=Spawn::create(attack,spawn_after, NULL);
                                
                                //last
                                auto story=CallFunc::create([]{

                                    auto story=StoryLayer::create("ending_2", []{
                                        Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
                                    });
                                    Director::getInstance()->getRunningScene()->addChild(story);
                                });
                                
                                back_final->runAction(Sequence::create(spawn_before_attack,spawn_attack,remove_thumb,story, NULL));
                            });
                            Director::getInstance()->getRunningScene()->addChild(story);
                        });
                        
                        
                        parent->runAction(Sequence::create(fadein_back,delay,spawn_run,delay_1,add_final_story, NULL));
                    });
                    Director::getInstance()->getRunningScene()->addChild(story);
                });
                
                auto seq_back_2=Sequence::create(fadein_back_2,delay,open_door,attention_action,run_thumb,fadein_final,remove_back_2,addStory_Hawk, NULL);
                
                parent->runAction(seq_back_2);
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        parent->runAction(Sequence::create(seq_barrel,addStory_barrel, NULL));
    }
    else if(key=="back_61"){
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            auto mistake=parent->getChildByName("sup_61_mistake.png");
            mistake->setOpacity(255);
        }
        callback(true);
    }
    else if(key == "answer_stair") {
        
        auto delay = DelayTime::create(.5);
        auto stairAction = CallFunc::create([this, parent, callback](){//階段を降る
            this->touchAction("stair", parent, callback);
        });
        
        parent->runAction(Sequence::create(delay, stairAction, NULL));
        
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void PigHat2ActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造
    if (key=="bag") {
        Common::playSE("p.mp3");
        auto rope=parent->itemImage->getChildByName("bag_up_rope.png");
        auto sword=createSpriteOnClip("bag_up_sword.png", true, parent,true);
        auto original_pos_sword=sword->getPosition();
        sword->setPosition(original_pos_sword.x+sword->getContentSize().width*.1, original_pos_sword.y-sword->getContentSize().height*.1);
        //
        auto seq_sword=TargetedAction::create(sword, Sequence::create(Spawn::create(FadeIn::create(1),
                                                                                    MoveTo::create(2, original_pos_sword), NULL),
                                                                      DelayTime::create(1),
                                                                      Spawn::create(EaseOut::create(MoveBy::create(1, Vec2(-sword->getContentSize().width*.2, -parent->backImage->getContentSize().height*1)), 2),
                                                                                    createSoundAction("sword.mp3"),
                                                                                    TargetedAction::create(rope, FadeOut::create(1)),
                                                                                    NULL), NULL));
        
        auto open=createSpriteOnClip("bag_open.png", true, parent,true);
        auto book=createSpriteToCenter("bag_up_book.png", true, open);
        book->setPositionY(open->getContentSize().height*.25);
        open->addChild(book);
        
        //cover
        createSpriteToCenter("bag_open_cover.png", false, open, true);
        
        auto change=createChangeAction(1, .7, parent->itemImage, open,"bohu.mp3");
        auto delay=DelayTime::create(1);
        auto get_book=TargetedAction::create(book, Spawn::create(FadeIn::create(.5),
                                                                 MoveTo::create(1, open->getContentSize()/2),
                                                                 createSoundAction("sa.mp3"), NULL));
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(seq_sword,delay,change,delay->clone(),get_book,delay->clone(),call, NULL));
    }
    else if(key=="book")
    {
        Common::playSE("p.mp3");
        auto key_0=createSpriteOnClip("book_1_key_0.png", true, parent,true);
        auto key_1=createSpriteOnClip("book_1_key_1.png", true, parent, true);
        auto open=createSpriteOnClip("book_1_open.png", true, parent, true);
        
        auto fadein_key=TargetedAction::create(key_0, Sequence::create(FadeIn::create(1),
                                                                    createSoundAction("key_0.mp3"), NULL));
        auto delay=DelayTime::create(1);
        auto change=Sequence::create(createChangeAction(1, .7, key_0, key_1),
                                     createSoundAction("key_1.mp3"), NULL);
        auto change_1=Spawn::create(createChangeAction(.5, .3, key_1, open,"pinpon.mp3"),
                                    TargetedAction::create(parent->itemImage, FadeOut::create(.3)), NULL);
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_key,delay,change,delay->clone(),change_1,delay->clone(),call, NULL));
    }
}

#pragma mark - Private
Spawn* PigHat2ActionManager::bellAction(cocos2d::Node *parent, cocos2d::Node *bell, int index)
{
    auto duration=1.0;
    auto angle=5;
    auto shake=swingAction(angle, duration/2, bell, 1.5, true);
    
    auto particle=[parent,bell](int index){
        auto call=CallFunc::create([parent,bell,index]{
            Common::playSE("handbell.mp3");
            
            auto pos=Vec2(parent->getContentSize().width*bell->getAnchorPoint().x, parent->getContentSize().height*bell->getAnchorPoint().y);
            auto size=parent->getContentSize().width*.125;
            std::vector<Color4F> startColors={Color4F(1, .984, 0, 1),Color4F(.015, .2, 1, 1),Color4F(.58, .215, 1, 1),Color4F(.58, .07, 0, 1),Color4F(0,.565, .317, 1),Color4F(.462, .839, 1, 1)};
            
            auto particle=ParticleSystemQuad::create("particle_music.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(pos);
            particle->setLife(2.5);
            particle->resetSystem();
            particle->setStartSize(size);
            particle->setEndSize(size);
            particle->setTotalParticles(10);
            particle->setGravity(Vec2(size*.5, -size*2));
            particle->setStartColor(startColors.at(index));
            particle->setEndColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 0));
            
            particle->setAngle(110);
            particle->setSpeed(size*3);
            particle->setEndSpin(10);
            particle->setEndSpinVar(20);
            parent->addChild(particle);
        });
        
        return Sequence::create(call, NULL);
    };
    
    return Spawn::create(shake,particle(index), NULL);
}

#pragma mark - カスタムアクション
void PigHat2ActionManager::customAction(std::string key,Node*parent)
{
    if (key == CustomAction_Title) {//タイトルシーンでは権利表記をコードで表示する。
        auto text = "©鈴木央・講談社／「劇場版 七つの大罪」製作委員会";
        auto rightsLabel = Label::createWithSystemFont(text, "", 30);
        
        while (Director::getInstance()->getRunningScene()->getContentSize().width*.7 < rightsLabel->getContentSize().width) {//ラベルが画面をはみ出している間はずっと通る。
            rightsLabel->setSystemFontSize(rightsLabel->getSystemFontSize()-1);
        }
        
        rightsLabel->setPosition(parent->getContentSize().width/2, BannerBridge::getBannerHeight()+rightsLabel->getContentSize().height*.7);
        parent->addChild(rightsLabel);
    }
}
