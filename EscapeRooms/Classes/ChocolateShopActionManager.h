//
//  ChocolateShopActionManager.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/11/29.
//
//

#ifndef __EscapeContainer__ChocolateShopActionManager__
#define __EscapeContainer__ChocolateShopActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class ChocolateShopActionManager:public CustomActionManager
{
private:
    
public:
    static ChocolateShopActionManager* manager;
    static ChocolateShopActionManager* getInstance();
   
    
    
    //over ride
    int getDefaultBackNum();
    void backAction(std::string key,int backID,Node*parent,ShowType type);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void touchBeganAction(std::string key,Node*parent);
    
    void dragAction(std::string key,Node*parent,Vec2 pos);
    
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    
    void customAction(std::string key,Node*parent);
    
private:
    std::vector<Vec2> getMatchPoses(Node* parent, int backID);
    ParticleSystemQuad* showFireParticle(Node *parent, int backID, int num);
    FiniteTimeAction* iceMachineAction(Node* parent);

};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
