//
//  WhiteDayActionManager.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/03/05.
//
//

#include "WhiteDayActionManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "GameScene.h"

using namespace cocos2d;

WhiteDayActionManager* WhiteDayActionManager::manager =NULL;

WhiteDayActionManager* WhiteDayActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new WhiteDayActionManager();
    }
    return manager;
}

#pragma mark - Back
void WhiteDayActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="pigs")
    {
        //armを
        auto pig_0=parent->getChildByName("sup_18_pig_0.png");
        auto cover_0=createSpriteToCenter("sup_18_pig_0.png", false, pig_0);
        cover_0->setLocalZOrder(1);
        pig_0->addChild(cover_0);
        auto pig_1=parent->getChildByName("sup_18_pig_1.png");
        auto cover_1=createSpriteToCenter("sup_18_pig_1.png", false, pig_1);
        cover_1->setLocalZOrder(1);
        pig_1->addChild(cover_1);
        
        auto scale_original=pig_0->getScale();
        
        std::vector<Vec2>vecs_0={Vec2(.23,.52),Vec2(.35,.52)};
        std::vector<Vec2>vecs_1={Vec2(.64,.52),Vec2(.76,.52)};

        for (int i=0; i<2; i++) {
            auto arm=AnchorSprite::create(vecs_0.at(i), parent->getContentSize(), StringUtils::format("sup_18_arm_0_%d.png",i));
            pig_0->addChild(arm);
            auto arm_1=AnchorSprite::create(vecs_1.at(i), parent->getContentSize(), StringUtils::format("sup_18_arm_1_%d.png",i));
            pig_1->addChild(arm_1);
        }
        
        auto breakAction=[pig_1,pig_0,parent,this,scale_original](int index){
            auto pig=(index==0)?pig_0:pig_1;
            auto arm_0=pig->getChildren().at(1);
            auto arm_1=pig->getChildren().at(2);
            auto attention=this->createSpriteToCenter(StringUtils::format("sup_18_at_%d.png",index), true, parent);
            parent->addChild(attention);
            
            auto height=parent->getContentSize().height*.05;
            auto angle=-30;
            auto pig_action=TargetedAction::create(pig, Sequence::create(EaseOut::create(MoveBy::create(.1, Vec2(0, height)), 1.5),
                                                                         EaseIn::create(MoveBy::create(.1, Vec2(0, -height)), 1.5), NULL));
            auto arm_action=Spawn::create(TargetedAction::create(arm_0, RotateBy::create(.2, -angle)),
                                          TargetedAction::create(arm_1, RotateBy::create(.2, angle)), NULL) ;
            auto gum_action=Spawn::create(Common::createSoundAction("pan.mp3"),
                                          TargetedAction::create(parent->getChildByName(StringUtils::format("gum_%d",index)),
                                                                 Spawn::create(ScaleTo::create(.1, scale_original*.01),
                                                                               FadeOut::create(0),NULL)), NULL);
            auto attention_action=TargetedAction::create(attention, Spawn::create(FadeIn::create(.1),
                                                                                  Sequence::create(EaseOut::create(MoveBy::create(.2, Vec2(0, height)), 1.5), EaseIn::create(MoveBy::create(.2, Vec2(0, -height)), 1.5),
                                                                                                   DelayTime::create(.2),
                                                                                                   FadeOut::create(.2),
                                                                                                   NULL), NULL));
            
            auto spawn=Spawn::create(pig_action,arm_action,gum_action,attention_action, NULL);
            auto arm_action_1=Spawn::create(TargetedAction::create(arm_0, RotateBy::create(.2, angle)),
                                          TargetedAction::create(arm_1, RotateBy::create(.2, -angle)), NULL) ;
            auto seq=Sequence::create(spawn,DelayTime::create(.5),arm_action_1,DelayTime::create(.7), NULL);
            return seq;
        };
        
        Vector<FiniteTimeAction*>gumActions;
        
        if(DataManager::sharedManager()->getEnableFlagWithFlagID(7)){
            auto gum=AnchorSprite::create(Vec2(.27, .54), parent->getContentSize(), "sup_18_gum_0.png");
            
            auto seq=gumAction(parent, gum, true);
            gumActions.pushBack(Sequence::create(seq,breakAction(0), NULL));
        }
        if(DataManager::sharedManager()->getEnableFlagWithFlagID(32)){
            auto gum=AnchorSprite::create(Vec2(.728, .54), parent->getContentSize(), "sup_18_gum_1.png");
            auto seq=gumAction(parent, gum, false);
            gumActions.pushBack(Sequence::create(seq,breakAction(1), NULL));
        }
        
        if (gumActions.size()>0) {
            parent->runAction(Repeat::create(Spawn::create(gumActions), UINT_MAX));
        }
    }
    else if(key=="wheel")
    {
        candyAction(parent,backID);
    }
    else if(key=="doll")
    {
        dollAction(parent,backID);
    }
    else if(key=="back_1"){
        dollAction(parent,backID);
        candyAction(parent,backID);
    }
    else if(key=="back_2"){
        Vector<FiniteTimeAction*>gumActions;
        
        auto height=parent->getContentSize().height*.01;
        if(DataManager::sharedManager()->getEnableFlagWithFlagID(7)){
            auto gum=AnchorSprite::create(Vec2(.3, .37), parent->getContentSize(), "sup_2_gum_0.png");
            auto pig=parent->getChildByName("sup_2_pig_0.png");
            auto seq=gumAction(parent, gum, true);
            auto breakAction=Spawn::create(TargetedAction::create(gum, Spawn::create(ScaleTo::create(.05, .01),FadeOut::create(.05), NULL)),
                                           jumpAction(height, .15, pig, 1.5), NULL);
            gumActions.pushBack(Sequence::create(seq,breakAction,DelayTime::create(1), NULL));
        }
        if(DataManager::sharedManager()->getEnableFlagWithFlagID(32)){
            auto gum=AnchorSprite::create(Vec2(.483, .37), parent->getContentSize(), "sup_2_gum_1.png");
            auto pig=parent->getChildByName("sup_2_pig_1.png");
            auto seq=gumAction(parent, gum, false);
            auto breakAction=Spawn::create(TargetedAction::create(gum, Spawn::create(ScaleTo::create(.05, .01),FadeOut::create(.05), NULL)),
                                           jumpAction(height, .15, pig, 1.5), NULL);
            gumActions.pushBack(Sequence::create(seq,breakAction,DelayTime::create(1), NULL));
        }
        
        if (gumActions.size()>0) {
            parent->runAction(Repeat::create(Spawn::create(gumActions), UINT_MAX));
        }
    }
}

#pragma mark - Touch
void WhiteDayActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key.compare(0,3,"pig")==0)
    {
        Common::playSE("pig_1.mp3");
        auto num=atoi(key.substr(4,1).c_str());
        auto cloud_anchor=Vec2(.36,.66);
        if (num==1) {
            cloud_anchor=Vec2(.63,.66);
        }
        
        auto cloud=AnchorSprite::create(cloud_anchor, parent->getContentSize(), StringUtils::format("sup_18_cloud_%d.png",num));
        cloud->setOpacity(0);
        parent->addChild(cloud);
        auto original_scale=cloud->getScale();
        cloud->setScale(.1);
        
        auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(.5, original_scale),
                                                                                    FadeIn::create(.5), NULL),
                                                                      Common::createSoundAction("nyu.mp3"),
                                                                      DelayTime::create(.5),
                                                                      Spawn::create(ScaleTo::create(.5, .1),
                                                                                    FadeOut::create(.5), NULL),RemoveSelf::create(), NULL));
        
        auto pig=parent->getChildByName<Sprite*>(StringUtils::format("sup_18_pig_%d.png",num));
        auto duration_scale=.2;
        
        auto original_scale_pig=pig->getScale();
        auto scale_0=ScaleTo::create(duration_scale, original_scale_pig*1.1, original_scale_pig*.9);
        auto scale_1=ScaleTo::create(duration_scale, original_scale_pig, original_scale_pig*1.05);
        auto scale_2=ScaleTo::create(duration_scale, original_scale_pig);
        
        auto seq_pig=TargetedAction::create(pig,Sequence::create(scale_0,scale_1,scale_2, NULL));
        
        auto spawn=Spawn::create(seq_cloud,seq_pig, NULL);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,call, NULL));
    }
    else if(key.compare(0,3,"gum")==0){
        Common::playSE("p.mp3");
        parent->stopAllActions();
        if(parent->getChildByName("gum_0")){
            auto gum=parent->getChildByName("gum_0");
            gum->runAction(Spawn::create(ScaleTo::create(.3, .01),
                                         FadeOut::create(1), NULL));
        }
        if(parent->getChildByName("gum_1")){
            auto gum=parent->getChildByName("gum_1");
            gum->runAction(Spawn::create(ScaleTo::create(.3, .01),
                                         FadeOut::create(1), NULL));
        }
        
        auto index=atoi(key.substr(4,1).c_str());
        std::vector<Vec2>vecs={Vec2(.275, .38),Vec2(.725, .38)};
        auto item=AnchorSprite::create(vecs.at(index), parent->getContentSize(), StringUtils::format("anim_18_gum_%d.png",index));
        item->setOpacity(0);
        parent->addChild(item);
        
        auto pig=parent->getChildByName<Sprite*>(StringUtils::format("sup_18_pig_%d.png",index));
        auto duration_scale=.2;
        
        auto original_scale=pig->getScale();
        auto scale_0=ScaleTo::create(duration_scale, original_scale*1.1, original_scale*.9);
        auto scale_1=ScaleTo::create(duration_scale, original_scale, original_scale*1.05);
        auto scale_2=ScaleTo::create(duration_scale, original_scale);
        auto seq_pig=TargetedAction::create(pig,Sequence::create(scale_0,scale_1,scale_2, NULL));

        auto distance=parent->getContentSize().height*.16;

        auto fadein=TargetedAction::create(item, FadeIn::create(1));
        auto spawn=TargetedAction::create(item, Spawn::create(ScaleBy::create(2, .6),EaseIn::create(MoveBy::create(2, Vec2(0, distance)), 2) , NULL));
        auto sound=Common::createSoundAction("paku.mp3");
        auto fadeout=TargetedAction::create(item, FadeOut::create(.2));
        auto spawn_eat=Spawn::create(fadeout,seq_pig, NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,spawn,sound,spawn_eat,DelayTime::create(.7),call, NULL));
    }
#pragma mark ドライバ
    else if(key=="driver")
    {
        Common::playSE("p.mp3");
        
        auto action=[parent,this](int index){
            auto driver=this->createSpriteToCenter(StringUtils::format("anim_39_%d.png",index), true, parent);
            parent->addChild(driver);
            
            auto screw=parent->getChildByName(StringUtils::format("sup_39_%d.png",index));
            
            auto fadein=FadeIn::create(.5);
            
            auto distance=parent->getContentSize().width*.01;
            auto distance_remove=parent->getContentSize().height*.02;
            auto shake=Repeat::create(Sequence::create(MoveBy::create(.1,Vec2(distance, distance)),
                                                       CallFunc::create([]{Common::playSE("kacha_1.mp3");}),
                                                       MoveBy::create(.1,Vec2(-distance, -distance)), NULL),3);
            auto remove_screw_0=TargetedAction::create(screw, FadeOut::create(0));

            
            return Sequence::create(TargetedAction::create(driver, Sequence::create(fadein,shake, NULL)),
                                    remove_screw_0,
                                    TargetedAction::create(driver, Spawn::create(MoveBy::create(.3, Vec2(0, distance_remove)),
                                                                                 FadeOut::create(.3), NULL)),
                                    TargetedAction::create(driver, FadeOut::create(0)),NULL);
        };
        
        auto chair=parent->getChildByName("sup_39_chair.png");
        auto remove=TargetedAction::create(chair, Spawn::create(FadeOut::create(1),
                                                                MoveBy::create(1, Vec2(0, parent->getContentSize().height*.2)), NULL));
        
        auto call=CallFunc::create([callback](){
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(action(0),DelayTime::create(.3),action(1),DelayTime::create(.3),action(2),DelayTime::create(.7),remove,DelayTime::create(.5),call, NULL));
    }
    else if(key.compare(0,4,"coin")==0){
        Common::playSE("p.mp3");
        
        auto index=atoi(key.substr(5,1).c_str());
        auto distance=parent->getContentSize().width*.1;
        
        auto coin=AnchorSprite::create(Vec2(.75, .333), parent->getContentSize(), "anim_8_coin.png");
        coin->setOpacity(0);
        auto original_pos=coin->getPosition();
        coin->setPosition(coin->getPosition().x+distance, coin->getPosition().y-distance);
        parent->addChild(coin);
        auto original_scale=coin->getScale();
        coin->setScale(original_scale*1.5);
        
        auto gum=createSpriteToCenter(StringUtils::format("anim_8_gum_%d.png",index), false, parent);
        gum->setPositionY(parent->getContentSize().height*.65);
        parent->addChild(gum);
        
        auto seq_coin=TargetedAction::create(coin, Sequence::create(FadeIn::create(.7),
                                                                    DelayTime::create(.5),
                                                                    Spawn::create(ScaleTo::create(1, original_scale),
                                                                                  MoveTo::create(1, original_pos), NULL),
                                                                    DelayTime::create(.2),
                                                                    Common::createSoundAction("chalin.mp3"),
                                                                    FadeOut::create(.3),NULL));
        
        auto seq_gum=TargetedAction::create(gum, Sequence::create(Common::createSoundAction("koto.mp3"),
                                                                  EaseIn::create(MoveTo::create(.2, parent->getContentSize()/2), 2) ,
                                                                  JumpTo::create(.2, parent->getContentSize()/2, parent->getContentSize().height*.01, 2),
                                                                  DelayTime::create(.5),
                                                                  FadeOut::create(.7),NULL));
        
        auto call=CallFunc::create([callback](){
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_coin,DelayTime::create(1),seq_gum,call, NULL));
    }
    else if(key=="knife"){
        auto knife=AnchorSprite::create(Vec2(.725, .65), parent->getContentSize(), "anim_45_knife.png");
        knife->setOpacity(0);
        parent->addChild(knife);
        
        Common::playSE("p.mp3");
        Vector<FiniteTimeAction*>actions;
        auto fadein=FadeIn::create(.5);
        actions.pushBack(fadein);
        
        auto angle=10;
        auto distance=parent->getContentSize().height*.1;
        auto duration_cut=.2;
        auto distance_move=parent->getContentSize().width*.18;
        
        for (int i=0; i<4; i++) {
            auto cake=parent->getChildByName(StringUtils::format("sup_45_%d.png",i));
            
            auto cut_0=Spawn::create(RotateBy::create(duration_cut, angle),
                                   MoveBy::create(duration_cut, Vec2(0, -distance)),
                                     Common::createSoundAction("sa.mp3"), NULL);
            actions.pushBack(cut_0);
            auto fadeout=TargetedAction::create(cake, FadeOut::create(.3));
            actions.pushBack(fadeout);
            
            auto cut_1=Spawn::create(RotateBy::create(duration_cut, -angle),
                                     MoveBy::create(duration_cut, Vec2(0, distance)), NULL);
            actions.pushBack(cut_1);
            
            if (i==2) {
                distance_move=parent->getContentSize().width*.25;
            }
            
            auto move=MoveBy::create(.7, Vec2(-distance_move, 0));
            actions.pushBack(move);
        }
        
        actions.pushBack(FadeOut::create(.5));
        
        auto seq_knife=TargetedAction::create(knife, Sequence::create(actions));

        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_knife,call, NULL));
    }
    else if(key=="freeze"){
        Common::playSE("p.mp3");
        
        auto anim=createSpriteToCenter("anim_41_mold.png", true, parent);
        parent->addChild(anim);
        
        auto close=createSpriteToCenter("back_41.png", true, parent);
        parent->addChild(close);
        
        auto fadein=TargetedAction::create(anim, FadeIn::create(.5));
        auto delay_0=DelayTime::create(1);

        auto close_action=TargetedAction::create(close, Spawn::create(FadeIn::create(.1),
                                                                      Common::createSoundAction("ban.mp3"), NULL));
        auto sound=Common::createSoundAction("timer.mp3");
        auto delay=DelayTime::create(3.5);
        
        auto open_action=TargetedAction::create(close, Spawn::create(Common::createSoundAction("gacha.mp3"),
                                                                     FadeOut::create(.1),NULL));
        auto delay_1=DelayTime::create(.5);
        auto fadeout=TargetedAction::create(anim, FadeOut::create(1));
        auto call=CallFunc::create([callback](){
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay_0,close_action,sound,delay,open_action,delay_1,fadeout,call, NULL));
    }
    else if(key=="match"){
        Common::playSE("p.mp3");
        
        auto match=createSpriteToCenter("anim_51_match.png", true, parent);
        parent->addChild(match);
        
        //create action
        auto fire_action=[this,match,parent](Vec2 pos){
            auto call=CallFunc::create([this,parent,pos]{
                auto size=parent->getContentSize().width*.02;

                auto fire=this->createFire(Vec2(parent->getContentSize().width*pos.x, parent->getContentSize().height*pos.y),size);
                
                parent->addChild(fire);
            });
            
            auto distance=parent->getContentSize().height*.01;
            auto move=TargetedAction::create(match, Sequence::create(MoveBy::create(.1, Vec2(-distance, -distance)),
                                                                     MoveBy::create(.1, Vec2(distance, distance)), NULL));
            
            return Spawn::create(call,move, NULL);
        };
        
        //
        Vector<FiniteTimeAction*>actions;
        auto fadein=TargetedAction::create(match, FadeIn::create(.5));
        actions.pushBack(fadein);
        auto delay_0=DelayTime::create(1);
        actions.pushBack(delay_0);
        
        auto fire_match=CallFunc::create([parent,this,match]{
            auto size=parent->getContentSize().width*.02;
            Common::playSE("huo.mp3");

            auto fire=createFire(Vec2(.35*parent->getContentSize().width, .78*parent->getContentSize().height), size);
            fire->setPosVar(Vec2(size*.2, 0));
            fire->setName("fire");
            match->addChild(fire);
        });
        actions.pushBack(fire_match);
        
        std::vector<Vec2>poses={Vec2(.35, .78),Vec2(.425, .78),Vec2(.5, .78),Vec2(.577, .78),Vec2(.658, .78)};
        for(int i=0;i<5;i++)
        {
            actions.pushBack(fire_action(poses.at(i)));
            actions.pushBack(DelayTime::create(.7));
            actions.pushBack(TargetedAction::create(match, MoveBy::create(.7, Vec2(parent->getContentSize().width*.08, 0))));
        }
        
        auto fadeout=TargetedAction::create(match, FadeOut::create(1));
        auto remove_fire=CallFunc::create([match]{
            auto fire=match->getChildByName<ParticleSystemQuad*>("fire");
            if (fire) {
                fire->setDuration(.3);
            }
        });
        actions.pushBack(Spawn::create(fadeout,remove_fire, NULL));
        
        //change
        auto back=createSpriteToCenter("back_51.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        back->setLocalZOrder(1);
        parent->addChild(back);
        back->addChild(createSpriteToCenter("sup_51_candle_1.png", false, back));
        
        auto fadein_1=TargetedAction::create(back, FadeIn::create(1));
        auto fire_1=CallFunc::create([this,back]{
            auto size=back->getContentSize().width*.02;
            std::vector<Vec2>poses={Vec2(.35, .64),Vec2(.425, .5),Vec2(.5, .565),Vec2(.577, .7),Vec2(.658, .64)};

            for (int i=0; i<5; i++) {
                auto pos=poses.at(i);
                auto fire=this->createFire(Vec2(back->getContentSize().width*poses.at(i).x, back->getContentSize().height*poses.at(i).y), size);
                back->addChild(fire);
            }
        });
        actions.pushBack(Spawn::create(fadein_1,fire_1, NULL));
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="monkey"){
        Common::playSE("p.mp3");
        
        auto cookie=createSpriteToCenter("anim_50_cookie.png", true, parent);
        parent->addChild(cookie);
        auto cookie_1=createSpriteToCenter("sup_50_cookie_1.png", true, parent);
        parent->addChild(cookie_1);
        
        auto monkey_0=parent->getChildByName<Sprite*>("sup_50_monkey_0.png");
        auto monkey_1=createSpriteToCenter("anim_50_monkey.png", true, parent);
        parent->addChild(monkey_1);
        

        auto fadein=TargetedAction::create(cookie, FadeIn::create(.7));
        auto delay_0=DelayTime::create(.7);
        auto change=Spawn::create(createChangeAction(1, .5, monkey_0, monkey_1),
                                  Common::createSoundAction("monkey.mp3"), NULL);
        auto change_1=createChangeAction(1, .5, cookie, cookie_1);
        auto change_2=createChangeAction(1, .5, monkey_1, monkey_0);
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay_0,change,delay_0->clone(),change_1,change_2,call,NULL));
    }
    else if(key=="bake"){
        Common::playSE("p.mp3");
        
        auto cookie_0=createSpriteToCenter("anim_38_cookie_0.png", true, parent);
        parent->addChild(cookie_0);
        auto cookie_1=createSpriteToCenter("anim_38_cookie_1.png", true, parent);
        parent->addChild(cookie_1);
        
        auto close=createSpriteToCenter("back_38.png", true, parent);
        close->setCascadeOpacityEnabled(true);
        parent->addChild(close);
        
        auto sup=createSpriteToCenter("anim_38_bake.png", false, parent);
        close->addChild(sup);
        
        auto fadein=TargetedAction::create(cookie_0, FadeIn::create(.7));
        auto delay_0=DelayTime::create(.7);
        auto bake_action=TargetedAction::create(close, Spawn::create(Common::createSoundAction("ban.mp3"),
                                                                     FadeIn::create(.2), NULL));
        auto timer=Common::createSoundAction("timer.mp3");
        auto delay_1=DelayTime::create(4);
        auto open=Spawn::create(TargetedAction::create(close, FadeOut::create(.2)),
                                Common::createSoundAction("gacha.mp3"),
                                createChangeAction(.3, .1, cookie_0, cookie_1), NULL);
        auto fadeout=TargetedAction::create(cookie_1, FadeOut::create(.7));
        auto delay_2=DelayTime::create(.5);
        auto call=CallFunc::create([callback](){
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay_0,bake_action,timer,delay_1,open,fadeout,delay_2,call,NULL));

    }
    else if(key=="clear"){
        auto story=StoryLayer::create("ending", [this,parent](Ref*ref)
        {//お宝ゲット->ケーキ取得
            auto cake=parent->getChildByName("sup_13_cake.png");
            auto fadeout=TargetedAction::create(cake, FadeOut::create(1));
            auto call=CallFunc::create([this,parent]{
                Common::playSE("pinpon.mp3");
                
                //scene change->豚入店
                auto back_1=this->createSpriteToCenter("back_1.png", true, parent);
                back_1->setCascadeOpacityEnabled(true);
                parent->addChild(back_1);
                
                this->candyAction(back_1,1);
                this->dollAction(back_1,1);
                
                auto cover=this->createSpriteToCenter("sup_1_cover.png", false, parent);
                back_1->addChild(cover);
                
                auto pig_0=this->createSpriteToCenter("anim_1_pig_0.png", true, parent);
                parent->addChild(pig_0);
                auto pig_1=this->createSpriteToCenter("anim_1_pig_1.png", true, parent);
                parent->addChild(pig_1);
                
                //キョロキョロ
                auto pig_2=this->createSpriteToCenter("anim_1_pig_2.png", true, parent);
                parent->addChild(pig_2);
                auto pig_3=this->createSpriteToCenter("anim_1_pig_3.png", true, parent);
                parent->addChild(pig_3);
                auto pig_4=this->createSpriteToCenter("anim_1_pig_4.png", true, parent);
                parent->addChild(pig_4);
                //!マーク
                auto attention=this->createSpriteToCenter("anim_1_attention.png", true, parent);
                parent->addChild(attention);
                auto height=back_1->getContentSize().height*.02;
                
                auto fadein=TargetedAction::create(back_1, FadeIn::create(1));
                auto anim_0=Spawn::create(TargetedAction::create(pig_0, FadeIn::create(.2)),
                                          Common::createSoundAction("bell_2.mp3"), NULL);
                auto anim_1=Spawn::create(TargetedAction::create(pig_0, RemoveSelf::create()),
                                          TargetedAction::create(pig_1, FadeIn::create(.2)),
                                          Common::createSoundAction("ban.mp3"), NULL);
                auto anim_2=Spawn::create(this->createChangeAction(.5, .3, pig_1, pig_2),Common::createSoundAction("nyu.mp3"), NULL);
                auto anim_3=Spawn::create(this->createChangeAction(.5, .3, pig_2, pig_3), Common::createSoundAction("nyu.mp3"), NULL) ;
                auto anim_4=Spawn::create(this->createChangeAction(.5, .3, pig_3, pig_4),
                                          TargetedAction::create(attention,Spawn::create(FadeIn::create(.1),
                                                                                         Sequence::create(MoveBy::create(.15, Vec2(0, height)),
                                                                                                          MoveBy::create(.15, Vec2(0, -height)), NULL), NULL) ),
                                          Common::createSoundAction("surprise.mp3"), NULL);
                
                //scene change
                auto back_2=this->createSpriteToCenter("anim_pig_5.png", true, parent);
                back_2->setCascadeOpacityEnabled(true);
                parent->addChild(back_2);
                
                auto body=AnchorSprite::create(Vec2(.5, .15), back_2->getContentSize(), "anim_pig_5_body.png");
                body->setCascadeOpacityEnabled(true);
                back_2->addChild(body);
                
                auto arm_0=AnchorSprite::create(Vec2(.4, .4), back_2->getContentSize(), "anim_pig_5_0.png");
                body->addChild(arm_0);
                auto arm_1=AnchorSprite::create(Vec2(.575, .4), back_2->getContentSize(), "anim_pig_5_1.png");
                body->addChild(arm_1);
                
                auto head=AnchorSprite::create(Vec2(.5, .45), back_2->getContentSize(), "anim_pig_5_head.png");
                body->addChild(head);
                
                auto fadein_1=TargetedAction::create(back_2, FadeIn::create(1));
                auto call=CallFunc::create([this,parent,body,head,arm_0,arm_1]{
                    auto story=StoryLayer::create("ending1",[this,parent,body,head,arm_0,arm_1](Ref*ref){
                        auto cake=this->createSpriteToCenter("anim_pig_cake.png", true, parent);
                        parent->addChild(cake);
                        
                        auto attention=this->createSpriteToCenter("anim_pig_attention.png", true, parent);
                        parent->addChild(attention);
                        auto height=parent->getContentSize().height*.03;
                        
                        auto give=TargetedAction::create(cake, Spawn::create(FadeIn::create(.7),
                                                                             ScaleBy::create(1.5, .8), NULL));
                        auto eat=Spawn::create(TargetedAction::create(cake, FadeOut::create(.5)),
                                               Common::createSoundAction("paku.mp3"),
                                               TargetedAction::create(head, Repeat::create(this->createBounceAction(head, .3), 2)), NULL);
                        auto shake_body=this->createBounceAction(body, .5);
                        auto attention_action=TargetedAction::create(attention, Spawn::create(FadeIn::create(.1),
                                                                                              Common::createSoundAction("surprise.mp3"),
                                                                                              Sequence::create(MoveBy::create(.2, Vec2(0, height)),
                                                                                                               MoveBy::create(.2, Vec2(0, -height)),
                                                                                                               DelayTime::create(.7), NULL), NULL));
                        //あばれぶた
                        auto angle=40;
                        auto duration=.2;
                        auto count=7;
                        auto rage=Spawn::create(TargetedAction::create(attention, FadeOut::create(.2)),
                                                TargetedAction::create(arm_0, Repeat::create(Sequence::create(RotateBy::create(duration, angle),
                                                                                                              RotateBy::create(duration, -angle), NULL), count)),
                                                TargetedAction::create(arm_1, Repeat::create(Sequence::create(RotateBy::create(duration, -angle),
                                                                                                              RotateBy::create(duration, angle), NULL), count)),
                                                Common::createSoundAction("pig_1.mp3"),
                                                Repeat::create(createBounceAction(body, .2), count),
                                                NULL);
                        
                        auto call=CallFunc::create([this,parent,body]{
                            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

                            auto story=StoryLayer::create("ending2", [this,parent,body](Ref*ref){
                                auto open=createSpriteToCenter("anim_pig_6_.png", true, parent);
                                parent->addChild(open);
                                
                                auto change=Spawn::create(Common::createSoundAction("pig_1.mp3")
                                                          ,Common::createSoundAction("bell_2.mp3"),
                                                          createChangeAction(1, .5, body, open), NULL);
                                auto close=Spawn::create(Common::createSoundAction("ban.mp3"),
                                                         TargetedAction::create(open, FadeOut::create(.2)), NULL);
                                
                                auto call=CallFunc::create([]{
                                    auto story=StoryLayer::create("ending3", [](Ref*ref){
                                        Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
                                    });
                                    Director::getInstance()->getRunningScene()->addChild(story);
                                });
                                parent->runAction(Sequence::create(change,DelayTime::create(1),close,DelayTime::create(.7),call, NULL));
                            });
                            Director::getInstance()->getRunningScene()->addChild(story);
                        });
                        
                        parent->runAction(Sequence::create(give,eat,DelayTime::create(.3),shake_body,DelayTime::create(.5),attention_action,rage,DelayTime::create(1),call, NULL));
                    });
                    Director::getInstance()->getRunningScene()->addChild(story);
                });
                parent->runAction(Sequence::create(fadein,DelayTime::create(1),anim_0,DelayTime::create(1),anim_1,DelayTime::create(1),anim_2,DelayTime::create(.5),anim_3,DelayTime::create(1),anim_4,DelayTime::create(1),fadein_1,DelayTime::create(1),call, NULL));
            });
            
            parent->runAction(Sequence::create(fadeout,call, NULL));
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
    else if (key=="dontopen")
    {
        auto story=StoryLayer::create("dontopen", [callback](Ref*ref){
            callback(true);
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
    else if(key=="freezer_open")
    {
        if (DataManager::sharedManager()->getTemporaryFlag("smoke").asInt()==0) {
            auto particle=ParticleSystemQuad::create("smoke.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width/2,parent->getContentSize().height*.475);
            particle->setDuration(.2);
            particle->setStartSize(parent->getContentSize().width*.1);
            particle->setStartSizeVar(particle->getStartSize()*.2);
            particle->setEndSize(particle->getStartSize()*4);
            particle->setSpeed(parent->getContentSize().height*.075);
            particle->setSpeedVar(particle->getSpeed()*.2);
            particle->setGravity(Vec2(0, particle->getSpeed()*1));
            particle->setLife(3);
            particle->setTotalParticles(1500);
            particle->setPosVar(Vec2(parent->getContentSize().width*.35, parent->getContentSize().height*.15));
            particle->setAngle(270);
            particle->setAngleVar(5);
            parent->addChild(particle);
            
            DataManager::sharedManager()->setTemporaryFlag("smoke", 1);
        }
        
        
        callback(true);
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void WhiteDayActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{
    
}


#pragma mark - Private
Sequence* WhiteDayActionManager::gumAction(cocos2d::Node *parent, cocos2d::Node *gum, bool isLeft)
{
    auto scale_original=gum->getScale();
    auto duration=2.5;
    auto rate=1.5;
    
    gum->setName(StringUtils::format("gum_%d",!isLeft));
    gum->setScale(.01);
    gum->setOpacity(0);
    parent->addChild(gum);
    
    auto fadein=TargetedAction::create(gum, FadeIn::create(.1));
    auto scale_0=TargetedAction::create(gum,EaseInOut::create(ScaleTo::create(duration,scale_original* .33), rate));
    auto scale_1=TargetedAction::create(gum,EaseInOut::create(ScaleTo::create(duration,scale_original* .66), rate));
    auto scale_2=TargetedAction::create(gum,EaseInOut::create(ScaleTo::create(duration,scale_original), rate));
    
    if (isLeft) {
        return Sequence::create(fadein,scale_1,DelayTime::create(.3),scale_0,DelayTime::create(.3),scale_1,DelayTime::create(.3),scale_2,DelayTime::create(.3), NULL);

    }
    else{
        return Sequence::create(fadein,scale_2,DelayTime::create(.3),scale_1,DelayTime::create(.3),scale_0,DelayTime::create(.3),scale_2,DelayTime::create(.3), NULL);
    }
}

void WhiteDayActionManager::dollAction(cocos2d::Node *parent, int back)
{
    auto anchor=Vec2(.5, .5);
    auto angle=10;

    if (back==1) {
        angle=5;
        anchor=Vec2(.583, .35);
    }
    auto head=AnchorSprite::create(anchor, parent->getContentSize(), StringUtils::format("sup_%d_head.png",back));
    parent->addChild(head);
    
    Vector<FiniteTimeAction*>actions;
    auto duration=2.0;
    auto rate=1.5;
    actions.pushBack(TargetedAction::create(head, Sequence::create(EaseInOut::create(RotateBy::create(duration, angle), rate),
                                                                   EaseInOut::create(RotateBy::create(duration, -angle), rate), NULL)));
    actions.pushBack(TargetedAction::create(head, Sequence::create(EaseInOut::create(RotateBy::create(duration, -angle), rate),
                                                                   EaseInOut::create(RotateBy::create(duration, angle), rate), NULL)));
    
    parent->runAction(Repeat::create(Sequence::create(DelayTime::create(.7),actions.at(0),actions.at(1),actions.at(1)->clone(),actions.at(0)->clone(),actions.at(1)->clone(),actions.at(0)->clone(),DelayTime::create(.7), NULL), UINT_MAX));
}

void WhiteDayActionManager::candyAction(cocos2d::Node *parent, int back)
{
    if (DataManager::sharedManager()->getEnableFlagWithFlagID(37)) {
        auto anchor=Vec2(.5, .53);
        if (back==1) {
            anchor=Vec2(.12, .485);
        }
        else if(back==5){
            anchor=Vec2(.277, .382);
        }
        
        auto wheel=AnchorSprite::create(anchor, parent->getContentSize(), StringUtils::format("sup_%d_candy.png",back));
        parent->addChild(wheel);

        if(DataManager::sharedManager()->isPlayingMinigame()&&back==6){
            auto mistake=parent->getChildByName("sup_6_mistake.png");
            mistake->setAnchorPoint(wheel->getAnchorPoint());
            mistake->setPosition(wheel->getPosition());
            
            auto rotate=EaseInOut::create(RotateBy::create(12, 300), 1.2);
            auto spawn=Spawn::create(TargetedAction::create(wheel, rotate),
                                     TargetedAction::create(mistake, rotate->clone()), NULL);
            auto call=CallFunc::create([]{DataManager::sharedManager()->setTemporaryFlag("candy", 1);});
            parent->runAction(Sequence::create(spawn,call, NULL));
        }
        else{
            wheel->runAction(Repeat::create(RotateBy::create(12, 360), UINT_MAX));
        }
    }
}

ParticleSystemQuad* WhiteDayActionManager::createFire(cocos2d::Vec2 pos, float size)
{
    auto particle=ParticleSystemQuad::create("fire.plist");
    particle->setPosition(pos);
    particle->setAutoRemoveOnFinish(true);
    particle->setStartSize(size);
    particle->setEndSize(size*2);
    particle->setSpeed(size);
    particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
    
    return particle;
}

/*Sprite* WhiteDayActionManager::createSpriteToCenter(std::string name, bool invisible, cocos2d::Node *parent)
{
    auto spr=Sprite::createWithSpriteFrameName(name);
    spr->setPosition(parent->getContentSize()/2);
    spr->setName(name);
    if (invisible) {
        spr->setOpacity(0);
    }
    return spr;
}

Spawn* WhiteDayActionManager::createChangeAction(float duration_fadeout,float duration_fadein, cocos2d::Sprite *before, cocos2d::Sprite *after)
{
    auto spawn=Spawn::create(TargetedAction::create(before, FadeOut::create(duration_fadeout)),
                             TargetedAction::create(after, FadeIn::create(duration_fadein)), NULL);
    
    return spawn;
}

TargetedAction* WhiteDayActionManager::createBounceAction(cocos2d::Node *node, float duration)
{
    auto scale=node->getScale();
    return TargetedAction::create(node, Sequence::create(EaseIn::create(ScaleTo::create(duration, scale*1.1, scale*.95), 1.5) ,
                                                         EaseOut::create(ScaleTo::create(duration, scale*.95, scale*1.05), 1.5),
                                                         ScaleTo::create(duration/2, scale)
                                                         , NULL));
}*/

