//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__PublicBathActionManager__
#define __EscapeContainer__PublicBathActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class PublicBathActionManager:public CustomActionManager
{
public:
    static PublicBathActionManager* manager;
    static PublicBathActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
