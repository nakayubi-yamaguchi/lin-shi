//
//  LiveHouseActionManager.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/03/30.
//
//

#include "LiveHouseActionManager.h"
#include "DataManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"

using namespace cocos2d;

LiveHouseActionManager* LiveHouseActionManager::manager =NULL;

LiveHouseActionManager* LiveHouseActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new LiveHouseActionManager();
    }
    return manager;
}

#pragma mark - Back
void LiveHouseActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="headphone"){
        //ヘッドホン
        auto particle=[parent](int index){
            auto call=CallFunc::create([parent,index]{
                auto pos=Vec2(parent->getContentSize().width*.5, parent->getContentSize().height*.5);
                auto size=parent->getContentSize().width*.15;
                std::vector<Color4F> startColors={Color4F(1, .6, .6, 1),Color4F(.46, .95, .46, 1),Color4F::YELLOW};
                
                auto particle=ParticleSystemQuad::create("particle_music.plist");
                particle->setAutoRemoveOnFinish(true);
                particle->setPosition(pos);
                particle->setLife(3);
                particle->resetSystem();
                particle->setStartSize(size);
                particle->setEndSize(size);
                particle->setGravity(Vec2(size*1.5, -size*.7));
                particle->setStartColor(startColors.at(index));
                particle->setEndColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 0));
                
                particle->setAngle(135);
                particle->setSpeed(size*3);
                parent->addChild(particle);
            });
            
            return Sequence::create(call,DelayTime::create(1), NULL);
        };
        
        auto sound=createSoundAction("rock.mp3");
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(.5),sound,particle(0),particle(1),particle(0),particle(2),particle(1),DelayTime::create(1), NULL), UINT_MAX));
    }
    else if(key=="clear"){
        auto story=StoryLayer::create("ending", [](Ref*ref){
            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

            auto story_1=StoryLayer::create("ending1", [](Ref*ref){
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story_1);
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
}

#pragma mark - Touch
void LiveHouseActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="brush")
    {
        Common::playSE("p.mp3");
        
        auto stain=parent->getChildByName<Sprite*>("sup_16_stain.png");
        auto mark=createSpriteToCenter("sup_16_mark.png", true, parent);
        parent->addChild(mark);
        
        auto item=AnchorSprite::create(Vec2(.51, .4), parent->getContentSize(),"sup_16_brush.png");
        item->setOpacity(0);
        parent->addChild(item);

        //fadein
        auto fadein=FadeIn::create(1);
        auto sound=CallFunc::create([this]{
            Common::playSE("goshigoshi.mp3");
        });
        //move
        auto distance=parent->getContentSize().width*.05;
        auto duration=.2;
        Vector<FiniteTimeAction*>moves;
        for (int i=1; i<6; i++) {
            auto move=MoveBy::create(duration+i*.025, Vec2(distance*i*pow(-1, i)+distance*1*(i/2), distance*i*pow(-1, i)-distance*.4*(i/2)));
            moves.pushBack(move);
        }
        
        auto seq=TargetedAction::create(item, Sequence::create(fadein,sound,Sequence::create(moves), NULL));
        auto change=Spawn::create(TargetedAction::create(item, FadeOut::create(.7)),
                                  createChangeAction(1, .7, stain, mark), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq,change,DelayTime::create(.5),call,NULL));
    }
    else if(key=="screen")
    {
        auto back=createSpriteToCenter("back_2.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        SupplementsManager::getInstance()->addSupplementToMain(back,2);
        parent->addChild(back);
        
        auto screen=back->getChildByName("sup_2_screen.png");
        
        //animation
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        
        auto fadeout=Spawn::create(TargetedAction::create(screen, FadeOut::create(2)),
                                   createSoundAction("weeen.mp3"), NULL);
        
        auto fadeout_1=Spawn::create(TargetedAction::create(back, FadeOut::create(1)),
                                     createSoundAction("pinpon.mp3"), NULL);
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,DelayTime::create(.7),fadeout,DelayTime::create(1),fadeout_1,call,NULL));
    }
#pragma mark ipod
    else if(key.compare(0,4,"ipod")==0){
        auto isLeft=!(bool)atoi(key.substr(5,1).c_str());
        auto index=DataManager::sharedManager()->getTemporaryFlag("ipod").asInt();
        auto nextIndex=(int)(index+pow(-1, isLeft));
        auto disable=(nextIndex<0||nextIndex>3);
        
        Vector<FiniteTimeAction*>actions;
        
        auto before=parent->getChildByName(StringUtils::format("sup_26_%d.png",index));
        if (disable) {
            callback(true);
        }
        else{
            Common::playSE("pi.mp3");
            DataManager::sharedManager()->setTemporaryFlag("ipod", nextIndex);
            auto distance=parent->getContentSize().width*.5;
            auto afterName=StringUtils::format("sup_26_%d.png",nextIndex);
            auto after=parent->getChildByName(afterName);
            if (!after) {
                after=createSpriteToCenter(afterName, false, parent, true);
            }
            after->setPositionX(parent->getContentSize().width*.5+pow(-1, isLeft)*distance);
            
            auto move=MoveBy::create(.2, Vec2(-distance*pow(-1, isLeft), 0));
            actions.pushBack(Spawn::create(TargetedAction::create(before, move),
                                           TargetedAction::create(after, move->clone()),
                                           NULL));
            auto call=CallFunc::create([callback]{
                callback(true);
            });
            actions.pushBack(call);
            parent->runAction(Sequence::create(actions));
        }
    }
    else if(key=="coin")
    {//こいん
        Common::playSE("p.mp3");
        
        auto coin=createSpriteToCenter("anim_44_coin.png", true, parent);
        parent->addChild(coin);
        
        auto pig=parent->getChildByName<Sprite*>("sup_44_pig.png");
        auto juice=createSpriteToCenter("sup_44_juices.png", true, parent);
        parent->addChild(juice);
        
        auto coin_seq=TargetedAction::create(coin, Sequence::create(FadeIn::create(1),
                                                                    ScaleBy::create(1, .7),
                                                                    FadeOut::create(.7), NULL));
        
        Vector<FiniteTimeAction*>actions;
        auto duration=1.0f;
        for (int i=0; i<3; i++) {
            auto anim=createSpriteToCenter(StringUtils::format("anim_44_%d.png",i), true, parent);
            parent->addChild(anim);
            
            if (i==0) {
                actions.pushBack(createSoundAction("pop_1.mp3"));
                actions.pushBack(createChangeAction(duration, duration*.7, pig, anim));
            }
            else{
                if (i==1) {
                    actions.pushBack(createSoundAction("goto.mp3"));
                }
                actions.pushBack(createChangeAction(duration, duration*.7, parent->getChildByName<Sprite*>(StringUtils::format("anim_44_%d.png",i-1)), anim));
            }
            
            actions.pushBack(DelayTime::create(.5));
        }
        
        auto reset=Spawn::create(TargetedAction::create(pig, FadeIn::create(.7)),
                                 TargetedAction::create(parent->getChildByName<Sprite*>("anim_44_2.png"), FadeOut::create(1)),
                                 TargetedAction::create(juice, FadeIn::create(.5)), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(coin_seq,Sequence::create(actions),reset,DelayTime::create(1),call, NULL));
    }
    else if (key=="cloud")
    {//吹きだし
        Common::playSE("pig_1.mp3");
        auto num=DataManager::sharedManager()->getNowBack();
        auto cloud_anchor=Vec2(.628,.715);
        
        auto cloud=AnchorSprite::create(cloud_anchor, parent->getContentSize(), StringUtils::format("sup_%d_cloud.png",num));
        cloud->setOpacity(0);
        parent->addChild(cloud);
        auto original_scale=cloud->getScale();
        cloud->setScale(.1);
        
        auto seq_pig=createBounceAction(parent->getChildByName("sup_44_pig.png"), .3);
        
        auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(.5, original_scale),
                                                                                    FadeIn::create(.5), NULL),
                                                                      createSoundAction("nyu.mp3"),
                                                                      DelayTime::create(.5),
                                                                      Spawn::create(ScaleTo::create(.5, .1),
                                                                                    FadeOut::create(.5), NULL), NULL));
        
        
        auto spawn=Spawn::create(seq_cloud,seq_pig, NULL);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn,call, NULL));
    }
    else if(key=="smokin")
    {//たばこに火
        Common::playSE("p.mp3");
        
        auto sunglass=parent->getChildByName<Sprite*>("sup_12_sunglass.png");
        sunglass->setLocalZOrder(1);
        
        auto match=createSpriteToCenter("sup_12_match.png", true, parent);
        match->setPositionX(parent->getContentSize().width*.55);
        parent->addChild(match);
        
        auto back=createSpriteToCenter("back_34.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        back->setLocalZOrder(2);
        parent->addChild(back);
        
        auto sup=createSpriteToCenter("sup_34_key.png", false, parent);
        back->addChild(sup);
        auto sup_tabacco=createSpriteToCenter("sup_34_tabacco.png", false, parent);
        back->addChild(sup_tabacco);
        
        auto seq_match=TargetedAction::create(match,Sequence::create(FadeIn::create(.5),
                                                               MoveTo::create(1, parent->getContentSize()/2),
                                                               DelayTime::create(.5),
                                                               Repeat::create(jumpAction(parent->getContentSize().height*.02, .1, match, 1.5), 2),
                                                                     createSoundAction("huo.mp3"), NULL));
        
        auto add_fire=CallFunc::create([parent]{
            auto size=parent->getContentSize().width*.02;
            
            auto particle=ParticleSystemQuad::create("fire.plist");
            particle->setPosition(parent->getContentSize().width/2,parent->getContentSize().height*.45);
            particle->setDuration(1);
            particle->setAutoRemoveOnFinish(true);
            particle->setStartSize(size);
            particle->setEndSize(size*2);
            particle->setSpeed(size);
            particle->setTotalParticles(200);
            particle->setPosVar(Vec2(parent->getContentSize().width*.01, 0));
            parent->addChild(particle);
        });
        auto delay=DelayTime::create(1);
        auto fadeout_match=TargetedAction::create(match, FadeOut::create(.5));
        auto add_smoke=CallFunc::create([parent]{
            auto particle=SupplementsManager::getInstance()->getParticle(parent, DataManager::sharedManager()->getParticleVector(false, 34).at(0).asValueMap());
            particle->setDuration(5.5);
            particle->setLocalZOrder(1);
            particle->setTotalParticles(200);
            particle->setLife(5);
            particle->setSpeed(particle->getStartSize()*5);
            particle->setPositionY(parent->getContentSize().height*.45);
            parent->addChild(particle);
        });
        
        auto delay_1=DelayTime::create(5);
        
        auto move=TargetedAction::create(sunglass, MoveBy::create(.3, Vec2(0, parent->getContentSize().height*.1)));
        auto delay_2=DelayTime::create(2);
        auto change=Spawn::create(createSoundAction("pig.mp3"),
                                  TargetedAction::create(back, FadeIn::create(2)), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_match,add_fire,delay,fadeout_match,add_smoke,delay_1,move,delay_2,change,call, NULL));
    }
    else if(key=="scissors"){
        Common::playSE("chokichoki.mp3");
        
        auto listband=parent->getChildByName<Sprite*>("sup_12_listband.png");
        
        auto scissors=createSpriteToCenter("anim_12_scissors.png", true, parent);
        parent->addChild(scissors);
        
        auto fadein=TargetedAction::create(scissors, FadeIn::create(1));
        auto fadeouts=Spawn::create(TargetedAction::create(scissors, FadeOut::create(.7)),
                                    TargetedAction::create(listband, FadeOut::create(.7)), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,DelayTime::create(.5),fadeouts,call, NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void LiveHouseActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{
    if (key.compare(0,5,"stamp")==0) {
        
    }
}
