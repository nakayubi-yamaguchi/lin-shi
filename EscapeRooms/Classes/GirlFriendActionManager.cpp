//
//  SchoolFesActionManager.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2018/11/29.
//
//

#include "GirlFriendActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "GameScene.h"
#include "TouchManager.h"
#include "FlipSprite.h"

using namespace cocos2d;

GirlFriendActionManager* GirlFriendActionManager::manager =NULL;

GirlFriendActionManager* GirlFriendActionManager::getInstance()
{
    if (manager==NULL) {
        manager = new GirlFriendActionManager();
    }
        
    return manager;
}

#pragma mark - 背景
int GirlFriendActionManager::getDefaultBackNum()
{
    /*
    if(!DataManager::sharedManager()->isShowedOpeningStory()) {
        return 0;
    }*/
    
    return 1;
}

#pragma mark - Back
void GirlFriendActionManager::backAction(std::string key, int backID, Node *parent, ShowType type)
{
#pragma mark 神経衰弱カード配置
    if(key == "trump_arrange"){
        std::vector<Vec2> anchorPoints = {Vec2(.21, .69), Vec2(.498, .69), Vec2(.81, .69), Vec2(.13, .317), Vec2(.377, .317), Vec2(0.622, .317), Vec2(0.87, .317)};
        
        int i = 0;
        
        for (auto anchorPoint : anchorPoints) {
            auto fileName = StringUtils::format("sup_79_trump_%d",i);

            auto invisibleKey = "invisible_" + fileName;
            auto isInvisible = DataManager::sharedManager()->getTemporaryFlag(invisibleKey).asBool();

            if (!isInvisible) {
                auto flipSprite = FlipSprite::create(fileName, anchorPoint, parent);
                parent->addChild(flipSprite);
            }
            
            i++;
        }
    }
#pragma mark 加湿器
    else if(key == "humidification" && DataManager::sharedManager()->getEnableFlagWithFlagID(7)){
        createHumidificationParticle(parent, backID, type);
    }
#pragma mark ライトアップ
    else if(key == "lightup" && DataManager::sharedManager()->getEnableFlagWithFlagID(31) && DataManager::sharedManager()->getEnableFlagWithFlagID(32)) {
        
        auto lightAction = showLight(parent, backID, type);
        auto lightSeq = Sequence::create(lightAction, DelayTime::create(2), NULL);
        
        auto seq = Sequence::create(DelayTime::create(1), Repeat::create(lightSeq, -1), NULL);
        parent->runAction(seq);
    }
#pragma mark 絵画
    else if(key == "picture") {
        auto rotatePicture = parent->getChildByName<Sprite*>("sup_42_picture.png");
        
        bool enable49 = DataManager::sharedManager()->getEnableFlagWithFlagID(49);
        bool enable50 = DataManager::sharedManager()->getEnableFlagWithFlagID(50);
        bool enable51 = DataManager::sharedManager()->getEnableFlagWithFlagID(51);
        //Rotateに直張りしないと、補完画像が一緒に回転してくれない。
        if (enable49) {//ピースはめる
            createSpriteToCenter("sup_42_piece_1.png", false, rotatePicture, true);
        }
        
        if (enable50) {//ピースはめる
            createSpriteToCenter("sup_42_piece_2.png", false, rotatePicture, true);
        }
        
        if (enable51) {//パズル回答終了
            //上に動かしたりとか、動作あるなら。
            rotatePicture->setPositionY(rotatePicture->getPositionY()+rotatePicture->getContentSize().height*.2);
        }
    }
#pragma mark コイン
    else if(key == "coin" && !DataManager::sharedManager()->getEnableFlagWithFlagID(41)) {
        float size = 0;
        Vec2 pos = Vec2::ZERO;
        
        if (backID == 2) {
            size = parent->getContentSize().width*.03;
            pos = Vec2(parent->getContentSize().width*.773, parent->getContentSize().height*.32);
        }
        else if (backID == 20) {
            size = parent->getContentSize().width*.08;
            pos = Vec2(parent->getContentSize().width*.3, parent->getContentSize().height*.217);
        }
        else if (backID == 21) {
            size = parent->getContentSize().width*.12;
            pos = Vec2(parent->getContentSize().width*.557, parent->getContentSize().height*.358);
        }
        
        auto particleBatch = ParticleBatchNode::create("kira.png");
        particleBatch->setName("coinparticle");
        parent->addChild(particleBatch, 1);
        auto particle = ParticleSystemQuad::create("kirakira.plist");
        particle->setAutoRemoveOnFinish(true);
        particle->setPosition(pos);
        particle->setTotalParticles(12);
        particle->setStartSpinVar(10);
        particle->setLife(.5);
        particle->setLifeVar(.2);
        particle->setAngleVar(20);
        particle->setStartSize(size);
        particle->setEndColor(Color4F(0, 0, 0, .5));
        particle->setEndSize(particle->getStartSize()*1.2);
        particle->setEndSpin(15);
        particle->setEndSpinVar(15);
        particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
        
        particleBatch->addChild(particle);
    }
#pragma mark サムモグラ
    else if (key == "mogura" && !DataManager::sharedManager()->getEnableFlagWithFlagID(39)) {
        //アニメーション開始！
        Vector<FiniteTimeAction*> actions;
        
        //箱揺れまでの間隔
        actions.pushBack(DelayTime::create(1));
        
        //ボックスを揺らす
        auto box_0 = createSpriteToCenter("sup_56_box_0.png", false, parent, true);//parent->getChildByName<Sprite*>("sup_56_box_0.png");
        auto seq = Sequence::create(MoveTo::create(.1, Vec2(box_0->getPositionX()-parent->getContentSize().width*.005,box_0->getPositionY())),
                                    MoveTo::create(.1, Vec2(box_0->getPositionX()+parent->getContentSize().width*.005,box_0->getPositionY())), NULL);
        actions.pushBack(createSoundAction("bohu.mp3", type));
        actions.pushBack(Repeat::create(TargetedAction::create(box_0, seq), 4));
        actions.pushBack(DelayTime::create(.7));
        
        //ふたが開く
        auto box_1 = createSpriteToCenter("sup_56_box_1.png", true, parent, true);
        actions.pushBack(createChangeAction(.5, .5, box_0, box_1, "put_book.mp3"));
        actions.pushBack(DelayTime::create(.8));
        
        //サムがのぞく
        auto pig_0 = createSpriteToCenter("sup_56_pig_0.png", true, box_1, true);
        float hidePosY = parent->getContentSize().height*.28;
        pig_0->setPositionY(hidePosY);
        createSpriteToCenter("sup_56_cover_1.png", false, box_1, true);
        
        Vector<FiniteTimeAction*> repeatActions;
        auto enableHammer = CallFunc::create([this](){
            if (!this->enableMoguraAction()) {//コールファンクとかがstopActionで止まらない。Nodeに対してのアクションじゃないから？
                return;
            }
            
            DataManager::sharedManager()->setTemporaryFlag("enable_hammer", 1);
        });
        auto disableHammer = CallFunc::create([this](){
            if (!this->enableMoguraAction()) {//コールファンクとかがstopActionで止まらない。Nodeに対してのアクションじゃないから？
                return;
            }
            DataManager::sharedManager()->setTemporaryFlag("enable_hammer", 0);
        });
        auto nyuSound = CallFunc::create([this, type, parent](){
            log("サウンドを再生します%d",this->enableMoguraAction());

            if (!this->enableMoguraAction()) {//コールファンクとかがstopActionで止まらない。Nodeに対してのアクションじゃないから？
                return;
            }
            parent->runAction(createSoundAction("nyu.mp3", type));
        });
        
        auto pigSeq = Sequence::create(TargetedAction::create(pig_0, FadeIn::create(0)),
                                       enableHammer,
                                       nyuSound,
                                       EaseOut::create(MoveTo::create(.6, Vec2(pig_0->getPositionX(), parent->getContentSize().height*.5)), 1.5) ,
                                       DelayTime::create(.8),
                                       EaseIn::create(MoveTo::create(.6, Vec2(pig_0->getPositionX(), hidePosY)), 2),
                                       TargetedAction::create(pig_0, FadeOut::create(0)),
                                       disableHammer, NULL);
        repeatActions.pushBack(TargetedAction::create(pig_0, pigSeq));
        repeatActions.pushBack(DelayTime::create(.9));
        
        //指定回数叩かれているか確認。
        repeatActions.pushBack(CallFunc::create([this, parent, pig_0](){
            if (!this->enableMoguraAction()) {

                parent->stopActionByTag(MoguraActionTag);
            }
        }));
        
        auto runActionCall = CallFunc::create([parent, repeatActions](){
            auto sequence = Sequence::create(repeatActions);
            
            auto repeat = Repeat::create(sequence, -1);
            repeat->setTag(MoguraActionTag);
            parent->runAction(repeat);
        });
        actions.pushBack(runActionCall);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 犬吠える
    else if (key == "dogs_bow" && DataManager::sharedManager()->getEnableFlagWithFlagID(10) && DataManager::sharedManager()->getEnableFlagWithFlagID(15)) {
        
        Vector<FiniteTimeAction*> actions;
        
        //開始までの間
        actions.pushBack(DelayTime::create(.6));
        
        
        std::vector<int> answer = {1, 0, 0, 1, 0, 1};
        
        auto dog_0 = parent->getChildByName<Sprite*>("sup_35_boydog_1.png");
        transformToAnchorSprite(parent, dog_0, Vec2(.28, .37));
        auto dog_1 = parent->getChildByName<Sprite*>("sup_35_girldog_0.png");
        transformToAnchorSprite(parent, dog_1, Vec2(.685, .373));

        Vector<Sprite*> dogs = {dog_0, dog_1};
        
        std::vector<Vec2> cloudAnchorPoints = {Vec2(.263, .582), Vec2(.775, .6)};
        for (auto num : answer) {
            //グニャン→BOW！！
            actions.pushBack(createBounceAction(dogs.at(num), .2, .25));
            actions.pushBack(createSoundAction("dog.mp3", type));
            
            //クラウドbow
            Vec2 cloudAnchorPoint = cloudAnchorPoints.at(num);
            auto bowFileName = StringUtils::format("sup_35_bow_%d.png", num);
            
            auto bow = parent->getChildByName<Sprite*>(bowFileName);
            if (bow == NULL) {
                bow = createAnchorSprite(bowFileName, cloudAnchorPoint.x, cloudAnchorPoint.y, true, parent, true);
            }
            bow->setScale(.1);
            
            float duration = .5;
            auto seq_cloud=TargetedAction::create(bow, Sequence::create(Spawn::create(ScaleTo::create(duration, 1),
                                                                                      FadeIn::create(duration), NULL),
                                                                        DelayTime::create(duration),
                                                                        Spawn::create(ScaleTo::create(duration, .1),
                                                                                      FadeOut::create(duration), NULL), NULL));
            
            actions.pushBack(TargetedAction::create(bow, seq_cloud));
            actions.pushBack(DelayTime::create(.7));
        }
        
        actions.pushBack(DelayTime::create(1));

        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
}

#pragma mark - Touch
void GirlFriendActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    Vector<FiniteTimeAction*> actions;
    //コールバック
    auto call = CallFunc::create([callback]{
        callback(true);
    });

#pragma mark 吹き出し（クラウド）
    if(key.substr(0, 5) == "cloud") {
        int num = DataManager::sharedManager()->getNowBack();
        
        if (num == 35) {//犬
            int index = atoi(key.substr(6).c_str());
            std::vector<std::string> dogType = {"boydog", "girldog"};
            Vec2 charaAnchor = (index == 0)? Vec2(.297, .35) : Vec2(.685, .373);
            Vec2 cloudAnchor = (index == 0)? Vec2(.235, .542) : Vec2(.762, .582);
            auto charaFileName = StringUtils::format("sup_35_%s_0.png",dogType.at(index).c_str());
            auto cloudFileName = StringUtils::format("sup_35_cloud_%d.png",index);

            actions.pushBack(cloudBounceAction(parent, {charaFileName}, charaAnchor, cloudFileName, cloudAnchor, "dog.mp3"));
            actions.pushBack(call);
        }
        else if (num == 66) {//ピンキー
            actions.pushBack(cloudBounceAction(parent, {"sup_66_pig_0.png"}, Vec2(.5, .267), "sup_66_cloud.png", Vec2(.417, .518), "pig.mp3"));
            actions.pushBack(call);
        }
        else if (num == 73) {//サムりんご
            actions.pushBack(cloudBounceAction(parent, {"sup_73_pig_0.png"}, Vec2(.5, .27), "sup_73_cloud.png", Vec2(.392, .692), "pig.mp3"));
            actions.pushBack(call);
        }
    }
#pragma mark コルク家の下扉を開ける
    else if (key == "openhouse") {
        //正解後少し待機
        actions.pushBack(DelayTime::create(1));
        
        //back_68に遷移
        auto back_68 = createSpriteToCenter("back_68.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back_68, 68, true);
        actions.pushBack(TargetedAction::create(back_68, FadeIn::create(.8)));
        actions.pushBack(DelayTime::create(1));
        
        //正解音
        bool test = false;
        if (test) {
            actions.pushBack(correctSoundAction());
        }
        else {
            auto open = createSpriteToCenter("sup_68_0_1.png", true, back_68, true);
            auto puzzle = createSpriteToCenter("sup_68_piece.png", false, open, true);
            actions.pushBack(createSoundAction("su.mp3"));
            actions.pushBack(TargetedAction::create(open, FadeIn::create(.3)));
        }
        actions.pushBack(DelayTime::create(1));
        
        //戻る。
        actions.pushBack(TargetedAction::create(back_68, FadeOut::create(.8)));
        actions.pushBack(DelayTime::create(1.2));
        
        actions.pushBack(call);
    }
#pragma mark  ぴこぴこハンマー
    else if (key == "hammer") {
        
        auto hammer = createAnchorSprite("sup_56_hammer.png", .835, .648, true, parent, true);

        //auto pig = parent->getChildByName<Sprite*>(StringUtils::format("sup_20_pig_%d.png",index));
        //hammer->setPosition(hammerPos);
        auto star = CallFunc::create([parent, this, hammer](){
            Vec2 delta = Vec2(.3, 0);

            Common::playSE("piko.mp3");
            
            //星を表示する
            auto particle=ParticleSystemQuad::create("particle_hit.plist");
            particle->setPosition(.448*parent->getContentSize().width, .721*parent->getContentSize().height);
            particle->setSpeed(particle->getSpeed()/3);
            parent->addChild(particle);
            
            //正解カウント
            int attentionCount = DataManager::sharedManager()->getTemporaryFlag(Temporarykey_Attention).asInt();
            
            //びっくりマークを出す。
            std::vector<Vec2> anchorPoints = {Vec2(.215, .582), Vec2(.263, .655), Vec2(.328, .708)};
            Vec2 anchorPoint = anchorPoints.at(attentionCount);
            auto attention = createAnchorSprite(StringUtils::format("sup_56_attention_%d.png",attentionCount).c_str(), anchorPoint.x, anchorPoint.y, true, parent, true);
            auto original_scale = attention->getScale();
            attention->setScale(.1);
            float duration = .2;
            auto attentionAction = EaseOut::create(Spawn::create(createSoundAction("surprise.mp3"),
                                                                 ScaleTo::create(duration, original_scale),
                                                                 FadeIn::create(duration), NULL), 2);
            parent->runAction(TargetedAction::create(attention, attentionAction));
            
            //カウントアップ
            DataManager::sharedManager()->setTemporaryFlag(Temporarykey_Attention, attentionCount+1);
            
            //同じ場所で何度も叩けないように
            DataManager::sharedManager()->setTemporaryFlag(Temporarykey_Enable_Hammer, 0);
        });
        
        auto checkCompleteMogura = CallFunc::create([this, parent, callback](){
            if (this->enableMoguraAction()) {//モグラ続く
                return;
            }
        
            AnswerManager::getInstance()->enFlagIDs.push_back(39);
            AnswerManager::getInstance()->removeItemIDs.push_back(20);
            
            //ワープ
            Warp warp;
            warp.warpID=55;
            warp.replaceType=ReplaceTypeFadeIn;
            WarpManager::getInstance()->warps.push_back(warp);
            
            Vector<FiniteTimeAction*> completeActions;
            
            //ちょっと溜める
            completeActions.pushBack(DelayTime::create(1.5));
            
            //びっくりマーク消す
            Vector<FiniteTimeAction*> attentionActions;
            for (int i = 0; i < 3; i++) {
                auto attention = parent->getChildByName<Sprite*>(StringUtils::format("sup_56_attention_%d.png",i));
                attentionActions.pushBack(TargetedAction::create(attention, FadeOut::create(.1)));
            }
            completeActions.pushBack(Spawn::create(attentionActions));
            
            //ダンボール開く
            auto box_1 = parent->getChildByName<Sprite*>("sup_56_box_1.png");
            auto box_2 = createSpriteToCenter("sup_56_box_2.png", true, parent, true);
            auto pig_1 = createSpriteToCenter("sup_56_pig_1.png", true, box_2, true);
            pig_1->setPositionY((.5-.57)*parent->getContentSize().height);
            createSpriteToCenter("sup_56_cover_2.png", false, box_2, true);

            completeActions.pushBack(this->createChangeAction(.2, .2, box_1, box_2, "bohu.mp3"));
            completeActions.pushBack(TargetedAction::create(pig_1, FadeIn::create(0)));
            completeActions.pushBack(DelayTime::create(1.2));
            
            //豚ジャンプ
            float jumpDuration = 1.4;
            auto jump = MoveTo::create(jumpDuration, Vec2(pig_1->getPositionX(), parent->getContentSize().height*.75));
            auto easeoutJump = EaseOut::create(jump, 3);
            float fadeoutDuration = .9;
            auto fadeoutSeq = Sequence::create(DelayTime::create(fadeoutDuration),
                                               FadeOut::create(jumpDuration-fadeoutDuration), NULL);
            auto jumpSpawn = Spawn::create(this->createSoundAction("pig.mp3"),
                                           easeoutJump,
                                           fadeoutSeq, NULL);
            
            completeActions.pushBack(TargetedAction::create(pig_1, jumpSpawn));
            
            //back5に遷移
            auto back_5 = createSpriteToCenter("back_5.png", true, parent, true);
            SupplementsManager::getInstance()->addSupplementToMain(back_5, 5, true);
            
            auto close = back_5->getChildByName<Sprite*>("sup_5_close.png");
            close->removeFromParent();
            
            completeActions.pushBack(TargetedAction::create(back_5, FadeIn::create(.6)));
            completeActions.pushBack(DelayTime::create(.6));
            
            //豚出現
            auto sup_5_pig = createSpriteToCenter("sup_5_anim.png", true, back_5, true);
            completeActions.pushBack(TargetedAction::create(sup_5_pig, FadeIn::create(.5)));
            completeActions.pushBack(DelayTime::create(.7));
            
            //豚走り去る
            float run_away_duration = 1;
            auto jumpto_pig = JumpBy::create(run_away_duration, Vec2(-.7*parent->getContentSize().width, -.3*parent->getContentSize().height), parent->getContentSize().height*.1, 4);
            float duration_to_fadeout = .7;
            auto fadeoutSeq_pig = Sequence::create(DelayTime::create(duration_to_fadeout),
                                                   this->createSoundAction("pig.mp3"),
                                                   FadeOut::create(run_away_duration-duration_to_fadeout), NULL);
            
            auto run_away_spawn = Spawn::create(jumpto_pig, fadeoutSeq_pig, NULL);
            completeActions.pushBack(TargetedAction::create(sup_5_pig, run_away_spawn));
            completeActions.pushBack(DelayTime::create(1));
            
            /*
            //back_55に遷移
            auto back_55 = createSpriteToCenter("back_55.png", true, parent, true);
            SupplementsManager::getInstance()->addSupplementToMain(back_55, 55, true);
            completeActions.pushBack(TargetedAction::create(back_55, FadeIn::create(.6)));
            completeActions.pushBack(DelayTime::create(.7));*/
            
            completeActions.pushBack(this->correctSoundAction());
            completeActions.pushBack(CallFunc::create([callback](){//ラムダ式外で定義されているにあるアクションは使えない。
                if (callback) {
                    callback(true);
                }
            }));
            
            parent->runAction(Sequence::create(completeActions));
        });
        
        auto seq_hammer=TargetedAction::create(hammer, Sequence::create(FadeIn::create(.05),
                                                                        RotateBy::create(.2, 20),
                                                                        DelayTime::create(.05),
                                                                        Spawn::create(EaseIn::create(RotateBy::create(.15, -30), 1.3),
                                                                                      /*ScaleBy::create(.15, .8),*/ NULL),
                                                                        Spawn::create(EaseOut::create(RotateBy::create(.15, 30), 1.3),
                                                                                      /*ScaleBy::create(.15, 1.0/.8),*/ NULL),
                                                                        star,
                                                                        FadeOut::create(.1),
                                                                        checkCompleteMogura, NULL));
        actions.pushBack(seq_hammer);
        
        auto originalCall = CallFunc::create([this, callback](){
            if (this->enableMoguraAction() && callback) {
                callback(true);
                log("モグラくるので、コールバック！！！");
            }
        });
        actions.pushBack(originalCall);
    }
#pragma mark 正解後にポスターを上にずらす
    else if (key == "openPoster") {
        auto picture = parent->getChildByName<Sprite*>("sup_42_picture.png");
        actions.pushBack(createSoundAction("su.mp3"));
        actions.pushBack(TargetedAction::create(picture, EaseInOut::create(MoveBy::create(.5, Vec2(0, parent->getContentSize().height*.2)), 2)));
        actions.pushBack(DelayTime::create(.3));
        
        actions.pushBack(call);
    }
#pragma mark ハムスター解放
    else if(key=="unlock_hamster" && DataManager::sharedManager()->getEnableFlagWithFlagID(36)){
        
        auto back_25 = createSpriteToCenter("back_25.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back_25, 25, true);
        
        auto close = back_25->getChildByName<Sprite*>("sup_25_close.png");
        //扉が開いている画像はまだフラグが立っていないので、自動で貼られない。
        auto open = createSpriteToCenter("sup_25_open_0.png", true, back_25, true);
        actions.pushBack(createChangeAction(.5, .5, close, open));
        actions.pushBack(DelayTime::create(.5));
        
        actions.pushBack(TargetedAction::create(back_25, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.6));
        actions.pushBack(hamsterAction(back_25));
        
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark クリア
    else if(key=="clear"){
        
        actions.pushBack(useItemSoundAction());

        //ドアノブをはめる
        auto doornob = createSpriteToCenter("sup_9_doornob.png", true, parent, true);
        actions.pushBack(createSoundAction("kata.mp3"));
        actions.pushBack(TargetedAction::create(doornob, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.7));
        
        //ドアの引きに遷移
        auto back_19 = createSpriteToCenter("back_19.png", true, parent, true);
        actions.pushBack(TargetedAction::create(back_19, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(1));
        
        //扉開く
        auto open = createSpriteToCenter("sup_19_open.png", true, back_19, true);
        actions.pushBack(createSoundAction("gacha.mp3"));
        actions.pushBack(TargetedAction::create(open, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.5));
        
        //エンディング
        actions.pushBack(CallFunc::create([parent]{
            auto story=StoryLayer::create("ending", []{
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        }));
    }
#pragma mark - アイテム使用系
    else if (key.substr(0, 4) == "use_") {
        
        auto item = key.substr(4);
        log("アイテムをあげる%s",item.c_str());
        actions.pushBack(useItemSoundAction());
        
#pragma mark 骨あげる
        if (item == "bone") {
            //骨を置く
            auto bone = createSpriteToCenter("sup_35_bone.png", true, parent, true);
            actions.pushBack(TargetedAction::create(bone, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(.7));
            
            //犬飛び起きる
            auto boydog_0 = parent->getChildByName<Sprite*>("sup_35_boydog_0.png");
            auto boydog_1 = createAnchorSprite("sup_35_boydog_1.png", .28, .37, true, parent, true);
            actions.pushBack(createChangeAction(.5, .5, boydog_0, boydog_1));
            actions.pushBack(DelayTime::create(.8));
            
            //グニャン→BOW！！
            actions.pushBack(createBounceAction(boydog_1, .2, .25));
            actions.pushBack(createSoundAction("dog.mp3"));
            
            auto bow = createAnchorSprite("sup_35_bow_0.png", .263, .582, true, parent, true);
            auto original_scale = bow->getScale();
            bow->setScale(.1);
            float duration = .2;
            auto bowAction = EaseOut::create(Spawn::create(ScaleTo::create(duration, original_scale),
                                                           FadeIn::create(duration), NULL), 2);
            actions.pushBack(TargetedAction::create(bow, bowAction));
            
            actions.pushBack(DelayTime::create(.8));
            
            //back20に遷移
            auto back_20 = createSpriteToCenter("back_20.png", true, parent, true);
            SupplementsManager::getInstance()->addSupplementToMain(back_20, 20, true);
            actions.pushBack(TargetedAction::create(back_20, FadeIn::create(.6)));
            actions.pushBack(DelayTime::create(.7));
            
            //bow消す
            actions.pushBack(TargetedAction::create(bow, RemoveSelf::create()));
            
            //豚驚く
            auto attentionPigFileName = "sup_20_pig.png";
            auto pig_attention = back_20->getChildByName<Sprite*>(attentionPigFileName);
            
            actions.pushBack(attentionAction(back_20, attentionPigFileName, Vec2(.705, .153), "sup_20_attention.png"));
            actions.pushBack(DelayTime::create(.8));
            
            //豚切り替え
            auto pig_0 = createSpriteToCenter("sup_20_pig_0.png", true, back_20, true);
            auto dogfood = createSpriteToCenter("sup_20_dogfood.png", true, back_20, true);
            auto pig_1 = createSpriteToCenter("sup_20_pig_1.png", true, back_20, true);
            
            actions.pushBack(createChangeAction(.5, .5, pig_attention, pig_0, "pop_1.mp3"));
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(createChangeAction(.5, .5, {pig_0}, {pig_1, dogfood}, "pop_1.mp3"));
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(createSoundAction("pig.mp3"));
            actions.pushBack(TargetedAction::create(pig_1, FadeOut::create(.6)));
            actions.pushBack(DelayTime::create(1.2));
            
            actions.pushBack(TargetedAction::create(back_20, FadeOut::create(.6)));
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark ドッグフードあげる
        else if (item == "dogfood") {
            auto anim_dogfood = createAnchorSprite("sup_35_dogfood_anim.png", .695, .259, true, parent, true);
            actions.pushBack(TargetedAction::create(anim_dogfood, FadeIn::create(.6)));
            actions.pushBack(DelayTime::create(.6));
            
            //ドッグフードそそぐ
            auto angle = 20;
            actions.pushBack(createSoundAction("gasagoso.mp3"));
            actions.pushBack(TargetedAction::create(anim_dogfood, EaseInOut::create(RotateBy::create(.7, angle), 1.5)));
            
            //ドッグフード増える
            auto dogfood = createSpriteToCenter("sup_35_dogfood.png", true, parent, true);
            actions.pushBack(TargetedAction::create(dogfood, FadeIn::create(.5)));
            
            //ドッグフード戻る
            actions.pushBack(TargetedAction::create(anim_dogfood, EaseInOut::create(RotateBy::create(.7, -angle), 1.5)));
            actions.pushBack(DelayTime::create(.3));
            actions.pushBack(DelayTime::create(.5));
            
            actions.pushBack(TargetedAction::create(anim_dogfood, FadeOut::create(.5)));
            actions.pushBack(DelayTime::create(.4));
            
            //犬喜ぶ
            auto dog = parent->getChildByName<Sprite*>("sup_35_girldog_0.png");
            transformToAnchorSprite(parent, dog, Vec2(.685, .373));
            actions.pushBack(createBounceAction(dog, .3, .1));
            actions.pushBack(createSoundAction("dog.mp3"));
            
            auto bow = createAnchorSprite("sup_35_bow_1.png", .775, .6, true, parent, true);
            auto original_scale = bow->getScale();
            bow->setScale(.1);

            float duration = .5;
            auto seq_cloud=TargetedAction::create(bow, Sequence::create(Spawn::create(ScaleTo::create(duration, original_scale),
                                                                                      FadeIn::create(duration), NULL),
                                                                        DelayTime::create(duration),
                                                                        Spawn::create(ScaleTo::create(duration, .1),
                                                                                      FadeOut::create(duration), NULL), NULL));
            
            actions.pushBack(TargetedAction::create(bow, seq_cloud));
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark ペットボトルセット
        else if (item == "petbottle") {
            //ペットボトルをセット
            auto petbottle = createSpriteToCenter("sup_11_petbottle.png", true, parent, true);
            actions.pushBack(TargetedAction::create(petbottle, FadeIn::create(1)));
            actions.pushBack(DelayTime::create(.6));
            actions.pushBack(createSoundAction("drink.mp3"));
            
            actions.pushBack(DelayTime::create(.8));
            
            //煙パーティクル
            auto smoke=CallFunc::create([this, parent]{
                this->createHumidificationParticle(parent, 11, ShowType_Normal);
            });
            actions.pushBack(smoke);
            actions.pushBack(DelayTime::create(3));
            
            //加湿
            auto beforeSp = createSpriteToCenter("sup_11_before.png", true, parent, true);
            auto afterSp = createSpriteToCenter("sup_11_after.png", true, parent, true);
            actions.pushBack(createChangeAction(.5, .5, beforeSp, afterSp, "pi.mp3"));
            actions.pushBack(DelayTime::create(.7));
        }
#pragma mark 奥の部屋の鍵を開ける
        else if (item == "key") {
            auto key_0=createSpriteToCenter("sup_47_key_0.png", true, parent,true);
            auto key_1=createSpriteToCenter("sup_47_key_1.png", true, parent,true);
            
            actions.pushBack(createChangeActionSequence(1, .7, {key_0,key_1}, {"kacha.mp3","key_1.mp3"}));
        }
#pragma mark ライトセット
        else if (item.substr(0, 5) == "light") {
            int index = atoi(item.substr(6).c_str());
            auto light = createSpriteToCenter(StringUtils::format("sup_61_%s.png", item.c_str()), true, parent, true);
            
            actions.pushBack(TargetedAction::create(light, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(.8));
        }
#pragma mark パズルピースをはめる
        else if (item.substr(0, 5) == "piece") {
            auto rotatePicture = parent->getChildByName<Sprite*>("sup_42_picture.png");
            auto piece = createSpriteToCenter(StringUtils::format("sup_42_%s.png", item.c_str()), true, rotatePicture, true);
            
            actions.pushBack(putAction(piece, "koto.mp3", .5, .3));
            actions.pushBack(DelayTime::create(.8));
        }
#pragma mark ドアノブをはめる
        else if (item == "doornob") {
            auto doornob = createSpriteToCenter("sup_9_doornob.png", true, parent, true);
            
            actions.pushBack(putAction(doornob, "koto.mp3", .5, .3));
            actions.pushBack(DelayTime::create(.8));
        }
#pragma mark ペンを使用
        else if (item == "pen") {
            auto item = createAnchorSprite("sup_54_pen.png", .193, .444, true, parent, true);
            
            //fadein
            actions.pushBack(TargetedAction::create(item, FadeIn::create(1)));
            actions.pushBack(createSoundAction("pen.mp3"));
           
            //move
            std::vector<Vec2> movePoses = {Vec2(.347, .636), Vec2(.347, .352), Vec2(.483, .636), Vec2(.483, .352), Vec2(.719, .636), Vec2(.719, .352), Vec2(.855, .636), Vec2(.855, .352)};
            auto distance = parent->getContentSize().width*.1;
            auto duration = .2;
            
            Vector<FiniteTimeAction*> moves;
            for (auto movePos : movePoses) {
                auto move = MoveTo::create(.3, Vec2(parent->getContentSize().width*movePos.x, parent->getContentSize().height*movePos.y));
                moves.pushBack(move);
            }
            auto moveSeq = Sequence::create(moves);
            actions.pushBack(TargetedAction::create(item, moveSeq));
            
            auto mark = createSpriteToCenter("sup_54_mark.png", true, parent, true);
            actions.pushBack(createChangeAction(.7, .5, item, mark));
            actions.pushBack(DelayTime::create(.5));
        }
#pragma mark ストラップをあげる
        else if (item == "strap") {
            auto strap = createSpriteToCenter("sup_66_strap.png", true, parent, true);
            actions.pushBack(giveAction(strap, 1, .8, true));

            //ピンキー後ろ向く
            auto pig_0 = parent->getChildByName<Sprite*>("sup_66_pig_0.png");
            auto pig_1 = createAnchorSprite("sup_66_pig_1.png", .5, .455, true, parent, true);
            pig_1->setLocalZOrder(1);//ストラップをよりも手前に。
            float originY = pig_1->getPositionY();//ジャンプした高さ
            pig_1->setPositionY(parent->getContentSize().height*.267);
            // createSpriteToCenter("sup_66_pig_1.png", true, parent, true);
            actions.pushBack(createChangeAction(.5, .5, pig_0, pig_1, "pop_4.mp3"));
            actions.pushBack(DelayTime::create(.6));
            
            //ジャンプ
            Vector<FiniteTimeAction*> decoActions;
            auto bounce = createBounceAction(pig_1, .3, .1);
            auto jumpTo = EaseOut::create(MoveTo::create(.6, Vec2(pig_1->getPositionX(), originY)), 2);
            auto strap_1 = createSpriteToCenter("sup_66_strap_1.png", true, parent, true);
            auto fadein_deco = TargetedAction::create(strap_1, FadeIn::create(.05));
            auto jumpBack = EaseIn::create(MoveTo::create(.4, pig_1->getPosition()), 2);

            decoActions.pushBack(bounce);
            decoActions.pushBack(createSoundAction("jump.mp3"));
            decoActions.pushBack(jumpTo);
            decoActions.pushBack(createSoundAction("basket.mp3"));
            decoActions.pushBack(fadein_deco);
            decoActions.pushBack(jumpBack);
            auto jumpSeq = Sequence::create(decoActions);
            actions.pushBack(TargetedAction::create(pig_1, jumpSeq));
            actions.pushBack(DelayTime::create(.7));
            
            //豚も正面向く
            actions.pushBack(createChangeAction(.6, .6, pig_1, pig_0, "pig.mp3"));
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark タネをあげる
        else if (item == "seed") {
            
            auto anim_seed = createAnchorSprite("sup_25_seed_0.png", .888, .494, true, parent, true);
            actions.pushBack(TargetedAction::create(anim_seed, FadeIn::create(.6)));
            actions.pushBack(DelayTime::create(.6));
            
            //タネそそぐ
            auto angle = 30;
            actions.pushBack(TargetedAction::create(anim_seed, EaseInOut::create(RotateBy::create(.6, angle), 1.5)));
            
            //タネ入れ戻る
            actions.pushBack(createSoundAction("gasagoso.mp3"));
            actions.pushBack(TargetedAction::create(anim_seed, EaseInOut::create(RotateBy::create(.3, -angle), 1.5)));
            actions.pushBack(DelayTime::create(.3));
            
            //タネもられる
            auto seed = createSpriteToCenter("sup_25_seed.png", true, parent, true);
            actions.pushBack(TargetedAction::create(seed, FadeIn::create(.5)));
            
            actions.pushBack(TargetedAction::create(anim_seed, FadeOut::create(.5)));
            actions.pushBack(DelayTime::create(.4));
            
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(33)) {
                actions.pushBack(hamsterAction(parent));
            }
        }
#pragma mark スコップで掘る
        else if (item == "scoop") {
            auto scoop = createAnchorSprite("sup_21_scoop.png", .878, .655, true, parent, true);
            auto cover = createSpriteToCenter("sup_21_cover.png", false, parent, true);
            
            actions.pushBack(TargetedAction::create(scoop, FadeIn::create(.5)));
            actions.pushBack(DelayTime::create(.4));
            
            //animate
            auto distance = parent->getContentSize().width*.15;
            auto angle = 5;
            auto duration_dig = .25;
            auto first = Sequence::create(RotateBy::create(.2, -angle),
                                          MoveBy::create(.5, Vec2(distance/3, distance/3)),
                                          FadeIn::create(.5), NULL);
            
            auto dig = Repeat::create(Sequence::create(EaseIn::create(Spawn::create(MoveBy::create(duration_dig, Vec2(-distance/2, -distance)),
                                                                                    RotateBy::create(duration_dig, angle), NULL),1.5),
                                                       CallFunc::create([]{Common::playSE("dig.mp3");}),DelayTime::create(.3),
                                                       EaseOut::create(Spawn::create(MoveBy::create(duration_dig, Vec2(distance/2, distance)),
                                                                                     RotateBy::create(duration_dig, -angle), NULL), 1.5), NULL), 3);
            
            auto fadeout = FadeOut::create(.5);
            auto seq_scop = TargetedAction::create(scoop, Sequence::create(first,dig,fadeout, NULL));
            
            actions.pushBack(seq_scop);
            
        }
        
#pragma mark コインをあげる
        else if (item == "coin") {
            //コインを使用
            auto coin=createSpriteToCenter("sup_71_coin.png", true, parent,true);
            actions.pushBack(TargetedAction::create(coin, FadeIn::create(.7)));
            actions.pushBack(DelayTime::create(.7));
            actions.pushBack(createSoundAction("chalin.mp3"));
            actions.pushBack(TargetedAction::create(coin, FadeOut::create(.7)));
            actions.pushBack(DelayTime::create(1));

            if (DataManager::sharedManager()->getEnableFlagWithFlagID(42) || DataManager::sharedManager()->getEnableFlagWithFlagID(43)) {//どちらか達成していたら。
                AnswerManager::getInstance()->getItemIDs.push_back(14);

                //コルク落下
                auto clip = createClippingNodeToCenter("sup_71_clip.png", parent);
                auto corkItem = createSpriteToCenter("sup_71_gachacork.png", false, parent);
                corkItem->setPositionY(parent->getContentSize().height*.65);
                clip->addChild(corkItem);
                
                auto seq_item=TargetedAction::create(corkItem, Sequence::create(Common::createSoundAction("koto.mp3"),
                                                                                EaseIn::create(MoveTo::create(.2, parent->getContentSize()/2), 2) ,
                                                                                JumpTo::create(.2, parent->getContentSize()/2, parent->getContentSize().height*.01, 2),
                                                                                DelayTime::create(.5),
                                                                                FadeOut::create(.7),NULL));
                actions.pushBack(TargetedAction::create(corkItem, seq_item));
                actions.pushBack(DelayTime::create(1));
            }
        }
#pragma mark コルク人形をセット
        else if (item == "cork") {
            auto cork_blue = createSpriteToCenter("sup_68_bluecork.png", true, parent, true);
            auto cork_red = parent->getChildByName<Sprite*>("sup_68_redcork.png");
            
            //コルク青をセット
            auto stage = parent->getChildByName<Sprite*>("sup_68_stage.png");
            actions.pushBack(createChangeAction(.5, .5, stage, cork_blue)/*TargetedAction::create(cork_blue, FadeIn::create(.7))*/);
            actions.pushBack(DelayTime::create(.7));
            
            //オルゴール
            actions.pushBack(createSoundAction("music_box.mp3"));

            //コルク
            auto cork_0 = createSpriteToCenter("sup_68_cork_0.png", true, parent, true);
            auto cork_1 = createSpriteToCenter("sup_68_cork_1.png", true, parent, true);
            auto cork_2 = createSpriteToCenter("sup_68_cork_2.png", true, parent, true);
            auto cork_3 = createSpriteToCenter("sup_68_cork_3.png", true, parent, true);
            
            actions.pushBack(createChangeAction(.5, .5, {cork_red, cork_blue}, {cork_0}));
            actions.pushBack(DelayTime::create(.6));
            actions.pushBack(createChangeAction(.5, .5, cork_0, cork_1));
            actions.pushBack(DelayTime::create(.6));
            actions.pushBack(createChangeAction(.5, .5, cork_1, cork_2));
            actions.pushBack(DelayTime::create(.6));
            actions.pushBack(createChangeAction(.5, .5, cork_2, cork_3));
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(TargetedAction::create(cork_3, FadeOut::create(.5)));
            actions.pushBack(DelayTime::create(.5));
            
            //sup_68_open
            auto open = createSpriteToCenter("sup_68_open.png", true, parent, true);
            actions.pushBack(createSoundAction("gacha.mp3"));
            actions.pushBack(TargetedAction::create(open, FadeIn::create(.6)));
            actions.pushBack(DelayTime::create(1));
        }
#pragma mark りんごをサムにあげる
        else if (item == "apple") {
            //りんごあげる
            auto apple = createSpriteToCenter("sup_73_apple.png", true, parent, true);
            actions.pushBack(giveAction(apple, .6, .8, true));
            actions.pushBack(DelayTime::create(.2));
            
            //りんご咥える
            Vec2 anchorPoint = Vec2(.5, .276);
            auto pig_0 = parent->getChildByName<Sprite*>("sup_73_pig_0.png");
            transformToAnchorSprite(parent, pig_0, anchorPoint);
            
            auto pig_1 = createAnchorSprite("sup_73_pig_1.png", anchorPoint.x, anchorPoint.y, true, parent, true);
            actions.pushBack(createSoundAction("paku.mp3"));
            actions.pushBack(createChangeAction(.4, .4, pig_0, pig_1));
             actions.pushBack(DelayTime::create(.5));
            
            //おいしぃぃぃー！
            actions.pushBack(createSoundAction("pig.mp3"));
            auto repeatBounce = Repeat::create(createBounceAction(pig_1, .2, .1), 4);
            actions.pushBack(repeatBounce);
            actions.pushBack(DelayTime::create(.5));
            
            //飲み込むと同時にりんご無くす
            float eatDuration = .5;
            auto eatSpawn = Spawn::create(createChangeAction(eatDuration, eatDuration, pig_1, pig_0, "drink.mp3"),
                                          createBounceAction(pig_0, eatDuration, .3),
                                          createBounceAction(pig_1, eatDuration, .3) , NULL);
            actions.pushBack(eatSpawn);
            actions.pushBack(DelayTime::create(.6));
            
            //パズルに手をかける
            auto pig_2 = createSpriteToCenter("sup_73_pig_2.png", true, parent, true);
            actions.pushBack(createChangeAction(.3, .3, pig_0, pig_2));
            actions.pushBack(swingAction(Vec2(parent->getContentSize().width*.03, 0), .3, pig_2, 1.5, false, 5));
            actions.pushBack(DelayTime::create(.6));
            
            //パズルをくれる
            auto pig_3 = createSpriteToCenter("sup_73_pig_3.png", true, parent, true);
            actions.pushBack(createChangeAction(0, .0, pig_2, pig_3, "poku.mp3"));
            actions.pushBack(DelayTime::create(1.5));
        }
        
        //完了
        actions.pushBack(correctSoundAction());
        actions.pushBack(call);
    }
#pragma mark - 指定無し
    else{
        actions.pushBack(call);
    }
    parent->runAction(Sequence::create(actions));
}

void GirlFriendActionManager::showAnswerAction(std::string key, Node *parent, ShowType showType)
{
    
}

#pragma mark - touch began
void GirlFriendActionManager::touchBeganAction(std::string key, Node *parent)
{
}


#pragma mark - Drag
void GirlFriendActionManager::dragAction(std::string key, Node *parent, Vec2 pos)
{
    
}

#pragma mark - Item
void GirlFriendActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造 addする際はcreateSpriteOnClipを使用するとよい
    Vector<FiniteTimeAction*>actions;
    log("アイテムアクション発動");
#pragma mark スイッチ
    if (key=="bearbutton") {//スイッチを押す
        log("スイッチボタンのアクション発動");
        auto switch_on = createSpriteOnClip("switch_on.png", true, parent, true);
        actions.pushBack(TargetedAction::create(switch_on, FadeIn::create(.5)));
        actions.pushBack(createSoundAction("kacha.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        
        auto back_4 = createSpriteToCenter("back_4.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back_4, 4, true);
        actions.pushBack(TargetedAction::create(back_4, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.8));
        
        auto bear_1 = back_4->getChildByName<Sprite*>("sup_4_bear_1.png");
        auto bear_2 = createSpriteToCenter("sup_4_bear_2.png", true, back_4, true);
        auto bear_3 = createSpriteToCenter("sup_4_bear_3.png", true, back_4, true);
        
        actions.pushBack(createChangeAction(.7, .7, bear_1, bear_2, "gigigi.mp3"));
        actions.pushBack(DelayTime::create(.7));
        actions.pushBack(createChangeAction(.7, .7, bear_2, bear_3, "gigigi.mp3"));
        actions.pushBack(createSoundAction("bohu.mp3"));

        actions.pushBack(DelayTime::create(1));
        
        actions.pushBack(TargetedAction::create(back_4, FadeOut::create(.5)));
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(correctSoundAction());
    }
#pragma mark ドッグフードにハサミ
    else if (key == "dogfood") {
        actions.pushBack(useItemSoundAction());
        
        auto scissors=createAnchorSprite("dogfood_scissors.png", .228, .777, true, parent->itemImage, true);
        
        //seq
        actions.pushBack(TargetedAction::create(scissors, FadeIn::create(1)));
        actions.pushBack(createSoundAction("chokichoki.mp3"));
        actions.pushBack(swingAction(4, .3, scissors, 1, true, 2));
        actions.pushBack(DelayTime::create(.5));
    }
#pragma mark ひまわりの種にハサミ
    else if (key == "seed") {
        actions.pushBack(useItemSoundAction());
        
        auto scissors=createAnchorSprite("seed_scissors.png", .258, .745, true, parent->itemImage, true);
        
        //seq
        actions.pushBack(TargetedAction::create(scissors, FadeIn::create(1)));
        actions.pushBack(createSoundAction("chokichoki.mp3"));
        actions.pushBack(swingAction(4, .3, scissors, 1, true, 2));
        actions.pushBack(DelayTime::create(.5));
    }
    
    //call
    auto call=CallFunc::create([callback](){
        callback(true);
    });
    actions.pushBack(call);
    
    parent->runAction(Sequence::create(actions));
}

#pragma mark - Custom
void GirlFriendActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    
}

#pragma mark - Private
void GirlFriendActionManager::createHumidificationParticle(cocos2d::Node *parent, int backID, ShowType type)
{
    std::vector<float> nosePosXs = {.383, .408};
    float nosePosY = .358;
    float sizeRatio = .007;
    
    if (backID == 1) {
        nosePosXs = {.545, .550};
        nosePosY = .588;
        sizeRatio = .002;
    }
    else if (backID == 10) {
        nosePosXs = {.197, .207};
        nosePosY = .717;
        sizeRatio = .004;
    }
    
    for (int i = 0; i<2; i++) {
        auto size=parent->getContentSize().width*sizeRatio;
        auto particle=ParticleSystemQuad::create("smoke.plist");
        particle->setAutoRemoveOnFinish(true);
        particle->setTotalParticles(10);
        particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
        particle->setPosition(parent->getContentSize().width*nosePosXs.at(i),parent->getContentSize().height*nosePosY);
        particle->setPosVar(Vec2(size*.1, size*.05));
        
        particle->setLife(2.5);
        particle->setDuration(/*.45*/-1);
        particle->setStartSizeVar(size*.1);
        particle->resetSystem();
        particle->setStartSize(size);
        particle->setEndSize(size*20);
        particle->setStartColor(Color4F(1, 1, 1, .4));
        particle->setSpeed(size*18);
        particle->setGravity(Vec2(size*4*pow(-1, i), size*20));
        particle->setEndSpin(30);
        particle->setEndSpinVar(30);
        particle->setAngle(235+70*i);
        particle->setAngleVar(2);
        parent->addChild(particle);
    }
    
    if (backID == 11) {
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(1.5));
        actions.pushBack(createSoundAction("pop.mp3", type));
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
}

FiniteTimeAction* GirlFriendActionManager::showLight(Node *parent, int backID, ShowType type)
{
    //番号定義は行順
    std::vector<int> answer = {0, 4, 3, 5, 7, 6, 1, 2};
    Vector<FiniteTimeAction*> actions;
    for (int num : answer) {
        std::string fileName = StringUtils::format("sup_%d_lightup_%d.png", backID, num);
        auto lightUpSp = createSpriteToCenter(fileName, true, parent, true);
        
        if (backID == 61) {//化粧台アップ
            actions.pushBack(createSoundAction("pikon.mp3", type));
        }
        actions.pushBack(TargetedAction::create(lightUpSp, FadeIn::create(.6)));
        actions.pushBack(DelayTime::create(.3));
        actions.pushBack(TargetedAction::create(lightUpSp, FadeOut::create(.6)));
        actions.pushBack(DelayTime::create(.5));
    }
    
    return Sequence::create(actions);
}

bool GirlFriendActionManager::enableMoguraAction()
{
    int attentionCount = DataManager::sharedManager()->getTemporaryFlag(Temporarykey_Attention).asInt();
    if (attentionCount == 3) {
        
        return false;
    }
    
    return true;
}

FiniteTimeAction* GirlFriendActionManager::hamsterAction(Node* parent)
{
    AnswerManager::getInstance()->enFlagIDs.push_back(36.1);
    
    Vector<FiniteTimeAction*> actions;
    //げっ歯類アニメ
    auto open_0 = parent->getChildByName<Sprite*>("sup_25_open_0.png");
    auto coin = createSpriteToCenter("sup_25_coin.png", true, parent, true);
    auto footprint_0 = createSpriteToCenter("sup_25_footprint_0.png", true, parent, true);
    auto footprint_1 = createSpriteToCenter("sup_25_footprint_1.png", true, parent, true);
    auto footprint_2 = createSpriteToCenter("sup_25_footprint_2.png", true, parent, true);
    auto footprint_3 = createSpriteToCenter("sup_25_footprint_3.png", true, parent, true);
    auto footprint_4 = createSpriteToCenter("sup_25_footprint_4.png", true, parent, true);
    auto ham_0 = createAnchorSprite("sup_25_hamster_0.png", .25, .258, true, parent, true);
    
    actions.pushBack(createChangeAction(.5, .5, {open_0}, {ham_0, coin, footprint_0}, "squirre.mp3"));
    actions.pushBack(DelayTime::create(.5));
    
    //大きくなりながら、移動
    float move_duration = 1.2;
    auto scale = ScaleTo::create(move_duration, 1.1);
    auto jumpTo = JumpTo::create(move_duration, Vec2(parent->getContentSize().width*.19, parent->getContentSize().height*.18), parent->getContentSize().height*.05, 5);
    float seq_duration_1 = .6;
    auto seq_1 = Sequence::create(DelayTime::create(seq_duration_1), TargetedAction::create(footprint_1, FadeIn::create(.1)), NULL);
    
    auto moveSpawn_1 = Spawn::create(scale, jumpTo, seq_1, NULL);
    actions.pushBack(TargetedAction::create(ham_0, moveSpawn_1));
    actions.pushBack(DelayTime::create(.5));
    
    //ハムスター向き変え
    auto ham_1 = createAnchorSprite("sup_25_hamster_1.png", .19, .167, true, parent, true);
    actions.pushBack(createChangeAction(.5, .5, {ham_0}, {ham_1, footprint_2}, "squirre.mp3"));
    actions.pushBack(DelayTime::create(.5));
    
    //走っていく
    auto jumpTo_2 = JumpTo::create(move_duration, Vec2(parent->getContentSize().width*.5, ham_1->getPositionY()), parent->getContentSize().height*.05, 5);
    
    float seq_duration_2 = .35;
    auto seq_2 = Sequence::create(DelayTime::create(seq_duration_2),
                                  TargetedAction::create(footprint_3, FadeIn::create(.1)),
                                  DelayTime::create(seq_duration_2),
                                  TargetedAction::create(footprint_4, FadeIn::create(.1)), NULL);
    
    auto moveSpawn_2 = Spawn::create(TargetedAction::create(ham_1, jumpTo_2), seq_2, NULL);
    actions.pushBack(moveSpawn_2);
    actions.pushBack(DelayTime::create(.4));
    
    //ハムスター正面向く
    auto ham_2 = createSpriteToCenter("sup_25_hamster_2.png", true, parent, true);
    actions.pushBack(createChangeAction(.5, .5, ham_1, ham_2, "squirre.mp3"));
    actions.pushBack(DelayTime::create(1));
    
    return Sequence::create(actions);
}
