//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "CandyHouseActionManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"

using namespace cocos2d;

CandyHouseActionManager* CandyHouseActionManager::manager =NULL;

CandyHouseActionManager* CandyHouseActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new CandyHouseActionManager();
    }
    return manager;
}

#pragma mark - Back
void CandyHouseActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="candy")
    {
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(18)) {
            auto candy=parent->getChildByName(StringUtils::format("sup_%d_candy.png",backID));
            auto rotate=TargetedAction::create(candy,Repeat::create(RotateBy::create(6, 360), UINT_MAX));
            parent->runAction(rotate);
        }
        
        if (DataManager::sharedManager()->isPlayingMinigame()&&backID==1) {
            auto open=parent->getChildByName("sup_1_barrelopen.png");
            open->setLocalZOrder(1);
        }
    }
    else if(key=="monkeycheck")
    {//ボルダリング側 猿の位置確認
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(29)&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(30)) {
            if (backID==2) {
                auto monkeys=createSpriteToCenter("sup_2_monkeys.png", false, parent);
                parent->addChild(monkeys);
            }
        }
        else{
            if (backID==2||backID==16) {
                auto monkey=createSpriteToCenter(StringUtils::format("sup_%d_greenmonkey.png",backID), false, parent);
                parent->addChild(monkey);
            }
            
            if (backID!=16) {
                auto monkey_1=createSpriteToCenter(StringUtils::format("sup_%d_redmonkey.png",backID), false, parent);
                parent->addChild(monkey_1);
            }
        }
    }
    else if(key=="back_44"){
        //さるたち
        //heart particle
        auto heart=[parent](bool isLeft){
            auto call=CallFunc::create([parent,isLeft]{
                Common::playSE("pop.mp3");
                std::vector<Vec2>vecs={Vec2(parent->getContentSize().width*.55,parent->getContentSize().height* .7),Vec2(parent->getContentSize().width*.45,parent->getContentSize().height* .7)};
                
                auto size=parent->getContentSize().width*.1;
                
                auto particle=ParticleSystemQuad::create("particle_heart.plist");
                particle->setAutoRemoveOnFinish(true);
                particle->setPosition(vecs.at(isLeft));
                particle->setDuration(.5);
                particle->setLife(1.75);
                particle->resetSystem();
                particle->setStartSize(size);
                particle->setStartSizeVar(0);
                particle->setEndSize(size*.5);
                particle->setEndSizeVar(0);
                particle->setGravity(Vec2(-pow(-1, isLeft)*size*2, -size*1.5));
                particle->setStartColor(Color4F(Common::getColorFromHex(DataManager::sharedManager()->getColorCodeData("red"))));
                particle->setEndColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 0));
                particle->setAngle(45+90*isLeft);
                particle->setSpeed(size*3.5);
                parent->addChild(particle);
            });
            
            return call;
        };
        
        auto left=Sequence::create(heart(true),DelayTime::create(2), NULL);
        auto right=Sequence::create(heart(false),DelayTime::create(2), NULL);
        
        parent->runAction(Repeat::create(Sequence::create(DelayTime::create(1),left,right,left->clone(),left->clone(),right->clone(),right->clone(),DelayTime::create(1), NULL), UINT_MAX));
    }
    else if(key=="clear"&&!DataManager::sharedManager()->isPlayingMinigame()){
        auto story=StoryLayer::create("ending1", [](Ref*ref){
            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

            auto story_1=StoryLayer::create("ending2", [](Ref*ref){
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story_1);
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
    else if(key=="mouse"&&DataManager::sharedManager()->isPlayingMinigame()){
        auto mouse=parent->getChildByName("sup_31_mistake.png");
        mouse->setPositionX(parent->getContentSize().width*.65);
        
        parent->runAction(Sequence::create(DelayTime::create(1),
                                           createSoundAction("nyu.mp3"),
                                           TargetedAction::create(mouse, JumpTo::create(1, parent->getContentSize()/2, parent->getContentSize().height*.03, 3)),
                                           CallFunc::create([]{DataManager::sharedManager()->setTemporaryFlag("mouse", 1);}), NULL));
    }
    else if(key=="table"&&DataManager::sharedManager()->isPlayingMinigame()){
        auto mistake=parent->getChildByName("sup_30_mistake_1.png");
        mistake->setOpacity(0);
        
        auto leg=createSpriteToCenter("sup_30_mistake_0.png", false, parent);
        parent->addChild(leg);
    }
    else if(key=="window"&&DataManager::sharedManager()->isPlayingMinigame()){
        auto window=parent->getChildByName("sup_8_barrelopen.png");
        window->setLocalZOrder(1);
    }
}

#pragma mark - Touch
void CandyHouseActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="stopcandy")
    {//キャンディ特有
        auto back_5=Sprite::createWithSpriteFrameName("back_5.png");
        back_5->setPosition(parent->getContentSize()/2);
        back_5->setOpacity(0);
        back_5->setCascadeOpacityEnabled(true);
        parent->addChild(back_5);
        
        auto candy=Sprite::createWithSpriteFrameName("sup_5_candy.png");
        candy->setAnchorPoint(Vec2(.5,.48));
        candy->setPosition(parent->getContentSize().width/2,parent->getContentSize().height*.48);
        back_5->addChild(candy);
        
        auto cover=Sprite::createWithSpriteFrameName("sup_5_cover.png");
        cover->setPosition(parent->getContentSize()/2);
        back_5->addChild(cover);
        
        //animation
        auto rotate_0=TargetedAction::create(candy, RotateBy::create(3, 180));
        auto fadein=TargetedAction::create(back_5, FadeIn::create(1));
        auto spawn_0=Spawn::create(rotate_0,fadein,NULL);
        
        auto rotate_1=TargetedAction::create(candy, EaseOut::create(RotateBy::create(4, 180),1.1));
        auto sound=createSoundAction("kacha.mp3");
        auto delay=DelayTime::create(.7);
        auto call=CallFunc::create([this,callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(spawn_0,rotate_1,sound,delay,call,NULL));
    }
    else if(key=="kappa")
    {
        Common::playSE("p.mp3");
        
        auto item=createSpriteToCenter("sup_50_kappa.png", true, parent);
        item->setPositionY(parent->getContentSize().height*.6);
        parent->addChild(item);
        
        auto candy_0=parent->getChildByName<Sprite*>("sup_50_candy_0.png");
        auto candy_1=createSpriteToCenter("sup_50_candy_1.png", true, parent);
        parent->addChild(candy_1);
        
        //animation
        auto fadein=TargetedAction::create(item, FadeIn::create(1));
        auto delay=DelayTime::create(.5);
        auto move=TargetedAction::create(item, Sequence::create(MoveTo::create(1, parent->getContentSize()/2),
                                                                createSoundAction("kacha.mp3"), NULL));
        auto delay_1=DelayTime::create(1);
        auto change=Spawn::create(createChangeAction(1.5, .7, candy_0, candy_1),
                                  createSoundAction("pop.mp3"), NULL);

        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,move,delay_1,change,delay_1->clone(),call,NULL));
    }
    else if(key=="driver")
    {
        Common::playSE("p.mp3");
        
        auto driver=Sprite::createWithSpriteFrameName("sup_41_driver.png");
        driver->setPosition(parent->getContentSize()/2);
        driver->setLocalZOrder(3);
        driver->setOpacity(0);
        parent->addChild(driver);
        
        auto screw_0=parent->getChildByName("sup_41_screw_1.png");
        auto screw_1=parent->getChildByName("sup_41_screw_0.png");
        
        auto distance=parent->getContentSize().width*.01;
        auto fadein= FadeIn::create(.7);
        auto shake=Repeat::create(Sequence::create(MoveBy::create(.1,Vec2(distance, distance)),
                                                   CallFunc::create([]{Common::playSE("kacha_1.mp3");}),
                                                   MoveBy::create(.1,Vec2(-distance, -distance)), NULL),3);
        
        auto seq_0=TargetedAction::create(driver, Sequence::create(fadein,shake, NULL));
        auto remove_screw_0=TargetedAction::create(screw_1, RemoveSelf::create());
        
        auto move=MoveBy::create(1, Vec2(parent->getContentSize().width*.27, parent->getContentSize().height*.135));
        auto seq_1=TargetedAction::create(driver, Sequence::create(move,shake->clone(), NULL));
        auto remove_screw_1=TargetedAction::create(screw_0, RemoveSelf::create());
        
        auto spawn=Spawn::create(TargetedAction::create(driver, FadeOut::create(.5)), NULL);
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_0,remove_screw_0,seq_1,remove_screw_1,DelayTime::create(.5),spawn,DelayTime::create(.3),call, NULL));
    }
    else if(key=="coffeecup"){
        auto back=createSpriteToCenter("back_51.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        SupplementsManager::getInstance()->addSupplementToMain(back, 51);
        
        auto door=back->getChildByName<Sprite*>("sup_51_door_0.png");
        auto door_after=createSpriteToCenter("sup_51_door_1.png", true, parent);
        back->addChild(door_after);

        auto fadein=TargetedAction::create(back, FadeIn::create(1.5));
        auto delay=DelayTime::create(1);
        auto change=Spawn::create(createChangeAction(1, .5, door, door_after),
                                  createSoundAction("gacha.mp3"), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,change,delay->clone(),call, NULL));
    }
    else if(key=="pig")
    {//キャンディ特有 吹き出しを出す
        int index=0;
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(23)) {
            index=1;
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(24)) {
                index=2;
                if (DataManager::sharedManager()->getEnableFlagWithFlagID(25)) {
                    return;
                }
            }
        }

        Common::playSE("pig_1.mp3");
        
        auto cloud=Sprite::createWithSpriteFrameName(StringUtils::format("cloud_%d.png",index));
        cloud->setAnchorPoint(Vec2(.63, .71));
        cloud->setPosition(parent->getContentSize().width*.63,parent->getContentSize().height*.71);
        cloud->setOpacity(0);
        parent->addChild(cloud);
        
        auto arm_left=parent->getChildByName("arm_left.png");
        auto arm_right=parent->getChildByName("arm_right.png");
        
        //anim
        auto scale_0=ScaleTo::create(0, .1);
        auto scale_1=ScaleTo::create(.25, 1);
        auto fadein=FadeIn::create(.25);
        auto spawn_0=Spawn::create(scale_1,fadein, NULL);
        auto delay_0=DelayTime::create(1);
        auto scale_2=ScaleTo::create(.25, .1);
        auto fadeout=FadeOut::create(.25);
        auto spawn_1=Spawn::create(scale_2,fadeout,NULL);
        auto remove=RemoveSelf::create();
        auto seq_cloud=TargetedAction::create(cloud,Sequence::create(scale_0,spawn_0,delay_0,spawn_1,remove, NULL));
        
        auto angle=20;
        auto rotate_0=TargetedAction::create(arm_left,EaseInOut::create(RotateTo::create(.2, angle),1.2));
        auto rotate_1=TargetedAction::create(arm_right,EaseInOut::create(RotateTo::create(.2, angle),1.2));
        auto rotate_spawn_0=Spawn::create(rotate_0,rotate_1, NULL);
        
        auto rotate_2=TargetedAction::create(arm_left,EaseInOut::create(RotateTo::create(.2, -angle),1.2));
        auto rotate_3=TargetedAction::create(arm_right,EaseInOut::create(RotateTo::create(.2, -angle),1.2));
        auto rotate_spawn_1=Spawn::create(rotate_2,rotate_3, NULL);
        
        auto rotate_4=TargetedAction::create(arm_left,EaseInOut::create(RotateTo::create(.2, 0),1.2));
        auto rotate_5=TargetedAction::create(arm_right,EaseInOut::create(RotateTo::create(.2, 0),1.2));
        auto rotate_spawn_2=Spawn::create(rotate_4,rotate_5, NULL);
        
        auto rotate_seq=Sequence::create(rotate_spawn_0,rotate_spawn_1,rotate_spawn_0->clone(),rotate_spawn_1->clone(),DelayTime::create(.2),rotate_spawn_2,NULL);
        
        auto call=CallFunc::create([this,callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(Spawn::create(seq_cloud,rotate_seq, NULL),call, NULL));
    }
    else if (key.compare(0,5,"jelly")==0)
    {//ゼリーをはめる
        auto index=atoi(key.substr(6,1).c_str());
        
        Vector<FiniteTimeAction*>actions;

        auto jelly=createSpriteToCenter(StringUtils::format("sup_16_jerry_%d.png",index), true, parent);
        parent->addChild(jelly);
        
        auto fadein=Sequence::create(TargetedAction::create(jelly, FadeIn::create(1)),
                                                            createSoundAction("kacha.mp3"), NULL);
        actions.pushBack(fadein);
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        
        if ((index==0&&DataManager::sharedManager()->getEnableFlagWithFlagID(30))||
            (index==1&&DataManager::sharedManager()->getEnableFlagWithFlagID(29))) {
            //猿の移動
            auto monkey=parent->getChildByName<Sprite*>("sup_16_greenmonkey.png");
            
            auto monkey_0=createSpriteToCenter("anim_monkey_0.png", true, parent);
            parent->addChild(monkey_0);
            auto monkey_1=createSpriteToCenter("anim_monkey_1.png", true, parent);
            parent->addChild(monkey_1);
            
            actions.pushBack(Spawn::create(createChangeAction(1, .5, monkey, monkey_0),
                                           createSoundAction("monkey.mp3"), NULL));
            actions.pushBack(DelayTime::create(.7));
            actions.pushBack(Spawn::create(createChangeAction(1, .5, monkey_0, monkey_1),
                                           createSoundAction("monkey.mp3"), NULL));
            actions.pushBack(DelayTime::create(.7));
            actions.pushBack(TargetedAction::create(monkey_1, FadeOut::create(1)));
            actions.pushBack(DelayTime::create(.7));
        }
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key.compare(0,5,"candy")==0)
    {//豚にキャンディ
        Common::playSE("koto.mp3");
        
        Vector<FiniteTimeAction*>actions;
        
        auto index=atoi(key.substr(6,1).c_str());
        auto candy=createSpriteToCenter(StringUtils::format("sup_23_candy_%d.png",index), true, parent);
        parent->addChild(candy);
        
        auto cheek=createSpriteToCenter("cheek.png", true, parent);
        parent->addChild(cheek);
        
        auto back=createSpriteToCenter("back_23.png", true, parent);
        back->setCascadeOpacityEnabled(true);
        parent->addChild(back);
        auto anim=createSpriteToCenter("anim_pig_0.png", false, parent);
        back->addChild(anim);
        
        auto back_2=createSpriteToCenter("back_23.png", true, parent);
        back_2->setCascadeOpacityEnabled(true);
        parent->addChild(back_2);
        auto sleep=createSpriteToCenter("sup_23_pig_1.png", false, parent);
        back_2->addChild(sleep);
        auto dish=createSpriteToCenter("sup_23_dish.png", false, parent);
        back_2->addChild(dish);
        
        auto fadein_candy=TargetedAction::create(candy, FadeIn::create(1));
        actions.pushBack(fadein_candy);
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        
        auto fadein_anim=Spawn::create(TargetedAction::create(back, FadeIn::create(1)),
                                       TargetedAction::create(candy, FadeOut::create(.7)),
                                       createSoundAction("paku.mp3"), NULL);
        actions.pushBack(fadein_anim);
        actions.pushBack(delay->clone());
        
        auto fadeouts=Spawn::create(TargetedAction::create(back,FadeOut::create(1)),
                                    TargetedAction::create(cheek, FadeIn::create(.7)),
                                    createSoundAction("pig_1.mp3"), NULL);
        actions.pushBack(fadeouts);
        actions.pushBack(delay->clone());
        
        if (index==2) {
            auto fadeouts_1=Spawn::create(TargetedAction::create(cheek, FadeOut::create(.7)),
                                          TargetedAction::create(back_2, FadeIn::create(1)),
                                          createSoundAction("pig_1.mp3"), NULL);
            actions.pushBack(fadeouts_1);
            actions.pushBack(delay->clone());
        }
        else{
            auto fadeout_cheek=TargetedAction::create(cheek, FadeOut::create(.7));
            actions.pushBack(fadeout_cheek);
        }
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="putdoll"){
        Common::playSE("p.mp3");
        
        auto doll=createSpriteToCenter("sup_9_doll.png", true, parent);
        parent->addChild(doll);
        
        auto fadein=TargetedAction::create(doll, FadeIn::create(1));
        auto sound=createSoundAction("kacha.mp3");
        auto delay=DelayTime::create(1);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,sound,delay,call, NULL));
    }
    else if(key=="knife")
    {//キャンディ特有
        Common::playSE("p.mp3");
        
        auto knife=createSpriteToCenter("anim_knife.png", true, parent);
        parent->addChild(knife);
        
        auto doll=parent->getChildByName<Sprite*>("sup_9_doll.png");
        
        auto fadein_0=FadeIn::create(.5);
        auto move_0=MoveBy::create(.75, Vec2(0, -parent->getContentSize().height*.025));
        auto delay_0=DelayTime::create(.5);
        auto move_1=MoveBy::create(.2, Vec2(0, parent->getContentSize().height*.05));
        auto sound_0=createSoundAction("kacha.mp3");
        auto seq_knife=TargetedAction::create(knife,Sequence::create(fadein_0,move_0,delay_0,move_1,sound_0,NULL));
        
        auto delay_1=DelayTime::create(.5);
        
        //とぶ
        auto sound_1=CallFunc::create([this]{
            Common::playSE("gun-fire.mp3");
        });
        auto fly=TargetedAction::create(doll, EaseOut::create(MoveBy::create(.5, Vec2(0, parent->getContentSize().height)), 2));
        auto delay_2=DelayTime::create(.5);
        auto sound_2=createSoundAction("hit.mp3");
        auto delay_3=DelayTime::create(.5);
        
        //おちる
        auto sound_3=CallFunc::create([this,doll]
                                      {//調整も行う
                                          doll->setLocalZOrder(2);
                                          doll->setAnchorPoint(Vec2(.5,.6));
                                          Common::playSE("fall.mp3");
                                      });
        auto move_2=EaseIn::create(MoveBy::create(2, Vec2(0, -parent->getContentSize().height*3)),2);
        auto rotate=RotateBy::create(2, 360);
        auto fall=TargetedAction::create(doll, Spawn::create(move_2,rotate, NULL));
        auto call=CallFunc::create([this,callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_knife,delay_1,sound_1,fly,delay_2,sound_2,delay_3,sound_3,fall,call, NULL));
    }
    else if(key=="present")
    {//キャンディ特有 プレゼント
        auto call=CallFunc::create([callback]{
            auto story=StoryLayer::create("ending", [callback](Ref*ref){
                callback(true);
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        parent->runAction(Sequence::create(call, NULL));
    }
    else if(key=="curtain"&&DataManager::sharedManager()->isPlayingMinigame()){
        auto leg=parent->getChildByName("sup_30_mistake_0.png");
        leg->setOpacity(0);
        
        auto mistake=parent->getChildByName("sup_30_mistake_1.png");
        mistake->setOpacity(255);
        
        callback(true);
    }
    else if(key=="freezer_open")
    {
        if (DataManager::sharedManager()->getTemporaryFlag("smoke").asInt()==0) {
            auto particle=ParticleSystemQuad::create("smoke.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width/2,parent->getContentSize().height*.475);
            particle->setDuration(.2);
            particle->setStartSize(parent->getContentSize().width*.1);
            particle->setStartSizeVar(particle->getStartSize()*.2);
            particle->setEndSize(particle->getStartSize()*4);
            particle->setSpeed(parent->getContentSize().height*.075);
            particle->setSpeedVar(particle->getSpeed()*.2);
            particle->setGravity(Vec2(0, particle->getSpeed()*1));
            particle->setLife(2.5);
            particle->setTotalParticles(1500);
            particle->setPosVar(Vec2(parent->getContentSize().width*.275, parent->getContentSize().height*.24));
            particle->setAngle(270);
            particle->setAngleVar(5);
            parent->addChild(particle);
            
            DataManager::sharedManager()->setTemporaryFlag("smoke", 1);
        }
        
        callback(true);
    }
    else if(key=="dontopen"){
        auto story=StoryLayer::create("dontopen", [callback](Ref*ref){
            callback(true);
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void CandyHouseActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{
    if (key.compare(0,5,"stamp")==0) {

    }
}
