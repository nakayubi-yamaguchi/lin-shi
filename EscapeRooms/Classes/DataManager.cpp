//
//  SettingDataManager.cpp
//  Kebab
//
//  Created by 成田凌平 on 2016/12/17.
//
//

#include "DataManager.h"
#include "NotificationKeys.h"
#include "Utils/Common.h"
#include "Utils/FireBaseBridge.h"
#include <time.h>

#include "AnswerManager.h"
#include "WarpManager.h"

#include "Utils/ValueHelper.h"
#include "Utils/NativeBridge.h"
#include "AnalyticsManager.h"

#include "SupplementsManager.h"

#define FilePath_Back "back.plist"
#define FilePath_Item "item.plist"
#define FilePath_Touch "touch.plist"
#define FilePath_Flag "flag.plist"
#define FilePath_OtherString "otherStrings.plist"
#define FilePath_AppText "app_text.plist"
#define FilePath_Achievement "achievement.plist"

#define FilePath_TalkText "talk_text.plist"

#define FilePath_MiniGame "minigame.plist"
#define FilePath_PanelAd "panelAd.plist"
#define FilePath_Animation "animation.plist"
#define FilePath_StoryData "story.plist"
#define FilePath_ColorData "color.plist"
#define FilePath_SpeakerData "speaker.plist"

#define FilePath_ItemData "itemData.plist"
#define FilePath_FlagData "flagData.plist"
#define FilePath_ReleasedHintData "releasedHintData.plist"
#define FilePath_OtherFlagData "otherFlagData.plist"
#define FilePath_RecordData "recordData.plist"
#define FilePath_EternalRecordData "titleRecordData.plist"//タイトルごとに持つべきデータ:プレ動画視聴やプレイ回数など、[START]してもリセットされないもの。

using namespace cocos2d;

DataManager* DataManager::manager =NULL;

DataManager* DataManager::sharedManager()
{
    if (manager==NULL) {
        manager=new DataManager();
        manager->init();
        
    }
    
    return manager;
}

void DataManager::init()
{//初期値をセット
    nowBack=1;
    
    //コインをローディング
    loadingCoin();
    //ユーザIDをローディング
    loadUserID();

    
    Common::performProcessForDebug([this](){m_coin=0;}, nullptr);
    
}

#pragma mark- 一括ロード
void DataManager::loadAllData()
{
    //コンティニューの時は、セーブデータを読み込みする
    loadItemData();
    loadFlagData();
    loadOtherFlagData();
    loadReleasedHintData();
    loadRecordData();
    loadMysteryMap();
    
    //開始と同時にセット
    setEnFlagTime(time(NULL));
    setIsPromoteHint(false);
}

#pragma mark- ファイル操作
#pragma mark 背景
void DataManager::reloadNowBack()
{
    nowBackMap=getBackData();
}

ValueMap DataManager::getBackData(int num)
{
    auto backdata = getPlistMapWithKey(FilePath_Back);
    
    auto map=backdata[StringUtils::format("%d",num)].asValueMap();

    return map;
}

ValueMap DataManager::getBackData()
{
    return getBackData(getNowBack());
}

std::string DataManager::getBackName()
{
    return getBackName(getNowBack());
}

std::string DataManager::getBackName(int backID)
{
    //表示ファイル名をロード
    auto map=getBackData(getNowBack());
    auto fileName=map["fileName"].asString();
    if (fileName.size()==0) {
        fileName=StringUtils::format("back_%d.png",backID);
    }
    return fileName;
}


#pragma mark タッチ
ValueVector DataManager::getTouchDataArray()
{
    auto touchdata = getPlistMapWithKey(FilePath_Touch);
    
    auto value=touchdata[StringUtils::format("%d",getNowBack())];
    if (value.isNull())
    {//タッチデータなし 空の配列を返す
        ValueVector vec;
        return vec;
    }
    
    return value.asValueVector();
}

#pragma mark アニメーション
Value DataManager::getAnimationData(int animationID)
{
    auto animationData = getPlistMapWithKey(FilePath_Animation);
    auto value=animationData[StringUtils::format("%d",animationID)];
    
    return value;
}

#pragma mark アイテム
void DataManager::reloadNowItem()
{
    nowItemMap=getItemData(getShowingItemID());
}

ValueMap DataManager::getItemData(int itemID)
{
    auto itemdata = getPlistMapWithKey(FilePath_Item);
    auto itemV=itemdata[StringUtils::format("%d",itemID)];
    if (!itemV.isNull()) {
        return itemV.asValueMap();
    }
    
    ValueMap empty;
    return empty;
}

void DataManager::addNewItem(int itemID)
{//アイテムを入手。リロード&セーブ必須
    auto find_itr=find(items.begin(), items.end(), itemID);//見つからない時はend()を返す
    bool found= find_itr!=items.end();
    
    if (found) {
        log("すでに持ってるあいてむです");
        return;
    }
    
    NativeBridge::vibration3D();
    
    items.push_back(itemID);
    
    saveItemData();
    
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyReloadItems);
}

bool DataManager::findItemWithItemID(int itemID)
{//指定アイテムを所持しているかの判定
    for (int i : items) {
        if (i==itemID) {
            return true;
        }
    }
    return false;
}

void DataManager::changeItemWithIDs(int fromID, int toID)
{//アイテム変化。リロード&セーブ必須
    //アイテムを削除して良いか確認する
    auto map=getItemData(fromID);
    if (!map["notRemoveByCombine"].isNull()) {
        if(map["notRemoveByCombine"].asBool())
        {//アイテム削除しない->変化後のアイテムのみaddする
            addNewItem(toID);
            return;
        }
    }
    
    for (int i=0;i<items.size();i++) {
        if (items[i]==fromID) {
            items[i]=toID;
            break;
        }
    }
    
    saveItemData();
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyReloadItems);
}

void DataManager::separateItemWithIDs(int fromID, ValueVector ids)
{
    removeItemWithID(fromID);
    for (auto value: ids) {
        addNewItem(value.asInt());
    }
    
    saveItemData();
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyReloadItems);
}

void DataManager::removeItemWithID(int itemID)
{//アイテムの削除。リロード&セーブ必須
    if (itemID==0) {
        return;
    }
    
    bool removed=false;
    
    for (int i=0;i<items.size();i++) {
        auto IDinItems=items[i];
        if (IDinItems==itemID) {
            items.erase(items.begin()+i);
            removed=true;
            
            if (getSelectedItemID()==IDinItems)
            {//削除アイテムを選択していたらリセット
                setSelectedItemID(0);
            }
            
            break;
        }
    }
    
    if (removed) {
        saveItemData();
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyReloadItems);
    }
}

bool DataManager::isExistItemData()
{
    auto path = EscapeDataManager::getInstance()->getPlayTitleDataDirWithFile(FilePath_ItemData);

    return FileUtils::getInstance()->isFileExist(path);
}

void DataManager::loadItemData()
{
    auto path = EscapeDataManager::getInstance()->getPlayTitleDataDirWithFile(FilePath_ItemData);

    // ValueMapとして読み込む
    auto itemValueVector = FileUtils::getInstance()->getValueVectorFromFile(path);

    for (auto item: itemValueVector) {//std::vectorに変換
        items.push_back(item.asInt());
    }    
}

void DataManager::saveItemData()
{//itemsをvalueMapにしてセーブ
    ValueVector itemData;
    for (auto item :items) {
        itemData.push_back(Value(item));
    }
    CCLOG("アイテム情報をこれから保存します");
    EscapeDataManager::getInstance()->saveDataOnPlay(FilePath_ItemData, Value(itemData));
}

#pragma mark フラグ
void DataManager::setEnableFlag(float flagID)
{
    if (flagID==0) {
        return;
    }
    
    //新しいフラグを立てたらその時刻を記憶
    if (!getEnableFlagWithFlagID(flagID)) {
        setEnFlagTime(time(NULL));
    }
    
    log("フラグたてまっせ%s", getFlagIDKey(flagID).c_str());
    flagMap[getFlagIDKey(flagID)] = true;
    
    //デバッグログ sortして出力
    Common::performProcessForDebug([this]{
        std::vector<float>sortFlagVec;
        
        //flagMapをチェック.
        for (auto &key :flagMap) {
            sortFlagVec.push_back(atof(key.first.c_str()));
        }
        
        sortFlagVec=ValueHelper::sort(sortFlagVec, true);
        
        
        for (int i=0; i<sortFlagVec.size(); i++) {
            auto key=getFlagIDKey(sortFlagVec[i]);
            log("%s:%d",key.c_str(),flagMap[key.c_str()].asBool());
        }
    });

    //ヒント表示対象flagをセット
    setFlagIDForHint();
    
    saveFlagData();
}

void DataManager::setDisableFlag(float flagID)
{
    if (flagID==0) {
        return;
    }
    
    //新しいフラグを立てたらその時刻を記憶
    if (!getEnableFlagWithFlagID(flagID)) {
        setEnFlagTime(time(NULL));
    }
    
    flagMap[getFlagIDKey(flagID)] = false;
    
    //デバッグログ sortして出力
    Common::performProcessForDebug([this]{
        std::vector<float>sortFlagVec;
        
        //flagMapをチェック.
        for (auto &key :flagMap) {
            sortFlagVec.push_back(atof(key.first.c_str()));
        }
        
        sortFlagVec=ValueHelper::sort(sortFlagVec, true);
        
        for (int i=0; i<sortFlagVec.size(); i++) {
            auto key=getFlagIDKey(sortFlagVec[i]);
            //log("%s:%d",key.c_str(),flagMap[key.c_str()].asBool());
        }
    });
    
    saveFlagData();
}

void DataManager::setEnableFlags(float min, float max)
{
    auto map=getPlistMapWithKey(FilePath_Flag);
    for (auto v : map) {
        auto keyFlag=atof(v.first.c_str());
    
        if (keyFlag>=min&&
            keyFlag<=max) {
            setEnableFlag(keyFlag);
        }
    }
}

bool DataManager::getEnableFlagWithFlagID(float flagID)
{
    if(flagID<=0)
    {
        return false;
    }
    
    auto enable=flagMap[getFlagIDKey(flagID).c_str()].asBool();
    return enable;
}

bool DataManager::getEnableFlagWithFlagIDs(ValueVector flagIDs)
{
    auto judge=true;//正解判定フラグが立っているか

    for (auto v : flagIDs) {
        if(!getEnableFlagWithFlagID(v.asFloat())){
            judge=false;
            log("フラグ不足 %d",v.asInt());
            break;
        }
    }
    
    return judge;
}

int DataManager::getFlagCount(float min, float max)
{
    auto counter=0;
    auto map=getFlagFileData();
    
    for (auto v:map) {
        auto flag=atof(v.first.c_str());
        
        if (flag>=min&&
            flag<=max) {
            counter++;
        }
    }
    
    return counter;
}

int DataManager::getEnableFlagCount(float min, float max)
{
    auto counter=0;
    auto map=getFlagFileData();
    
    for (auto v:map) {
        auto flag=atof(v.first.c_str());

        if (flag>=min&&
            flag<=max&&
            getEnableFlagWithFlagID(flag)) {
            counter++;
        }
    }
    
    return counter;
}

bool DataManager::isExistFlagData()
{
    auto path = EscapeDataManager::getInstance()->getPlayTitleDataDirWithFile(FilePath_FlagData);

    return FileUtils::getInstance()->isFileExist(path);
}

float DataManager::getProgress()
{
    int flagCount;
    auto minigameCount=getFlagCount(getMiniGameData()["startFlagID"].asFloat(), UINT_MAX)-1;
    auto enableCount=0;
    
    if (!isPlayingMinigame()) {
        flagCount=(int)getFlagFileData().size()-minigameCount;
        enableCount=getEnableFlagCount(0, getMiniGameData()["startFlagID"].asInt());
    }
    else{
        flagCount=minigameCount;
        enableCount=getEnableFlagCount(getMiniGameData()["startFlagID"].asInt()+1,INT_MAX);
    }
    
    return (float)enableCount*100/(float)flagCount;
}

void DataManager::loadFlagData()
{
    auto path = EscapeDataManager::getInstance()->getPlayTitleDataDirWithFile(FilePath_FlagData);

    // ValueMapとして読み込む
    flagMap = FileUtils::getInstance()->getValueMapFromFile(path);
    
    setFlagIDForHint();
    log("フラグデータをローディングしました。%s",path.c_str());
}

void DataManager::saveFlagData()
{
    auto file = EscapeDataManager::getInstance()->getPlayTitleDataDirForSaveWithFile(FilePath_FlagData);
    //log("これから保存します[%s]",file.c_str());

    EscapeDataManager::getInstance()->saveDataOnPlay(FilePath_FlagData, Value(flagMap));
}

#pragma mark- 一時的フラグ管理
void DataManager::setTemporaryFlag(std::string key, int value)
{
    log("一時的なフラグ key:%s value:%d",key.c_str(),value);
    
    temporaryFlagMap[key]=StringUtils::format("%d",value);
}

Value DataManager::getTemporaryFlag(std::string key)
{
    return temporaryFlagMap[key];
}

ValueVector DataManager::getTemporarySupplementsVec(ValueMap map)
{
    auto supplements_v=map["supplements_temporary"];
    if (supplements_v.isNull())
    {//補完画像データなし。からのmapを返す
        ValueVector empty;
        return empty;
    }
    
    return supplements_v.asValueVector();
}

#pragma mark- その他フラグ
void DataManager::loadOtherFlagData()
{
    auto path = EscapeDataManager::getInstance()->getPlayTitleDataDirWithFile(FilePath_OtherFlagData);
    
    // ValueMapとして読み込む
    otherFlagMap = FileUtils::getInstance()->getValueMapFromFile(path);
    
    log("その他のフラグデータをローディングしました。");
}

void DataManager::setOtherFlag(std::string key,int value)
{
    log("その他フラグ key:%s value:%d",key.c_str(),value);
    otherFlagMap[key]=StringUtils::format("%d",value);
    
    saveOtherFlagData();
}

Value DataManager::getOtherFlag(std::string key)
{
    return otherFlagMap[key];
}

void DataManager::saveOtherFlagData()
{
    CCLOG("その他のフラグのデータを書き出します。");
    
    EscapeDataManager::getInstance()->saveDataOnPlay(FilePath_OtherFlagData, Value(otherFlagMap));
}

ValueVector DataManager::getOtherFlagSupplementsVec(ValueMap map)
{
    auto supplements_v=map["supplements_other"];
    if (supplements_v.isNull())
    {//補完画像データなし。からのmapを返す
        ValueVector empty;
        return empty;
    }
    
    return supplements_v.asValueVector();
}

#pragma mark ヒント
std::string DataManager::getHintTitle(int hintIndex)
{
    auto flagData=getFlagFileData();

    auto flagIndex=getFlagIDForHint();//ヒント表示するフラグID
    auto map=flagData[StringUtils::format("%s",getFlagIDKey(flagIndex).c_str())].asValueMap();
    auto titleValue=map["place"];
    if (titleValue.isNull()) {
        return "HINT";
    }
    
    auto titleMap=titleValue.asValueMap();
    auto localize=Common::localize("en", "ja", "ch", "tw", "ko");
    
    auto hintTitle=titleMap[localize].asString();
    
    if (hintTitle.size()==0) {
        log("ヒントtitleが空なので英語版を入れます");
        hintTitle=titleMap["en"].asString();
    }
    
    return hintTitle;
}

ValueVector DataManager::getHintArray(bool includeUseAnswerImage)
{
    auto flagData=getFlagFileData();
    auto flagIndex = getFlagIDForHint();//ヒント表示するフラグID
    
    //flagIndexをstring型に変換する
    auto map=flagData[getFlagIDKey(flagIndex)].asValueMap();
    ValueVector hintArray;
    if (!map["hintArray"].isNull()) {//ヒント,アンサー,解説が配列になっているパターン
        hintArray = map["hintArray"].asValueVector();
        
        auto itr = hintArray.begin();
        while (itr != hintArray.end()){
            auto hintValue = *itr;
            
            HintType hintType = getHintType(hintValue.asValueMap());
            bool needDelete = (!includeUseAnswerImage && hintType == HintType_Answer) || (!EscapeDataManager::getInstance()->getPlayTitleIsCollectionDisplayForHint() && hintType == HintType_Commentary);
            if(needDelete){
                itr = hintArray.erase(itr);
            }
            else{
                itr++;
            }
        }
    }
    else {
        if (!map["hintImage"].isNull()) {
            hintArray=map["hintImage"].asValueVector();
        }
        else{
            //以前のヒントパターンを読み込む
            hintArray=map["hint"].asValueVector();
        }
        
        //useImageをする場合、ひとつpushする
        if (includeUseAnswerImage) {
            auto useImageMap=getUseImageInAnswerValueMap(map);
            
            if (useImageMap.size()>0) {
                map=FileUtils::getInstance()->getValueMapFromFile(FilePath_AppText);
                auto textMap=map["answerImage"].asValueMap();
                hintArray.push_back(Value(textMap));
            }
           
        }
    }
    
    return hintArray;
}

int DataManager::getHintCount()
{
    auto arraySize=(int)getHintArray(true).size();
    return arraySize;
}

float DataManager::getFlagIDForHint()
{
    return flagIDforHint;
}

void DataManager::setFlagIDForHint()
{
    auto flagData=getFlagFileData();
    //フラグnum順にソートを行う
    std::vector<float>nums;
    
    for (auto v : flagData) {
        auto num=atof(v.first.c_str());
        nums.push_back(num);
    }
        
    nums= ValueHelper::sort(nums,true);
    
    //先頭からフラグをチェック
    for (auto num:nums) {
        log("num:%f",num);
        if (num>=flagIDforHint) {
            bool enable = getEnableFlagWithFlagID(num);
            if (!enable) {
                flagIDforHint=num;
                break;
            }
        }
    }
}

void DataManager::loadReleasedHintData()
{
    auto path = EscapeDataManager::getInstance()->getPlayTitleDataDirWithFile(FilePath_ReleasedHintData);

    // ValueMapとして読み込む
    releasedHintMap = FileUtils::getInstance()->getValueMapFromFile(path);
    
    log("解放ヒントデータをローディングしました。");
}

void DataManager::saveReleasedHintData()
{
    CCLOG("解放ヒントのデータを書き出します");

    EscapeDataManager::getInstance()->saveDataOnPlay(FilePath_ReleasedHintData, Value(releasedHintMap));
}

void DataManager::setReleasedHintIndex(int hintIndex)
{
    auto flagID=getFlagIDForHint();
    releasedHintMap[StringUtils::format("%s_%d",getFlagIDKey(flagID).c_str(),hintIndex)]=1;
    
    log("フラグ%.1fのヒント%d番目を解放しました[%s]",flagID,hintIndex,StringUtils::format("%s_%d",getFlagIDKey(flagID).c_str(),hintIndex).c_str());
    
    saveReleasedHintData();
}

ValueMap DataManager::getFlagMapForFlag(float flagID)
{
    auto flagData=getFlagFileData();
    
    //flagIndexをstring型に変換する
    auto map=flagData[getFlagIDKey(flagID)].asValueMap();
    return map;
}

ValueMap DataManager::getUseImageInAnswerValueMap(ValueMap map)
{
    if (isEscapeRoomsGrammerForFlag()) {
        for (auto value : getHintArray(true)) {
            auto map = value.asValueMap();
            if (getHintType(map) == HintType_Answer) {
                return map;
            }
        }
    }
    
    if (!map["useImage"].isNull()) {
        return map["useImage"].asValueMap();
    }
    
    ValueMap empty;
    return empty;
}

ValueMap DataManager::getUseImageInHintValueMap(ValueMap map)
{
    if (!map["useImage_hint"].isNull()) {
        return map["useImage_hint"].asValueMap();
    }
    
    ValueMap empty;
    return empty;
}

bool DataManager::isReleasedHint(int hintIndex)
{
    auto flagID = getFlagIDForHint();
    return isReleasedHint(hintIndex, flagID);
}

bool DataManager::isReleasedHint(int hintIndex, float flagID)
{
    return releasedHintMap[StringUtils::format("%s_%d",getFlagIDKey(flagID).c_str(),hintIndex)].asBool();
}

int DataManager::getReleasedHintNum()
{
    int flagID=getFlagIDForHint();
    int hintCount = getHintCount();
    int releasedNum=getReleasedHintNum(flagID,hintCount);

    return releasedNum;
}

int DataManager::getReleasedHintNum(float flagID, int hintCount)
{
    int releasedNum = -1;
    
    for (int i = 0; i < hintCount; i++) {
        if (isReleasedHint(i, flagID)) {//ヒントが解放されている
            releasedNum = i;
        }
        else {//ヒント未解放
            break;
        }
    }
        
    return releasedNum;
}

ValueVector DataManager::getReleaseHintVector()
{
    float flagID=getFlagIDForHint();
    int hintCount = getHintCount();
    
    ValueVector releasedHints;
    for (int i = 0; i < hintCount; i++) {
        if (isReleasedHint(i, flagID)) {//ヒントが解放されている
            releasedHints.push_back(Value(i));
        }
        else {//ヒント未解放
        }
    }
    
    log("ヒントベクターの中身を表示します");
    ValueHelper::dumpValue(Value(releasedHintMap));
    return releasedHints;
}

std::string DataManager::getFlagIDKey(float flagID)
{
    bool isDecimal=(ceilf(flagID)!=floorf(flagID));//小数点以下を切り上げ(ceil)、切り下げした場合(floor)の整数値が変わればdecimal
    
    if (!isDecimal) {
        return StringUtils::format("%d",(int)flagID);
    }
    else{
        return StringUtils::format("%.1f",flagID);
    }
}

HintType DataManager::getHintType(ValueMap hintMap)
{
    
    Value hintTypeValue = hintMap["hintType"];
    if (!hintTypeValue.isNull()) {
        HintType hintType = (HintType)hintTypeValue.asInt();
        
        log("ヒントタイプを表示します%d",hintType);
        return hintType;
    }
    
    hintTypeValue = hintMap["isAnswer"];
    if (hintTypeValue.asBool()) {
        return HintType_Answer;
    }
    
    hintTypeValue = hintMap["isCommentary"];
    if (hintTypeValue.asBool()) {
        return HintType_Commentary;
    }
    
    hintTypeValue = hintMap["type"];
    if (!hintTypeValue.isNull()) {
        auto hintTypeString = hintTypeValue.asString();
        if (hintTypeString == "commentary") {
            return HintType_Commentary;
        }
        else if (hintTypeString == "answer") {
            return HintType_Answer;
        }
    }
    
    return HintType_Hint;
}

bool DataManager::isEscapeRoomsGrammerForFlag()
{
    auto flagData=getFlagFileData();
    auto flagIndex = getFlagIDForHint();//ヒント表示するフラグID
    //flagIndexをstring型に変換する
    auto map=flagData[getFlagIDKey(flagIndex)].asValueMap();
    
    return !map["hintArray"].isNull();
}

#pragma mark - ShowType
bool DataManager::isShowTypeAnswer(ShowType type)
{
    return (type==ShowType_Answer||
            type==ShowType_Answer_Big);
};

bool DataManager::isShowTypeHint(ShowType type)
{
    return (type==ShowType_Hint||
            type==ShowType_Hint_Big);
};

#pragma mark ステージ中保持するデータ
ValueMap DataManager::getFlagFileData()
{
    if (flagFileData.size()==0) {
        auto path = EscapeDataManager::getInstance()->getPlayTitleResourceDirWithFile(FilePath_Flag);
        flagFileData = FileUtils::getInstance()->getValueMapFromFile(path);
    }
    
    //ValueHelper::dumpValue(Value(flagFileData));
    
    return flagFileData;
}

#pragma mark ストーリー
ValueVector DataManager::getStoryData(std::string storyName)
{
    auto storydata = getPlistMapWithKey(FilePath_StoryData);
    
    auto map = storydata[storyName.c_str()].asValueMap();
    auto storyVector = map["messages"];

    if (storyVector.isNull()){//表示中の背景にパネル広告を配置しない。空のmapを返す
        ValueVector empty;
        return empty;
    }
    
    return storyVector.asValueVector();
}

std::string DataManager::getColorCodeData(std::string colorName)
{
    static auto colordata = FileUtils::getInstance()->getValueMapFromFile(FilePath_ColorData);
    
    auto colorcode = colordata[colorName.c_str()];
    if (colorcode.isNull()){
        return "";
    }
    return colordata[colorName.c_str()].asString();
}

ValueMap DataManager::getSpeakerData(const char *characterName)
{
    auto speakerdata = getPlistMapWithKey(FilePath_SpeakerData);
    
    auto speakerValue = speakerdata[characterName];
    
    if (speakerValue.isNull()) {
        ValueMap nullMap;
        return nullMap;
    }
    
    return speakerValue.asValueMap();
}

bool DataManager::isShowedOpeningStory()
{
    return getOtherFlag(StartGameKey).asBool();
}

#pragma mark その他テキスト
std::string DataManager::getAppName()
{
    auto map=getPlistMapWithKey(FilePath_OtherString)["AppName"].asValueMap();
    auto localize=Common::localize("en", "ja", "ch","tw","ko");
    auto name=map[localize].asString();
    if (name.size()==0) {
        name=map["en"].asString();
    }
    
    return name;
}

std::string DataManager::getHashTag()
{
    auto map=getPlistMapWithKey(FilePath_OtherString)["HashTag"].asValueMap();
    auto localize=Common::localize("en", "ja", "ch","tw","ko");
    auto name=map[localize].asString();
    if (name.size()==0) {
        name=map["en"].asString();
    }
    
    return name;
}

std::string DataManager::getNotificationString()
{//~~~から脱出しよう！の文字列を返す
    //stage nameを取得
    auto value=getPlistMapWithKey(FilePath_OtherString)["Notification"];
    std::string name;
    if (value.isNull()) {
        name=getHashTag();
    }
    else{
        auto map=value.asValueMap();
        auto localize=Common::localize("en", "ja", "ch","tw","ko");
        name=map[localize].asString();
        if (name.size()==0) {
            name=map["en"].asString();
        }
    }
    
    //[~から脱出しよう]テキストを送信する
    auto string=StringUtils::format(getSystemMessage("localPush_stage").c_str(),name.c_str());
    
    auto clear=Common::localize("Progress", "クリア率", "进展", "進展", "진행");
    auto append=StringUtils::format("[%s:%.0f％] %s",clear.c_str(),getProgress(),string.c_str());
    return append;
}

std::string DataManager::getTitleSE()
{
    return getPlistMapWithKey(FilePath_OtherString)["title_se"].asString();
}

std::string DataManager::getBgmName(bool isEnding)
{
    std::string key="bgm";
    if (isEnding) {
        key="bgm_ending";
    }
    
    return getPlistMapWithKey(FilePath_OtherString)[key].asString();
}


#pragma mark システムメッセージ
ValueMap DataManager::getAppTextMap(std::string key)
{
    static auto map=FileUtils::getInstance()->getValueMapFromFile(FilePath_AppText);
    auto textMap=map[key].asValueMap();
    return textMap;
}

std::string DataManager::getSystemMessage(std::string key)
{
    auto textMap = getAppTextMap(key);
    return Common::localize(textMap["en"].asString(), textMap["ja"].asString(), textMap["ch"].asString(), textMap["tw"].asString(), textMap["ko"].asString());
}

std::string DataManager::getTalkMessage()
{
    static auto vec=FileUtils::getInstance()->getValueVectorFromFile(FilePath_TalkText);
    auto index=UserDefault::getInstance()->getIntegerForKey(UserKey_PigTalkIndex);
    index=index%vec.size();
    
    Common::performProcessForDebug([&index](){
        index = 16;
        
    }, nullptr);

    auto textMap=vec.at(index).asValueMap();
    UserDefault::getInstance()->setIntegerForKey(UserKey_PigTalkIndex, index+1);
    
    auto text=Common::localize(textMap["en"].asString(), textMap["ja"].asString(), textMap["ch"].asString(), textMap["tw"].asString(), textMap["ko"].asString());
    
    if (text.size()==0) {
        //テキストがセットされていない場合indexを進めて再取得
        return getTalkMessage();
    }
    
    return text;
}

#pragma mark- ミニゲーム
void DataManager::loadMysteryMap()
{
    auto map=getMiniGameData()["mystery"].asValueMap();
    for (auto v : map) {
        auto mysteryID=atoi(v.first.c_str());//mysteryID
        auto onItem=v.second.asValueMap()["onItem"].asBool();
        ValueMap supplementMap=v.second.asValueMap()["supplements"].asValueMap();
        for (auto v_sup : supplementMap) {
            auto ID=atoi(v_sup.first.c_str());//back or item
            Value mysteryIDsValue;//既存チェック
            if (onItem) {
                mysteryIDsValue=mysteryMap_onItem[StringUtils::format("%d",ID)];
            }
            else{
                mysteryIDsValue=mysteryMap[StringUtils::format("%d",ID)];
            }
            
            
            if (mysteryIDsValue.isNull()) {
                //指定のidにmysteryを新規追加する
                ValueVector vector;
                vector.push_back(Value(mysteryID));
                if (onItem) {
                    mysteryMap_onItem[StringUtils::format("%d",ID)]=Value(vector);
                }
                else{
                    mysteryMap[StringUtils::format("%d",ID)]=Value(vector);
                }
            }
            else{
                mysteryIDsValue.asValueVector().push_back(Value(mysteryID));
                if (onItem) {
                    mysteryMap_onItem[StringUtils::format("%d",ID)]=Value(mysteryIDsValue.asValueVector());
                }
                else{
                    mysteryMap[StringUtils::format("%d",ID)]=Value(mysteryIDsValue.asValueVector());
                }
            }
            
            //log("間違い back: %d mystery %d",backID,mysteryID);
        }
    }
    
    /*for (auto v : mysteryMap) {
        auto backID=v.first;
        auto mysteryIDs=v.second.asValueVector();
        log("間違いbackは%s : まちがい%ld個",backID.c_str(),mysteryIDs.size());
    }*/
}

ValueMap DataManager::getMiniGameData()
{
    auto map=getPlistMapWithKey(FilePath_MiniGame);
    
    return map;
}

ValueMap DataManager::getMysteryMap(int mysteryID)
{
    auto map=getMiniGameData();
    
    return map["mystery"].asValueMap()[StringUtils::format("%d",mysteryID)].asValueMap();
}

int DataManager::getFoundMysteryCount()
{
    auto map=getMiniGameData();
    auto correctCount=0;
    auto mysteryMapV=map["mystery"];
    if (mysteryMapV.isNull()) {
        return 0;
    }
    
    for (auto v : mysteryMapV.asValueMap()) {
        auto flagContainMap=v.second.asValueMap();//フラグ情報を持つマップ
        if (getEnableFlagWithFlagID(flagContainMap["enFlagID"].asFloat())) {
            correctCount++;
        }
    }
    
    return correctCount;
}

std::string DataManager::getMysteryCorrectSupName(int mysteryID)
{
    auto mysteryMap=getMysteryMap(mysteryID);
    auto supName=mysteryMap["supName_correct"].asValueMap()[StringUtils::format("%d",getNowBack())].asString();
    
    return supName;
}

bool DataManager::isPlayingMinigame()
{
    return getEnableFlagWithFlagID(getMiniGameData()["startFlagID"].asInt());
}

#pragma mark- 補完画像
ValueMap DataManager::getSupplementsMap(ValueMap backMap)
{
    auto supplements_map=backMap["supplements"];
    if (supplements_map.isNull())
    {//補完画像データなし。からのmapを返す
        ValueMap empty;
        return empty;
    }
    
    return supplements_map.asValueMap();
}

std::vector<std::string> DataManager::getRemoveSupplementsNames(ValueMap map, bool onItem)
{
    auto supplements_map=map["supplements_remove"];
    if (supplements_map.isNull())
    {//補完画像データなし。
        std::vector<std::string>empty;
        return empty;
    }
    
    //取り除くべき補完画像名を配列に格納する
    std::vector<std::string> removes;
    for (auto& value :supplements_map.asValueMap())
    {//allkeys,allvaluesをしゅとくしますよ
        auto key=value.first;//キーとなるフラグ firstでしゅとくできる
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(atof(key.c_str())))
        {//対象フラグが立っているので補完画像名を格納
            auto fileNames=value.second.asValueVector();//secondでvalue取得できる
            
            for (auto&nameValue :fileNames) {
                removes.push_back(nameValue.asString());
            }
        }
    }
    
    return removes;
}

int DataManager::getZorderBySupName(std::string supName, int original,int backNum)
{
    auto map=getBackData(backNum);
    auto value=map["zOrders"];
    if (!value.isNull()) {
        auto vec=value.asValueVector();
        for (auto v :vec) {
            auto vMap=v.asValueMap();
            if (vMap["key"].asString()==supName) {
                return vMap["value"].asInt();
            }
        }
    }
    
    //coverNameを確認
    auto names=map["coverNames"];
    if (!names.isNull()) {
        auto vec=names.asValueVector();
        for (auto name:vec) {
            if (name.asString()==supName) {
                return 1;
            }
        }
    }
    
    return original;
}

#pragma mark Custom
ValueMap DataManager::getCustomSupplementsMap(std::string customActionKey)
{
    int ID=getShowingItemID();
    ValueMap map;
    if (ID==0) {
        ID=getNowBack();
        map=getBackData(ID);
    }
    else{
        map=getItemData(ID);
    }
    return getCustomSupplementsMap(customActionKey, map);
}


ValueMap DataManager::getCustomSupplementsMap(std::string customActionKey, ValueMap map)
{
    auto value=map[customActionKey];
    if (value.isNull())
    {//補完画像データなし。からのmapを返す
        ValueMap empty;
        return empty;
    }
    
    return value.asValueMap();
}

ValueVector DataManager::getCustomSupplementsVector(std::string customActionKey, ValueMap map)
{
    auto value=map[customActionKey];
    if (value.isNull())
    {//補完画像データなし。からのmapを返す
        ValueVector empty;
        return empty;
    }
    
    return value.asValueVector();
}

bool DataManager::checkCustomActionTouchable(cocos2d::ValueMap map)
{
    auto touchable=true;
    if (getEnableFlagWithFlagID(map["disableFlagID"].asFloat())){
        touchable=false;
    }
    
    return touchable;
}

#pragma mark Particle
ValueVector DataManager::getParticleVector(bool onItem, int num)
{
    ValueMap map;
    if (onItem) {
        map=getItemData(num);
    }
    else{
        map=getBackData(num);
    }
    
    auto supplement=map["particles"];
    if (supplement.isNull())
    {//補完画像データなし。からのmapを返す
        ValueVector empty;
        return empty;
    }
    
    return supplement.asValueVector();
}

#pragma mark パネル広告
ValueMap DataManager::getPanelData(int num)
{
    auto paneldata = getPlistMapWithKey(FilePath_PanelAd);
    auto map=paneldata[StringUtils::format("%d",num)];
    
    if (map.isNull())
    {//表示中の背景にパネル広告を配置しない。空のmapを返す
        ValueMap empty;
        return empty;
    }
    
    return map.asValueMap();
}

#pragma mark photo
#pragma mark photo
void DataManager::setValueToPhotoMap(int backID)
{
    setValueToPhotoMap(backID, CustomAction_SwitchSprite);
    setValueToPhotoMap(backID, CustomAction_SliderSprite);
}

void DataManager::setValueToPhotoMap(int backID, std::string actionKey)
{
    if (actionKey==CustomAction_SwitchSprite) {
        for (auto switchSpr: SupplementsManager::getInstance()->switchSprites) {
            switchSpr->save(ShowType_OnPhoto);
        }
    }
    else if (actionKey==CustomAction_SliderSprite) {
        for (auto sliderSpr: SupplementsManager::getInstance()->sliderSprites) {
            sliderSpr->save(true);
        }
    }
}

#pragma mark- reset
void DataManager::resetAllData(bool deleteData)
{//データをすべてリセット
    AnswerManager::getInstance()->resetPasscode(false);
    AnswerManager::getInstance()->resetPasscode(true);
    AnswerManager::getInstance()->clear();
    WarpManager::getInstance()->clear();

    setNowBack(1,ReplaceTypeNone);
    setSelectedItemID(0);
    setShowingItemID(0);
    setShowingAngle(0);
    setShowingPhotoID(0);
    setIsShowingSideBar(false);
    
    items.clear();
    flagMap.clear();
    temporaryFlagMap.clear();
    otherFlagMap.clear();
    recordData.clear();
    releasedHintMap.clear();
    mysteryMap.clear();
    mysteryMap_onItem.clear();
    
    //staticで取得していた、データをマップで保持。データ削除時に消す。
    plistDataMap.clear();
    
    //保持データを消す
    flagFileData.clear();
    flagIDforHint=0;

    //セーブ消去
    if(deleteData){
        FileUtils::getInstance()->removeFile(EscapeDataManager::getInstance()->getPlayTitleDataDirWithFile(FilePath_FlagData));
        FileUtils::getInstance()->removeFile(EscapeDataManager::getInstance()->getPlayTitleDataDirWithFile(FilePath_ItemData));
        FileUtils::getInstance()->removeFile(EscapeDataManager::getInstance()->getPlayTitleDataDirWithFile(FilePath_OtherFlagData));
        FileUtils::getInstance()->removeFile(EscapeDataManager::getInstance()->getPlayTitleDataDirWithFile(FilePath_RecordData));
    }
}

void DataManager::resetMinigameData()
{//startFlagより大きいflagを削除する
    auto startFlag=getMiniGameData()["startFlagID"].asInt();
    
    std::vector<std::string>removeKeys;
    
    for (auto v:flagMap) {
        auto key=v.first;
        if (atoi(key.c_str())>startFlag) {
            //ここでeraseするとおかしくなるので格納するだけ
            removeKeys.push_back(key);
        }
    }
    
    for (auto key :removeKeys) {
        flagMap.erase(key);
    }
    
    //指定の立てるフラグがあれば
    if (!getMiniGameData()["setOtherFlag"].isNull()) {
        log("指定のotherFlagをたてる");
        auto otherMap=getMiniGameData()["setOtherFlag"].asValueMap();
        DataManager::sharedManager()->setOtherFlag(otherMap["key"].asString(), otherMap["value"].asInt());
    }
    
    saveFlagData();
}

#pragma mark- 記録管理
#pragma mark プレイデータ毎に持つ情報
void DataManager::loadAllRecordData()
{
    int titleSize = (int)EscapeDataManager::getInstance()->getTitleDataSortedVector().size();
    for (int i=0; i<titleSize; i++) {//タイトル名を全て検索
        auto fileName = EscapeDataManager::getInstance()->getTitleNameWithIndex(i);
        
        //リスタートで消えるデータ
        auto recordDataPath = EscapeDataManager::getInstance()->getTitleDataDirWithIndexAndFile(i, FilePath_RecordData);
        auto recordDataValueMap = FileUtils::getInstance()->getValueMapFromFile(recordDataPath);
        allRecordData[fileName] = recordDataValueMap;

        //リスタートで消えないデータ
        auto eternalRecordDataPath = EscapeDataManager::getInstance()->getTitleDataDirWithIndexAndFile(i, FilePath_EternalRecordData);
        auto eternalRecordDataValueMap = FileUtils::getInstance()->getValueMapFromFile(eternalRecordDataPath);
        allEternalRecordData[fileName] = eternalRecordDataValueMap;
    }
    
    //読み込み終わったらマイグレーションチェック
    migrationRecordData();
}

void DataManager::migrationRecordData()
{
    auto migration_clear = [this](const char* recordKey, ValueMap& recordMap, ValueMap& eternalRecordMap)->bool{
        
        auto isClearValue = recordMap[recordKey];
        auto isClear = (isClearValue.isNull())? false: isClearValue.asBool();
        
        if (isClear) {
            eternalRecordMap[recordKey] = true;
            recordMap.erase(recordKey);
        }
        
        return isClear;
    };
    
    if (UserDefault::getInstance()->getBoolForKey("Migration")) return;
    
    int titleSize = (int)EscapeDataManager::getInstance()->getTitleDataSortedVector().size();

    for (int i=0; i<titleSize; i++) {
        auto fileName = EscapeDataManager::getInstance()->getTitleNameWithIndex(i);
        auto recordMap = allRecordData[fileName];
        auto eternalRecordMap = allEternalRecordData[fileName];
        
        //クリアデータを持っているかどうか確認
        auto isClearMigration = migration_clear(RecordKey_ClearGame, recordMap, eternalRecordMap);
        
        //ミニゲームクリアデータを持っているかどうか確認
        auto isMiniClearMigration = migration_clear(RecordKey_ClearMiniGame, recordMap, eternalRecordMap);
       
        if (isClearMigration || isMiniClearMigration) {
            ValueHelper::dumpValue(Value(recordMap));
            ValueHelper::dumpValue(Value(eternalRecordMap));
            
            allRecordData[fileName] = recordMap;
            allEternalRecordData[fileName] = eternalRecordMap;
            
            EscapeDataManager::getInstance()->saveData(i, FilePath_RecordData, Value(recordMap));
            EscapeDataManager::getInstance()->saveData(i, FilePath_EternalRecordData, Value(eternalRecordMap));
        }
    }
    UserDefault::getInstance()->setBoolForKey("Migration", true);
}

void DataManager::loadRecordData()
{
    auto titleName =  EscapeDataManager::getInstance()->getPlayTitleName();
    
    auto isExistKeyInMap = [this](const char* key)->bool{
        return (allRecordData.find(key) != allRecordData.end());
    };
    
    if (!isExistKeyInMap(titleName.c_str())) {//データがないなら
        auto path = EscapeDataManager::getInstance()->getPlayTitleDataDirWithFile(FilePath_RecordData);
        //ValueMapとして読み込む
        recordData = FileUtils::getInstance()->getValueMapFromFile(path);
    }
    else {
        //ValueMapとして読み込む
        recordData = allRecordData.at(titleName);
    }
    
    log("記録データをローディングしました。%s",titleName.c_str());
}

bool DataManager::isClearStage(int index, const char *recordKey)
{
    auto fileName = EscapeDataManager::getInstance()->getTitleNameWithIndex(index);
    auto indexTitleMap = allEternalRecordData[fileName];
    auto clearValue = indexTitleMap[recordKey].asBool();
    
    return clearValue;
}

TitleCondition DataManager::getClearTitleCondition(int index)
{
    auto typeCondition = EscapeDataManager::getInstance()->getTypeTitleCondition(index);
    
    if (!EscapeDataManager::getInstance()->hasClearDataTitleCondition(typeCondition)) {//クリアデータを持たないタイプ。広告 or 投票の時は。
        return TitleCondition_None;
    }
    
    if (DataManager::sharedManager()->isClearStage(index, RecordKey_ClearMiniGame)) {//このステージはミニゲームまでクリアしてる
        return TitleCondition_CLEAR_MINIGAME;
    }
    else if (DataManager::sharedManager()->isClearStage(index, RecordKey_ClearGame)) {//ミニゲームまではプレイしていないけど、本編はクリアしている。
        return TitleCondition_CLEAR;
    }
    else {
        auto path = EscapeDataManager::getInstance()->getTitleDataDirWithIndexAndFile(index, FilePath_FlagData);
    
        auto isPlaying = FileUtils::getInstance()->isFileExist(path);
        
        if (isPlaying) {
            return TitleCondition_ISPLAYING;
        }
    }
    
    return TitleCondition_Unplayed;
}

void DataManager::record(RecordType recordType)
{
    if (recordType==RecordType_StartDate||recordType==RecordType_StopDate)
    {//日時を記録 差分を計算しTimeを加算
        auto recordedDate=recordData[RecordKey_Date].asInt();
        auto now=time(NULL);
        
        auto time=recordData[RecordKey_Time].asInt();
        if (recordedDate>0) {
            time+=(now-recordedDate);
            time=MAX(0, time);
        }
        log("時間計測開始 recorded:%d now:%ld time:%d",recordedDate,now,time);
        
        recordData[RecordKey_Time]=time;
        
        if (recordType==RecordType_StartDate) {
            recordData[RecordKey_Date]=(int)now;
        }
        else{
            log("時間計測停止");
            recordData[RecordKey_Date]=0;
        }
    }
    else{//ヒントを記録
        auto count=recordData[RecordKey_Hint].asInt();
        recordData[RecordKey_Hint]=count+1;
    }
    
    //保持しているレコードデータマップに上書き
    auto playTitleName = EscapeDataManager::getInstance()->getPlayTitleName();
    allRecordData[playTitleName] = recordData;
    
    EscapeDataManager::getInstance()->saveDataOnPlay(FilePath_RecordData, Value(recordData));
}

Value DataManager::getRecordData(RecordType recordType)
{
    Value result = recordData[transformRecordTypeToKey(recordType)];
    
    return result;
}

std::string DataManager::transformRecordTypeToKey(RecordType recordType)
{
    std::string recordKey;
    switch (recordType) {
        case RecordType_StartDate:
        case RecordType_StopDate:
            recordKey = RecordKey_Time;
            break;
        case RecordType_Hint:
            recordKey = RecordKey_Hint;
            break;
        default:
            log("引数が意図していないタイプ。要確認%d",recordType);
            assert(false);
            
            break;
    }
    
    return recordKey;
}

void DataManager::testAllClear()
{
    Common::performProcessForDebug([this](){
        auto isExistKeyInMap = [this](const char* key)->bool{
            return (allEternalRecordData.find(key) != allEternalRecordData.end());
        };
        
        int count = (int)EscapeDataManager::getInstance()->getTitleDataSortedVector().size();
        for (int i = 0; i<count; i++) {
            auto titleName = EscapeDataManager::getInstance()->getTitleNameWithIndex(i);
            
            ValueMap recordMap;
            if (!isExistKeyInMap(titleName.c_str())) {//データがないなら
                auto path = EscapeDataManager::getInstance()->getPlayTitleDataDirWithFile(FilePath_RecordData);
                //ValueMapとして読み込む
                recordMap = FileUtils::getInstance()->getValueMapFromFile(path);
            }
            else {
                //ValueMapとして読み込む
                recordMap = allEternalRecordData.at(titleName);
            }
            
            if (EscapeDataManager::getInstance()->hasClearDataTitleCondition(EscapeDataManager::getInstance()->getTypeTitleCondition(i))) {
                recordMap[RecordKey_ClearMiniGame]=true;
            }
            
            //保持しているレコードデータマップに上書き
            allEternalRecordData[titleName] = recordMap;
            
            EscapeDataManager::getInstance()->saveData(i, FilePath_EternalRecordData, Value(recordMap));
        }
        
    }, nullptr);
}

int DataManager::getTotalStageRecord(const char* recordKey)
{
    int totalCount = 0;
    int count = (int)EscapeDataManager::getInstance()->getTitleDataSortedVector().size();
    for (int i = 0; i<count; i++) {
        
        int intValue = 0;
        auto boolValue = isClearStage(i, recordKey);
        intValue = (int)boolValue;
        
        //log("%sの%s状態は%d", EscapeDataManager::getInstance()->getTitleNameWithIndex(i).c_str(), recordKey, intValue);
        totalCount+=intValue;
    }
    
    log("%d個のステージをクリアしている。",totalCount);
    return totalCount;
}

#pragma mark プレイタイトル毎に持つ情報
Value DataManager::getEternalRecordDataWithIndex(int index, const char *recordKey)
{//
    auto fileName = EscapeDataManager::getInstance()->getTitleNameWithIndex(index);
    auto recordDataOfIndex =  allEternalRecordData[fileName];

    log("[%s]のデータを表示しまう",fileName.c_str());
    ValueHelper::dumpValue(Value(recordDataOfIndex));
    
    return recordDataOfIndex[recordKey];
}

Value DataManager::getEternalRecordDataOnPlay(const char *recordKey)
{//
    auto playFileName = EscapeDataManager::getInstance()->getPlayTitleName();
    auto recordData = allEternalRecordData[playFileName];
    
    log("[%s]のデータを表示しまう;;",playFileName.c_str());
    ValueHelper::dumpValue(Value(recordData));
    return recordData[recordKey];
}

void DataManager::recordEternalDataOnPlay(const char *recordKey, Value value)
{
    auto playFileName = EscapeDataManager::getInstance()->getPlayTitleName();
    auto map = allEternalRecordData[playFileName];
    map[recordKey] = value;
    
    EscapeDataManager::getInstance()->saveDataOnPlay(FilePath_EternalRecordData, Value(map));
    allEternalRecordData[playFileName] = map;
}

void DataManager::recordEternalDataMapOnPlay(ValueMap savedMap)
{
    auto playFileName = EscapeDataManager::getInstance()->getPlayTitleName();
    auto map = allEternalRecordData[playFileName];

    for (auto pair : savedMap) {
        map[pair.first] = pair.second;
    }
    
    log("永久データを保存しました");
    ValueHelper::dumpValue(Value(map));
    
    EscapeDataManager::getInstance()->saveDataOnPlay(FilePath_EternalRecordData, Value(map));

    allEternalRecordData[playFileName] = map;
}

TitleCondition DataManager::getIsValidTimeCondition(int index)
{
    //ロックされるまで7日を切っていたら、TitleCondition_COUNTDOWN_TOLOCKED
    auto daysToLocked = getDaysToLocked(index);
    if (1 <= daysToLocked && daysToLocked <= Period_Unlocked + 1) {
        return TitleCondition_COUNTDOWN_TOLOCKED;
    }
    
    auto condition = EscapeDataManager::getInstance()->getReleaseTitleCondition(index);
    auto isValidC = isValidTimeCondition(index, condition)? condition : TitleCondition_None;
    
    log("インデックス%dコンディション%d::有効かどうか%d", index, condition, isValidC);
    
    
    return isValidC;
}

bool DataManager::isValidTimeCondition(int index, TitleCondition condition)
{
    std::string mapkey = "";
    bool isTime = true;
    switch (condition) {
        case TitleCondition_Pre:
            mapkey = MapKey_VideoDate;
            break;
        case TitleCondition_New:
            mapkey = MapKey_ReleaseDate;
            break;
        case TitleCondition_LOCKED:
            mapkey = MapKey_LockedDate;
            break;
      
        default:
            isTime = false;//広告とかチュートリアルとか
            break;
    }
    
    if (!isTime) {
        return false;
    }
    
    return checkTimeCondition(index, mapkey.c_str());
}

bool DataManager::checkTimeCondition(int index, const char *key)
{
    bool condition = true;
    if (!strcmp(key, MapKey_ReleaseDate)) {
        auto playcountValue = getEternalRecordDataWithIndex(index, RecordKey_PlayCount);
        if (!playcountValue.isNull() && playcountValue.asInt()>0)//データがあって,プレイしたことがあれば
            condition = false;
    }
    else if (!strcmp(key, MapKey_VideoDate)) {
        auto isWatchedValue = getEternalRecordDataWithIndex(index, RecordKey_PreMovie);
        if (!isWatchedValue.isNull() && isWatchedValue.asBool())//データがあって、視聴していたら。
            condition = false;
    }
    else if (!strcmp(key, MapKey_LockedDate)) {
        int nowTime = (int)Common::dateWithTimeStamp(TimeUnit_Day, FireBaseBridge::getInstance()->getServerTimestamp());//Common::dateWithTimeStamp(FireBaseBridge::getInstance()->getServerTimestamp());
        time_t unlockedTimeStamp = getEternalRecordDataWithIndex(index, RecordKey_UnlockedTimeStamp).asDouble();
        int unlocked_deadline = (int)Common::dateWithTimeInterval(TimeUnit_Day, unlockedTimeStamp, 60*60*24*Period_Unlocked);//Common::dateWithTimeIntervalDate(unlockedTimeStamp, Period_Unlocked);
        
        log("[%s]のアンロック検証%d,::::%d", EscapeDataManager::getInstance()->getTitleNameWithIndex(index).c_str(), nowTime, unlocked_deadline);
        
        if (nowTime <= unlocked_deadline)//アンロックの期間なら。
            condition = false;
    }
    else {
        log("%s:の条件をチェック",key);
    }
    
   // log("%s:の条件をチェック→%d",key ,condition);
    
    return condition;
}

bool DataManager::isUpdateCondition(int index)
{
    //サーバーから落としたデータ
    auto titleDlMap = EscapeDataManager::getInstance()->getTitleMapWithIndex(index);
    auto titleVersionSt = titleDlMap[MapKey_TitleVersion].asString();
    int titleVersion = atoi(titleVersionSt.c_str());
    
    //プレイデータとして保存されているデータ
    auto lastplayVersionSt = getEternalRecordDataWithIndex(index, RecordKey_LastPlayVersion).asString();
    int lastplayVersion = atoi(lastplayVersionSt.c_str());
    
    log("%d,,,%d,,,%d", lastplayVersion < titleVersion , titleVersion > 1 , lastplayVersion > 0);
    
    return (lastplayVersion < titleVersion && titleVersion > 1 && lastplayVersion > 0);
}

bool DataManager::getPlayableWithNoCoins(int index)
{
    auto typeCondition = EscapeDataManager::getInstance()->getTypeTitleCondition(index);
    if (EscapeDataManager::getInstance()->conditionOfFixedTitle(typeCondition)) {//広告かチュートリアルなら、プレイ促進しない。
        return false;
    }
    
    auto clearCondition = getClearTitleCondition(index);
    if (clearCondition == TitleCondition_CLEAR || clearCondition == TitleCondition_CLEAR_MINIGAME) {//クリアしていたら、プレイ促進しない
        return false;
    }
    
    auto isRepearing = EscapeDataManager::getInstance()->isRepairing(index);
    if (isRepearing) {//修理中なら、プレイ促進しない。
        return false;
    }

    auto releaseCondition = getIsValidTimeCondition(index);
    if (!(releaseCondition == TitleCondition_Pre || releaseCondition == TitleCondition_LOCKED)) {//先行プレイとロック期間以外の時
        return true;
    }
    
    return false;
}

int DataManager::getDaysToLocked(int index)
{
    int days = -1;
    
    auto type = EscapeDataManager::getInstance()->getTypeTitleCondition(index);
    if (EscapeDataManager::getInstance()->conditionOfFixedTitle(type)) {
        return days;
    }
    
    long long nowTime = Common::dateWithTimeStamp(TimeUnit_Day, FireBaseBridge::getInstance()->getServerTimestamp());//Common::dateWithTimeStamp(FireBaseBridge::getInstance()->getServerTimestamp());
    time_t unlockedTimeStamp = getEternalRecordDataWithIndex(index, RecordKey_UnlockedTimeStamp).asDouble();
    if (unlockedTimeStamp > 0) {//アンロック期間中。アンロック締め切りまでの日数を計算。
        long long unlockedPeriod = Common::dateWithTimeInterval(TimeUnit_Day, unlockedTimeStamp, 60*60*24*Period_Unlocked);//Common::dateWithTimeIntervalDate(unlockedTimeStamp, Period_Unlocked);
        if (unlockedPeriod < nowTime) {//アンロック期間を過ぎている。
            return days;
        }
        
        days = (int)(Common::differenceTime(nowTime, unlockedPeriod)/60/60/24)+1;//Common::differenceDays(unlockedPeriod, nowTime)+1;//当日も含める
    }
    else{//アンロックしたことない。LockedDateまでの日数を計算
        auto lockedDate = EscapeDataManager::getInstance()->getIntDateFromVersionPlist(index, MapKey_LockedDate, 0);
        days = (int)(Common::differenceTime(nowTime, lockedDate)/60/60/24)+1;//Common::differenceDays(lockedDate, nowTime)+1;//当日も含める
    }
    
    return days;
}

#pragma mark- アクセサ
int DataManager::getNowBack()
{
    return nowBack;
}

void DataManager::setNowBack(int var, ReplaceType replaceType)
{
    nowBack=var;
    log("Back : %d",nowBack);

    //通知を発行
    auto event=EventCustom(NotificationKeyWillChangeBack);
    auto data=Value(replaceType);
    event.setUserData(&data);

    Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
}

int DataManager::getSelectedItemID()
{
    return selectedItemID;
}

void DataManager::setSelectedItemID(int _selectedItemID)
{
    selectedItemID=_selectedItemID;
    
    //通知を発行
    auto event=EventCustom(NotificationKeyChangeSelectedItemID);
    Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
}

#pragma mark- プライベート
ValueMap DataManager::getPlistMapWithKey(const char *key)
{
    auto isExistKeyInPlistMap = [this](const char* key)->bool{
        return (plistDataMap.find(key) != plistDataMap.end());
    };
    
    if (!isExistKeyInPlistMap(key)) {//データがないなら
        auto path = EscapeDataManager::getInstance()->getPlayTitleResourceDirWithFile(key);
        
        log("パスは%s",path.c_str());
        
        auto map = FileUtils::getInstance()->getValueMapFromFile(path);
        plistDataMap.insert({key, map});
    }
    return plistDataMap.at(key);
}

#pragma mark ユーザー
void DataManager::loadUserID()
{
    setUserID(UserDefault::getInstance()->getStringForKey(UserKey_UserID));
}

void DataManager::setUserID(std::string var)
{
    userID=var;
    UserDefault::getInstance()->setStringForKey(UserKey_UserID, var.c_str());
}

std::string DataManager::getUserID()
{
    return userID;
}

bool DataManager::getIsPaidUser()
{
    if ((UserDefault::getInstance()->getIntegerForKey(UserKey_DailyBonusCoin, 0) != 0)) {
        UserDefault::getInstance()->setBoolForKey(UserKey_IsPaidUser, true);
    }
    
    return UserDefault::getInstance()->getBoolForKey(UserKey_IsPaidUser);
}

void DataManager::postUserData()
{
    if (getUserID().size()==0) {
        return;
    }
    
    auto dir = StringUtils::format("%s_%s%s/", Users, FirebaseDownloadDir, getUserID().c_str());
    
    std::map<std::string, int>map={
        {"coins",getCoin()},
        {"clearStages",getTotalStageRecord(RecordKey_ClearGame)},
        {"paid",getIsPaidUser()},
        {"prePlay",getNowPoint(Achievement_PrePlay)}//,
        //{"timestamp",FireBaseBridge::getInstance()->getServerTimestamp()}
    };
    
    FireBaseBridge::getInstance()->setUserProperty("Coins", StringUtils::format("%d",getCoin()).c_str());
    
    log("ユーザーズのデータは%s",dir.c_str());
    //丸ごと上書き
    /*
    FireBaseBridge::getInstance()->setValue(dir, map, [](bool successed){
        log("トランザクションは終わりです。%d",successed);
    });*/
}

#pragma mark- コイン関連
void DataManager::loadingCoin()
{
    m_coin = UserDefault::getInstance()->getIntegerForKey(UserKey_Coin, 0);
}

void DataManager::addCoin(int added)
{
    m_coin += added;
    
    UserDefault::getInstance()->setIntegerForKey(UserKey_Coin, m_coin);
    
    //コインの量に増減があった場合は変更通知を飛ばす
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyChangedOwnedCoin);
    
    //postし直す
    postUserData();
}

int DataManager::getCoin()
{
    return m_coin;
}

bool DataManager::isEnoughCoin(int used)
{
    return (m_coin >= used);
}

void DataManager::useCoin(int used)
{    
    addCoin(used*-1);
}

std::string DataManager::getCoinSt()
{
    float num = (float)m_coin;
    return NativeBridge::transformThreeComma(num);
}

void DataManager::addChangeCoinNotification(cocos2d::Node *parent, const std::function<void (EventCustom *)> &callback)
{
    //通知を登録します。
    auto changeOwnCoinListner=EventListenerCustom::create(NotificationKeyChangedOwnedCoin, callback);
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(changeOwnCoinListner, parent);
}

#pragma mark- ログインボーナス関連
bool DataManager::isSavedDateNew(std::string key)
{
    time_t lastDate = UserDefault::getInstance()->getDoubleForKey(key.c_str(), 0);
    auto lld_long = Common::transformTimeToDate(lastDate);//server_tmを生成前に変換しておかないと、なぜかlastLogin_tmが書き換わってしまう。

    time_t serverTime = FireBaseBridge::getInstance()->getServerTimestamp();
    if (serverTime != 0) {//サーバーの時刻をしっかりと取得できている。
        auto sd_long = Common::transformTimeToDate(serverTime);
        
        if (lld_long < sd_long) {//現在時刻の方が進んでいたら
            return true;
        }
    }
    return false;
}

void DataManager::saveDate(std::string key)
{
    auto serverDate = FireBaseBridge::getInstance()->getServerTimestamp();
    log("%s,受け取り サーバーdateを保存します%f",__func__,serverDate);
    UserDefault::getInstance()->setDoubleForKey(key.c_str(), serverDate);
}

void DataManager::incrementLoginCount()
{
    UserDefault::getInstance()->setIntegerForKey(UserKey_LoginDayCount, UserDefault::getInstance()->getIntegerForKey(UserKey_LoginDayCount)+1);
}

#pragma mark- 初心者パック購入時
void DataManager::purchasedBeginnersPack(int dailyBonusCoin, int dailyPeriod)
{
    auto serverDate = FireBaseBridge::getInstance()->getServerTimestamp();

    UserDefault::getInstance()->setDoubleForKey(UserKey_GotDailyBonusDate, serverDate);
    UserDefault::getInstance()->setIntegerForKey(UserKey_DailyBonusCount, dailyPeriod);
    UserDefault::getInstance()->setIntegerForKey(UserKey_DailyBonusCoin, dailyBonusCoin);
    
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyAchievementBadge);
}

bool DataManager::canGotDailyCoin()
{
    auto bonusCount = UserDefault::getInstance()->getIntegerForKey(UserKey_DailyBonusCount);
    if (bonusCount == 0) return false;//デイリーボーナスを貰い切った。

    auto todayServerDate = FireBaseBridge::getInstance()->getServerTimestamp();
    auto lastGotDate = UserDefault::getInstance()->getDoubleForKey(UserKey_GotDailyBonusDate);
    
    auto todayServerLongDate = Common::transformTimeToDate(todayServerDate);
    auto lastGotLongDate = Common::transformTimeToDate(lastGotDate);
    log("本日は%lld,最後に受け取ったのは%lld",todayServerLongDate, lastGotLongDate);
    if (todayServerLongDate <= lastGotLongDate) return false;
    
    return true;
}

void DataManager::gotDailyBonus()
{
    auto bonusCount = UserDefault::getInstance()->getIntegerForKey(UserKey_DailyBonusCount)-1;
    UserDefault::getInstance()->setIntegerForKey(UserKey_DailyBonusCount, bonusCount);
    
    //受け取った日にちを取得
    auto todayServerDate = FireBaseBridge::getInstance()->getServerTimestamp();
    UserDefault::getInstance()->setDoubleForKey(UserKey_GotDailyBonusDate, todayServerDate);
    
    int dailyCoin = UserDefault::getInstance()->getIntegerForKey(UserKey_DailyBonusCoin);
    addCoin(dailyCoin);
    
    log("デイリーボーナスコインを受け取った%d::%f::%d", bonusCount, todayServerDate, dailyCoin);    
}

#pragma mark- プレイ中のヒント促進
bool DataManager::checkAutoPromoteHint()
{
    if (getIsPromoteHint()){//一度ヒントに促したら、もうやらない。
        return false;
    }
    
    if (EscapeDataManager::getInstance()->getTypePlayTitleCondition() != TitleCondition_TUTORIAL) {
        auto elapsedTime=time(NULL)-getEnFlagTime();
        //log("%s 経過時間 %ld",__func__,elapsedTime);
        if (elapsedTime>AutoPromoteHint) {
            setIsPromoteHint(true);
            return true;
        }
    }
    
    return false;
}

#pragma mark - アチーブメント
std::vector<Achievement> DataManager::getAchievements()
{
    std::vector<Achievement>vec;
    for (int i=Achievement_Login; i<=Achievement_BuyBeginnersPack; i++) {
        vec.push_back((Achievement)i);
    }
    
    return vec;
}

ValueMap DataManager::getAchievementMap()
{
    static auto map=FileUtils::getInstance()->getValueMapFromFile(FilePath_Achievement);
    return map;
}

std::string DataManager::getAchievementThumbnailName(Achievement achievement)
{
    auto key=getAchievementKey(achievement);
    auto map=getAchievementMap()[key].asValueMap();
    auto fileName=map["thumbnail"].asString();
    if (fileName.size()==0) {
        fileName="pig_face.png";
    }
    return fileName;
}

std::string DataManager::getAchievementString(Achievement achievement)
{
    auto key=getAchievementKey(achievement);
    
    auto map=getAchievementMap()[key].asValueMap();
    auto point=getAchievementPoint(achievement);
    return StringUtils::format(map["text"].asValueMap()[Common::getLocalizeCode().c_str()].asString().c_str(),point);
}

int DataManager::getAchievementPoint(Achievement achievement)
{
    auto key=getAchievementKey(achievement);
    
    auto map=getAchievementMap()[key].asValueMap();
    auto points=map["achievePoints"].asValueVector();//達成報酬を得られるポイントの配列
    
    auto key_gotAchieve=getGotAchievementKey(achievement);
    
    //取得済みのポイントを取得
    int gotAchievementPoint=UserDefault::getInstance()->getIntegerForKey(getGotAchievementKey(achievement).c_str());

    for (auto value : points) {
        if (value.asInt()>gotAchievementPoint) {
            return value.asInt();
        }
    }
    
    //全て達成済みの場合は0を返す
    return 0;
}

int DataManager::getNowPoint(Achievement achievement)
{
    if (achievement==Achievement_StageClear)
    {//recordDataからの読み込み
        return getTotalStageRecord(RecordKey_ClearGame);
    }
    else if (achievement==Achievement_MinigameClear)
    {//recordDataからの読み込み
        return getTotalStageRecord(RecordKey_ClearMiniGame);
    }
    else if(achievement==Achievement_BuyBeginnersPack)
    {//1or0
        return (UserDefault::getInstance()->getIntegerForKey(UserKey_DailyBonusCoin, 0) != 0);
    }
    else
    {//UserDefaultからの読み込み
        return UserDefault::getInstance()->getIntegerForKey(getAchievementKey(achievement).c_str());
    }
}

void DataManager::setAchievementPoint(Achievement achievement)
{//達成した項目を記録
    auto key_gotAchieve=getGotAchievementKey(achievement);
    int point=getAchievementPoint(achievement);
    UserDefault::getInstance()->setIntegerForKey(key_gotAchieve.c_str(), point);
    
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyAchievementBadge);
}

std::string DataManager::getAchievementKey(Achievement achievement)
{
    switch (achievement) {
        case Achievement_Login:
            return UserKey_LoginDayCount;
            break;
        case Achievement_TotalStampByMovie:
            return UserKey_TotalStampByMovie;
            break;
        case Achievement_PrePlay:
            return UserKey_PrePlayCount;
            break;
        case Achievement_StageClear:
            return "StageClear";
            break;
        case Achievement_MinigameClear:
            return "MinigameClear";
            break;
        case Achievement_BuyBeginnersPack:
            return "BuyBeginnersPack";
            break;
        default:
            break;
    }
    
    return "";
}

std::string DataManager::getGotAchievementKey(Achievement achievement)
{
    auto key=getAchievementKey(achievement);
    return StringUtils::format("GotAchieve%s",key.c_str());
}

bool DataManager::existAchievableItems()
{
    auto exist=false;
    for (auto achievement : getAchievements()) {
        auto nowP=getNowPoint(achievement);
        auto p=getAchievementPoint(achievement);
        if (nowP>=p&&p!=0) {
            exist=true;
            break;
        }
    }
    return exist;
}
