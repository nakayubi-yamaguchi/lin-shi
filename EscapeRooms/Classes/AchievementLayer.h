//
//  SettingLayer.h
//  EscapeRooms
//
//  Created by yamaguchinarita on 2017/11/22.
//
//

#ifndef _____EscapeContainer________AchievementLayer_____
#define _____EscapeContainer________AchievementLayer_____

#include "cocos2d.h"
#include "Utils/CustomAlert.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "Utils/PurchaseLayer.h"
#include "DataManager.h"
USING_NS_CC;
USING_NS_CC_EXT;

class AchievementLayer : public CustomAlert, TableViewDelegate, TableViewDataSource
{
public:
    static AchievementLayer* create(const ccMenuCallback &callback);
    virtual bool init(const ccMenuCallback &callback);
       
private:
    /**達成項目内容 対象のキーを格納*/
    std::vector<Achievement>achievements;
    std::vector<std::string>achievementStrings;//tableに表示するtext項目
    std::vector<int>achievementNowPoints;//gaugeに表示する
    std::vector<int>achievementPoints;//gaugeに表示する
    Vector<Sprite*>achievementThumnails;//thumbnailを

    TableView *m_tableView;
    Size cellSize;
    
    void reloadData();
    void createUI();
    
    void pushAchieve(ssize_t idx);
    
    // TableViewDelegateがScrollViewDelegateを継承している事情で必要
    virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view) override{};
    virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) override{};
    
    /** セルのサイズを設定*/
    virtual Size cellSizeForTable(TableView* table) override;
    /** セルの中身を設定*/
    virtual TableViewCell* tableCellAtIndex(TableView* table,ssize_t idx) override;
    /** セルの数を設定*/
    virtual ssize_t numberOfCellsInTableView(TableView* table) override;
    
    /** セルが選択された時*/
    virtual void tableCellHighlight(TableView* table, TableViewCell* cell) override;
    /** セルが選択解除された時*/
    virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell) override;
    /** セルをタップしたときの処理*/
    virtual void tableCellTouched(TableView* table,TableViewCell* cell) override;
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
