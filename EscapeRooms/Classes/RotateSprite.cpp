//
//  RotateSprite.cpp
//  Yashiki
//
//  Created by 成田凌平 on 2017/04/13.
//
//

#include "RotateSprite.h"
#include "Utils/Common.h"
#include "DataManager.h"

using namespace cocos2d;

RotateSprite* RotateSprite::create(std::string name, Size parentSize, ValueMap map, Value answerV, int number, std::string temporaryKey)
{
    auto node=new RotateSprite();
    if (node&&node->init(name,parentSize,map,answerV,number,temporaryKey)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    
    return node;
}

bool RotateSprite::init(std::string name, Size parentSize, ValueMap map, Value answerV, int number, std::string temporaryKey)
{
    if (!map["name"].isNull()) {
        name=map["name"].asString();
    }
    
    //.png部分をカット 他と形式を合わせる&過去の作品との互換性保持のため
    if (name.compare(name.length()-4,4,".png")==0) {
        name=name.substr(0,name.length()-4);
    }
    
    if (!AnchorSprite::init(Vec2(map["x"].asFloat(), map["y"].asFloat()), parentSize,false, StringUtils::format("%s.png",name.c_str()))) {
        return false;
    }
    
    setScale(parentSize.width/getContentSize().width);
    setFilename(name);
    setName(StringUtils::format("%s.png",name.c_str()));
    setNumber(number);//何番目のspriteか
    setDuration(1);
    if (!map["duration"].isNull()) {
        setDuration(map["duration"].asFloat());
    }
    //se指定
    if(!map["playSE"].isNull()){
        setSE(map["playSE"].asString());
    }
    //angle 1移動あたりの回転角
    setAngle(360);
    if (!map["angle"].isNull()) {
        setAngle(map["angle"].asFloat());
    }
    //index
    setMaxIndex(360/angle);
    
    //autoReverse
    if (!map["autoReverse"].isNull()) {
        autoReverseMap=map["autoReverse"].asValueMap();
    }
    //連動させるフラグがある時
    auto flagKey=map["temporaryFlagKey"].asString();
    if (flagKey.size()==0&&
        temporaryKey.size()>0) {
        //動的に生成するパターン
        flagKey=StringUtils::format("%s_%d",temporaryKey.c_str(),number);
    }
    setCustumFlagKey(flagKey);
    
    log("rotateのキー %s",getCustumFlagKey().c_str());
    
    //
    auto memory=map["memory"].asBool();
    setMemory(memory);
    
    //
    setFlashFileName(map["flashFileName"].asString());
    if (getFlashFileName().size()==0) {
        setFlashFileName(map["flashName"].asString());
    }
        
    //save読み込み
    if (getMemory()) {
        setIndex(DataManager::sharedManager()->getTemporaryFlag(getTemporaryKey(getFilename())).asInt());
        setRotation(getAngle()*getIndex());
    }
    
    //defaultでangleをセット(フラグによる)
    if (!map["angleWithFlag"].isNull()) {
        auto angleMap=map["angleWithFlag"].asValueMap();
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(angleMap["flag"].asInt())) {
            setRotation(angleMap["angle"].asFloat());
        }
    }
    
    //secure
    if (!answerV.isNull()) {
        if (answerV.asValueMap()["secure"].asBool()) {
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(answerV.asValueMap()["enFlagID"].asInt())) {
                auto answer=answerV.asValueMap()["answer"].asString();
                
                auto _index=atoi(answer.substr(number*2,2).c_str());
                setIndex(_index);
                setRotation(getAngle()*getIndex());
            }
        }
    }
    
    //
    auto removeFlag=map["removeFlagID"].asFloat();
    auto needFlag=map["needFlagID"].asFloat();
    if (DataManager::sharedManager()->getEnableFlagWithFlagID(removeFlag)||
        (needFlag>0&&!DataManager::sharedManager()->getEnableFlagWithFlagID(needFlag))) {
        setOpacity(0);
        setDisable(true);
    }
    
    return true;
}

#pragma mark- 回転
void RotateSprite::rotate(ccMenuCallback callback)
{
    if (getSE().size()>0) {
        Common::playSE(getSE().c_str());
    }
    
    Vector<FiniteTimeAction*>actions;
    
    auto rotateBy=EaseIn::create(RotateBy::create(getDuration(), getAngle()),1.25);
    actions.pushBack(rotateBy);
    if (autoReverseMap.size()>0) {
        auto delay=DelayTime::create(autoReverseMap["delay"].asFloat());
        actions.pushBack(delay);
        auto reverse=EaseIn::create(RotateBy::create(getDuration(), -getAngle()),1.25);
        actions.pushBack(reverse);
    }
    
    auto call=CallFunc::create([this,callback](){
        this->setIndex((this->getIndex()+1)%this->getMaxIndex());
        this->save();
        
        callback(NULL);
    });
    actions.pushBack(call);
    
    runAction(Sequence::create(actions));
}

#pragma mark - 保存系
std::string RotateSprite::getTemporaryKey(std::string name)
{
    if(getCustumFlagKey().size()>0)
    {//指定のキーがある
        return getCustumFlagKey();
    }
    
    auto key=StringUtils::format("rotate_%s",name.c_str());
    return key;
}

void RotateSprite::save()
{
    if (getMemory()) {
        auto key=getTemporaryKey(getFilename());
        DataManager::sharedManager()->setTemporaryFlag(key, getIndex());
    }
}
