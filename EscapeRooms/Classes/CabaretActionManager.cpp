//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "CabaretActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"

using namespace cocos2d;

CabaretActionManager* CabaretActionManager::manager =NULL;

CabaretActionManager* CabaretActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new CabaretActionManager();
    }
    return manager;
}

#pragma mark - Back
int CabaretActionManager::getDefaultBackNum()
{
    if (DataManager::sharedManager()->isShowedOpeningStory()) {
        return 1;
    }
    
    return 4;
}

void CabaretActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="pipe"&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(2))
    {//煙をプカプカ
        std::vector<int>indexs={0,1,2,2,1,0};//scale
        Vector<FiniteTimeAction*>actions_spawn;
        
        auto anchor=Vec2(.5, .38);
        auto distance=parent->getContentSize().height*.5;
        std::string soundName="pop.mp3";
        if (backID==16) {
            anchor=Vec2(.33, .5);
            distance*=.5;
            soundName="";
        }
        
        for (int i=0; i<indexs.size(); i++) {
            auto smoke=AnchorSprite::create(anchor, parent->getContentSize(),true, StringUtils::format("sup_%d_smoke.png",backID));
            parent->addChild(smoke);
            auto scale_original=smoke->getScale()*(1+indexs.at(i)*.5);
            smoke->setScale(.1);
            auto pos_original=smoke->getPosition();
            
            Vector<FiniteTimeAction*>spawns;
            auto seq_smoke=TargetedAction::create(smoke,Sequence::create(Spawn::create(Sequence::create(FadeTo::create(1,200),//fades
                                                                                                        DelayTime::create(1),
                                                                                                        FadeOut::create(2),
                                                                                                        NULL),
                                                                                       ScaleTo::create(.2*(indexs.at(i)+1), scale_original),//scale
                                                                                       createSoundAction(soundName.c_str()),
                                                                                       EaseOut::create(MoveBy::create(4, Vec2(0, distance)), 1.25),//move
                                                                                       NULL),
                                                                         MoveTo::create(.1, pos_original),
                                                                         ScaleTo::create(.1, .1),
                                                                         NULL));
            spawns.pushBack(seq_smoke);
            if (backID==17) {
                spawns.pushBack(createBounceAction(parent->getChildByName("sup_17_pig.png"), .3, .025));
            }
            
            actions_spawn.pushBack(Sequence::create(DelayTime::create(.5+i*1.25),
                                                    Spawn::create(spawns), NULL));
        }
        
        parent->runAction(Repeat::create(Sequence::create(Spawn::create(actions_spawn),
                                                          DelayTime::create(2), NULL), -1));
    }
    else if(key=="pole"&&DataManager::sharedManager()->getEnableFlagWithFlagID(5))
    {//pole移動 左、右 2回しよう
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(6)&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(17)) {
            return;
        }
        
        Node* pig_stand;
        bool isLeft=!DataManager::sharedManager()->getEnableFlagWithFlagID(17);
        Node* pig;
        auto distance=parent->getContentSize().height*.1;

        if (backID==3) {
            distance=parent->getContentSize().height*.05;
        }
        
        float up = -6;
        float center = -1;
        float down = 5;
        
        std::vector<float>indexs = {up, down, center, down, center, up};

        if (isLeft) {
            pig_stand=parent->getChildByName(StringUtils::format("sup_%d_pig_stand.png",backID));
            pig=parent->getChildByName(StringUtils::format("sup_%d_pig_pole.png",backID));
            pig->setOpacity(0);
        }
        else{
            pig_stand=parent->getChildByName(StringUtils::format("sup_%d_pig_chip.png",backID));
            pig=parent->getChildByName(StringUtils::format("sup_%d_pig_pole_1.png",backID));
            
            log("クラッッシュ%d",backID);
            pig->setOpacity(0);
            
            indexs = {up, down, up, down, center, up};
        }
        
        auto pos_original=pig->getPosition();
        //auto scale_original=pig->getScale();
        auto delay=DelayTime::create(1);
        auto duration=1.3;
        
        Vector<FiniteTimeAction*>actions;
        auto fadein_pig_pole=Spawn::create(
                                           createChangeAction(.5, .3, pig_stand, pig),
                                           CallFunc::create([backID]{
            if (backID!=3) {
                Common::playSE("pop.mp3");
            }}),
                                           
                                           CallFunc::create([pig,pos_original,indexs,distance](){
            pig->setPositionY(pos_original.y-distance*(indexs[0]/2));
        }), NULL);
        actions.pushBack(fadein_pig_pole);
        actions.pushBack(delay->clone());
        
        int i = 1;
        while (i<indexs.size()) {
            auto index = indexs[i];
            Vector<FiniteTimeAction*>actions_spawn;
            if (backID!=3) {
                actions.pushBack(createSoundAction("pop_3.mp3"));
            }
            auto move=EaseInOut::create(MoveTo::create(duration, Vec2(pos_original.x, pos_original.y-distance*(index/2))), 1.3);
            auto delay = DelayTime::create(.3);
            actions_spawn.pushBack(Sequence::create(move, delay, NULL));
            
            auto spawn=Spawn::create(actions_spawn);
            actions.pushBack(Spawn::create(TargetedAction::create(pig, spawn), NULL));
            i++;
        }
        
        actions.pushBack(delay->clone());
        auto fadeout_pig_pole=Spawn::create(createChangeAction(.5, .3, pig, pig_stand),
                                            CallFunc::create([backID]{
            if (backID!=3) {
                Common::playSE("landing.mp3");
            }
        }),
                                            NULL);
        actions.pushBack(fadeout_pig_pole);
        
        auto seq_pole=Sequence::create(actions);

        parent->runAction(Repeat::create(Sequence::create(delay,seq_pole,delay->clone(), NULL), -1));
    }
    else if(key=="singer"&&DataManager::sharedManager()->getEnableFlagWithFlagID(13))
    {//歌う
        singerAction(parent);
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            //伸びるスタンド
            auto mistake=parent->getChildByName("sup_36_mistake.png");
            
            auto delay=DelayTime::create(1);
            auto move=TargetedAction::create(mistake, EaseInOut::create(MoveBy::create(2, Vec2(-parent->getContentSize().width*.0235, parent->getContentSize().height*.242)), 1.3));
            auto spawn=Spawn::create(move,
                                     Sequence::create(DelayTime::create(1),
                                                      createSoundAction("emission.mp3"),
                                                      NULL),
                                     NULL);
            auto call=CallFunc::create([]{
                DataManager::sharedManager()->setTemporaryFlag("mistake_mic", 1);
            });
            parent->runAction(Sequence::create(delay,spawn,call, NULL));
        }
    }
    else if(key=="granpa")
    {//じじい
        auto body=parent->getChildByName("sup_47_pig.png");
        createSpriteToCenter("sup_47_pig_arm.png", false, body, true);
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(16)) {
            createSpriteToCenter("sup_47_cap.png", false, body, true);
        }
    }
    else if(key=="lip")
    {//くちびる
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(34)) {
            auto arm=parent->getChildByName("sup_77_arm.png");
            auto arm_anim=createAnchorSprite("sup_77_arm_anim.png", .585, .4, true, parent, true);
            arm_anim->setLocalZOrder(arm->getLocalZOrder());
            Vector<FiniteTimeAction*>actions;
            auto delay=DelayTime::create(.5);
            actions.pushBack(delay);
            auto change_0=createChangeAction(.7, .5, arm, arm_anim);
            actions.pushBack(change_0);
            
            std::vector<int>indexs={0,1,2,0,1,2};
            for (int i=0; i<6; i++) {
                auto swing=swingAction(20, .7, arm_anim, 1.5, false);
                auto lip=Sequence::create(DelayTime::create(.5),
                                          CallFunc::create([parent,indexs,i]{
                    Common::playSE("pop_4.mp3");
                    auto size=parent->getContentSize().width*.2;
                    auto index=indexs.at(i);
                    /*BlendFunc blend;
                     blend.src = GL_RGB;//透過部分以外?
                     blend.dst = GL_RGB;//透過部分が変わる？*/
                    
                    auto particle=ParticleSystemQuad::create("particle_music.plist");
                    particle->setAutoRemoveOnFinish(true);
                    particle->setCascadeOpacityEnabled(true);
                    particle->setTexture(Sprite::create(StringUtils::format("chu_%d.png",index))->getTexture());
                    particle->setPosition(parent->getContentSize().width*.45,parent->getContentSize().height*.525);
                    particle->setLife(2);
                    particle->resetSystem();
                    particle->setStartSize(size);
                    particle->setEndSize(size);
                    particle->setTotalParticles(5);
                    particle->setGravity(Vec2(size*.15, -size*1));
                    particle->setStartColor(Color4F::WHITE);
                    particle->setEndColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 0));
                    particle->setLocalZOrder(1);
                    particle->setAngle(120+45*index);
                    particle->setSpeed(size*2);
                    particle->setEndSpin(10);
                    particle->setEndSpinVar(20);
                    parent->addChild(particle);
                    
                }), NULL);
                actions.pushBack(Spawn::create(swing,lip, NULL));
            }
            
            auto change_1=createChangeAction(1, .7, arm_anim, arm);
            actions.pushBack(change_1);
            
            actions.pushBack(delay->clone());
            
            parent->runAction(Repeat::create(Sequence::create(actions), -1));
        }
        else{
            auto arm=parent->getChildByName("sup_77_arm.png");
            arm->retain();
            arm->removeFromParent();
            auto pig=parent->getChildByName("sup_77_pig.png");
            pig->addChild(arm);
        }
    }
    else if(key=="door")
    {//door前のぶた
        auto pig=parent->getChildByName("sup_51_pig_stage.png");
        if (pig) {
            auto seq_pig=skewAction(pig, 1, 3, -1);
            parent->runAction(seq_pig);
        }
    }
}

#pragma mark - Touch
void CabaretActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="pipe")
    {//パイプに火
        Common::playSE("p.mp3");
        auto match=createSpriteToCenter("sup_17_match.png", true, parent,true);

        auto seq_match=TargetedAction::create(match, Sequence::create(FadeIn::create(1),
                                                                      Repeat::create(swingAction(Vec2(0, parent->getContentSize().height*.02), .1, match, 1.51, false), 2),
                                                                      DelayTime::create(.3),
                                                                      CallFunc::create([this,parent]{
            Common::playSE("huo.mp3");
            auto particle=ParticleSystemQuad::create("fire.plist");
            particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width*.51, parent->getContentSize().height*.385);
            particle->setTotalParticles(40);
            particle->setDuration(1.5);
            particle->setStartColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 150));
            particle->setPosVar(Vec2(parent->getContentSize().width*.005, parent->getContentSize().height*.005));
            particle->setStartSize(parent->getContentSize().width*.03);
            particle->setSpeed(parent->getContentSize().height*.025);
            
            parent->addChild(particle);
        }),
                                                                      FadeOut::create(1),
                                                                      NULL));
        
        auto delay=DelayTime::create(2.5);
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(seq_match,delay,call,NULL));
    }
    else if(key=="cooler")
    {//ワインクーラーせいかいご
        auto addBack=putAction(parent, "back_7.png","", 1, nullptr);
        auto delay=DelayTime::create(1);
        auto open=createSpriteToCenter("sup_7_0_1.png", true, parent,true);
        createSpriteToCenter("sup_7_wine.png", false, open,true);
        createSpriteToCenter("sup_7_magnet.png", false, open,true);
        auto openAction=TargetedAction::create(open, Spawn::create(FadeIn::create(1),
                                                                   createSoundAction("gacha.mp3"), NULL));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(addBack,openAction,delay->clone(),call,NULL));
    }
    else if(key=="stage_switch")
    {//回転スイッチ正解後->ステージ点灯
        auto back=createSpriteToCenter("back_37.png", true, parent,true);
        auto back_1=createSpriteToCenter("back_39.png", true, back,true);
        createSpriteToCenter("sup_39_pig_stand.png", false, back_1,true);
        auto fadein=TargetedAction::create(back, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto fadein_1=TargetedAction::create(back_1, Spawn::create(FadeIn::create(.2),
                                                                   createSoundAction("spotlight_0.mp3"), NULL));
        auto fadeout=TargetedAction::create(back, FadeOut::create(1));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,fadein_1,delay->clone(),fadeout,delay->clone(),call,NULL));
    }
    else if(key=="cloud"){
        Vector<FiniteTimeAction*>actions_spawn;

        auto num=DataManager::sharedManager()->getNowBack();
        std::string soundName="pig_1.mp3";

        auto cloud_anchor=Vec2(.625,.68);
        if (num==47) {
            soundName="pig.mp3";
            cloud_anchor=Vec2(.6,.68);
            actions_spawn.pushBack(createBounceAction(parent->getChildByName("sup_47_pig.png"), .3, .05));
        }
        else if(num==39){
            cloud_anchor=Vec2(.6,.35);
            actions_spawn.pushBack(createBounceAction(parent->getChildByName("sup_39_pig_chip.png"), .3, .05));
        }
        else if(num==77){
            cloud_anchor=Vec2(.6,.628);
            actions_spawn.pushBack(createBounceAction(parent->getChildByName("sup_77_pig.png"), .3, .05));
        }
        else if(num==48){
            actions_spawn.pushBack(createBounceAction(parent->getChildByName("sup_48_pig.png"), .3, .05));
        }
        
        Common::playSE(soundName.c_str());

        if (num!=77) {
            auto cloud=AnchorSprite::create(cloud_anchor, parent->getContentSize(), StringUtils::format("sup_%d_cloud.png",num));
            cloud->setOpacity(0);
            parent->addChild(cloud);
            auto original_scale=cloud->getScale();
            cloud->setScale(.1);
            
            auto seq_cloud=TargetedAction::create(cloud, Sequence::create(Spawn::create(ScaleTo::create(.5, original_scale),
                                                                                        FadeIn::create(.5), NULL),
                                                                          createSoundAction("nyu.mp3"),
                                                                          DelayTime::create(.5),
                                                                          Spawn::create(ScaleTo::create(.5, .1),
                                                                                        FadeOut::create(.5), NULL), NULL));
            actions_spawn.pushBack(seq_cloud);
        }
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(Spawn::create(actions_spawn),call, NULL));
    }
    else if(key=="handbell")
    {//ベルアニメ
        Common::playSE("p.mp3");
        auto bell=createSpriteToCenter("sup_48_handbell.png", true, parent,true);
        auto fadein=TargetedAction::create(bell, FadeIn::create(1));
        auto scale=TargetedAction::create(bell, ScaleBy::create(1, .8));
        auto fadeout_bell=TargetedAction::create(bell, FadeOut::create(1));
        
        auto arm=createSpriteToCenter("sup_48_arm_1.png", true, parent, true);
        auto bell_arm=createAnchorSprite("sup_48_handbell_arm.png", .275, .2, false, arm, true);
        
        auto fadein_arm=TargetedAction::create(arm, FadeIn::create(1));
        auto shake=Spawn::create(createSoundAction("handbell.mp3"),
                                 swingAction(10, .3, bell_arm, 1.5,true), NULL);
        
        auto back_46=createSpriteToCenter("back_46.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back_46, 46, true);
        auto ice=createSpriteToCenter("sup_46_ice.png", true, back_46,true);
        
        auto boy=createSpriteToCenter("sup_46_boy.png", true, parent,true);
        
        auto seq_46=Sequence::create(TargetedAction::create(back_46, FadeIn::create(1)),
                                     TargetedAction::create(arm, RemoveSelf::create()),
                                     DelayTime::create(1),
                                     createSoundAction("pig.mp3"),
                                     TargetedAction::create(boy, FadeIn::create(1)),
                                     DelayTime::create(1),
                                     createChangeAction(1, .5, boy, ice,"koto.mp3"),
                                     DelayTime::create(1),
                                     TargetedAction::create(back_46, FadeOut::create(1)), NULL);
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,scale,Spawn::create(fadein_arm,fadeout_bell, NULL),DelayTime::create(1),shake,DelayTime::create(1),seq_46,call, NULL));
    }
    else if(key=="putmicro")
    {//マイクを置く
        auto put=putAction(parent, "sup_36_microphone.png", "koto.mp3", 1, nullptr);
        auto call_sing=CallFunc::create([this,parent]{
            this->singerAction(parent);
        });
        auto delay=DelayTime::create(3);
        
        auto back_51=createSpriteToCenter("back_51.png", true, parent, true);
        back_51->setLocalZOrder(1);
        SupplementsManager::getInstance()->addSupplementToMain(back_51, 51, true);
        
        auto anchorMap=DataManager::sharedManager()->getBackData(51)["anchorSprites"].asValueMap()["sprites"].asValueVector().at(0).asValueMap();
        
        auto pig=back_51->getChildByName("sup_51_pig_door.png");
        auto pig_1=createAnchorSprite(anchorMap["name"].asString(), anchorMap["x"].asFloat(), anchorMap["y"].asFloat(), true, back_51, true);
        auto fadein_back=TargetedAction::create(back_51, FadeIn::create(1));
        auto delay_1=DelayTime::create(1);
        auto change_pig=createChangeAction(1, .7, pig, pig_1);
        auto call_dance=CallFunc::create([this,back_51]{
            this->backAction("door", 51, back_51);
        });
        auto fadeout_door=TargetedAction::create(back_51, FadeOut::create(1));
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(put,call_sing,delay,fadein_back,delay_1,change_pig,delay_1->clone(),call_dance,delay->clone(),fadeout_door,call, NULL));
    }
    else if (key.compare(0,4,"neon")==0)
    {//ネオン点灯
        auto index=atoi(key.substr(5,1).c_str());
        auto back=createSpriteToCenter("back_38.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 38);
        auto neon=createSpriteToCenter(StringUtils::format("sup_38_neon_%d.png",index), true, back,true);
        
        auto delay=DelayTime::create(1);
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto fadein_neon=TargetedAction::create(neon, Spawn::create(FadeIn::create(1),
                                                                    createSoundAction("spotlight_1.mp3"), NULL));
        auto fadeout_back=TargetedAction::create(back, FadeOut::create(1));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_back,delay,fadein_neon,delay->clone(),fadeout_back,delay->clone(),call, NULL));
    }
    
    else if(key=="givecap"){
        Common::playSE("p.mp3");
        auto pig=parent->getChildByName("sup_47_pig.png");
        auto arm=pig->getChildByName("sup_47_pig_arm.png");
        auto arm_coin=createSpriteToCenter("sup_47_arm_coin.png", true, pig,true);

        auto cap=createSpriteToCenter("sup_47_cap_anim.png", true, parent,true);
        cap->setLocalZOrder(1);
        
        auto fadein_cap=TargetedAction::create(cap, FadeIn::create(1));
        auto scale_cap=TargetedAction::create(cap, ScaleBy::create(1, .8));
        
        auto cap_onpig=createSpriteToCenter("sup_47_cap.png", true, pig,true);

        auto change_cap=createChangeAction(1, .7, cap, cap_onpig,"pop_5.mp3");
        auto bounce=Spawn::create(createBounceAction(pig, .25, .1),
                                  createSoundAction("pig.mp3"), NULL);
        auto change_arm=createChangeAction(1, .7, arm, arm_coin,"pop_1.mp3");
        
        auto delay=DelayTime::create(1);
        
        auto call=CallFunc::create([callback](){
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_cap,scale_cap,change_cap,delay,bounce,delay->clone(),change_arm,delay->clone(),call, NULL));
    }
    else if(key=="coin"){
        Common::playSE("p.mp3");
        auto pig=parent->getChildByName("sup_39_pig_chip.png");
        auto coin=createSpriteToCenter("sup_39_coin.png", true, parent,true);

        auto fadein_0=TargetedAction::create(coin, FadeIn::create(1));
        auto scale_0=TargetedAction::create(coin, ScaleBy::create(1, .8));
        auto fadeout_0=TargetedAction::create(coin, FadeOut::create(1));
        
        auto pig_jump=Spawn::create(
                                    createSoundAction("pig_1.mp3"),
                                    createBounceAction(pig, .1),
                                    jumpAction(parent->getContentSize().height*.05, .2, pig, 1.5), NULL);
        auto delay=DelayTime::create(1);
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_0,scale_0,fadeout_0,pig_jump,delay,call, NULL));
    }
    else if(key=="moai")
    {//モアイ正解後
        auto back=createSpriteToCenter("sup_6_anim.png", true, parent,true);
        auto arm_0=createSpriteToCenter("sup_6_arm_0.png", false, back,true);
        auto arm_1=createSpriteToCenter("sup_6_arm_1.png", true, back,true);

        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto give=createChangeAction(1, .5, arm_0, arm_1,"pop_5.mp3");
        auto call=CallFunc::create([callback](){
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_back,delay,give,delay->clone(),call, NULL));
    }
    else if (key=="whiteboard")
    {//whiteBoard正解後
        Vector<FiniteTimeAction*>actions;
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        
        for (int i=0; i<3; i++) {
            std::string fileName;
            std::string soundName;
            if (i<2) {
                fileName=StringUtils::format("sup_72_anim_%d.png",i);
                if (i==0) {
                    soundName="pop.mp3";
                }
            }
            else{
                fileName="back_78.png";
                soundName="kacha.mp3";
            }
            auto spr=createSpriteToCenter(fileName, true, parent,true);
            
            actions.pushBack(TargetedAction::create(spr, Spawn::create(FadeIn::create(1),
                                                                       createSoundAction(soundName), NULL)));
            actions.pushBack(DelayTime::create(.7));
            
        }
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="wig"){
        Common::playSE("p.mp3");
        auto wig=createSpriteToCenter("sup_77_wig.png", true, parent, true);
        auto wig_after=createSpriteToCenter("sup_77_pig_wig.png", true, parent,true);
        auto give=giveAction(wig, 1, .8,false);
        auto delay=DelayTime::create(1);
        auto change=createChangeAction(.5, .5, wig, wig_after,"pop_5.mp3");
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(give,delay,change,delay->clone(),call, NULL));
    }
    else if(key=="pickel")
    {//ピッケルを使用
        Common::playSE("p.mp3");
        auto pickel=createAnchorSprite("sup_11_pickel.png", .77, .25, true, parent, true);
        auto bin=parent->getChildByName("sup_11_wine.png");
        auto key=createSpriteToCenter("sup_11_key.png", true, parent,true);
        auto fadein=TargetedAction::create(pickel, FadeIn::create(1));
        auto delay=DelayTime::create(1);
        auto attack=TargetedAction::create(pickel, Sequence::create(EaseInOut::create(RotateBy::create(1, 30), 1.5),
                                                                    delay->clone(),
                                                                    Spawn::create(EaseIn::create(RotateBy::create(1, -60), 1.5),
                                                                                  createSoundAction("swing_0.mp3"),
                                                                                  ScaleBy::create(1, .9),
                                                                                  TargetedAction::create(parent, Sequence::create(DelayTime::create(.5),
                                                                                                                                  FadeOut::create(.5), NULL)),
                                                                                  NULL),
                                                                    DelayTime::create(.2),
                                                                    createSoundAction("glass-break1.mp3"),
                                                                    TargetedAction::create(bin, RemoveSelf::create()),
                                                                    RemoveSelf::create(),
                                                                    NULL));
        
        auto fadein_key=TargetedAction::create(key, FadeIn::create(.1));
        auto fadein_back=FadeIn::create(1);
        auto fadeout_key=TargetedAction::create(key, FadeOut::create(1));
        auto call=CallFunc::create([callback](){
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,delay,attack,delay->clone(),fadein_key,fadein_back,delay->clone(),fadeout_key,call, NULL));
    }
    else if(key=="key"){
        Common::playSE("p.mp3");
        
        auto key_0=createSpriteToCenter("sup_64_key_0.png", true, parent, true);
        auto key_1=createSpriteToCenter("sup_64_key_1.png", true, parent, true);

        auto key_action_0=TargetedAction::create(key_0, Sequence::create(FadeIn::create(1),
                                                                         createSoundAction("kacha.mp3"), NULL));
        auto delay=DelayTime::create(1);
        auto change=createChangeAction(1, .5, key_0, key_1,"kacha.mp3");
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(key_action_0,delay->clone(),change,delay->clone(),call, NULL));
    }
    else if(key=="namecard")
    {//名刺箱正解
        auto back=createSpriteToCenter("back_65.png", true, parent, true);
        auto door=createSpriteToCenter("sup_65_0_1.png", true, parent, true);
        createSpriteToCenter("sup_65_card.png", false, door,true);
        
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        auto open=TargetedAction::create(door, Spawn::create(FadeIn::create(1),
                                                                         createSoundAction("kacha.mp3"), NULL));
        auto delay=DelayTime::create(1);
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_back,delay,open,delay->clone(),call, NULL));
    }
    else if(key=="bounce"){
        Common::stopAllSE();
        
        auto num=DataManager::sharedManager()->getNowBack();
        std::string soundName="pig.mp3";
        if (num==48) {
            soundName="pig_1.mp3";
        }
        Common::playSE(soundName.c_str());
        
        auto pig=parent->getChildByName(StringUtils::format("sup_%d_pig.png",num));
        pig->stopAllActions();
        pig->setScale(parent->getBoundingBox().size.width/parent->getScale()/pig->getContentSize().width);
        
        parent->runAction(createBounceAction(pig, .2,.025));
        
        callback(NULL);
    }
    else if(key=="cantopen"){

        auto addStory=CallFunc::create([parent,callback]{
            auto story=StoryLayer::create("cantopen", [callback]{
                callback(true);
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        parent->runAction(addStory);
    }
    else if(key=="getnamecard"){
        auto addStory=CallFunc::create([parent,callback]{
            auto story=StoryLayer::create("ending", [parent,callback]{
                auto card=SupplementsManager::getInstance()->switchSprites.at(0)->getChildByName("sup_65_card.png");
                
                auto fadeout=TargetedAction::create(card, FadeOut::create(1));
                auto delay=DelayTime::create(.3);
                auto call=CallFunc::create([callback](){
                    callback(true);
                });
                
                parent->runAction(Sequence::create(fadeout,delay,call, NULL));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        
        parent->runAction(addStory);
    }
    else if(key=="clear"){
        Common::playSE("p.mp3");
        
        auto card=createSpriteToCenter("sup_47_namecard.png", true, parent, true);
        auto give_card=giveAction(card, 1, .8, true);
        
        auto delay=DelayTime::create(1);
        auto addStory=CallFunc::create([parent]{
            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

            auto story=StoryLayer::create("ending1", [parent]{
                
                auto call=CallFunc::create([parent](){
                    Director::getInstance()->replaceScene(TransitionFade::create(8, ClearScene::createScene(), Color3B::WHITE));
                });
                
                parent->runAction(Sequence::create(call, NULL));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        parent->runAction(Sequence::create(give_card,delay,addStory, NULL));
    }
    else if(key=="dontopen"){
        auto addStory=CallFunc::create([parent]{
            auto story=StoryLayer::create("dontopen", []{});
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(addStory,call, NULL));
    }
    else{
        callback(true);
    }
}

#pragma mark - Item
void CabaretActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造
    if (key=="tiara") {//ティアラの組み合わせ
        Common::playSE("p.mp3");
        bool showingHair=(DataManager::sharedManager()->getShowingItemID()==10);
        auto item_0=createSpriteOnClip("hair.png", true, parent,true);
        //
        auto item_1=createSpriteOnClip("sup_tiara.png", true, parent, true);
        
        auto duration=1.0;
        if (showingHair) {
            duration=0;
        }
        auto change=createChangeAction(duration,duration*.7, parent->itemImage, item_0);
        auto delay=DelayTime::create(duration);
        auto get=TargetedAction::create(item_1, FadeIn::create(1));
        auto sound=createSoundAction("kacha.mp3");
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(change,delay,get,sound,delay->clone(),call, NULL));
    }
}

#pragma mark - Clear
void CabaretActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    if (key==CustomAction_ReviewLayer) {
        DataManager::sharedManager()->removeItemWithID(16);
    }
}

#pragma mark -
void CabaretActionManager::singerAction(cocos2d::Node *parent)
{
    auto singer=parent->getChildByName("sup_36_singer.png");
    
    auto particle=[parent](int index){
        auto call=CallFunc::create([parent,index]{
            Common::playSE("pop.mp3");
            
            auto pos=Vec2(parent->getContentSize().width*.3, parent->getContentSize().height*.75);
            auto size=parent->getContentSize().width*.15;
            std::vector<Color4F> startColors={Color4F::RED,Color4F::BLUE,Color4F::GREEN,Color4F::BLUE,Color4F::BLUE,Color4F::GREEN};
            
            auto particle=ParticleSystemQuad::create("particle_music.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(pos);
            particle->setLife(2.5);
            particle->resetSystem();
            particle->setStartSize(size);
            particle->setEndSize(size);
            particle->setTotalParticles(5);
            particle->setGravity(Vec2(size*.2, -size*2));
            particle->setStartColor(startColors.at(index));
            particle->setEndColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 0));
            
            particle->setAngle(110);
            particle->setSpeed(size*2);
            particle->setEndSpin(10);
            particle->setEndSpinVar(20);
            parent->addChild(particle);
        });
        
        return Sequence::create(call, NULL);
    };
    
    Vector<FiniteTimeAction*>actions;
    actions.pushBack(DelayTime::create(.5));
    auto angle=2.5;
    auto duration=1.5;
    auto duration_delay=.3;
    for (int i=0; i<3; i++) {
        auto skew=TargetedAction::create(singer,Sequence::create(particle(2*i),
                                                                 EaseOut::create(SkewTo::create(duration, -angle, 0), 1.5),
                                                                 DelayTime::create(duration_delay),
                                                                 particle(2*i+1),
                                                                 EaseIn::create(SkewTo::create(duration, angle, 0),1.5),
                                                                 DelayTime::create(duration_delay),
                                                                 NULL));
        actions.pushBack(skew);
    }
    
    actions.pushBack(DelayTime::create(1.5));
    
    auto rep=Repeat::create(Sequence::create(actions), -1);
    parent->runAction(rep);
}
