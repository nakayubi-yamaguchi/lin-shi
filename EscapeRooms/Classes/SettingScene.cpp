//
//  SettingScene.cpp
//  Princess
//
//  Created by ナカユビ on 2017/09/09.
//
//
#include "SettingScene.h"

#include <string.h>
#include "UIParts/MenuItemScale.h"
#include "UIParts/TextMenuItem.h"
#include "ConfirmAlertLayer.h"
#include "DescriptionLayer.h"
#include "DataManager.h"
#include "PromoteHintLayer.h"

#include "Utils/Common.h"
#include "Utils/CustomDirector.h"
#include "Utils/CustomAlert.h"
#include "Utils/NativeAdManager.h"
#include "Utils/BannerBridge.h"

#include "EscapeLoadingScene.h"
#include "EscapeDataManager.h"

#include "AnalyticsManager.h"
#include "EscapeStageSprite.h"

using namespace cocos2d;
Scene* SettingScene::createScene() {
    auto scene = Scene::create();
    auto layer = SettingScene::create();
    scene->addChild(layer);
    
    return scene;
}

bool SettingScene::init() {
    if (!LayerColor::initWithColor(Color4B::WHITE)) {
        return false;
    }
    
    createMenues();
    
    
    return true;
}

// トランジション終了時に呼ばれる
void SettingScene::onEnterTransitionDidFinish()
{
    showPanelAd();
}

#pragma mark- プライベート
void SettingScene::createMenues()
{
    //背景画像
    auto back = EscapeStageSprite::createWithSpriteFileName("room_image.png");
    back->setScale(getContentSize().width/back->getContentSize().width);//幅基準でめいっぱい表示
    back->setPosition(getContentSize()/2);
    back->setOpacity(200);
    addChild(back);
    
    auto title_height=getContentSize().height*.15;
    
    title_back=Sprite::create();
    title_back->setTextureRect(Rect(0, 0, getContentSize().width, title_height));
    title_back->setPosition(getContentSize().width/2,(getContentSize().height<1200)? getContentSize().height-BannerBridge::getBannerHeight()-title_height/2 : getContentSize().height*.8);
    title_back->setColor(Color3B::BLACK);
    title_back->setOpacity(150);
    addChild(title_back);
    
    auto title = EscapeStageSprite::createWithSpriteFileName("title.png");
    title->setScale(title_height/title->getContentSize().height);//title_backの高さ基準
    title->setPosition(title_back->getPosition());
    addChild(title);
    
    //メニュー背景の黒いバー
    auto menues_height=getContentSize().height*.2;
    menues_back=Sprite::create();
    menues_back->setTextureRect(Rect(0, 0, getContentSize().width, menues_height));
    menues_back->setPosition(getContentSize().width/2,getContentSize().height*.2);
    menues_back->setColor(Color3B::BLACK);
    menues_back->setOpacity(100);
    addChild(menues_back);
    
    
    //メニューボタンたち
    std::vector<std::string> titleMenuFileNames = {"home.png","hint_icon.png","help.png"};
    Vector<MenuItem*> menuItems;
    
    //メニューの羅列
    auto width = menues_back->getContentSize().height*.55;
    auto menuPisitionY = menues_height*.7;
    auto space = (getContentSize().width-width*titleMenuFileNames.size())/(titleMenuFileNames.size()+1);
    
    for (int i = 0; i < titleMenuFileNames.size(); i++) {
        auto sprite=Sprite::createWithSpriteFrameName(titleMenuFileNames.at(i).c_str());
        
        auto menuItem = MenuItemScale::create(sprite,sprite, [this](Ref *sender){
            //アラートを表示する際はパネルを隠す
            //BannerBridge::removeBanner(BANNER_APPID_RECTANGLE);
            BannerBridge::hideBanner(BANNER_APPID_RECTANGLE);
            
            Common::playClick();
            auto senderItem = (MenuItemLabel*)sender;
            switch (senderItem->getTag()) {
                case 0://戻るか確認
                {
                    auto message = DataManager::sharedManager()->getSystemMessage("interrupt_game");
                    auto alert = ConfirmAlertLayer::create(ConfirmAlertType_Twice, message, [this](Ref* sender){
                        auto alert = (ConfirmAlertLayer*)sender;
                        int selectedButton = alert->getSelectedButtonNum();
                        
                        if (selectedButton == 0) {//キャンセル
                            this->showPanelAd();
                        }
                        else {//遷移する
                            log("前の画面に戻る操作");
                            Common::stopBgm();
                            //popシーンしてからじゃないとゲームシーンが解放されない。
                            Director::getInstance()->popScene();
                            Director::getInstance()->replaceScene(TransitionFade::create(.5, EscapeLoadingScene::createScene(LoadingType_RemoveCathe),Color3B::WHITE));
                        }
                    });
                    alert->showAlert(this);
                }
                    break;
                case 1://ヒント見るか？
                {
                    this->showHintLayer();
                }
                    break;
                case 2://やり方
                {
                    this->showDescriptionLayer();
                }
                    break;
                    
                default:
                    break;
            }
        });
        
        menuItem->setTag(i);
        menuItem->setScale(width/sprite->getContentSize().height);
        menuItem->setTappedScale(.95);
        menuItem->setPosition(space + width/2 + (space + width)*i,
                              menuPisitionY);
        
        menuItems.pushBack(menuItem);
    }
    
    auto titleMenu = Menu::createWithArray(menuItems);
    titleMenu->setPosition(0 ,0);
    menues_back->addChild(titleMenu);
    
    auto item=TextMenuItem::create("CLOSE", Size(getContentSize().width*.4, menues_back->getContentSize().height*.3), Common::getColorFromHex(MyColor), [this](Ref*sender){
        //連打無効処理
        auto menuItem = (MenuItemScale*)sender;
        menuItem->setEnabled(false);
        
        Common::playClick();
        
        auto director = (CustomDirector *)Director::getInstance();
        director->popSceneWithTransition<TransitionFade>(0.5);
        
        //パネル広告を閉じる
        BannerBridge::hideBanner(BANNER_APPID_RECTANGLE);
    });
    item->setPosition(menues_back->getContentSize().width/2, (menuPisitionY-width/2)/2);
    
    auto closeMenu=Menu::create(item, NULL);
    closeMenu->setPosition(0, 0);
    menues_back->addChild(closeMenu);
}

void SettingScene::showPanelAd()
{
    auto titleUnder =  getContentSize().height-(title_back->getPositionY()-title_back->getContentSize().height/2);
    auto menuUpper = getContentSize().height-(menues_back->getPositionY()+menues_back->getContentSize().height/2);
    auto position = Vec2(getContentSize().width/2, (titleUnder+menuUpper)/2);
    BannerBridge::createRectangle(BANNER_APPID_RECTANGLE, position);
}

#pragma mark- ヒント関連
void SettingScene::showHintLayer()
{
    auto type = EscapeDataManager::getInstance()->getTypePlayTitleCondition();

    if (type == TitleCondition_TUTORIAL) {
        auto message = DataManager::sharedManager()->getSystemMessage("tutorial_hint");
        auto confirmAlert = ConfirmAlertLayer::create(ConfirmAlertType_Single, message.c_str(), [this](Ref* sender){
            this->showPanelAd();
        });
        confirmAlert->showAlert();
    }
    else {//チュートリアル以外
        
        auto promoteHintAlert = PromoteHintLayer::create([this](Ref* sender){
            auto promoteLayer = (PromoteHintLayer*)sender;
            if(promoteLayer->getSelectedButtonNum() == 0) {//キャンセルが押されたら、パネルを表示します
                this->showPanelAd();
            }
        });
        promoteHintAlert->showAlert(this);
    }
}

#pragma mark - Description
void SettingScene::showDescriptionLayer()
{
    auto description = DescriptionLayer::create([this](Ref*sender){
        log("やり方説明を閉じました");
        //やり方を閲覧後、広告を表示します。
        this->showPanelAd();
    },240,true);
    this->addChild(description);
}

#pragma mark- 解放
SettingScene::~SettingScene()
{
    log("セッティングシーンを解放します。");
}
