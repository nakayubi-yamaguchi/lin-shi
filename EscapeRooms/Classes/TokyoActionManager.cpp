//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "TokyoActionManager.h"
#include "Utils/Common.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"

using namespace cocos2d;

TokyoActionManager* TokyoActionManager::manager =NULL;

TokyoActionManager* TokyoActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new TokyoActionManager();
    }
    return manager;
}

#pragma mark - Back
void TokyoActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="long_light")
    {
        if(!DataManager::sharedManager()->getEnableFlagWithFlagID(18)){
            return;
        }
        auto light=Sprite::createWithSpriteFrameName(StringUtils::format("anim_%d_light.png",backID));
        light->setOpacity(0);
        light->setPosition(parent->getContentSize()/2);
        parent->addChild(light);
        
        auto repeat=[](int count){
            auto duration=.3;
            auto flash=Sequence::create(EaseOut::create(FadeIn::create(duration), 1.5) ,EaseIn::create(FadeOut::create(duration), 1.5), NULL);
            auto repeat_action=Repeat::create(flash, count);
            
            return Sequence::create(repeat_action,DelayTime::create(.3), NULL);
        };
        
        light->runAction(Repeat::create(Sequence::create(DelayTime::create(1),repeat(3),repeat(5),repeat(2),repeat(1), NULL),UINT_MAX));
    }
    else if(key=="hole"){
        auto distance=parent->getContentSize().width*.05;

        if (DataManager::sharedManager()->isPlayingMinigame()) {
            auto mistake=parent->getChildByName<Sprite*>("sup_35_mistake.png");
            auto mistake_1=parent->getChildByName<Sprite*>("sup_35_mistake_1.png");
            
            auto move_0=TargetedAction::create(mistake, Sequence::create(MoveBy::create(.5, Vec2(-distance, 0)),
                                                                         DelayTime::create(1),
                                                                                        MoveBy::create(1,Vec2(distance,0)), NULL));
            auto move_1=TargetedAction::create(mistake_1, Sequence::create(MoveBy::create(.5, Vec2(distance, 0)),
                                                                           DelayTime::create(1),
                                                                         MoveBy::create(1,Vec2(-distance,0)),
                                                                           CallFunc::create([]{
                DataManager::sharedManager()->setTemporaryFlag("mistake_mouse", 1);
            }), NULL));
            
            parent->runAction(Spawn::create(move_0,move_1, NULL));
        }
        else{
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(24)) {
                return;
            }
            auto mouse=AnchorSprite::create(Vec2(.592, .379), parent->getContentSize(), "anim_35_mouse.png");
            parent->addChild(mouse);
            
            auto move=TargetedAction::create(mouse, MoveBy::create(.5, Vec2(distance, 0)));
            auto move_1=TargetedAction::create(mouse, MoveBy::create(1, Vec2(-distance, 0)));
            
            mouse->runAction(Sequence::create(move,DelayTime::create(1),move_1, NULL));
        }
    }
    else if (key=="wheel"){
        auto rotate=AnchorSprite::create(Vec2(.5, .547), parent->getContentSize(), "sup_6_rotate.png");
        parent->addChild(rotate);
        
        auto cover=Sprite::createWithSpriteFrameName("sup_6_cover.png");
        cover->setPosition(parent->getContentSize()/2);
        cover->setName("sup_6_cover.png");
        parent->addChild(cover);
        
        if(DataManager::sharedManager()->getEnableFlagWithFlagID(26)){
            startWheel(false,parent,NULL);
        }
    }
    else if(key=="wheel_out")
    {
        if(!DataManager::sharedManager()->getEnableFlagWithFlagID(26)){
            return;
        }
        auto mouse=parent->getChildByName(StringUtils::format("sup_%d_mouse.png",backID));
        auto height=parent->getContentSize().height*.005;
        auto jump=TargetedAction::create(mouse, Sequence::create(EaseOut::create(MoveBy::create(.1, Vec2(0, height)),1.5),
                                                                   EaseIn::create(MoveBy::create(.1, Vec2(0,-height)), 1.5), NULL));
        parent->runAction(Repeat::create(jump, UINT_MAX));
    }
    else if(key=="ufo")
    {
        Vector<FiniteTimeAction*>actions;
        for (int i=0; i<4; i++) {
            auto spr=Sprite::createWithSpriteFrameName(StringUtils::format("ufo_%d.png",i));
            spr->setOpacity(0);
            spr->setPosition(parent->getContentSize()/2);
            parent->addChild(spr);

            Sequence*fades;
            Repeat*moves;
            auto distance=parent->getContentSize().height*.01;
            auto move_seq=Sequence::create(EaseInOut::create(MoveBy::create(2.5/4, Vec2(0, distance)), 1.5),
                                           EaseInOut::create(MoveBy::create(2.5/4, Vec2(0, -distance)), 1.5), NULL);

            if (i==0&&DataManager::sharedManager()->isPlayingMinigame()) {
                auto mistake=parent->getChildByName("sup_45_mistake.png");
                mistake->setOpacity(0);
                fades=Sequence::create(Spawn::create(FadeIn::create(1),TargetedAction::create(mistake, FadeIn::create(1)),CallFunc::create([i]{
                    if (i==0) {
                        DataManager::sharedManager()->setTemporaryFlag("mistake_ufo", 1);
                    }}), NULL) , NULL);
                moves=Repeat::create(Spawn::create(move_seq,TargetedAction::create(mistake,EaseInOut::create(move_seq->clone(),1.5)), NULL), UINT_MAX);
                
            }
            else{
                fades=Sequence::create(FadeIn::create(1),DelayTime::create(.5),FadeOut::create(1), NULL);
                moves=Repeat::create(move_seq,2);
            }
            
            
            actions.pushBack(TargetedAction::create(spr, Sequence::create(CallFunc::create([]{Common::playSE("ufo.mp3");}),Spawn::create(fades,moves, NULL)
            ,DelayTime::create(.5),NULL)));
        }
        
        
        if(DataManager::sharedManager()->isPlayingMinigame()){
            auto seq=Sequence::create(DelayTime::create(1),actions.at(2),actions.at(3),Spawn::create(actions.at(0),
                                                                                                                   Sequence::create(DelayTime::create(2.5),
                                                                                                                                    Repeat::create(Sequence::create(actions.at(1)->clone(),actions.at(2)->clone(),actions.at(3)->clone(), NULL), UINT_MAX), NULL), NULL), NULL);
            parent->runAction(Sequence::create(seq, NULL));
        }
        else{
            auto seq=Sequence::create(DelayTime::create(1),actions.at(0),actions.at(2),actions.at(3),actions.at(1)->clone(),actions.at(3)->clone(),actions.at(2)->clone(), NULL);
            auto repeat=Repeat::create(seq, UINT_MAX);
            
            parent->runAction(repeat);
        }
    }
}

void TokyoActionManager::startWheel(bool willStart, Node *parent, const onFinished &callback)
{
    Vector<FiniteTimeAction*>actions;
    
    auto wheel=parent->getChildByName("sup_6_rotate.png");
    auto rotate=TargetedAction::create(wheel,RotateBy::create(1, -360));
    
    auto mouse_1=Sprite::createWithSpriteFrameName("anim_6_mouse_1.png");
    mouse_1->setPosition(parent->getContentSize()/2);
    parent->addChild(mouse_1);
    
    auto height=parent->getContentSize().height*.02;
    
    auto jump=TargetedAction::create(mouse_1, Sequence::create(EaseOut::create(MoveBy::create(.1, Vec2(0, height)),1.5),
                                                               EaseIn::create(MoveBy::create(.1, Vec2(0,-height)), 1.5), NULL));
    auto change_0=CallFunc::create([mouse_1]{mouse_1->setSpriteFrame("anim_6_mouse_2.png");});
    auto change_1=CallFunc::create([mouse_1]{mouse_1->setSpriteFrame(Sprite::createWithSpriteFrameName("anim_6_mouse_1.png")->getSpriteFrame());});
    auto run=Sequence::create(jump,change_0,jump->clone(),change_1, NULL);
    if (willStart) {
        Common::playSE("p.mp3");
        
        auto screen=Sprite::createWithSpriteFrameName("sup_6_screen.png");
        screen->setPosition(parent->getContentSize()/2);
        screen->setOpacity(0);
        parent->addChild(screen);
        
        auto spawn=Spawn::create(
                                 TargetedAction::create(mouse_1, Spawn::create(FadeIn::create(1),
                                                                               Repeat::create(run,20), NULL)),
                                 TargetedAction::create(wheel, RotateBy::create(100, -360*100)),
                                 TargetedAction::create(screen,Sequence::create(FadeIn::create(3),
                                                                                CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        }), NULL)), NULL);
        actions.pushBack(spawn);
        
        parent->runAction(Sequence::create(actions));
    }
    else{
        auto jumps=TargetedAction::create(mouse_1,Repeat::create(run, UINT_MAX));
        auto rotates=Repeat::create(rotate, UINT_MAX);
        actions.pushBack(Spawn::create(jumps,rotates, NULL));
    }
    
    parent->runAction(Sequence::create(actions));
}

#pragma mark - Touch
void TokyoActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="printer")
    {
        Common::playSE("p.mp3");
        auto paper_0=Sprite::createWithSpriteFrameName("anim_24_paper_0.png");
        paper_0->setPosition(parent->getContentSize()/2);
        paper_0->setOpacity(0);
        parent->addChild(paper_0);
        
        auto paper_1=Sprite::createWithSpriteFrameName("anim_24_paper_1.png");
        paper_1->setPosition(parent->getContentSize()/2);
        paper_1->setOpacity(0);
        parent->addChild(paper_1);
        
        auto distance_0=parent->getContentSize().height*.15;
        auto repeat_count=5;
        
        auto fadein_0=TargetedAction::create(paper_0, FadeIn::create(1));
        auto move_0=TargetedAction::create(paper_0, Repeat::create(Sequence::create(DelayTime::create(.5),CallFunc::create([]{Common::playSE("printer_0.mp3");}),Spawn::create(MoveBy::create(.2, Vec2(0, -distance_0/repeat_count)),ScaleBy::create(.2, 1,.8), NULL), NULL), repeat_count));
        
        auto distance_1=-parent->getContentSize().height*.2;
        auto fadein_1=TargetedAction::create(paper_1, FadeIn::create(2));//delay混み
        auto sound=CallFunc::create([]{Common::playSE("printer_1.mp3");});
        auto move_1=TargetedAction::create(paper_1,EaseOut::create(MoveBy::create(1.5, Vec2(0, distance_1)),1.5));
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        parent->runAction(Sequence::create(fadein_0,move_0,fadein_1,sound,move_1,DelayTime::create(1),call, NULL));
    }
    else if(key.compare(0,5,"light")==0)
    {
        Common::playSE("kacha.mp3");
        
        auto index=atoi(key.substr(6,1).c_str());

        auto light=Sprite::createWithSpriteFrameName(StringUtils::format("light_%d.png",index));
        light->setPosition(parent->getContentSize()/2);
        light->setOpacity(0);
        parent->addChild(light);
        
        auto seq=TargetedAction::create(light, Sequence::create(FadeIn::create(.15),DelayTime::create(.5),FadeOut::create(.15), NULL));
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq,call,NULL));
    }
    else if(key=="catfood")
    {
        Common::playSE("p.mp3");
        
        std::vector<std::string>fileNames={"sup_11_food.png","sup_11_cat.png","anim_11_catfood.png"};
        for (auto name: fileNames) {
            auto spr=Sprite::createWithSpriteFrameName(name);
            spr->setPosition(parent->getContentSize()/2);
            spr->setOpacity(0);
            spr->setName(name);
            parent->addChild(spr);
        }
        
        auto catfood=parent->getChildByName(fileNames.at(2));
        auto food=parent->getChildByName(fileNames.at(0));
        auto cat=parent->getChildByName(fileNames.at(1));
        auto distance=parent->getContentSize().height*.05;
        
        auto fadein_0=TargetedAction::create(catfood, FadeIn::create(1));
        
        auto shake=TargetedAction::create(catfood, Repeat::create(Sequence::create(EaseIn::create(MoveBy::create(.3, Vec2(0, -distance)),2),CallFunc::create([]{Common::playSE("sa.mp3");}),DelayTime::create(.3),MoveBy::create(.3,Vec2(0,distance)), NULL), 2));
        
        auto fade_spawn=Spawn::create(TargetedAction::create(catfood, FadeOut::create(.5)),
                                                             TargetedAction::create(food, FadeIn::create(.5)), NULL);
        
        auto sound=CallFunc::create([]{Common::playSE("cat_0.mp3");});
        auto fadein_1=TargetedAction::create(cat, FadeIn::create(1));
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_0,shake,fade_spawn,DelayTime::create(.7),sound,fadein_1,DelayTime::create(.5),call,NULL));
    }
#pragma mark driver
    else if(key=="driver")
    {
        Common::playSE("p.mp3");
        auto driver=Sprite::createWithSpriteFrameName("anim_29_driver.png");
        driver->setPosition(parent->getContentSize()/2);
        driver->setOpacity(0);
        parent->addChild(driver);
        
        auto screw_0=parent->getChildByName("sup_29_screw_0.png");
        auto screw_1=parent->getChildByName("sup_29_screw_1.png");
        auto cover=parent->getChildByName("sup_29_cover.png");
        auto plug=Sprite::createWithSpriteFrameName("sup_29_plug.png");
        plug->setPosition(parent->getContentSize()/2);
        plug->setOpacity(0);
        parent->addChild(plug);

        auto distance=parent->getContentSize().width*.01;
        auto fadein= FadeIn::create(.7);
        auto shake=Repeat::create(Sequence::create(MoveBy::create(.1,Vec2(distance, distance)),
                                                   CallFunc::create([]{Common::playSE("kacha_1.mp3");}),
                                                   MoveBy::create(.1,Vec2(-distance, -distance)), NULL),3);
        
        auto seq_0=TargetedAction::create(driver, Sequence::create(fadein,shake, NULL));
        auto remove_screw_0=TargetedAction::create(screw_1, FadeOut::create(0));
        
        auto move=MoveBy::create(1, Vec2(0, parent->getContentSize().height*.4175));
        auto seq_1=TargetedAction::create(driver, Sequence::create(move,shake->clone(), NULL));
        auto remove_screw_1=TargetedAction::create(screw_0, FadeOut::create(0));

        auto spawn=Spawn::create(TargetedAction::create(plug, FadeIn::create(.5)),
                                 TargetedAction::create(cover, FadeOut::create(.5)),
                                 TargetedAction::create(driver, FadeOut::create(.5)), NULL);
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_0,remove_screw_0,seq_1,remove_screw_1,DelayTime::create(.5),spawn,DelayTime::create(.3),call, NULL));
    }
    else if (key=="mouse"){
        startWheel(true, parent,[callback](bool finished){
            callback(true);
        });
    }
    else if(key.compare(0,5,"piano")==0){
        auto index=atoi(key.substr(6,1).c_str());
        
        AnswerManager::getInstance()->appendPasscode(StringUtils::format("%d",index));
        
        auto spr=parent->getChildByName(StringUtils::format("sup_36_%d.png",index));
        spr->setOpacity(0);
        
        std::vector<std::string>sounds={"fa.mp3","si.mp3","mi_1.mp3","ra_1.mp3"};
        
        Common::playSE(sounds.at(index).c_str());
        auto delay=DelayTime::create(.1);
        auto call=CallFunc::create([this,spr,callback,parent]{
            spr->setOpacity(255);
            auto pass=AnswerManager::getInstance()->getPasscode();
            auto correct=false;
            if (pass.size()==6) {
                log("pass %s",pass.c_str());
                if (pass=="213210"&&!DataManager::sharedManager()->getEnableFlagWithFlagID(27)) {
                    Common::playSE("pinpon.mp3");
                    correct=true;
                    AnswerManager::getInstance()->enFlagIDs.push_back(27);
                }
                else{
                    AnswerManager::getInstance()->resetPasscode();
                }
            }
            
            if (correct) {
                this->blindAction(parent,callback);
            }
            else{
                callback(true);
            }
        });
        
        parent->runAction(Sequence::create(delay,call, NULL));
    }
    else if(key=="apple"){
        auto story=StoryLayer::create("apple", [callback](Ref*ref){
            callback(true);
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
    else if(key=="dontopen"){
        auto story=StoryLayer::create("dontopen", [callback](Ref*ref){
            callback(true);
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
    else if(key=="clear"){
        auto story=StoryLayer::create("ending", [callback,parent](Ref*ref){
            Common::stopBgm();
            //alarm
            auto light=Sprite::createWithSpriteFrameName("anim_47_light.png");
            light->setPosition(parent->getContentSize()/2);
            light->setOpacity(0);
            parent->addChild(light);
            
            auto delay=DelayTime::create(.5);
            auto alarm=CallFunc::create([]{Common::playSE("alarm.mp3");});
            auto fades=TargetedAction::create(light, Repeat::create(Sequence::create(EaseOut::create(FadeIn::create(1), 1.5),
                                                                                     EaseIn::create(FadeOut::create(1), 1.5), NULL), UINT_MAX));
            auto call=Sequence::create(DelayTime::create(2),
                                       CallFunc::create([callback,parent,light]{
                auto story_1=StoryLayer::create("ending1", [callback,parent,light](Ref*ref){
                    auto back=Sprite::createWithSpriteFrameName("anim_5_back.png");
                    back->setOpacity(0);
                    back->setPosition(parent->getContentSize()/2);
                    back->setCascadeOpacityEnabled(false);
                    parent->addChild(back);
                    
                    auto heri=Sprite::createWithSpriteFrameName("heri_0.png");
                    heri->setPosition(parent->getContentSize().width/2,parent->getContentSize().height);
                    back->addChild(heri);
                    
                    Vector<SpriteFrame*>frames;
                    for(int i=1;i<3;i++){
                        auto s=Sprite::createWithSpriteFrameName(StringUtils::format("heri_%d.png",i));
                        frames.pushBack(s->getSpriteFrame());
                    }
                    
                    auto animation=Animation::createWithSpriteFrames(frames);
                    animation->setDelayPerUnit(2.0f/60.0f);//??フレームぶん表示
                    animation->setLoops(UINT_MAX);
                    
                    auto animate=TargetedAction::create(heri, Animate::create(animation));
                    parent->runAction(animate);

                    auto cover=Sprite::createWithSpriteFrameName("sup_5_cover.png");
                    cover->setPosition(parent->getContentSize()/2);
                    cover->setOpacity(0);
                    back->addChild(cover);
                    
                    auto fadein=Spawn::create(TargetedAction::create(back, FadeIn::create(2)),
                                              TargetedAction::create(cover, FadeIn::create(1)), NULL);
                    auto remove_light=TargetedAction::create(light, RemoveSelf::create());
                    auto delay=DelayTime::create(1);
                    
                    auto move=TargetedAction::create(heri, Sequence::create(CallFunc::create([]{Common::playSE("heri.mp3");}),EaseOut::create(MoveTo::create(2, cover->getPosition()), 2), NULL));
                    auto call=Sequence::create(DelayTime::create(1),
                                               CallFunc::create([parent,heri,cover]{
                        EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

                        auto story=StoryLayer::create("ending2", [heri,parent,cover](Ref*ref){
                            auto pig=Sprite::createWithSpriteFrameName("sup_5_pig.png");
                            pig->setPosition(heri->getContentSize()/2);
                            pig->setOpacity(0);
                            heri->addChild(pig);
                            
                            auto move_0=TargetedAction::create(heri,EaseIn::create(MoveTo::create(.5, Vec2(parent->getContentSize().width/2, parent->getContentSize().height*.35)), 2));
                            auto fadein=TargetedAction::create(pig, FadeIn::create(1));
                            auto sound=CallFunc::create([]{Common::playSE("heri.mp3");});
                            auto move_1=TargetedAction::create(heri, EaseOut::create(JumpTo::create(2, Vec2(parent->getContentSize().width/2,parent->getContentSize().height*.6), parent->getContentSize().height*.1, 1), 2));
                            auto delay=DelayTime::create(1);
                            auto move_2=TargetedAction::create(heri, Spawn::create(RotateBy::create(.3, -5),
                                                                                   EaseIn::create(MoveBy::create(3, Vec2(-parent->getContentSize().width,0)), 2), NULL));
                            auto remove_heri=TargetedAction::create(heri, RemoveSelf::create());
                            
                            auto scale_cover=TargetedAction::create(cover,Spawn::create(ScaleBy::create(4, 2.5),MoveBy::create(4, Vec2(0, -parent->getContentSize().height*.75)),EaseIn::create(FadeOut::create(4), 2), NULL) );
                            
                            auto remove_cover=TargetedAction::create(cover, RemoveSelf::create());
                            
                            //飛び去るへり
                            auto heri_1=Sprite::createWithSpriteFrameName("heri_3.png");
                            heri_1->setPosition(parent->getContentSize()/2);
                            heri_1->setOpacity(0);
                            parent->addChild(heri_1);
                            Vector<SpriteFrame*>frames;
                            for(int i=3;i<5;i++){
                                auto s=Sprite::createWithSpriteFrameName(StringUtils::format("heri_%d.png",i));
                                frames.pushBack(s->getSpriteFrame());
                            }
                            
                            auto animation=Animation::createWithSpriteFrames(frames);
                            animation->setDelayPerUnit(2.0f/60.0f);//??フレームぶん表示
                            animation->setLoops(UINT_MAX);
                            
                            heri_1->runAction(Animate::create(animation));
                            
                            auto move=TargetedAction::create(heri_1, Spawn::create(ScaleTo::create(5, .01),
                                                                                   FadeIn::create(.1),
                                                                                   Sequence::create(RotateBy::create(.5, 5),
                                                                                                    RotateBy::create(1, -10),
                                                                                                    RotateBy::create(.5, 5), NULL),
                                                                                   Sequence::create(EaseOut::create(MoveTo::create(2, Vec2(parent->getContentSize().width/2, parent->getContentSize().height*.2)),2),
                                                                                                    MoveTo::create(3, Vec2(parent->getContentSize().width/2, parent->getContentSize().height*.7)), NULL), NULL));
                            auto call=CallFunc::create([]{
                                auto end=StoryLayer::create("ending3", [](Ref*ref){
                                    Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
                                });
                                Director::getInstance()->getRunningScene()->addChild(end);
                            });
                            
                            parent->runAction(Sequence::create(sound,move_0,fadein,move_1,delay,sound->clone(),move_2,remove_heri,scale_cover,remove_cover,DelayTime::create(1),sound->clone(),move,DelayTime::create(1),call, NULL));
                        });
                        Director::getInstance()->getRunningScene()->addChild(story);
                    }), NULL);
                    auto heri_spawn=Spawn::create(move,animate,call, NULL);
                    
                    parent->runAction(Sequence::create(fadein,remove_light,delay,heri_spawn, NULL));
                });
                Director::getInstance()->getRunningScene()->addChild(story_1);

            }), NULL);
            
            auto alarm_spawn=Spawn::create(fades,call, NULL);
            
            parent->runAction(Sequence::create(delay,alarm,alarm_spawn, NULL));
        });
        Director::getInstance()->getRunningScene()->addChild(story);
    }
    else{
        callback(true);
    }
}

void TokyoActionManager::blindAction(Node *parent, const onFinished &callback)
{
    auto back=Sprite::createWithSpriteFrameName("back_43.png");
    back->setPosition(parent->getContentSize()/2);
    back->setOpacity(0);
    back->setCascadeOpacityEnabled(true);
    parent->addChild(back);
    
    auto close=Sprite::createWithSpriteFrameName("sup_43_close.png");
    close->setPosition(parent->getContentSize()/2);
    back->addChild(close);
    
    auto open=Sprite::createWithSpriteFrameName("sup_43_open.png");
    open->setOpacity(0);
    open->setPosition(parent->getContentSize()/2);
    parent->addChild(open);
    
    auto delay=DelayTime::create(1);
    auto fadein=TargetedAction::create(back, FadeIn::create(1));
    auto delay_0=DelayTime::create(1);
    auto sound_0=CallFunc::create([]{Common::playSE("sa.mp3");});
    auto spawn=Spawn::create(TargetedAction::create(close, FadeOut::create(1)),
                             TargetedAction::create(open, FadeIn::create(1)), NULL);
    auto sound=CallFunc::create([]{Common::playSE("pinpon.mp3");});
    auto delay_1=DelayTime::create(1);
    auto fadeout=Spawn::create(TargetedAction::create(back, FadeOut::create(1)),
                               TargetedAction::create(open, FadeOut::create(1)), NULL);
    auto call=CallFunc::create([callback]{
        callback(true);
    });
    
    parent->runAction(Sequence::create(delay,fadein,delay_0,sound_0,spawn,sound,delay_1,fadeout,call, NULL));
}

#pragma mark - Clear Scene
void TokyoActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    if (key==CustomAction_ReviewLayer){
        DataManager::sharedManager()->removeItemWithID(14);
    }
}
