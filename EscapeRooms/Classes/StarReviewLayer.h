//
//  StarReviewLayer.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/08.
//
//

#ifndef __EscapeContainer__StarReviewLayer__
#define __EscapeContainer__StarReviewLayer__

#include "cocos2d.h"
#include "StarsSprite.h"

USING_NS_CC;

class StarsReviewLayerDelegate
{
public:
    virtual void didClosedStarsReviewLayer(){};
};

class StarReviewLayer : public cocos2d::LayerColor
{
public:
    virtual bool init();
    CREATE_FUNC(StarReviewLayer);
    StarsSprite* starsSprite;
    Sprite* popup;
    Label*titleLabel;
    
    StarsReviewLayerDelegate*mDelegate;
    void setDelegate(StarsReviewLayerDelegate*p){mDelegate=p;};
    
    
    Size popup_size;
    float title_height;
    float buttons_height;
    
    void createListener();
    void createMain();
    void pushMenuButton(int index);
    void close();
    
    /**表示中animation、Submitできないanimation*/
    CC_SYNTHESIZE(bool, isAnimating, IsAnimating);
};

#endif /* defined(__EscapeContainer__StarReviewLayer__) */
