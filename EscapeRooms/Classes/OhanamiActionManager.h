//
//  ChocolateShopActionManager.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/11/29.
//
//

#ifndef __EscapeContainer__OhanamiActionManager__
#define __EscapeContainer__OhanamiActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

enum class OhanamiBezierType {
    STRAIGHT = 0,
    MOUNTAIN,
    ROTATION,
    WAVE
};

USING_NS_CC;

class OhanamiActionManager:public CustomActionManager
{    
public:
    static OhanamiActionManager* manager;
    static OhanamiActionManager* getInstance();
   
    //over ride
    int getDefaultBackNum();
    void backAction(std::string key,int backID,Node*parent,ShowType type);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void touchBeganAction(std::string key,Node*parent);
    
    void dragAction(std::string key,Node*parent,Vec2 pos);
    
    void itemBackAction(std::string key,PopUpLayer*parent);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    
    void customAction(std::string key,Node*parent);
    
    void showAnswerAction(std::string key, Node *parent, ShowType showType);
    void cameraShotAction(int backID);
    void arrowAction(std::string key,Node*parent,const onFinished& callback);


private:
    FiniteTimeAction* singAction(Node* parent);
    FiniteTimeAction* skewWithBeerAction(Sprite* target);    
    FiniteTimeAction* kujiAction(Node *parent, int index, ShowType type);
    ActionInterval* bezierCustomActions(Node *parent, Sprite *target, Vec2 endPos, OhanamiBezierType bezierType, bool fromLeft, float pow_);
    
    FiniteTimeAction* airplaneAction(Node *parent);
    void clearParticleAction(cocos2d::Node *parent);

};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
