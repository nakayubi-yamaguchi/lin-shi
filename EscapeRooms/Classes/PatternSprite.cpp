//
//  PatternSprite.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/04/17.
//
//

#include "PatternSprite.h"
#include "DataManager.h"
#include "CustomActionManager.h"
#include "Utils/Common.h"
using namespace cocos2d;

PatternSprite* PatternSprite::create(cocos2d::Node *parent)
{
    auto node=new PatternSprite();
    if (node&&node->init(parent)) {
        node->autorelease();
        return node;
    }
    
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool PatternSprite::init(cocos2d::Node *parent)
{
    if (!Sprite::init()) {
        return false;
    }
    
    setTextureRect(Rect(0, 0, parent->getContentSize().width, parent->getContentSize().height));
    setOpacity(0);
    setCascadeOpacityEnabled(false);
    
    beginPos=Vec2(0, 0);
    
    return true;
}

#pragma mark - ドラッグ時に呼びだし
void PatternSprite::drag(cocos2d::Vec2 pos, bool onItem)
{
    DrawNode* line;
    if (drawNodes.size()>0) {
        line=drawNodes.at(drawNodes.size()-1);//最後尾のlineを取得
        auto newBeginPos=beginPos;
        
        //終点判定を行う スタート開始範囲を再利用
        auto index=0;
        for (auto areaPos : startAreas)
        {//タッチ開始範囲を確認
            auto indexKey=AnswerManager::getInstance()->transformNumberToString(index);
            Rect endArea;
            
            //原点パターンによる分岐
            if(getCenteringAnchor()){
                endArea=Rect(getContentSize().width*areaPos.x-endAreaWidth/2, getContentSize().height*areaPos.y-endAreaWidth/2, endAreaWidth, endAreaWidth);
            }
            else{
                endArea=Rect(getContentSize().width*areaPos.x+startAreaWidth/2-endAreaWidth/2, getContentSize().height*areaPos.y+startAreaWidth/2-endAreaWidth/2, endAreaWidth, endAreaWidth);
            }
            
            if (endArea.containsPoint(pos)&&
                AnswerManager::getInstance()->getPasscode(onItem).find(indexKey)==std::string::npos) {
                log("パターン 追加確認 %s",indexKey.c_str());
                //範囲の中心に終点をセット
                pos=Vec2(endArea.origin.x+endAreaWidth/2, endArea.origin.y+endAreaWidth/2);
                
                //新たな線を追加
                newBeginPos=pos;
                addNewLine(newBeginPos, index);
                
                //input
                AnswerManager::getInstance()->appendPasscode(indexKey, onItem);
                
                break;
            }
            
            index++;
        }
        
        //今操作している線を描画し直す
        line->clear();
        line->setLineWidth(getLineWidth());
        line->drawLine(beginPos, pos, lineColor);
        
        //始点のリセット
        beginPos=newBeginPos;
    }
    else
    {//ラインを1つも引いていない
        auto index=0;
        for (auto areaPos : startAreas)
        {//タッチ開始範囲を確認
            Rect startArea;
            //原点パターンによる分岐
            if(getCenteringAnchor()){
                startArea=Rect(getContentSize().width*areaPos.x-startAreaWidth/2, getContentSize().height*areaPos.y-startAreaWidth/2, startAreaWidth, startAreaWidth);
            }
            else{
                startArea=Rect(getContentSize().width*areaPos.x, getContentSize().height*areaPos.y, startAreaWidth, startAreaWidth);
            }
            
            
            if (startArea.containsPoint(pos)) {
                //1つ目のラインを描画開始
                beginPos=getCenterOnStartArea(index);

                addNewLine(beginPos,index);
                //input
                auto indexKey=AnswerManager::getInstance()->transformNumberToString(index);
                AnswerManager::getInstance()->appendPasscode(indexKey, onItem);
                
                break;
            }
            
            index++;
        }
    }
}

#pragma mark - タッチendで呼びだし
void PatternSprite::end(bool onItem)
{
    if (drawNodes.size()<2) {
        clear(onItem);
        return;
    }
    
    //操作中の中途半端なラインを削除する
    auto line=drawNodes.at(drawNodes.size()-1);
    line->removeFromParent();
}

void PatternSprite::check(cocos2d::Node *parent, cocos2d::ValueMap answerMap, cocos2d::ValueMap map,bool onItem, const onFinished &callback)
{
    Vector<FiniteTimeAction*>actions;
    
    //正解判定
    auto correct=false;
    auto passcode=AnswerManager::getInstance()->getPasscode();
    log("入力中 %s answerable? %d",passcode.c_str(),AnswerManager::getInstance()->checkCustomActionAnswerable(answerMap));
    
    if (passcode==answerMap["answer"].asString()&&
        AnswerManager::getInstance()->checkCustomActionAnswerable(answerMap)) {
        correct=true;
        auto delay=DelayTime::create(.2);
        actions.pushBack(delay);
    }
    else{
        if (drawNodes.size()>1) {
            //点が2つ以上存在する時のみ演出を入れる
            Common::playSE(map["incorrectPlaySE"].asString().c_str());
            
            auto shakeSupName=map["shakeSupName"].asString();
            
            if (shakeSupName.size()>0) {
                auto sup=parent->getChildByName<Sprite*>(shakeSupName);
                auto duration=.1;
                auto vec=Vec2(parent->getContentSize().width*.02, 0);
                auto ease=1.5;
                auto shake=TargetedAction::create(sup, Sequence::create(EaseInOut::create(MoveBy::create(duration, vec), ease),
                                                                        EaseInOut::create(MoveBy::create(duration*2,Vec2(-vec.x*2, -vec.y*2)), ease),
                                                                        EaseInOut::create(MoveBy::create(duration, vec), ease), NULL));
                actions.pushBack(shake);
            }
        }
        
        clear(onItem);
    }
    
    auto call=CallFunc::create([callback,correct]{
        callback(correct);
    });
    actions.pushBack(call);
    
    parent->runAction(Sequence::create(actions));
}

DrawNode* PatternSprite::drawLineOnPattern(cocos2d::Vec2 start, cocos2d::Vec2 end)
{
    auto line=DrawNode::create();
    line->setLineWidth(getLineWidth());
    line->drawLine(start, end, lineColor);
    addChild(line);
    
    return line;
}

void PatternSprite::drawDotOnPattern(cocos2d::Vec2 pos)
{
    auto dot=DrawNode::create();
    dot->drawDot(pos, getLineWidth()*2, getLineColor());
    addChild(dot);
}

Vec2 PatternSprite::getCenterOnStartArea(int index)
{
    auto areaPos=startAreas.at(index);
    Rect startArea;
    if (getCenteringAnchor()) {
        startArea=Rect(getContentSize().width*areaPos.x-startAreaWidth/2, getContentSize().height*areaPos.y-startAreaWidth/2, startAreaWidth, startAreaWidth);
    }
    else{
        startArea=Rect(getContentSize().width*areaPos.x, getContentSize().height*areaPos.y, startAreaWidth, startAreaWidth);
    }
    return Vec2(startArea.origin.x+startAreaWidth/2, startArea.origin.y+startAreaWidth/2);
}

#pragma mark - Private
void PatternSprite::addNewLine(cocos2d::Vec2 pos, int index)
{
    Common::playSE(getPlaySE().c_str());
    
    auto line=drawLineOnPattern(pos, pos);    
    drawNodes.pushBack(line);
    
    //dotを追加
    drawDotOnPattern(pos);
}

void PatternSprite::clear(bool onItem)
{
    //画像を全てremove
    removeAllChildren();
    drawNodes.clear();
    
    AnswerManager::getInstance()->resetPasscode(onItem);
}
