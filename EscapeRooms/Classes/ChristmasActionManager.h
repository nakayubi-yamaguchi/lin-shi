//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__ChristmasActionManager__
#define __EscapeContainer__ChristmasActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class ChristmasActionManager:public CustomActionManager
{
public:
    static ChristmasActionManager* manager;
    static ChristmasActionManager* getInstance();
    
    //over ride
    void openingAction(Node*node,const onFinished& callback);
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void customAction(std::string key,Node*parent);
    void showAnswerAction(std::string key,Node*parent);

private:
    FiniteTimeAction* pictureAction(int backID,Node*parent,bool animate);
    FiniteTimeAction* monkeyAction(int index,Node*parent);
    void fallSnow(Node*parent,int backID);
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
