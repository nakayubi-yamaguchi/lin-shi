//
//  ContentsAlert.cpp
//  EscapeRooms
//
//  Created by yamaguchinarita on 2017/11/22.
//
//

#include "AchievementMenuLayer.h"
#include "DataManager.h"

#include "Utils/RewardMovieManager.h"
#include "Utils/ContentsAlert.h"
#include "Utils/UtilsMethods.h"
#include "Utils/Common.h"

#include "UIParts/MenuItemScale.h"
#include "UIParts/MenuForScroll.h"
#include "UIParts/RoundedBoxSprite.h"
#include "UIParts/SelectToggleBox.h"

#include "Utils/AchievementManager.h"

using namespace cocos2d;

AchievementMenuLayer* AchievementMenuLayer::create(const ccMenuCallback &callback)
{
    auto node =new AchievementMenuLayer();
    if (node && node->init(callback)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool AchievementMenuLayer::init(const ccMenuCallback &callback) {
    if (!CustomAlert::init(Common::localize("ACHIEVEMENT", "達成項目"), "", {"CLOSE"}, callback)) {
        return false;
    }
    
    setTitleColor(LightBlackColor);
    
    createAchievementMenu();
    
    return true;
}

#pragma mark- Private
void AchievementMenuLayer::createAchievementMenu()
{
    minSize = Size(round(getContentSize().width*.9), round(getContentSize().height*.7));
    
    //メインスプライトとタイトルラベルの位置を変更
    mainSprite->setTextureRect(Rect(0, 0, minSize.width, minSize.height));
    arrange();
    
    std::vector<std::string> items = {
        Common::localize("Rule", "達成済み"),
        Common::localize("Status", "ステータス")
    };
    
    auto segmentSprite = Sprite::create();
    segmentSprite->setTextureRect(Rect(0, 0, mainSprite->getContentSize().width, mainSprite->getContentSize().height*.4/*適当な高さ*/));
    segmentSprite->setPosition(mainSprite->getContentSize().width/2, titleLabel->getPositionY()-titleLabel->getContentSize().height/2-segmentSprite->getContentSize().height/2);
    segmentSprite->setCascadeOpacityEnabled(true);
    segmentSprite->setColor(Common::getColorFromHex(WhiteColor2));
    mainSprite->addChild(segmentSprite);
    
    auto segmentSize = Size(mainSprite->getContentSize().width-2, getContentSize().height*.05);
    auto onNormal = Common::getColorFromHex(MyColor);
    auto onSelected = Common::getColorFromHex(WhiteColor2);
    
    auto segment = SelectToggleBox::create(items, segmentSize, onNormal, onSelected, [this](Ref* sender){
        
        auto beforeOffset = m_tableContentOffset;
         m_tableContentOffset = m_tableView->getContentOffset();
        
        auto segment = (SelectToggleBox*) sender;
        int selected = segment->getSelectedToggleNum();
        
        //選択状態の更新
        _selectedSegment=selected;
        
        //テーブルをリロード
        m_tableView->reloadData();
        m_tableView->setContentOffset(beforeOffset);
    });
    
    segment->setPosition(segmentSprite->getContentSize().width/2, segmentSprite->getContentSize().height-segmentSize.height/2);
    segment->setSelectedToggleNum(_selectedSegment);
    
    segmentSprite->addChild(segment);
    
    //テーブルビューを生成
    auto tableViewSize=Size(mainSprite->getContentSize().width, mainSprite->getContentSize().height*.7-space);
    auto width=tableViewSize.width;
    cellSize=Size(width,width/6.5);
    
    m_tableView =TableView::create(this, tableViewSize);
    m_tableView->setDirection(TableView::Direction::VERTICAL);
    m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
    m_tableView->setDelegate(this);
    m_tableView->setPosition((mainSprite->getContentSize().width-tableViewSize.width)/2, m_menuItems.at(0)->getPositionY()+space*1.5);
    
    //アラートを削除する際にオパシティが透過するように。
    m_tableView->setCascadeOpacityEnabled(true);
    for (auto subView : m_tableView->getChildren()) {
        subView->setCascadeOpacityEnabled(true);
    }
    
    mainSprite->addChild(m_tableView);
    
    //オフセットを保持
    m_tableContentOffset = m_tableView->getContentOffset();
}

#pragma mark - Table
#pragma mark data source
ssize_t AchievementMenuLayer::numberOfCellsInTableView(TableView *table)
{
    return AchievementManager::getInstance()->loadingAchievementPlist().size();
}

Size AchievementMenuLayer::cellSizeForTable(TableView* table)
{
    return cellSize;
}

TableViewCell* AchievementMenuLayer::tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx)
{
    auto cell = table->dequeueCell();
    cell = new TableViewCell();
    cell->setCascadeOpacityEnabled(true);
    cell->autorelease();
    
    //白背景
    auto backSprite = Sprite::create();
    backSprite->setTextureRect(Rect(0, 0, cellSize.width, cellSize.height));
    backSprite->setPosition(cellSize/2);
    backSprite->setColor(Common::getColorFromHex(WhiteColor2));
    cell->addChild(backSprite);
    
    //選択状態
    auto selectedSprite = Sprite::create();
    selectedSprite->setTextureRect(backSprite->getTextureRect());
    selectedSprite->setColor(Common::getColorFromHex(LightBlackColor));
    selectedSprite->setOpacity(0);
    selectedSprite->setName("Selected");
    selectedSprite->setPosition(backSprite->getPosition());
    cell->addChild(selectedSprite);
    
    //ボーダーライン
    if (idx == 0) {
        Sprite* upperLine = Sprite::create();
        upperLine->setAnchorPoint(Point(0, 0));
        upperLine->setTextureRect(Rect(0, 0, cellSize.width, 1));
        upperLine->setPositionY(cellSize.height-2);
        upperLine->setColor(Common::getColorFromHex(LightBlackColor));
        cell->addChild(upperLine);
    }
    
    Sprite* line = Sprite::create();
    line->setAnchorPoint(Point(0, 0));
    line->setTextureRect(Rect(0, 0, cellSize.width, 1));
    line->setColor(Common::getColorFromHex(LightBlackColor));
    cell->addChild(line);
    
    //<!-----サムネイル画像------>
    auto thumbnailBack = Sprite::create();
    thumbnailBack->setTextureRect(Rect(0, 0, cellSize.height*.7, cellSize.height*.7));
    thumbnailBack->setColor(Common::getColorFromHex(MyColor));
    thumbnailBack->setPosition(cellSize.height/2, cellSize.height/2);
    thumbnailBack->setCascadeOpacityEnabled(true);
    cell->addChild(thumbnailBack);
    
    auto thumbnailBack2 = Sprite::create();
    thumbnailBack2->setTextureRect(Rect(0, 0, thumbnailBack->getContentSize().width*.9, thumbnailBack->getContentSize().width*.9));
    thumbnailBack2->setColor(Common::getColorFromHex(WhiteColor2));
    thumbnailBack2->setPosition(thumbnailBack->getContentSize()/2);
    thumbnailBack2->setCascadeOpacityEnabled(true);
    thumbnailBack->addChild(thumbnailBack2);
    
    auto thumbnailSprite = Sprite::createWithSpriteFrameName("present.png");
    thumbnailSprite->setScale(thumbnailBack2->getContentSize().height*.8/thumbnailSprite->getContentSize().height);
    thumbnailSprite->setPosition(thumbnailBack->getContentSize()/2);
    thumbnailBack->addChild(thumbnailSprite);
    
    auto achievementMap = AchievementManager::getInstance()->loadingAchievementPlist().at(idx).asValueMap();
    
    //<!-----名前ラベル------>
    auto itemText = achievementMap[MapKey_LocalizedName].asValueMap()[Common::localize("en", "ja", "ch", "tw", "ko")].asString();
    auto itemFontSize = thumbnailSprite->getBoundingBox().size.height/2;
    auto itemLabel = Label::createWithTTF(itemText, HiraginoMaruFont, itemFontSize);
    itemLabel->enableBold();
    itemLabel->setTextColor(Color4B(Common::getColorFromHex(LightBlackColor)));
    cell->addChild(itemLabel);
    
    if (_selectedSegment == 0) {//達成済み
        itemLabel->setPosition(thumbnailSprite->getPositionX()+cellSize.height*1.2/2+itemLabel->getContentSize().width/2,cellSize.height*.75);
    }
    else {//ステータス
        itemLabel->setPosition(thumbnailSprite->getPositionX()+cellSize.height*1.2/2+itemLabel->getContentSize().width/2,cellSize.height*.5);
    }

    //<!-----プログレス----->
    if (_selectedSegment == 0) {//レコードポイント
        auto recordPoints = achievementMap[MapKey_ResultPoint].asValueVector();
        auto nowCount = 5;//random(0, 50);//現在
        int recordDenominator = 0;//達成項目の分母
        bool isClear = false;//全てのアチーブメントをクリアしている。
        
        for (int i = 0; i < (int)recordPoints.size(); i++) {//どこまで達成しているか記憶
            int recordPoint = recordPoints.at(i).asInt();
            recordDenominator = recordPoint;

            if (nowCount < recordPoint) {//条件を達成していない場合
                break;
            }
            else {//達成している。
                if (i == recordPoints.size()-1) {//最後の一つなら
                    isClear = true;//このアチーブメント項目は全てクリアしている。
                }
                else{
                    nowCount-=recordPoint;
                }
            }
        }
        
        if (!isClear) {
            auto progressPoint = MIN(recordDenominator, nowCount);
            float progressRatio = (float)progressPoint/(float)recordDenominator;
            
            auto progressBack = Sprite::create();
            progressBack->setTextureRect(Rect(0, 0, cellSize.width*.5, cellSize.height*.35));
            progressBack->setPosition(cellSize.width*.45, cellSize.height*.4);
            progressBack->setColor(/*Common::getColorFromHex(WhiteColor)*/Color3B::GRAY);
            progressBack->setCascadeOpacityEnabled(true);
            cell->addChild(progressBack);
            
            auto progressBar = Sprite::create();
            progressBar->setTextureRect(Rect(0, 0, progressBack->getContentSize().width*progressRatio, progressBack->getContentSize().height*.9));
            progressBar->setPosition(progressBar->getContentSize().width/2, progressBack->getContentSize().height/2);
            progressBar->setColor(Common::getColorFromHex(MyColor));
            progressBack->addChild(progressBar);
            
            //進捗ラベル
            auto progressText = StringUtils::format("%d/%d", progressPoint, recordDenominator);
            auto progressLabel = Label::createWithTTF(progressText, HiraginoMaruFont, progressBack->getContentSize().height*.8);
            progressLabel->setTextColor(Color4B(Common::getColorFromHex(WhiteColor2)));
            progressLabel->setPosition(progressBack->getContentSize()/2);
            progressBack->addChild(progressLabel);
        }
        else {
            auto clearLabel = Label::createWithTTF("-----CLEAR!!-----", HiraginoMaruFont, cellSize.height*.3);
            clearLabel->enableBold();
            clearLabel->enableItalics();
            clearLabel->setPosition(cellSize.width*.45, cellSize.height*.4);
            clearLabel->setTextColor(Color4B::RED);
            cell->addChild(clearLabel);
        }
        
        //<!-----購入ボタン------>
        auto p_sprite = Sprite::create();
        float p_height = cellSize.height/2;
        p_sprite->setTextureRect(Rect(0, 0, p_height*3, p_height));
        p_sprite->setColor(Common::getColorFromHex(MyColor));
        p_sprite->setCascadeOpacityEnabled(true);
        
        //立体的に見せるよう
        auto p_sprite_back = Sprite::create();
        p_sprite_back->setTextureRect(Rect(0, 0, p_sprite->getContentSize().width, p_sprite->getContentSize().height*.04));
        p_sprite_back->setColor(Common::getColorFromHex(BlackColor));
        p_sprite_back->setOpacity(180);
        p_sprite_back->setPosition(p_sprite->getContentSize().width/2, -p_sprite_back->getContentSize().height/2);
        p_sprite->addChild(p_sprite_back);
        
        //金額ラベル
        auto priceLabel = Label::createWithTTF("詳細", Common::getUsableFontPath(HiraginoMaruFont), p_sprite->getContentSize().height*.45);
        priceLabel->setPosition(p_sprite->getContentSize()/2);
        priceLabel->enableBold();
        p_sprite->addChild(priceLabel);
        
        //ボタン作成
        auto purchaseButton = MenuItemScale::create(p_sprite, p_sprite, [this, idx](Ref*sender){
            
        });
        purchaseButton->setTappedScale(.9);
        purchaseButton->setPosition(cellSize.width-purchaseButton->getContentSize().width/2*1.2, cellSize.height/2);
        
        auto purchaseMenu = MenuForScroll::createWithArray({purchaseButton});
        //purchaseMenu->setScrollView((ui::ScrollView*)m_tableView);
        purchaseMenu->setPosition(Vec2::ZERO);
        
        cell->addChild(purchaseMenu);
    }
    else {
        //<!-----購入ボタン------>
        auto p_sprite = Sprite::create();
        float p_height = cellSize.height/2;
        p_sprite->setTextureRect(Rect(0, 0, p_height*3, p_height));
        p_sprite->setColor(Common::getColorFromHex(MyColor));
        p_sprite->setCascadeOpacityEnabled(true);
        
        //立体的に見せるよう
        auto p_sprite_back = Sprite::create();
        p_sprite_back->setTextureRect(Rect(0, 0, p_sprite->getContentSize().width, p_sprite->getContentSize().height*.04));
        p_sprite_back->setColor(Common::getColorFromHex(BlackColor));
        p_sprite_back->setOpacity(180);
        p_sprite_back->setPosition(p_sprite->getContentSize().width/2, -p_sprite_back->getContentSize().height/2);
        p_sprite->addChild(p_sprite_back);
        
        //金額ラベル
        auto priceLabel = Label::createWithTTF("100回", Common::getUsableFontPath(HiraginoMaruFont), p_sprite->getContentSize().height*.45);
        priceLabel->setPosition(p_sprite->getContentSize()/2);
        priceLabel->enableBold();
        p_sprite->addChild(priceLabel);
        
        //ボタン作成
        auto purchaseButton = MenuItemScale::create(p_sprite, p_sprite, [this, idx](Ref*sender){
            
        });
        purchaseButton->setEnabled(false);
        //purchaseButton->setTappedScale(.9);
        purchaseButton->setPosition(cellSize.width-purchaseButton->getContentSize().width/2*1.2, cellSize.height/2);
        
        auto purchaseMenu = MenuForScroll::createWithArray({purchaseButton});
        //purchaseMenu->setScrollView((ui::ScrollView*)m_tableView);
        purchaseMenu->setPosition(Vec2::ZERO);
        
        cell->addChild(purchaseMenu);
    }
    
    return cell;
}

#pragma mark delegate
void AchievementMenuLayer::tableCellHighlight(TableView* table, TableViewCell* cell)
{

    auto selectedSprite = cell->getChildByName("Selected");
    auto fadein = FadeTo::create(.1, 255/3);
    selectedSprite->runAction(fadein);
}

void AchievementMenuLayer::tableCellUnhighlight(cocos2d::extension::TableView *table, cocos2d::extension::TableViewCell *cell)
{
    auto selectedSprite = cell->getChildByName("Selected");
    auto fadeout = FadeOut::create(.1);
    selectedSprite->runAction(fadeout);
}

void AchievementMenuLayer::tableCellTouched(cocos2d::extension::TableView *table, cocos2d::extension::TableViewCell *cell)
{
    return;
    
    //選択されたアニメーション
    auto selectedSprite = Sprite::create();
    selectedSprite->setTextureRect(Rect(0, 0, cellSize.width ,cellSize.height));
    selectedSprite->setColor(Common::getColorFromHex(LightBlackColor));
    selectedSprite->setOpacity(100);
    selectedSprite->setPosition(cellSize/2);
    cell->addChild(selectedSprite);
    
    auto fadeout = FadeOut::create(.2);
    auto remove = RemoveSelf::create();
    selectedSprite->runAction(Sequence::create(fadeout, remove, NULL));
    //

}



