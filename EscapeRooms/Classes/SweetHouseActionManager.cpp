//
//  IzakayaActionManager.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2018/10/17.
//
//

#include "SweetHouseActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "GameScene.h"
#include "TouchManager.h"

using namespace cocos2d;

SweetHouseActionManager* SweetHouseActionManager::manager =NULL;


SweetHouseActionManager* SweetHouseActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new SweetHouseActionManager();
    }
    
    return manager;
}

#pragma mark - Back
void SweetHouseActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
#pragma mark ハット
    if(key=="hat")
    {
        if (DataManager::sharedManager()->getTemporaryFlag("hat").asInt()==1) {
            auto hat=parent->getChildByName("sup_14_hat.png");
            parent->runAction(moveHat(parent, hat,false));
        }
    }
#pragma mark クッキーの目
    else if (key == "eyes") {
        
        Vector<FiniteTimeAction*>actions;
        auto distance=parent->getContentSize().width*.015;
        auto duration=0;
        for (int i=0; i<4; i++) {
            auto eyes=createAnchorSprite(StringUtils::format("sup_%d_eyes_%d.png",backID,i), .25+i*.25, .3, false, parent, true);

            auto x=distance;
            auto y=distance;
            if (i==0) {
                y=0;
            }
            else if (i==3){
                x=-distance;
                y=0;
            }
            else if(i==1){
                x=0;
                y=-distance;
            }
            else{
                x=-distance;
                y=0;
            }
            actions.pushBack(Sequence::create(DelayTime::create(0),
                                              swingAction(Vec2(x, y), duration, eyes, 1.5, false), NULL));
        }

        parent->runAction(Repeat::create(Spawn::create(actions), 1));
    }
#pragma mark 暖炉けむり
    else if (key == "fireplace"&&
             DataManager::sharedManager()->getEnableFlagWithFlagID(19)) {
        parent->runAction(addSmoke(parent, backID));
    }
#pragma mark 魚
    else if(key=="fish")
    {
        Vector<FiniteTimeAction*>actions;
        if (!DataManager::sharedManager()->getEnableFlagWithFlagID(32)) {
            auto clip=createClippingNodeToCenter("sup_67_clip.png", parent);
            auto fish=createSpriteToCenter("sup_67_fish_0.png", false, parent);
            clip->addChild(fish);
            transformToAnchorSprite(parent, fish, Vec2(.5, .28));
            
            auto rotates=Repeat::create(Sequence::create(EaseInOut::create(RotateBy::create(3, -180), 1.5),
                                                         DelayTime::create(.5),
                                                         EaseInOut::create(RotateBy::create(3, -180), 1.5),
                                                         NULL),-1);
            
            fish->runAction(rotates);
        }
        else if(DataManager::sharedManager()->isPlayingMinigame()){
            auto mistake=parent->getChildByName("sup_67_mistake.png");
            mistake->retain();
            mistake->removeFromParent();
            auto clip=createClippingNodeToCenter("sup_67_clip_1.png", parent);
            clip->addChild(mistake);
            mistake->setPositionY(parent->getContentSize().height*.1);
            createSpriteToCenter("sup_67_cover.png", false, parent,true);
            
            actions.pushBack(DelayTime::create(1.5));
            
            actions.pushBack(TargetedAction::create(mistake, Sequence::create(createSoundAction("bubble.mp3"),
                                                                              Spawn::create(EaseOut::create(MoveTo::create(1.5, parent->getContentSize()/2), 1.5),
                                                                                            swingAction(3, .2, mistake, 1.5, true),
                                                                                            NULL),
                                                                              CallFunc::create([mistake,parent]{
                mistake->retain();
                mistake->removeFromParent();
                parent->addChild(mistake);
                
                DataManager::sharedManager()->setTemporaryFlag("mistake_67", 1);
            }),
                                                                               NULL)));
            
            parent->runAction(Sequence::create(actions));
        }
    }
#pragma mark  つぼ
    else if(key=="pot"){
        auto pig=parent->getChildByName("sup_87_pig_0.png");
        transformToAnchorSprite(parent, pig, Vec2(.5, .25));
        
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(36)) {
            parent->runAction(addSkullSmoke(parent, backID));
        }
    }
#pragma mark  ロフト
    else if(key=="loft"){
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(36)) {
            parent->runAction(addSkullSmoke(parent, backID));
        }
    }
#pragma mark ドア
    else if(key=="door")
    {//init
        DataManager::sharedManager()->setTemporaryFlag("door", 0);
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            auto mistake=parent->getChildByName("sup_9_mistake.png");
            mistake->runAction(Sequence::create(DelayTime::create(.5),
                                                FadeIn::create(.5), NULL));
        }
    }
#pragma mark しろうさぎ
    else if(key=="whiteRabbit"&&DataManager::sharedManager()->getEnableFlagWithFlagID(51)){
        auto gun=createSpriteToCenter("sup_27_gun.png", false, parent,true);
        
        parent->runAction(giveGunAction(parent, gun, false));
    }
}

#pragma mark - Touch
void SweetHouseActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
        Vector<FiniteTimeAction*> actions;
#pragma mark クラウド。欲しがる。
    if (key == "cloud") {
        int num = DataManager::sharedManager()->getNowBack();
        if (num == 10) {//monkey
            auto cloudBounce = cloudBounceAction(parent, {"sup_10_monkey_0.png"}, Vec2(.833, .15), "sup_10_cloud.png", Vec2(.75, .618), "");
            actions.pushBack(cloudBounce);
        }
        else if (num == 27) {//rabbit
            auto cloud = cloudAction(parent, "sup_27_cloud.png", Vec2(.415, .473), .4);
            actions.pushBack(cloud);
        }
        else if (num == 28) {//rabbit
            auto cloud = cloudAction(parent, "sup_28_cloud.png", Vec2(.5, .5), .4);
            actions.pushBack(cloud);
        }else if (num == 23) {//monkey
            auto cloudBounce = cloudBounceAction(parent, {"sup_23_monkey_0.png"}, Vec2(.5, .2), "sup_23_cloud.png", Vec2(.4, .67), "");
            actions.pushBack(cloudBounce);
        }
        else if (num == 68) {//monkey
            auto cloudBounce = cloudBounceAction(parent, {"sup_68_monkey_0.png"}, Vec2(.86, .25), "sup_68_cloud.png", Vec2(.8, .618), "");
            actions.pushBack(cloudBounce);
        }
        else if (num == 87) {//pig
            
            auto cloudBounce = cloudBounceAction(parent, {"sup_87_pig_0.png"}, Vec2(.5, .25),StringUtils::format("sup_87_cloud_%d.png",DataManager::sharedManager()->getEnableFlagWithFlagID(33)) , Vec2(.42, .61), "");
            actions.pushBack(cloudBounce);
        }
        
        //コールバック
        auto call = CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark クリーム+さる
    else if (key.compare(0,5,"cream")==0) {
        Common::playSE("p.mp3");
        
        auto backID=DataManager::sharedManager()->getNowBack();
        
        auto monkey_0=parent->getChildByName(StringUtils::format("sup_%d_monkey_0.png",backID));
        
        //give
        auto item = createAnchorSprite(StringUtils::format("sup_%d_cream.png",backID), .85, .5, true, parent, true);
        actions.pushBack(giveAction(item, 1, .7, true));
        
        //change monkey
        auto monkey_1=createSpriteToCenter(StringUtils::format("sup_%d_monkey_1.png",backID), true, parent, true);
        actions.pushBack(createChangeAction(1, .5, monkey_0, monkey_1, "monkey.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        //change to 2
        auto monkey_2=createAnchorSprite(StringUtils::format("sup_%d_monkey_2.png",backID),.315,.13, true, parent, true);
        monkey_2->setLocalZOrder(1);
        actions.pushBack(createChangeAction(1, .5, monkey_1, monkey_2, "pop.mp3"));

        //draw
        std::vector<float> distances={.125,.15,.1};
        auto distance_y=-parent->getContentSize().height*.01;
        std::vector<float> bounces={.05,.05,.1,.05};
        if (backID==68) {
            distances={.22,.22,.22};
            bounces={.05,.05,.1,.05};
            distance_y=0;
        }

        for (int i=0; i<4; i++) {
            std::string fileName;
            fileName=StringUtils::format("sup_%d_character_%d.png",backID,i);
            
            auto character=createSpriteToCenter(fileName, true, parent, true);
            auto fadein_character=TargetedAction::create(character, FadeIn::create(.5));
            auto bounce=createBounceAction(monkey_2, .5, bounces.at(i));
            
            auto spawn=Spawn::create(fadein_character,
                                     createSoundAction("pop.mp3"),
                                     bounce,
                                     NULL);
            actions.pushBack(spawn);
            
            //move
            if (i<3) {
                auto move=TargetedAction::create(monkey_2, EaseOut::create(MoveBy::create(.7, Vec2(distances.at(i)*parent->getContentSize().width, distance_y)), 1.5));
                actions.pushBack(move);
            }
        }
        
        actions.pushBack(DelayTime::create(1));
        
        //change monkey
        actions.pushBack(createChangeAction(.7, .5, monkey_2, monkey_0, "monkey.mp3"));
        actions.pushBack(DelayTime::create(1));
            
        //callback
        auto call = CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark うさぎ+ナイフ
    else if (key == "knife") {
        Common::playSE("p.mp3");
        
        auto rabbit_0=parent->getChildByName("sup_28_rabbit_0.png");
        
        //give
        auto item = createAnchorSprite("sup_28_knife.png", .5, .5, true, parent, true);
        actions.pushBack(giveAction(item, 1, .7, true));
        
        //catch
        auto knife_on_rabbit=createSpriteToCenter("sup_28_knife_1.png", true, rabbit_0,true);
        actions.pushBack(createSoundAction("pop.mp3"));
        actions.pushBack(TargetedAction::create(knife_on_rabbit, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(1));
        
        //jump up & set cake
        auto rabbit_1=createAnchorSprite("sup_28_rabbit_1.png", .4, .13, true, parent, true);
        auto rabbit_2=createSpriteToCenter("sup_28_rabbit_2.png", true, parent,true);

        auto cover=createSpriteToCenter("sup_28_cover.png", true, parent,true);
        
        auto cake_1=createSpriteToCenter("sup_28_cake_1.png", true, parent,true);
        auto cake_cover=createSpriteToCenter("sup_28_cake_cover.png", true, parent,true);
        
        auto change_cake=Spawn::create(TargetedAction::create(parent->getChildByName("sup_28_cake_0.png"), FadeOut::create(0)),
                                       TargetedAction::create(cover, FadeIn::create(0)),
                                       TargetedAction::create(cake_1, FadeIn::create(0)),
                                       TargetedAction::create(cake_cover, FadeIn::create(0))
                                       , NULL);
        actions.pushBack(change_cake);
        
        auto spawn=Spawn::create(createChangeAction(.2, .1, rabbit_0, rabbit_1),
                                 TargetedAction::create(knife_on_rabbit, FadeOut::create(.2)),
                                 TargetedAction::create(rabbit_1, JumpBy::create(1.5, Vec2(-parent->getContentSize().width*.1,0),parent->getContentSize().height*.2, 1)),
                                 Sequence::create(DelayTime::create(.5),
                                                  createBounceAction(rabbit_1, .3, .05), NULL),
                                 createSoundAction("jump.mp3")
                                 , NULL);
        actions.pushBack(spawn);
        
        //cake cut
        auto change_rabbit=createChangeAction(.3, .2, rabbit_1, rabbit_2, "sword.mp3");
        auto cut_off=TargetedAction::create(cake_cover, Spawn::create(FadeOut::create(.5),
                                                                      createSoundAction("sword_2.mp3"),
                                                                      EaseOut::create(MoveBy::create(.5, Vec2(parent->getContentSize().width*.15, 0)), 2.0),
                                                                      NULL));
        actions.pushBack(Sequence::create(change_rabbit,
                                          DelayTime::create(1),
                                       cut_off,
                                       NULL));
        
        
        actions.pushBack(DelayTime::create(1));
        
        //change
        actions.pushBack(createChangeAction(1, .7, rabbit_2, rabbit_0, "pop.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark ケーキopen
    else if(key == "cake_open") {
        auto back_28=createSpriteToCenter("back_28.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back_28, 28, true);
        
        //fadein
        actions.pushBack(TargetedAction::create(back_28, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        
        //case
        auto case_=back_28->getChildByName("sup_28_case.png");
        auto cake=createSpriteToCenter("sup_28_cake_0.png", true, parent,true);
        
        actions.pushBack(createChangeAction(.5, .2, case_, cake,"pop.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark チョコopen
    else if(key=="openChoco")
    {
        auto back_48=createSpriteToCenter("back_48.png", true, parent,true);
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(Spawn::create(TargetedAction::create(back_48, FadeIn::create(1)),
                                       createSoundAction("pop.mp3"), NULL));
        actions.pushBack(DelayTime::create(1));

        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark ハット
    else if(key == "hat") {
        auto hat=parent->getChildByName("sup_14_hat.png");
        
        Common::playSE("pop.mp3");
        actions.pushBack(moveHat(parent, hat, true));
        
        auto call=CallFunc::create([callback](){
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark うさぎ+gun
        else if (key=="gun") {
            Common::playSE("p.mp3");
            
            auto rabbit_0 = parent->getChildByName(StringUtils::format("sup_27_rabbit_0.png"));
            transformToAnchorSprite(parent, rabbit_0, Vec2(.5, .232));
            
            auto gun=createSpriteToCenter("sup_27_gun.png", true, rabbit_0,true);
            
            auto rabbit_1_container=Sprite::create();
            rabbit_1_container->setTextureRect(Rect(0,0,parent->getContentSize().width,parent->getContentSize().height));
            rabbit_1_container->setOpacity(0);
            rabbit_1_container->setPosition(parent->getContentSize()/2);
            parent->addChild(rabbit_1_container);
            
            auto rabbit_1 = createAnchorSprite("sup_27_rabbit_1.png", rabbit_0->getAnchorPoint().x, rabbit_0->getAnchorPoint().y, true, rabbit_1_container, true);

            //アニメーション
            auto give_gun=giveGunAction(parent, gun, true);
            actions.pushBack(createSoundAction("pop_5.mp3"));
            actions.pushBack(give_gun);
            actions.pushBack(DelayTime::create(1));
            auto bounce=createBounceAction(rabbit_0, .5, .05);
            actions.pushBack(bounce);
            
            //change 銃を構える
            auto change_rabbit_1=createChangeAction(1, .5, rabbit_0, rabbit_1, "pop.mp3");
            actions.pushBack(change_rabbit_1);
            actions.pushBack(DelayTime::create(.5));
            
            //炎を発射
            auto duration=6.0;
            auto shake_rabbit=TargetedAction::create(rabbit_1, jumpAction(Vec2(-parent->getContentSize().width*.02, 0), .1, rabbit_1, 1, duration/.25));
            auto move_rabbit=TargetedAction::create(rabbit_1_container, Sequence::create(MoveBy::create(duration/6, Vec2(-parent->getContentSize().width*.25, 0)),
                                                                                         MoveBy::create(duration/3, Vec2(parent->getContentSize().width*.6, 0)),
                                                                                         MoveBy::create(duration/6, Vec2(-parent->getContentSize().width*.35, 0)), NULL));
            
            auto fire=CallFunc::create([parent,rabbit_1,duration]{
                Common::playSE("fire.mp3");
                BlendFunc func;
                func.dst=1;
                func.src=770;
                
                auto particle=ParticleSystemQuad::create("fire.plist");
                particle->setTexture(Sprite::create("particle_fire.png")->getTexture());
                particle->setStartColor(Color4F(.1, .1, 1, 1));
                particle->setEndColor(Color4F(.1,.1,1,0));
                
                particle->setBlendFunc(func);
                particle->setAutoRemoveOnFinish(true);
                particle->setDuration(duration-2);
                particle->setLife(.8);
                particle->setLifeVar(0);
                particle->setPosition(parent->getContentSize().width*.44,parent->getContentSize().height*.33);
                particle->setPosVar(Vec2::ZERO);
                particle->setSpeed(parent->getContentSize().height*1);
                particle->setSpeedVar(particle->getSpeed()*.02);
                particle->setGravity(Vec2(0, parent->getContentSize().height*3));
                particle->setTotalParticles(200);
                particle->setAngle(-90);
                particle->setAngleVar(10);
                particle->setStartSize(parent->getContentSize().width*.04);
                particle->setStartSizeVar(parent->getContentSize().width*.02);
                particle->setEndSize(parent->getContentSize().width*.08);
                particle->setEndSizeVar(parent->getContentSize().width*.02);
                
                rabbit_1->addChild(particle);
            });
            
            Vector<FiniteTimeAction*>hint_actions;
            auto duration_hint=duration/7;
            hint_actions.pushBack(TargetedAction::create(createSpriteToCenter("sup_27_hint_2.png", true, parent,true), FadeIn::create(duration_hint)));
            hint_actions.pushBack(TargetedAction::create(createSpriteToCenter("sup_27_hint_1.png", true, parent,true), FadeIn::create(duration_hint)));
            hint_actions.pushBack(TargetedAction::create(createSpriteToCenter("sup_27_hint_0.png", true, parent,true), FadeIn::create(duration_hint)));
            hint_actions.pushBack(TargetedAction::create(createSpriteToCenter("sup_27_hint_3.png", true, parent,true), FadeIn::create(duration_hint)));
            hint_actions.pushBack(TargetedAction::create(createSpriteToCenter("sup_27_hint_4.png", true, parent,true), FadeIn::create(duration_hint)));
            
            auto seq_hint=Sequence::create(hint_actions);
            
            actions.pushBack(Spawn::create(shake_rabbit,move_rabbit,fire,seq_hint, NULL));
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(createChangeAction(.5, .3, rabbit_1, rabbit_0));
            actions.pushBack(DelayTime::create(1));
            auto call=CallFunc::create([callback](){
                Common::playSE("pinpon.mp3");
                callback(true);
            });
            actions.pushBack(call);
            
            parent->runAction(Sequence::create(actions));
        }
#pragma mark シェフ+コイン
        else if (key == "coins") {
            Common::playSE("p.mp3");
            auto chef_0=parent->getChildByName("sup_35_chef.png");
            auto chef_1=createAnchorSprite("sup_35_chef_1.png", .42, .2, true, parent,true);
            auto chef_2=createAnchorSprite("sup_35_chef_2.png", .42, .2, true, parent,true);
            auto pos_chef_2=chef_2->getPosition();
            chef_2->setPositionY(-parent->getContentSize().height*.4);

            //give
            auto coins = createSpriteToCenter("sup_35_coins.png", true, parent, true);
            coins->setLocalZOrder(1);
            actions.pushBack(TargetedAction::create(coins, FadeIn::create(1)));
            actions.pushBack(TargetedAction::create(coins, Spawn::create(MoveBy::create(1, Vec2(0, parent->getContentSize().height*.1)),
                                                                         ScaleBy::create(1, .9), NULL)));
            actions.pushBack(DelayTime::create(.5));
            
            //change chef
            actions.pushBack(createChangeAction(1, .7, {chef_0,coins}, {chef_1}, "pop.mp3"));
            actions.pushBack(DelayTime::create(1));
            
            //jump
            actions.pushBack(createSoundAction("pig.mp3"));
            actions.pushBack(Spawn::create(createBounceAction(chef_1, .3, .05),
                                           jumpAction(Vec2(0, parent->getContentSize().height*.1), .15, chef_1, 1.5,2),
                                           NULL));
            actions.pushBack(DelayTime::create(1));
            
            //sink
            actions.pushBack(TargetedAction::create(chef_1, MoveBy::create(2, Vec2(0, -parent->getContentSize().height*.5))));
            actions.pushBack(createSoundAction("goshigoshi_short.mp3"));
            //re appear
            actions.pushBack(createChangeAction(1, 1, chef_1, chef_2));
            actions.pushBack(TargetedAction::create(chef_2, EaseOut::create(MoveTo::create(2, pos_chef_2), 1.5)));
            actions.pushBack(createSoundAction("pig.mp3"));
            actions.pushBack(DelayTime::create(2));
            
            //コールバック
            auto call=CallFunc::create([callback](){
                //Common::playSE("pinpon.mp3");
                callback(true);
            });
            actions.pushBack(call);
            
            parent->runAction(Sequence::create(actions));
        }
#pragma mark かぎ
        else if (key == "key") {
            Common::playSE("p.mp3");
            auto key_0=createSpriteToCenter("sup_62_key_0.png", true, parent,true);
            auto key_1=createSpriteToCenter("sup_62_key_1.png", true, parent,true);

            auto action=createChangeActionSequence(1, .7, {key_0,key_1}, {"kacha.mp3","key_1.mp3"});
            actions.pushBack(action);
            
            auto call=CallFunc::create([callback]{
                Common::playSE("pinpon.mp3");
                callback(true);
            });
            actions.pushBack(call);
            parent->runAction(Sequence::create(actions));
        }
#pragma mark チョコ注ぐ
        else if (key == "chocolate") {
            Common::playSE("p.mp3");
            //monkey
            auto monkey_0=parent->getChildByName("sup_23_monkey_0.png");
            transformToAnchorSprite(parent, monkey_0, Vec2(.5, .2));
            
            //pot
            auto item=createAnchorSprite("sup_23_pot.png", .52, .5, true, parent, true);
            actions.pushBack(TargetedAction::create(item, FadeIn::create(1)));
            actions.pushBack(createSoundAction("monkey.mp3"));
            actions.pushBack(createBounceAction(monkey_0, .5, .025));
            
            //pour
            auto angle=-40;
            auto clip=createClippingNodeToCenter("sup_23_clip.png", parent);
            auto choco=createSpriteToCenter("sup_23_chocolate.png", false, parent);
            choco->setPositionY(parent->getContentSize().height*.4);
            clip->addChild(choco);
            auto spawn=Spawn::create(TargetedAction::create(item, RotateBy::create(3, angle)),
                                     createSoundAction("glass-wine1.mp3"),
                                     TargetedAction::create(choco, MoveTo::create(3,parent->getContentSize()/2)),
                                     NULL);
            actions.pushBack(spawn);
            //take back
            auto takeback=TargetedAction::create(item, Sequence::create(RotateBy::create(2, -angle),
                                                                        FadeOut::create(1), NULL));
            actions.pushBack(takeback);
            
            //change monkey
            auto monkey_1=createSpriteToCenter("sup_23_monkey_1.png", true, parent,true);
            auto monkey_2=createSpriteToCenter("sup_23_monkey_2.png", true, parent,true);
            auto knife=createSpriteToCenter("sup_23_knife.png", true, parent,true);

            auto monkey_3=createSpriteToCenter("sup_23_monkey_3.png", true, parent,true);

            actions.pushBack(createChangeAction(.7, .3, {monkey_0,choco}, {monkey_1},"monkey.mp3"));
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(createChangeAction(.7, .3, monkey_1, monkey_2,"drink.mp3"));
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(createChangeAction(.7, .3, monkey_2, monkey_0,"pop.mp3"));
            actions.pushBack(DelayTime::create(1));
            actions.pushBack(createSoundAction("monkey.mp3"));
            actions.pushBack(createBounceAction(monkey_0, .5, .05));
            actions.pushBack(createChangeAction(.7, .3, monkey_0, monkey_3,"pop.mp3"));
            actions.pushBack(DelayTime::create(.5));
            actions.pushBack(Spawn::create(TargetedAction::create(monkey_3, FadeOut::create(.7)),
                                           TargetedAction::create(knife, FadeIn::create(0)),
                                           NULL));
            actions.pushBack(DelayTime::create(1));

            auto call=CallFunc::create([callback]{
                Common::playSE("pinpon.mp3");
                callback(true);
            });
            actions.pushBack(call);
            parent->runAction(Sequence::create(actions));
        }
#pragma mark はしごを置く
    else if(key=="putRadder")
    {
        Common::playSE("p.mp3");
        auto put=putAction(createSpriteToCenter("sup_49_ladders.png", true, parent,true), "koto.mp3", 1, 1);
        actions.pushBack(put);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
#pragma mark キャンディはめ
        else if (key.compare(0,8,"putCandy")==0) {
            Common::playSE("p.mp3");
            auto index=atoi(key.substr(9,1).c_str())+1;
            //put
            auto item=createSpriteToCenter(StringUtils::format("sup_31_candy_%d.png",index), true, parent,true);
            actions.pushBack(putAction(item, "kacha.mp3", .5,1));
            
            //コールバック
            auto call=CallFunc::create([callback]{
                Common::playSE("pinpon.mp3");
                callback(true);
            });
            actions.pushBack(call);
            
            parent->runAction(Sequence::create(actions));
        }
#pragma mark キャンディレバー
    else if (key.compare(0,5,"candy")==0) {
        auto index=atoi(key.substr(6,1).c_str());
        //pull
        auto action=candyPullAction(parent, index);
        actions.pushBack(action);
        //コールバック
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }

#pragma mark キャンディ正解後
    else if (key == "chimney") {
        auto clip=createClippingNodeToCenter("sup_31_clip.png", parent);
        
        for (int i=0; i<4; i++) {
            auto santas=createSpriteToCenter(StringUtils::format("sup_31_santas_%d.png",i), false, parent);
            clip->addChild(santas);
            
            santas->setPositionY(parent->getContentSize().height*.3);
            
            actions.pushBack(Spawn::create(DelayTime::create(.2*i),
                                           createSoundAction("nyu.mp3"),
                                           TargetedAction::create(santas, MoveTo::create(.2, parent->getContentSize()/2))
                                           , NULL));
        }

        actions.pushBack(DelayTime::create(1));
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark マッチ
    else if (key == "match") {
        Common::playSE("p.mp3");
        
        //set match
        auto item = createSpriteToCenter("sup_17_match.png", true, parent, true);
        actions.pushBack(TargetedAction::create(item, Sequence::create(FadeIn::create(1),
                                                                       swingAction(Vec2(0, parent->getContentSize().height*.02), .1, item, 1.5, false, 2), NULL)));
        actions.pushBack(DelayTime::create(1));
        
        //fire
        auto addFire=CallFunc::create([parent]{
            Common::playSE("huo.mp3");
            
            auto particleMap=DataManager::sharedManager()->getParticleVector(false, 17).at(0).asValueMap();
            auto particle=SupplementsManager::getInstance()->getParticle(parent, particleMap);
            parent->addChild(particle);
        });
        actions.pushBack(addFire);
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(TargetedAction::create(item, FadeOut::create(1)));
        
        //scene change
        auto back_13=createSpriteToCenter("back_13.png", true, parent,true);
        back_13->setLocalZOrder(1);
        SupplementsManager::getInstance()->addSupplementToMain(back_13, 13, true);
        
        actions.pushBack(TargetedAction::create(back_13, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        
        auto snowman_0=back_13->getChildByName("sup_13_snowman_0.png");
        transformToAnchorSprite(parent, snowman_0, Vec2(.5, 0));
        auto snowman_1=createSpriteToCenter("sup_13_snowman_1.png", true, back_13, true);

        actions.pushBack(Spawn::create(createChangeAction(1, .7, snowman_0, snowman_1, "pop_accend.mp3"),
                                       TargetedAction::create(snowman_0, EaseIn::create(ScaleBy::create(1, 1,.7), 2.0)), NULL));
        actions.pushBack(DelayTime::create(1.5));
        actions.pushBack(TargetedAction::create(back_13, FadeOut::create(1)));
        
        //コールバック
        auto call=CallFunc::create([callback,this,parent]{
            Common::playSE("pinpon.mp3");
            parent->runAction(this->addSmoke(parent, DataManager::sharedManager()->getNowBack()));
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark ドアあける
    else if (key == "open") {
        //
        Common::playSE("gacha.mp3");
        
        auto open = createSpriteToCenter("sup_13_open.png", true, parent, true);
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            createSpriteToCenter("sup_13_mistake_open.png", false, open, true);
        }
        actions.pushBack(TargetedAction::create(open, FadeIn::create(.3)));
        actions.pushBack(DelayTime::create(.3));
        //コールバック
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark シャベル
    else if (key == "scop") {
        Common::playSE("p.mp3");

        auto scop = createAnchorSprite("sup_69_scop.png",.5,.8, true, parent, true);
        auto cover=createSpriteToCenter("sup_69_cover.png", true, parent,true);
        actions.pushBack(TargetedAction::create(scop, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        
        //ready
        auto angle=-30;
        auto rotate_0=TargetedAction::create(scop, EaseInOut::create(RotateBy::create(.5, angle), 1.5));
        auto rotate_1=TargetedAction::create(scop, EaseIn::create(RotateBy::create(.25, -angle), 2.0));
        
        actions.pushBack(rotate_0);
        actions.pushBack(TargetedAction::create(cover, FadeIn::create(1)));
        
        //dig
        auto snow=CallFunc::create([parent,this]
                                   {
                                       Common::playSE("dig.mp3");
                                       
                                       BlendFunc func;
                                       func.dst=1;
                                       func.src=770;
                                       
                                       auto particle=ParticleSystemQuad::create("particle_shower.plist");
                                       particle->setTexture(Sprite::create("particle_fire.png")->getTexture());
                                       particle->setStartColor(Color4F(1, 1, 1, 1));
                                       particle->setEndColor(Color4F(1,1,1,0));
                                       particle->setBlendFunc(func);
                                       particle->setAutoRemoveOnFinish(true);
                                       particle->setDuration(.2);
                                       particle->setPosition(parent->getContentSize().width*.65,parent->getContentSize().height*.61);
                                       particle->setAngle(70);
                                       particle->setAngleVar(15);
                                       particle->setSpeed(parent->getContentSize().height*.5);
                                       particle->setSpeedVar(particle->getSpeed()*.1);
                                       particle->setGravity(Vec2(0, -parent->getContentSize().height*1));
                                       particle->setTotalParticles(30);
                                       particle->setLife(1);
                                       particle->setLifeVar(.25);
                                       particle->setStartSize(parent->getContentSize().width*.03);
                                       particle->setStartSizeVar(parent->getContentSize().width*.02);
                                       particle->setEndSize(parent->getContentSize().width*.02);
                                       particle->setEndSizeVar(parent->getContentSize().width*.01);
                                       
                                       parent->addChild(particle);
                                   });
        auto seq=Sequence::create(rotate_1,
                                  snow,
                                  DelayTime::create(1),
                                  rotate_0->clone(),NULL);
        
        //
        auto back_67=createSpriteToCenter("back_67.png", true, parent,true);
        createSpriteToCenter("sup_67_fish_0.png", false, back_67,true);
        auto fadein_back=Sequence::create(CallFunc::create([scop]{scop->setLocalZOrder(1);}),
                                          TargetedAction::create(back_67,FadeIn::create(2))
                                                        , NULL);
        
        actions.pushBack(Sequence::create(Repeat::create(seq, 3),
                                       fadein_back, NULL));
        
        //
        actions.pushBack(DelayTime::create(.5));
        
        //faddout scop
        actions.pushBack(TargetedAction::create(scop, FadeOut::create(.5)));
        
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark つり
    else if (key == "fishing") {
        Common::playSE("p.mp3");
        //put
        auto fishing = createSpriteToCenter(StringUtils::format("sup_67_fishing.png"), true, parent, true);
        fishing->setPositionY(parent->getContentSize().height*.6);
        actions.pushBack(TargetedAction::create(fishing, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(TargetedAction::create(fishing, MoveTo::create(.5, parent->getContentSize()/2)));
        actions.pushBack(createSoundAction("pochan.mp3"));
        
        //change fish
        auto fish_0=parent->getChildByName("sup_67_clip.png")->getChildByName("sup_67_fish_0.png");
        auto fish_1=createSpriteToCenter("sup_67_fish_1.png", true, parent,true);
        actions.pushBack(TargetedAction::create(fish_0, FadeOut::create(1)));
        actions.pushBack(TargetedAction::create(fish_0, RemoveSelf::create()));
        actions.pushBack(DelayTime::create(1));
        
        actions.pushBack(CallFunc::create([parent]{
            Common::playSE("bashaa.mp3");
            NativeBridge::vibration3D();
            
            BlendFunc func;
            func.dst=1;
            func.src=770;
            
            auto particle=ParticleSystemQuad::create("particle_shower.plist");
            particle->setTexture(Sprite::create("particle_fire.png")->getTexture());
            particle->setStartColor(Color4F(.5, .5, 1, 1));
            particle->setEndColor(Color4F(.5,.5,1,0));
            particle->setBlendFunc(func);
            particle->setAutoRemoveOnFinish(true);
            particle->setDuration(.75);
            particle->setPosition(parent->getContentSize().width*.5,parent->getContentSize().height*.55);
            particle->setSpeed(parent->getContentSize().height*1);
            particle->setSpeedVar(particle->getSpeed()*.1);
            particle->setGravity(Vec2(0, -parent->getContentSize().height*3));
            particle->setTotalParticles(50);
            particle->setAngle(90);
            particle->setAngleVar(10);
            particle->setLife(.5);
            particle->setLifeVar(.25);
            particle->setStartSize(parent->getContentSize().width*.02);
            particle->setStartSizeVar(parent->getContentSize().width*.01);
            particle->setEndSize(parent->getContentSize().width*.03);
            particle->setEndSizeVar(parent->getContentSize().width*.01);
            
            parent->addChild(particle);
        }));
        
        actions.pushBack(TargetedAction::create(fish_1, FadeIn::create(.1)));
        
        //pull
        auto distance=parent->getContentSize().height*.02;
        auto pull=Spawn::create(swingAction(Vec2(0, -distance), .1, fish_1, 1.5, false, 2),
                                swingAction(Vec2(0, -distance), .1, fishing, 1.5, false, 2),
                                createSoundAction("nyu.mp3"),
                                NULL);
        actions.pushBack(Repeat::create(Sequence::create(pull,DelayTime::create(.2), NULL), 4));
        actions.pushBack(DelayTime::create(.3));
        
        //get
        auto fishing_1=createSpriteToCenter("sup_67_fishing.png", true, parent,true);
        fishing_1->setPosition(parent->getContentSize().width*.52,parent->getContentSize().height*.825);

        auto fish_2=createAnchorSprite("sup_67_fish_2.png", .525, .81, true, parent, true);
        auto change_fish=createChangeAction(.5, .3, fish_1, fish_2);
        auto change_fishing=createChangeAction(.5, .3, fishing, fishing_1);
        auto swing=swingAction(5, .5, fish_2, 1.5, true, 2);
        actions.pushBack(Spawn::create(change_fish,
                                       change_fishing,
                                       createSoundAction("pop.mp3"),
                                       swing, NULL));
        
        actions.pushBack(DelayTime::create(.5));
        //コールバック
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark つぼ
    else if (key.compare(0,3,"pot")==0) {
        Common::playSE("p.mp3");
        auto index=atoi(key.substr(4,1).c_str());
        std::vector<std::string>itemNames={"sup_87_fish.png","sup_87_mashroom.png"};
        auto item=createSpriteToCenter(itemNames.at(index), true, parent,true);
        auto pig=parent->getChildByName("sup_87_pig_0.png");
        
        //throw
        auto distance_takeback=-parent->getContentSize().height*.1;
        auto seq_item_0=TargetedAction::create(item, Sequence::create(FadeIn::create(1),
                                                                    createSoundAction("pop_5.mp3"),
                                                                    MoveBy::create(1, Vec2(0, distance_takeback)),
                                                                    DelayTime::create(1),
                                                                     NULL));
        actions.pushBack(seq_item_0);
        
        auto seq_item_1=TargetedAction::create(item, Sequence::create(createSoundAction("pop_3.mp3"),
                                                                      Spawn::create(RotateBy::create(1, 250),
                                                                                    ScaleBy::create(1, .5),
                                                                                    JumpBy::create(1, Vec2(0, parent->getContentSize().height*.01), parent->getContentSize().height*.3, 1),
                                                                                    Sequence::create(DelayTime::create(.9),
                                                                                                     FadeOut::create(.1), NULL),
                                                                                    NULL),
                                                                      createSoundAction("dopon.mp3"),
                                                                      NULL)
                                               );
        actions.pushBack(seq_item_1);
        
        //add smoke
        auto smoke=[parent](bool strong){
            auto act=CallFunc::create([parent,strong]{
                BlendFunc func;
                func.dst=771;
                func.src=770;
                
                auto particle=ParticleSystemQuad::create("smoke.plist");
                particle->setAutoRemoveOnFinish(true);
                particle->setDuration(.2);
                particle->setPosition(parent->getContentSize().width/2,parent->getContentSize().height*.45);
                particle->setBlendFunc(func);
                particle->setStartColor(Color4F(.2,0,0,.5));
                particle->setEndColor(Color4F(0, 0, 0, 0));
                particle->setSpeed(parent->getContentSize().height*.1);
                particle->setGravity(Vec2(0, -parent->getContentSize().height*.025));
                particle->setTotalParticles(20);
                particle->setAngle(90);
                particle->setAngleVar(10);
                particle->setLife(2);
                particle->setLifeVar(.5);
                particle->setStartSize(parent->getContentSize().width*.1);
                particle->setEndSize(parent->getContentSize().width*.2);
                
                if (strong) {
                    particle->setDuration(2);
                    particle->setSpeed(parent->getContentSize().height*.3);
                    particle->setTotalParticles(100);
                    particle->setAngleVar(25);

                }
                
                parent->addChild(particle);
            });
            
            return act;
        };
        
        actions.pushBack(smoke(false));
        actions.pushBack(DelayTime::create(.2));
        //pig
        actions.pushBack(createBounceAction(pig, .3, .1));
        
        actions.pushBack(DelayTime::create(1));
        
        if (index==1) {
            //smoke
            actions.pushBack(createSoundAction("bubble_1.mp3"));
            actions.pushBack(smoke(true));
            actions.pushBack(DelayTime::create(.2));
            
            actions.pushBack(Repeat::create(Spawn::create(createBounceAction(pig, .2, .15),
                                                          jumpAction(parent->getContentSize().height*.05, .225, pig, 1.5), NULL), 6));
        }
        
        actions.pushBack(DelayTime::create(2));
        //コールバック
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        if (index==1) {
            actions.pushBack(Spawn::create(addSkullSmoke(parent, 87),
                                           call, NULL));
        }
        else{
            actions.pushBack(call);
        }
        
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark ペンギンを置く
    else if(key.compare(0,10,"putpenguin")==0)
    {
        Common::playSE("p.mp3");
        auto index=atoi(key.substr(11,1).c_str());
        auto put=putAction(createSpriteToCenter(StringUtils::format("sup_36_penguin_%d_0.png",index), true, parent,true), "koto.mp3", 1, 1);
        actions.pushBack(put);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
}

#pragma mark - Item
void SweetHouseActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造 addする際はcreateSpriteOnClipを使用するとよい
    Vector<FiniteTimeAction*>actions;
#pragma mark 袋
    if (key=="knife") {
        Common::playSE("p.mp3");
        
        auto ribon=parent->itemImage->getChildByName("bag_ribon.png");
        ribon->setLocalZOrder(1);
        auto knife=createSpriteToCenter("anim_knife.png", true, parent->itemImage, true);
        knife->setPositionY(parent->itemImage->getContentSize().height);
        actions.pushBack(TargetedAction::create(knife, FadeIn::create(1)));
        
        //move
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(createSoundAction("sword.mp3"));
        actions.pushBack(TargetedAction::create(knife, EaseIn::create(MoveTo::create(.3, parent->itemImage->getContentSize()/2), 1.5)));
        actions.pushBack(TargetedAction::create(ribon, FadeOut::create(1)));
        actions.pushBack(DelayTime::create(.5));

        //change
        auto open=createSpriteToCenter("bag_open.png", true, parent->backImage,true);
        actions.pushBack(createChangeAction(1, .5, parent->itemImage, open));
        actions.pushBack(DelayTime::create(1));
        
        //add gun
        auto clip=createClippingNodeToCenter("bag_open_clip.png", open);
        auto gun=createSpriteToCenter("bag_gun.png", false, open);
        clip->addChild(gun);
        gun->setPositionY(-open->getContentSize().height*.2);
        
        actions.pushBack(createSoundAction("pop.mp3"));
        actions.pushBack(TargetedAction::create(gun, EaseInOut::create(MoveTo::create(1, open->getContentSize()/2), 1.5)));
        actions.pushBack(DelayTime::create(1));

        //call
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark Ending
    else if(key=="open_box")
    {
        auto open=createSpriteToCenter("device_open.png", true, parent->backImage,true);
        auto letter=createSpriteToCenter("letter.png", false, open,true);
        //change
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createChangeAction(1, .7, parent->itemImage, open,"po.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        //story
        //call
        auto call=CallFunc::create([letter,parent,this](){
            auto story=StoryLayer::create("ending", [letter,parent,this]{
                Vector<FiniteTimeAction*>actions;
                
                auto scene=(GameScene*)Director::getInstance()->getRunningScene()->getChildren().at(1);
                auto back_99=createSpriteToCenter("back_99.png", true, scene->mainSprite,true);
                auto white=Sprite::create();
                white->setTextureRect(Rect(0,0,back_99->getContentSize().width,back_99->getContentSize().height));
                white->setPosition(back_99->getPosition());
                white->setOpacity(0);
                scene->mainSprite->addChild(white);
                
                actions.pushBack(parent->fadeOutAll(1));
                //white out
                actions.pushBack(TargetedAction::create(white, FadeIn::create(3)));
                actions.pushBack(TargetedAction::create(back_99, FadeIn::create(2)));//delay
                actions.pushBack(TargetedAction::create(white, EaseIn::create(FadeOut::create(3), 1.5)));
                actions.pushBack(DelayTime::create(1));
                
                //起床
                auto call=CallFunc::create([this]{
                    auto story=StoryLayer::create("ending1", [this]{
                        Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
                    });
                    Director::getInstance()->getRunningScene()->addChild(story);
                });
                actions.pushBack(call);
                
                
                parent->runAction(Sequence::create(actions));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
}


#pragma mark - Custom
void SweetHouseActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    
}

#pragma mark - Show Answer
void SweetHouseActionManager::showAnswerAction(std::string key, cocos2d::Node *parent)
{
    if (key=="candy") {
        auto answerMap=DataManager::sharedManager()->getBackData(31)["useInput"].asValueMap()["answer"].asValueMap();
        auto answer=answerMap["answer"].asString();
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(.3));
        for (int i=0; i<answer.size(); i++) {
            actions.pushBack(candyPullAction(parent, atoi(answer.substr(i,1).c_str())));
            actions.pushBack(DelayTime::create(.2));
        }
        actions.pushBack(DelayTime::create(1));
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
}

#pragma mark - Story
void SweetHouseActionManager::storyAction(std::string key, cocos2d::Node *parent, const onFinished &callback)
{
    if (key=="playBGM") {
        EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

        callback(true);
    }
    else{
        callback(true);
    }
}

#pragma mark - Private
FiniteTimeAction* SweetHouseActionManager::moveHat(Node *parent, Node *hat, bool animate)
{
    return TargetedAction::create(hat, MoveBy::create(.5*animate, Vec2(0, parent->getContentSize().height*.25)));
}

FiniteTimeAction* SweetHouseActionManager::candyPullAction(Node *parent, int index)
{
    auto candy_0=parent->getChildByName(StringUtils::format("sup_31_candy_%d.png",index));
    auto name=StringUtils::format("sup_31_candy_%d_1.png",index);
    auto candy_1=parent->getChildByName(name);
    if (!candy_1) {
        candy_1=createSpriteToCenter(name, true, parent,true);
    }
    
    return Sequence::create(createSoundAction("kacha.mp3"),
                            createChangeAction(.2, .1, candy_0, candy_1),
                            DelayTime::create(.1),
                            createChangeAction(.2, .1, candy_1, candy_0),
                            TargetedAction::create(candy_1, FadeOut::create(0)), NULL);
}

FiniteTimeAction* SweetHouseActionManager::addSkullSmoke(Node *parent, int backID)
{
    std::vector<int>indexs={1,1,0,0,1,0,0};
    Vector<FiniteTimeAction*>actions;
    actions.pushBack(DelayTime::create(1));

    for (auto index : indexs) {
        auto addSmoke=CallFunc::create([parent,backID,index,this]{
            auto width=parent->getContentSize().width*.33;
            auto pos =Vec2(parent->getContentSize().width*.5, parent->getContentSize().height*.43);
            auto distance_x=parent->getContentSize().width*.1;
            auto distance_y=parent->getContentSize().height*.5;
            auto angle=10;
            if (backID==80) {
                width=parent->getContentSize().width*.08;
                pos =Vec2(parent->getContentSize().width*.26, parent->getContentSize().height*.4);
                distance_x=parent->getContentSize().width*.02;
                distance_y=parent->getContentSize().height*.15;
            }
            else{
                Common::playSE("bubble.mp3");
            }
            
            auto smoke=EscapeStageSprite::createWithSpriteFileName(StringUtils::format("smoke_skull_%d.png",index));
            smoke->setPosition(pos);
            smoke->setColor(Color3B(200, 200, 200));
            parent->addChild(smoke);
            auto scale=width/smoke->getContentSize().width;
            smoke->setScale(scale*.1);
            smoke->setOpacity(0);
            
            ccBezierConfig config;
            config.controlPoint_1=Vec2(distance_x*pow(-1, index), distance_y*.5);
            config.controlPoint_2=Vec2(-distance_x*pow(-1, index), distance_y*.5);
            config.endPosition=Vec2(0, distance_y);

            auto spawn=Spawn::create(ScaleTo::create(3, scale),
                                     Sequence::create(EaseInOut::create(RotateBy::create(1.5, angle), 1.5),
                                                      EaseInOut::create(RotateBy::create(2.5, -angle*2), 1.5),
                                                      NULL),
                                     Sequence::create(FadeTo::create(1.5, 200
                                                                     ),DelayTime::create(.5),FadeOut::create(2), NULL),
                                     BezierBy::create(4, config), NULL);
            auto seq=Sequence::create(spawn,RemoveSelf::create(), NULL);
            
            smoke->runAction(seq);
        });
        
        if (backID==87) {
            auto pig=parent->getChildByName("sup_87_pig_0.png");
            actions.pushBack(Spawn::create(addSmoke,
                                           createBounceAction(pig, .25, .05), NULL));
            actions.pushBack(DelayTime::create(2));
        }
        else{
            actions.pushBack(addSmoke);
            actions.pushBack(DelayTime::create(2.5));
        }
    }
    
    actions.pushBack(DelayTime::create(2));

    return Repeat::create(Sequence::create(actions), -1);
}

FiniteTimeAction* SweetHouseActionManager::giveGunAction(Node *parent,Node*gun, bool animate)
{
    transformToAnchorSprite(parent, gun, Vec2(.5, .115));
    auto distance=parent->getContentSize().height*.18;
    auto duration=0;
    if (animate) {
        duration=1.5;
    }
    return TargetedAction::create(gun, Sequence::create(FadeIn::create(duration),
                                                 Spawn::create(ScaleBy::create(duration, .5),
                                                               JumpBy::create(duration, Vec2(0, distance), parent->getContentSize().height*.3, 1)
                                                               , NULL),
                                                 NULL));
    
}

FiniteTimeAction* SweetHouseActionManager::addSmoke(Node *parent, int backID)
{
    Vector<FiniteTimeAction*> actions;
    std::vector<int>indexs={0,1,1,1,0,0,1};
    
    auto createSmoke=[parent,this,backID](int index)
    {
        auto smoke=EscapeStageSprite::createWithSpriteFileName("smoke_donut.png");
        //target scale
        auto scale=parent->getContentSize().width*.15/smoke->getContentSize().width;
        auto pos=Vec2(.33*parent->getContentSize().width,.77*parent->getContentSize().height);
        auto distance_x=parent->getContentSize().width*.05;
        auto distance_y=parent->getContentSize().height*.15;
        if (backID==2) {
            scale=parent->getContentSize().width*.05/smoke->getContentSize().width;
            pos=Vec2(.433*parent->getContentSize().width,.57*parent->getContentSize().height);
            distance_x=parent->getContentSize().width*.02;
            distance_y=parent->getContentSize().height*.075;
        }
        
        smoke->setOpacity(0);
        smoke->setScale(scale*.4,scale*.1);
        smoke->setColor(Color3B(100, 100, 100));
        smoke->setPosition(pos);
        parent->addChild(smoke);
        
        ccBezierConfig config;
        config.controlPoint_1=Vec2(0, parent->getContentSize().height*.1);
        config.controlPoint_2=Vec2(distance_x*pow(-1, index), distance_y);
        config.endPosition=Vec2(-distance_x*2*pow(-1, index), distance_y*1.5);
        auto spawn=Spawn::create(Sequence::create(FadeIn::create(1),
                                                  CallFunc::create([backID,this]{
            if (backID!=2) {
                this->createSoundAction("pop.mp3");
            }
        }),
                                                  DelayTime::create(.5),
                                                  FadeOut::create(.5),
                                                  NULL),
                                 EaseIn::create(ScaleTo::create(2, scale,scale*.7), 1.5),
                                 BezierBy::create(2, config), NULL);
        
        smoke->runAction(Sequence::create(spawn,
                                          DelayTime::create(1),
                                          RemoveSelf::create(),
                                          NULL));
    };
    
    actions.pushBack(DelayTime::create(1));
    
    for (auto i : indexs) {
        actions.pushBack(CallFunc::create([createSmoke,i]{
            createSmoke(i);
        }));
        actions.pushBack(DelayTime::create(1.5));
    }
    actions.pushBack(DelayTime::create(2));
    return Repeat::create(Sequence::create(actions), -1);
}
