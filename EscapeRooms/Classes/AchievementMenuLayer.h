//
//  SettingLayer.h
//  EscapeRooms
//
//  Created by yamaguchinarita on 2017/11/22.
//
//

#ifndef _____EscapeContainer________AchievementMenuLayer_____
#define _____EscapeContainer________AchievementMenuLayer_____

#include "cocos2d.h"
#include "Utils/CustomAlert.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;

class AchievementMenuLayer : public CustomAlert, TableViewDelegate, TableViewDataSource
{
public:
    static AchievementMenuLayer* create(const ccMenuCallback &callback);
    virtual bool init(const ccMenuCallback &callback);
       
private:

    TableView *m_tableView;
    Vec2 m_tableContentOffset;
    Size cellSize;
    int _selectedSegment;
    
    void createPremiumMenu();
    void createAchievementMenu();

    // TableViewDelegateがScrollViewDelegateを継承している事情で必要
    virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view) override{};
    virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) override{};
    
    /** セルのサイズを設定*/
    virtual Size cellSizeForTable(TableView* table) override;
    /** セルの中身を設定*/
    virtual TableViewCell* tableCellAtIndex(TableView* table,ssize_t idx) override;
    /** セルの数を設定*/
    virtual ssize_t numberOfCellsInTableView(TableView* table) override;
    
    /** セルが選択された時*/
    virtual void tableCellHighlight(TableView* table, TableViewCell* cell) override;
    /** セルが選択解除された時*/
    virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell) override;
    /** セルをタップしたときの処理*/
    virtual void tableCellTouched(TableView* table,TableViewCell* cell) override;
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
