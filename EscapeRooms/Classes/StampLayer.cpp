//
//  StampLayer.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/06/29.
//
//

#include "StampLayer.h"
#include "Utils/ContentsAlert.h"
#include "DataManager.h"
#include "Utils/Common.h"
#include "Utils/FireBaseBridge.h"
#include "AnalyticsManager.h"
#include "Utils/RewardMovieManager.h"
#include "Utils/InterstitialBridge.h"
#include "NotificationKeys.h"


using namespace cocos2d;

StampLayer* StampLayer::create(const onFinish &callback)
{
    return create(0, callback);
}

StampLayer* StampLayer::create(int addStampCount, const onFinish &callback)
{
    return create(addStampCount, callback, nullptr);
}

StampLayer* StampLayer::create(int addStampCount, const onFinish &callback, const onSaved &callback_onsaved)
{
    auto layer = new StampLayer();
    if (layer&&layer->init(addStampCount,callback,callback_onsaved)) {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_RELEASE(layer);
    
    return layer;
}

bool StampLayer::init(int addStampCount, const onFinish &callback, const onSaved &callback_onsaved)
{
    if (!LayerColor::initWithColor(Color4B(0, 0, 0, 0))) {
        return false;
    }
    
    m_callback=callback;
    m_callback_saved=callback_onsaved;
    
    addTouchListener();
    stamp_count=UserDefault::getInstance()->getIntegerForKey(UserKey_NowStampCount);
    stamp_count_today=UserDefault::getInstance()->getIntegerForKey(getKeyOfStampCountToday().c_str());
    
    //遅らせないと出ない
    runAction(Sequence::create(FadeTo::create(.3, 100),
                               CallFunc::create([this,addStampCount]{this->showAlert(addStampCount,false);}), NULL));
    return true;
}

#pragma mark - UI表示
void StampLayer::showAlert(int addStampCount, bool byMovie)
{
    auto stamp_card=createStampCard();
    auto title=DataManager::sharedManager()->getSystemMessage("bonus_title");
    auto rest=MaxGetStampCount-stamp_count_today;//残り獲得数
    //closebuttonを右上に移動し、スタンプ取得ボタンを目立たせる
    auto closeHighLight=true;//(AnalyticsManager::getInstance()->getAnalyticsStampType()!=AnalyticsStampType_Normal);
    if (byMovie) {
        rest-=1;
    }
    
    auto message=StringUtils::format(DataManager::sharedManager()->getSystemMessage("stamp_message").c_str(),MaxGetStampCount,rest);
    std::vector<std::string> buttons;
    
    if(closeHighLight){
        if (rest==0) {
            buttons={"Close"};
        }
        else{
            buttons={DataManager::sharedManager()->getSystemMessage("getStamp")};
        }
    }
    else{
        buttons={"Close",DataManager::sharedManager()->getSystemMessage("login_onemore")};
        if (rest==0) {
            buttons.erase(++buttons.begin());
        }
        else if (rest==MaxGetStampCount){
            buttons.erase(++buttons.begin());
            buttons.push_back(DataManager::sharedManager()->getSystemMessage("view_video_button"));
        }
    }
    
    
    auto alert=ContentsAlert::create(stamp_card, title, message, buttons, [this,stamp_card,closeHighLight,rest](Ref*ref){
        auto alert=(ContentsAlert*)ref;
        if (closeHighLight) {
            if (rest==0) {
                this->close();
            }
            else{
                //動画再生
                this->checkConditionOfReward();
            }
        }
        else{
            if (alert->getSelectedButtonNum()==0) {
                this->close();
            }
            else{
                //動画再生
                this->checkConditionOfReward();
            }
        }
    });
    
    //右上にボタンをつける
    if (closeHighLight) {
        alert->displayCloseButton([this]{
            this->close();
        });
    }
    
    //表示終了までボタン不可
    alert->setDisableButton((addStampCount>0));
    //表示後のコールバックをとる
    alert->showAlert([this,addStampCount,byMovie,stamp_card,alert](Ref*sender){
        //スタンプアニメ.終了でボタン操作可能になる
        if (addStampCount>0) {
            pushStamp(stamp_card,addStampCount,byMovie,[alert]{
                alert->setDisableButton(false);
            });
        }
    });
}

Sprite* StampLayer::createStampCard()
{
    auto sprite=Sprite::createWithSpriteFrameName("stampcard_login.png");
    sprite->setCascadeOpacityEnabled(true);
    //押されているスタンプを配置
    for (int i=0; i<stamp_count; i++) {
        addStamp(sprite,i);
    }
    
    //ボーナスを配置
    addBonusSprite(sprite,stamp_count,false);
    
    return sprite;
}

Sprite* StampLayer::addStamp(cocos2d::Node *parent, int index)
{
    auto x=xs.at(index%5);
    auto y=ys.at(index/5);
    
    auto stamp=Sprite::createWithSpriteFrameName("pig_face.png");
    stamp->setScale(parent->getContentSize().width*stamp_width/stamp->getContentSize().width);
    stamp->setPosition(parent->getContentSize().width*x,parent->getContentSize().height*y);
    parent->addChild(stamp);
    
    return stamp;
}

void StampLayer::addBonusSprite(cocos2d::Node *parent, int stamp_count_now, bool animate)
{
    auto counter=0;
    for (auto index : bonusIndexs) {
        if (index>stamp_count_now-1) {
            //配置して良い
            auto coin=Sprite::createWithSpriteFrameName("coin_image.png");
            coin->setName(StringUtils::format("coin_%d",index));
            
            auto x=xs.at(index%5);
            auto y=ys.at(index/5);
            coin->setScale(parent->getContentSize().width*stamp_width/coin->getContentSize().width/parent->getScale());
            coin->setPosition(parent->getContentSize().width*x,parent->getContentSize().height*y);
            coin->setCascadeOpacityEnabled(true);
            parent->addChild(coin);
            
            auto label=Label::createWithTTF(StringUtils::format("×%d",bonusCounts.at(counter)),Common::getUsableFontPath(MyFont) , parent->getContentSize().width*.185);
            label->setTextColor(Color4B::WHITE);
            label->enableOutline(Color4B::BLACK,4);
            label->setPosition(coin->getContentSize().width/2, coin->getContentSize().height*.1);
            coin->addChild(label);
            
            if (animate) {
                coin->setOpacity(0);
                coin->runAction(FadeIn::create(.2));
            }
        }
        
        counter++;
    }
}

#pragma mark - スタンプ押すアニメ
void StampLayer::pushStamp(cocos2d::Node *parent, int addStampCount, bool byMovie, const onFinish &callback)
{
    Vector<FiniteTimeAction*>actions;
    auto max_index=xs.size()*ys.size()-1;
    auto add_coin=0;

    //スタンプを押すアニメーションを格納する
    for (int i=0; i<addStampCount; i++) {
        auto index=(stamp_count+i)%(max_index+1);
        
        auto stamp=addStamp(parent,(int)index);
        stamp->setOpacity(0);
        
        auto original_scale=stamp->getScale();
        stamp->setScale(original_scale*2);
        
        auto push=TargetedAction::create(stamp, Sequence::create(Spawn::create(FadeIn::create(.5),
                                                                               EaseIn::create(ScaleTo::create(.5, original_scale), 2), NULL),
                                                                 CallFunc::create([]{Common::playSE("koto.mp3");}),
                                                                 NULL));
        
        //ボーナス取得判定
        auto bonus_index=-1;
        for(int j=0;j<bonusIndexs.size();j++)
        {//ボーナス取得のあるindexを検索
            auto _bonus_index=bonusIndexs.at(j);
            if (index==_bonus_index) {
                add_coin+=bonusCounts.at(j);
                bonus_index=_bonus_index;
                break;
            }
        }
        
        if (bonus_index>=0)
        {//coinSpriteの削除も一緒に行う
            auto coinSpr=parent->getChildByName(StringUtils::format("coin_%d",bonus_index));
            actions.pushBack(Spawn::create(push,
                                           CallFunc::create([]{Common::playSE("pop.mp3");}),
                                           TargetedAction::create(coinSpr, Sequence::create(FadeOut::create(.5),
                                                                                            
                                                                                            RemoveSelf::create(),
                                                                                            NULL)),
                                           NULL));
            
        }else{
            actions.pushBack(push);
        }
        
        //スタンプカードのrefresh
        if (index==max_index) {
            Vector<FiniteTimeAction*>spawns_refresh;
            for (auto child : parent->getChildren()) {
                spawns_refresh.pushBack(TargetedAction::create(child, Sequence::create(FadeOut::create(.2),
                                                                                       RemoveSelf::create(),
                                                                                       CallFunc::create([this,parent]{
                    this->addBonusSprite(parent, 0, true);
                }), NULL)));
            }
            
            actions.pushBack(Spawn::create(spawns_refresh));
        }
    }
    
    //終了処理
    if (add_coin>0)
    {//コイン追加後はアラートを表示する
        actions.pushBack(DelayTime::create(.5));
        
        //get alert
        auto call_alert=CallFunc::create([this,callback,add_coin,byMovie,addStampCount,parent]{
            Common::playSE("p.mp3");
            
            auto alert=ContentsAlert::create(StringUtils::format("get_coin_%d.png",RandomHelper::random_int(0, 1)), "", StringUtils::format(DataManager::sharedManager()->getSystemMessage("getCoinWithNum").c_str(),add_coin), {"OK"}, [callback](Ref*ref){
                callback();
            });
            alert->showAlert();
            
            //セーブ,データ処理
            this->saveStampCounts(add_coin,addStampCount,byMovie);
        });
        actions.pushBack(call_alert);
    }
    else{
        auto call=CallFunc::create([callback,add_coin,addStampCount,byMovie,this]{
            //セーブ,データ処理
            this->saveStampCounts(add_coin,addStampCount,byMovie);
            callback();
        });
        actions.pushBack(call);
    }
    
    runAction(Sequence::create(actions));
}

std::string StampLayer::getKeyOfStampCountToday()
{
    auto todayServerDate = FireBaseBridge::getInstance()->getServerTimestamp();
    auto key_today=StringUtils::format(UserKey_GetStamp,Common::transformTimeToDate(todayServerDate));
    
    return key_today;
}

void StampLayer::saveStampCounts(int getBonusCount, int addStampCount, bool byMovie)
{
    NativeBridge::vibration3D();
    
    DataManager::sharedManager()->addCoin(getBonusCount);
    
    stamp_count=(stamp_count+addStampCount)%(xs.size()*ys.size());
    UserDefault::getInstance()->setIntegerForKey(UserKey_NowStampCount, stamp_count);
    //log("%s : %d",__func__,stamp_count);
    
    if (byMovie)
    {//動画再生によるボーナス
        //analytics送信
        if (stamp_count_today==0) {
            AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_GetStamp_0);
        }
        else if (stamp_count_today==1) {
            AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_GetStamp_1);
        }
        else {
            AnalyticsManager::getInstance()->sendAnalytics(AnalyticsKeyType_GetStamp_2);
        }
        
        DataManager::sharedManager()->saveDate(UserKey_LoginBonusDate);
        auto totalStamp=UserDefault::getInstance()->getIntegerForKey(UserKey_TotalStampByMovie);
        UserDefault::getInstance()->setIntegerForKey(UserKey_TotalStampByMovie, totalStamp+1);
        stamp_count_today++;
        UserDefault::getInstance()->setIntegerForKey(getKeyOfStampCountToday().c_str(), stamp_count_today);
        
        //badge
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyAchievementBadge);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKeyStampBadge);
    }
    
    //save処理後のcallback
    if (m_callback_saved) {
        m_callback_saved(byMovie);
    }
}

#pragma mark - 動画
void StampLayer::checkConditionOfReward()
{
    Common::performProcessForDebug([this]
    {//DebugMode==2
        this->showAlert(1, true);
    }, [this]
    {//else
        if (!RewardMovieManager::sharedManager()->isPrepared()) {//動画の読み込みができていない。
            std::string message;
            std::string alertFileName="";
            if (movie_error_count>=2) {
                message=DataManager::sharedManager()->getSystemMessage("no_video_storage");
                alertFileName="shortage.png";
            }
            else{
                message=DataManager::sharedManager()->getSystemMessage("no_video");
                alertFileName="shortage.png";
            }
            
            auto confirmAlert = ContentsAlert::create(alertFileName,"Infomation", message, {"Close","Reload"}, [this](Ref*ref){
                auto _ref=(CustomAlert*)ref;
                if (_ref->getSelectedButtonNum()==0) {
                    this->close();
                }
                else{
                    movie_error_count++;
                    if (movie_error_count>=3)
                    {//かわりにインステを表示
                        if (InterstitialBridge::isReady(INTERSTITIAL_LAUNCH_APPID)) {
                            movie_error_count=0;
                            InterstitialBridge::showIS(INTERSTITIAL_LAUNCH_APPID,[this](){
                                
                                this->showAlert(1,true);
                            });
                        }
                        else{
                            //動画もロード失敗
                            this->checkConditionOfReward();
                        }
                    }
                    else{
                        this->checkConditionOfReward();
                    }
                }
            });
            confirmAlert->showAlert(this);
        }
        else {//動画の再生準備が整っている
            movie_error_count=0;

            RewardMovieManager::sharedManager()->play([this](bool complete){
                if (complete) {
                    this->showAlert(1,true);
                }
                else{
                    log("再生に失敗した");
                    close();
                }
            });
        }
    });
}

#pragma mark 閉じる
void StampLayer::close()
{
    runAction(Sequence::create(FadeOut::create(.3),
                               CallFunc::create([this]{
        if (m_callback) {
            m_callback();
        }
    }),RemoveSelf::create(), NULL));
}

#pragma mark- タッチ
void StampLayer::addTouchListener()
{
    auto listener=EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);//タッチ透過しない
    
    listener->onTouchBegan=[this](Touch*touch, Event*event)
    {
        return true;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}
