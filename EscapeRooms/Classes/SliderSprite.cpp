//
//  SliderSprite.cpp
//  EntranceCeremony
//
//  Created by 成田凌平 on 2017/03/31.
//
//

#include "SliderSprite.h"
#include "Utils/Common.h"
#include "DataManager.h"

using namespace cocos2d;

SliderSprite* SliderSprite::create(ValueMap map, cocos2d::Value answerMapV, std::string name, int number,Size parentSize,std::string temporaryFlagkey)
{
    return create(map, answerMapV, name, number,parentSize, temporaryFlagkey,false);
}

SliderSprite* SliderSprite::create(ValueMap map, cocos2d::Value answerMapV, std::string name, int number,Size parentSize,std::string temporaryFlagkey,bool onPhoto)
{
    auto node=new SliderSprite();
    if (node&&node->init(map,answerMapV,name,number,parentSize,temporaryFlagkey,onPhoto)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    
    return node;
}

bool SliderSprite::init(ValueMap map, Value answerMapV, std::string name, int number, Size parentSize, std::string temporaryFlagkey, bool onPhoto)
{
    auto filename=StringUtils::format("%s_%d.png",name.c_str(),number);
    if (!EscapeStageSprite::init(filename)) {
        return false;
    }
    
    setScale(parentSize.width/getContentSize().width);
    setDuration(.3);
    setNumber(number);//何番目のspriteか
    setFileName(name);
    setIsReverse(map["reverse"].asBool());
    setSlideDirection(SlideDirection_Accend);
    //移動ポジション配列をセット
    addPositions(map["positions"].asValueVector());
    
    //連動させるフラグがある時
    auto flagKey=map["temporaryFlagKey"].asString();
    if (flagKey.size()==0&&temporaryFlagkey.size()>0) {
        flagKey=StringUtils::format("%s_%d",temporaryFlagkey.c_str(),number);
    }

    setCustomFlagKey(flagKey);
    
    //デフォルトポジションをセット(save dataも読む)
    setDefaultPosition(answerMapV,onPhoto);
    
    //
    setLocalZOrder(map["zOrder"].asInt());
    
    //supplementチェック
    if (!map["supplements"].isNull()) {
        auto supMap=map["supplements"].asValueMap();
        for (auto v:supMap) {
            auto key=v.first;//キーとなるフラグ firstでしゅとくできる
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(atoi(key.c_str()))) {
                auto fileNames=v.second.asValueVector();
                for (auto name:fileNames) {
                    auto spr = EscapeStageSprite::createWithSpriteFileName(name.asString());
                    spr->setPosition(getContentSize()/2);
                    addChild(spr);
                }
            }
        }
    }
    
    return true;
}

void SliderSprite::addPositions(ValueVector vector)
{
    for (auto v : vector) {
        auto posMap=v.asValueMap();
        auto vec2=Vec2(getBoundingBox().size.width*posMap["x"].asFloat(), getBoundingBox().size.height*posMap["y"].asFloat());
        
        positions.push_back(vec2);
    }
}

void SliderSprite::setDefaultPosition(Value answer, bool onPhoto)
{//
    auto key=getTemporaryKey(getFileName(),onPhoto);
    auto index=0;
    if (!DataManager::sharedManager()->getTemporaryFlag(key).isNull()) {
        index=DataManager::sharedManager()->getTemporaryFlag(key).asInt();
    }
    
    //フラグによる固定チェック
    if (!answer.isNull()) {
        auto answerMap=answer.asValueMap();
        if (answerMap["secure"].asBool()) {
            if (DataManager::sharedManager()->getEnableFlagWithFlagID(answerMap["enFlagID"].asFloat())) {
                auto answer=answerMap["answer"].asString();
                index=atoi(answer.substr(getNumber(),1).c_str());
            }
        }
    }
    setPosIndex(index);
    setPosition(positions[index]);
}

#pragma mark キー
std::string SliderSprite::getTemporaryKey(std::string name)
{
    return getTemporaryKey(name,false);
}

std::string SliderSprite::getTemporaryKey(std::string name,bool onPhoto)
{
    std::string key;
    if(getCustomFlagKey().size()>0)
    {//指定のキーがある
        key=getCustomFlagKey();
    }
    else{
        key=StringUtils::format("slide_%s_%d",name.c_str(),getNumber());
    }
    
    if (onPhoto) {
        return key.append("photo");
    }
    
    return key;
}

#pragma mark save
void SliderSprite::save()
{
    save(false);
}

void SliderSprite::save(bool onPhoto)
{
    auto key=getTemporaryKey(getFileName(),onPhoto);
    save(key);
}

void SliderSprite::save(std::string key)
{
    DataManager::sharedManager()->setTemporaryFlag(key, getPosIndex());//posを記憶
}


#pragma mark- Public
void SliderSprite::slide(ccMenuCallback callback)
{
    if (getIsReverse()) {
        if (getPosIndex()==positions.size()-1&&
            getSlideDirection()==SlideDirection_Accend) {
            setSlideDirection(SlideDirection_Decend);
        }
        else if(getPosIndex()==0&&
                getSlideDirection()==SlideDirection_Decend){
            setSlideDirection(SlideDirection_Accend);
        }
    }
    
    auto value=(getSlideDirection()==SlideDirection_Accend)?1:-1;
    int nextIndex=(getPosIndex()+value)%positions.size();

    
    setPosIndex(nextIndex);
    auto newPos=positions[getPosIndex()];
    
    //sound
    if (getPlaySE().size()>0) {
        Common::playSE(getPlaySE().c_str());
    }
    
    //animation
    auto move=MoveTo::create(getDuration(), newPos);
    auto call=CallFunc::create([this,callback](){
        this->save(false);
        callback(NULL);
    });
    
    runAction(Sequence::create(move,call, NULL));
}
