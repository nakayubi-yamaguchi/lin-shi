//
//  VoteLayer.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2019/02/13.
//
//

#include "VoteLayer.h"
#include "Utils/Common.h"
#include "DataManager.h"
#include "UIParts/MenuItemScale.h"
#include "Utils/FireBaseBridge.h"
#include "Utils/RewardMovieManager.h"
#include "ShopMenuLayer.h"
#include "UIParts/Spinner.h"
#include "Utils/ValueHelper.h"
#include "Utils/FireBaseBridge.h"
#include "Utils/UtilsMethods.h"
#include "UserDataManager.h"
#include "UIParts/NCIndicator.h"
#include "CustomActionManager.h"


using namespace cocos2d;
using namespace utils;

/**1票あたりのコイン使用量*/
#define CoinPerVote 2
#define Interim "Interim"

#define PercentLabelName "percent_"
#define TotalVoteLabelName "totalVoteLabelName"
#define IndicatorName "indicatorName"
#define NotificationKey_LoadedInterim "NotificationKey_LoadedInterim"

bool VoteLayer::init() {
    if (!ContentsAlert::init(NULL, "", "", {}, nullptr)) {
        return false;
    }
    
    m_selectedCandidate = 0;
    
    auto voteVec = EscapeDataManager::getInstance()->getVoteDataVector();
    m_voteInfo = (voteVec.size()>0)? voteVec.at(0).asValueMap() : ValueMap();
    m_key_name = m_voteInfo[MapKey_KeyName].asString();
    m_isVotedToday = UserDataManager::getInstance()->isVotedToday(m_key_name.c_str());
    m_voteTerm = EscapeDataManager::getInstance()->getVoteTerm(m_voteInfo);
    
    if (m_voteTerm == VoteTerm_Result) {
        titleLabel->setString("！投票結果！");
        titleLabel->setTextColor(Color4B::WHITE);
        for (int i = 0; i < titleLabel->getStringLength(); i++) {
            auto letter = titleLabel->getLetter(i);
            
            if (i == 0 || i == titleLabel->getStringLength()-1) {
                letter->setColor(Color3B::RED);
            }
            else {
                letter->setColor(Common::getColorFromHex(LightBlackColor));
            }
        }
    }
    else {
        titleLabel->setString("投票受付中");
    }
    
    
    loadResultValue();

    createContents();
    
    showResult();
    
    displayCloseButton();
    
    return true;
}

#pragma mark - UI
void VoteLayer::createContents()
{
    //背景の白
    float backWidth = m_contentsNode->getContentSize().width;
    auto sprite = Sprite::create();
    sprite->setTextureRect(Rect(0, 0, backWidth, backWidth*.85));
    sprite->setCascadeOpacityEnabled(true);
    
    float candidateWidth = sprite->getContentSize().width/2.5;
    float candidateSpace = candidateWidth*.05;
    Size candidateSize = Size(candidateWidth, candidateWidth);
    float otherContentsHeight = (sprite->getContentSize().height-candidateSize.height)/2;//候補者ボタン以外のそれぞれの高さ。上部分と下部分
    
#pragma mark 上部：テーマラベル作成
    auto upperContentsSprite = Sprite::create();
    upperContentsSprite->setPosition(sprite->getContentSize()/2);
    upperContentsSprite->setCascadeOpacityEnabled(true);
    sprite->addChild(upperContentsSprite);
    
    //ラベル
    auto themeSt = EscapeDataManager::getInstance()->getLocalizeStringForVoteWithKey(m_voteInfo, MapKey_Theme);
    auto themeLabel = Label::createWithTTF(themeSt, Common::getUsableFontPath(HiraginoMaruFont), getContentSize().width*.05);
    themeLabel->setTextColor(Color4B(Common::getColorFromHex(LightBlackColor)));
    upperContentsSprite->addChild(themeLabel);

#pragma mark 中部：候補者ボタン作成
    auto middleContentsSprite = Sprite::create();
    middleContentsSprite->setPosition(sprite->getContentSize()/2);
    middleContentsSprite->setCascadeOpacityEnabled(true);
    sprite->addChild(middleContentsSprite);
    
    auto candidateVector = m_voteInfo[MapKey_Candidates].asValueVector();
    int i = 0;
    for (auto candidateValue : candidateVector) {
        auto candidateMap = candidateValue.asValueMap();
        auto imageColorCode = candidateMap[MapKey_ImageColorCode].asString();
        auto candidateFileName = candidateMap[MapKey_FileName].asString();
        auto commitment = EscapeDataManager::getInstance()->getLocalizeStringForVoteWithKey(candidateMap, MapKey_Simplicity);
        auto detail = EscapeDataManager::getInstance()->getLocalizeStringForVoteWithKey(candidateMap, MapKey_Detail);
        Color3B imageColor = Common::getColorFromHex(DataManager::sharedManager()->getColorCodeData(imageColorCode));
        
        auto backSp = Sprite::create();
        backSp->setTextureRect(Rect(0, 0, candidateSize.width, candidateSize.height));
        backSp->setCascadeOpacityEnabled(true);
        backSp->setColor(imageColor);
        
        auto white = Sprite::create();
        white->setTextureRect(Rect(0, 0, backSp->getContentSize().width-candidateSpace, backSp->getContentSize().height-candidateSpace));
        white->setPosition(backSp->getContentSize()/2);
        white->setColor(Common::getColorFromHex(WhiteColor2));
        white->setCascadeOpacityEnabled(true);

        backSp->addChild(white);
        
        //ロゴ表示してみる。
        auto stencil = Sprite::create();
        stencil->setTextureRect(Rect(0, 0, white->getContentSize().width, white->getContentSize().height));
        stencil->setPosition(white->getContentSize()/2);
        stencil->setColor(Color3B::BLACK);
        
        auto clip = ClippingNode::create(stencil);
        clip->setInverted(false);
        clip->setCascadeOpacityEnabled(true);
        clip->setAlphaThreshold(0.0f);
        
        auto logo = Sprite::createWithSpriteFrameName("alert_back_logo.png");
        logo->setScale(stencil->getContentSize().height/logo->getContentSize().height);
        logo->setPosition(stencil->getContentSize()/2);
        logo->setOpacity(80);
        clip->addChild(logo);
        white->addChild(clip);
        
        
        //キャラ画像
        auto candidateSp = Sprite::createWithSpriteFrameName(StringUtils::format("%s.png", candidateFileName.c_str()));
        candidateSp->setScale(white->getContentSize().width/candidateSp->getContentSize().width);
        candidateSp->setPosition(backSp->getContentSize()/2);
        candidateSp->setCascadeOpacityEnabled(true);
        backSp->addChild(candidateSp);
        
        //公約ラベル
        auto commitmentLabel = Label::createWithTTF(commitment, Common::getUsableFontPath(HiraginoMaruFont), candidateSp->getTextureRect().size.width/8);
        commitmentLabel->enableBold();
        commitmentLabel->setPosition(candidateSp->getContentSize().width/2, candidateSp->getContentSize().height*.8);
        commitmentLabel->setTextColor(Color4B(Common::getColorFromHex(WhiteColor2)));
        commitmentLabel->enableOutline(Color4B(Common::getColorFromHex(LightBlackColor)), 2);
   

        candidateSp->addChild(commitmentLabel);
        
        auto candidateMenuItem = MenuItemScale::create(backSp, backSp, [this](Ref* sender){
            Common::playClick();
            
            if (m_voteTerm == VoteTerm_Result) {
                auto denyVoteAlert = CustomAlert::create(AlertTitle_INFORMATION, "投票は既に締め切りしました。またのご投票をお待ちしております。", {"OK"}, nullptr);
                denyVoteAlert->showAlert(this);
                return;
            }
            
            
            auto menuItem = (MenuItemScale*)sender;
            int selectedNum = menuItem->getTag();
            
            m_selectedCandidate = selectedNum;
         
            this->showSpeachAlert();
        });
        candidateMenuItem->setTag(i);
        candidateMenuItem->setPosition(sprite->getContentSize().width*(.25*pow(-1, (i+1))), 0);

        candidateMenuItem->setTappedScale(.95);
        m_candidateMenuItems.pushBack(candidateMenuItem);
        
        i++;
    }
    
    auto candidateMenu = Menu::createWithArray(m_candidateMenuItems);
    candidateMenu->setPosition(Vec2::ZERO);
    middleContentsSprite->addChild(candidateMenu);
    
#pragma mark 下部：ラベルorプログレスバー
    //バー or 投票アシスト
    VoteTerm voteTerm = EscapeDataManager::getInstance()->getVoteTerm(m_voteInfo);
    auto underContentsSprite = Sprite::create();
    underContentsSprite->setCascadeOpacityEnabled(true);
    underContentsSprite->setPosition(sprite->getContentSize().width/2, otherContentsHeight/2);
    sprite->addChild(underContentsSprite);
    
    if (voteTerm == VoteTerm_NONE) {
        std::string voteText;
        if (m_isVotedToday) {
            voteText = "本日分は既に投票済みです。";
        }
        else {
            voteText = "候補者をタップして投票しよう！";
        }
        
        voteText += StringUtils::format("\n中間発表:%s 結果発表:%s",EscapeDataManager::getInstance()->getDateStringForVote(m_voteInfo, MapKey_InterimDate).c_str(), EscapeDataManager::getInstance()->getDateStringForVote(m_voteInfo, MapKey_DeadlineDate).c_str());
        auto voteLabel = Label::createWithTTF(voteText, Common::getUsableFontPath(HiraginoMaruFont), getContentSize().width*.04);
        voteLabel->setTextColor(Color4B(Common::getColorFromHex(LightBlackColor)));
        voteLabel->setPosition(underContentsSprite->getContentSize()/2);
        underContentsSprite->addChild(voteLabel);
    }
    else {
        auto barWidth = backWidth;
        auto barheight = titleLabel->getContentSize().height*.75;
        auto spr=Sprite::create();
        spr->setTextureRect(Rect(0, 0, barWidth, barheight));
        spr->setColor(Common::getColorFromHex(DataManager::sharedManager()->getColorCodeData(candidateVector.at(0).asValueMap()[MapKey_ImageColorCode].asString())));
        
        auto spr_back=Sprite::create();
        spr_back->setTextureRect(Rect(0, 0, barWidth, barheight));
        spr_back->setColor(Common::getColorFromHex(DataManager::sharedManager()->getColorCodeData(candidateVector.at(1).asValueMap()[MapKey_ImageColorCode].asString())));
        
        log("はいけいのカラーは%s",candidateVector.at(1).asValueMap()[MapKey_ImageColorCode].asString().c_str());
        
        m_gaugeSprite = GaugeSprite::create(spr, spr_back);
        //m_gaugeSprite->setPosition(sprite->getContentSize().width/2, otherContentsHeight/2);
        m_gaugeSprite->setPercentage(50);//表示直後は中間にいる
        
        //インジケーターを回す。データがあればすぐに消える
        NCIndicator *indicator = NCIndicator::create();
        indicator->setScale(m_gaugeSprite->getContentSize().height*.8/indicator->getContentSize().height);
        indicator->setPosition(m_gaugeSprite->getContentSize()/2);
        indicator->setName(IndicatorName);
        m_gaugeSprite->addChild(indicator);
        
        //パーセントラベル
        /*
        ValueMap resultMap;
        if (!m_VoteResultValue.isNull()) {
            resultMap = m_VoteResultValue.asValueMap();
        }
        
        int firstVotes = resultMap["first"].asInt();
        int secondVotes = resultMap["second"].asInt();
        std::vector<int> votesVector = {firstVotes, secondVotes};
        int totalVoteCount = firstVotes + secondVotes;*/
        
        auto fontSize = barheight*.7;
        
        Vector<Label*> percentLabels;
        for (int i = 0; i < 2; i++) {
            
            /*float percent = roundf((float)votesVector.at(i)/(float)totalVoteCount*100);

            if (std::isnan(percent)) {
                percent = 0;
            }*/
            auto powValue = pow(-1, (i+1));

            auto percentLabel = Label::createWithTTF(""/*StringUtils::format("%s%%", NativeBridge::transformThreeComma(percent).c_str())*/, LocalizeFont, fontSize);
            percentLabel->setName(StringUtils::format("%s%d", PercentLabelName, i));
            percentLabel->setWidth(sprite->getContentSize().width*.4);
            
            //percentLabel->setPosition(Vec2(m_gaugeSprite->getContentSize().width/2+powValue*m_gaugeSprite->getContentSize().width/2-powValue*(percentLabel->getWidth()/2), -percentLabel->getContentSize().height/2));
            
            percentLabel->setPosition(Vec2(m_gaugeSprite->getContentSize().width/2/*+powValue*m_gaugeSprite->getContentSize().width/2*/-powValue*(percentLabel->getWidth()/2), -percentLabel->getContentSize().height/2));
            
            percentLabel->setTextColor(Color4B(Common::getColorFromHex(WhiteColor2)));
            percentLabel->setAlignment((i == 0)? TextHAlignment::LEFT : TextHAlignment::RIGHT, TextVAlignment::CENTER);
            percentLabel->setTextColor(Color4B(Common::getColorFromHex(DataManager::sharedManager()->getColorCodeData(candidateVector.at(i).asValueMap()[MapKey_ImageColorCode].asString()))));
            
            m_gaugeSprite->addChild(percentLabel);
            
            percentLabels.pushBack(percentLabel);
            
            /*
            if (i == 0 && !m_VoteResultValue.isNull()) {//ゲージをアニメーションして動かす
                m_gaugeSprite->setPercentage(percent, true, .5);
            }*/
        }
        underContentsSprite->addChild(m_gaugeSprite);
        
        //総得票数ラベル
        if (m_voteTerm == VoteTerm_Interim) {
            auto voteCountText = StringUtils::format("投票締め切り:%s", EscapeDataManager::getInstance()->getDateStringForVote(m_voteInfo, MapKey_DeadlineDate).c_str());
            
            auto totalVoteLabel = Label::createWithTTF(voteCountText, Common::getUsableFontPath(HiraginoMaruFont), fontSize*.9);
            totalVoteLabel->setTextColor(Color4B(Common::getColorFromHex(LightBlackColor)));
            totalVoteLabel->setName(TotalVoteLabelName);
            totalVoteLabel->setPosition(sprite->getContentSize().width/2, m_gaugeSprite->getContentSize().height+totalVoteLabel->getContentSize().height/2*1.2);
            m_gaugeSprite->addChild(totalVoteLabel);
        }
        
        auto setStrings = [this, indicator, percentLabels](EventCustom*event)
        {
            log("つうちが飛んできたでよ");
            ValueMap resultMap = m_VoteResultValue.asValueMap();
            int firstVotes = resultMap["first"].asInt();
            int secondVotes = resultMap["second"].asInt();
            std::vector<int> votesVector = {firstVotes, secondVotes};
            int totalVoteCount = firstVotes + secondVotes;
            
            float percent = roundf(firstVotes/(float)totalVoteCount*100);
            if (std::isnan(percent)) {
                percent = 0;
            }
            m_gaugeSprite->setPercentage(percent, true, .5);
            
            //ラベルの更新
            int i = 0;
            for (auto percentLabel : percentLabels) {
                percentLabel->setString(StringUtils::format("%s票", NativeBridge::transformThreeComma(votesVector.at(i)).c_str()));
                
                auto powValue = pow(-1, (i+1));
                percentLabel->setPosition(Vec2(m_gaugeSprite->getContentSize().width/2+powValue*m_gaugeSprite->getContentSize().width/2-powValue*(percentLabel->getContentSize().width/2*1.2), -percentLabel->getContentSize().height/2));
               
                i++;
            }
            
            //インジケーターを消す。
            indicator->setOpacity(0);
            indicator->removeFromParent();
        };
        
        if (!m_VoteResultValue.isNull()) {//結果データがあるなら。
            setStrings(NULL);
        }
#pragma mark 通知
        if (m_voteTerm == VoteTerm_Interim && m_VoteResultValue.isNull()) {

            auto InterimListner=EventListenerCustom::create(NotificationKey_LoadedInterim, setStrings);
            Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(InterimListner, this);
        }
    }
   
    setSubContentsSprite(sprite, Size::ZERO);
    
#pragma mark 整列
    Size upperSize = getCascadeBoundingBox(upperContentsSprite).size;
    Size middleSize = candidateSize;//レイヤーを含んでいると正しく値が取れない。//getCascadeBoundingBox(middleContentsSprite).size;
    Size underSize = getCascadeBoundingBox(underContentsSprite).size;
    
    float contentsSpace = (sprite->getContentSize().height-upperSize.height-middleSize.height-underSize.height)/4;
    
    underContentsSprite->setPositionY(contentsSpace+underSize.height/2);
    middleContentsSprite->setPositionY(sprite->getContentSize().height-contentsSpace*2-themeLabel->getContentSize().height-middleSize.height/2);
    upperContentsSprite->setPositionY(sprite->getContentSize().height-contentsSpace-themeLabel->getContentSize().height/2);
}

void VoteLayer::showResult()
{
    //結果発表以外 || 結果のデータがないならメニューアイテムを操作しなくて良い。
    if (m_voteTerm != VoteTerm_Result || m_VoteResultValue.isNull()) return;
  
    auto resultMap = m_VoteResultValue.asValueMap();
    int firstVotes = resultMap["first"].asInt();
    int secondVotes = resultMap["second"].asInt();
    
    if (firstVotes == secondVotes) return;//引き分けなら
    
    int winIndex = firstVotes < secondVotes;
    
    int i = 0;
    
    auto candidateVector = m_voteInfo[MapKey_Candidates].asValueVector();
    for (auto menuItem : m_candidateMenuItems) {
        auto candidateMap = candidateVector.at(i).asValueMap();
        
        auto candidateMenuItem = (MenuItemScale*)menuItem;
        float scale = 0;
        if (i == winIndex) {//勝者の加工
            //紙吹雪
            auto stencil = Sprite::create();
            stencil->setTextureRect(Rect(0, 0, candidateMenuItem->getContentSize().width, candidateMenuItem->getContentSize().height));
            stencil->setPosition(candidateMenuItem->getContentSize()/2);
            stencil->setColor(Color3B::BLACK);
            
            auto clip = ClippingNode::create(stencil);
            clip->setInverted(false);
            clip->setAlphaThreshold(0.0f);
            log("クリップのサイズは{%f,%f}",clip->getContentSize().width, clip->getContentSize().height);
         
            //ほんとはどっか一箇所にまとめたい。パーティクル
            std::vector<Color4F>colors;
            colors.push_back(Color4F(Common::getColorFromHex("3681a8")));//濃い青
            colors.push_back(Color4F(Common::getColorFromHex("dd7672")));//濃いオレンジ
            colors.push_back(Color4F(Common::getColorFromHex("1da481")));//濃い緑
            colors.push_back(Color4F(Common::getColorFromHex("6db134")));//明るい緑
            colors.push_back(Color4F(Common::getColorFromHex("f0a85c")));//黄色
            colors.push_back(Color4F(Common::getColorFromHex("d3146c")));//赤色
            
            for (int i=0; i<colors.size(); i++) {
                auto particle=ParticleSystemQuad::create("particle_texture.plist");
                particle->setAutoRemoveOnFinish(true);
                particle->resetSystem();
                
                particle->setStartSize(stencil->getContentSize().width*.035);
                particle->setStartSizeVar(stencil->getContentSize().width*.005);
                particle->setEndSize(particle->getStartSize());
                particle->setSpeed(stencil->getContentSize().height*.2);
                particle->setPosition(stencil->getContentSize().width/2,stencil->getContentSize().height+particle->getStartSize());
                particle->setPosVar(Vec2(stencil->getContentSize().width*.8, 0));
                particle->setStartColor(colors[i]);
                particle->setEndColor(colors[i]);
                clip->addChild(particle);
            }
            
            candidateMenuItem->addChild(clip);
            
            //王冠マーク
            auto imageColorName = candidateMap[MapKey_ImageColorCode].asString();
            Color3B imageColor = Common::getColorFromHex(DataManager::sharedManager()->getColorCodeData(imageColorName));
            float crownHeight = candidateMenuItem->getContentSize().height/4.5;
            auto circle = Sprite::createWithSpriteFrameName("circle.png");
            circle->setCascadeOpacityEnabled(true);
            circle->setScale(crownHeight/circle->getContentSize().height);
            circle->setColor(imageColor);
            circle->setPosition(candidateMenuItem->getContentSize().width/2, candidateMenuItem->getContentSize().height);
            candidateMenuItem->addChild(circle);
            
            auto crown = Sprite::createWithSpriteFrameName("crown_icon.png");
            crown->setColor(Color3B::YELLOW);
            crown->setScale(circle->getContentSize().width*.6/crown->getContentSize().width);
            crown->setPosition(circle->getContentSize()/2);
            circle->addChild(crown);
           
            
            scale = 1.1;
            candidateMenuItem->setTappedScale(1.1);
        }
        else {//敗者の加工
            
            auto gray = Sprite::create();
            gray->setTextureRect(Rect(0, 0, candidateMenuItem->getContentSize().width, candidateMenuItem->getContentSize().height));
            gray->setPosition(candidateMenuItem->getContentSize()/2);
            gray->setColor(Common::getColorFromHex(BlackColor));
            gray->setOpacity(100);
            candidateMenuItem->addChild(gray);
            
            scale = .8;
        }
        
        float originalScale = candidateMenuItem->getScale();
        candidateMenuItem->setScale(originalScale * scale);
        
        candidateMenuItem->setEnabled(false);
        
        i++;
    }
}

#pragma mark - 投票用アラート
void VoteLayer::showSpeachAlert()
{
    //m_selectedCandidateを使って表示内容を変更する。
    auto candidateMap = m_voteInfo[MapKey_Candidates].asValueVector().at(m_selectedCandidate).asValueMap();
    
    auto candidateName = EscapeDataManager::getInstance()->getLocalizeStringForVoteWithKey(candidateMap,MapKey_TitleName);

    auto speechSt = EscapeDataManager::getInstance()->getLocalizeStringForVoteWithKey(candidateMap, MapKey_Detail);
    auto nameSt = StringUtils::format("%sの公約", candidateName.c_str());
    auto candidateFileName = candidateMap[MapKey_FileName].asString();

    auto contents = Sprite::create/*WithSpriteFrameName*/(/*"alert_back_logo.png"*/);
    contents->setTextureRect(Rect(0, 0, mainSprite->getContentSize().width*.8, mainSprite->getContentSize().width*.4));
    contents->setCascadeOpacityEnabled(true);
    contents->setColor(mainSprite->getColor());
    //contents->setOpacity(true);
    auto greetingSp = Sprite::createWithSpriteFrameName(StringUtils::format("%s_greeting.png", candidateFileName.c_str()));
    greetingSp->setScale(contents->getContentSize().height/greetingSp->getContentSize().height);
    greetingSp->setPosition(contents->getContentSize()/2);
    contents->addChild(greetingSp);
    
    std::vector<std::string> buttons;
    if (m_isVotedToday) {
        buttons = {"CLOSE"};
    }
    else {
        buttons = {"コインで投票", "動画で投票"};
    }
    
    
    auto voteConfirmAlert = ContentsAlert::create(contents, nameSt, speechSt, buttons, [this](Ref* sender){
        auto alert = (ContentsAlert*)sender;
        int selectedNum = alert->getSelectedButtonNum();
        log("%d番目のボタンが押されました。", selectedNum);
        if (!m_isVotedToday) {
            if (selectedNum == 0) {//コインで投票
                log("これからコインで投票確認");
                
                this->showVoteAlertWithCoins();
            }
            else {//動画で投票
                log("これから動画で投票確認");
                
                this->showVoteAlertWithVideo();
            }
        }
        else {
            m_selectedCandidate = 0;
        }
        
    });
    
    if (!m_isVotedToday) {
        voteConfirmAlert->displayCloseButton([this](){
            log("確認ボタンが閉じられたブヒ");
            m_selectedCandidate = 0;
        });
    }
    
    voteConfirmAlert->showAlert();
}

void VoteLayer::showVoteAlertWithCoins()
{
    //コインで投票アラートが表示されたら、投票数1セット
    m_voteCount = 1;

    std::string text = "投票するコイン数を指定してください。2コインで一票投票が可能です。%d票投票します。";

    auto voteConfirmAlert = ContentsAlert::create(NULL, "確認", "", {"投票"}, nullptr);
    //コンテンツを指定
    auto sprite = Sprite::create();
    sprite->setTextureRect(Rect(0, 0, mainSprite->getContentSize().width, mainSprite->getContentSize().width/3));//3:1
    
    float spinnerWidth = sprite->getContentSize().width*3/5;
    Size spinnerSize = Size(spinnerWidth, spinnerWidth/3);
    auto spinner = Spinner::create(spinnerSize, [voteConfirmAlert, text, this](Ref* sender){
        Common::playSE("pi.mp3");
        
        auto spine = (Spinner*)sender;
        int count = spine->getCount();
        log("カウントは%d",count);
        
        //投票数を記憶
        m_voteCount = count/CoinPerVote;
        
        //メッセージを更新
        auto voteMessage = StringUtils::format(text.c_str(), m_voteCount);
        voteConfirmAlert->setContentsMessage(voteMessage);
    });
    spinner->setUnit(CoinPerVote);
    spinner->setCount(CoinPerVote);
    spinner->setMaxCount(200);
    spinner->setMinCount(CoinPerVote);
    spinner->setPosition(sprite->getContentSize()/2);
    sprite->addChild(spinner);
    
    //コイン画像
    float coinSpace = (sprite->getContentSize().width-spinnerWidth)/2;
    /*
    auto coinSp = Sprite::createWithSpriteFrameName("coin_image.png");
    coinSp->setScale(coinSpace*.85/coinSp->getContentSize().width);
    coinSp->setPosition(coinSpace/2, sprite->getContentSize().height/2);
    sprite->addChild(coinSp);*/
    
    //コインラベル
    auto coinLabel = Label::createWithTTF("コイン", Common::getUsableFontPath(HiraginoMaruFont), coinSpace/5);
    coinLabel->setPosition(sprite->getContentSize().width-coinSpace+coinLabel->getContentSize().width/2, sprite->getContentSize().height/2-spinnerSize.height/2+coinLabel->getContentSize().height/2);
    coinLabel->setTextColor(Color4B(Common::getColorFromHex(MyColor)));
    sprite->addChild(coinLabel);
    
    //setSubContentsSpriteの前に行う必要あり。
    auto voteMessage = StringUtils::format(text.c_str(), spinner->getCount()/2);
    voteConfirmAlert->setContentsMessage(voteMessage);
    voteConfirmAlert->setSubContentsSprite(sprite, Size::ZERO);
    
    voteConfirmAlert->setSelectedButtonCallback([this, spinner](Ref* sender){
        
        bool isEnoughCoin = DataManager::sharedManager()->isEnoughCoin(spinner->getCount());
        if (isEnoughCoin) {//コインが十分ある。
            this->vote(VoteType_Coin);
        }
        else {//コイン足りない。
            auto shortageSt = DataManager::sharedManager()->getSystemMessage("shortageCoin");
            auto shortageAlert = ContentsAlert::create("shortage.png", AlertTitle_INFORMATION, shortageSt.c_str(), {DataManager::sharedManager()->getSystemMessage("close"), DataManager::sharedManager()->getSystemMessage("shop")}, [this](Ref* sender){
                
                auto al = (ContentsAlert*)sender;
                auto selectedNum = al->getSelectedButtonNum();
                if (selectedNum == 0) {
                    this->showSpeachAlert();
                }
                else if (selectedNum == 1) {
                    auto shopLayer = ShopMenuLayer::create([this](Ref*sender){
                        this->showVoteAlertWithCoins();
                    });
                    
                    shopLayer->showAlert(this);
                }
            });
            shortageAlert->showAlert(this);
        }
    });
    voteConfirmAlert->displayCloseButton([this](){
        log("確認ボタンが閉じられたブヒ");
        //this->showSpeachAlert();
    });
    
    voteConfirmAlert->showAlert();
}

void VoteLayer::showVoteAlertWithVideo()
{
    //動画アラートが表示されたら、1票をセット。
    m_voteCount = 1;
    
    auto voteConfirmAlert = ContentsAlert::create("thumb_question.png", "確認", "動画を視聴して1票投票しますか？\n*投票はコインとビデオどちらか一方しか行うことができません。", {"コインで投票", "動画で投票"}, [this](Ref* sender){
        auto alert = (ContentsAlert*)sender;
        int selectedNum = alert->getSelectedButtonNum();
        
        if (selectedNum == 0) {//コインで投票
            this->showVoteAlertWithCoins();
        }
        else {//動画で投票
            bool debug = false;
            
            Common::performProcessForDebug([&debug](){
                debug = true;
            }, nullptr);
            
            if (debug) {
                this->vote(VoteType_Video);
                return;
            }
            
            RewardMovieManager::sharedManager()->play([this](bool completed){
                if (completed) {//動画視聴完了
                    log("サンキューアラート出すか悩む。動画で投票が完了しました。");
                    this->vote(VoteType_Video);
                }
                else {//動画視聴失敗
                    log("視聴失敗アラートを挟む");
                    this->showVoteAlertWithVideo();
                }
            });
        }
        
        log("%d番目のボタンが押されました。", selectedNum);
    });
    voteConfirmAlert->displayCloseButton([this](){
        log("確認ボタンが閉じられたブヒ");
        //this->showSpeachAlert();
    });
    voteConfirmAlert->showAlert();
}

#pragma mark - 投票処理
void VoteLayer::loadResultValue()
{
    if (m_voteTerm == VoteTerm_NONE)//中間発表にも行ってないなら、結果データいらない。
        return;
    
    //中間結果もしくは結果を読み込みセット。データはアプリ起動時に読み込み済み
    auto votesDirName = (m_voteTerm == VoteTerm_Interim)? StringUtils::format("%s_%s", Votes, Interim) : Votes;
    auto voteValue = EscapeDataManager::getInstance()->getFB_AppDataMap()[votesDirName];
    m_VoteResultValue = Value();
    if (!voteValue.isNull()) {
        auto voteValueMap = voteValue.asValueMap();
        m_VoteResultValue = voteValueMap[m_key_name];
    }
    else if (m_voteTerm == VoteTerm_Interim) {//結果がない && 中間発表ならサーバから読み込み。
        interimReport();
    }
}

void VoteLayer::vote(VoteType voteType)
{
    if (m_key_name.size()>0) {
        //auto fbDir = StringUtils::format("%s_%s_%s/%s/", Votes, Interim, FirebaseDownloadDir, key.c_str());
        
        auto fbDir = getFirVoteDir(false, m_key_name.c_str());

        //選択番号でfirebaseにあげると、取得の際に勝手に配列で取得される。バグる。
        std::vector<std::string> candidateNames = {"first", "second"};
        auto childKey = candidateNames.at(m_selectedCandidate);
        
        log("投票します。[%s]",fbDir.c_str());
   
        FireBaseBridge::getInstance()->transactionPost(fbDir, {{childKey, m_voteCount}}, [this, voteType](bool successed){
            
            Common::performProcessForDebug([&successed](){
                //successed = false;
            }, nullptr);
            
            if (successed) {
                
                std::string voteKey;
                Value voteValue;
                switch (voteType) {
                    case VoteType_Coin:{
                        //コインを使用。マイグレーションしたら、コイン使用処理はUserDataManagerに投げる。
                        int usedCoins = m_voteCount*CoinPerVote;
                        DataManager::sharedManager()->useCoin(usedCoins);
                        
                        voteKey = VoteKey_UseCoins;
                        voteValue = Value(usedCoins);
                        
                        break;
                    }
                    case VoteType_Video:{
                        voteKey = VoteKey_IsShowVideo;
                        voteValue = Value(true);
                        
                        break;
                    }
                    default:{
                        log("これ呼ばれていたらおかしい。コインとビデオ以外で投票されている例外。");
                        Common::performProcessForDebug([](){
                            assert(false);
                        }, nullptr);
                    }
                        break;
                }
                
                UserDataManager::getInstance()->saveVoteData(m_voteInfo[MapKey_KeyName].asString().c_str(), voteKey.c_str(), voteValue, m_voteCount);
                
                std::string completeVoteText = "ご投票ありがとうございます。";
                int dayToInterim = EscapeDataManager::getInstance()->getDaysToVoteDate(m_voteInfo, MapKey_InterimDate);
                int dayToDeadline = EscapeDataManager::getInstance()->getDaysToVoteDate(m_voteInfo, MapKey_DeadlineDate);

                if (dayToInterim == 1) {//中間発表まであと1日
                    completeVoteText += "明日はいよいよ中間発表！";
                    
                }
                else if (dayToDeadline == 1) {//結果発表まであと1日
                    completeVoteText = completeVoteText + "明日はいよいよ結果発表！忘れずに結果をチェックしてくださいね。";
                }
                else if (dayToDeadline > 1) {//結果発表まで2日以上ある
                    completeVoteText += "明日のご投票もお待ちしてますね！";
                }
                
                
                UtilsMethods::showThanksAlert(completeVoteText.c_str(), [this](Ref* sender){
                    //投票完了したら、とじちゃう
                    closeAnimation(false);
                });
            }
            else {
                auto failedVoteText = "投票に失敗しました。通信環境が良好な状態で再度お試しくださいませ。";
                auto alert = ContentsAlert::create("failed.png", AlertTitle_INFORMATION, failedVoteText, {"OK"}, [](Ref *sender){
                    
                });
                alert->showAlert(this);
            }
            
            log("投票が完了しました！！！%d", successed);
        });
    }
    
    log("！！！！！！投票が完了しました！！！！！グラフの表示などを行うよ。");
}

void VoteLayer::interimReport()
{
    auto fbDir = getFirVoteDir(true, m_key_name.c_str());
    log("取得する投票のキーは%s",fbDir.c_str());
    FireBaseBridge::getInstance()->loadingValue(fbDir.c_str(), [this](bool successed, Value value){
        log("値を取得しました。%d",successed);
        ValueHelper::dumpValue(value);
        if (value.isNull()) {//中間発表のデータが無かったら
            auto fbDir = getFirVoteDir(false, m_key_name.c_str());
            FireBaseBridge::getInstance()->loadingValue(fbDir.c_str(), [this](bool successed, Value value){
                 if (!value.isNull() && successed) {//
                     
                     ValueMap valueMap = value.asValueMap();
                     
                     log("取得したValueをセットします");
                     std::map<std::string, int> map;
                     for (ValueMap::const_iterator itr = value.asValueMap().begin(); itr != value.asValueMap().end(); itr++) {
                         auto firstSt = (*itr).first;
                         auto secondValue = (*itr).second;

                         //ValueHelper::dumpValue(secondValue);
                         
                         map[firstSt] = secondValue.asInt();
                     }
                     this->readVoteData(value);

                     FireBaseBridge::getInstance()->setValue(getFirVoteDir(true, m_key_name.c_str()), map, [](bool successed){
                         log("中間発表の値をセットしました。%d",successed);
                     });
                 }
            });
        }
        else {//中間発表データがある
            log("中間発表データがある");
            if (m_voteTerm == VoteTerm_Interim) {//中間発表中なら取得データを保持
                this->readVoteData(value);
            }
        }
    });
}

/*
void VoteLayer::resultReport()
{
    auto fbDir = getFirVoteDir(false, m_key_name.c_str());
    
    FireBaseBridge::getInstance()->loadingValue(fbDir.c_str(), [this](bool successed, Value value){
        if (!value.isNull() && successed) {//
            ValueMap valueMap = value.asValueMap();
            this->readVoteData(value);
        }
    });
}*/

void VoteLayer::readVoteData(Value value)
{
    m_VoteResultValue = value;
    
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(NotificationKey_LoadedInterim);

    return;
    auto resultMap = m_VoteResultValue.asValueMap();
    
    int firstVotes = resultMap["first"].asInt();
    int secondVotes = resultMap["second"].asInt();
    std::vector<int> votesVector = {firstVotes, secondVotes};
    int totalVoteCount = firstVotes + secondVotes;

    auto totalVotesLabel = m_gaugeSprite->getChildByName<Label*>(TotalVoteLabelName);
    totalVotesLabel->setString(StringUtils::format("総得票数 %s票", NativeBridge::transformThreeComma(totalVoteCount).c_str()));
    
    for (int i = 0; i < 2; i++) {
        float percent = roundf((float)votesVector.at(i)/(float)totalVoteCount*100);

       auto percentLabel = m_gaugeSprite->getChildByName<Label*>(StringUtils::format("%s%d", PercentLabelName, i));
        percentLabel->setString(StringUtils::format("%s%%",NativeBridge::transformThreeComma(percent).c_str()));
        auto powValue = pow(-1, (i+1));
       
        if (i == 0) {//ゲージをアニメーションして動かす
            m_gaugeSprite->setPercentage(percent, true, .5);
        }
    }
    
    auto indicator = m_gaugeSprite->getChildByName<NCIndicator*>(IndicatorName);
    indicator->removeFromParent();
    
    auto bounding = getCascadeBoundingBox(m_gaugeSprite);
    log("コンテントサイズは{%f, %f}::バウンディングボックス{%f,%f}", m_gaugeSprite->getBoundingBox().size.width, m_gaugeSprite->getBoundingBox().size.height, bounding.size.width, bounding.size.height);
}

std::string VoteLayer::getFirVoteDir(bool isInterim, const char* childkey)
{
    auto fbDir = StringUtils::format("%s%s/%s", FirebaseDownloadDir, Votes, childkey);
    
    if (isInterim) {
        fbDir = StringUtils::format("%s%s_%s/%s", FirebaseDownloadDir, Votes, Interim, childkey);
    }
    
    log("投票のFirebaseディレクトリは%s",fbDir.c_str());
    
    return fbDir;
}


#pragma mark - オーバーライド
void VoteLayer::arrange()
{//カスタムアラートの配置規則を上書き
    //CustomAlert::arrange();
    //spaceはボタン半分の高さを含んでる?
    float titleHeight = space * 2;//タイトルの表示高さ.space*がtitleLabelの高さに対応
    float contentsHeight = messageLabel->getContentSize().height + titleHeight;
    
    log("%f::::カスタムアラート::::%f", minSize.height , messageLabel->getContentSize().height + titleHeight);
    mainSprite->setTextureRect(Rect(0, 0, minSize.width, contentsHeight));
    
    titleLabel->setPosition(mainSprite->getContentSize().width/2, mainSprite->getContentSize().height-space);
    messageLabel->setPosition(mainSprite->getContentSize().width/2, messageLabel->getContentSize().height/2);//メッセージラベルを気持ち上に配置する。
}
