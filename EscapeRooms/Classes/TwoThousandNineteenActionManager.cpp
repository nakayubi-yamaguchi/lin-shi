//
//  IzakayaActionManager.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2018/10/17.
//
//

#include "TwoThousandNineteenActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "GameScene.h"
#include "TouchManager.h"

using namespace cocos2d;

TwoThousandNineteenActionManagerManager* TwoThousandNineteenActionManagerManager::manager =NULL;


TwoThousandNineteenActionManagerManager* TwoThousandNineteenActionManagerManager::getInstance()
{
    if (manager==NULL) {
        manager=new TwoThousandNineteenActionManagerManager();
    }
    
    return manager;
}

#pragma mark - Back
void TwoThousandNineteenActionManagerManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
#pragma mark 凧
    if (key == "kite") {
        Vector<FiniteTimeAction*>actions;
        
        auto swingMove = parent->getContentSize().height * ((backID == 60)? .02 : .01);
        
        for (int i = 0; i < 4; i++) {
            auto kite = parent->getChildByName<Sprite*>(StringUtils::format("sup_%d_tako_%d.png", backID, i));
            auto swing=swingAction(Vec2(0, swingMove), 2, kite, 2, false, -1);
            actions.pushBack(swing);
            
            if (DataManager::sharedManager()->isPlayingMinigame()&&
                i==0) {
                auto mistake=parent->getChildByName(StringUtils::format("sup_%d_mistake.png", backID));
                actions.pushBack(swingAction(Vec2(0, swingMove), 2, mistake, 2, false, -1));
            }
        }
        
        parent->runAction(Repeat::create(Spawn::create(actions), 1));
    }
#pragma mark 餅
    else if (key == "mochi" && DataManager::sharedManager()->getEnableFlagWithFlagID(22)) {
        Vector<FiniteTimeAction*>actions;

        std::vector<int> answers = {0, 1, 2, 1, 2, 1};
        auto mochi_1 = createSpriteToCenter("sup_65_mochi_1.png", true, parent, true);
        auto mochi_2 = createSpriteToCenter("sup_65_mochi_2.png", true, parent, true);
        auto mochi_3 = createSpriteToCenter("sup_65_mochi_3.png", true, parent, true);

        vector<Sprite*> mochis = {mochi_1, mochi_2, mochi_3};
        int i = 0;
        actions.pushBack(DelayTime::create(1));
        for (auto answer : answers) {
            if (i == 0) {
                actions.pushBack(TargetedAction::create(mochis[answer], FadeIn::create(.5)));
            }
            else {
                actions.pushBack(createChangeAction(.5, .5, mochis[answers[i-1]], mochis[answer], "nose.mp3"));
            }
            actions.pushBack(DelayTime::create(1));
            
            //最後
            if (i == answers.size()-1) {
                actions.pushBack(TargetedAction::create(mochis[answer], FadeOut::create(.5)));
                actions.pushBack(DelayTime::create(1));
            }
            i++;
        }
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
        
        auto particle = showFireParticle(parent, backID);
        particle->setPosition(parent->getContentSize().width*.467, parent->getContentSize().height*.226);
        particle->setStartSize(particle->getStartSize()*.9);
        particle->setEndSize(particle->getEndSize()*.9);
    }
#pragma mark 鼻水&震える豚
    else if (key == "coldpig" && !DataManager::sharedManager()->getEnableFlagWithFlagID(32)) {
        
        Vector<FiniteTimeAction*> actions;
        //豚が震える
        auto pig = parent->getChildByName<Sprite*>(StringUtils::format("sup_%d_pig_0.png", backID));
        actions.pushBack(swingAction(Vec2(parent->getContentSize().width*.01, 0), .1, pig, 1.5, false, 5));
        
        //鼻水垂らす
        std::vector<Vec2> hanamizuAnchors = {Vec2(.47, .462), Vec2(.523, .462)};
        
        if (backID == 68) {
            hanamizuAnchors = {Vec2(.38, .568), Vec2(.4, .568)};
        }
        
        Vector<Sprite*> hanamizu;
        
        int i = 0;
        for (auto hanamizuAnchor : hanamizuAnchors) {
            auto hana = createAnchorSprite(StringUtils::format("sup_%d_hanamizu_%d.png", backID, i), hanamizuAnchor.x, hanamizuAnchor.y, false, pig, true);
            hana->setScaleY(0);
            hanamizu.pushBack(hana);
            
            i++;
        }
        
        std::vector<int> answers = {0, 1, 1, 0, 1, 1, 0, 0};
        
        for (auto answer : answers) {
            if (backID == 69)
                actions.pushBack(createSoundAction("pop_5.mp3"));
            
            actions.pushBack(TargetedAction::create(hanamizu.at(answer), EaseOut::create(ScaleTo::create(.8, 1, 1), 2)));
            actions.pushBack(DelayTime::create(.8));
            
            if (backID == 69)
                actions.pushBack(createSoundAction("pop_2.mp3"));
            
            actions.pushBack(TargetedAction::create(hanamizu.at(answer), EaseOut::create(ScaleTo::create(.1, 1, 0), 2)));
            actions.pushBack(DelayTime::create(1));
        }
        
        pig->runAction(Repeat::create(Sequence::create(actions), -1));
    }
#pragma mark 囲炉裏の火
    else if (key == "irorifire" && DataManager::sharedManager()->getEnableFlagWithFlagID(29)) {
        auto fire = showFireParticle(parent);
        fire->setTotalParticles(150);
        
        if (backID == 30) {
            fire->setLife(fire->getLife()/2.5);
            fire->setPositionY(parent->getContentSize().height*.572);
            fire->setStartSize(fire->getStartSize()*.9);
            fire->setEndSize(fire->getEndSize()*.9);
            fire->setPosVar(fire->getPosVar()/2);
            fire->setSpeed(fire->getSpeed()*.7);
        }
        else if (backID == 3) {
            fire->setLife(fire->getLife()*.15);
            fire->setPositionY(parent->getContentSize().height*.238);
            fire->setStartSize(fire->getStartSize()*.65);
            fire->setEndSize(fire->getEndSize()*.65);
            fire->setSpeed(fire->getSpeed()*.5);
            fire->setPosVar(fire->getPosVar()/4);
        }
        
    }
#pragma mark 鼻ちょうちん
    else if (key == "back_47" && !DataManager::sharedManager()->getEnableFlagWithFlagID(17)) {
        auto balloonFile = "sup_47_balloon.png";
        auto balloonAct = balloonAction(parent, balloonFile, Vec2(.813, .417), 2);
        auto balloon = parent->getChildByName(balloonFile);
        
        balloon->runAction(balloonAct);
        
    }
}

#pragma mark - Touch
void TwoThousandNineteenActionManagerManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
        Vector<FiniteTimeAction*> actions;
#pragma mark クラウド。欲しがる。
    if (key == "cloud") {
        int num = DataManager::sharedManager()->getNowBack();
        if (num == 10) {//monkey
            auto cloudBounce = cloudBounceAction(parent, {"sup_10_monkey_0.png"}, Vec2(.833, .15), "sup_10_cloud.png", Vec2(.75, .618), "");
            actions.pushBack(cloudBounce);
        }
        else if (num == 27) {//rabbit
            auto cloud = cloudAction(parent, "sup_27_cloud.png", Vec2(.415, .473), .4);
            actions.pushBack(cloud);
        }
        else if (num == 28) {//rabbit
            auto cloud = cloudAction(parent, "sup_28_cloud.png", Vec2(.5, .5), .4);
            actions.pushBack(cloud);
        }else if (num == 23) {//monkey
            auto cloudBounce = cloudBounceAction(parent, {"sup_23_monkey_0.png"}, Vec2(.5, .2), "sup_23_cloud.png", Vec2(.4, .67), "");
            actions.pushBack(cloudBounce);
        }
        else if (num == 68) {//monkey
            auto cloudBounce = cloudBounceAction(parent, {"sup_68_monkey_0.png"}, Vec2(.86, .25), "sup_68_cloud.png", Vec2(.8, .618), "");
            actions.pushBack(cloudBounce);
        }
        else if (num == 87) {//pig
            
            auto cloudBounce = cloudBounceAction(parent, {"sup_87_pig_0.png"}, Vec2(.5, .25),StringUtils::format("sup_87_cloud_%d.png",DataManager::sharedManager()->getEnableFlagWithFlagID(33)) , Vec2(.42, .61), "");
            actions.pushBack(cloudBounce);
        }
        else if (num == 64) {//pig:山追加
            auto cloudBounce = cloudBounceAction(parent, {"sup_64_pig_0.png"}, Vec2(.495, .288), "sup_64_cloud.png", Vec2(.628, .729), "pig.mp3");

            actions.pushBack(cloudBounce);
        }
        
        //コールバック
        auto call = CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark ドアopen まちがい
    else if(key=="opendoor")
    {
        Common::playSE("su.mp3");
        auto open=createAnchorSprite("sup_17_open.png",.55,.6, true, parent,true);
        actions.pushBack(TargetedAction::create(open, FadeIn::create(.2)));
        
        parent->getChildByName("sup_17_mistake.png")->setOpacity(255);
        
        //callback
        auto call = CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark クリア
    else if (key=="clear") {
        Common::playSE("su.mp3");
        auto open=createAnchorSprite("sup_17_open.png",.55,.6, true, parent,true);
        createSpriteToCenter("sup_17_postman.png", false, open,true);
        actions.pushBack(TargetedAction::create(open, FadeIn::create(.2)));
        actions.pushBack(DelayTime::create(1));
        
        auto back_29=createSpriteToCenter("back_29.png", true, parent,true);
        createAnchorSprite("sup_29_postman.png", .5, .18, false, back_29, true);
        actions.pushBack(TargetedAction::create(back_29, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        
        //callback
        auto call = CallFunc::create([parent
                                      ,this]{
            auto story=StoryLayer::create("ending1", [parent,this]{
                Director::getInstance()->replaceScene(TransitionFade::create(5, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark おちゃ注ぐ
    else if (key == "tea") {
        Common::playSE("p.mp3");
        //cup
        auto cup=parent->getChildByName("sup_49_cup.png");
        
        //pot
        auto item=createAnchorSprite("sup_49_kettle.png", .5, .5, true, parent, true);
        actions.pushBack(TargetedAction::create(item, FadeIn::create(1)));
        
        //pour
        auto angle=-3;
        auto clip=createClippingNodeToCenter("sup_49_clip.png", parent);
        auto tea=createSpriteToCenter("sup_49_tea.png", true, parent);
        tea->setPositionY(parent->getContentSize().height*.4);
        clip->addChild(tea);
        
        //particle
        auto addP=CallFunc::create([parent,cup]{
            auto particle=ParticleSystemQuad::create("smoke.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width/2, parent->getContentSize().height*.6);
            particle->setPosVar(Vec2(parent->getContentSize().width*.07, 0));
            particle->setSpeed(parent->getContentSize().height*.07);
            particle->setTotalParticles(50);
            particle->setAngle(90);
            particle->setAngleVar(10);
            particle->setLife(2);
            particle->setDuration(4);
            particle->setStartSize(parent->getContentSize().width*.05);
            particle->setEndSize(parent->getContentSize().width*.1);
            particle->setLocalZOrder(2);
            cup->addChild(particle);
        });
        
        auto spawn=Spawn::create(TargetedAction::create(item, RotateBy::create(3, angle)),
                                 createSoundAction("jobojobo.mp3"),
                                 Sequence::create(DelayTime::create(.5),
                                                  addP, NULL),
                                 TargetedAction::create(tea, MoveTo::create(2,parent->getContentSize()/2)),
                                 TargetedAction::create(tea, FadeIn::create(1)),
                                 NULL);
        actions.pushBack(spawn);
        actions.pushBack(TargetedAction::create(item, FadeOut::create(1)));
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(Spawn::create(TargetedAction::create(cup, FadeOut::create(1)),
                                       TargetedAction::create(tea, FadeOut::create(1)), NULL));
        actions.pushBack(DelayTime::create(.5));
        
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
#pragma mark すみ
    else if (key=="ink") {
        Common::playSE("p.mp3");
        
        auto ink=createAnchorSprite("sup_56_ink.png",.55,.6, true, parent,true);
        actions.pushBack(TargetedAction::create(ink, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        
        auto liquid=createSpriteToCenter("sup_56_liquid.png", true, parent,true);
        
        auto angle=-15;
        ink->setRotation(-angle);
        
        actions.pushBack(TargetedAction::create(ink, Sequence::create(RotateBy::create(.7, angle),
                                                                      createSoundAction("jobojobo.mp3"),
                                                                      TargetedAction::create(liquid, FadeIn::create(1)),
                                                                      RotateBy::create(.7, -angle),
                                                                      NULL)));
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(TargetedAction::create(ink, FadeOut::create(.7)));
        actions.pushBack(DelayTime::create(.5));
        
        //callback
        auto call = CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark ふで
    else if (key=="brush") {
        Common::playSE("p.mp3");
        
        auto brush=createAnchorSprite("sup_56_brush.png",.5,.5, true, parent,true);
        auto brush_ink=createSpriteToCenter("sup_56_brush_ink.png", true, brush,true);
        actions.pushBack(TargetedAction::create(brush, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        
        auto distance=-parent->getContentSize().height*.1;
        brush->setPositionY(brush->getPositionY()-distance);
        actions.pushBack(createSoundAction("pop.mp3"));
        actions.pushBack(Spawn::create(jumpAction(distance, .3, brush, 1.5,2),
                                       Sequence::create(DelayTime::create(.3),
                                                        TargetedAction::create(brush_ink, FadeIn::create(.6)),
                                                        NULL),
                                       NULL));
        
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(TargetedAction::create(brush, FadeOut::create(.7)));
        actions.pushBack(DelayTime::create(.5));
        
        //callback
        auto call = CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 切手
    else if (key=="stamp") {
        Common::playSE("p.mp3");
        
        auto doll_0=parent->getChildByName("sup_91_doll_0.png");
        auto doll_1=createSpriteToCenter("sup_91_doll_1.png", true, parent,true);
        auto doll_2=createSpriteToCenter("sup_91_doll_2.png", true, parent,true);
        auto doll_3=createSpriteToCenter("sup_91_doll_3.png", true, parent,true);

        auto card=parent->getChildByName("sup_91_card.png");
        auto stamp_1=createSpriteToCenter("sup_91_stamp.png", true, card,true);

        auto stamp=createSpriteToCenter("sup_91_stamp_anim.png", true, parent,true);
        actions.pushBack(giveAction(stamp, .7, .8, true));
        actions.pushBack(createChangeAction(.7, .5, doll_0, doll_1, "pop.mp3"));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(Spawn::create(createChangeAction(.7, .5, doll_1, doll_2, "pop.mp3"),
                                       TargetedAction::create(stamp_1, FadeIn::create(1)), NULL));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createChangeAction(.7, .5, doll_2, doll_3, "pop.mp3"));

        actions.pushBack(TargetedAction::create(card, TargetedAction::create(card, FadeOut::create(1))));
        actions.pushBack(DelayTime::create(1));
        
        //story
        auto call = CallFunc::create([callback,parent]{
            auto story=StoryLayer::create("ending", [callback,parent]
                                          {
                                              callback(true);
                                          });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark バナナ
    else if (key=="banana") {
        Common::playSE("p.mp3");
        
        auto monkey=parent->getChildByName("sup_83_monkey.png");
        auto banana=createSpriteToCenter("sup_83_banana.png", true, parent,true);
        actions.pushBack(giveAction(banana, 1, .7, true));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createSoundAction("monkey.mp3"));
        actions.pushBack(createBounceAction(monkey, .3, .1));
        actions.pushBack(DelayTime::create(.5));

        //callback
        auto call = CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 小判
    else if (key=="koban") {
        Common::playSE("p.mp3");
        
        auto cat=parent->getChildByName("sup_50_cat.png");
        auto koban=createSpriteToCenter("sup_50_koban.png", true, parent,true);
        actions.pushBack(giveAction(koban, 1, .7, true));
        
        auto koban_oncat=createSpriteToCenter("sup_50_koban_oncat.png", true, cat,true);
        actions.pushBack(TargetedAction::create(koban_oncat, Spawn::create(FadeIn::create(.5),
                                                                           createSoundAction("pop.mp3"), NULL)));
        actions.pushBack(DelayTime::create(1));
        
        //move
        actions.pushBack(TargetedAction::create(cat, Spawn::create(createSoundAction("cat_0.mp3"),
                                                                   EaseInOut::create(MoveBy::create(1, Vec2(-parent->getContentSize().width, 0)), 1.5),
                                                                   NULL)));
        actions.pushBack(DelayTime::create(1));
        
        //callback
        auto call = CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark さいころ
    else if (key=="dice") {
        Common::playSE("p.mp3");
        
        // give dice
        auto dice=createSpriteToCenter("sup_85_dices.png", true, parent,true);
        actions.pushBack(giveAction(dice, 1, .9, true));
        actions.pushBack(DelayTime::create(.5));
        
        //rats
        auto rats_0=parent->getChildByName("sup_85_rats_0.png");
        auto rats_1=createSpriteToCenter("sup_85_rats_1.png", true, parent,true);
        auto rats_2=createAnchorSprite("sup_85_rats_2.png",.5,.2, true, parent,true);
        
        //
        actions.pushBack(createChangeAction(.7, .3, rats_0, rats_1, "pop.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        //throw dice
        std::vector<Vec2>anchors={Vec2(.126, .33),Vec2(.375, .33),Vec2(.625, .33),Vec2(.855, .33)};
        Vector<FiniteTimeAction*> fadeins;
        fadeins.pushBack(createChangeAction(.7, .3, rats_1, rats_2,"pop.mp3"));
        
        std::vector<float>distanceXs={-.03,-.01,.01,.03};
        Vector<FiniteTimeAction*>throws;
        auto duration_pull=.3;
        auto duration_delay=.1;
        auto duration_throw=.3;
        Vector<FiniteTimeAction*>fadeouts;
        auto duration_fadeout=.15;
        
        auto scale_original=rats_2->getScale();
        throws.pushBack(TargetedAction::create(rats_2, Sequence::create(ScaleTo::create(duration_pull, scale_original,scale_original*.9),
                                                                        DelayTime::create(duration_delay),
                                                                        createSoundAction("pop_3.mp3"),
                                                                        ScaleTo::create(duration_throw, scale_original), NULL)));
        
        auto distance_pull=-parent->getContentSize().height*.03;
        
        for (int i=0; i<4; i++) {
            auto anchor=anchors.at(i);
            auto dice=createAnchorSprite(StringUtils::format("sup_85_dice_%d.png",i), anchor.x, anchor.y, true, parent, true);
            fadeins.pushBack(TargetedAction::create(dice, FadeIn::create(.7)));
            
            throws.pushBack(TargetedAction::create(dice, Sequence::create(MoveBy::create(duration_pull, Vec2(0, distance_pull)),
                                                                          DelayTime::create(duration_delay),
                                                                          MoveBy::create(duration_pull, Vec2(0, -distance_pull)),
                                                                          Spawn::create(JumpBy::create(1.0, Vec2(parent->getContentSize().width*distanceXs.at(i), -parent->getContentSize().height*.2), parent->getContentSize().height*.3, 1),
                                                                                        Repeat::create(RotateBy::create(1.0/2, 360), 2),
                                                                                        ScaleBy::create(1.0, 1.2),
                                                                                        NULL),
                                                                          Spawn::create(jumpAction(parent->getContentSize().height*.05, .2, dice, 1.5),
                                                                                        RotateBy::create(.2*2, 360),
                                                                                        NULL),
                                                                          NULL)));
            
            fadeouts.pushBack(TargetedAction::create(dice, FadeOut::create(duration_fadeout)));
        }
        actions.pushBack(Spawn::create(fadeins));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(Spawn::create(throws));
        
        //change dice
        auto dices_1=createSpriteToCenter("sup_85_dices_1.png", true, parent,true);
        fadeouts.pushBack(TargetedAction::create(dices_1, FadeIn::create(duration_fadeout)));
        actions.pushBack(Spawn::create(fadeouts));
        actions.pushBack(DelayTime::create(1));
        
        //change scene
        auto back_86=createSpriteToCenter("back_86.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back_86, 86, false);
        
        actions.pushBack(TargetedAction::create(back_86, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(2));
        
        auto chips_0=back_86->getChildByName("sup_86_chips_0.png");
        auto chips_1=createSpriteToCenter("sup_86_chips_1.png", true, back_86,true);
        actions.pushBack(createChangeAction(1, .7, chips_0, chips_1, "pop.mp3"));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(createChangeAction(0, 0, rats_2, rats_0));
        actions.pushBack(TargetedAction::create(back_86, FadeOut::create(1)));
        actions.pushBack(DelayTime::create(1));
        
        //callback
        auto call = CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark ふえ
    else if (key=="flute") {
        Common::playSE("p.mp3");
        
        auto pig_0=parent->getChildByName("sup_53_pig_0.png");
        auto pig_1=createAnchorSprite("sup_53_pig_1.png", .5, .2, true, parent, true);
        auto flute=createSpriteToCenter("sup_53_flute.png", true, parent,true);
        
        auto give=giveAction(flute, 1, .8, true);
        actions.pushBack(give);
        actions.pushBack(createChangeAction(1, .3, pig_0, pig_1,"paku.mp3"));
        
        //music
        auto particle=CallFunc::create([parent]{
            Common::playSE("flute.mp3");
            auto pos=Vec2(parent->getContentSize().width*.33, parent->getContentSize().height*.55);
            auto size=parent->getContentSize().width*.15;
            
            auto particle=ParticleSystemQuad::create("particle_music.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setDuration(6);
            particle->setPosition(pos);
            particle->setLife(6);
            particle->setLifeVar(0);
            particle->resetSystem();
            particle->setStartSize(size);
            particle->setEndSize(size);
            particle->setTotalParticles(6);
            particle->setGravity(Vec2(size*1, -size*.4));
            particle->setStartColor(Color4F::RED);
            particle->setEndColor(Color4F(255, 0, 0, 0));
            
            particle->setAngle(135);
            particle->setSpeed(size*2);
            parent->addChild(particle);
        });
        actions.pushBack(particle);
        actions.pushBack(Repeat::create(createBounceAction(pig_1, .5), 6));
        //actions.pushBack(DelayTime::create(3));
        
        //scene change
        auto back_47=createSpriteToCenter("back_47.png", true, parent,true);
        back_47->setLocalZOrder(1);
        SupplementsManager::getInstance()->addSupplementToMain(back_47, 47, true);
        back_47->getChildByName("sup_47_flutepig_0.png")->removeFromParent();
        createSpriteToCenter("sup_47_flutepig_1.png", false, back_47,true);
        auto match=createSpriteToCenter("sup_47_match.png", true, back_47,true);
        
        auto sleep_pig_0=back_47->getChildByName("sup_47_sleeppig_0.png");
        auto eye=createSpriteToCenter("sup_47_eyes.png", true, sleep_pig_0,true);
        
        actions.pushBack(TargetedAction::create(back_47, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(2));
        actions.pushBack(Spawn::create(attentionAction(back_47, "sup_47_sleeppig_0.png", sleep_pig_0->getAnchorPoint(), "sup_47_attention.png",.1,.05),
                                       TargetedAction::create(eye, FadeIn::create(.1)), NULL));
        actions.pushBack(DelayTime::create(1));
        
        //run pig
        auto sleep_pig_1=createAnchorSprite("sup_47_sleeppig_1.png",.76,.27, true, back_47,true);
        actions.pushBack(createChangeAction(.5, .3, {sleep_pig_0}, {sleep_pig_1,match}, "pop.mp3"));
        auto jumpCount=4;
        auto jump_duration=.5;
        actions.pushBack(Spawn::create(jumpAction(parent->getContentSize().height*.03, jump_duration/jumpCount, sleep_pig_1, 1.5, jumpCount),
                                       createSoundAction("pop_3.mp3"),
                                       TargetedAction::create(sleep_pig_1, MoveBy::create(jump_duration, Vec2(parent->getContentSize().width*.05, -parent->getContentSize().height*.6))),
                                       NULL));
        
        //scene change
        auto back_64=createSpriteToCenter("back_64.png", true, parent,true);
        back_64->setLocalZOrder(1);
        auto pig_on_snow=createSpriteToCenter("sup_64_pig_0.png", true, back_64,true);
        auto fadein_back=TargetedAction::create(back_64, FadeIn::create(1));
        actions.pushBack(fadein_back);
        actions.pushBack(TargetedAction::create(back_47, RemoveSelf::create()));
        actions.pushBack(DelayTime::create(1));
        auto fadein_pig=TargetedAction::create(pig_on_snow, FadeIn::create(.5));
        
        actions.pushBack(createSoundAction("pig.mp3"));
        actions.pushBack(fadein_pig);
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(TargetedAction::create(back_64, FadeOut::create(1)));
        actions.pushBack(DelayTime::create(1));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
#pragma mark リモコン
    else if (key=="remocon") {
        Common::playSE("p.mp3");
        
        auto screen_0=parent->getChildByName("sup_48_screen_0.png");
        auto screen_1=createSpriteToCenter("sup_48_screen_1.png", true, parent,true);
        auto remocon=createSpriteToCenter("sup_48_remocon.png", true, parent,true);
        actions.pushBack(TargetedAction::create(remocon, Sequence::create(FadeIn::create(1),
                                                                          createSoundAction("pi.mp3"),
                                                                          jumpAction(-parent->getContentSize().height*.025, .1, remocon, 1.5),
                                                                          NULL)));
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(createChangeAction(1, .5, screen_0, screen_1));
        actions.pushBack(TargetedAction::create(remocon, FadeOut::create(.5)));
        actions.pushBack(DelayTime::create(1));
        
        //callback
        auto call = CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 和人形箱
    else if (key=="dollbox") {
        Common::playSE("p.mp3");
        auto key_0=createSpriteToCenter("sup_36_key_0.png", true, parent,true);
        auto key_1=createSpriteToCenter("sup_36_key_1.png", true, parent,true);
        
        auto action=createChangeActionSequence(1, .7, {key_0,key_1}, {"kacha.mp3","key_1.mp3"});
        actions.pushBack(action);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
#pragma mark マッチ
    else if (key=="match") {
        actions.pushBack(useItemSoundAction());
        
        //マッチ着火
        int nowBack = DataManager::sharedManager()->getNowBack();
        auto fireParticle = showFireParticle(parent, nowBack);
        fireParticle->setTotalParticles(0);
        actions.pushBack(matchAction(parent, nowBack, fireParticle));
        
        //ちょっと待機
        actions.pushBack(DelayTime::create(1));
        
        actions.pushBack(correctSoundAction());
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark ソリあげる
    else if (key=="givesori") {
        actions.pushBack(useItemSoundAction());
        
        //ソリをあげる
        auto sori = createSpriteToCenter("sup_64_sori.png", true, parent, true);
        actions.pushBack( TargetedAction::create(sori, FadeIn::create(.5)));
        actions.pushBack(createSoundAction("sa.mp3"));
        actions.pushBack(DelayTime::create(.5));
        
        //ソリに乗る
        auto pig_0 = parent->getChildByName<Sprite*>("sup_64_pig_0.png");
        auto pig_1 = createSpriteToCenter("sup_64_pig_1.png", true, parent, true);
        transformToAnchorSprite(parent, pig_1,  Vec2(.398, .064));
        actions.pushBack(createChangeAction(.5, .5, {sori, pig_0}, {pig_1}));
        actions.pushBack(DelayTime::create(.5));
        
        //傾いてGO
        auto rotate = RotateTo::create(.4, 10);
        auto move = EaseInOut::create(MoveTo::create(1, Vec2(parent->getContentSize().width*1.3, -parent->getContentSize().height*.5)), 1.5);
        auto fadeout = FadeOut::create(1);
        auto spawn = Spawn::create(createSoundAction("pop_3.mp3"), fadeout, move, NULL);
        actions.pushBack(TargetedAction::create(pig_1, Sequence::create(rotate, spawn, NULL)));
        actions.pushBack(DelayTime::create(.5));
        
        //back60で滑っていく
        auto back60 = createSpriteToCenter("back_60.png", true, parent, true);
        SupplementsManager::getInstance()->addSupplementToMain(back60, 60);
        back60->removeChildByName("sup_60_pig_0.png");
        actions.pushBack(TargetedAction::create(back60, FadeIn::create(.5)));
        
        //引きで豚が滑る
        auto pig_60 = createAnchorSprite("sup_60_pig_1.png", .49, .291, false, back60, true);
        pig_60->setPosition(back60->getContentSize().width*.203, back60->getContentSize().height*.365);
        pig_60->setRotation(10);
        
        auto landPos = Vec2(back60->getContentSize().width*.358, back60->getContentSize().height*.292);
        auto endPos = Vec2(back60->getContentSize().width*.787, back60->getContentSize().height*.285);
       
        auto move_pig = Sequence::create(MoveTo::create(.8, landPos), MoveTo::create(1.6, endPos), NULL);
        auto rotate_pig = Sequence::create(DelayTime::create(.8), RotateTo::create(1.6, 0), NULL);
        actions.pushBack(createSoundAction("pig.mp3"));
        actions.pushBack(TargetedAction::create(pig_60, EaseIn::create(Spawn::create(move_pig, rotate_pig, NULL), 2.4)));

        //ボンッ
        actions.pushBack(createSoundAction("gun-fire.mp3"));
        auto showBomb = CallFunc::create([back60, endPos](){
            auto particle=ParticleSystemQuad::create("bomb.plist");
            particle->setPosition(endPos);
            //particle->setDuration(.5);
            //particle->setLife(.5);
            particle->setSpeed(particle->getSpeed()*.9);
            particle->setStartSize(particle->getStartSize()*.5);
            particle->setEndSize(particle->getEndSize()*.5);

            back60->addChild(particle);
        });
        actions.pushBack(showBomb);
        
        //豚を消す。
        actions.pushBack(TargetedAction::create(pig_60, FadeOut::create(.2)));
        
        //かまくらに穴開ける。
        auto kamakura = createSpriteToCenter("sup_60_kamakura.png", true, back60, true);
        actions.pushBack(TargetedAction::create(kamakura, FadeIn::create(.1)));
        actions.pushBack(DelayTime::create(2));

        
        //back60
        actions.pushBack(TargetedAction::create(back60, FadeOut::create(.5)));
        actions.pushBack(DelayTime::create(.5));
        
        actions.pushBack(correctSoundAction());
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark お茶あげる
    else if (key=="giveyunomi") {
        actions.pushBack(useItemSoundAction());
        
        auto pig = parent->getChildByName<Sprite*>("sup_69_pig_0.png");
        
        //湯呑みあげる
        auto yunomi = createSpriteToCenter("sup_69_yunomi.png", true, parent, true);
        actions.pushBack(giveAction(yunomi, .6, .85, false));
        actions.pushBack(DelayTime::create(.5));
        
        //湯呑み受け取る
        auto anim_0 = createSpriteToCenter("sup_69_anim_0.png", true, pig, true);
        actions.pushBack(createChangeAction(.5, .5, yunomi, anim_0, "po.mp3"));
        actions.pushBack(DelayTime::create(.8));
        
        //鼻水のアニメーションを止める
        auto stopHanamizu = CallFunc::create([pig](){
            pig->stopAllActions();
            
            for (auto child : pig->getChildren()) {
                auto name = child->getName();
                if (name.find("hanamizu") != std::string::npos) {
                    child->setOpacity(0);
                }
            }
        });
        actions.pushBack(stopHanamizu);

        //お茶飲む
        auto anim_1 = createSpriteToCenter("sup_69_anim_1.png", true, pig, true);
        actions.pushBack(createChangeAction(.5, .5, anim_0, anim_1, "drink.mp3"));
        actions.pushBack(DelayTime::create(1.5));
        
        //飲み終える
        actions.pushBack(createChangeAction(.5, .5, anim_1, anim_0));
        actions.pushBack(DelayTime::create(.5));

        //頰を赤らめる
        auto cheek = createSpriteToCenter("sup_69_cheek.png", true, pig, true);
        actions.pushBack(createSoundAction("pop.mp3"));
        actions.pushBack(TargetedAction::create(cheek, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(1.5));
        
        //鍵くれる
        auto anim_2 = createSpriteToCenter("sup_69_anim_2.png", true, pig, true);
        actions.pushBack(createSoundAction("pig.mp3"));
        actions.pushBack(createChangeAction(.3, .3, anim_0, anim_2));
        actions.pushBack(DelayTime::create(1));
        
        actions.pushBack(correctSoundAction());
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark ガスバーナー使用
    else if (key=="gasburner") {
        actions.pushBack(useItemSoundAction());

        //ガスバーナー表示
        auto burner = createSpriteToCenter("sup_71_firegun.png", true, parent, true);
        burner->setLocalZOrder(2);
        actions.pushBack(setItemAction(parent, burner));
        //actions.pushBack(DelayTime::create(1));
        
        //Fireパーティクルを表示
        float duration = 4;
        auto fire = CallFunc::create([burner, duration]{
            Common::playSE("fire.mp3");
          
                        auto particle=ParticleSystemQuad::create("fire.plist");
            particle->setTexture(Sprite::create("particle_fire.png")->getTexture());
            
            particle->setAutoRemoveOnFinish(true);
            particle->setDuration(duration);
            particle->setLife(.6);
            particle->setLifeVar(0);
            particle->setPosition(burner->getContentSize().width*.688,burner->getContentSize().height*.442);
            particle->setPosVar(Vec2::ZERO);
            particle->setSpeed(burner->getContentSize().height);
            particle->setSpeedVar(particle->getSpeed()*.02);
            particle->setGravity(Vec2(burner->getContentSize().width, -burner->getContentSize().height));
            particle->setTotalParticles(200);
            particle->setAngle(145);
            particle->setAngleVar(10);
            particle->setStartSize(burner->getContentSize().width*.04);
            particle->setStartSizeVar(burner->getContentSize().width*.02);
            particle->setEndSize(burner->getContentSize().width*.08);
            particle->setEndSizeVar(burner->getContentSize().width*.02);
            
            burner->addChild(particle);
        });
        actions.pushBack(fire);
        
        //銃を揺らす&氷溶ける同時実行。
        int repeat = 20;
        auto swing = swingAction(Vec2(0, parent->getContentSize().height*.02), duration/2/repeat, burner, 2, false, repeat);
        
        //氷溶ける&煙
        float delayduration = 1;
        auto smoke = CallFunc::create([parent](){
            auto particle=ParticleSystemQuad::create("smoke.plist");
            particle->setName("smoke");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width/2, parent->getContentSize().height*.5);
            particle->setPosVar(Vec2(parent->getContentSize().width*.2, 0));
            particle->setSpeed(parent->getContentSize().height*.1);
            particle->setTotalParticles(50);
            particle->setAngle(90);
            particle->setAngleVar(10);
            particle->setLife(2);
            particle->setStartSize(parent->getContentSize().width*.1);
            particle->setEndSize(parent->getContentSize().width*.2);
            particle->setLocalZOrder(2);
            parent->addChild(particle);
        });
        auto ice = parent->getChildByName<Sprite*>("sup_71_pond_0.png");
        auto seq_melt = Sequence::create(DelayTime::create(delayduration), smoke, TargetedAction::create(ice, FadeOut::create(duration-delayduration)), NULL);
        actions.pushBack(Spawn::create(swing, seq_melt, NULL));
        
        //煙を消す
        auto endSmoke = CallFunc::create([parent](){
            auto smoke = parent->getChildByName<ParticleSystemQuad*>("smoke");
            smoke->setDuration(.5);
        });
        actions.pushBack(endSmoke);
        actions.pushBack(DelayTime::create(.5));
        
        //銃消す
        auto spawn_burner = Spawn::create(MoveBy::create(.5, Vec2(0, parent->getContentSize().height*.05)), FadeOut::create(.5), NULL);
        actions.pushBack(TargetedAction::create(burner, spawn_burner));
        
        //終わり
        actions.pushBack(correctSoundAction());
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 網使用
    else if (key=="net") {
        actions.pushBack(useItemSoundAction());
        
        auto net = createAnchorSprite("sup_71_net.png", .753, .656, true, parent, true);
        auto banana = createSpriteToCenter("sup_71_net_banana.png", true, net, true);
        
        auto cover = createSpriteToCenter("sup_71_watercover.png", true, parent, true);
        cover->setOpacity(255/2);
        auto waterBanana = parent->getChildByName<Sprite*>("sup_71_banana.png");
        waterBanana->setLocalZOrder(2);
        auto bananaShadow = parent->getChildByName<Sprite*>("sup_71_banana_shadow.png");

        //ネットを表示
        actions.pushBack(setItemAction(parent, net));
        
        //ネット助走
        auto move = EaseInOut::create(MoveBy::create(.3, Vec2(parent->getContentSize().width*.1, parent->getContentSize().height*.1)), 2) ;
        actions.pushBack(TargetedAction::create(net, move));
        
        //ネットですくう
        float netDuration = .7;
        auto jump = TargetedAction::create(net, JumpTo::create(netDuration, Vec2(parent->getContentSize().width*.255, parent->getContentSize().height*.644), -parent->getContentSize().height*.15, 1));
        
        auto bananaSeq = Sequence::create(DelayTime::create(netDuration/2),
                                          createSoundAction("bashaa.mp3"),
                                          createChangeAction(.2, .2, {bananaShadow, waterBanana}, {banana}), NULL);
        
        actions.pushBack(Spawn::create(createSoundAction("swing_0.mp3"), jump, bananaSeq, NULL));
        actions.pushBack(DelayTime::create(.6));
        
        //ネット上に移動しながらフェードアウト
        auto fadeoutSpawn = Spawn::create(MoveBy::create(.5, Vec2(0, parent->getContentSize().height*.1)), FadeOut::create(.5), NULL);
        actions.pushBack(TargetedAction::create(net, fadeoutSpawn));
        actions.pushBack(DelayTime::create(.5));
        
        //終わり
        actions.pushBack(correctSoundAction());
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 囲炉裏にマッチ
    else if (key=="match_irori") {
        //アイテム使用
        actions.pushBack(useItemSoundAction());

        //炎パーティクル生成
        auto fireParticle = showFireParticle(parent);
        fireParticle->setTotalParticles(0);
        
        //マッチ着火
        int nowBack = DataManager::sharedManager()->getNowBack();
        actions.pushBack(matchAction(parent, nowBack, fireParticle));
        
        //ちょっと待機
        actions.pushBack(DelayTime::create(1));
        
        //湯気表示
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(30)) {
            actions.pushBack(boiledActions(parent));
        }
        
        actions.pushBack(correctSoundAction());
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark やかんを置く
    else if (key=="yakan") {
        actions.pushBack(useItemSoundAction());
        
        auto yakan = createSpriteToCenter("sup_31_kettle.png", true, parent, true);
        yakan->setLocalZOrder(2);
        auto huta = createSpriteToCenter("sup_31_huta.png", true, parent, true);
        huta->setLocalZOrder(2);
        actions.pushBack(Spawn::create(setItemAction(parent, yakan),
                                       setItemAction(parent, huta), NULL));
        actions.pushBack(DelayTime::create(.8));
        
        //スーザン・ボイル
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(29)) {//火がついていたら
            actions.pushBack(boiledActions(parent));
        }
        
        actions.pushBack(correctSoundAction());
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 顔に落書き
    else if (key=="hude") {
        //筆をあげる
        actions.pushBack(useItemSoundAction());
        auto hude_0 = createSpriteToCenter("sup_24_hude_0.png", true, parent, true);
        actions.pushBack(giveAction(hude_0, .5, .9, .5));
        actions.pushBack(DelayTime::create(.5));
        
        //筆受け取る
        auto pig_0_1 = parent->getChildByName<Sprite*>("sup_24_pig_0_1.png");
        auto pig_0_2 = parent->getChildByName<Sprite*>("sup_24_pig_0_2.png");
        auto hude_1 = createSpriteToCenter("sup_24_hude_1.png", true, pig_0_2, true);
        actions.pushBack(TargetedAction::create(hude_1, FadeIn::create(.5)));
        actions.pushBack(DelayTime::create(.5));
        
        //豚向かい合う
        auto pig_1_1 = createAnchorSprite("sup_24_pig_1_1.png", .287, .083, true, parent, true);
        auto pig_1_2 = createAnchorSprite("sup_24_pig_1_2.png", .725, 0, true, parent, true);
        actions.pushBack(createChangeAction(.5, .5, {pig_0_1, pig_0_2}, {pig_1_1, pig_1_2}, "pop_4.mp3"));
        actions.pushBack(DelayTime::create(.5));
        
        //豚がボヨン
        auto bounceSpawn = Spawn::create(createBounceAction(pig_1_1, .25, .05),
                                         createBounceAction(pig_1_2, .25, .05), NULL);
        actions.pushBack(createSoundAction("pop_5.mp3"));
        actions.pushBack(bounceSpawn);
        actions.pushBack(DelayTime::create(.3));

        //豚筆で書き始める
        auto pig_2_1 = createSpriteToCenter("sup_24_pig_2_1.png", true, parent, true);
        auto pig_2_2 = createSpriteToCenter("sup_24_pig_2_2.png", true, parent, true);
        auto pig_3_1 = createSpriteToCenter("sup_24_pig_3_1.png", true, parent, true);
        auto pig_3_2 = createSpriteToCenter("sup_24_pig_3_2.png", true, parent, true);
        actions.pushBack(createChangeAction(.5, .5, {pig_1_1, pig_1_2}, {pig_2_1, pig_2_2}));
        actions.pushBack(DelayTime::create(.3));
        
        //シャカシャカ書く
        actions.pushBack(createSoundAction("pig.mp3"));

        Vector<Sprite*> paints = {
            createSpriteToCenter("sup_24_paint_0.png", true, parent, true),
            createSpriteToCenter("sup_24_paint_1.png", true, parent, true),
            createSpriteToCenter("sup_24_paint_2.png", true, parent, true)};

        for (int i = 0; i < paints.size(); i++) {
            actions.pushBack(createSoundAction("pen.mp3"));

            for (int l = 0; l < 8; l++) {
                actions.pushBack(createChangeAction(0, 0, {pig_2_1, pig_2_2}, {pig_3_1, pig_3_2}));
                actions.pushBack(DelayTime::create(.1));
                actions.pushBack(createChangeAction(0, 0, {pig_3_1, pig_3_2}, {pig_2_1, pig_2_2}));
                actions.pushBack(DelayTime::create(.1));
            }
            actions.pushBack(TargetedAction::create(paints.at(i), FadeIn::create(.1)));
        }
        actions.pushBack(DelayTime::create(.3));
       
        //正面に戻る
        Vector<Node*> beforePigSprites = {pig_2_1, pig_2_2};//paints;
        for (auto paint : paints) {
            beforePigSprites.pushBack(paint);
        }
        auto pig_paint = createSpriteToCenter("sup_24_pig_paint.png", true, parent, true);
        actions.pushBack(createSoundAction("pig.mp3"));
        actions.pushBack(createChangeAction(.4, .4, beforePigSprites, {pig_0_1, pig_0_2, pig_paint}));
        actions.pushBack(DelayTime::create(.8));

        //コールバック
        actions.pushBack(correctSoundAction());
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 笛の豚と筆の豚とネズミと猿のグニョン
    else if (key=="bounce") {
        
        int backNum = DataManager::sharedManager()->getNowBack();
        if (backNum == 53) {//笛豚
            auto pig = parent->getChildByName<Sprite*>("sup_53_pig_0.png");
            transformToAnchorSprite(parent, pig, Vec2(.5, .144));
            actions.pushBack(createSoundAction("pig.mp3"));
            actions.pushBack(createBounceAction(pig, .3, .1));
        }
        else if(backNum == 85){//ネズミ
            auto mouse = parent->getChildByName<Sprite*>("sup_85_rats_0.png");
            transformToAnchorSprite(parent, mouse, Vec2(.5, .194));
            actions.pushBack(createSoundAction("squirre.mp3"));
            actions.pushBack(createBounceAction(mouse, .3, .07));
        }
        else if (backNum == 83) {//猿
            auto monkey = parent->getChildByName<Sprite*>("sup_83_monkey.png");
            transformToAnchorSprite(parent, monkey, Vec2(.5, .314));
            actions.pushBack(createSoundAction("monkey.mp3"));
            actions.pushBack(createBounceAction(monkey, .3, .07));
        }
        else {//筆豚
            auto pig = parent->getChildByName<Sprite*>("sup_24_pig_0_2.png");
            transformToAnchorSprite(parent, pig, Vec2(.69, .082));
            actions.pushBack(createSoundAction("pig.mp3"));
            actions.pushBack(createBounceAction(pig, .3, .07));
        }
        
        //コールバック
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
}

#pragma mark - Item
void TwoThousandNineteenActionManagerManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造 addする際はcreateSpriteOnClipを使用するとよい
    Vector<FiniteTimeAction*>actions;
#pragma mark 電池
    if (key.compare(0,7,"battery")==0) {
        Common::playSE("p.mp3");
        
        auto index=atoi(key.substr(8,1).c_str());
        
        auto battery=createSpriteToCenter(StringUtils::format("battery_remocon_%d.png",index), true, parent->itemImage, true);
        
        //move
        actions.pushBack(TargetedAction::create(battery, Sequence::create(FadeIn::create(1),
                                                                          createSoundAction("kacha.mp3"),
                                                                          NULL)));

        actions.pushBack(DelayTime::create(1));
        //call
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 小槌
    else if (key=="gavel") {
        Common::playSE("p.mp3");
        
        SupplementsManager::getInstance()->transformToAnchorSprite(parent->backImage, parent->itemImage, Vec2(.55, .35));
        
        auto particle=CallFunc::create([parent]{
            auto particle=ParticleSystemQuad::create("particle_texture.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->resetSystem();
            
            particle->setDuration(.2);
            particle->setTotalParticles(100);
            particle->setTexture(Sprite::create("kira.png")->getTexture());
            particle->setStartSize(parent->getContentSize().width*.1);
            particle->setStartSizeVar(parent->getContentSize().width*.05);
            particle->setEndSize(particle->getStartSize());
            particle->setSpeed(parent->backImage->getContentSize().height*.25);
            particle->setSpeedVar(parent->backImage->getContentSize().height*.1);
            particle->setPosition(parent->backImage->getContentSize().width*.3,parent->backImage->getContentSize().height*.5);
            particle->setPosVar(Vec2(0, 0));
            particle->setLife(1);
            particle->setLifeVar(.2);
            particle->setStartColor(Color4F::YELLOW);
            particle->setStartColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 150));
            particle->setEndColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 0));
            parent->clip->addChild(particle);
        });
        
        
        
        //shake
        actions.pushBack(TargetedAction::create(parent->itemImage, Sequence::create(MoveBy::create(.5, Vec2(parent->backImage->getContentSize().width*.1, 0)),
                                                                                    DelayTime::create(.5),
                                                                                    Repeat::create(Sequence::create(createSoundAction("handbell.mp3"),
                                                                                                                    swingAction(-20, .3, parent->itemImage, 1.5, false),
                                                                                                                    particle,
                                                                                                                    DelayTime::create(.5)
                                                                                                                    , NULL), 2),
                                                                                    NULL)));
        
        actions.pushBack(DelayTime::create(1));
        
        //call
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 年賀状
    else if (key=="stamp") {
        Common::playSE("p.mp3");
        
        auto stamp=createAnchorSprite("stamp_anim.png", .29, .8, true, parent->itemImage, true);
        auto scale_original=stamp->getScale();
        stamp->setScale(scale_original*1.5);
        
        //move
        actions.pushBack(TargetedAction::create(stamp, Sequence::create(FadeIn::create(1),
                                                                        ScaleTo::create(.5, scale_original),
                                                                        createSoundAction("pop.mp3"), NULL)));
        
        
        actions.pushBack(DelayTime::create(1));
        
        //call
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark 福袋
    else if (key=="bag") {
        Common::playSE("p.mp3");
        
        auto scissors=createAnchorSprite("bag_scissors.png", .29, .8, true, parent->itemImage, true);
        
        //seq
        actions.pushBack(TargetedAction::create(scissors, Sequence::create(FadeIn::create(1),
                                                                        createSoundAction("chokichoki.mp3"), NULL)));
        
        actions.pushBack(DelayTime::create(.5));
        
        //change
        auto bag=createSpriteToCenter("bag.png", true, parent->backImage,true);
        auto tape=createSpriteToCenter("bag_tape.png", false, bag,true);
        actions.pushBack(createChangeAction(1, .7, parent->itemImage, bag, ""));
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(TargetedAction::create(tape, FadeOut::create(.5)));

        auto bag_open=createSpriteToCenter("bag_open.png", true, parent->backImage,true);
        actions.pushBack(createChangeAction(1, .7, bag, bag_open, "pop.mp3"));
        actions.pushBack(DelayTime::create(1));

        
        //call
        auto call=CallFunc::create([callback](){
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
}


#pragma mark - Custom
void TwoThousandNineteenActionManagerManager::customAction(std::string key, cocos2d::Node *parent)
{
    if (key==CustomAction_Title) {
        clearParticleAction(parent);
    }
}

void TwoThousandNineteenActionManagerManager::clearParticleAction(cocos2d::Node *parent)
{
    auto particle=ParticleSystemQuad::create("particle_snow.plist");
    particle->setAutoRemoveOnFinish(true);
    particle->resetSystem();
    particle->setDuration(-1);
    particle->setLife(10);
    particle->setLifeVar(0);
    particle->setTotalParticles(100);
    particle->setStartSize(parent->getContentSize().width*.025);
    particle->setStartSizeVar(parent->getContentSize().width*.01);
    particle->setEndSize(particle->getStartSize());
    particle->setSpeed(parent->getContentSize().height*.08);
    particle->setSpeedVar(parent->getContentSize().height*.02);
    particle->setStartColor(Color4F(1, 1, 1, 1));
    particle->setStartColorVar(Color4F(0, 0, 0, .2));
    particle->setPosition(parent->getContentSize().width/2,parent->getContentSize().height*.9+particle->getStartSize());
    particle->setPosVar(Vec2(parent->getContentSize().width*.6, parent->getContentSize().height*.1));
    parent->addChild(particle);
}

#pragma mark - Show Answer
void TwoThousandNineteenActionManagerManager::showAnswerAction(std::string key, cocos2d::Node *parent)
{
    
}

#pragma mark - Story
void TwoThousandNineteenActionManagerManager::storyAction(std::string key, cocos2d::Node *parent, const onFinished &callback)
{
    Vector<FiniteTimeAction*>actions;
    if (key=="givecard") {
        auto mainSpr=((GameScene*)Director::getInstance()->getRunningScene()->getChildren().at(1))->mainSprite;
        auto card=createAnchorSprite("sup_29_card.png",.5,.5, true, mainSpr,true);
        auto card_1=createAnchorSprite("sup_29_card_1.png",.5,.5, true, mainSpr,true);
        auto back_43=createSpriteToCenter("back_43.png", true, mainSpr,true);
        
        //Common::playSE("pig.mp3");
        actions.pushBack(DelayTime::create(1));
        actions.pushBack(giveAction(card, 1, .8, true,"pop.mp3"));
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(giveAction(card_1, 1, 1.5, true,"pig.mp3"));
        actions.pushBack(TargetedAction::create(back_43, FadeIn::create(1)));
        
        
        auto call=CallFunc::create([callback]{
            callback(true);;
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else{
        callback(true);
    }
}

#pragma mark - Private
ParticleSystemQuad* TwoThousandNineteenActionManagerManager::showFireParticle(cocos2d::Node *parent,int backID)
{
    auto clipName = "clip";
    auto fireName = "fire";
    auto clipNode = parent->getChildByName<ClippingNode*>(clipName);
    
    if (clipNode != NULL) {
        auto particle = clipNode->getChildByName<ParticleSystemQuad*>(fireName);
        if (particle != NULL) {//あるなら貼らずに返す。
            log("炎パーティクルあるから返すよ。");
            return particle;
        }
    }
    
    auto clipImageName = StringUtils::format("sup_%d_clip.png",backID);
    auto stencil=createSpriteToCenter(clipImageName, false, parent);
    auto clip=ClippingNode::create(stencil);
    clip->setName(clipName);
    clip->setInverted(false);
    clip->setAlphaThreshold(0.0f);
    
    auto particle=ParticleSystemQuad::create("fire.plist");
    particle->setName(fireName);
    particle->setPositionType(ParticleSystem::PositionType::RELATIVE);
    particle->setAutoRemoveOnFinish(true);
    particle->setPosition(parent->getContentSize().width*.5, parent->getContentSize().height*.55);
    particle->setTotalParticles(150);
    particle->setStartColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 150));
    particle->setPosVar(Vec2(parent->getContentSize().width*.075, parent->getContentSize().height*.05));
    particle->setStartSize(parent->getContentSize().width*.075);
    particle->setSpeed(parent->getContentSize().height*.08);
    
    clip->addChild(particle);
    parent->addChild(clip);
    
    return particle;
}

ParticleSystemQuad* TwoThousandNineteenActionManagerManager::showFireParticle(cocos2d::Node *parent)
{
    auto fireParticle = ParticleSystemQuad::create("fire.plist");
    fireParticle->setPositionType(ParticleSystem::PositionType::RELATIVE);
    fireParticle->setAutoRemoveOnFinish(true);
    fireParticle->setPosition(parent->getContentSize().width*.5, parent->getContentSize().height*.355);
    fireParticle->setStartColor(Color4F(fireParticle->getStartColor().r, fireParticle->getStartColor().g, fireParticle->getStartColor().b, 150));
    fireParticle->setPosVar(Vec2(parent->getContentSize().width*.045, parent->getContentSize().height*.05));
    fireParticle->setStartSize(parent->getContentSize().width*.06);
    fireParticle->setSpeed(parent->getContentSize().height*.08);
    fireParticle->setLife(.8);
    fireParticle->setTotalParticles(0);
    
    parent->addChild(fireParticle);
    
    return fireParticle;
}

FiniteTimeAction* TwoThousandNineteenActionManagerManager::boiledActions(Node *parent)
{
    Vector<FiniteTimeAction*> boiledActions;
    //蓋カタカタ
    auto lid = parent->getChildByName<Sprite*>("sup_31_huta.png");
    transformToAnchorSprite(parent, lid, Vec2(.522, .506));
    int rotate = 7;
    float duration = .1;
    boiledActions.pushBack(createSoundAction("gatagata.mp3"));
    for (int i=0; i<5; i++) {
        boiledActions.pushBack(TargetedAction::create(lid, RotateTo::create(duration, rotate)));
        boiledActions.pushBack(TargetedAction::create(lid, RotateTo::create(duration, -1*rotate)));
    }
    boiledActions.pushBack(TargetedAction::create(lid, RotateTo::create(duration, 0)));
    boiledActions.pushBack(DelayTime::create(.8));
    
    //湯気出る
    boiledActions.pushBack(createSoundAction("spray.mp3"));
    auto yugeCall = CallFunc::create([parent](){
        float xPosRatio = .39;
        float yPosRatio = .473;
        float startSize = parent->getContentSize().width*.025;
        float speed = parent->getContentSize().height*.35;
        
        auto particle=ParticleSystemQuad::create("smoke.plist");
        particle->setAutoRemoveOnFinish(true);
        particle->setPosition(parent->getContentSize().width*xPosRatio, parent->getContentSize().height*yPosRatio);
        particle->setPosVar(Vec2(0, 0));
        particle->setSpeed(speed);
        particle->setTotalParticles(200);
        particle->setAngle(150);
        particle->setAngleVar(10);
        particle->setLife(2);
        particle->setStartSize(startSize);
        particle->setEndSize(particle->getStartSize()*3);
        particle->setLocalZOrder(1);
        particle->setDuration(.8);
        parent->addChild(particle);
    });
    boiledActions.pushBack(yugeCall);
    boiledActions.pushBack(DelayTime::create(1.5));
    
    return Sequence::create(boiledActions);
}
