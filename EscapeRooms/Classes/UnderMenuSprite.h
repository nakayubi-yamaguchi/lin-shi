//
//  TopMenuSprite.h
//  Kebab
//
//  Created by 成田凌平 on 2016/12/17.
//
// 移動操作用のメニューを管理

#ifndef __Kebab__UnderMenuSprite__
#define __Kebab__UnderMenuSprite__

#include "cocos2d.h"


USING_NS_CC;
class UnderMenuSprite:public Sprite
{
public:
    static UnderMenuSprite* create(Rect rect);
    bool init(Rect rect);
    Vector<MenuItem*> arrows;//矢印保持

    void loadData();
    void reloadArrows();
    
    CC_SYNTHESIZE(bool, enable, Enable);

private:
    ValueMap arrowMap;//矢印の遷移先データ
    
    void createArrows();
    void showPromoteHintLayer();
    ~UnderMenuSprite();
};

#endif /* defined(__Kebab__UnderMenuSprite__) */
