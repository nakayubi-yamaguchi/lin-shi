//
//  EscapeStageSprite.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/07/04.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____
#define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____

#include "cocos2d.h"
USING_NS_CC;

//各タイトルのリソースに含まれるアトラス画像を使用する場合はこのスプライトを使用する。

class EscapeStageSprite : public Sprite
{
public:
    static EscapeStageSprite* createWithSpriteFileName(const std::string &spriteFileName);

    virtual bool init(const std::string &spriteFileName);
    
    /**ローカルテスト用に画像キャッシュ*/
    static void prepareLocalTest();

private:

};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
