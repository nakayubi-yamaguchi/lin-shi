//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__PigHat2ActionManager__
#define __EscapeContainer__PigHat2ActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class PigHat2ActionManager:public CustomActionManager
{
public:
    static PigHat2ActionManager* manager;
    static PigHat2ActionManager* getInstance();
    
    int getDefaultBackNum();
    
    void openingAction(Node*node,const onFinished& callback);
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    
    void customAction(std::string key,Node*parent);
    
private:
    Spawn* bellAction(Node*parent,Node*bell,int index);

};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
