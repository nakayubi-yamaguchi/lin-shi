//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__WeddingActionManager__
#define __EscapeContainer__WeddingActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class WeddingActionManager:public CustomActionManager
{
public:
    static WeddingActionManager* manager;
    static WeddingActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void dragAction(std::string key,Node*parent,Vec2 pos);
    
    void candleAction(Node*parent);
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
