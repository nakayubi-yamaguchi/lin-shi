//
//  TopMenuSprite.h
//  Kebab
//
//  Created by 成田凌平 on 2016/12/17.
//
//ヒントと取得アイテムの使用ボタン管理

#ifndef __Kebab__TopMenuSprite__
#define __Kebab__TopMenuSprite__

#include "cocos2d.h"

#define MaxItem 6

USING_NS_CC;

class TopMenuSprite:public Sprite
{
public:
    static TopMenuSprite* create(Rect rect);
    bool init(Rect rect);
    
    float menuSpace;
    float boxWidth;
    float menuWidth;
    
    CC_SYNTHESIZE(bool, enable, Enable);
private:
    Menu*itemsMenu;
    Label*minigameLabel;
    
    void createItemBoxes();
    void createMenuButton();
    void showHintLayer();
    void adaptMiniGame();
    void reloadMinigameLabel();
    void reloadItemBox();
    
    //通知
    void addNotification();

    ~TopMenuSprite();
};

#endif /* defined(__Kebab__TopMenuSprite__) */
