//
//  SchoolFesActionManager.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/11/29.
//
//

#ifndef __EscapeContainer__SchoolFesActionManager__
#define __EscapeContainer__SchoolFesActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class SchoolFesActionManager:public CustomActionManager
{
public:
    static SchoolFesActionManager* manager;
    static SchoolFesActionManager* getInstance();
    int moguraIndex;
    int moguraCorrectCount;//正解通りにもぐらを叩いた回数。
    std::vector<int> moguraAnswers;
    
    //over ride
    int getDefaultBackNum();
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void touchBeganAction(std::string key,Node*parent);
    
    void dragAction(std::string key,Node*parent,Vec2 pos);
    
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    
    void customAction(std::string key,Node*parent);
    
private:
    FiniteTimeAction* showHinotamaParticle(Node* parent, int backID);
    std::vector<Vec2> getRokuroRousokuPoses(int backID);
    ParticleSystemQuad* showFireParticle(cocos2d::Node *parent, int backID, int num);
    FiniteTimeAction* rokuroActions(Node* parent, std::vector<int> answers);

};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
