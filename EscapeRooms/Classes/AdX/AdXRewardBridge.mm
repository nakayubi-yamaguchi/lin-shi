//
//  AdXRewardBridge.mm
//  FluctAdXxFluctSDK-mobile
//
//  Created by RyoKosuge on 2019/03/29.
//

#import "AdXRewardBridge.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface AdXRewardCallback: NSObject<GADRewardBasedVideoAdDelegate>
@property (nonatomic) bool completeMovie;
@property (nonatomic) int loadingCount;
@property (nonatomic) AdXRewardBridgeDelegate *delegate_id;
@end

@implementation AdXRewardCallback

#pragma mark- リワードデリゲート
- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd didRewardUserWithReward:(GADAdReward *)reward
{
    NSString *rewardMessage =
    [NSString stringWithFormat:@"Reward received with currency %@ , amount %lf",
     reward.type,
     [reward.amount doubleValue]];
    
    self.completeMovie = true;
    AdXRewardBridge::logOfBridge([[NSString stringWithFormat:@"Reward based video ad%@", rewardMessage] UTF8String]);
}

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    AdXRewardBridge::logOfBridge("Reward based video ad is received.");
    self.loadingCount = 0;
}

- (void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    AdXRewardBridge::logOfBridge("Opened reward based video ad.");
}

- (void)rewardBasedVideoAdDidStartPlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    AdXRewardBridge::logOfBridge("Reward based video ad started playing.");
    
    if (self.delegate_id){
        self.delegate_id->didStartAdXReward();
    }
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    AdXRewardBridge::logOfBridge("Reward based video ad is closed.");

    if (self.delegate_id) {
        self.delegate_id->didCloseAdXReward(self.completeMovie);
    }

    self.completeMovie = false;
}

- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    AdXRewardBridge::logOfBridge("Reward based video ad will leave application.");
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error {
    NSLog(@"AdXのローディングに失敗しました。%@",error);
    if (self.delegate_id) {
        self.delegate_id->didFailAdXReward((int)error.code);
    }
}

#pragma mark- 再読み込み用
-(void)reloadAdX
{
    AdXRewardBridge::logOfBridge("AdXのリワードを再読み込みします。");
    _loadingCount = 0;
}

#pragma mark- 解放
-(void)dealloc{
    NSString *log = [NSString stringWithFormat:@"%s", __func__];
    AdXRewardBridge::logOfBridge([log UTF8String]);
    [super dealloc];
}

@end

#pragma mark - AdXリワードbridge
AdXRewardCallback *adxRewardCallback = [AdXRewardCallback new];

void AdXRewardBridge::loadReward()
{
    logOfBridge("動画広告をローディング");
    DFPRequest *request = [DFPRequest request];
    auto adUnitID = DebugMode == 0 ? MOVIE_REWARD_ADX_AD_UNIT_ID : MOVIE_REWARD_ADX_SAMPLE_AD_UNIT_ID;
    // auto adUnitID = MOVIE_REWARD_ADX_SAMPLE_UNITID;
    GADRewardBasedVideoAd.sharedInstance.delegate = adxRewardCallback;
    [[GADRewardBasedVideoAd sharedInstance] loadRequest:request
                                           withAdUnitID:[NSString stringWithUTF8String:adUnitID]];
}

bool AdXRewardBridge::isReady()
{
    return GADRewardBasedVideoAd.sharedInstance.isReady;
}

void AdXRewardBridge::show()
{
    if (isReady()) {
        UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        
        NSLog(@"[%@]の広告を表示します",[[GADRewardBasedVideoAd sharedInstance] adNetworkClassName]);
        
        if (rootViewController)//ルートビューコントローラーがあれば。
            [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:rootViewController];
    } else {
        logOfBridge("ローディングが完了していない。");
    }
}

void AdXRewardBridge::logOfBridge(std::string text)
{
    NSLog(@"[%@]%s", [NSString stringWithUTF8String:LOG_ADX_REWARD], text.c_str());
}

void AdXRewardBridge::setDelegate(AdXRewardBridgeDelegate *delegate)
{
    if (adxRewardCallback) {

        if (adxRewardCallback.delegate_id) {
            adxRewardCallback.delegate_id = nil;
        }

        adxRewardCallback.delegate_id = delegate;
    }
}
