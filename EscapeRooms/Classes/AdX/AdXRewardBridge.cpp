//
//  AdXRewardBridge.cpp
//  FluctAdXxFluctSDK-mobile
//
//  Created by RyoKosuge on 2019/03/29.
//

#include "AdXRewardBridge.h"
#include <jni.h>
#include "platform/android/jni/JniHelper.h"

#define ADX_REWARD_CLASS_NAME "org/cocos2dx/cpp/AdXRewardBridge"

using namespace cocos2d;

static AdXRewardBridgeDelegate* sAdXRewardCallback = NULL;

void AdXRewardBridge::setDelegate(AdXRewardBridgeDelegate* delegate) {
    sAdXRewardCallback = delegate;
}

void AdXRewardBridge::loadReward() {
    cocos2d::JniMethodInfo t;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, ADX_REWARD_CLASS_NAME, "loadAdXReward",
                                                "(Ljava/lang/String;)V")) {
        auto adUnitID =
                DebugMode == 0 ? MOVIE_REWARD_ADX_AD_UNIT_ID : MOVIE_REWARD_ADX_SAMPLE_AD_UNIT_ID;
        jstring utfAdUnitID = t.env->NewStringUTF(adUnitID);
        t.env->CallStaticVoidMethod(t.classID, t.methodID, utfAdUnitID);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(utfAdUnitID);
    }
}

bool AdXRewardBridge::isReady() {
    cocos2d::JniMethodInfo t;
    bool isPrepare = false;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, ADX_REWARD_CLASS_NAME, "isReady", "()Z")) {
        jboolean ready = (jboolean) t.env->CallStaticBooleanMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
        isPrepare = ready;
    }
    log("動画広告の準備状況%d", isPrepare);
    return isPrepare;
}

void AdXRewardBridge::show() {
    cocos2d::JniMethodInfo t;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, ADX_REWARD_CLASS_NAME, "show", "()V")) {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}

#ifdef __cplusplus
extern "C"
{
#endif
JNIEXPORT void Java_org_cocos2dx_cpp_AdXRewardBridge_didStart(JNIEnv* env, jobject thiz) {
    if (sAdXRewardCallback != nullptr) {
        sAdXRewardCallback->didStartAdXReward();
    }
}

JNIEXPORT void
Java_org_cocos2dx_cpp_AdXRewardBridge_didClose(JNIEnv* env, jobject thiz, jboolean completed) {
    if (sAdXRewardCallback != nullptr) {
        sAdXRewardCallback->didCloseAdXReward(completed);
    }
}

JNIEXPORT void
Java_org_cocos2dx_cpp_AdXRewardBridge_didFail(JNIEnv* env, jobject thiz, jint errorCode) {
    if (sAdXRewardCallback != nullptr) {
        sAdXRewardCallback->didFailAdXReward(errorCode);
    }
}

#ifdef __cplusplus
}
#endif
