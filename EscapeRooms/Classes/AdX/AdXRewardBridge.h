//
//  AdXRewardBridge.h
//  FluctAdXxFluctSDK
//
//  Created by RyoKosuge on 2019/03/29.
//

#ifndef AdXRewardBridge_h
#define AdXRewardBridge_h

#include "cocos2d.h"
#include "../Utils/AdConstants.h"
#include "../Utils/Common.h"


#define LOG_ADX_REWARD "AdXのリワード広告"

class AdXRewardBridgeDelegate
{
public:
    virtual void didStartAdXReward(){};
    virtual void didCloseAdXReward(bool completed){};
    virtual void didFailAdXReward(int errorCode){};
};

class AdXRewardBridge
{
public:
    
    static void loadReward();
    static bool isReady();
    static void show();
    
    static void setDelegate(AdXRewardBridgeDelegate* delegate);
    static void logOfBridge(std::string text);
};

#endif /* AdXRewardBridge_h */
