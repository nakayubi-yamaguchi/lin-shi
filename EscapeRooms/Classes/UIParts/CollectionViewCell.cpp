//
//  CollectionViewCell.cpp
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/09/26.
//
//

#include "CollectionViewCell.h"
CollectionViewCell* CollectionViewCell::create(cocos2d::Node *normalSprite, cocos2d::Node *selectedSprite)
{
    CollectionViewCell *pRet = new CollectionViewCell();
    if (pRet && pRet->init(normalSprite, selectedSprite))
    {
        //log("セルのクリエイト");
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

bool CollectionViewCell::init(cocos2d::Node *normalSprite, cocos2d::Node *selectedSprite)
{
    return initWithImage(normalSprite, selectedSprite, nullptr);;
}

ssize_t CollectionViewCell::getIdx() const
{
    return _idx;
}

void CollectionViewCell::setIdx(ssize_t idx)
{
    _idx = idx;
}
