//
//  Spinner.cpp
//  Onigokko
//
//  Created by yamaguchinarita on 2016/08/15.
//
//

#include "Spinner.h"
#include "../Utils/Common.h"
#include "MenuItemScale.h"

using namespace cocos2d;

Spinner::Spinner():m_orgCallback(NULL),m_size(0,0), m_eachItemSize(0,0),m_menuItems(NULL), m_counterLabel(NULL),m_count(0),m_minCount(0),m_maxCount(0),m_onReachLimitCallback(NULL),m_unit(0),m_pressedCount(0)
{
}

Spinner::~Spinner()
{
    log("個数選択UIが解放されました。");
}

Spinner * Spinner::create(Size size, const cocos2d::ccMenuCallback &callback)
{
    Spinner *pRet = new Spinner();
    if (pRet && pRet->init(size,callback))
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

bool Spinner::init(Size size, const cocos2d::ccMenuCallback &callback)
{
    //初期値を入力
    m_orgCallback = callback;//コールバックを保持
    m_size = size;
    m_eachItemSize = Size(size.width/3, size.height);
    m_count = 0;
    m_maxCount = 99;
    m_minCount = 0;
    m_unit = 1;
    setCascadeOpacityEnabled(true);
    
    m_grayColor = Color3B(240, 240, 240);
    m_imageColor = Common::getColorFromHex(MyColor);
    
    m_menuItems = createMenuItems();
    
    if (!Menu::initWithArray(m_menuItems)) {
        return false;
    }
    alignItemsHorizontallyWithPadding(m_eachItemSize.width);//横に整列させる
    
    //中央のラベル
    createCounterLabel();
    
    return true;
}

int Spinner::getCount()
{
    return m_count;
}

void Spinner::setCount(int var)
{
    m_count =  MIN(MAX(var, m_minCount), m_maxCount) ;//0〜99の範囲
    
    //テキストを更新
    auto text = StringUtils::format("%d",m_count);
    m_counterLabel->setString(text);
    
    //コールバック
    if (m_count == m_maxCount) {
        if (m_onReachLimitCallback) {
            m_onReachLimitCallback(true, m_count);
        }
    }
    else if (m_count == m_minCount) {
        if (m_onReachLimitCallback) {
            m_onReachLimitCallback(false, m_count);
        }
    }
}

void Spinner::setUnit(int unit)
{
    m_unit = unit;
}

#pragma mark- プライベート
Vector<MenuItem*> Spinner::createMenuItems()
{
    Vector<MenuItem*> menuItems;
    
    std::vector<std::string> labelStrings = {"−","＋"};
    std::vector<std::string> fileNames = {"left.png","right.png"};
    int itemCount = (int)labelStrings.size();//+と-のボタン
    
    for (int i=0; i<itemCount; i++) {
        
        //スプライト生成
        auto normalImage=createSpriteWithLabel(fileNames.at(i) ,false, labelStrings.at(i));
        
        //メニューアイテムを生成、配列に追加
        auto menuItem = MenuItemScale::create(normalImage, normalImage, [this](Ref* sender){
            auto choiceButton = (MenuItemScale*)sender;
            this->calculateCount((SpinnerType)choiceButton->getTag());
        });
        menuItem->setTag(i);
        
        menuItem->setScale(m_eachItemSize.width/menuItem->getContentSize().width);

        menuItems.pushBack(menuItem);
    }
    
    return menuItems;
}

Sprite* Spinner::createSpriteWithLabel(std::string fileName, bool isSelectedType, std::string labelString)
{
    auto back = Sprite::createWithSpriteFrameName(fileName.c_str());
    back->setColor(m_grayColor);
    back->setCascadeOpacityEnabled(true);
    
    //+または-のラベル
    auto selectedLabel = Label::createWithTTF(labelString, HiraginoMaruFont,  MIN(back->getContentSize().width, back->getContentSize().height)/1.8);
    selectedLabel->setPosition(back->getContentSize()/2);
    //カラー設定
    if(!isSelectedType){//通常状態
        selectedLabel->setTextColor(Color4B(m_imageColor));
    }
    else{//選択状態
        selectedLabel->setTextColor(Color4B(m_grayColor));
    }
    
    back->addChild(selectedLabel);

    return back;
}

void Spinner::createCounterLabel()
{
    auto text = StringUtils::format("%d",m_count);
    m_counterLabel = Label::createWithTTF(text, HiraginoMaruFont, m_eachItemSize.height/1.8);
    
    m_counterLabel->setTextColor(Color4B(m_imageColor));
    
    m_counterLabel->setPosition(0, 0);
    
    addChild(m_counterLabel);
}

void Spinner::calculateCount(SpinnerType spinnerType)
{
    int beforeCount = getCount();
    if (spinnerType == SpinnerType_Minus) {//マイナスボタン
        setCount(m_count -= m_unit);
    }
    else{
        setCount(m_count += m_unit);
    }
    
    //コールバック
    if (/*!isPressed() &&*/beforeCount != getCount() && m_orgCallback) {//コメントアウトを外すと、長押し中はコールバックが呼ばれない。後々プロパティにしてもいい。値に変動がある時だけコールバック返す。
        m_orgCallback(this);
    }
}

void Spinner::startPressed()
{
    schedule(schedule_selector(Spinner::onTouchPressed), .1);
}

void Spinner::stopPressed()
{
    if (isPressed() && m_orgCallback) {
        log("長押し終わりのコールバックを返す");
        m_orgCallback(this);
    }
    
    m_pressedCount = 0;
    unschedule(schedule_selector(Spinner::onTouchPressed));
}

#pragma mark - パブリック
void Spinner::setMaxCount(int maxCount)
{
    m_maxCount = maxCount;
}

void Spinner::setMinCount(int minCount)
{
    m_minCount = minCount;
}

void Spinner::setOnReachedLimitCallback(const onReachedLimitCallback& callback)
{
    m_onReachLimitCallback = callback;
}

bool Spinner::isPressed()
{//.6秒以上なら長押し中
    return m_pressedCount > .6;
}

#pragma mark タッチ関連
bool Spinner::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    // 該当するMenuItemがなければfalseが返ってくる
    if (!Menu::onTouchBegan(touch, event)) {
        return false;
    }
    
    log("タッチが開始されました。");
    
    startPressed();
    
    return true;
}


void Spinner::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
    Menu::onTouchEnded(touch, event);
    
    log("タッチが終了されました。%d",(_selectedItem != NULL));

    // コールバック解除
    stopPressed();
}


void Spinner::onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *event)
{
    Menu::onTouchCancelled(touch, event);
    
    log("タッチがしかキャンセルされました。");

    stopPressed();
}

void Spinner::onTouchMoved(Touch* touch, Event* event)
{
    
    State before = _state;
    
    Menu::onTouchMoved(touch, event);
    
    log("タッチが移動されました。%d",(_selectedItem != NULL));

    if (before == _state) {
        return;
    }
    

    // ココに来るときは画面タップ状態で一旦領域から外れて戻ってきた時
    if (_state == State::WAITING) {
        // 領域からはずれたので解除
        stopPressed();
       
    } else if (_state == State::TRACKING_TOUCH) {
        // 領域に復帰したので、コールバックスタート
        startPressed();
    }
}


void Spinner::onTouchPressed(float delta)
{
    m_pressedCount += delta;
    
    
    if (_selectedItem == NULL) {//選択アイテムがない == タッチ終わっている。
        log("ロング終わり");
        
        stopPressed();
        
        return;
    }
    
    if (m_pressedCount > 1) {//長押しが1秒を超えた。
        if (_selectedItem != NULL) {//選択アイテムがある
            calculateCount((SpinnerType)_selectedItem->getTag());
        }
    }
}
