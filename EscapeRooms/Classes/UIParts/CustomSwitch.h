//
//  CustomSwitch.h
//  Onigokko
//
//  Created by yamaguchinarita on 2016/08/29.
//
//

#ifndef __Onigokko__CustomSwitch__
#define __Onigokko__CustomSwitch__

#include "cocos2d.h"

class CustomSwitch : public cocos2d::Menu
{
public:
    CustomSwitch();
    virtual ~CustomSwitch();

    static CustomSwitch* create(float height,const cocos2d::ccMenuCallback& callback);
    bool init(float height, const cocos2d::ccMenuCallback& callback);
    
    bool isOn();
    void setOn(bool isOn);
    void setOn(bool isOn, bool animated);
    
    void setOnColor(cocos2d::Color3B color);
    void setOffColor(cocos2d::Color3B color);
    
    cocos2d::Size getSwitchSize();
    
private:
    void initialSwitchMenuItem();
    void switchAnimation(bool animate);
    
    //このボタンをタップした時の処理
    void onTapThis(cocos2d::Ref* sender);
    
protected:
    cocos2d::ccMenuCallback m_orgCallback;
    cocos2d::Color3B m_onColor;
    cocos2d::Color3B m_offColor;
    cocos2d::Sprite* m_switch;
    cocos2d::MenuItemSprite* m_backMenuItem;
    bool m_isOn;
    cocos2d::Vec2 m_onPosition;
    cocos2d::Vec2 m_offPosition;
    float m_height;
};

#endif /* defined(__Onigokko__CustomSwitch__) */