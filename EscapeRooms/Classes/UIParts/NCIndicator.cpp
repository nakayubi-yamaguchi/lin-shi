//
//  NCIndicator.cpp
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/09/28.
//
//

#include "NCIndicator.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "Utils/Common.h"

#else
#include "Common.h"

#endif


using namespace cocos2d;

NCIndicator* NCIndicator::create(SwithInfo switchInfo)
{
    auto node=new NCIndicator();
    if (node&&node->init(switchInfo)) {
        node->autorelease();
        return node;
    }
    CC_SAFE_RELEASE(node);
    
    return node;
}

NCIndicator* NCIndicator::create(const char *fileName)
{
    auto node=new NCIndicator();
    if (node&&node->init(fileName)) {
        node->autorelease();
        return node;
    }
    CC_SAFE_RELEASE(node);
    
    return node;
}

bool NCIndicator::init()
{
    return init("loading.png");
}

bool NCIndicator::init(const char *fileName)
{
    if (!Sprite::initWithSpriteFrameName(fileName)) {
        return false;
    }
    m_indicatorType = IndicatorType_Rotate;
    schedule(schedule_selector(NCIndicator::rotateIndicator), .1);
    return true;
}

bool NCIndicator::init(SwithInfo switchInfo)
{
    //auto fileName = StringUtils::format("%s0.png", switchInfo.fileName.c_str());

    if (!Sprite::init()) {
        return false;
    }
    
    setTextureRect(Rect(0, 0, switchInfo.frameWidth, 0));
    setOpacity(0);
    m_indicatorType = IndicatorType_Switch;
    
    float widthAndSpaceRatio = 7;//ボタンの幅に対して、1/7がスペース
    float buttonWidth = widthAndSpaceRatio*switchInfo.frameWidth/((widthAndSpaceRatio + 1)* switchInfo.switchCount-1);

    float buttonSpace = buttonWidth/widthAndSpaceRatio;

    for (int i = 0; i<switchInfo.switchCount; i++) {
        auto loadingSp = Sprite::createWithSpriteFrameName(switchInfo.fileName);
        loadingSp->setScale(buttonWidth/loadingSp->getContentSize().width);
        loadingSp->setTag(i+1);
        
        //画像のサイズに応じて、thisの高さを決定する
        setTextureRect(Rect(0, 0, getTextureRect().size.width, loadingSp->getBoundingBox().size.height));
        loadingSp->setPosition(
                               getContentSize().width/2 - (buttonWidth + buttonSpace)/2*(switchInfo.switchCount-1) + (buttonWidth+buttonSpace)*i, getContentSize().height/2);

        if (i == 0) {
            loadingSp->setColor(Common::getColorFromHex(OrangeColor2));
        }
        else {
            loadingSp->setColor(Common::getColorFromHex(BlueColor));
        }
        
        addChild(loadingSp);
    }
    
    
    switchIndicator(0, switchInfo);
    return true;
}

void NCIndicator::rotateIndicator(float delta)
{
    setRotation(getRotation()+360.0/12.0);
}

void NCIndicator::switchIndicator(int switchCounter, SwithInfo switchInfo)
{
    int num = switchCounter%switchInfo.switchCount+1;
    
    auto orangeSp = getChildByTag(num);
    orangeSp->setColor(Common::getColorFromHex(OrangeColor2));
    
    auto blueSp = getChildByTag((num==1)? switchInfo.switchCount: num-1);//先頭をオレンジに変える場合は最後尾を青に変える
    blueSp->setColor(Common::getColorFromHex(BlueColor));
    
    auto delay = DelayTime::create(switchInfo.frameTime);
    auto callback = CallFunc::create([this, switchCounter, switchInfo](){
        int nextNum = switchCounter+1;
        
        this->switchIndicator(nextNum, switchInfo);
    });
    runAction(Sequence::create(delay, callback, NULL));
}

NCIndicator::~NCIndicator()
{
    log("インジケーターの解放");
}
