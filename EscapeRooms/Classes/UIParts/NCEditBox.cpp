//
//  CustomEditBox.cpp
//  Onigokko
//
//  Created by 成田凌平 on 2016/07/26.
//
//

#include "NCEditBox.h"
#include "ui/UIEditBox/UIEditBoxImpl.h"//editBoxImplを使用するために必要

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "Common.h"

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "Utils/Common.h"

#endif

using namespace cocos2d;

NCEditBox* NCEditBox::create(cocos2d::Size size, cocos2d::ui::Scale9Sprite *normalSprite)
{
    auto node=new (std::nothrow)NCEditBox();
    
    if (node&&node->init(size,normalSprite)) {
        node->autorelease();
    }
    
    return node;
}

bool NCEditBox::init(cocos2d::Size size, cocos2d::ui::Scale9Sprite *normalSprite)
{
    if (!EditBox::initWithSizeAndBackgroundSprite(size, normalSprite)) {
        return false;
    }
    
    //初期サイズを記憶
    setOriginalSize(size);
    
    //自身にデリゲートしてい
    setDelegate(this);
    
    return true;
}

#pragma mark- public
void NCEditBox::openKeyBoard()
{
    touchDownAction(NULL, cocos2d::ui::Widget::TouchEventType::ENDED);
}

void NCEditBox::closeKeyBoard()
{
    _editBoxImpl->closeKeyboard();
}

std::string NCEditBox::getText()
{
    return _editBoxImpl->getText();
}

#pragma mark- EditBox Delegate
void NCEditBox::editBoxEditingDidBegin(cocos2d::ui::EditBox *editBox)
{
    if (mDelegate) {
        mDelegate->customEditBoxEditingDidBegin();
    }
}

void NCEditBox::editBoxEditingDidEnd(cocos2d::ui::EditBox *editBox)
{
    //log("didEnd");
}

void NCEditBox::editBoxTextChanged(cocos2d::ui::EditBox *editBox, const std::string &text)
{//包括サイズチェック
    auto _fontSize = editBox->getContentSize().height*.8;
    float textHeight=MIN(getTextHeightWithFontPath(getContentSize().width, MyFont, _fontSize, editBox->getText()), _fontSize*4);//高さ上限をつける
    
    bool changed=false;//包括高さ変化フラグ
    float difference=0;//包括高さ増減量
    
    if (getTextHeight()!=textHeight&&getTextHeight()!=0&&textHeight>0)
    {//テキストサイズが変わった 0の時は除外
        changed=true;
        difference=textHeight-getTextHeight();
    }
    
    if (textHeight>0)
    {//記憶する
        setTextHeight(textHeight);
    }
    
    if (mDelegate)
    {//テキスト高さ変化量を教える
        mDelegate->customEditBoxTextChanged(changed,difference);
    }
}

void NCEditBox::editBoxReturn(cocos2d::ui::EditBox *editBox)
{    
    if (mDelegate){
        mDelegate->customEditBoxDidReturn();
    }
}

#pragma mark- IME Delegate
void NCEditBox::keyboardWillShow(IMEKeyboardNotificationInfo &info)
{
    auto evt = EventCustom(KeyboardWillShowNotification);
    evt.setUserData(&info);
    getEventDispatcher()->dispatchEvent(&evt);
}

void NCEditBox::keyboardDidShow(IMEKeyboardNotificationInfo &info)
{
    auto evt = EventCustom(KeyboardDidShowNotification);
    evt.setUserData(&info);
    getEventDispatcher()->dispatchEvent(&evt);
}


void NCEditBox::keyboardWillHide(IMEKeyboardNotificationInfo &info)
{
    auto evt = EventCustom(KeyboardWillHideNotification);
    evt.setUserData(&info);
    getEventDispatcher()->dispatchEvent(&evt);
}

void NCEditBox::keyboardDidHide(IMEKeyboardNotificationInfo &info)
{
    auto evt = EventCustom(KeyboardDidHideNotification);
    evt.setUserData(&info);
    getEventDispatcher()->dispatchEvent(&evt);
}

#pragma mark - プライベート
float NCEditBox::getTextHeightWithFontPath(float contentwidth, std::string fontPath, float fontSize, std::string text)
{
    auto la=Label::createWithTTF(text, fontPath, fontSize);
    la->setWidth(contentwidth);
    
    return la->getContentSize().height;
}
