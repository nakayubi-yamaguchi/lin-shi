//
//  MenuForScroll.h
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/03/31.
//
//

#ifndef __Regitaro2__MenuForScroll__
#define __Regitaro2__MenuForScroll__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocos-ext.h"

USING_NS_CC;
#define SCROLL_THRESHOLD 20//メニューのタッチを無効にするスクロール閾値

using namespace ui;

class MenuForScroll : public Menu
{
public:
    MenuForScroll();
    ~MenuForScroll();
    static MenuForScroll* create();
    static MenuForScroll* create(MenuItem* item, ...) CC_REQUIRES_NULL_TERMINATION;
    static MenuForScroll* createWithItems(MenuItem* item, va_list args);
    static MenuForScroll* createWithArray(const Vector<MenuItem*>& arrayOfItems);
    virtual bool init() override;
    virtual bool initWithArray(const Vector<MenuItem*>& arrayOfItems);
    
    void setScrollView(ui::ScrollView* scrollView);

    virtual bool onTouchBegan(Touch* touch, Event* event) override;
    virtual void onTouchEnded(Touch* touch, Event* event) override;
    virtual void onTouchCancelled(Touch* touch, Event* event) override;
    virtual void onTouchMoved(Touch* touch, Event* event) override;
protected:
    Point scrollStartPos;
    
private:
    EventListenerTouchOneByOne* _listener;
    ui::ScrollView* _scrollView;
    Vec2 m_scrollStartPosition;

};

#endif /* defined(__Regitaro2__MenuForScroll__) */
