//
//  NCPiecChart.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2018/07/17.
//

#include "NCPieChart.h"

#include "../Utils/Common.h"


NCPieChart* NCPieChart::create(const char *circleFileName)
{
    auto node =new NCPieChart();
    if (node && node->initWithFileName(circleFileName)) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool NCPieChart::initWithFileName(const char *circleFileName)
{
    if (!Sprite::initWithSpriteFrameName(circleFileName)) {
        return false;
    }

    setOpacity(0);//contentSizeを使えるようにしているfile名セットしているだけ。見えなくていい。
    setIsAnimate(true);
    m_circleFileName = circleFileName;
    
    return true;
}

#pragma mark - Public
void NCPieChart::addPieChartDetails(std::vector<PieChartDetail> pieChartDetails)
{
    m_PieChartDetails = pieChartDetails;
}

void NCPieChart::showPieChart(cocos2d::Node *parent)
{
    //プログレスバー生成
    std::vector<float> transValues;
    float sumValue = 0;
    for (int i=0; i<m_PieChartDetails.size(); i++) {
        //合計バリューを計算
        auto pieChartDetail = m_PieChartDetails[i];
        sumValue += pieChartDetail.value;
        
        
        auto bar = Sprite::createWithSpriteFrameName(m_circleFileName);
        bar->setColor(Common::getColorFromHex(pieChartDetail.colorCode));
        bar->setPosition(getContentSize()/2);
        
        //プログレスバー表示
        auto progress = ProgressTimer::create(bar);
        progress->setPosition(getContentSize()/2);
        progress->setType(ProgressTimer::Type::RADIAL);
        progress->setLocalZOrder((int)m_PieChartDetails.size()-i);//最初に貼ったやつを上位表示
        progress->setTag(i+1);
        progress->setPercentage(0);
        addChild(progress);
        
        
        transValues.push_back(sumValue);
    }
    parent->addChild(this);

    Common::startTimeForDebug("円グラフのアニメーション");
    //アニメーション
    showAnimation((isAnimate)? 0: 100, transValues, sumValue);
}


#pragma mark - Private
void NCPieChart::showAnimation(float count, std::vector<float> transValues, float sumValue)
{
    auto delay = DelayTime::create(.01);
    auto callfunc = CallFunc::create([this, count, transValues, sumValue](){
        for (int i=0; i<m_PieChartDetails.size(); i++) {
            auto progress = (ProgressTimer*)getChildByTag(i+1);
            progress->setPercentage(MIN(transValues[i]/sumValue*100, count));
            addChild(progress);
        }
        
        if (count < 100) {
            showAnimation(count+3, transValues, sumValue);
        }
        else {
            Common::stopTimeForDebug("円グラフのアニメーション");
        }
    });

    runAction(Sequence::create(delay, callfunc, NULL));
}
