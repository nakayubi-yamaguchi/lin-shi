//
//  NCIndicator.h
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/09/28.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________NCIndicator_____
#define _____PROJECTNAMEASIDENTIFIER________NCIndicator_____

#include "cocos2d.h"

typedef enum {
    IndicatorType_Rotate,//回転
    IndicatorType_Switch,//切り替え
}IndicatorType;

//構造体
struct SwithInfo{
    std::string fileName;
    int switchCount;
    float frameTime;
    float frameWidth;
};

class NCIndicator : public cocos2d::Sprite
{
public:

    CREATE_FUNC(NCIndicator);
    static NCIndicator* create(const char* fileName);
    static NCIndicator* create(SwithInfo switchInfo);
    virtual bool init();
    virtual bool init(const char* fileName);
    virtual bool init(SwithInfo switchInfo);

    IndicatorType getIndicatorType(){return m_indicatorType;};
    
private:
    IndicatorType m_indicatorType;
    
    void rotateIndicator(float delta);
    void switchIndicator(int switchCounter, SwithInfo switchInfo);

    ~NCIndicator();
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
