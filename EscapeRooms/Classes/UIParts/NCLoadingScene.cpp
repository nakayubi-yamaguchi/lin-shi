//
//  LoadingLayer.cpp
//  Onigokko
//
//  Created by yamaguchinarita on 2016/08/24.
//

#include "NCLoadingScene.h"
#include "NCIndicator.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "Common.h"

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "Utils/Common.h"

#endif

using namespace cocos2d;

Scene* NCLoadingScene::createScene() {
    auto scene = Scene::create();
    auto layer = NCLoadingScene::create(Director::getInstance()->getRunningScene()->getContentSize(), "");
    scene->addChild(layer);
    
    return scene;
}

NCLoadingScene* NCLoadingScene::create(cocos2d::Size size, std::string text)
{
    NCLoadingScene *pRet = new NCLoadingScene();
    if (pRet && pRet->init(size, text))
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

bool NCLoadingScene::init(cocos2d::Size size, std::string text) {
    if (!LayerColor::initWithColor(Color4B(Common::getColorFromHex(BlackColor)))) {
        return false;
    }
    
    setOpacity(210);
    setContentSize(size);
    
    showText = text;
    
    // 初期化
    createListener();
    createIndicator();    
    
    return true;
}

void NCLoadingScene::createListener()
{
    //タッチ透過させない
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [this](Touch *touch,Event*event)->bool{
        
        auto rect = Rect(getPositionX(), getPositionY(), getBoundingBox().size.width, getBoundingBox().size.height);
        
        auto glpoint = getParent()->convertToNodeSpace(touch->getLocation());
        
        if (rect.containsPoint(glpoint)){
            
            log("レイヤー内をたっぷした");
            //タブのみタッチ透過
            return true;
        }
        log("レイヤー外をたっぷした");
        
        return false;
    };
    
    auto dip = Director::getInstance()->getEventDispatcher();
    dip->addEventListenerWithSceneGraphPriority(listener, this);
}

void NCLoadingScene::createIndicator()
{
    log("インジケータを生成");
    
    //インジケーター
    auto indicator = NCIndicator::create();
    indicator->setPosition(getContentSize()/2);//ど真ん中に表示
   
    //メイン
    /*
    auto mainSize = indicator->getBoundingBox().size.width*2;
    auto main = Sprite::createWithSpriteFrameName("loading_back.png");
    main->setColor(Common::getColorFromHex(BlackColor));
    main->setScale(mainSize/main->getContentSize().width);
    main->setPosition(getContentSize()/2);
    main->setOpacity(210);
    addChild(main);*/
    
    addChild(indicator);
    
    //下部のラベル
    loadingLabel = Label::createWithTTF(showText, MyFont, indicator->getBoundingBox().size.height/3);
    loadingLabel->setTextColor(Color4B(Common::getColorFromHex(WhiteColor)));
    loadingLabel->setPosition(indicator->getPositionX(), indicator->getPositionY()-indicator->getBoundingBox().size.height/2-loadingLabel->getBoundingBox().size.height/2);
    loadingLabel->setAlignment(TextHAlignment::LEFT);
    addChild(loadingLabel);
}

void NCLoadingScene::setShowText(std::string text)
{
    showText = text;
    
    loadingLabel->setString(showText);
}

#pragma mark- 解放
NCLoadingScene::~NCLoadingScene()
{
    log("ローディングレイヤーを解放します。");
}
