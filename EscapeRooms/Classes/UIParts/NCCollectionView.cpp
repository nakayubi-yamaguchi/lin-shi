//
//  CollectionView.cpp
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/09/25.


#include "NCCollectionView.h"
#include "MenuForScroll.h"

#define CollectionMenuName "CollectionMenu"

using namespace cocos2d;

NCCollectionView* NCCollectionView::create(CollectionLayout layout, CollectionViewDataSource *dataSource, cocos2d::Size size)
{
    NCCollectionView *pRet = new NCCollectionView();
    if (pRet && pRet->initWithLayout(layout, dataSource, size))
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

bool NCCollectionView::initWithLayout(CollectionLayout layout, CollectionViewDataSource *dataSource, cocos2d::Size size)
{
    if (!ScrollView::init()) {
        return false;
    }
    
    setCollectionLayout(layout);
    setDataSource(dataSource);

    setContentSize(size);
    _updateCellPositions();
    
    
    return true;
}

void NCCollectionView::setCollectionLayout(CollectionLayout layout)
{
    m_layout = layout;
}
                                         
void NCCollectionView::_updateCellPositions()
{
    int cellCount = (int)m_dataSource->numberOfCellsInCollectionView(this);
    
    int row = cellCount/m_layout.inColumnCount;//行
    int column = cellCount%m_layout.inColumnCount;//列
    
    if (column>0) {
        row+=1;
    }
    
    if (cellCount > 0)
    {
        float innerCellHeight = 0.0;
        
        //インナーサイズを決定します。
        //セルの高さを計算
        for (int i = 0; i < cellCount; i+=m_layout.inColumnCount) {
            auto cellSize = m_dataSource->collectionCellSizeForIndex(this, i);
            innerCellHeight += cellSize.height;
        }
        
        //ヘッダーの高さを計算
        float innerHeaderHeight = 0.0;
        for (int i = 0; i < row; i++) {
            log("%d番目のへっだー",i);
            auto headerSize = m_dataSource->headerSizeForCollectionView(this, i);
            innerHeaderHeight += headerSize.height;
        }
        
        auto innerSize = Size(getContentSize().width, m_layout.spaceInRow*(row+1) + innerCellHeight + innerHeaderHeight);
        setInnerContainerSize(innerSize);
        
        //セルを生成します。
        int lastRow = -1;//最後に追加したヘッダーの番号
        float headerPosY = MAX(getContentSize().height,innerSize.height);
        float cellPosY = 0;
        for (int i=0; i < cellCount; i++)
        {
            auto cellSize = m_dataSource->collectionCellSizeForIndex(this, i);
            
            int  cellRow = i / m_layout.inColumnCount;
            int  cellColumn = i % m_layout.inColumnCount;

            auto headerSize = m_dataSource->headerSizeForCollectionView(this, cellRow);

            //log("{%d,%d}",cellRow, cellColumn);
            if (lastRow != cellRow) {//行が変わった。
                
                float delta = m_layout.spaceInRow+headerSize.height/2;
                headerPosY-= delta;
                
                //セルのポジションのために位置をずらす
                cellPosY = headerPosY-(headerSize.height/2 + cellSize.height/2);
                
                if (headerSize.width > 0 && headerSize.height > 0) {
                    auto headerSprite = Sprite::create();
                    headerSprite->setOpacity(0);
                    headerSprite->setTextureRect(Rect(0, 0, headerSize.width, headerSize.height));
                    headerSprite = m_dataSource->collectionHeaderAtIndex(this, headerSprite, cellRow);
                    headerSprite->setPosition(getContentSize().width/2, headerPosY);
                    addChild(headerSprite);
                }
                lastRow = cellRow;
            }
            
            //セル
            auto cell = m_dataSource->collectionCellAtIndex(this, i);
            cell->setIdx(i);
            
            cell->setScale(cellSize.width/cell->getContentSize().width, cellSize.height/cell->getContentSize().height);
            cell->setPosition(Vec2(m_layout.spaceInColumn+cellSize.width/2+(m_layout.spaceInColumn+cellSize.width)*cellColumn, cellPosY));
            
            cell->setCallback([this,i](Ref* sender)
                              {
                                  log("%d番目のボタンが押されました。",i);
                                  if (m_collectionViewDelegate) {
                                      m_collectionViewDelegate->collectionCellTouched(this, (CollectionViewCell*)sender);
                                  }
                              });
            
            m_cells.pushBack(cell);
            
            //ポジションをアイコン下部にする
         headerPosY = cellPosY-cellSize.height/2;
        }
        
        auto menu = MenuForScroll::createWithArray(m_cells);
        menu->setName(CollectionMenuName);
        menu->setScrollView(this);
        addChild(menu);
    }
}

#pragma mark - public
void NCCollectionView::reloadData()
{
    //Menuを削除します。
    removeChildByName(CollectionMenuName);
    
    //保持しているセルの配列も空っぽにします。
    m_cells.clear();
    
    //セル生成
    _updateCellPositions();
}
