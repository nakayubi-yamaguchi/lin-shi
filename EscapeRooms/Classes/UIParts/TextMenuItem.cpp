//
//  TextMenuItem.cpp
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/02/24.
//
//

#include "TextMenuItem.h"
#include "../Utils/Common.h"
using namespace cocos2d;

TextMenuItem* TextMenuItem::create(std::string text, cocos2d::Size size, cocos2d::Color3B color, const ccMenuCallback &callback)
{
    return create(text, size, color, false,callback);
}

TextMenuItem* TextMenuItem::create(std::string text, cocos2d::Size size, cocos2d::Color3B color,bool shadow, const ccMenuCallback &callback)
{
    return create(text, size, color, shadow, 0, callback);
}


TextMenuItem *TextMenuItem::create(std::string text, cocos2d::Size size, cocos2d::Color3B color, bool shadow, float fontSizeRatio, const cocos2d::ccMenuCallback &callback)
{
    return create(text, size, color, shadow, fontSizeRatio, Common::getUsableFontPath(HiraginoMaruFont), callback);
}

TextMenuItem *TextMenuItem::create(std::string text, Size size, Color3B color, bool shadow, float fontSizeRatio, std::string fontpath, const ccMenuCallback &callback)
{
    auto node=new TextMenuItem();
    if (node&&node->init(text, size,color,shadow, fontSizeRatio,fontpath, callback)) {
        node->autorelease();
        return node;
    }
    CC_SAFE_RELEASE(node);
    return node;
}

bool TextMenuItem::init(std::string text, Size size, Color3B color, bool shadow, float fontSizeRatio, std::string fontpath, const ccMenuCallback &callback)
{
    setIsShadow(shadow);
    auto butBack=createBack(size, color, shadow, fontSizeRatio,fontpath,text);
    
    return initWithImage(butBack, butBack, callback);
}

#pragma mark -
Sprite* TextMenuItem::createBack(Size size, Color3B color, bool shadow, float fontSizeRatio, std::string fontpath, std::string text)
{
    buttonSprite=Sprite::create();
    buttonSprite->setTextureRect(Rect(0, 0, size.width, size.height));
    buttonSprite->setColor(color);
    buttonSprite->setCascadeOpacityEnabled(true);
    
    if(shadow){
        //立体的に見せる
        auto butBack= Sprite::create();
        butBack->setTextureRect(Rect(0, 0, buttonSprite->getContentSize().width, buttonSprite->getContentSize().height*.04));
        butBack->setColor(Common::getColorFromHex(DarkGrayColor));
        butBack->setPosition(buttonSprite->getContentSize().width/2, -butBack->getContentSize().height/2);
        buttonSprite->addChild(butBack);
    }
    
    auto butLabel=Label::createWithTTF(text, fontpath, (fontSizeRatio == 0)? size.height*.6: size.height*fontSizeRatio);
    butLabel->setAlignment(TextHAlignment::CENTER);
    butLabel->setPosition(buttonSprite->getContentSize()/2);
    butLabel->setTag(ButtonLabelTag);
    buttonSprite->addChild(butLabel);
    
    //自動size調整
    Common::sizeFit(butLabel, size.width);
    
    return buttonSprite;
}

void TextMenuItem::setButtonColor(cocos2d::Color3B color)
{
    buttonSprite->setColor(color);
}

TextMenuItem::~TextMenuItem()
{
    //rlog("release TextMenuItem");
}
