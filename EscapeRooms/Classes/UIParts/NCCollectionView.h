//
//  CollectionView.h
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/09/25.
//
//

#ifndef _____EscapeContainer________CollectionView_____
#define _____EscapeContainer________CollectionView_____

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "CollectionViewCell.h"

USING_NS_CC;

class NCCollectionView;

struct CollectionLayout{//レイアウト構造体
    float spaceInRow;//縦のスペース
    float spaceInColumn;//横のスペース
    int inColumnCount;//横に何個並べるか
};

class CollectionViewDataSource
{
public:
    
    virtual ~CollectionViewDataSource() {}

    virtual Size collectionCellSizeForIndex(NCCollectionView *collection, ssize_t idx) {
        return cellSizeForCollectionView(collection);
    };
    
    virtual Size cellSizeForCollectionView(NCCollectionView *collection) {
        return Size::ZERO;
    };
    
    virtual Size headerSizeForCollectionView(NCCollectionView *collection, ssize_t row_idx) {
        return Size::ZERO;
    };

    virtual Sprite* collectionHeaderAtIndex(NCCollectionView *collection, Sprite *headerSprite, ssize_t row_idx) {
        return headerSprite;
    };
    
    //以下必須
    virtual CollectionViewCell* collectionCellAtIndex(NCCollectionView *collection, ssize_t idx) = 0;
    
    virtual ssize_t numberOfCellsInCollectionView(NCCollectionView *collection) = 0;
};

class  CollectionViewDelegate
{
public:
    virtual void collectionCellTouched(NCCollectionView* collection, CollectionViewCell* cell) = 0;
};

class NCCollectionView : public cocos2d::ui::ScrollView
{
public:
    static NCCollectionView* create(CollectionLayout layout, CollectionViewDataSource* dataSource, Size size);
    
    virtual bool initWithLayout(CollectionLayout layout, CollectionViewDataSource* dataSource, Size size);

    //ゲッター、セッター
    CollectionLayout getCollectionLayout(){return m_layout; };
    CollectionViewDataSource* getDataSource() { return m_dataSource; }
    void setDataSource(CollectionViewDataSource* source) {m_dataSource = source; }
    CollectionViewDelegate* getDelegate() { return m_collectionViewDelegate; }
    void setDelegate(CollectionViewDelegate* pDelegate) { m_collectionViewDelegate = pDelegate; }
    
    /**セルを更新します。*/
    void reloadData();
    
    /**コレクションレイアウトをセットします。*/
    void setCollectionLayout(CollectionLayout layout);

    virtual ~NCCollectionView(){log("コレクションビューを解放します。");};
    
protected:
    CollectionLayout m_layout;
    CollectionViewDataSource* m_dataSource;
    CollectionViewDelegate* m_collectionViewDelegate;
    Vector<MenuItem*> m_cells;

    void _updateCellPositions();
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
