//
//  CustomMenuItem.h
//  Onigokko
//
//  Created by yamaguchinarita on 2016/08/11.
//
//

#ifndef __Onigokko__MenuItemScale__
#define __Onigokko__MenuItemScale__

#include "cocos2d.h"
class MenuItemScale : public cocos2d::MenuItemImage
{
public:
    MenuItemScale();
    virtual ~MenuItemScale();
    
    static MenuItemScale* create(const char *normal, const char *selected,
                                  const cocos2d::ccMenuCallback& callback);
    static MenuItemScale* create(cocos2d::Node* normalSprite, cocos2d::Node* selectedSprite,
                                 const cocos2d::ccMenuCallback& callback);
    bool initWithImage(const char *normal, const char* selected, const cocos2d::ccMenuCallback& callback);
    bool initWithImage(cocos2d::Node* normalSprite, cocos2d::Node* selectedSprite,
                       const cocos2d::ccMenuCallback& callback);
    void setTappedScale(float tappedScale);
    float getTappedScale();
    // タップした時の効果音番号
    CC_SYNTHESIZE(int, m_seNo, SeNo);
    
    virtual void selected();
    virtual void unselected();
    
private:
    cocos2d::ccMenuCallback m_orgCallback;
        
    // このボタンをタップした時の処理
    void onTapThis(cocos2d::Ref* sender);
    
protected:
    float _originalScale;
    float m_tappedScale;

};
#endif /* defined(__Onigokko__MenuItemScale__) */
