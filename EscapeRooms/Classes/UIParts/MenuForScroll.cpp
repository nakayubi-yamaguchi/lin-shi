//
//  MenuForScroll.cpp
//  Regitaro2
//
//  Created by yamaguchinarita on 2016/03/31.
//
//

#include "MenuForScroll.h"

MenuForScroll::MenuForScroll():_listener(nullptr),_scrollView(nullptr)
{
    
}

#pragma mark - 生成

MenuForScroll* MenuForScroll::create()
{
    return MenuForScroll::create(nullptr, nullptr);
}

MenuForScroll* MenuForScroll::create(MenuItem* item, ...)
{
    va_list args;
    va_start(args,item);
    
    MenuForScroll *ret = MenuForScroll::createWithItems(item, args);
    
    va_end(args);
    
    return ret;
}

MenuForScroll* MenuForScroll::createWithItems(MenuItem* item, va_list args)
{
    Vector<MenuItem*> items;
    if( item )
    {
        items.pushBack(item);
        MenuItem *i = va_arg(args, MenuItem*);
        while(i)
        {
            items.pushBack(i);
            i = va_arg(args, MenuItem*);
        }
    }
    
    return MenuForScroll::createWithArray(items);
}

MenuForScroll* MenuForScroll::createWithArray(const Vector<MenuItem*>& arrayOfItems)
{
    auto ret = new (std::nothrow) MenuForScroll();
    if (ret && ret->initWithArray(arrayOfItems))
    {
        ret->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(ret);
    }
    
    return ret;
}

#pragma mark - 初期化

bool MenuForScroll::init()
{
    return initWithArray(Vector<MenuItem*>());
}

bool MenuForScroll::initWithArray(const Vector<MenuItem*>& arrayOfItems)
{
    if (Layer::init())
    {
        _enabled = true;
        
        setContentSize(Size(0,0));
        
        int z=0;
        
        for (auto& item : arrayOfItems)
        {
            this->addChild(item, z);
            z++;
        }
        
        _selectedItem = nullptr;
        _state = Menu::State::WAITING;
        
        // enable cascade color and opacity on menus
        setCascadeColorEnabled(true);
        setCascadeOpacityEnabled(true);
        
        _listener = EventListenerTouchOneByOne::create();
        CC_SAFE_RETAIN(_listener);
        _listener->setSwallowTouches(true);
        
        _listener->onTouchBegan = CC_CALLBACK_2(Menu::onTouchBegan, this);
        _listener->onTouchMoved = CC_CALLBACK_2(Menu::onTouchMoved, this);
        _listener->onTouchEnded = CC_CALLBACK_2(Menu::onTouchEnded, this);
        _listener->onTouchCancelled = CC_CALLBACK_2(Menu::onTouchCancelled, this);
        
        _eventDispatcher->addEventListenerWithSceneGraphPriority(_listener, this);
        
        return true;
    } else {
        return false;
    }
}


#pragma mark- パブリック
void MenuForScroll::setScrollView(ScrollView* scrollView)
{
    _scrollView = scrollView;
    _listener->setSwallowTouches(false);
    _scrollView->addEventListener([this](Ref* ref, ScrollView::EventType eventType) {
        
        auto scroll = (ScrollView*)ref;
        auto containerVec = scroll->getInnerContainerPosition();
        
        if (eventType == ScrollView::EventType::SCROLLING_BEGAN) {
            m_scrollStartPosition = containerVec;
        }
        else if(eventType == ScrollView::EventType::SCROLLING && m_scrollStartPosition.distanceSquared(scroll->getInnerContainerPosition()) > std::pow(SCROLL_THRESHOLD, 2)){//閾値以上移動したら、メニューのタッチを解除する。
            this->onTouchCancelled(nullptr, nullptr);
        }
    });
}

#pragma mark - タッチ
bool MenuForScroll::onTouchBegan(Touch* touch, Event* event)
{
    if (_scrollView) {
        Point touchPoint = touch->getLocation();
        
        //Rect rect=_scrollView->getBoundingBox();
        //Spriteなどの子として貼り付けた時用に絶対座標に変換してから使用するようにしないといけない。
        Rect rect = Rect(_scrollView->getParent()->convertToWorldSpace(_scrollView->getPosition()),_scrollView->getContentSize());
        
        //log("タッチポイント{%f,%f}", touchPoint.x, touchPoint.y);
        //log("タッチレクト{%f,%f,%f,%f}", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
        if (rect.containsPoint(touchPoint)) {
            //log("タッチを開始します{%f,%f}",touchPoint.x,touchPoint.y);
            m_scrollStartPosition =  Vec2(touchPoint.x, touchPoint.y);
            return Menu::onTouchBegan(touch, event);
        } else {
            //log("タッチ失敗");
            return false;
        }
    } else {
        //log("タッチを開始します。");
        
        return Menu::onTouchBegan(touch, event);
    }
}

void MenuForScroll::onTouchEnded(Touch* touch, Event* event)
{
    //log("タッチをエンドします。");

    if (_state == Menu::State::TRACKING_TOUCH) {
        //log("タッチをエンドします。:");

        Menu::onTouchEnded(touch, event);
    }
}

void MenuForScroll::onTouchCancelled(Touch* touch, Event* event)
{
    log("タッチをキャンセルします。");
    if (_state == Menu::State::TRACKING_TOUCH) {
        log("タッチをキャンセルします。");

        Menu::onTouchCancelled(touch, event);
    }
}

void MenuForScroll::onTouchMoved(Touch* touch, Event* event)
{
    auto touchPoint = touch->getLocation();

    auto moveVec = Vec2(touchPoint.x, touchPoint.y);
    auto delta = m_scrollStartPosition.distanceSquared(moveVec);
    //log("タッチをムーブします。{%f}", delta);
    
    if (_state == Menu::State::TRACKING_TOUCH) {
        //log("タッチをムーブします。");
        Menu::onTouchMoved(touch, event);
    }
}

#pragma mark- 解放
MenuForScroll::~MenuForScroll()
{
    CC_SAFE_RELEASE_NULL(_listener);
}



