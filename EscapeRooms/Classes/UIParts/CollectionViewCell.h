//
//  CollectionViewCell.h
//  EscapeContainer
//
//  Created by yamaguchinarita on 2017/09/26.
//
//

#ifndef __EscapeContainer__CollectionViewCell__
#define __EscapeContainer__CollectionViewCell__

#include "cocos2d.h"
#include "MenuItemScale.h"

USING_NS_CC;

class CollectionViewCell: public MenuItemScale
{
public:    
    static CollectionViewCell* create(Node *normalSprite, Node *selectedSprite);
    
    bool init(Node *normalSprite, Node *selectedSprite);

    ssize_t getIdx() const;
    void setIdx(ssize_t uIdx);

private:
    ssize_t _idx;
};

#endif /* defined(__EscapeContainer__CollectionViewCell__) */
