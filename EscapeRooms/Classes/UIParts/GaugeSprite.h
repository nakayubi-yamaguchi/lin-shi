//
//  GaugeSprite.h
//  Bird
//
//  Created by 成田凌平 on 2017/10/11.
//
//

#ifndef __Bird__GaugeSprite__
#define __Bird__GaugeSprite__

#include "cocos2d.h"
USING_NS_CC;

typedef std::function<void(bool successed)> onFinished;

class GaugeSprite : public Sprite
{
public:
    /**伸縮するゲージ、背景ゲージを渡す*/
    static GaugeSprite* create(Sprite*sprite,Sprite*sprite_back);
    bool init(Sprite*sprite,Sprite*sprite_back);
    
    //UI
    void createGaugeBack(Sprite*spr);
    void createGauge(Sprite*spr);
    void createDisplayLabel(std::string string,float fontSize);
    void setDisplayString(std::string text);
    void setDisplayLabelPos(Vec2 pos);
    void setDisplayLabelPos(float x,float y);
    void createTitleLabel(std::string string,float fontSize);
    void setTitleString(std::string text);
    void setTitleLabelPos(Vec2 pos);
    void setTitleLabelPos(float x,float y);

    /**0~1.0*/
    void setMidPoint(Vec2 vec);
    /**0~100.0*/
    void setPercentage(float value);
    void setPercentage(float value,bool animate,float duration);
    /*値を加算**/
    void addValue(double var,bool animate,const onFinished& callback);

    //プロパティ
    CC_SYNTHESIZE(float, duration, Duration);//アニメーション速度
    //ゲージ
    Sprite*gauge_back;
    Sprite*gauge;
    //表示ラベル
    Label*displayLabel;
    Label*titleLabel;
private:
    
};

#endif /* defined(__Bird__GaugeSprite__) */
