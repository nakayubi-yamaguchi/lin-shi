//
//  TextMenuItem.h
//  EscapeRooms
//
//  Created by 成田凌平 on 2018/02/24.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________TextMenuItem_____
#define _____PROJECTNAMEASIDENTIFIER________TextMenuItem_____

#include "cocos2d.h"
#include "MenuItemScale.h"
#define ButtonLabelTag 1

USING_NS_CC;

class TextMenuItem :public MenuItemScale
{
public:
    static TextMenuItem*create(std::string text,Size size,Color3B color,const ccMenuCallback& callback);
    static TextMenuItem*create(std::string text,Size size,Color3B color,bool shadow,const ccMenuCallback& callback);
    /**ratioは高さに対するfontSize*/
    static TextMenuItem*create(std::string text,Size size,Color3B color, bool shadow, float fontSizeRatio,const ccMenuCallback& callback);
    static TextMenuItem*create(std::string text,Size size,Color3B color, bool shadow, float fontSizeRatio,std::string fontpath,const ccMenuCallback& callback);

    bool init(std::string text,Size size,Color3B color,bool shadow,float fontSizeRatio,std::string fontpath,const ccMenuCallback& callback);
    void setButtonColor(Color3B color);
    CC_SYNTHESIZE(bool, isShadow, IsShadow);
    ~TextMenuItem();
private:
    Sprite*buttonSprite;
    Sprite* createBack(Size size,Color3B color,bool shadow, float fontSizeRatio,std::string fontpath, std::string text);
    
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
