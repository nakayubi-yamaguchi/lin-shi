//
//  CustomMenuItem.cpp
//  Onigokko
//
//  Created by yamaguchinarita on 2016/08/11.
//
//

#include "MenuItemScale.h"

using namespace cocos2d;

const unsigned int kZoomActionTag = 0xc0c05002;

MenuItemScale::MenuItemScale():m_orgCallback(NULL)
, m_seNo(0), _originalScale(0.0)
{
}

MenuItemScale::~MenuItemScale()
{
}

MenuItemScale * MenuItemScale::create(const char *normal, const char *selected, const ccMenuCallback &callback)
{
    MenuItemScale *pRet = new MenuItemScale();
    if (pRet && pRet->initWithImage(normal, selected,  callback))
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

MenuItemScale * MenuItemScale::create(cocos2d::Node *normalSprite, cocos2d::Node *selectedSprite, const cocos2d::ccMenuCallback &callback)
{
    MenuItemScale *pRet = new MenuItemScale();
    if (pRet && pRet->initWithImage(normalSprite, selectedSprite,  callback))
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

bool MenuItemScale::initWithImage(const char *normal, const char* selected, const cocos2d::ccMenuCallback& callback)
{
    m_orgCallback = callback;//コールバックを保持
    _originalScale = 1.0f;
    m_tappedScale = 1.1f;

    // 通常時のボタン画像
    auto pNormal = Sprite::create(normal);
    Sprite* pSelected = NULL;
    if (selected) {
        pSelected = Sprite::create(selected);
    }else{
        // 選択時の画像の指定がなければ、
        // 通常時の画像にグレーをセットして選択時の画像とする
        pSelected = Sprite::create(normal);
        pSelected->setColor(Color3B(105, 105, 105));
    }
    
    return initWithNormalSprite(pNormal, pSelected, NULL, CC_CALLBACK_1(MenuItemScale::onTapThis, this));
}

bool MenuItemScale::initWithImage(cocos2d::Node *normalSprite, cocos2d::Node *selectedSprite, const cocos2d::ccMenuCallback &callback)
{
    m_orgCallback = callback;//コールバックを保持
    _originalScale = 1.0f;
    m_tappedScale = 1.1f;
    
    return initWithNormalSprite(normalSprite, selectedSprite, NULL, CC_CALLBACK_1(MenuItemScale::onTapThis, this));
}

void MenuItemScale::onTapThis(Ref *sender)
{
    if(_enabled)
    {
        this->stopAllActions();
        this->setScale( _originalScale );
    }

    if (m_orgCallback) {//コールバック
        m_orgCallback(this);
    }
}

void MenuItemScale::selected()
{
    MenuItemImage::selected();

    //log("選択されている%d",_enabled);
    // subclass to change the default action
    if(_enabled)
    {
        Action *action = getActionByTag(kZoomActionTag);
        if (action){
            this->stopAction(action);
        }
        else{
            _originalScale = this->getScale();
        }
        
        Action *zoomAction = ScaleTo::create(0.1f, _originalScale * m_tappedScale);
        zoomAction->setTag(kZoomActionTag);
        this->runAction(zoomAction);
    }
}

void MenuItemScale::unselected()
{
    //log("選択されていない%d",_enabled);

    MenuItemImage::unselected();

    // subclass to change the default action
    if(_enabled)
    {
        this->stopActionByTag(kZoomActionTag);
        Action *zoomAction = ScaleTo::create(0.1f, _originalScale);
        zoomAction->setTag(kZoomActionTag);
        this->runAction(zoomAction);
    }
}

#pragma mark - パブリック
void MenuItemScale::setTappedScale(float tappedScale)
{
    m_tappedScale = tappedScale;
}

float MenuItemScale::getTappedScale()
{
    return m_tappedScale;
}
