//
//  NCPiecChart.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/07/17.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____
#define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____

#include "cocos2d.h"
USING_NS_CC;

struct PieChartDetail{
    std::string name;
    float value;
    const char* colorCode;
};

class NCPieChart : public Sprite
{
public:
    static NCPieChart* create(const char* circleFileName);
    bool initWithFileName(const char* circleFileName);
    
    void addPieChartDetails(std::vector<PieChartDetail> pieChartDetails);
    
    /**呼び出し時にアニメーションが開始される。*/
    void showPieChart(Node* parent);

private:
    const char* m_circleFileName;//白色の画像ファイル名が望ましい。
    std::vector<PieChartDetail> m_PieChartDetails;
    CC_SYNTHESIZE(bool, isAnimate, IsAnimate);
    
    void showAnimation(float count, std::vector<float> transValues, float sumValue);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
