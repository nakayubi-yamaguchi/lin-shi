//
//  LoadingLayer.h
//  Onigokko
//
//  Created by yamaguchinarita on 2016/08/24.
//
//

#ifndef __Onigokko__LoadingLayer__
#define __Onigokko__LoadingLayer__

#include "cocos2d.h"

class NCLoadingScene : public cocos2d::LayerColor
{
public:
    static cocos2d::Scene *createScene();
    static NCLoadingScene* create(cocos2d::Size size, std::string text);
    bool init(cocos2d::Size size, std::string text);
    
    void setShowText(std::string text);
private:
    void createListener();
    void createIndicator();
    
    ~NCLoadingScene();
    
protected:
    
    std::string showText;
    cocos2d::Label *loadingLabel;
};

#endif /* defined(__Onigokko__LoadingLayer__) */
