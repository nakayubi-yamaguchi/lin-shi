//
//  GaugeSprite.cpp
//  Bird
//
//  Created by 成田凌平 on 2017/10/11.
//
//

#include "GaugeSprite.h"
#include "../Utils/Common.h"
using namespace cocos2d;

GaugeSprite* GaugeSprite::create(cocos2d::Sprite *sprite, cocos2d::Sprite *sprite_back)
{
    auto node=new GaugeSprite;
    if (node&&node->init(sprite,sprite_back)) {
        node->autorelease();
        return node;
    }
    CC_SAFE_RELEASE(node);
    return node;
}

bool GaugeSprite::init(cocos2d::Sprite *sprite, cocos2d::Sprite *sprite_back)
{
    if(!Sprite::init()){
        return false;
    }
    setDuration(.3);
    setCascadeOpacityEnabled(true);
    setTextureRect(sprite_back->getTextureRect());
    
    createGaugeBack(sprite_back);
    createGauge(sprite);
        
    return true;
}

#pragma mark- UI
void GaugeSprite::createGaugeBack(cocos2d::Sprite *spr)
{
    gauge_back=spr;
    gauge_back->setPosition(getContentSize()/2);
    addChild(gauge_back);
}

void GaugeSprite::createGauge(cocos2d::Sprite *spr)
{    
    gauge=spr;
    addChild(gauge);
    
    setMidPoint(Vec2(0, .5));
    setPercentage(0);
}

#pragma mark DisplayLabel
void GaugeSprite::createDisplayLabel(std::string string, float fontSize)
{
    displayLabel=Label::createWithTTF(string, LocalizeFont, fontSize);
    displayLabel->setPosition(getContentSize()/2);
    displayLabel->setTextColor(Color4B(Common::getColorFromHex(WhiteColor2)));
    //displayLabel->enableOutline(Color4B(60, 60, 60, 255),fontSize*.1);
    displayLabel->enableOutline(Color4B(/*60, 60, 60, 255*/Common::getColorFromHex(LightBlackColor)),fontSize*.07);
    displayLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    addChild(displayLabel);
}

void GaugeSprite::setDisplayString(std::string text)
{
    displayLabel->setString(text);
}

void GaugeSprite::setDisplayLabelPos(cocos2d::Vec2 pos)
{
    displayLabel->setPosition(pos);
}

void GaugeSprite::setDisplayLabelPos(float x, float y)
{
    displayLabel->setPosition(x,y);
}

#pragma mark TitleLabel
void GaugeSprite::createTitleLabel(std::string string, float fontSize)
{
    titleLabel=Label::createWithTTF(string, LocalizeFont, fontSize);
    titleLabel->setPosition(getContentSize()/2);
    titleLabel->setTextColor(Color4B::WHITE);
    //titleLabel->enableOutline(Color4B(60, 60, 60, 255),fontSize*.1);
    titleLabel->enableOutline(Color4B(/*60, 60, 60, 255*/Common::getColorFromHex(LightBlackColor)),fontSize*.07);
    titleLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    addChild(titleLabel);
}

void GaugeSprite::setTitleString(std::string text)
{
    titleLabel->setString(text);
}

void GaugeSprite::setTitleLabelPos(cocos2d::Vec2 pos)
{
    titleLabel->setPosition(pos);
}

void GaugeSprite::setTitleLabelPos(float x, float y)
{
    titleLabel->setPosition(x,y);
}
#pragma mark -
void GaugeSprite::setMidPoint(cocos2d::Vec2 vec)
{
    gauge->setAnchorPoint(vec);
    gauge->setPosition(getContentSize().width/2-gauge->getContentSize().width*(.5-vec.x),
                    getContentSize().height/2-gauge->getContentSize().height*(.5-vec.y));
}

void GaugeSprite::setPercentage(float value)
{
    setPercentage(value, false, 0);
}

void GaugeSprite::setPercentage(float value, bool animate, float duration)
{
    if (animate) {
        auto scaleto=TargetedAction::create(gauge, ScaleTo::create(duration, value/100,1));
        runAction(scaleto);
    }
    else{
        gauge->setScale(value/100,1);
    }
}
