//
//  CustomSwitch.cpp
//  Onigokko
//
//  Created by yamaguchinarita on 2016/08/29.
//
//

#include "CustomSwitch.h"

using namespace cocos2d;

#define Name_BackSprite "backSprite"

CustomSwitch::CustomSwitch():m_orgCallback(NULL),m_backMenuItem(NULL),m_switch(NULL),m_onPosition(0,0),m_offPosition(0,0),m_isOn(NULL),m_height(0)
{
}

CustomSwitch::~CustomSwitch()
{
    log("カスタムスイッチが解放されました。");
}

CustomSwitch* CustomSwitch::create(float height, const cocos2d::ccMenuCallback &callback)
{
    CustomSwitch *pRet = new CustomSwitch();
    if (pRet && pRet->init(height,callback))
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

bool CustomSwitch::init(float height, const cocos2d::ccMenuCallback &callback)
{
    log("カスタムスイッチの意にっと");
    //初期値を入力
    m_height = height;
    m_orgCallback = callback;//コールバックを保持
       
    m_onColor = Color3B::GREEN;
    m_offColor = Color3B::GRAY;
    
    //MenuItemの初期化。
    initialSwitchMenuItem();
    
    Vector<MenuItem*> items;
    items.pushBack(m_backMenuItem);
    if (!Menu::initWithArray(items)) {
        return false;
    }
    
    //デフォルトはオフ
    setOn(false);
    
    return true;
}

void CustomSwitch::initialSwitchMenuItem()
{
    auto normalSprite = Sprite::create("switchBack.png");
    auto selectedSprite = Sprite::create("switchBack.png");
    normalSprite->setName(Name_BackSprite);
    selectedSprite->setName(Name_BackSprite);
    
    //メニューアイテムを生成
    m_backMenuItem = MenuItemSprite::create(normalSprite, selectedSprite,CC_CALLBACK_1(CustomSwitch::onTapThis, this));
    m_backMenuItem->setScale(m_height/m_backMenuItem->getContentSize().height);
    
    //スイッチの位置を初期化
    m_onPosition = Vec2(m_backMenuItem->getContentSize().width-m_backMenuItem->getContentSize().height/2 ,m_backMenuItem->getContentSize().height/2);
    
    m_offPosition = Vec2(m_backMenuItem->getContentSize().height/2, m_backMenuItem->getContentSize().height/2);

    //スワイプされるボタン部分
    m_switch = Sprite::create("thumb.png");
    m_switch->setScale(m_backMenuItem->getContentSize().height*.8/m_switch->getContentSize().height);
    m_switch->setPosition(m_offPosition);//デフォルトはオフ
    m_backMenuItem->addChild(m_switch);
}

void CustomSwitch::onTapThis(Ref* sender)
{
    //状態を変更
    setOn(!m_isOn, true);

    //コールバック
    if (m_orgCallback) {
        m_orgCallback(this);
    }
}

void CustomSwitch::switchAnimation(bool animate)
{
    //スイッチ部分のアニメーションを全て止める
    m_switch->stopAllActions();
    
    float duration = 0;
    
    if (animate) {//アニメーションあり
        duration = .2;
    }
    
    Vec2 distination;
    Color3B color;
    
    if (m_isOn) {
        distination = m_onPosition;
        color = m_onColor;
    }
    else{
        distination = m_offPosition;
        color = m_offColor;
    }
    
    for (auto node : m_backMenuItem->getChildren()) {//メニュアイテムの画像に色つけ
        if (node->getName() == Name_BackSprite) {
            auto tint = TintTo::create(duration, color);
            node->runAction(tint);
        }
    }
    
    log("目的地{%f,%f}",distination.x, distination.y);
    auto move = MoveTo::create(duration, distination);
    auto callback = CallFunc::create([distination](){
        
        log("アニメーション終了{%f,%f}",distination.x, distination.y);
    });
    
    m_switch->runAction(Sequence::create(move, callback, NULL));
}

#pragma mark- パブリック
bool CustomSwitch::isOn()
{
    return m_isOn;
}

void CustomSwitch::setOn(bool isOn){setOn(isOn, false);}

void CustomSwitch::setOn(bool isOn, bool animated)
{
    if (m_isOn == isOn)
        return;
    
    m_isOn = isOn;
    
    //スイッチを更新
    switchAnimation(animated);
}

void CustomSwitch::setOnColor(cocos2d::Color3B color)
{
    m_onColor = color;
    
    if (m_isOn) {
        for (auto node : m_backMenuItem->getChildren()) {//メニュアイテムの画像に色つけ
            if (node->getName() == Name_BackSprite) {
                node->setColor(m_onColor);
            }
        }
    }
}

void CustomSwitch::setOffColor(cocos2d::Color3B color)
{
    m_offColor = color;
    
    if (!m_isOn) {
        for (auto node : m_backMenuItem->getChildren()) {//メニュアイテムの画像に色つけ
            if (node->getName() == Name_BackSprite) {
                node->setColor(m_offColor);
            }
        }
    }
}

Size CustomSwitch::getSwitchSize()
{
    return m_backMenuItem->getBoundingBox().size;
}
