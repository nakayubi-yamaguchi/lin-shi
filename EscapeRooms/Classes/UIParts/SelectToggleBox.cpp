//
//  SelectBox.cpp
//  Onigokko
//
//  Created by yamaguchinarita on 2016/08/11.
//

#include "SelectToggleBox.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "Utils/Common.h"

#else
#include "Common.h"

#endif
using namespace cocos2d;

SelectToggleBox::SelectToggleBox():m_orgCallback(NULL),selectedToggleNum(0),_size(0,0),_menuItems(NULL), _menuLabels(NULL)
{
}

SelectToggleBox::~SelectToggleBox()
{
    log("トグルボックスが解放されました。");
}

SelectToggleBox* SelectToggleBox::create(std::vector<std::string> items, Size size, const cocos2d::ccMenuCallback &callback)
{
    return create(items, size, Color3B(0, 0, 0), Color3B(0, 0, 0), callback);
}

SelectToggleBox* SelectToggleBox::create(std::vector<std::string> items, Size size, Color3B onNormal, Color3B onSelected, const cocos2d::ccMenuCallback &callback)
{
    SelectToggleBox *pRet = new SelectToggleBox();
    if (pRet && pRet->initWithItems(items, size, onNormal, onSelected, callback))
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

bool SelectToggleBox::initWithItems(std::vector<std::string> items, Size size, const cocos2d::ccMenuCallback &callback)
{
    return initWithItems(items, size, Color3B(0, 0, 0), Color3B(0, 0, 0), callback);
}

bool SelectToggleBox::initWithItems(std::vector<std::string> items, cocos2d::Size size, Color3B onNormal, ::Color3B onSelected, const cocos2d::ccMenuCallback &callback)
{
    //初期値を入力
    m_orgCallback = callback;//コールバックを保持
    _size = size;
    
    if (onNormal.r==0 && onNormal.g==0 && onNormal.b==0) {//空なら
        _normalColor = Color3B(235, 235, 235);
    }
    else{//指定されている
        _normalColor = onNormal;
    }
    
    if (onSelected.r==0 && onSelected.g==0 && onSelected.b==0) {//空なら
        _selectedColor = Color3B(84, 202, 196);
    }
    else{//指定されている
        _selectedColor = onSelected;
    }
    
    _menuItems = createMenuItems(items);
    
    if (!Menu::initWithArray(_menuItems)) {
        return false;
    }
    alignItemsHorizontallyWithPadding(2);//横に整列させる
    _menuLabels = createLabels(items);//メニューにラベルを載せる。スケール対策
    
    return true;
}

int SelectToggleBox::getSelectedToggleNum()
{
    return selectedToggleNum;
}

void SelectToggleBox::setSelectedToggleNum(int var)
{
    //選択されているトグル番号をセット
    selectedToggleNum = var;
    
    //指定番目のトグルを選択状態にする。
    auto targetToggle = (MenuItemToggle*)_menuItems.at(selectedToggleNum);
    targetToggle->setSelectedIndex(true);
    
    //全トグルの選択状態を走査
    reloadToggle();
}

void SelectToggleBox::setCallback(ccMenuCallback callback)
{//コールバックをセット
    m_orgCallback = callback;
}

Size SelectToggleBox::getMenuSize()
{
    return _size;
}

#pragma mark- プライベート
Vector<MenuItem*> SelectToggleBox::createMenuItems(std::vector<std::string> items)
{
    Vector<MenuItem*> menuItems;
    for (int i=0; i<items.size(); i++) {
        
        auto preferSize = Size(_size.width/items.size(), _size.height);

        auto back=Sprite::create();
        back->setTextureRect(Rect(0, 0, preferSize.width, preferSize.height));
        back->setColor(_normalColor);
        
        auto backSelected=Sprite::create();
        backSelected->setTextureRect(back->getTextureRect());
        backSelected->setColor(_selectedColor);
        
        auto menuItem = MenuItemSprite::create(back, back);
        auto menuItemSelected = MenuItemSprite::create(backSelected, backSelected);
        
        //トグル作成。
        auto toggle = MenuItemToggle::createWithCallback(CC_CALLBACK_1(SelectToggleBox::onTapThis, this), menuItem, menuItemSelected, NULL);
        toggle->setTag(i);//後で選択状態とかを変更するよう
        toggle->setCascadeOpacityEnabled(true);
        //スケール指定して、メニューへ追加
        toggle->setScale(preferSize.width/menuItem->getContentSize().width, preferSize.height/toggle->getContentSize().height);
        
        menuItems.pushBack(toggle);
    }
    
    return menuItems;
}

Vector<Label*> SelectToggleBox::createLabels(std::vector<std::string> labelNames)
{
    Vector<Label*> labels;
    for (int i=0; i<labelNames.size(); i++) {
        
        auto targetToggle = (MenuItemToggle*)_menuItems.at(i);
        
        //ラベルを追加
        auto label = Label::createWithTTF(labelNames.at(i), HiraginoMaruFont, targetToggle->getBoundingBox().size.height/2);
        label->setPosition(targetToggle->getPosition());
        addChild(label, 2);
        
        labels.pushBack(label);
    }
    
    return labels;
}

void SelectToggleBox::onTapThis(Ref* sender)
{
    //選択状態を変更したりする処理。
    auto targetToggle = (MenuItemToggle*)sender;
    
    auto toggleSelected = targetToggle->getSelectedIndex();
    
    if (toggleSelected) {//選択状態にされた
        
        //log("トグルの選択状態%d",toggleSelected);
        
        int selectedNum = targetToggle->getTag();
        
        //選択されているトグル番号をセット
        setSelectedToggleNum(selectedNum);
        
        //全トグルの選択状態を走査
        reloadToggle();
    }
    else{//選択状態を解除.それは認められない。
        targetToggle->setSelectedIndex(1);
        
        //選択状態の変更を認めないので、コールバック返す必要もない。
        return;
    }
    
    //コールバック
    if (m_orgCallback) {
        m_orgCallback(this);
    }
}

void SelectToggleBox::reloadToggle()
{
    for (int i=0; i<_menuItems.size(); i++) {
        
        if (i!=getSelectedToggleNum()) {//操作したトグル以外のトグルを抽出
            auto otherToggle = (MenuItemToggle*) _menuItems.at(i);
            
            otherToggle->setSelectedIndex(0);//選択状態を解除
            
            //ラベルの色も更新
            auto otherLabel = (Label*) _menuLabels.at(i);
            otherLabel->setTextColor(Color4B(_selectedColor));
        }
        else{//選択トグル
            auto otherLabel = (Label*) _menuLabels.at(i);
            otherLabel->setTextColor(Color4B(_normalColor));
        }
    }
}


