//
//  CustomEditBox.h
//  Onigokko
//
//  Created by 成田凌平 on 2016/07/26.
//
// closeKeyBoard() メソッド持ちのeditBox

#ifndef __Onigokko__CustomEditBox__
#define __Onigokko__CustomEditBox__

#include "cocos2d.h"
#include "ui/CocosGUI.h"


#define KeyboardWillShowNotification "KeyboardWillShowNotification"
#define KeyboardDidShowNotification "KeyboardDidShowNotification"
#define KeyboardWillHideNotification "KeyboardWillHideNotification"
#define KeyboardDidHideNotification "KeyboardDidHideNotification"

USING_NS_CC;
using namespace cocos2d::ui;

class NCEditBoxDelegate{
public:
    virtual void customEditBoxEditingDidBegin(){};
    virtual void customEditBoxDidReturn(){};
    virtual void customEditBoxTextChanged(bool textHeightChanged,float textDifference){};
};

class NCEditBox:public ui::EditBox,public ui::EditBoxDelegate
{
public:
    static NCEditBox*create(Size size,ui::Scale9Sprite* normalSprite);
    bool init(Size size,ui::Scale9Sprite* normalSprite);
    
    CC_SYNTHESIZE(Size, _originalSize, OriginalSize);//サイズの初期値
    CC_SYNTHESIZE(float, _testHeight, TextHeight);//包括サイズ
    
    NCEditBoxDelegate*mDelegate;
    
    void openKeyBoard();
    void closeKeyBoard();
    void setCustomDelegate(NCEditBoxDelegate*delegate){mDelegate=delegate;};
    
    std::string getText();
    
private:
    //editBoxDelegate
    virtual void editBoxEditingDidBegin(ui::EditBox* editBox);
    virtual void editBoxEditingDidEnd(ui::EditBox* editBox);
    virtual void editBoxTextChanged(ui::EditBox* editBox, const std::string& text);
    virtual void editBoxReturn(ui::EditBox* editBox);
    
    virtual void keyboardWillShow(IMEKeyboardNotificationInfo& info);
    virtual void keyboardDidShow(IMEKeyboardNotificationInfo& info);
    virtual void keyboardWillHide(IMEKeyboardNotificationInfo& info);
    virtual void keyboardDidHide(IMEKeyboardNotificationInfo& info);
    
    
    /**ラベルの高さ取得*/
    float getTextHeightWithFontPath(float contentwidth, std::string fontPath, float fontSize, std::string text);
   
};


#endif /* defined(__Onigokko__CustomEditBox__) */
