//
//  RoundedBoxSprite.h
//  Byuun
//
//  Created by 成田凌平 on 2016/03/30.
//
//

#ifndef __Byuun__RoundedBoxSprite__
#define __Byuun__RoundedBoxSprite__

#include "cocos2d.h"

USING_NS_CC;

class RoundedBoxSprite:public Sprite
{
public:
    static RoundedBoxSprite*create(Size _size,Color3B BGColor,float CornerRadius,int CornerSegment);
    virtual bool init(Size _size,Color3B BGColor,float CornerRadius,int CornerSegment);
    
    void setMaskedColor(Color3B color);
    
private:
    Sprite*maskedSprite;
    Node* createRoundedRectMaskNode(Size size, float radius, int cornerSegments);
    void appendCubicBezier(int startPoint, std::vector<Vec2>& verts, const Vec2& from, const Vec2& control1, const Vec2& control2, const Vec2& to, uint32_t segments);
};

#endif /* defined(__Byuun__RoundedBoxSprite__) */