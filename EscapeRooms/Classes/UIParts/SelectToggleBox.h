//
//  SelectBox.h
//  Onigokko
//
//  Created by yamaguchinarita on 2016/08/11.
//
//

#ifndef __Onigokko__SelectToggleBox__
#define __Onigokko__SelectToggleBox__

#include "cocos2d.h"

class SelectToggleBox : public cocos2d::Menu
{
public:
    SelectToggleBox();
    virtual ~SelectToggleBox();
    
    static SelectToggleBox* create(std::vector<std::string> items, cocos2d::Size size,const cocos2d::ccMenuCallback& callback);
    
    static SelectToggleBox* create(std::vector<std::string> items, cocos2d::Size size, cocos2d::Color3B onNormal, cocos2d::Color3B onSelected,const cocos2d::ccMenuCallback& callback);
    

    bool initWithItems(std::vector<std::string> items, cocos2d::Size size, const cocos2d::ccMenuCallback& callback);
    
    bool initWithItems(std::vector<std::string> items, cocos2d::Size size, cocos2d::Color3B onNormal, cocos2d::Color3B onSelected, const cocos2d::ccMenuCallback& callback);

    
    int getSelectedToggleNum();
    void setSelectedToggleNum(int var);
    
    void setCallback(cocos2d::ccMenuCallback callback);
    cocos2d::Size getMenuSize();

private:
    int selectedToggleNum;
   
    cocos2d::Vector<cocos2d::MenuItem*> createMenuItems(std::vector<std::string> items);
    cocos2d::Vector<cocos2d::Label*> createLabels(std::vector<std::string> labelNames);

    //このボタンをタップした時の処理
    void onTapThis(cocos2d::Ref* sender);
    
    void reloadToggle();
    
protected:
    cocos2d::Size _size;
    cocos2d::Vector<cocos2d::MenuItem*> _menuItems;
    cocos2d::Vector<cocos2d::Label*> _menuLabels;
    cocos2d::ccMenuCallback m_orgCallback;
    
    cocos2d::Color3B _normalColor;
    cocos2d::Color3B _selectedColor;
};

#endif /* defined(__Onigokko__SelectToggleBox__) */
