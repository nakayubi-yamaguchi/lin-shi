//
//  NCDownloadingBar.cpp
//  EscapeRooms
//
//  Created by yamaguchi on 2018/08/22.
//
//

#include "NCDownloadingBar.h"
#include "../Utils/Common.h"
#include "NCIndicator.h"

using namespace cocos2d;

NCDownloadingBar* NCDownloadingBar::create()
{
    auto node =new NCDownloadingBar();
    if (node && node->init()) {
        node->autorelease();
        return node;
    }
    
    CC_SAFE_RELEASE(node);
    return node;
}

bool NCDownloadingBar::init()
{
    if (!Sprite::initWithSpriteFrameName("wideband.png")) {
        return false;
    }
    
    setColor(Common::getColorFromHex(MyColor));

    createContents();
    
    return true;
}

void NCDownloadingBar::createContents()
{
    float indicatorAreaSquare = getContentSize().height*.8;
    
    //インジケーター
    auto indicator = NCIndicator::create();
    indicator->setScale(indicatorAreaSquare*.8/indicator->getContentSize().height);
    indicator->setPosition(indicatorAreaSquare/2, getContentSize().height/2);
    addChild(indicator);
    
    //ダウンロード中ラベル
    auto dlLabel = Label::createWithTTF(Common::localize("Downloading","ダウンロード中"), HiraginoMaruFont, getContentSize().height*.45);
    dlLabel->setTextColor(Color4B(Common::getColorFromHex(WhiteColor2)));
    dlLabel->setWidth(getContentSize().width-indicatorAreaSquare);
    dlLabel->setPosition(indicatorAreaSquare+dlLabel->getWidth()/2, getContentSize().height/2);
    addChild(dlLabel);
}

NCDownloadingBar::~NCDownloadingBar()
{
    log("ダウンローディングバーを解放します。");
}
