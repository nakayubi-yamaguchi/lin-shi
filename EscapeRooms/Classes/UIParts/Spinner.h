//
//  Spinner.h
//  Onigokko
//
//  Created by yamaguchinarita on 2016/08/15.
//
//

#ifndef __Onigokko__Spinner__
#define __Onigokko__Spinner__

#include "cocos2d.h"


typedef enum
{
    SpinnerType_Minus=0,
    SpinnerType_Plus
}SpinnerType;

/**下限もしくは上限に達した時のコールバック*/
using onReachedLimitCallback = std::function<void(bool isMax, int limitCount)>;

/**サイズ固定です。*/
class Spinner : public cocos2d::Menu
{
public:
    Spinner();
    virtual ~Spinner();
    
    static Spinner* create(cocos2d::Size size,const cocos2d::ccMenuCallback& callback);
    
    bool init(cocos2d::Size size, const cocos2d::ccMenuCallback& callback);
    
    /**現在のカウントを取得*/
    int getCount();
    
    /**最大値を指定*/
    void setMaxCount(int maxCount);
    
    /**最小値を指定*/
    void setMinCount(int minCount);
    
    /**カウントを指定*/
    void setCount(int var);
    
    /**変動単位を指定*/
    void setUnit(int unit);
    
    /**長押し中かどうか*/
    bool isPressed();
    
private:
    cocos2d::Vector<cocos2d::MenuItem*> createMenuItems();
    cocos2d::Sprite* createSpriteWithLabel(std::string fileName, bool isSelectedType, std::string labelString);
    void createCounterLabel();
    
    //このボタンをタップした時の処理
    void onTapThis(cocos2d::Ref* sender);
    
    /**カウントを計算する*/
    void calculateCount(SpinnerType spinnerType);
    
    /**長押し開始*/
    void startPressed();
    
    /**長押し終了*/
    void stopPressed();
    
protected:
    cocos2d::Size m_size;
    cocos2d::Size m_eachItemSize;
    
    cocos2d::Vector<cocos2d::MenuItem*> m_menuItems;
    cocos2d::Label* m_counterLabel;
    cocos2d::ccMenuCallback m_orgCallback;
    
    cocos2d::Color3B m_grayColor;
    cocos2d::Color3B m_imageColor;
    int m_count;
    
    /**最大値。未指定の場合は99*/
    int m_maxCount;
    
    /**最小値。未指定の場合は0*/
    int m_minCount;
    
    /**加減単位。デフォルト1*/
    int m_unit;
    
    /**下限 or 上限に達した時のコールバック*/
    onReachedLimitCallback m_onReachLimitCallback;
    
    /**長押し秒数*/
    float m_pressedCount;

    /**限界値に達した時のコールバックを指定*/
    virtual void setOnReachedLimitCallback(const onReachedLimitCallback& callback);

    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) override;
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event) override;
    virtual void onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *event) override;
    virtual void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event) override;
    virtual void onTouchPressed(float delta);
    

};

#endif /* defined(__Onigokko__Spinner__) */
