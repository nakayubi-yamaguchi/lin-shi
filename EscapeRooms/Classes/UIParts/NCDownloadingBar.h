//
//  NCDownloadingBar.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/08/22.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________NCDownloadingBar_____
#define _____PROJECTNAMEASIDENTIFIER________NCDownloadingBar_____

#include "cocos2d.h"

using namespace cocos2d;

class NCDownloadingBar : public Sprite
{
public:
    static NCDownloadingBar* create();
    
    bool init();

private:
    /**インジケーターとラベル*/
    void createContents();
    
    ~NCDownloadingBar();
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
