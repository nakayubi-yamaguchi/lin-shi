//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__BabyRoomActionManager__
#define __EscapeContainer__BabyRoomActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class BabyRoomActionManager: public CustomActionManager
{
public:
    static BabyRoomActionManager* manager;
    static BabyRoomActionManager* getInstance();
    
    void openingAction(Node*node,const onFinished& callback);
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);

    Sequence* createLightAction(int num,Node*parent);

private:
    Sprite*createSpriteToCenter(std::string name,bool invisible,Node* parent);
    CallFunc*createSoundAction(std::string fileName);
    Spawn* createChangeAction(float duration_fadeout,float duration_fadein,Sprite*before,Sprite*after);
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
