//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__GuestHouseActionManager__
#define __EscapeContainer__GuestHouseActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class GuestHouseActionManager : public CustomActionManager
{
public:
    static GuestHouseActionManager* manager;
    static GuestHouseActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
private:
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
