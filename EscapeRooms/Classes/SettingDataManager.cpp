//
//  SettingDataManager.cpp
//  Kebab
//
//  Created by 成田凌平 on 2016/12/17.
//
//

#include "SettingDataManager.h"

using namespace cocos2d;

SettingDataManager* SettingDataManager::manager =NULL;

SettingDataManager* SettingDataManager::sharedManager()
{
    if (manager==NULL) {
        manager=new SettingDataManager();
        manager->init();
    }
    
    return manager;
}

void SettingDataManager::init()
{
    
}

#pragma mark-
void SettingDataManager::setStopBgm(bool var)
{
    stopBgm = var;
    
    UserDefault::getInstance()->setBoolForKey("StopBGM", stopBgm);
}

bool SettingDataManager::getStopBgm()
{
    return UserDefault::getInstance()->getBoolForKey("StopBGM", false);

}

void SettingDataManager::setStopSe(bool var)
{
    stopSe = var;
    
    UserDefault::getInstance()->setBoolForKey("StopSE", stopSe);
}

bool SettingDataManager::getStopSe()
{
    return UserDefault::getInstance()->getBoolForKey("StopSE", false);
}

void SettingDataManager::setStopVibe(bool var)
{
    stopVibe = var;
    
    UserDefault::getInstance()->setBoolForKey("StopVibe", stopVibe);
}

bool SettingDataManager::getStopVibe()
{
    return UserDefault::getInstance()->getBoolForKey("StopVibe", false);
}


void SettingDataManager::setShowTapImage(bool var)
{
    UserDefault::getInstance()->setBoolForKey("ShowTapImage", var);
}

bool SettingDataManager::getShowTapImage()
{
    return UserDefault::getInstance()->getBoolForKey("ShowTapImage");
}

void SettingDataManager::setAutoRemoveCathe(bool var)
{
    UserDefault::getInstance()->setBoolForKey("AutoRemoveCathe", var);
}

bool SettingDataManager::getAutoRemoveCathe()
{
    return UserDefault::getInstance()->getBoolForKey("AutoRemoveCathe", true);
}

void SettingDataManager::setSortType(SortType sortType)
{
    UserDefault::getInstance()->setIntegerForKey("SortType", sortType);
}

SortType SettingDataManager::getSortType()
{
    return (SortType)UserDefault::getInstance()->getIntegerForKey("SortType", SortType_Date);
}

void SettingDataManager::setIsAscend(bool isAscend)
{
    UserDefault::getInstance()->setBoolForKey("IsAscend", isAscend);
}

bool SettingDataManager::getIsAscend()
{
   return  UserDefault::getInstance()->getIntegerForKey("IsAscend", false);
}
