//
//  IzakayaActionManager.h
//  EscapeRooms
//
//  Created by yamaguchi on 2018/10/17.
//
//

#ifndef __EscapeContainer__TwoThousandNineteenActionManagerManager__
#define __EscapeContainer__TwoThousandNineteenActionManagerManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class TwoThousandNineteenActionManagerManager:public CustomActionManager
{
public:
    static TwoThousandNineteenActionManagerManager* manager;
    static TwoThousandNineteenActionManagerManager* getInstance();
    
    //over ride
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void customAction(std::string key,Node*parent);
    void clearParticleAction(cocos2d::Node *parent);
    void storyAction(std::string key, cocos2d::Node *parent, const onFinished &callback);
    void showAnswerAction(std::string key, cocos2d::Node *parent);
    
private:
    ParticleSystemQuad* showFireParticle(Node *parent, int backID);
    ParticleSystemQuad* showFireParticle(Node *parent);
    FiniteTimeAction* boiledActions(Node* parent);
};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
