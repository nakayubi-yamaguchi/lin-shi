//
//  SliderSprite.h
//  EntranceCeremony
//
//  Created by 成田凌平 on 2017/03/31.
//
//

#ifndef __EntranceCeremony__SliderSprite__
#define __EntranceCeremony__SliderSprite__

#include "cocos2d.h"
#include "EscapeStageSprite.h"

USING_NS_CC;

typedef enum{
    SlideDirection_Accend,
    SlideDirection_Decend
}SlideDirection;

class SliderSprite:public EscapeStageSprite
{
public:
    static SliderSprite*create(ValueMap map,Value answerMapV,std::string name,int number,Size parentSize,std::string temporaryFlagkey);
    static SliderSprite*create(ValueMap map,Value answerMapV,std::string name,int number,Size parentSize,std::string temporaryFlagkey,bool onPhoto);

    bool init(ValueMap map,Value answerMapV,std::string name,int number,Size parentSize,std::string temporaryFlagkey,bool onPhoto);
    
    std::vector<Vec2>positions;
    
    CC_SYNTHESIZE(int, number, Number)//何番目のsliderか
    CC_SYNTHESIZE(int, posIndex, PosIndex)//今の位置を記憶
    CC_SYNTHESIZE(float, duration,Duration);//スライドduration
    CC_SYNTHESIZE(std::string, playSE,PlaySE);//スライド効果音
    CC_SYNTHESIZE(std::string, customFlagKey, CustomFlagKey);
    CC_SYNTHESIZE(std::string, fileName, FileName);//sup_%d部分
    CC_SYNTHESIZE(bool, isReverse, IsReverse);//往復するような動きに
    CC_SYNTHESIZE(SlideDirection, slideDirection, SlideDirection);//indexの方向

    void slide(ccMenuCallback callback);
    
    /**キーを自動生成*/
    void save();
    /**キーを自動生成 onPhoto*/
    void save(bool onPhoto);
    /**キーを完全指定*/
    void save(std::string key);
private:
    void addPositions(ValueVector vector);
    void setDefaultPosition(Value answer,bool onPhoto);
    std::string getTemporaryKey(std::string name);
    std::string getTemporaryKey(std::string name,bool onPhoto);
};

#endif /* defined(__EntranceCeremony__SliderSprite__) */
