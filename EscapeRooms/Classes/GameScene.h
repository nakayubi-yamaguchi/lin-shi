//
//  GameScene.h
//  Kebab
//
//  Created by 成田凌平 on 2016/12/17.
//
//UI包括コンテナ 広告管理とシーン切り替え

#include "TopMenuSprite.h"
#include "UnderMenuSprite.h"
#include "MainSprite.h"

#ifndef __Kebab__GameScene__
#define __Kebab__GameScene__

#include "cocos2d.h"

USING_NS_CC;


typedef enum{
    ChangeAnimationLeft=0,
    ChangeAnimationRight,
    ChangeAnimationFade
}ChangeAnimation;

class GameScene : public cocos2d::LayerColor,MainSpriteDelegate
{
public:
    virtual bool init();
    static Scene *createScene();
    CREATE_FUNC(GameScene);
    void onEnterTransitionDidFinish();
    void showDiscription();
    
    //UI比率
    float topBannerHeight;
    float underBannerHeight;
    float mainImageHeight;
    float topMenuHeight;
    float underMenuHeight;
    
    //UI
    TopMenuSprite*topSprite;
    MainSprite*mainSprite;
    UnderMenuSprite*underSprite;
    
    //UIメソッド
    void createMain();
    void createTopMenu();//獲得アイテム、設定ボタン表示
    void createUnderMenu();//視点、scene変更ボタン
    void createDownloadBar();//ダウンロードバー。チュートリアル中のみ。
    
    //通知
    void addNotification();
    void showIS(float delay);
    
    //mainSpriteDelegate
    void changeMainSprite(ReplaceType replaceType);

    //プレイ中か判定
    bool isPlay;
    
    /**背景のデフォルトを決定*/
    void setDefaultBackNum();
    
    //dealloc
    ~GameScene();
};


#endif /* defined(__Kebab__GameScene__) */
