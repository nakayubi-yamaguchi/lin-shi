//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__HalloweenActionManager__
#define __EscapeContainer__HalloweenActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class HalloweenActionManager:public CustomActionManager
{
public:
    static HalloweenActionManager* manager;
    static HalloweenActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    
    FiniteTimeAction*createSands(Node*parent,int type);
    Spawn*createHands(Node*parent);
    
    void customAction(std::string key,Node*parent);
private:

};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
