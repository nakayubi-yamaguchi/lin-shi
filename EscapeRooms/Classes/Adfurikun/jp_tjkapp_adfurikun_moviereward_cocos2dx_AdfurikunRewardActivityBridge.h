#include <jni.h>

#ifndef _Included_org_cocos2dx_cpp_AppActivity
#define _Included_org_cocos2dx_cpp_AppActivity

extern "C" {
    
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_moviereward_cocos2dx_AdfurikunRewardActivityBridge_onPrepareSuccess
    (JNIEnv *, jclass, jstring);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_moviereward_cocos2dx_AdfurikunRewardActivityBridge_onStartPlaying
    (JNIEnv *, jclass, jstring, jstring);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_moviereward_cocos2dx_AdfurikunRewardActivityBridge_onFinishedPlaying
    (JNIEnv *, jclass, jstring);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_moviereward_cocos2dx_AdfurikunRewardActivityBridge_onFailedPlaying
    (JNIEnv *, jclass, jstring);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_moviereward_cocos2dx_AdfurikunRewardActivityBridge_onAdClose
    (JNIEnv *, jclass, jstring);

}

#endif