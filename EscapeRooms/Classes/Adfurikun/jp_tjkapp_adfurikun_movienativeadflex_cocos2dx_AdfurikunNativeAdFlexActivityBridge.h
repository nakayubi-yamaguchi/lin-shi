#include <jni.h>

#ifndef _Included_org_cocos2dx_cpp_AppActivity
#define _Included_org_cocos2dx_cpp_AppActivity

extern "C" {
    
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienativeadflex_cocos2dx_AdfurikunNativeAdFlexActivityBridge_onPrepareSuccess
    (JNIEnv *, jclass, jstring);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienativeadflex_cocos2dx_AdfurikunNativeAdFlexActivityBridge_onStartPlaying
    (JNIEnv *, jclass, jstring, jstring);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienativeadflex_cocos2dx_AdfurikunNativeAdFlexActivityBridge_onFinishedPlaying
    (JNIEnv *, jclass, jstring);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienativeadflex_cocos2dx_AdfurikunNativeAdFlexActivityBridge_onFailedPlaying
    (JNIEnv *, jclass, jstring);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienativeadflex_cocos2dx_AdfurikunNativeAdFlexActivityBridge_onAdClose
    (JNIEnv *, jclass, jstring);

}

#endif