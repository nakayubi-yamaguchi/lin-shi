//
//  ADFMovieRewardNativeManager.h
//  adfurikunSample
//
//  Created by tsukui on 2016/02/09.
//
//

#ifndef adfurikunSample_ADFMovieRewardNativeManager_h
#define adfurikunSample_ADFMovieRewardNativeManager_h
#import "ADFMovieRewardCocosAdapter.h"

@interface ADFMovieRewardNativeManager : NSObject<ADFMovieRewardCocosAdapterDelegate>
+ (void)initializeMovieRewardWithAppID:(NSString*)appID;
+ (void)setDelegate:(NSString *)appID delegate:(void *)delegate;
+ (void *)getDelegate:(NSString *)appID;
+ (void)detachDelegate:(NSString *)appID;
+ (ADFMovieRewardCocosAdapter *)getAdapter:(NSString *)appID;
+ (bool)isPreparedMovieReward:(NSString*)appID;
+ (void)playMovieReward:(NSString*)appID;
+ (void)dispose_handle;
@end

#endif
