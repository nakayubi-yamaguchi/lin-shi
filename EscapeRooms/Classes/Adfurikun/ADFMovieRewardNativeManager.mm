#import "ADFMovieReward.h"
#import "ADFMovieRewardNativeManager.h"
#import "platform/ios/CCEAGLView-ios.h"

@interface ADFMovieRewardNativeManager()
@end

@implementation ADFMovieRewardNativeManager

// クラス自身のインスタンス
static ADFMovieRewardNativeManager* instance = nil;
//動画リワードのインスタンスを格納する為の配列
__strong static NSMutableDictionary* adMovieRewardList = nil;

- (id)init {
    self = [super init];
    if ( self ) {
    }
    return self;
}

+ (void)initializeMovieRewardWithAppID:(NSString*)appID {
    if([appID length] < 1){return;}
    if ( instance == nil) {
        instance = [ADFMovieRewardNativeManager new];
    }
    if(adMovieRewardList == nil){
        adMovieRewardList = [@{} mutableCopy];
    }
    
    //iOSが対応バージョンなら、指定した広告枠で動画読み込みを開始
    if(![ADFmyMovieReward isSupportedOSVersion]){
        return;
    }
    [ADFmyMovieReward initWithAppID:appID viewController:[ADFMovieRewardNativeManager getRootViewController]];
    //Unity用のアダプタークラスを生成
    ADFMovieRewardCocosAdapter* cocosAdapter =
    [[ADFMovieRewardCocosAdapter alloc] initWithAppID:appID];
    cocosAdapter.delegate = instance;
    //アダプターをリストに保存
    [adMovieRewardList setObject:cocosAdapter forKey:appID];
}

+ (void)setDelegate:(NSString *)appID delegate:(void *)delegate {
    if([appID length] < 1){ return; }
    //アダプターを取得
    ADFMovieRewardCocosAdapter* adapter = [ADFMovieRewardNativeManager getAdapter:appID];
    if(adapter != nil){
        [ADFmyMovieReward getInstance:appID delegate:adapter];
        //コールバック(c++)を設定
        [adapter setCallback:delegate];
        //コールバック(Objective-c)を設定
        adapter.delegate = instance;
    }
}

+ (void *)getDelegate:(NSString *)appID{
    if([appID length] < 1){ return nil; }
    ADFMovieRewardCocosAdapter* adapter = [ADFMovieRewardNativeManager getAdapter:appID];
    if(adapter != nil){
        return [adapter getCallback];
    }
    return nil;
}

+ (void)detachDelegate:(NSString*)appID{
    ADFMovieRewardCocosAdapter* adapter = (ADFMovieRewardCocosAdapter*)[adMovieRewardList valueForKey:appID];
    if(adapter != nil) {
        adapter.delegate = nil;
    }
    if([adapter getCallback] != nil){
        [adapter setCallback:nil];
    }
}

+ (bool)isPreparedMovieReward:(NSString*)appID{
    NSLog(@"isPreparedMovieReward");
    ADFMovieRewardCocosAdapter* adapter = [ADFMovieRewardNativeManager getAdapter:appID];
    if(adapter != nil){
        return [[adapter getMovieReward] isPrepared];
    }
    return false;
}

+ (void)playMovieReward:(NSString*)appID{
    NSLog(@"playMovieReward");
    ADFMovieRewardCocosAdapter* adapter = [ADFMovieRewardNativeManager getAdapter:appID];
    if(adapter != nil){
        [[adapter getMovieReward] play];
    }
}

#pragma mark - ADFmyMovieReward delegate

/**< 広告の表示準備が終わった */
- (void)AdsFetchCompleted:(NSString *)appID isTestMode_inApp:(BOOL)isTestMode_inApp{
    NSLog(@"AdsFetchCompleted:-");
    [(__bridge CCEAGLView*)(cocos2d::Director::getInstance()->getOpenGLView()->getEAGLView()) setNeedsLayout];
    ADFMovieRewardCocosAdapter* adapter = [ADFMovieRewardNativeManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieRewardDelegate *pDelegate = (Adfurikun::ADFMovieRewardDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->prepareSuccess([ADFMovieRewardNativeManager convCocosParamFormat:appID]);
        }
    }
}

/**< 広告の表示が開始したか */
- (void)AdsDidShow:(NSString *)appID adnetworkKey:(NSString *)adNetworkKey{
    NSLog(@"AdsDidShow:-");
    ADFMovieRewardCocosAdapter* adapter = [ADFMovieRewardNativeManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieRewardDelegate *pDelegate = (Adfurikun::ADFMovieRewardDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->startPlaying(
                                    [ADFMovieRewardNativeManager convCocosParamFormat:appID]
                                    ,[ADFMovieRewardNativeManager convCocosParamFormat:adNetworkKey]
                                    );
        }
    }
}

/**< 広告の表示を最後まで終わったか */
- (void)AdsDidCompleteShow:(NSString *)appID{
    NSLog(@"AdsDidCompleteShow:-");
    ADFMovieRewardCocosAdapter* adapter = [ADFMovieRewardNativeManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieRewardDelegate *pDelegate = (Adfurikun::ADFMovieRewardDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->finishedPlaying([ADFMovieRewardNativeManager convCocosParamFormat:appID]);
        }
    }
}

/**< 動画広告再生エラー時のイベント */
- (void)AdsPlayFailed:(NSString *)appID{
    NSLog(@"AdsPlayFailed:-");
    ADFMovieRewardCocosAdapter* adapter = [ADFMovieRewardNativeManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieRewardDelegate *pDelegate = (Adfurikun::ADFMovieRewardDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->failedPlaying([ADFMovieRewardNativeManager convCocosParamFormat:appID]);
        }}
}

/**< 動画を閉じた時のイベント */
- (void)AdsDidHide:(NSString *)appID{
    NSLog(@"AdsDidHide:-");
    ADFMovieRewardCocosAdapter* adapter = [ADFMovieRewardNativeManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieRewardDelegate *pDelegate = (Adfurikun::ADFMovieRewardDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->adClose([ADFMovieRewardNativeManager convCocosParamFormat:appID]);
        }
    }
}

+ (UIViewController *)getRootViewController{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    if (window == nil) {
        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    }
    return window.rootViewController;
}

+ (ADFMovieRewardCocosAdapter *)getAdapter:(NSString *)appID{
    ADFMovieRewardCocosAdapter* adapter = (ADFMovieRewardCocosAdapter *)[adMovieRewardList objectForKey:appID];
    return (adapter != nil) ? adapter: nil;
}

+ (const char *)convCocosParamFormat:(NSString *)str{
    return (char*)[str UTF8String];
}

- (void)dispose{
    if(adMovieRewardList != nil) {
        [ADFmyMovieReward disposeAll];
        for (id key in [adMovieRewardList keyEnumerator]) {
            ADFMovieRewardCocosAdapter* o = (ADFMovieRewardCocosAdapter*)[adMovieRewardList valueForKey:key];
            if(o != nil) {
                [o dispose];
                o = nil;
                //[o release];
            }
        }
        [adMovieRewardList removeAllObjects];
        adMovieRewardList = nil;
    }
    instance = nil;
}

+ (void)dispose_handle{
    if(instance != nil){
        [instance dispose];
    }
}

@end
