#include "cocos2d.h"
#include "jp_tjkapp_adfurikun_movienative_cocos2dx_AdfurikunNativeActivityBridge.h"
#include "ADFMovieReward.h"

using Adfurikun::ADFMovieNative;
using Adfurikun::ADFMovieNativeDelegate;


JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienative_cocos2dx_AdfurikunNativeActivityBridge_onNativeMovieAdViewLoadFinish(JNIEnv *env, jclass clazz, jstring appId)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    ADFMovieNativeDelegate* listener = ADFMovieNative::getDelegate(id);
    if (listener != nullptr) {
        listener->onNativeMovieAdViewLoadFinish(id);
    }
    
    env->ReleaseStringUTFChars(appId, id);
}

JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienative_cocos2dx_AdfurikunNativeActivityBridge_onNativeMovieAdViewLoadError(JNIEnv *env, jclass clazz, jstring appId, jint errorCode)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    int errorId = errorCode;
    ADFMovieNativeDelegate* listener = ADFMovieNative::getDelegate(id);
    if (listener != nullptr) {
        listener->onNativeMovieAdViewLoadError(id , errorId);
    }
    
    env->ReleaseStringUTFChars(appId, id);
}
  
JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienative_cocos2dx_AdfurikunNativeActivityBridge_onNativeMovieAdViewPlayStart(JNIEnv *env, jclass clazz, jstring appId)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    ADFMovieNativeDelegate* listener = ADFMovieNative::getDelegate(id);
    if (listener != nullptr) {
        listener->onNativeMovieAdViewPlayStart(id);
    }
    
    env->ReleaseStringUTFChars(appId, id);
}

JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienative_cocos2dx_AdfurikunNativeActivityBridge_onNativeMovieAdViewPlayFinish(JNIEnv *env, jclass clazz, jstring appId, jboolean isVideoAd)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    ADFMovieNativeDelegate* listener = ADFMovieNative::getDelegate(id);
    if (listener != nullptr) {
        listener->onNativeMovieAdViewPlayFinish(id , (bool)isVideoAd);
    }
    
    env->ReleaseStringUTFChars(appId, id);
}

JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienative_cocos2dx_AdfurikunNativeActivityBridge_onNativeMovieAdViewPlayFail(JNIEnv *env, jclass clazz, jstring appId, jint errorCode)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    int errorId = errorCode;
    ADFMovieNativeDelegate* listener = ADFMovieNative::getDelegate(id);
    if (listener != nullptr) {
        listener->onNativeMovieAdViewPlayFail(id , errorId);
    }
    
    env->ReleaseStringUTFChars(appId, id);
}