//
//  ADFMovieNativeNativeManager.h
//  adfurikunSample
//
//
//

#ifndef adfurikunSample_ADFMovieNativeNativeManager_h
#define adfurikunSample_ADFMovieNativeNativeManager_h
#import "ADFMovieNativeCocosAdapter.h"

@interface ADFMovieNativeNativeManager : NSObject<ADFMovieNativeCocosAdapterDelegate>
+ (void)initializeMovieNativeWithAppID:(NSString*)appID pattern:(int)layoutPattern xPosition:(int)x yPosition:(int)y width:(int)width height:(int)height;
+ (void)initializeMovieNativeWithAppID:(NSString*)appID pattern:(int)layoutPattern;
+ (void)setDelegate:(NSString *)appID delegate:(void *)delegate;
+ (void *)getDelegate:(NSString *)appID;
+ (void)detachDelegate:(NSString *)appID;
+ (ADFMovieNativeCocosAdapter *)getAdapter:(NSString *)appID;
+ (void)load:(NSString*)appID;
+ (void)playVideo:(NSString*)appID;
+ (void)pauseVideo:(NSString*)appID;
+ (void)startAutoReload:(NSString*)appID;
+ (void)stopAutoReload:(NSString*)appID;
+ (void)showVideo:(NSString*)appID;
+ (void)hideVideo:(NSString*)appID;
+ (void)disableAd:(NSString*)appID;
+ (void)setFrame:(NSString*)appID xPosition:(int)x yPosition:(int)y width:(int)width height:(int)height;
+ (void)setConvertFrame:(NSString*)appID displaySizeW:(float)displaySizeW displaySizeH:(float)displaySizeH xPosition:(float)x yPosition:(float)y width:(float)width height:(float)height;
+ (void)setFrameGravity:(NSString*)appID displaySizeW:(float)displaySizeW displaySizeH:(float)displaySizeH width:(float)width height:(float)height horizontalGravity:(int)horizontalGravity verticalGravity:(int)verticalGravity;
+ (void)setFitWidthFrame:(NSString*)appID displaySizeH:(float)displaySizeH height:(float)height verticalGravity:(int)verticalGravity;
+ (void)dispose_handle;
@end

#endif
