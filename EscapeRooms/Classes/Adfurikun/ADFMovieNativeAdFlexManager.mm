#import "ADFMovieReward.h"
#import "ADFMovieNativeAdFlexManager.h"
#import "platform/ios/CCEAGLView-ios.h"

@interface ADFMovieNativeAdFlexManager()
@end

@implementation ADFMovieNativeAdFlexManager

// クラス自身のインスタンス
static ADFMovieNativeAdFlexManager* instance = nil;
//動画ネイティブアドフレックスのインスタンスを格納する為の配列
__strong static NSMutableDictionary* adMovieNativeAdFlexList = nil;

- (id)init {
    self = [super init];
    if ( self ) {
    }
    return self;
}

+ (void)initializeMovieNativeAdFlexWithAppID:(NSString*)appID {
    if([appID length] < 1){return;}
    if ( instance == nil) {
        instance = [ADFMovieNativeAdFlexManager new];
    }
    if(adMovieNativeAdFlexList == nil){
        adMovieNativeAdFlexList = [@{} mutableCopy];
    }
    
    //iOSが対応バージョンなら、指定した広告枠で動画読み込みを開始
    if(![ADFmyMovieNativeAdFlex isSupportedOSVersion]){
        return;
    }
    [ADFmyMovieNativeAdFlex initWithAppID:appID viewController:[ADFMovieNativeAdFlexManager getRootViewController]];
    //Unity用のアダプタークラスを生成
    ADFMovieNativeAdFlexCocosAdapter* cocosAdapter = [[ADFMovieNativeAdFlexCocosAdapter alloc] initWithAppID:appID];
    cocosAdapter.delegate = instance;
    //アダプターをリストに保存
    [adMovieNativeAdFlexList setObject:cocosAdapter forKey:appID];
}

+ (void)setDelegate:(NSString *)appID delegate:(void *)delegate {
    if([appID length] < 1){ return; }
    //アダプターを取得
    ADFMovieNativeAdFlexCocosAdapter* adapter = [ADFMovieNativeAdFlexManager getAdapter:appID];
    if(adapter != nil){
        [ADFmyMovieNativeAdFlex getInstance:appID delegate:adapter];
        //コールバック(c++)を設定
        [adapter setCallback:delegate];
        //コールバック(Objective-c)を設定
        adapter.delegate = instance;
    }
}

+ (void *)getDelegate:(NSString *)appID{
    if([appID length] < 1){ return nil; }
    ADFMovieNativeAdFlexCocosAdapter* adapter = [ADFMovieNativeAdFlexManager getAdapter:appID];
    if(adapter != nil){
        return [adapter getCallback];
    }
    return nil;
}

+ (void)detachDelegate:(NSString*)appID{
    ADFMovieNativeAdFlexCocosAdapter* adapter = (ADFMovieNativeAdFlexCocosAdapter*)[adMovieNativeAdFlexList valueForKey:appID];
    if(adapter != nil) {
        adapter.delegate = nil;
    }
    if([adapter getCallback] != nil){
        [adapter setCallback:nil];
    }
}

+ (bool)isPreparedMovieNativeAdFlex:(NSString*)appID{
    NSLog(@"isPreparedMovieNativeAdFlex");
    ADFMovieNativeAdFlexCocosAdapter* adapter = [ADFMovieNativeAdFlexManager getAdapter:appID];
    if(adapter != nil){
        return [[adapter getMovieNativeAdFlex] isPrepared];
    }
    return false;
}

+ (void)playMovieNativeAdFlex:(NSString*)appID{
    NSLog(@"playMovieNativeAdFlex");
    ADFMovieNativeAdFlexCocosAdapter* adapter = [ADFMovieNativeAdFlexManager getAdapter:appID];
    if(adapter != nil){
        [[adapter getMovieNativeAdFlex] play];
    }
}

+ (void)finishMovieNativeAdFlex:(NSString*)appID{
    NSLog(@"finishMovieNativeAdFlex");
    ADFMovieNativeAdFlexCocosAdapter* adapter = [ADFMovieNativeAdFlexManager getAdapter:appID];
    if(adapter != nil){
        [[adapter getMovieNativeAdFlex] finish];
    }
}

#pragma mark - ADFmyMovieNativeAdFlex delegate

/**< 広告の表示準備が終わった */
- (void)AdsFetchCompleted:(NSString *)appID isTestMode_inApp:(BOOL)isTestMode_inApp{
    NSLog(@"AdsFetchCompleted:-");
    [(__bridge CCEAGLView*)(cocos2d::Director::getInstance()->getOpenGLView()->getEAGLView()) setNeedsLayout];
    ADFMovieNativeAdFlexCocosAdapter* adapter = [ADFMovieNativeAdFlexManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieNativeAdFlexDelegate *pDelegate = (Adfurikun::ADFMovieNativeAdFlexDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->prepareSuccess([ADFMovieNativeAdFlexManager convCocosParamFormat:appID]);
        }
    }
}

/**< 広告の表示が開始したか */
- (void)AdsDidShow:(NSString *)appID adnetworkKey:(NSString *)adNetworkKey{
    NSLog(@"AdsDidShow:-");
    ADFMovieNativeAdFlexCocosAdapter* adapter = [ADFMovieNativeAdFlexManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieNativeAdFlexDelegate *pDelegate = (Adfurikun::ADFMovieNativeAdFlexDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->startPlaying(
                                    [ADFMovieNativeAdFlexManager convCocosParamFormat:appID]
                                    ,[ADFMovieNativeAdFlexManager convCocosParamFormat:adNetworkKey]
                                    );
        }
    }
}

/**< 広告の表示を最後まで終わったか */
- (void)AdsDidCompleteShow:(NSString *)appID{
    NSLog(@"AdsDidCompleteShow:-");
    ADFMovieNativeAdFlexCocosAdapter* adapter = [ADFMovieNativeAdFlexManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieNativeAdFlexDelegate *pDelegate = (Adfurikun::ADFMovieNativeAdFlexDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->finishedPlaying([ADFMovieNativeAdFlexManager convCocosParamFormat:appID]);
        }
    }
}

/**< 動画広告再生エラー時のイベント */
- (void)AdsPlayFailed:(NSString *)appID{
    NSLog(@"AdsPlayFailed:-");
    ADFMovieNativeAdFlexCocosAdapter* adapter = [ADFMovieNativeAdFlexManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieNativeAdFlexDelegate *pDelegate = (Adfurikun::ADFMovieNativeAdFlexDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->failedPlaying([ADFMovieNativeAdFlexManager convCocosParamFormat:appID]);
        }}
}

/**< 動画を閉じた時のイベント */
- (void)AdsDidHide:(NSString *)appID{
    NSLog(@"AdsDidHide:-");
    ADFMovieNativeAdFlexCocosAdapter* adapter = [ADFMovieNativeAdFlexManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieNativeAdFlexDelegate *pDelegate = (Adfurikun::ADFMovieNativeAdFlexDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->adClose([ADFMovieNativeAdFlexManager convCocosParamFormat:appID]);
        }
    }
}

+ (UIViewController *)getRootViewController{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    if (window == nil) {
        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    }
    return window.rootViewController;
}

+ (ADFMovieNativeAdFlexCocosAdapter *)getAdapter:(NSString *)appID{
    ADFMovieNativeAdFlexCocosAdapter* adapter = (ADFMovieNativeAdFlexCocosAdapter *)[adMovieNativeAdFlexList objectForKey:appID];
    return (adapter != nil) ? adapter: nil;
}

+ (const char *)convCocosParamFormat:(NSString *)str{
    return (char*)[str UTF8String];
}

- (void)dispose{
    if(adMovieNativeAdFlexList != nil) {
        [ADFmyMovieReward disposeAll];
        for (id key in [adMovieNativeAdFlexList keyEnumerator]) {
            ADFMovieNativeAdFlexCocosAdapter* o = (ADFMovieNativeAdFlexCocosAdapter*)[adMovieNativeAdFlexList valueForKey:key];
            if(o != nil) {
                [o dispose];
                o = nil;
                //[o release];
            }
        }
        [adMovieNativeAdFlexList removeAllObjects];
        adMovieNativeAdFlexList = nil;
    }
    instance = nil;
}

+ (void)dispose_handle{
    if(instance != nil){
        [instance dispose];
    }
}

@end
