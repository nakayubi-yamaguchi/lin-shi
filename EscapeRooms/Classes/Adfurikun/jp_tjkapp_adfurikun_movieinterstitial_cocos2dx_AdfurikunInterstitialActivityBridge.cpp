#include "cocos2d.h"
#include "jp_tjkapp_adfurikun_movieinterstitial_cocos2dx_AdfurikunInterstitialActivityBridge.h"
#include "ADFMovieReward.h"

using Adfurikun::ADFMovieInterstitial;
using Adfurikun::ADFMovieInterstitialDelegate;


JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movieinterstitial_cocos2dx_AdfurikunInterstitialActivityBridge_onPrepareSuccess(JNIEnv *env, jclass clazz, jstring appId)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    ADFMovieInterstitialDelegate* listener = ADFMovieInterstitial::getDelegate(id);
    if (listener != nullptr) {
        listener->prepareSuccess(id);
    }
    
    env->ReleaseStringUTFChars(appId, id);
}

JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movieinterstitial_cocos2dx_AdfurikunInterstitialActivityBridge_onStartPlaying(JNIEnv *env, jclass clazz, jstring appId, jstring jStrKey)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    const char* key = env->GetStringUTFChars(jStrKey, 0);
    ADFMovieInterstitialDelegate* listener = ADFMovieInterstitial::getDelegate(id);
    if (listener != nullptr) {
        listener->startPlaying(id, key);
    }
    
    env->ReleaseStringUTFChars(appId, id);
    env->ReleaseStringUTFChars(jStrKey, key);
}

JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movieinterstitial_cocos2dx_AdfurikunInterstitialActivityBridge_onFinishedPlaying(JNIEnv *env, jclass clazz, jstring appId)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    ADFMovieInterstitialDelegate* listener = ADFMovieInterstitial::getDelegate(id);
    if (listener != nullptr) {
        listener->finishedPlaying(id);
    }
    
    env->ReleaseStringUTFChars(appId, id);
}

JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movieinterstitial_cocos2dx_AdfurikunInterstitialActivityBridge_onFailedPlaying(JNIEnv *env, jclass clazz, jstring appId)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    ADFMovieInterstitialDelegate* listener = ADFMovieInterstitial::getDelegate(id);
    if (listener != nullptr) {
        listener->failedPlaying(id);
    }
    
    env->ReleaseStringUTFChars(appId, id);
}

JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movieinterstitial_cocos2dx_AdfurikunInterstitialActivityBridge_onAdClose(JNIEnv *env, jclass clazz, jstring appId)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    ADFMovieInterstitialDelegate* listener = ADFMovieInterstitial::getDelegate(id);
    if (listener != nullptr) {
        listener->adClose(id);
    }
    
    env->ReleaseStringUTFChars(appId, id);
}

