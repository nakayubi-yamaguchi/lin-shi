//
//  RewardCocosConstants.h
//  K2
//
//  Created by yamaguchinarita on 2016/03/11.
//

#ifndef RewardCocosConstants_h
#define RewardCocosConstants_h

// Include any system framework and library headers here that should be included in all compilation units.
// You will also need to set the Prefix Header build setting of one or more of your targets to reference this file.

// 広告IDはこちらで入れてください。
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//iOSの広告枠ID
static const char * MOVIE_REWARD_FACEBOOK_APPID = "";//
static const char * MOVIE_REWARD_APPID = "59e409972e3495381e00093e";//コンテナ
static const char * MOVIE_REWARD_APPID_VIIDLE = "7c7ff0511761462797c9a2980be8b99e";

#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
//Androidの広告枠ID
static const char * MOVIE_REWARD_FACEBOOK_APPID = "";//
static const char * MOVIE_REWARD_APPID = "59e40a962e3495181e000939";//コンテナ/：AppActivity側にIDを入力している。
static const char * MOVIE_REWARD_APPID_VIIDLE = "547fbc20e23e438286f3773fde092f4f";


#endif


#endif /* RewardCocosConstants_pch */
