#pragma mark - cocos native plugin

#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "ADFMovieReward.h"
#import "ADFMovieRewardNativeManager.h"
#import "ADFMovieInterstitialNativeManager.h"
#import "ADFMovieNativeNativeManager.h"
#import "ADFMovieNativeAdFlexManager.h"


namespace Adfurikun {
    
    void ADFMovieReward::initializeWithAppID(const char* appID){
        [ADFMovieRewardNativeManager initializeMovieRewardWithAppID:
         [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]];
    }
    
    
    void ADFMovieReward::setDelegate(const char* appID, ADFMovieRewardDelegate* delegate){
        [ADFMovieRewardNativeManager setDelegate:
         [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]
                                        delegate:delegate];
    }
    
    ADFMovieRewardDelegate* ADFMovieReward::getDelegate(const char* appID){
        ADFMovieRewardDelegate* o = (ADFMovieRewardDelegate *)[ADFMovieRewardNativeManager getDelegate:
                                                               [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]
                                                               ];
        if(o != NULL){
            return o;
        }
        return NULL;
    }
    
    void ADFMovieReward::detachDelegate(const char* appID){
        [ADFMovieRewardNativeManager detachDelegate:
         [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]];
    }
    
    bool ADFMovieReward::isPrepared(const char* appID){
        return [ADFMovieRewardNativeManager isPreparedMovieReward:
                [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]];
        return false;
    }
    
    void ADFMovieReward::play(const char* appID){
        [ADFMovieRewardNativeManager playMovieReward:
         [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]];
    }
    
    void ADFMovieReward::dispose(){
        [ADFMovieRewardNativeManager dispose_handle];
    }
    
    ADFMovieRewardDelegate::~ADFMovieRewardDelegate(){
        
    }
    
    //封印
    ADFMovieReward::ADFMovieReward(){}
    
    
    void ADFMovieInterstitial::initializeWithAppID(const char* appID){
        [ADFMovieInterstitialNativeManager initializeMovieInterstitialWithAppID:
         [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]];
    }
    
    
    void ADFMovieInterstitial::setDelegate(const char* appID, ADFMovieInterstitialDelegate* delegate){
        [ADFMovieInterstitialNativeManager setDelegate:
         [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]
                                              delegate:delegate];
    }
    
    ADFMovieInterstitialDelegate* ADFMovieInterstitial::getDelegate(const char* appID){
        ADFMovieInterstitialDelegate* o = (ADFMovieInterstitialDelegate *)[ADFMovieInterstitialNativeManager getDelegate:
                                                                           [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]
                                                                           ];
        if(o != NULL){
            return o;
        }
        return NULL;
    }
    
    void ADFMovieInterstitial::detachDelegate(const char* appID){
        [ADFMovieInterstitialNativeManager detachDelegate:
         [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]];
    }
    
    bool ADFMovieInterstitial::isPrepared(const char* appID){
        return [ADFMovieInterstitialNativeManager isPreparedMovieInterstitial:
                [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]];
        return false;
    }
    
    void ADFMovieInterstitial::play(const char* appID){
        [ADFMovieInterstitialNativeManager playMovieInterstitial:
         [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]];
    }
    
    void ADFMovieInterstitial::dispose(){
        [ADFMovieInterstitialNativeManager dispose_handle];
    }
    
    ADFMovieInterstitialDelegate::~ADFMovieInterstitialDelegate(){
        
    }
    
    //封印
    ADFMovieInterstitial::ADFMovieInterstitial(){}
	
    void ADFMovieNative::initializeWithAppID(const char* appId, int layoutPattern, int x, int y, int width, int height)
    {
		[ADFMovieNativeNativeManager initializeMovieNativeWithAppID:
		 [NSString stringWithCString:appId encoding:NSUTF8StringEncoding] pattern:layoutPattern xPosition:x yPosition:y width:width height:height];
    }
    
    void ADFMovieNative::initializeWithAppID(const char* appId, int layoutPattern)
    {
      [ADFMovieNativeNativeManager initializeMovieNativeWithAppID:
       [NSString stringWithCString:appId encoding:NSUTF8StringEncoding] pattern:layoutPattern];
    }
	
    void ADFMovieNative::setDelegate(const char* appId, ADFMovieNativeDelegate* delegate)
    {
        [ADFMovieNativeNativeManager setDelegate:
         [NSString stringWithCString:appId encoding:NSUTF8StringEncoding]
                                              delegate:delegate];
    }
	
	ADFMovieNativeDelegate* ADFMovieNative::getDelegate(const char* appId)
    {
        ADFMovieNativeDelegate* o = (ADFMovieNativeDelegate *)[ADFMovieNativeNativeManager getDelegate:
                                                                           [NSString stringWithCString:appId encoding:NSUTF8StringEncoding]
                                                                           ];
        if(o != NULL){
            return o;
        }
        return NULL;
    }
	
    void ADFMovieNative::detachDelegate(const char* appId)
    {
        [ADFMovieNativeNativeManager detachDelegate:
         [NSString stringWithCString:appId encoding:NSUTF8StringEncoding]];
    }
	
    void ADFMovieNative::load(const char* appId)
    {
        [ADFMovieNativeNativeManager load:
         [NSString stringWithCString:appId encoding:NSUTF8StringEncoding]];
    }
	
    void ADFMovieNative::playVideo(const char* appId)
    {
        [ADFMovieNativeNativeManager playVideo:
         [NSString stringWithCString:appId encoding:NSUTF8StringEncoding]];
    }
	
    void ADFMovieNative::pauseVideo(const char* appId)
    {
        [ADFMovieNativeNativeManager pauseVideo:
         [NSString stringWithCString:appId encoding:NSUTF8StringEncoding]];
    }
  
    void ADFMovieNative::startAutoReload(const char* appId)
    {
        [ADFMovieNativeNativeManager startAutoReload:
        [NSString stringWithCString:appId encoding:NSUTF8StringEncoding]];
    }
  
    void ADFMovieNative::stopAutoReload(const char* appId)
    {
        [ADFMovieNativeNativeManager stopAutoReload:
        [NSString stringWithCString:appId encoding:NSUTF8StringEncoding]];
    }
  
    void ADFMovieNative::showVideo(const char* appId)
    {
      [ADFMovieNativeNativeManager showVideo:
       [NSString stringWithCString:appId encoding:NSUTF8StringEncoding]];
    }
  
    void ADFMovieNative::hideVideo(const char* appId)
    {
      [ADFMovieNativeNativeManager hideVideo:
       [NSString stringWithCString:appId encoding:NSUTF8StringEncoding]];
    }
	
    void ADFMovieNative::disableAd(const char* appId)
    {
        [ADFMovieNativeNativeManager disableAd:
         [NSString stringWithCString:appId encoding:NSUTF8StringEncoding]];
    }
  
    void ADFMovieNative::setFrame(const char* appId, int x, int y, int width, int height)
    {
        [ADFMovieNativeNativeManager setFrame:
        [NSString stringWithCString:appId encoding:NSUTF8StringEncoding] xPosition:x yPosition:y width:width height:height];
    }
    
    void ADFMovieNative::setConvertFrame(const char* appId, float displaySizeW, float displaySizeH, float x, float y, float width, float height)
    {
      [ADFMovieNativeNativeManager setConvertFrame:[NSString stringWithCString:appId encoding:NSUTF8StringEncoding] displaySizeW:displaySizeW displaySizeH:displaySizeH xPosition:x yPosition:y width:width height:height];
    }
    
    void ADFMovieNative::setFrameGravity(const char* appId, float displaySizeW, float displaySizeH, float width, float height , int horizontalGravity, int verticalGravity)
    {
      [ADFMovieNativeNativeManager setFrameGravity:[NSString stringWithCString:appId encoding:NSUTF8StringEncoding] displaySizeW:displaySizeW displaySizeH:displaySizeH width:width height:height horizontalGravity:horizontalGravity verticalGravity:verticalGravity];
    }
  
    void ADFMovieNative::setFitWidthFrame(const char* appId, float displaySizeH, float height, int verticalGravity)
    {
      [ADFMovieNativeNativeManager setFitWidthFrame:[NSString stringWithCString:appId encoding:NSUTF8StringEncoding] displaySizeH:displaySizeH height:height verticalGravity:verticalGravity];
      
    }
	
    ADFMovieNativeDelegate::~ADFMovieNativeDelegate(){
        
    }
	
    ADFMovieNative::ADFMovieNative(){}
    
    
    void ADFMovieNativeAdFlex::initializeWithAppID(const char* appID){
        [ADFMovieNativeAdFlexManager initializeMovieNativeAdFlexWithAppID:
         [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]];
    }
    
    
    void ADFMovieNativeAdFlex::setDelegate(const char* appID, ADFMovieNativeAdFlexDelegate* delegate){
        [ADFMovieNativeAdFlexManager setDelegate:
         [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]
                                              delegate:delegate];
    }
    
    ADFMovieNativeAdFlexDelegate* ADFMovieNativeAdFlex::getDelegate(const char* appID){
        ADFMovieNativeAdFlexDelegate* o = (ADFMovieNativeAdFlexDelegate *)[ADFMovieNativeAdFlexManager getDelegate:[NSString stringWithCString:appID encoding:NSUTF8StringEncoding]];
        if(o != NULL){
            return o;
        }
        return NULL;
    }
    
    void ADFMovieNativeAdFlex::detachDelegate(const char* appID){
        [ADFMovieNativeAdFlexManager detachDelegate:
         [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]];
    }
    
    bool ADFMovieNativeAdFlex::isPrepared(const char* appID){
        return [ADFMovieNativeAdFlexManager isPreparedMovieNativeAdFlex:
                [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]];
        return false;
    }
    
    void ADFMovieNativeAdFlex::play(const char* appID){
        [ADFMovieNativeAdFlexManager playMovieNativeAdFlex:
         [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]];
    }
    
    void ADFMovieNativeAdFlex::closeAd(const char *appID){
        [ADFMovieNativeAdFlexManager finishMovieNativeAdFlex:
         [NSString stringWithCString:appID encoding:NSUTF8StringEncoding]];
    }
    
    void ADFMovieNativeAdFlex::dispose(){
        [ADFMovieNativeAdFlexManager dispose_handle];
    }
    
    ADFMovieNativeAdFlexDelegate::~ADFMovieNativeAdFlexDelegate(){
        
    }
    
    //封印
    ADFMovieNativeAdFlex::ADFMovieNativeAdFlex(){}
}

#endif
