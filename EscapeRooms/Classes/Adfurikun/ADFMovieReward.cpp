#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include "platform/android/jni/JniHelper.h"
#include "ADFMovieReward.h"
#include <string>
#include <map>

namespace Adfurikun {
  
  /** 広告枠IDに対応するデリゲートを管理するMap */
  std::map<std::string, ADFMovieRewardDelegate*> androidRewardDelegateMap;
  std::map<std::string, ADFMovieInterstitialDelegate*> androidInterstitialDelegateMap;
  std::map<std::string, ADFMovieNativeDelegate*> androidNativeDelegateMap;
  std::map<std::string, ADFMovieNativeAdFlexDelegate*> androidNativeAdFlexDelegateMap;
  
  ADFMovieRewardDelegate::~ADFMovieRewardDelegate() {}
  ADFMovieInterstitialDelegate::~ADFMovieInterstitialDelegate() {}
  ADFMovieNativeDelegate::~ADFMovieNativeDelegate() {}
  ADFMovieNativeAdFlexDelegate::~ADFMovieNativeAdFlexDelegate() {}
  
  /**
   初期化を行う
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieReward::initializeWithAppID(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/moviereward/cocos2dx/AdfurikunRewardActivityBridge", "initializeWithAppID", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }
  
  /**
   初期化を行う
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieInterstitial::initializeWithAppID(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movieinterstitial/cocos2dx/AdfurikunInterstitialActivityBridge", "initializeWithAppID", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }
  
  /**
   初期化を行う
   @param const char* (appID) 広告枠ID
   @param int (layoutPattern) レイアウトの種類
   @param int (x) レイアウトのx座標
   @param int (y) レイアウトのy座標
   @param int (width) レイアウトの横幅
   @param int (height) レイアウトの高さ
   */
  void ADFMovieNative::initializeWithAppID(const char* appId, int layoutPattern, int x, int y, int width, int height)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienative/cocos2dx/AdfurikunNativeActivityBridge", "initializeWithAppID", "(Ljava/lang/String;IIIII)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId, layoutPattern, x, y, width, height);
    }
  }
  
  /**
   初期化を行う
   @param const char* (appID) 広告枠ID
   @param int (layoutPattern) レイアウトの種類
   */
  void ADFMovieNative::initializeWithAppID(const char* appId, int layoutPattern)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienative/cocos2dx/AdfurikunNativeActivityBridge", "initializeWithAppID", "(Ljava/lang/String;I)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId, layoutPattern);
    }
  }

  /**
   初期化を行う
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieNativeAdFlex::initializeWithAppID(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienativeadflex/cocos2dx/AdfurikunNativeAdFlexActivityBridge", "initializeWithAppID", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }
  
  /**
   コールバックをセット
   @param const char* (appID) 広告枠ID
   @param NativeDelegate* delegate コールバック先
   */
  void ADFMovieReward::setDelegate(const char* appId, ADFMovieRewardDelegate* delegate)
  {
    const std::string strAppId(appId);
    androidRewardDelegateMap[strAppId] = delegate;
    
    // すでに準備完了の場合/Users/z.ryo.nozaki/adfully/cocos2dx-android/adfurikunmoviereward_cocos2dx/AdfurikunSampleCocos2dx/Classes/adfurikun/ADFMovieReward.cpp
    if (ADFMovieReward::isPrepared(appId)) {
      // 準備完了を通知する
      delegate->prepareSuccess(appId);
    }
  }
  
  /**
   コールバックをセット
   @param const char* (appID) 広告枠ID
   @param NativeDelegate* delegate コールバック先
   */
  void ADFMovieInterstitial::setDelegate(const char* appId, ADFMovieInterstitialDelegate* delegate)
  {
    const std::string strAppId(appId);
    androidInterstitialDelegateMap[strAppId] = delegate;
    
    // すでに準備完了の場合
    if (ADFMovieInterstitial::isPrepared(appId)) {
      // 準備完了を通知する
      delegate->prepareSuccess(appId);
    }
  }
  
  /**
   コールバックをセット
   @param const char* (appID) 広告枠ID
   @param ADFMovieNativeDelegate* delegate コールバック先
   */
  void ADFMovieNative::setDelegate(const char* appId, ADFMovieNativeDelegate* delegate)
  {
    const std::string strAppId(appId);
    androidNativeDelegateMap[strAppId] = delegate;
  }

  /**
   コールバックをセット
   @param const char* (appID) 広告枠ID
   @param ADFMovieNativeAdFlexDelegate* delegate コールバック先
   */
  void ADFMovieNativeAdFlex::setDelegate(const char* appId, ADFMovieNativeAdFlexDelegate* delegate)
  {
    const std::string strAppId(appId);
    androidNativeAdFlexDelegateMap[strAppId] = delegate;

    // すでに準備完了の場合
    if (ADFMovieNativeAdFlex::isPrepared(appId)) {
      // 準備完了を通知する
      delegate->prepareSuccess(appId);
    }
  }
  
  ADFMovieRewardDelegate* ADFMovieReward::getDelegate(const char* appId)
  {
    const std::string strAppId(appId);
    auto itr = androidRewardDelegateMap.find(strAppId);
    if (itr != androidRewardDelegateMap.end()) {
      return androidRewardDelegateMap[strAppId];
    } else {
      return nullptr;
    }
  }
  
  ADFMovieInterstitialDelegate* ADFMovieInterstitial::getDelegate(const char* appId)
  {
    const std::string strAppId(appId);
    auto itr = androidInterstitialDelegateMap.find(strAppId);
    if (itr != androidInterstitialDelegateMap.end()) {
      return androidInterstitialDelegateMap[strAppId];
    } else {
      return nullptr;
    }
  }
  
  ADFMovieNativeDelegate* ADFMovieNative::getDelegate(const char* appId)
  {
    const std::string strAppId(appId);
    auto itr = androidNativeDelegateMap.find(strAppId);
    if (itr != androidNativeDelegateMap.end()) {
      return androidNativeDelegateMap[strAppId];
    } else {
      return nullptr;
    }
  }

  ADFMovieNativeAdFlexDelegate* ADFMovieNativeAdFlex::getDelegate(const char* appId)
  {
    const std::string strAppId(appId);
    auto itr = androidNativeAdFlexDelegateMap.find(strAppId);
    if (itr != androidNativeAdFlexDelegateMap.end()) {
      return androidNativeAdFlexDelegateMap[strAppId];
    } else {
      return nullptr;
    }
  }
  
  /**
   動画の再生準備が完了しているか？
   @param const char* (appID) 広告枠ID
   @return bool 動画の再生準備が完了しているか？
   */
  
  bool ADFMovieReward::isPrepared(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/moviereward/cocos2dx/AdfurikunRewardActivityBridge", "isPrepared", "(Ljava/lang/String;)Z")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      return mi.env->CallStaticBooleanMethod(mi.classID, mi.methodID, utfAppId);
    }
    
    return false;
  }
  
  /**
   動画の再生準備が完了しているか？
   @param const char* (appID) 広告枠ID
   @return bool 動画の再生準備が完了しているか？
   */
  
  bool ADFMovieInterstitial::isPrepared(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movieinterstitial/cocos2dx/AdfurikunInterstitialActivityBridge", "isPrepared", "(Ljava/lang/String;)Z")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      return mi.env->CallStaticBooleanMethod(mi.classID, mi.methodID, utfAppId);
    }
    
    return false;
  }

  /**
   動画の再生準備が完了しているか？
   @param const char* (appID) 広告枠ID
   @return bool 動画の再生準備が完了しているか？
   */
  
  bool ADFMovieNativeAdFlex::isPrepared(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienativeadflex/cocos2dx/AdfurikunNativeAdFlexActivityBridge", "isPrepared", "(Ljava/lang/String;)Z")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      return mi.env->CallStaticBooleanMethod(mi.classID, mi.methodID, utfAppId);
    }
    
    return false;
  }
  
  /**
   動画を再生する
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieReward::play(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/moviereward/cocos2dx/AdfurikunRewardActivityBridge", "play", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }
  
  /**
   動画を再生する
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieInterstitial::play(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movieinterstitial/cocos2dx/AdfurikunInterstitialActivityBridge", "play", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }

  /**
   動画を再生する
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieNativeAdFlex::play(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienativeadflex/cocos2dx/AdfurikunNativeAdFlexActivityBridge", "play", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }

  /**
   動画を終了する
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieNativeAdFlex::closeAd(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienativeadflex/cocos2dx/AdfurikunNativeAdFlexActivityBridge", "closeAd", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }
  
  /**
   アドフリくん動画リワードSDKに関するリソースを開放する
   */
  void ADFMovieReward::dispose()
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/moviereward/cocos2dx/AdfurikunRewardActivityBridge", "dispose", "()V")) {
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID);
    }
    
    androidRewardDelegateMap.clear();
  }
  
  /**
   アドフリくん動画インターステシャルSDKに関するリソースを開放する
   */
  void ADFMovieInterstitial::dispose()
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movieinterstitial/cocos2dx/AdfurikunInterstitialActivityBridge", "dispose", "()V")) {
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID);
    }
    
    androidInterstitialDelegateMap.clear();
  }

  /**
   アドフリくん動画ネイティブアドフレックスSDKに関するリソースを開放する
   */
  void ADFMovieNativeAdFlex::dispose()
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienativeadflex/cocos2dx/AdfurikunNativeAdFlexActivityBridge", "dispose", "()V")) {
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID);
    }
    
    androidRewardDelegateMap.clear();
  }
  
  /**
   セットしたコールバックを外す
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieReward::detachDelegate(const char* appId)
  {
    const std::string strAppId(appId);
    auto itr = androidRewardDelegateMap.find(strAppId);
    if (itr != androidRewardDelegateMap.end()) {
      androidRewardDelegateMap.erase(itr);
    }
  }
  
  /**
   セットしたコールバックを外す
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieInterstitial::detachDelegate(const char* appId)
  {
    const std::string strAppId(appId);
    auto itr = androidInterstitialDelegateMap.find(strAppId);
    if (itr != androidInterstitialDelegateMap.end()) {
      androidInterstitialDelegateMap.erase(itr);
    }
  }
  
  /**
   セットしたコールバックを外す
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieNative::detachDelegate(const char* appId)
  {
    const std::string strAppId(appId);
    auto itr = androidNativeDelegateMap.find(strAppId);
    if (itr != androidNativeDelegateMap.end()) {
      androidNativeDelegateMap.erase(itr);
    }
  }

  /**
   セットしたコールバックを外す
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieNativeAdFlex::detachDelegate(const char* appId)
  {
    closeAd(appId);
    const std::string strAppId(appId);
    auto itr = androidNativeAdFlexDelegateMap.find(strAppId);
    if (itr != androidNativeAdFlexDelegateMap.end()) {
      androidNativeAdFlexDelegateMap.erase(itr);
    }
  }
  
  /**
   動画の広告情報を読み込む
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieNative::load(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienative/cocos2dx/AdfurikunNativeActivityBridge", "load", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }
  
  /**
   動画の再生をする
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieNative::playVideo(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienative/cocos2dx/AdfurikunNativeActivityBridge", "playVideo", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }
  
  /**
   動画の再生を一時停止する
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieNative::pauseVideo(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienative/cocos2dx/AdfurikunNativeActivityBridge", "pauseVideo", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }
  
  /**
   動画の広告をviewから削除する
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieNative::disableAd(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienative/cocos2dx/AdfurikunNativeActivityBridge", "disableAd", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }
  
  /**
   動画の広告の自動切り替えをonにする
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieNative::startAutoReload(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienative/cocos2dx/AdfurikunNativeActivityBridge", "startAutoReload", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }
  
  /**
   動画の広告の自動切り替えをoffにする
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieNative::stopAutoReload(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienative/cocos2dx/AdfurikunNativeActivityBridge", "stopAutoReload", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }
  
  /**
   動画の広告を表示する
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieNative::showVideo(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienative/cocos2dx/AdfurikunNativeActivityBridge", "showAd", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }
  
  /**
   動画の広告を非表示にする
   @param const char* (appID) 広告枠ID
   */
  void ADFMovieNative::hideVideo(const char* appId)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienative/cocos2dx/AdfurikunNativeActivityBridge", "hideAd", "(Ljava/lang/String;)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId);
    }
  }
  
  /**
   動画広告の座標と位置を変更する
   @param const char* (appID) 広告枠ID
   @param int (x) レイアウトのx座標
   @param int (y) レイアウトのy座標
   @param int (width) レイアウトの横幅
   @param int (height) レイアウトの高さ
   */
  void ADFMovieNative::setFrame(const char* appId, int x, int y, int width, int height)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienative/cocos2dx/AdfurikunNativeActivityBridge", "setFrame", "(Ljava/lang/String;IIII)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId, x, y, width, height);
    }
  }
  
  /**
   動画広告の座標と位置を変更する
   @param const char* (appID) 広告枠ID
   @param float (displaySizeW) cocos2dxの画面サイズ(幅)
   @param float (displaySizeH) cocos2dxの画面サイズ(高さ)
   @param float (x) レイアウトのx座標
   @param float (y) レイアウトのy座標
   @param float (width) レイアウトの横幅
   @param float (height) レイアウトの高さ
   */
  void ADFMovieNative::setConvertFrame(const char* appId, float displaySizeW, float displaySizeH, float x, float y, float width, float height)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienative/cocos2dx/AdfurikunNativeActivityBridge", "setConvertFrame", "(Ljava/lang/String;FFFFFF)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId, displaySizeW, displaySizeH, x, y, width, height);
    }
  }

  /**
   動画広告のGravityとサイズを変更
   @param const char* (appID) 広告枠ID
   @param float (displaySizeW) 画面サイズ(幅)
   @param float (displaySizeH) 画面サイズ(高さ)
   @param float (width) レイアウトの横幅
   @param float (height) レイアウトの高さ
   @param int (horizontalGravity) 水平方向 Gravity 0:LEFT 1:CENTER 2:RIGHT
   @param int (verticalGravity) 垂直方向 Gravity 0:TOP 1:CENTER 2:BOTTOM
   */
  void ADFMovieNative::setFrameGravity(const char* appId, float displaySizeW, float displaySizeH,float width, float height, int horizontalGravity, int verticalGravity)
  {
    cocos2d::JniMethodInfo mi;
    if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienative/cocos2dx/AdfurikunNativeActivityBridge", "setFrameGravity", "(Ljava/lang/String;FFFFII)V")) {
      jstring utfAppId = mi.env->NewStringUTF(appId);
      mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId, displaySizeW, displaySizeH, width, height , horizontalGravity , verticalGravity);
    }
  }

  /**
     動画広告をView横幅サイズに変更
     @param const char* (appID) 広告枠ID
     @param float (displaySizeH) 画面サイズ(高さ)
     @param float (height) レイアウトの高さ
     @param int (verticalGravity) 垂直方向 Gravity 0:TOP 1:CENTER 2:BOTTOM
  */
  void ADFMovieNative::setFitWidthFrame(const char* appId, float displaySizeH, float height, int verticalGravity)
    {
      cocos2d::JniMethodInfo mi;
      if (cocos2d::JniHelper::getStaticMethodInfo(mi, "jp/tjkapp/adfurikun/movienative/cocos2dx/AdfurikunNativeActivityBridge", "setFitWidthFrame", "(Ljava/lang/String;FFI)V")) {
        jstring utfAppId = mi.env->NewStringUTF(appId);
        mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, utfAppId, displaySizeH, height, verticalGravity);
      }
    }
}

#endif
