#import "ADFMovieReward.h"
#import "ADFMovieInterstitialNativeManager.h"
#import "platform/ios/CCEAGLView-ios.h"

@interface ADFMovieInterstitialNativeManager()
@end

@implementation ADFMovieInterstitialNativeManager

// クラス自身のインスタンス
static ADFMovieInterstitialNativeManager* instance = nil;
//動画リワードのインスタンスを格納する為の配列
__strong static NSMutableDictionary* adMovieInterstitialList = nil;

- (id)init {
    self = [super init];
    if ( self ) {
    }
    return self;
}

+ (void)initializeMovieInterstitialWithAppID:(NSString*)appID {
    if([appID length] < 1){return;}
    if ( instance == nil) {
        instance = [ADFMovieInterstitialNativeManager new];
    }
    if(adMovieInterstitialList == nil){
        adMovieInterstitialList = [@{} mutableCopy];
    }
    
    //iOSが対応バージョンなら、指定した広告枠で動画読み込みを開始
    if(![ADFmyMovieInterstitial isSupportedOSVersion]){
        return;
    }
    [ADFmyMovieInterstitial initWithAppID:appID viewController:[ADFMovieInterstitialNativeManager getRootViewController]];
    //Unity用のアダプタークラスを生成
    ADFMovieInterstitialCocosAdapter* cocosAdapter =
    [[ADFMovieInterstitialCocosAdapter alloc] initWithAppID:appID];
    cocosAdapter.delegate = instance;
    //アダプターをリストに保存
    [adMovieInterstitialList setObject:cocosAdapter forKey:appID];
}

+ (void)setDelegate:(NSString *)appID delegate:(void *)delegate {
    if([appID length] < 1){ return; }
    //アダプターを取得
    ADFMovieInterstitialCocosAdapter* adapter = [ADFMovieInterstitialNativeManager getAdapter:appID];
    if(adapter != nil){
        [ADFmyMovieInterstitial getInstance:appID delegate:adapter];
        //コールバック(c++)を設定
        [adapter setCallback:delegate];
        //コールバック(Objective-c)を設定
        adapter.delegate = instance;
    }
}

+ (void *)getDelegate:(NSString *)appID{
    if([appID length] < 1){ return nil; }
    ADFMovieInterstitialCocosAdapter* adapter = [ADFMovieInterstitialNativeManager getAdapter:appID];
    if(adapter != nil){
        return [adapter getCallback];
    }
    return nil;
}

+ (void)detachDelegate:(NSString*)appID{
    ADFMovieInterstitialCocosAdapter* adapter = (ADFMovieInterstitialCocosAdapter*)[adMovieInterstitialList valueForKey:appID];
    if(adapter != nil) {
        adapter.delegate = nil;
    }
    if([adapter getCallback] != nil){
        [adapter setCallback:nil];
    }
}

+ (bool)isPreparedMovieInterstitial:(NSString*)appID{
    NSLog(@"isPreparedMovieInterstitial");
    ADFMovieInterstitialCocosAdapter* adapter = [ADFMovieInterstitialNativeManager getAdapter:appID];
    if(adapter != nil){
        return [[adapter getMovieInterstitial] isPrepared];
    }
    return false;
}

+ (void)playMovieInterstitial:(NSString*)appID{
    NSLog(@"playMovieInterstitial");
    ADFMovieInterstitialCocosAdapter* adapter = [ADFMovieInterstitialNativeManager getAdapter:appID];
    if(adapter != nil){
        [[adapter getMovieInterstitial] play];
    }
}

#pragma mark - ADFmyMovieReward delegate

/**< 広告の表示準備が終わった */
- (void)AdsFetchCompleted:(NSString *)appID isTestMode_inApp:(BOOL)isTestMode_inApp{
    NSLog(@"AdsFetchCompleted:-");
    [(__bridge CCEAGLView*)(cocos2d::Director::getInstance()->getOpenGLView()->getEAGLView()) setNeedsLayout];
    ADFMovieInterstitialCocosAdapter* adapter = [ADFMovieInterstitialNativeManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieInterstitialDelegate *pDelegate = (Adfurikun::ADFMovieInterstitialDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->prepareSuccess([ADFMovieInterstitialNativeManager convCocosParamFormat:appID]);
        }
    }
}

/**< 広告の表示が開始したか */
- (void)AdsDidShow:(NSString *)appID adnetworkKey:(NSString *)adNetworkKey{
    NSLog(@"AdsDidShow:-");
    ADFMovieInterstitialCocosAdapter* adapter = [ADFMovieInterstitialNativeManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieInterstitialDelegate *pDelegate = (Adfurikun::ADFMovieInterstitialDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->startPlaying(
                                    [ADFMovieInterstitialNativeManager convCocosParamFormat:appID]
                                    ,[ADFMovieInterstitialNativeManager convCocosParamFormat:adNetworkKey]
                                    );
        }
    }
}

/**< 広告の表示を最後まで終わったか */
- (void)AdsDidCompleteShow:(NSString *)appID{
    NSLog(@"AdsDidCompleteShow:-");
    ADFMovieInterstitialCocosAdapter* adapter = [ADFMovieInterstitialNativeManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieInterstitialDelegate *pDelegate = (Adfurikun::ADFMovieInterstitialDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->finishedPlaying([ADFMovieInterstitialNativeManager convCocosParamFormat:appID]);
        }
    }
}

/**< 動画広告再生エラー時のイベント */
- (void)AdsPlayFailed:(NSString *)appID{
    NSLog(@"AdsPlayFailed:-");
    ADFMovieInterstitialCocosAdapter* adapter = [ADFMovieInterstitialNativeManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieInterstitialDelegate *pDelegate = (Adfurikun::ADFMovieInterstitialDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->failedPlaying([ADFMovieInterstitialNativeManager convCocosParamFormat:appID]);
        }}
}

/**< 動画を閉じた時のイベント */
- (void)AdsDidHide:(NSString *)appID{
    NSLog(@"AdsDidHide:-");
    ADFMovieInterstitialCocosAdapter* adapter = [ADFMovieInterstitialNativeManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieInterstitialDelegate *pDelegate = (Adfurikun::ADFMovieInterstitialDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->adClose([ADFMovieInterstitialNativeManager convCocosParamFormat:appID]);
        }
    }
}

+ (UIViewController *)getRootViewController{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    if (window == nil) {
        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    }
    return window.rootViewController;
}

+ (ADFMovieInterstitialCocosAdapter *)getAdapter:(NSString *)appID{
    ADFMovieInterstitialCocosAdapter* adapter = (ADFMovieInterstitialCocosAdapter *)[adMovieInterstitialList objectForKey:appID];
    return (adapter != nil) ? adapter: nil;
}

+ (const char *)convCocosParamFormat:(NSString *)str{
    return (char*)[str UTF8String];
}

- (void)dispose{
    if(adMovieInterstitialList != nil) {
        [ADFmyMovieReward disposeAll];
        for (id key in [adMovieInterstitialList keyEnumerator]) {
            ADFMovieInterstitialCocosAdapter* o = (ADFMovieInterstitialCocosAdapter*)[adMovieInterstitialList valueForKey:key];
            if(o != nil) {
                [o dispose];
                o = nil;
                //[o release];
            }
        }
        [adMovieInterstitialList removeAllObjects];
        adMovieInterstitialList = nil;
    }
    instance = nil;
}

+ (void)dispose_handle{
    if(instance != nil){
        [instance dispose];
    }
}

@end
