#import <Foundation/Foundation.h>
#import <ADFMovieReward/ADFmyMovieNativeAdView.h>

@protocol ADFMovieNativeCocosAdapterDelegate
@optional
- (void)onNativeMovieAdViewLoadFinish:(NSString *)appID;
- (void)onNativeMovieAdViewLoadError:(NSString *)appID errorCode:(int)errorCode;
- (void)onNativeMovieAdViewPlayStart:(NSString *)appID;
- (void)onNativeMovieAdViewPlayFinish:(NSString *)appID isVideo:(BOOL)isVideo;
- (void)onNativeMovieAdViewPlayFail:(NSString *)appID errorCode:(int)errorCode;

@end

@interface ADFMovieNativeCocosAdapter : NSObject <ADFmyMovieNativeAdViewDelegate , ADFmyNativeAdViewVideoDelegate>
@property (nonatomic, strong) NSObject<ADFMovieNativeCocosAdapterDelegate> *delegate;

- (id)initWithAppID:(NSString *)appID pattern:(int)pattern xPosition:(int)x yPosition:(int)y width:(int)width height:(int)height;
- (ADFmyMovieNativeAdView *)getMovieNative;
- (int)getX;
- (int)getY;
- (int)getWidth;
- (int)getHeight;
- (void)setFrame:(int)x yPosition:(int)y width:(int)width height:(int)height;
- (void)setCallback:(void* )object;
- (void *)getCallback;
- (void)dispose;


/**< 広告の読み込みが完了した時のイベント */
- (void)onNativeMovieAdViewLoadFinish:(NSString *)appID;

/**< 広告の読み込みが失敗した時のイベント */
- (void)onNativeMovieAdViewLoadError:(ADFMovieError *)error appID:(NSString *)appID;

/**< 広告の再生が開始した時のイベント */
- (void)onNativeMovieAdViewPlayStart:(NSString *)appID;

/**< 広告の再生が完了した時のイベント */
- (void)onNativeMovieAdViewPlayFinish:(NSString *)appID isVideo:(BOOL)isVideo;

/**< 広告の再生が失敗した時のイベント */
- (void)onNativeMovieAdViewPlayFail:(NSString *)appID error:(ADFMovieError *)error;

@end

