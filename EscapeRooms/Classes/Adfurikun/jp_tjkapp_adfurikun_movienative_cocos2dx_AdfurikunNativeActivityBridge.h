#include <jni.h>

#ifndef _Included_org_cocos2dx_cpp_AppActivity
#define _Included_org_cocos2dx_cpp_AppActivity

extern "C" {
    
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienative_cocos2dx_AdfurikunNativeActivityBridge_onNativeMovieAdViewLoadFinish
    (JNIEnv *, jclass, jstring);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienative_cocos2dx_AdfurikunNativeActivityBridge_onNativeMovieAdViewLoadError
    (JNIEnv *, jclass, jstring, jint);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienative_cocos2dx_AdfurikunNativeActivityBridge_onNativeMovieAdViewPlayStart
    (JNIEnv *, jclass, jstring);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienative_cocos2dx_AdfurikunNativeActivityBridge_onNativeMovieAdViewPlayFinish
    (JNIEnv *, jclass, jstring, jboolean);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienative_cocos2dx_AdfurikunNativeActivityBridge_onNativeMovieAdViewPlayFail
    (JNIEnv *, jclass, jstring, jint);

}

#endif
