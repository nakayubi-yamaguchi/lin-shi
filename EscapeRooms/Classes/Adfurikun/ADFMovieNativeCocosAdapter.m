//
//  ADFMovieRewardCocosAdapter.m
//
//

#import "ADFMovieNativeCocosAdapter.h"

@interface ADFMovieNativeCocosAdapter()
@property (nonatomic, strong) ADFmyMovieNativeAdView* movieNativeAdView;
@property (nonatomic, strong) NSString* appID;
@property int layoutPattern;
@property (nonatomic, assign)int x;
@property (nonatomic, assign)int y;
@property (nonatomic, assign)int width;
@property (nonatomic, assign)int height;
@property (nonatomic, assign) void* callbackObject;
@end

@implementation ADFMovieNativeCocosAdapter


- (id)initWithAppID:(NSString *)appID pattern:(int)pattern xPosition:(int)x yPosition:(int)y width:(int)width height:(int)height{
	self = [super init];
	if (self) {
		ADFmyMovieNativeAdView* nativeAdView = [[ADFmyMovieNativeAdView alloc] initWithAppID:appID layoutPattern:ADFMovieNativeAdLayoutPattern_Default];
		if(nativeAdView != nil){
			self.movieNativeAdView = nativeAdView;
		}
		self.appID = [[NSString alloc] initWithString:appID];
		self.x = x;
		self.y = y;
		self.width = width;
		self.height = height;
		self.layoutPattern = pattern;
	}
	return self;
}

- (ADFmyMovieNativeAdView *)getMovieNative{
	return self.movieNativeAdView;
}
- (int)getX {
	return self.x;
}
- (int)getY {
	return self.y;
	
}
- (int)getWidth {
	return self.width;
	
}
- (int)getHeight {
	return self.height;
	
}

- (void)setFrame:(int)x yPosition:(int)y width:(int)width height:(int)height {
  self.x = x;
  self.y = y;
  self.width = width;
  self.height = height;
  [self.movieNativeAdView getAdView].frame = CGRectMake(x, y, width, height);
}

- (void)setCallback:(void* )object{
	self.callbackObject = object;
}

- (void *)getCallback{
	return _callbackObject;
}


/**< 広告の読み込みが開始した時のイベント */
- (void)onNativeMovieAdViewLoadFinish:(NSString *)appID {
	if ( self.delegate ) {
		if ([self.delegate respondsToSelector:@selector(onNativeMovieAdViewLoadFinish:)]) {
			[self.delegate onNativeMovieAdViewLoadFinish:self.appID];
		}
	}
	
}

/**< 広告の読み込みが失敗した時のイベント */
- (void)onNativeMovieAdViewLoadError:(ADFMovieError *)error appID:(NSString *)appID {
	if ( self.delegate ) {
		if ([self.delegate respondsToSelector:@selector(onNativeMovieAdViewLoadError:errorCode:)]) {
			[self.delegate onNativeMovieAdViewLoadError:self.appID errorCode:error.errorCode];
		}
	}
}

/**< 広告の再生を開始した時のイベント */
- (void)onNativeMovieAdViewPlayStart:(NSString *)appID {
    if ( self.delegate ) {
        if ([self.delegate respondsToSelector:@selector(onNativeMovieAdViewPlayStart:)]) {
            [self.delegate onNativeMovieAdViewPlayStart:self.appID];
        }
    }
}

/**< 広告の再生を開始した時のイベント */
- (void)onNativeMovieAdViewPlayFinish:(NSString *)appID isVideo:(BOOL)isVideo {
    if ( self.delegate ) {
        if ([self.delegate respondsToSelector:@selector(onNativeMovieAdViewPlayFinish:isVideo:)]) {
            [self.delegate onNativeMovieAdViewPlayFinish:self.appID isVideo:isVideo];
        }
    }
}

/**< 広告の再生を開始した時のイベント */
- (void)onNativeMovieAdViewPlayFail:(NSString *)appID error:(ADFMovieError *)error{
    if ( self.delegate ) {
        if ([self.delegate respondsToSelector:@selector(onNativeMovieAdViewPlayFail:errorCode:)]) {
            [self.delegate onNativeMovieAdViewPlayFail:self.appID errorCode:error.errorCode];
        }
    }
}

- (void)dispose{
}

- (void) dealloc{
}
@end
