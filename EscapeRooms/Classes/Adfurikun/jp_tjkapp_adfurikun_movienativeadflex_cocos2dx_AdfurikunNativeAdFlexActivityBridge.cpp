#include "cocos2d.h"
#include "jp_tjkapp_adfurikun_movienativeadflex_cocos2dx_AdfurikunNativeAdFlexActivityBridge.h"
#include "ADFMovieReward.h"

using Adfurikun::ADFMovieNativeAdFlex;
using Adfurikun::ADFMovieNativeAdFlexDelegate;


JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienativeadflex_cocos2dx_AdfurikunNativeAdFlexActivityBridge_onPrepareSuccess(JNIEnv *env, jclass clazz, jstring appId)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    ADFMovieNativeAdFlexDelegate* listener = ADFMovieNativeAdFlex::getDelegate(id);
    if (listener != nullptr) {
        listener->prepareSuccess(id);
    }
    
    env->ReleaseStringUTFChars(appId, id);
}

JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienativeadflex_cocos2dx_AdfurikunNativeAdFlexActivityBridge_onStartPlaying(JNIEnv *env, jclass clazz, jstring appId, jstring jStrKey)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    const char* key = env->GetStringUTFChars(jStrKey, 0);
    ADFMovieNativeAdFlexDelegate* listener = ADFMovieNativeAdFlex::getDelegate(id);
    if (listener != nullptr) {
        listener->startPlaying(id, key);
    }
    
    env->ReleaseStringUTFChars(appId, id);
    env->ReleaseStringUTFChars(jStrKey, key);
}

JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienativeadflex_cocos2dx_AdfurikunNativeAdFlexActivityBridge_onFinishedPlaying(JNIEnv *env, jclass clazz, jstring appId)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    ADFMovieNativeAdFlexDelegate* listener = ADFMovieNativeAdFlex::getDelegate(id);
    if (listener != nullptr) {
        listener->finishedPlaying(id);
    }
    
    env->ReleaseStringUTFChars(appId, id);
}

JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienativeadflex_cocos2dx_AdfurikunNativeAdFlexActivityBridge_onFailedPlaying(JNIEnv *env, jclass clazz, jstring appId)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    ADFMovieNativeAdFlexDelegate* listener = ADFMovieNativeAdFlex::getDelegate(id);
    if (listener != nullptr) {
        listener->failedPlaying(id);
    }
    
    env->ReleaseStringUTFChars(appId, id);
}

JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movienativeadflex_cocos2dx_AdfurikunNativeAdFlexActivityBridge_onAdClose(JNIEnv *env, jclass clazz, jstring appId)
{
    const char* id = env->GetStringUTFChars(appId, 0);
    ADFMovieNativeAdFlexDelegate* listener = ADFMovieNativeAdFlex::getDelegate(id);
    if (listener != nullptr) {
        listener->adClose(id);
    }
    
    env->ReleaseStringUTFChars(appId, id);
}

