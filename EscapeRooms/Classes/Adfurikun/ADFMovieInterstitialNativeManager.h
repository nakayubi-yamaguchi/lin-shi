//
//  ADFMovieRewardNativeManager.h
//  adfurikunSample
//
//  Created by tsukui on 2016/02/09.
//
//

#ifndef adfurikunSample_ADFMovieInterstitialNativeManager_h
#define adfurikunSample_ADFMovieInterstitialNativeManager_h
#import "ADFMovieInterstitialCocosAdapter.h"

@interface ADFMovieInterstitialNativeManager : NSObject<ADFMovieInterstitialCocosAdapterDelegate>
+ (void)initializeMovieInterstitialWithAppID:(NSString*)appID;
+ (void)setDelegate:(NSString *)appID delegate:(void *)delegate;
+ (void *)getDelegate:(NSString *)appID;
+ (void)detachDelegate:(NSString *)appID;
+ (ADFMovieInterstitialCocosAdapter *)getAdapter:(NSString *)appID;
+ (bool)isPreparedMovieInterstitial:(NSString*)appID;
+ (void)playMovieInterstitial:(NSString*)appID;
+ (void)dispose_handle;
@end

#endif
