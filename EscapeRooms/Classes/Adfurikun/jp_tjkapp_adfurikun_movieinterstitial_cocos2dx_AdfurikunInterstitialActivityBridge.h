#include <jni.h>

#ifndef _Included_org_cocos2dx_cpp_AppActivity
#define _Included_org_cocos2dx_cpp_AppActivity

extern "C" {
    
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movieinterstitial_cocos2dx_AdfurikunInterstitialActivityBridge_onPrepareSuccess
    (JNIEnv *, jclass, jstring);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movieinterstitial_cocos2dx_AdfurikunInterstitialActivityBridge_onStartPlaying
    (JNIEnv *, jclass, jstring, jstring);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movieinterstitial_cocos2dx_AdfurikunInterstitialActivityBridge_onFinishedPlaying
    (JNIEnv *, jclass, jstring);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movieinterstitial_cocos2dx_AdfurikunInterstitialActivityBridge_onFailedPlaying
    (JNIEnv *, jclass, jstring);
    JNIEXPORT void JNICALL Java_jp_tjkapp_adfurikun_movieinterstitial_cocos2dx_AdfurikunInterstitialActivityBridge_onAdClose
    (JNIEnv *, jclass, jstring);

}

#endif