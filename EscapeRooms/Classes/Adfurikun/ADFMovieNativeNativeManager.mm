#import "ADFMovieReward.h"
#import "ADFMovieNativeNativeManager.h"
#import "platform/ios/CCEAGLView-ios.h"

@interface ADFMovieNativeNativeManager()

+ (int)getScreenPositionByGravity:(int)Gravity screenSize:(float)screenSize contentSize:(float)contentSize;

@end

@implementation ADFMovieNativeNativeManager

// クラス自身のインスタンス
static ADFMovieNativeNativeManager* instance = nil;
//動画ネイティブのインスタンスを格納する為の配列
__strong static NSMutableDictionary* adMovieNativeList = nil;

- (id)init {
    self = [super init];
    if ( self ) {
    }
    return self;
}

+ (void)initializeMovieNativeWithAppID:(NSString*)appID pattern:(int)pattern xPosition:(int)x yPosition:(int)y width:(int)width height:(int)height {
    if([appID length] < 1){return;}
    if ( instance == nil) {
        instance = [ADFMovieNativeNativeManager new];
    }
    if(adMovieNativeList == nil){
        adMovieNativeList = [@{} mutableCopy];
    }
    //iOSが対応バージョンなら、指定した広告枠で動画読み込みを開始
    if(![ADFmyMovieNative isSupportedOSVersion]){
        return;
    }
    [ADFmyMovieNativeAdView configureWithAppID:appID];
    ADFMovieNativeCocosAdapter* cocosAdapter = [[ADFMovieNativeCocosAdapter alloc] initWithAppID:appID pattern:pattern xPosition:(int)x yPosition:(int)y width:(int)width height:(int)height];
    cocosAdapter.delegate = instance;
    ADFmyMovieNativeAdView* nativeAdView = [cocosAdapter getMovieNative];
    [nativeAdView setVideoDelegate:cocosAdapter];
    //アダプターをリストに保存
    [adMovieNativeList setObject:cocosAdapter forKey:appID];
	
}

+ (void)initializeMovieNativeWithAppID:(NSString*)appID pattern:(int)pattern {
    [self initializeMovieNativeWithAppID:appID pattern:pattern xPosition:0 yPosition:0 width:0 height:0];
}

+ (void)setDelegate:(NSString *)appID delegate:(void *)delegate {
    if([appID length] < 1){ return; }
	
	//アダプターを取得
	ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
	if(adapter != nil){
		//コールバック(c++)を設定
		[adapter setCallback:delegate];
		//コールバック(Objective-c)を設定
		adapter.delegate = instance;
	}
}

+ (void *)getDelegate:(NSString *)appID{
    if([appID length] < 1){ return nil; }
    ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
    if(adapter != nil){
        return [adapter getCallback];
    }
    return nil;
}

+ (void)detachDelegate:(NSString*)appID{
	ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
    if(adapter != nil) {
        adapter.delegate = nil;
    }
    if([adapter getCallback] != nil){
        [adapter setCallback:nil];
    }
}


+ (void)load:(NSString*)appID{
    NSLog(@"loadMovieNative");
	ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
    if(adapter != nil){
        ADFmyMovieNativeAdView* nativeAdView = [adapter getMovieNative];
        [nativeAdView getAdView].hidden = YES;
		[nativeAdView loadAndNotifyTo:adapter];
    }
}
+ (void)playVideo:(NSString*)appID{
    NSLog(@"playVideo");
	ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
    if(adapter != nil){
        ADFmyMovieNativeAdView* nativeAdView = [adapter getMovieNative];
        [nativeAdView getAdView].hidden = NO;
		[nativeAdView playVideo];
    }
}

+ (void)pauseVideo:(NSString*)appID{
    NSLog(@"pauseVideo");
	ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
    if(adapter != nil){
        ADFmyMovieNativeAdView* nativeAdView = [adapter getMovieNative];
		[nativeAdView pauseVideo];
    }
}

+ (void)startAutoReload:(NSString*)appID{
  NSLog(@"startAutoReload");
  ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
  if(adapter != nil){
    ADFmyMovieNativeAdView* nativeAdView = [adapter getMovieNative];
    nativeAdView.autoLoad = YES;
  }
}

+ (void)stopAutoReload:(NSString*)appID{
  NSLog(@"stopAutoReload");
  ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
  if(adapter != nil){
    ADFmyMovieNativeAdView* nativeAdView = [adapter getMovieNative];
    nativeAdView.autoLoad = NO;
  }
}

+ (void)showVideo:(NSString*)appID{
  NSLog(@"showVideo");
  ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
  if(adapter != nil){
    ADFmyMovieNativeAdView* nativeAdView = [adapter getMovieNative];
    [nativeAdView getAdView].hidden = NO;
  }
}

+ (void)hideVideo:(NSString*)appID{
  NSLog(@"hideVideo");
  ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
  if(adapter != nil){
    ADFmyMovieNativeAdView* nativeAdView = [adapter getMovieNative];
    [nativeAdView pauseVideo];
    [nativeAdView getAdView].hidden = YES;
  }
}

+ (void)disableAd:(NSString*)appID{
    NSLog(@"disableAd");
	ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
    if(adapter != nil){
        ADFmyMovieNativeAdView* nativeAdView = [adapter getMovieNative];
    [nativeAdView pauseVideo];
    [[nativeAdView getAdView]removeFromSuperview];
    }
}

+ (void)setFrame:(NSString*)appID xPosition:(int)x yPosition:(int)y width:(int)width height:(int)height {
    NSLog(@"setFrame");
    ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
    if(adapter != nil){
      [adapter setFrame:x yPosition:y width:width height:height];
    }
}

+ (void)setConvertFrame:(NSString*)appID displaySizeW:(float)displaySizeW displaySizeH:(float)displaySizeH xPosition:(float)x yPosition:(float)y width:(float)width height:(float)height {
    NSLog(@"setConvertFrame");
    
    float scale = UIScreen.mainScreen.bounds.size.width / displaySizeW;
    int realWidth = width * scale;
    int realHeight = height * scale;
    int realX = (int)(x * scale);
    int realY = (int)((displaySizeH - y) * scale);
    
    [self setFrame:appID xPosition:realX yPosition:realY width:realWidth height:realHeight];
}

+ (void)setFrameGravity:(NSString*)appID displaySizeW:(float)displaySizeW displaySizeH:(float)displaySizeH width:(float)width height:(float)height horizontalGravity:(int)horizontalGravity verticalGravity:(int)verticalGravity {
    NSLog(@"setFrameGravity");
    
    float scale = UIScreen.mainScreen.bounds.size.width / displaySizeW;
    int realWidth = width * scale;
    int realHeight = height * scale;
    
    int realX = [self getScreenPositionByGravity:(horizontalGravity) screenSize:UIScreen.mainScreen.bounds.size.width contentSize:realWidth];
    int realY = [self getScreenPositionByGravity:(verticalGravity) screenSize:UIScreen.mainScreen.bounds.size.height contentSize:realHeight];
    
    [self setFrame:appID xPosition:realX yPosition:realY width:realWidth height:realHeight];
}

+ (void)setFitWidthFrame:(NSString*)appID displaySizeH:(float)displaySizeH height:(float)height verticalGravity:(int)verticalGravity {
    NSLog(@"setFitWidthFrame");
    float scale = UIScreen.mainScreen.bounds.size.height / displaySizeH;
    int realHeight = height * scale;
    int realWidth = UIScreen.mainScreen.bounds.size.width;
    
    int realY = [self getScreenPositionByGravity:(verticalGravity) screenSize:UIScreen.mainScreen.bounds.size.height contentSize:realHeight];
    
    [self setFrame:appID xPosition:0 yPosition:realY width:realWidth height:realHeight];
}

+ (int)getScreenPositionByGravity:(int)Gravity screenSize:(float)screenSize contentSize:(float)contentSize {
    float p = 0;
    switch(Gravity) {
        case 0:
            p = 0;
            break;
        case 1:
            p = (screenSize - contentSize) / 2;
            break;
        case 2:
            p = screenSize - contentSize;
            break;
    }
    return (int)p;
}

#pragma mark - ADFMovieNativeCocosAdapterDelegate


- (void)onNativeMovieAdViewLoadFinish:(NSString *)appID {
	//広告の表示など
	ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
    if(adapter != nil){
        ADFmyMovieNativeAdView* nativeAdView = [adapter getMovieNative];
		int x = [adapter getX];
		int y = [adapter getY];
		int width = [adapter getWidth];
		int height = [adapter getHeight];
    
    [nativeAdView getAdView].frame = CGRectMake(x, y,width,height);
      
		UIView* rootView = (__bridge UIView*)cocos2d::Director::getInstance()->getOpenGLView()->getEAGLView();
		[rootView addSubview:[nativeAdView getAdView]];
    }
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieNativeDelegate *pDelegate = (Adfurikun::ADFMovieNativeDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->onNativeMovieAdViewLoadFinish([ADFMovieNativeNativeManager convCocosParamFormat:appID]);
        }
    }
	
}
- (void)onNativeMovieAdViewLoadError:(NSString *)appID errorCode:(int)errorCode {
    ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieNativeDelegate *pDelegate = (Adfurikun::ADFMovieNativeDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->onNativeMovieAdViewLoadError([ADFMovieNativeNativeManager convCocosParamFormat:appID], errorCode);
        }
    }
	
}

- (void)onNativeMovieAdViewPlayStart:(NSString *)appID {
    ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieNativeDelegate *pDelegate = (Adfurikun::ADFMovieNativeDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->onNativeMovieAdViewPlayStart([ADFMovieNativeNativeManager convCocosParamFormat:appID]);
        }
    }
}

- (void)onNativeMovieAdViewPlayFinish:(NSString *)appID isVideo:(BOOL)isVideo {
    ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieNativeDelegate *pDelegate = (Adfurikun::ADFMovieNativeDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->onNativeMovieAdViewPlayFinish([ADFMovieNativeNativeManager convCocosParamFormat:appID], isVideo);
        }
    }
}

- (void)onNativeMovieAdViewPlayFail:(NSString *)appID errorCode:(int)errorCode {
    ADFMovieNativeCocosAdapter* adapter = [ADFMovieNativeNativeManager getAdapter:appID];
    if([adapter getCallback] != nil){
        Adfurikun::ADFMovieNativeDelegate *pDelegate = (Adfurikun::ADFMovieNativeDelegate*)[adapter getCallback];
        if(pDelegate != NULL){
            pDelegate->onNativeMovieAdViewPlayFail([ADFMovieNativeNativeManager convCocosParamFormat:appID], errorCode);
        }
    }
}

+ (ADFMovieNativeCocosAdapter *)getAdapter:(NSString *)appID{
    ADFMovieNativeCocosAdapter* adapter = (ADFMovieNativeCocosAdapter *)[adMovieNativeList objectForKey:appID];
    return (adapter != nil) ? adapter: nil;
}

+ (const char *)convCocosParamFormat:(NSString *)str{
    return (char*)[str UTF8String];
}

- (void)dispose{
}

+ (void)dispose_handle{
    if(instance != nil){
        [instance dispose];
    }
}

@end
