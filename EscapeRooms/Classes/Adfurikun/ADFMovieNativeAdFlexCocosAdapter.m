#import "ADFMovieNativeAdFlexCocosAdapter.h"

@interface ADFMovieNativeAdFlexCocosAdapter()
@property (nonatomic, strong) ADFmyMovieNativeAdFlex* movieNativeAdFlex;
@property (nonatomic, strong) NSString* appID;
@property (nonatomic, assign) void* callbackObject;
@end

@implementation ADFMovieNativeAdFlexCocosAdapter


- (id)initWithAppID:(NSString *)appID{
    self = [super init];
    if (self) {
        ADFmyMovieNativeAdFlex* nativeAdFlex = [ADFmyMovieNativeAdFlex getInstance:appID delegate:self];
        if(nativeAdFlex != nil){
            self.movieNativeAdFlex = nativeAdFlex;
        }
        self.appID = [[NSString alloc] initWithString:appID];
    }
    return self;
}

- (ADFmyMovieNativeAdFlex *)getMovieNativeAdFlex{
    return self.movieNativeAdFlex;
}

- (void)setCallback:(void* )object{
    self.callbackObject = object;
}

- (void *)getCallback{
    return _callbackObject;
}

/**< 広告を閉じた時のイベント */
- (void)AdsDidHide{
    if ( self.delegate ) {
        if ([self.delegate respondsToSelector:@selector(AdsDidHide:)]) {
            [self.delegate AdsDidHide:self.appID];
        }
    }
}

/**< 広告の表示準備が終わった時のイベント */
- (void)AdsFetchCompleted:(NSString *)appID isTestMode:(BOOL)isTestMode_inApp {
    if ( appID == nil || ![appID isEqualToString:self.appID]){
        return;
    }
    if ( self.delegate) {
        if ([self.delegate respondsToSelector:@selector(AdsFetchCompleted:isTestMode_inApp:)]) {
            [self.delegate AdsFetchCompleted:self.appID isTestMode_inApp:isTestMode_inApp];
        }
    }
}

/**< 広告の表示が開始した時のイベント */
- (void)AdsDidShow:(NSString *)adnetworkKey{
    if ( self.delegate ) {
        if ([self.delegate respondsToSelector:@selector(AdsDidShow:adnetworkKey:)]) {
            [self.delegate AdsDidShow:self.appID adnetworkKey:adnetworkKey];
        }
    }
}
/**< 広告の表示を最後まで終わったか */
- (void)AdsDidCompleteShow{
    if ( self.delegate ) {
        if ([self.delegate respondsToSelector:@selector(AdsDidCompleteShow:)]) {
            [self.delegate AdsDidCompleteShow:self.appID];
        }
    }
}
/**< 動画広告再生エラー時のイベント */
- (void)AdsPlayFailed{
    if ( self.delegate ) {
        if ([self.delegate respondsToSelector:@selector(AdsPlayFailed:)]) {
            [self.delegate AdsPlayFailed:self.appID];
        }
    }
}

- (void)dispose{
    _appID = @"";
    self.delegate = nil;
    if(_movieNativeAdFlex != nil){
        _movieNativeAdFlex = nil;
    }
    if(_callbackObject != nil){
        _callbackObject = nil;
    }
}

- (void) dealloc{
    [self dispose];
}
@end
