#ifndef ADFMovieNativeAdFlexManager_h
#define ADFMovieNativeAdFlexManager_h
#import "ADFMovieNativeAdFlexCocosAdapter.h"

@interface ADFMovieNativeAdFlexManager : NSObject<ADFMovieNativeAdFlexCocosAdapterDelegate>
+ (void)initializeMovieNativeAdFlexWithAppID:(NSString*)appID;
+ (void)setDelegate:(NSString *)appID delegate:(void *)delegate;
+ (void *)getDelegate:(NSString *)appID;
+ (void)detachDelegate:(NSString *)appID;
+ (ADFMovieNativeAdFlexCocosAdapter *)getAdapter:(NSString *)appID;
+ (bool)isPreparedMovieNativeAdFlex:(NSString*)appID;
+ (void)playMovieNativeAdFlex:(NSString*)appID;
+ (void)finishMovieNativeAdFlex:(NSString*)appID;
+ (void)dispose_handle;
@end

#endif /* ADFMovieNativeAdFlexManager_h */
