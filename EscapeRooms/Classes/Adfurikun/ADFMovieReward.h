#ifndef ADFMovieReward_h
#define ADFMovieReward_h

#include "cocos2d.h"

namespace Adfurikun {
  /**
   * アドフリくん動画リワードSDKからのコールバックを受け取るクラス
   */
  class ADFMovieRewardDelegate
  {
  public:
    virtual ~ADFMovieRewardDelegate();
    
    /**
     広告の表示準備が終わった時のイベント
     @param const char* (appID) 広告枠ID
     */
    virtual void prepareSuccess(const char* appId) = 0;
    /**
     広告の表示が開始した時のイベント
     @param const char* (appID) 広告枠ID
     @param const char* (adnetworkKey) アドネットワークキー
     */
    virtual void startPlaying(const char* appId, const char* adnetworkKey) = 0;
    /**
     広告の表示を最後まで終わったか
     @param const char* (appID) 広告枠ID
     */
    virtual void finishedPlaying(const char* appId) = 0;
    /**
     広告の表示を最後まで終わったか
     @param const char* (appID) 広告枠ID
     */
    virtual void failedPlaying(const char* appId) = 0;
    /**
     広告を閉じた時のイベント
     @param const char* (appID) 広告枠ID
     */
    virtual void adClose(const char* appId) = 0;
  };
  
  /**
   * 動画リワードSDKとの中継クラス
   */
  class ADFMovieReward
  {
  private:
    ADFMovieReward();
    
  public:
    /**
     初期化を行う
     @param const char* (appID) 広告枠ID
     */
    static void initializeWithAppID(const char* appId);
    /**
     コールバックをセット
     @param const char* (appID) 広告枠ID
     @param NativeDelegate* delegate コールバック先
     */
    static void setDelegate(const char* appId, ADFMovieRewardDelegate* delegate);
    /**
     設定したコールバックの受け取り
     @param	const char* appId ... 広告枠ID
     @return ADFMovieRewardDelegate * ... setDelegateで設定したコールバック(未設定の場合はNULL)
     */
    static ADFMovieRewardDelegate* getDelegate(const char* appId);
    /**
     動画の再生準備が完了しているか？
     @param const char* (appID) 広告枠ID
     @return bool 動画の再生準備が完了しているか？
     */
    static bool isPrepared(const char* appId);
    /**
     動画を再生する
     @param const char* (appID) 広告枠ID
     */
    static void play(const char* appId);
    /**
     アドフリくん動画リワードSDKに関するリソースを開放する
     */
    static void dispose();
    /**
     セットしたコールバックを外す
     @param const char* (appID) 広告枠ID
     */
    static void detachDelegate(const char* appID);
  };
  
  /**
   * アドフリくん動画インターステシャルSDKからのコールバックを受け取るクラス
   */
  class ADFMovieInterstitialDelegate
  {
  public:
    virtual ~ADFMovieInterstitialDelegate();
    
    /**
     広告の表示準備が終わった時のイベント
     @param const char* (appID) 広告枠ID
     */
    virtual void prepareSuccess(const char* appId) = 0;
    /**
     広告の表示が開始した時のイベント
     @param const char* (appID) 広告枠ID
     @param const char* (adnetworkKey) アドネットワークキー
     */
    virtual void startPlaying(const char* appId, const char* adnetworkKey) = 0;
    /**
     広告の表示を最後まで終わったか
     @param const char* (appID) 広告枠ID
     */
    virtual void finishedPlaying(const char* appId) = 0;
    /**
     広告の表示を最後まで終わったか
     @param const char* (appID) 広告枠ID
     */
    virtual void failedPlaying(const char* appId) = 0;
    /**
     広告を閉じた時のイベント
     @param const char* (appID) 広告枠ID
     */
    virtual void adClose(const char* appId) = 0;
  };
  
  /**
   * 動画インターステシャルSDKとの中継クラス
   */
  class ADFMovieInterstitial
  {
  private:
    ADFMovieInterstitial();
    
  public:
    /**
     初期化を行う
     @param const char* (appID) 広告枠ID
     */
    static void initializeWithAppID(const char* appId);
    /**
     コールバックをセット
     @param const char* (appID) 広告枠ID
     @param NativeDelegate* delegate コールバック先
     */
    static void setDelegate(const char* appId, ADFMovieInterstitialDelegate* delegate);
    /**
     設定したコールバックの受け取り
     @param	const char* appId ... 広告枠ID
     @return ADFMovieInterDelegate * ... setDelegateで設定したコールバック(未設定の場合はNULL)
     */
    static ADFMovieInterstitialDelegate* getDelegate(const char* appId);
    /**
     動画の再生準備が完了しているか？
     @param const char* (appID) 広告枠ID
     @return bool 動画の再生準備が完了しているか？
     */
    static bool isPrepared(const char* appId);
    /**
     動画を再生する
     @param const char* (appID) 広告枠ID
     */
    static void play(const char* appId);
    /**
     アドフリくん動画インターステシャルSDKに関するリソースを開放する
     */
    static void dispose();
    /**
     セットしたコールバックを外す
     @param const char* (appID) 広告枠ID
     */
    static void detachDelegate(const char* appID);
  };
  
  /**
   * アドフリくん動画ネイティブSDKからのコールバックを受け取るクラス
   */
  class ADFMovieNativeDelegate
  {
  public:
    virtual ~ADFMovieNativeDelegate();
    
    /**
     広告の読み込みが成功したときに呼ばれるコールバック
     @param const char* (appID) 広告枠ID
     */
    virtual void onNativeMovieAdViewLoadFinish(const char* appId) = 0;
    /**
     広告の読み込みが失敗したときに呼ばれるコールバック
     @param const char* (appID) 広告枠ID
     */
    virtual void onNativeMovieAdViewLoadError(const char* appId, int errorCode) = 0;
    /**
     広告の再生が開始したときに呼ばれるコールバック
     @param const char* (appID) 広告枠ID
     */
    virtual void onNativeMovieAdViewPlayStart(const char* appId) = 0;
    /**
     広告の再生が完了したときに呼ばれるコールバック
     @param const char* (appID) 広告枠ID
     @param bool (isVideoAd) 動画 : true , 静止画 : false
     */
    virtual void onNativeMovieAdViewPlayFinish(const char* appId, bool isVideoAd) = 0;
    /**
     広告の再生が失敗したときに呼ばれるコールバック
     @param const char* (appID) 広告枠ID
     @param int (errorCode) エラーコード
     */
    virtual void onNativeMovieAdViewPlayFail(const char* appId, int errorCode) = 0;
  };
  
  
  
  /**
   * 動画ネイティブSDKとの中継クラス
   */
  class ADFMovieNative
  {
  private:
    ADFMovieNative();
    
  public:

    enum HorizonGravity { H_LEFT = 0, H_CENTER = 1, H_RIGHT = 2 };
    enum VerticalGravity { V_TOP = 0, V_CENTER = 1, V_BOTTOM = 2 };
    /**
     初期化を行う
     @param const char* (appID) 広告枠ID
     @param int (layoutPattern) レイアウトの種類
     @param int (x) レイアウトのx座標
     @param int (y) レイアウトのy座標
     @param int (width) レイアウトの横幅
     @param int (height) レイアウトの高さ
     */
    static void initializeWithAppID(const char* appId, int layoutPattern, int x, int y, int width, int height);
    
    /**
     初期化を行う
     @param const char* (appID) 広告枠ID
     @param int (layoutPattern) レイアウトの種類
     @param int (x) レイアウトのx座標
     @param int (y) レイアウトのy座標
     @param int (width) レイアウトの横幅
     @param int (height) レイアウトの高さ
     */
    static void initializeWithAppID(const char* appId, int layoutPattern);
    
    /**
     コールバックをセット
     @param const char* (appID) 広告枠ID
     @param NativeDelegate* delegate コールバック先
     */
    static void setDelegate(const char* appId, ADFMovieNativeDelegate* delegate);
    /**
     設定したコールバックの受け取り
     @param	const char* appId ... 広告枠ID
     @return ADFMovieNativeDelegate * ... setDelegateで設定したコールバック(未設定の場合はNULL)
     */
    static ADFMovieNativeDelegate* getDelegate(const char* appId);
    /**
     セットしたコールバックを外す
     @param const char* (appID) 広告枠ID
     */
    static void detachDelegate(const char* appID);
    
    /**
     動画の広告情報を読み込む
     @param const char* (appID) 広告枠ID
     */
    static void load(const char* appID);
    
    /**
     動画の再生をする
     @param const char* (appID) 広告枠ID
     */
    static void playVideo(const char* appID);
    
    /**
     動画の再生を一時停止する
     @param const char* (appID) 広告枠ID
     */
    static void pauseVideo(const char* appID);
    
    /**
     動画広告をviewから削除する
     @param const char* (appID) 広告枠ID
     */
    static void disableAd(const char* appID);
    
    /**
     動画広告の自動切り替えをonにする
     @param const char* (appID) 広告枠ID
     */
    static void startAutoReload(const char* appID);
    
    /**
     動画広告の自動切り替えをoffにする
     @param const char* (appID) 広告枠ID
     */
    static void stopAutoReload(const char* appID);
    
    /**
     動画広告を表示する
     @param const char* (appID) 広告枠ID
     */
    static void showVideo(const char* appID);
    
    /**
     動画広告を非表示にする
     @param const char* (appID) 広告枠ID
     */
    static void hideVideo(const char* appID);
    
    /**
     動画広告の座標とサイズを変更
     @param const char* (appID) 広告枠ID
     @param int (x) レイアウトのx座標
     @param int (y) レイアウトのy座標
     @param int (width) レイアウトの横幅
     @param int (height) レイアウトの高さ
     */
    static void setFrame(const char* appId, int x, int y, int width, int height);
    
    /**
     動画広告の座標とサイズを変更
     @param const char* (appID) 広告枠ID
     @param float (displaySizeW) 画面サイズ(幅)
     @param float (displaySizeH) 画面サイズ(高さ)
     @param float (x) レイアウトのx座標
     @param float (y) レイアウトのy座標
     @param float (width) レイアウトの横幅
     @param float (height) レイアウトの高さ
     */
    static void setConvertFrame(const char* appId, float displaySizeW, float displaySizeH, float x, float y, float width, float height);
    
    /**
     動画広告のGravityとサイズを変更
     @param const char* (appID) 広告枠ID
     @param float (displaySizeW) 画面サイズ(幅)
     @param float (displaySizeH) 画面サイズ(高さ)
     @param float (width) レイアウトの横幅
     @param float (height) レイアウトの高さ
     @param int (horizontalGravity) 水平方向 Gravity 0:LEFT 1:CENTER 2:RIGHT
     @param int (verticalGravity) 垂直方向 Gravity 0:TOP 1:CENTER 2:BOTTOM
     */
    static void setFrameGravity(const char* appId, float displaySizeW, float displaySizeH, float width, float height , int horizontalGravity, int verticalGravity);

    /**
     動画広告をView横幅サイズに変更
     @param const char* (appID) 広告枠ID
     @param float (displaySizeH) 画面サイズ(高さ)
     @param float (height) レイアウトの高さ
     @param int (verticalGravity) 垂直方向 Gravity 0:TOP 1:CENTER 2:BOTTOM
    */
    static void setFitWidthFrame(const char* appId, float displaySizeH, float height, int verticalGravity);
  };
  
  /**
   * アドフリくん動画ネイティブアドフレックスSDKからのコールバックを受け取るクラス
   */
  class ADFMovieNativeAdFlexDelegate
  {
  public:
    virtual ~ADFMovieNativeAdFlexDelegate();
    
    /**
     広告の表示準備が終わった時のイベント
     @param const char* (appID) 広告枠ID
     */
    virtual void prepareSuccess(const char* appId) = 0;
    /**
     広告の表示が開始した時のイベント
     @param const char* (appID) 広告枠ID
     @param const char* (adnetworkKey) アドネットワークキー
     */
    virtual void startPlaying(const char* appId, const char* adnetworkKey) = 0;
    /**
     広告の表示を最後まで終わったか
     @param const char* (appID) 広告枠ID
     */
    virtual void finishedPlaying(const char* appId) = 0;
    /**
     広告の表示を最後まで終わったか
     @param const char* (appID) 広告枠ID
     */
    virtual void failedPlaying(const char* appId) = 0;
    /**
     広告を閉じた時のイベント
     @param const char* (appID) 広告枠ID
     */
    virtual void adClose(const char* appId) = 0;
  };
  
  /**
   * 動画ネイティブアドフレックスSDKとの中継クラス
   */
  class ADFMovieNativeAdFlex
  {
  private:
    ADFMovieNativeAdFlex();
    
  public:
    /**
     初期化を行う
     @param const char* (appID) 広告枠ID
     */
    static void initializeWithAppID(const char* appId);
    /**
     コールバックをセット
     @param const char* (appID) 広告枠ID
     @param ADFMovieNativeAdFlexDelegate* delegate コールバック先
     */
    static void setDelegate(const char* appId, ADFMovieNativeAdFlexDelegate* delegate);
    /**
     設定したコールバックの受け取り
     @param	const char* appId ... 広告枠ID
     @return ADFMovieNativeAdFlexDelegate * ... setDelegateで設定したコールバック(未設定の場合はNULL)
     */
    static ADFMovieNativeAdFlexDelegate* getDelegate(const char* appId);
    /**
     動画の再生準備が完了しているか？
     @param const char* (appID) 広告枠ID
     @return bool 動画の再生準備が完了しているか？
     */
    static bool isPrepared(const char* appId);
    /**
     動画を再生する
     @param const char* (appID) 広告枠ID
     */
    static void play(const char* appId);
    /**
     動画を終了する
     @param const char* (appID) 広告枠ID
     */
    static void closeAd(const char* appId);
    /**
     アドフリくん動画ネイティブアドフレックスSDKに関するリソースを開放する
     */
    static void dispose();
    /**
     セットしたコールバックを外す
     @param const char* (appID) 広告枠ID
     */
    static void detachDelegate(const char* appID);
  };

}
#endif /* ADFMovieReward_h */
