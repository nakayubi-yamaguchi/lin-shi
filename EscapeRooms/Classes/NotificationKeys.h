//
//  NotificationKeys.h
//  Kebab
//
//  Created by 成田凌平 on 2016/12/18.
//
//
#include "Utils/CommonNotificationKeys.h"

#ifndef NotificationKeys_h
#define NotificationKeys_h

#define NotificationKeyWillShowPopUp "willShowPopup"
#define NotificationKeyWillChangeBack "willChangeBack"
#define NotificationKeyReloadItems "reloadItems"
#define NotificationKeyChangeSelectedItemID "changeSelectedItemID"
#define NotificationKeyChangedOwnedCoin "ChangeCoin"

#define NotificationKeyFoundMystery "foundMystery"

#define NotificationKeyAchievementBadge "achievementbadge"
#define NotificationKeyStampBadge "stampbadge"

#define NotificationKeyClickBeginners "clickbeginners"

#endif /* NotificationKeys_h */
