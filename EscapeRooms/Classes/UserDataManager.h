//
//  UserDataManager.h
//  Exit
//
//  Created by 成田凌平 on 2019/02/02.
//
//

#ifndef _____PROJECTNAMEASIDENTIFIER________UserDataManager_____
#define _____PROJECTNAMEASIDENTIFIER________UserDataManager_____

#include "cocos2d.h"
#include "EscapeDataManager.h"

#define UserKey_GotStampDate "LoginBonusDate" //直近スタンプ受け取り日時
#define UserKey_GetStamp "GetStamp_%08lld"//日付を代入 日毎のスタンプ入手数

//保存場所はgetWritableFileDir/DataLists userID、コイン数、課金データ、達成項目状態などを記憶しておく。データベース丸投げを想定。
#define FileName_UserRecordData "userRecordData.plist"

/*
#define UserKey_Coin "Coin"//コイン
#define UserKey_LoginBonusDate "LoginBonusDate" //ログインボーナス受け取り日時
//#define UserKey_LoginBonusCount "LoginBonusCount"//ログインボーナス受け取り数
#define UserKey_GetStamp "GetStamp_%08lld"//日付を代入 日毎のスタンプ入手数
#define UserKey_GetRoulette "GetRoulette_%08lld"//日付を代入 日毎のルーレット回転数
#define UserKey_GetRouletteTotal "loginBonusCount"//トータル抽選回数
#define UserKey_LaunchCountDate "LaunchCount" //起動日数分析用 1日一回のみ、起動回数をfirebaseでカウントする(分析用)v.4.0~
#define UserKey_RegistNotification "RegistNotification"
#define UserKey_IsPaidUser "isPaiduser"
#define UserKey_NowStampCount "NowStampCount"//現在の溜まってるスタンプ数
*/

#define VoteKey_VoteData "VoteData"//投票データ親(Map)
#define VoteKey_UseCoins "UseCoins"//コイン(Int)
#define VoteKey_IsShowVideo "IsShowVideo"//動画を視聴(Bool)
#define VoteKey_VoteCount "VoteCount"//投票数（Int）
#define VoteKey_Timestamp "VoteTimestamp"//投票タイムスタンプ（Double）


class UserDataManager
{
public:
    static UserDataManager* manager;
    static UserDataManager* sharedManager();
    static UserDataManager* getInstance();
    void init();

#pragma mark -
    ValueMap map;//save対象map
    CC_SYNTHESIZE(std::string, userID, UserID);
#pragma mark ログイン日数
    /**起動日数を+1*/
    void incrementLaunchCount();
    
    CC_SYNTHESIZE(int, launchCount, LaunchCount);
#pragma mark - コイン
    /**コインを付与  またはマイナス*/
    void addCoin(int added,bool doSave);
    /**コインが足りるかどうか*/
    bool isEnoughCoin(int used);
    /**現在保持しているコインを3点カンマ付きで取得*/
    std::string getCoinSt();
    /**所持コイン数が変動した時の通知登録*/
    void addChangeCoinNotification(Node *parent, const std::function<void (EventCustom *)> &callback);
    
    CC_SYNTHESIZE(int, coin, Coin);
    CC_SYNTHESIZE(int, usedCoin, UsedCoin);//累計使用コイン addCoinがマイナスの時自動加算

#pragma mark - スタンプ
    void setGotStampDate();
    void addTotalStamp(int add);
    void addTotalStampByMovie(int add);

    /**現在のスタンプ数*/
    int nowStampCount;
    void setNowStampCount(int var);
    int getNowStampCount();
    
    CC_SYNTHESIZE(double, gotStampDateByMovie, GotStampDateByMovie);//動画での最新スタンプ取得日時
    CC_SYNTHESIZE(int, totalStampCountByMovie, TotalStampCountByMovie);
    CC_SYNTHESIZE(int , totalStampCount, TotalStampCount);

#pragma mark 課金
    CC_SYNTHESIZE(bool, isPaidUser, IsPaidUser);
    
#pragma mark 初心者パック->配列に移し替えるかも.(新たなパックとの重複可能性を考慮)
    /**ビギナーズパックを購入した際に呼ぶ*/
    void purchasedBeginnersPack(int dailyBonusCoin, int dailyPeriod);
    /**デイリーボーナス(課金アイテム)を受け取れるかどうか*/
    bool canGotDailyCoin();
    /**デイリーボーナス(課金アイテム)を付与する*/
    void addDailyBonus();
    
    /**パック付与日時 受け取るごとに書き換え*/
    CC_SYNTHESIZE(double, gotDailyBonusDate, GotDailyBonusDate);
    /**パック付与日数 受け取るごとにdecrementされる*/
    CC_SYNTHESIZE(int, dailyBonusCount, DailyBonusCount);
    /**1回あたりのパック付与コイン数*/
    CC_SYNTHESIZE(int, dailyBonusCoin, DailyBonusCoin);
    
#pragma mark - Vote
    
    /**投票履歴データを取得。サイズが0なら投票したことない。*/
    ValueMap getVoteDataMap();

    /**今日投票済みかどうかチェック*/
    bool isVotedToday(const char *keyName);
    
    /**
     投票データを保存します。
     @param keyName version.plistのkey_name
     @param voteKey VoteKey_UseCoins または VoteKey_ShowVideo
     @param voteValue 使用コイン数または動画視聴フラグをValue型で。
     */
    void saveVoteData(const char* keyName, const char* voteKey, Value voteValue, int voteCount);
    
    
#pragma mark - Save
    /**保存処理*/
    void save();
private:
#pragma mark - ユーザID
    void createUserID();//postのタイミングは他のプロパティが送信される時

    std::string getDataListPath();
    /**Userネストにデータをポストする.データ量が小さいのでネストまるまる上書きしている coin変更時、clear時に送信*/
    void postUserData();
    
    /**load*/
    void loadUserData();

};

#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____) */
