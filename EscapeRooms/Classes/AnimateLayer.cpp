//
//  AnimateLayer.cpp
//  Kebab
//
//  Created by 成田凌平 on 2016/12/21.
//
//

#include "AnimateLayer.h"
#include "Utils/Common.h"
#include "SupplementsManager.h"
#include "DataManager.h"
#include "EscapeStageSprite.h"

using namespace cocos2d;

AnimateLayer* AnimateLayer::create(ValueMap animation, cocos2d::Rect rect, cocos2d::Node *parent, const ccMenuCallback callback){
	auto layer =new AnimateLayer;
    if (layer&&layer->init(animation,rect,parent,callback)) {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_RELEASE(layer);
	
    return layer;
}

bool AnimateLayer::init(ValueMap animation, cocos2d::Rect rect, cocos2d::Node *parent, const ccMenuCallback callback)
{
    if (!LayerColor::initWithColor(Color4B(0, 0, 0, 0))) {
        return false;
    }
    
    setNode(parent);
    
    images=animation["images"].asValueVector();//2重配列
    if(!animation["ses"].isNull()){
        SEs=animation["ses"].asValueVector();
    }
    if(!animation["images_remove"].isNull()){
        images_remove=animation["images_remove"].asValueVector();
    }
    
    imageRect=rect;
    _fullscreen=animation["fullScreen"].asBool();
    _whiteBack=animation["whiteBack"].asBool();
    showIndex=0;
    _callback=callback;
    
    //
    auto duration=animation["duration"].asFloat();
    if (duration==0) {
        duration=.3;
    }
    setDuration(duration);
    
    addEventListner();
    showAnimateImage();
    
    //パネル広告を隠す。
    SupplementsManager::getInstance()->hidePanel();

    return true;
}

#pragma mark- タッチ
void AnimateLayer::addEventListner()
{
    auto listner=EventListenerTouchOneByOne::create();
    listner->setSwallowTouches(true);//透過させない
    listner->onTouchBegan=[this](Touch*touch,Event*event)
    {
        return true;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listner, this);
}

#pragma mark- UI
void AnimateLayer::showAnimateImage()
{//アニメーション画像を順々に表示。
    auto files=images[showIndex].asValueVector();//1フレームに表示する画像名配列
    ValueVector fileNames_remove;
    Vector<FiniteTimeAction*>fadeins;
    Vector<FiniteTimeAction*>waits;
    Vector<FiniteTimeAction*>fadeouts;
    Vector<FiniteTimeAction*>removes;
    
    auto duration_fadeout=getDuration()*1.2;
    
    if (images_remove.size()>0) {
        fileNames_remove=images_remove[showIndex].asValueVector();
    }
    
    if (_whiteBack&&showIndex==0)
    {//真っ白な背景を追加
        auto whiteBack=Sprite::create();
        whiteBack->setTextureRect(Rect(0, 0, imageRect.size.width, imageRect.size.height));
        whiteBack->setColor(Color3B(240, 240, 240));
        whiteBack->setOpacity(0);
        whiteBack->setPosition(imageRect.origin.x,imageRect.origin.y);
        addChild(whiteBack);
        
        //fadein
        auto fadein=TargetedAction::create(whiteBack,FadeIn::create(getDuration()));
        fadeins.pushBack(fadein);
    }
    
    //
    for (auto value :fileNames_remove)
    {//消すファイルをチェック
        if (getNode()) {
            auto sprite=getNode()->getChildByName<Sprite*>(value.asString());
            
            //fadeout
            if (!_fullscreen&&sprite) {
                auto fadeout=TargetedAction::create(sprite,FadeOut::create(duration_fadeout));
                fadeins.pushBack(fadeout);//fadeinのタイミングで消すためここ
            }
        }
    }

    for (auto value :files)
    {//追加ファイルを全チェック
        auto sprite = EscapeStageSprite::createWithSpriteFileName(value.asString());
        sprite->setPosition(imageRect.origin.x,imageRect.origin.y);
        sprite->setScale(imageRect.size.width/sprite->getContentSize().width);
        sprite->setOpacity(0);
        addChild(sprite);
        
        //fadein
        auto fadein=TargetedAction::create(sprite,FadeIn::create(getDuration()));
        fadeins.pushBack(fadein);
        
        //wait
        auto wait=DelayTime::create(1.2);
        waits.pushBack(wait);
        //fadeout
        if (!_fullscreen) {
            auto fadeout=TargetedAction::create(sprite,FadeOut::create(duration_fadeout));
            fadeouts.pushBack(fadeout);
        }
        
        //removes
        auto remove=TargetedAction::create(sprite,RemoveSelf::create());
        removes.pushBack(remove);
    }
    
    //
    Vector<FiniteTimeAction*>sequence;
    
    auto sound=CallFunc::create([this]{
        if (SEs.size()>0) {
            auto fileName=SEs[showIndex].asString();
            Common::playSE(fileName.c_str());
        }
    });
    sequence.pushBack(sound);
    
    auto spawn_fadein=Spawn::create(fadeins);
    sequence.pushBack(spawn_fadein);
    auto spawn_wait=Spawn::create(waits);
    sequence.pushBack(spawn_wait);
    
    auto callback=CallFunc::create([this](){//次の画像があれば表示開始
        showIndex++;
        if (showIndex!=images.size())
        {//次の画像を表示
            this->showAnimateImage();
        }
    });
    sequence.pushBack(callback);
    
    //次の画像があればfadeinに合わせてfadeout
    if (fadeouts.size()>0) {
        auto spawn_fadeout=Spawn::create(fadeouts);
        if (showIndex+1<images.size()) {
            sequence.pushBack(spawn_fadeout);
        }
        
        auto spawn_remove=Spawn::create(removes);
        sequence.pushBack(spawn_remove);
    }
    
    auto finish=CallFunc::create([this]()
    {//終了判定
        if (showIndex==images.size()) {
            if (SEs.size()==showIndex+1)
            {//終了時のseがセットされている
                auto fileName=SEs[showIndex].asString();
                Common::playSE(fileName.c_str());
            }
            _callback(this);
            
            this->removeFromParent();
        }
    });
    sequence.pushBack(finish);
    
    runAction(Sequence::create(sequence));
}

#pragma mark- 
AnimateLayer::~AnimateLayer()
{
    log("animateLayer解放");
}
