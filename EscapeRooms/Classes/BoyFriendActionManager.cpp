//
//  TempleActionManager.cpp
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#include "BoyFriendActionManager.h"
#include "Utils/Common.h"
#include "Utils/NativeBridge.h"
#include "AnchorSprite.h"
#include "DataManager.h"
#include "StoryLayer.h"
#include "ClearScene.h"
#include "SupplementsManager.h"
#include "Utils/BannerBridge.h"
#include "GameScene.h"
#include "TouchManager.h"
#include "EscapeDataManager.h"

using namespace cocos2d;

int gariben_counter=0;
int flower_counter=0;

BoyFriendActionManager* BoyFriendActionManager::manager =NULL;

BoyFriendActionManager* BoyFriendActionManager::getInstance()
{
    if (manager==NULL) {
        manager=new BoyFriendActionManager();
    }
    return manager;
}


#pragma mark - Back
void BoyFriendActionManager::backAction(std::string key, int backID, cocos2d::Node *parent)
{
    if(key=="gariben")
    {//がりべん
        std::vector<int>indexs={0,1,2,0,3,2};
        
        gariben_counter=0;
        auto spine = SupplementsManager::getInstance()->addSpine(parent, "gariben");
        spine->update(0);
        spine->setPosition(Vec2(parent->getContentSize().width/2, parent->getContentSize().height*.363));
        spine->setScale(parent->getContentSize().width*.3/spine->getBoundingBox().size.width);
        spine->setTimeScale(.75);
        //lamda particle
        auto particle=[parent,indexs,spine,backID](int i){
            spine->setAnimation(0, "animation", false);
            
            Common::playSE("pop_short.mp3");
            
            std::vector<std::string>fileNames={"plus.png",
                "minus.png",
                "times.png",
                "divide.png"
            };
            
            auto size=parent->getContentSize().width*.2;
            auto particle=ParticleSystemQuad::create("particle_music.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setCascadeOpacityEnabled(true);
            particle->setTexture(Sprite::create(fileNames.at(i))->getTexture());
            particle->setPosition(parent->getContentSize().width*.45,parent->getContentSize().height*.575);
            particle->setLife(2);
            particle->resetSystem();
            particle->setStartSize(size);
            particle->setEndSize(size);
            particle->setTotalParticles(4);
            particle->setGravity(Vec2(size*.15, -size*1));
            particle->setStartColor(Color4F::RED);
            particle->setEndColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 0));
            particle->setLocalZOrder(1);
            particle->setAngle(120);
            particle->setSpeed(size*1.75);
            particle->setStartSpin(0);
            particle->setStartSpinVar(0);
            particle->setEndSpin(0);
            particle->setEndSpinVar(0);
            parent->addChild(particle);
            
            gariben_counter=(gariben_counter+1)%indexs.size();
        };
        
        //animateも行う
        delayAndCallBack(parent,1,true, [particle,indexs,this]{
            particle(indexs.at(gariben_counter));
        });
        
        //completeListener
        spine->setCompleteListener([spine,particle,indexs,parent](spTrackEntry* entry){
            log("detect end. name:%s",entry->animation->name);
            Vector<FiniteTimeAction*>actions;
            
            
            if (gariben_counter==0) {
                if (!DataManager::sharedManager()->isPlayingMinigame()) {
                    actions.pushBack(DelayTime::create(2));
                }
            }
            
            if (!DataManager::sharedManager()->isPlayingMinigame()||
                gariben_counter!=0) {
                //次のアニメーション呼び出し
                auto call=CallFunc::create([indexs,spine,entry,particle]{
                    auto index=indexs.at(gariben_counter);
                    particle(index);
                });
                actions.pushBack(call);
            }
            else{
                //まちがいようアニメ
                auto mistake=parent->getChildByName("sup_14_mistake.png");
                auto scale_original=mistake->getScale();
                mistake->setScale(.1);
                auto fadeins=TargetedAction::create(mistake, Spawn::create(FadeIn::create(1),
                                                                           Common::createSoundAction("snoring.mp3"),
                                                                           ScaleTo::create(1, scale_original), NULL));
                auto call=CallFunc::create([]{DataManager::sharedManager()->setTemporaryFlag("mistake_14", 1);});
                auto rep=Repeat::create(TargetedAction::create(mistake, Sequence::create(EaseInOut::create(ScaleBy::create(1.5, .8), 1.5),
                                                                                         EaseInOut::create(ScaleTo::create(1.5, scale_original), 1.5),
                                                                                         Common::createSoundAction("snoring.mp3")
                                                                                         , NULL)), -1);
                actions.pushBack(DelayTime::create(1));
                actions.pushBack(fadeins);
                actions.pushBack(call);
                actions.pushBack(rep);
            }
            
            spine->runAction(Sequence::create(actions));
        });
        
        //parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
    else if(key=="fox")
    {//寝息
        std::vector<int>indexs={0,1,2,2,0,1,2,2};
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(1));
        auto counter=0;
        auto fox=parent->getChildByName("sup_20_fox.png");
        for (auto index : indexs) {
            //if (counter%2==0) {
                actions.pushBack(createSoundAction("snoring.mp3"));
            //}
            
            auto width=parent->getContentSize().width*.075*(index+1);
            auto z=EscapeStageSprite::createWithSpriteFileName("z.png");
            z->setScale(width/z->getContentSize().width);
            z->setColor(Color3B::BLACK);
            z->setPosition(parent->getContentSize().width*.4,parent->getContentSize().height*.6);
            z->setOpacity(0);
            parent->addChild(z);
            
            auto pos_original=z->getPosition();
            
            auto distance=parent->getContentSize().width*.1;
            ccBezierConfig config;
            config.controlPoint_1=Vec2(-distance*3, parent->getContentSize().height*.15);
            config.controlPoint_2=Vec2(distance*1.5, parent->getContentSize().height*.15);
            config.endPosition=Vec2(-distance*2, parent->getContentSize().height*.3);
            
            auto bezier=TargetedAction::create(z, Spawn::create(BezierBy::create(2, config),
                                                                Sequence::create(FadeIn::create(.3),
                                                                                 DelayTime::create(1.4),
                                                                                 FadeOut::create(.3), NULL),
                                                                 NULL));
            actions.pushBack(Spawn::create(bezier,
                                           createBounceAction(fox, .5, .03), NULL));
            actions.pushBack(TargetedAction::create(z, Spawn::create(MoveTo::create(0.5, pos_original),
                                                                     NULL)));
            counter++;
        }
        actions.pushBack(DelayTime::create(1));

        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
    else if(key=="smoke"&&
            DataManager::sharedManager()->getEnableFlagWithFlagID(9))
    {//けむりもくもく
        Vector<FiniteTimeAction*>actions;
        std::vector<int>indexs={2,1,0,1,2,1};
        for (auto index : indexs) {
            auto width=parent->getContentSize().width*(.075+.1*index);
            
            auto smoke=Sprite::create("smoke.png");
            smoke->setPosition(parent->getContentSize().width*.35,parent->getContentSize().height*.55);
            smoke->setScale(width/smoke->getContentSize().width);
            smoke->setOpacity(0);
            smoke->setColor(Color3B::BLACK);
            parent->addChild(smoke);
            
            auto scale_original=smoke->getScale();
            auto pos_original=smoke->getPosition();
            auto angle=RandomHelper::random_int(0, 15);

            auto distance=parent->getContentSize().width*.1;
            ccBezierConfig config;
            config.controlPoint_1=Vec2(-distance*2, parent->getContentSize().height*.25);
            config.controlPoint_2=Vec2(distance*.2, parent->getContentSize().height*.25);
            config.endPosition=Vec2(distance*.3, parent->getContentSize().height*.4);
            
            auto bezier=TargetedAction::create(smoke, Spawn::create(BezierBy::create(2, config),
                                                                    EaseInOut::create(RotateBy::create(2, angle), 1.5),
                                                                    ScaleBy::create(2, 1.25),
                                                                    createSoundAction("pop_short.mp3"),
                                                                    Sequence::create(FadeTo::create(1, 150),FadeOut::create(1), NULL),
                                                                    NULL));
            
            auto seq=TargetedAction::create(smoke, Sequence::create(bezier, NULL));
            actions.pushBack(seq);
            
            auto reset=TargetedAction::create(smoke, Spawn::create(MoveTo::create(0, pos_original),
                                                                   RotateTo::create(0, 0),
                                                                   ScaleTo::create(0, scale_original),
                                                                   NULL));
            actions.pushBack(reset);
        }
        
        actions.pushBack(DelayTime::create(1));
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
    else if(key=="flower")
    {//花 spineを使用
        std::vector<int>indexs={2,0,1,2,0};
        flower_counter=0;
        auto spine = SupplementsManager::getInstance()->addSpine(parent, "flower");

        //lamda particle
        auto particle=[parent,indexs,spine,backID](int index,bool left){
            
            if (!left) {
                spine->setAnimation(0, "right_swing", false);
            }
            else{
                spine->setAnimation(0, "left_swing", false);
            }

            auto pos=Vec2(parent->getContentSize().width*.45, parent->getContentSize().height*(.6));
            auto size=parent->getContentSize().width*.15;
            
            if (backID==11) {
                Common::playSE("pop_short.mp3");

                auto particle=ParticleSystemQuad::create("particle_music.plist");
                particle->setAutoRemoveOnFinish(true);
                particle->setPosition(pos);
                particle->setLife(3);
                particle->resetSystem();
                particle->setStartSize(size);
                particle->setEndSize(size);
                particle->setGravity(Vec2(size*.5, -size*.1));
                particle->setStartColor(Color4F::BLACK);
                particle->setEndColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 0));
                
                particle->setAngle(135+30*index);
                particle->setSpeed(size*2);
                parent->addChild(particle);
            }
            
            flower_counter=(flower_counter+1)%indexs.size();
        };
        
        auto pos=Vec2(parent->getContentSize().width*.5,parent->getContentSize().height*.385);
        auto width=parent->getContentSize().width*.3;
        if (backID==10) {
            pos=Vec2(parent->getContentSize().width*1.0/6.0, parent->getContentSize().height*165.0/660.0);
            width=parent->getContentSize().width*.12;
        }
        else if(backID==2){
            pos=Vec2(parent->getContentSize().width*.4, parent->getContentSize().height*337.0/660.0);
            width=parent->getContentSize().width*.035;
        }
        
        spine->setPosition(pos);
        spine->update(0);//これ呼ばないとsizeとれない
        spine->setScale(width/spine->getBoundingBox().size.width);
        
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(30)) {
            //animateも行う
            delayAndCallBack(parent,1,true, [particle,indexs,this]{
                particle(indexs.at(flower_counter),true);
            });
        }
        
        //completeListener
        spine->setCompleteListener([spine,particle,indexs](spTrackEntry* entry){
            //log("detect end. name:%s",entry->animation->name);
            Vector<FiniteTimeAction*>actions;
            
            if (flower_counter==0) {
                actions.pushBack(DelayTime::create(3));
            }
            
            auto call=CallFunc::create([indexs,spine,entry,particle]{
                auto left=(StringUtils::format("%s",entry->animation->name)=="left_swing");
                
                //particle action
                auto index=indexs.at(flower_counter);
                particle(index,!left);
            });
            actions.pushBack(call);
            
            spine->runAction(Sequence::create(actions));
        });
    }
    else if(key=="heart")
    {//カップル部屋
        //heart particle
        auto heart=[parent](int index){
            auto call=CallFunc::create([parent,index]{
                Common::playSE("pop.mp3");
                
                auto size=parent->getContentSize().width*.2;
                std::vector<std::string>colors={
                    "ff69b4",
                    "1e90ff"
                };
                
                auto rgb=colors.at(index);
                
                auto particle=ParticleSystemQuad::create("particle_heart.plist");
                particle->setAutoRemoveOnFinish(true);
                particle->setPosition(Vec2(parent->getContentSize().width*(.45+.1*index),parent->getContentSize().height* .5));
                particle->setPosVar(Vec2::ZERO);
                particle->setDuration(.5);
                particle->setLife(1.75);
                particle->resetSystem();
                particle->setStartSize(size);
                particle->setStartSizeVar(0);
                particle->setEndSize(size*.5);
                particle->setEndSizeVar(0);
                particle->setGravity(Vec2(size*1.25*pow(-1, index), -size*2));
                particle->setStartColor(Color4F(Common::getColorFromHex(colors.at(index))));
                particle->setEndColor(Color4F(particle->getStartColor().r, particle->getStartColor().g, particle->getStartColor().b, 0));
                particle->setAngle(135-90*index);
                particle->setSpeed(size*2.5);
                parent->addChild(particle);
            });
            
            return call;
        };
        
        
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(1));
        std::vector<int>indexs={0,1,0,0,1,1,1};
        for (auto index:indexs) {
            actions.pushBack(Sequence::create(Spawn::create(heart(index),
                                                             NULL),
                                              DelayTime::create(2), NULL));
        }
        actions.pushBack(DelayTime::create(1));

        parent->runAction(Repeat::create(Sequence::create(actions), UINT_MAX));
    }
    else if(key=="wheel")
    {
        for (int i=0; i<4; i++) {
            auto pig=parent->getChildByName(StringUtils::format("sup_34_backpig_%d.png",i));
            auto seq=Sequence::create(DelayTime::create(.1*(i%3)),
                                      jumpAction(parent->getContentSize().height*.005, .2, pig, 1.2), NULL);
            pig->runAction(Repeat::create(seq, -1));
        }
        
        auto pig_0=parent->getChildByName("sup_34_pig_0.png");
        auto pig_1=createAnchorSprite("sup_34_pig_1.png", .5, 360.0/660, true, parent, true);
        
        Vector<FiniteTimeAction*>actions;
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        auto change=createChangeAction(1, .5, pig_0, pig_1,"pig.mp3");
        actions.pushBack(change);
        
        //初動
        actions.pushBack(swingAction(-10, .5, pig_1, 1.5));
        
        //回転
        std::vector<int>indexs={100,-100,145,-100};
        for (int i=0; i<indexs.size(); i++) {
            auto index=indexs.at(i);
            
            //大回転
            actions.pushBack(createSoundAction("rolling.mp3"));
            auto angle_180=180*pow(-1, i);
            auto duration=.3;
            auto ease_rotates=1.25;
            auto rotates=Repeat::create(Sequence::create(EaseOut::create(RotateBy::create(duration, angle_180), ease_rotates),
                                                         EaseIn::create(RotateBy::create(duration, angle_180), ease_rotates), NULL), 4);
            actions.pushBack(rotates);
            
            //停止まで
            auto rotate_stop= TargetedAction::create(pig_1, Sequence::create(EaseOut::create(RotateBy::create(duration, index), 2),
                                                                             DelayTime::create(.25),
                                                                             EaseIn::create(RotateBy::create(duration,-index), 1.25), NULL));
            
            actions.pushBack(rotate_stop);
        }
        
        //終わり
        actions.pushBack(swingAction(10, .7, pig_1, 1.5));
        actions.pushBack(delay->clone());
        
        actions.pushBack(createChangeAction(1, .5, pig_1, pig_0, "pig.mp3"));
        actions.pushBack(delay->clone());
        
        auto seq=TargetedAction::create(pig_1, Sequence::create(actions));
        parent->runAction(Repeat::create(seq, -1));
    }
    else if(key=="money"&&DataManager::sharedManager()->getEnableFlagWithFlagID(38)){
        auto pig_1=parent->getChildByName("sup_8_pig_1.png");
        std::vector<int>indexs={2,1,2,0,2};
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(1));

        for (auto index : indexs) {
            auto name=StringUtils::format("sup_8_dollar_%d.png",index);
            Node* dollar;
            if (parent->getChildByName(name)) {
                dollar=pig_1->getChildByName(name);
            }
            else{
                dollar=createSpriteToCenter(name, true, pig_1,true);
            }
            
            auto flash=TargetedAction::create(dollar, Sequence::create(FadeIn::create(.5),
                                                                       DelayTime::create(.5),
                                                                       FadeOut::create(.5), NULL));
            actions.pushBack(Spawn::create(createBounceAction(pig_1, .5, .05),
                                           createSoundAction("coin.mp3"),
                                           flash, NULL));
            actions.pushBack(DelayTime::create(1));
        }
        actions.pushBack(DelayTime::create(1));

        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
    else if(key=="underBed"&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(14)){
        auto size=parent->getContentSize().width*.1;
        auto pos=Vec2(parent->getContentSize().width*.5, parent->getContentSize().height*.5);
        if (backID==2) {
            size=parent->getContentSize().width*.06;
            pos=Vec2(parent->getContentSize().width*.5, parent->getContentSize().height*.22);
        }
        auto particle = ParticleSystemQuad::create("kirakira.plist");
        particle->setAutoRemoveOnFinish(true);
        particle->setPosition(pos);
        particle->setTotalParticles(12);
        particle->setStartSpinVar(10);
        particle->setLife(.5);
        particle->setLifeVar(.2);
        particle->setAngleVar(20);
        particle->setStartSize(size);
        particle->setEndColor(Color4F(0, 0, 0, .5));
        particle->setEndSize(particle->getStartSize()*1.2);
        particle->setEndSpin(15);
        particle->setEndSpinVar(15);
        particle->setPositionType(ParticleSystem::PositionType::RELATIVE);

        if (backID==2) {
            parent->addChild(particle);
        }
        else{
            parent->getChildByName("sup_21_coin.png")->addChild(particle);
        }
    }
    else if(key=="back_2"){
        backAction("flower", backID, parent);
    }
    else if(key=="back_1"){
        //バグ修正
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(6)&&
            !DataManager::sharedManager()->getEnableFlagWithFlagID(7)) {
            //鍵を持っていなければ
                auto find_itr=find(DataManager::sharedManager()->items.begin(), DataManager::sharedManager()->items.end(), 2);
                bool found= find_itr!=DataManager::sharedManager()->items.end();
            
            if (!found) {
                log("フラグ6を消去します");
                DataManager::sharedManager()->flagMap["6"]=false;
            }
        }
    }
}

#pragma mark - Touch
void BoyFriendActionManager::touchAction(std::string key, Node *parent, const onFinished &callback)
{
    if(key=="back_56"){
        //ギミック正解後 switch open
        Vector<FiniteTimeAction*>actions;
        
        int backID=0;
        
        std::string back;
        std::string door;
        std::string sup_name="";
        std::string soundName="gacha.mp3";
        bool removeDoor=false;
        bool playPinpon=true;
        
        if (key=="back_56") {
            back="back_50.png";
            backID=50;
            door="sup_50_0_1.png";
            sup_name="sup_50_c.png";
        }
        
        auto open=openDoorAction(parent, back, backID, door, sup_name, soundName,removeDoor);
        actions.pushBack(open);

        auto call=CallFunc::create([callback,playPinpon]{
            if (playPinpon) {
                Common::playSE("pinpon.mp3");
            }
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="key")
    {//鍵開け
        Common::playSE("p.mp3");
        auto back_num=DataManager::sharedManager()->getNowBack();
        auto item=createAnchorSprite(StringUtils::format("sup_%d_key_0.png",back_num), .5, .5, true, parent, true);
        auto item_after=createAnchorSprite(StringUtils::format("sup_%d_key_1.png",back_num), .5, .5, true, parent, true);
        
        auto seq=TargetedAction::create(item, Sequence::create(FadeIn::create(1),
                                                               createSoundAction("kacha.mp3"),
                                                                DelayTime::create(1),
                                                               createChangeAction(1, .5, item, item_after,"key_1.mp3"),
                                                               DelayTime::create(.5),
                                                                NULL));
        
        auto call=CallFunc::create([callback](){
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq,DelayTime::create(1),call, NULL));
    }
    else if(key=="putCorn")
    {//コンロにコーン
        Common::playSE("p.mp3");
        auto item=createAnchorSprite("sup_67_popcorn_0.png", .5, .5, true, parent, true);
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(TargetedAction::create(item, FadeIn::create(1)));
        actions.pushBack(createSoundAction("kacha.mp3"));
        actions.pushBack(DelayTime::create(1));
        
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(21)) {
            //popcorn取得まで
            auto seq=popCornAction(parent);
            actions.pushBack(seq);
        }
        
        auto call=CallFunc::create([callback,this,parent]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    
    else if(key=="conro"){
        //conro ON
        auto back=createSpriteToCenter("back_67.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 67,true);
        
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(TargetedAction::create(back, FadeIn::create(1)));
        actions.pushBack(DelayTime::create(1));
        
        /*auto particle=CallFunc::create([parent,back,this]{
            Common::playSE("huo.mp3");
            auto p=SupplementsManager::getInstance()->getParticle(parent, DataManager::sharedManager()->getParticleVector(false, 67).at(0).asValueMap());
            back->addChild(p);
        });*/
        //actions.pushBack(particle);
        //actions.pushBack(DelayTime::create(1));
        
        if (DataManager::sharedManager()->getEnableFlagWithFlagID(20)) {
            actions.pushBack(popCornAction(back));
        }

        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="cloud"){
        Vector<FiniteTimeAction*>actions_spawn;
        
        auto num=DataManager::sharedManager()->getNowBack();
        
        auto cloud_anchor=Vec2(322.0/600.0,440.0/660.0);
        if (num==42) {
            actions_spawn.pushBack(createSoundAction("pig.mp3"));
        }
        else if(num==24){
            cloud_anchor=Vec2(.7,.5);
            actions_spawn.pushBack(createSoundAction("pig.mp3"));
        }
        
        auto cloud=cloudAction(parent, StringUtils::format("sup_%d_cloud.png",num), cloud_anchor);
        actions_spawn.pushBack(cloud);
        
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(Spawn::create(actions_spawn),call, NULL));
    }
    else if(key=="birds")
    {//とりに餌
        Common::playSE("p.mp3");
        
        Vector<FiniteTimeAction*>actions;
        
        
        auto corn=createSpriteToCenter("sup_31_popcorn.png", true, parent,true);
        actions.pushBack(TargetedAction::create(corn, FadeIn::create(1)));
        actions.pushBack(createSoundAction("sa.mp3"));
        
        for (int i=0; i<3; i++) {
            actions.pushBack(DelayTime::create(1));

            auto bird=createSpriteToCenter(StringUtils::format("sup_31_bird_%d.png",i), true, parent,true);
            actions.pushBack(createSoundAction("bird.mp3"));
            actions.pushBack(TargetedAction::create(bird, FadeIn::create(1)));
        }
        actions.pushBack(DelayTime::create(1));

        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="bath")
    {//お湯が溜まる
        auto back=createSpriteToCenter("back_93.png", true, parent, true);
        auto fadein_back=TargetedAction::create(back, FadeIn::create(1));
        
        auto water=createSpriteToCenter("sup_93_water.png", true, parent,true);

        auto delay=DelayTime::create(1);
        
        auto fadein=TargetedAction::create(water, Spawn::create(FadeIn::create(2),
                                                                createSoundAction("jaa.mp3"),
                                                                
                                                                NULL));

        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_back,delay,fadein,delay->clone(),call, NULL));
    }
    else if(key=="towel")
    {//
        Common::playSE("p.mp3");
        
        auto mark=createSpriteToCenter("sup_91_hint.png", true, parent);
        parent->addChild(mark);
        
        auto item=createSpriteToCenter("sup_91_towel.png", true, parent);
        item->setOpacity(0);
        parent->addChild(item);
        
        //fadein
        auto fadein=FadeIn::create(1);
        auto sound=CallFunc::create([this]{
            Common::playSE("goshigoshi.mp3");
        });
        //move
        auto distance=parent->getContentSize().width*.025;
        auto duration=.2;
        Vector<FiniteTimeAction*>moves;
        for (int i=1; i<6; i++) {
            auto move=MoveBy::create(duration+i*.025, Vec2(distance*i*pow(-1, i)+distance*1*(i/2), distance*i*pow(-1, i)-distance*.2*(i/2)));
            moves.pushBack(move);
        }
        
        auto seq=TargetedAction::create(item, Sequence::create(fadein,sound,Sequence::create(moves), NULL));
        auto change=Spawn::create(TargetedAction::create(item, FadeOut::create(.7)),
                                  TargetedAction::create(mark, FadeIn::create(1)), NULL);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq,change,DelayTime::create(.5),call,NULL));
    }
    else if(key=="freezer_open")
    {
        if (DataManager::sharedManager()->getTemporaryFlag("freezer").asInt()==0) {
            auto particle=ParticleSystemQuad::create("smoke.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setPosition(parent->getContentSize().width/2,parent->getContentSize().height*.475);
            particle->setDuration(.2);
            particle->setStartSize(parent->getContentSize().width*.1);
            particle->setStartSizeVar(particle->getStartSize()*.2);
            particle->setEndSize(particle->getStartSize()*4);
            particle->setSpeed(parent->getContentSize().height*.075);
            particle->setSpeedVar(particle->getSpeed()*.2);
            particle->setGravity(Vec2(0, particle->getSpeed()*1));
            particle->setLife(2.5);
            particle->setTotalParticles(1500);
            particle->setPosVar(Vec2(parent->getContentSize().width*.275, parent->getContentSize().height*.24));
            particle->setAngle(270);
            particle->setAngleVar(5);
            parent->addChild(particle);
            
            DataManager::sharedManager()->setTemporaryFlag("freezer", 1);
        }
        
        if (DataManager::sharedManager()->isPlayingMinigame()) {
            
            auto mistake=parent->getChildByName("sup_70_mistake.png");
            mistake->setOpacity(255);
        }
        
        callback(true);
    }
    else if(key.compare(0,7,"printer")==0)
    {//紙を排出
        Vector<FiniteTimeAction*>actions;
        auto back=createSpriteToCenter("back_55.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(back, 55, true);
        actions.pushBack(TargetedAction::create(back, FadeIn::create(   1)));
        
        auto item=createSpriteToCenter(StringUtils::format("sup_55_paper.png"), true, back,true);
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        auto fadein_item=TargetedAction::create(item, Spawn::create(FadeIn::create(1),
                                                                         createSoundAction("emission.mp3"), NULL));
        actions.pushBack(fadein_item);
        
        
        actions.pushBack(delay->clone());
        auto call=CallFunc::create([callback,parent]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
    else if(key.compare(0,7,"battery")==0)
    {//電池はめ
        Common::playSE("p.mp3");
        
        Vector<FiniteTimeAction*>actions;
        
        auto index=atoi(key.substr(8,1).c_str());
        auto item=createSpriteToCenter(StringUtils::format("sup_12_battery_%d.png",index), true, parent,true);
        auto open=TargetedAction::create(item, Sequence::create(FadeIn::create(1),
                                                                createSoundAction("kacha.mp3"), NULL));
        actions.pushBack(open);
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        
        if(index==1){
            auto back=createSpriteToCenter("back_11.png", true, parent,true);
            SupplementsManager::getInstance()->addSupplementToMain(back, 11, true);
            actions.pushBack(TargetedAction::create(back, FadeIn::create(1)));
            actions.pushBack(delay->clone());
        }
        
        auto call=CallFunc::create([callback,parent]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(open,
                                           delay,
                                           call, NULL));
    }
    else if(key=="scissors")
    {//はさみ
        Common::playSE("p.mp3");
        
        auto item=createSpriteToCenter(StringUtils::format("sup_47_scissors.png"), true, parent,true);
        item->setLocalZOrder(1);
        auto fadein=TargetedAction::create(item, Sequence::create(FadeIn::create(1), NULL));
        auto delay=DelayTime::create(1);
        
        auto distance=-parent->getContentSize().width*.01;
        auto shake=jumpAction(Vec2(distance, distance), .1, item, 1.2, 4);
        auto fadeout_0=TargetedAction::create(parent->getChildByName("sup_47_tape_0.png"), FadeOut::create(1));
        auto spawn_0=Spawn::create(shake,fadeout_0,createSoundAction("chokichoki.mp3"), NULL);
        auto move=TargetedAction::create(item, MoveBy::create(1, Vec2(0, parent->getContentSize().height*.275)));
        auto fadeout_1=TargetedAction::create(parent->getChildByName("sup_47_tape_1.png"), FadeOut::create(1));
        auto spawn_1=Spawn::create(shake->clone(),fadeout_1,createSoundAction("chokichoki.mp3"), NULL);
        auto fadeout_item=TargetedAction::create(item, FadeOut::create(1));
        
        auto call=CallFunc::create([callback,parent]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        parent->runAction(Sequence::create(fadein,delay,spawn_0,delay->clone(),move,delay->clone(),spawn_1,fadeout_item,call, NULL));
    }
    else if(key=="openPoster")
    {//poster ひらく
        Vector<FiniteTimeAction*>actions;
        auto item=parent->getChildByName("sup_47_picture.png");
        auto open=createSpriteToCenter("sup_47_open.png", true, parent,true);
        createSpriteToCenter("sup_47_pig_0.png", false, open,true);
        
        auto openAction=createChangeAction(1, .5, item, open,"gacha.mp3");
        actions.pushBack(openAction);
        actions.pushBack(DelayTime::create(1));

        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="unlock_pc")
    {//pc クリア
        Vector<FiniteTimeAction*>actions;
        auto unlock=createSpriteToCenter("back_43.png", true, parent,true);
        SupplementsManager::getInstance()->addSupplementToMain(unlock, 43, true);
        auto fadein_0=TargetedAction::create(unlock, Sequence::create(FadeIn::create(1),
                                                                      NULL));
        actions.pushBack(fadein_0);
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="bar")
    {//ベッド下
        Common::playSE("p.mp3");
        auto coin=parent->getChildByName("sup_21_coin.png");
        auto bar=createAnchorSprite("sup_21_bar.png", .5, .8, true, parent, true);
       
        auto fadein_bar=TargetedAction::create(bar, FadeIn::create(1));
        auto replace=CallFunc::create([coin,bar]{
            coin->retain();
            coin->removeFromParent();
            bar->addChild(coin);
        });
        auto delay=DelayTime::create(1);
        auto move=TargetedAction::create(bar, Spawn::create(Sequence::create(DelayTime::create(1),FadeOut::create(2), NULL),
                                                            ScaleBy::create(3, 2),
                                                            MoveBy::create(3, Vec2(0, -parent->getContentSize().height*.3)), NULL));
        
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein_bar,delay,replace,move,delay->clone(),call, NULL));
    }
    
    else if(key=="coin")
    {//コインでドア開ける
        Common::playSE("p.mp3");
        Vector<FiniteTimeAction*>actions;
        auto coin=createSpriteToCenter("sup_62_coin_0.png", true, parent,true);
        auto coin_1=createSpriteToCenter("sup_62_coin_1.png", true, parent,true);

        auto fadein_0=TargetedAction::create(coin, FadeIn::create(1));
        actions.pushBack(fadein_0);
        actions.pushBack(DelayTime::create(1));
        auto fadein_1=Spawn::create(createChangeAction(1, .5, coin, coin_1,"key_1.mp3"), NULL);
        actions.pushBack(fadein_1);
        actions.pushBack(DelayTime::create(1));
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="hammer")
    {//
        Common::playSE("p.mp3");
        //
        auto axe=AnchorSprite::create(Vec2(.09, .6), parent->getContentSize(), true, "sup_50_hammer.png");
        parent->addChild(axe);
        
        auto pig=parent->getChildByName("sup_50_pig.png");
        auto coin=createSpriteToCenter("sup_50_coins.png", true, parent);
        parent->addChild(coin);
        
        Vector<FiniteTimeAction*>actions;
        auto fadein=FadeIn::create(1);
        auto moveup=Spawn::create(MoveBy::create(1.75, Vec2(0, parent->getContentSize().height*.2)),
                                  Sequence::create(EaseOut::create(RotateBy::create(.5, 15), 1.5),
                                                   EaseOut::create(RotateBy::create(1.25, -30), 1.5), NULL), NULL);
        auto movedown=Spawn::create(EaseIn::create(MoveBy::create(.2, Vec2(0, -parent->getContentSize().height*.2)), 1.5),
                                    EaseIn::create(RotateBy::create(.2, 50), 1.5), NULL);
        
        auto smoke=CallFunc::create([parent]{
            auto size=parent->getContentSize().width*.1;
            
            auto particle=ParticleSystemQuad::create("smoke.plist");
            particle->setAutoRemoveOnFinish(true);
            particle->setTotalParticles(200);
            particle->setPosition(parent->getContentSize().width*(.55),parent->getContentSize().height*.45);
            particle->setPosVar(Vec2(parent->getContentSize().width*.225,parent->getContentSize().height*.175));
            particle->setLife(2);
            particle->setDuration(.5);
            particle->resetSystem();
            particle->setStartSize(size);
            particle->setStartSizeVar(size*.5);
            particle->setEndSize(size*2);
            particle->setStartColor(Color4F(1, 1, 1, .6));
            particle->setSpeed(size*.5);
            particle->setGravity(Vec2(0, size*1));
            particle->setEndSpin(30);
            particle->setEndSpinVar(30);
            particle->setAngle(90);
            particle->setAngleVar(270);
            parent->addChild(particle);
        });
        
        auto remove_pig=createChangeAction(1, .5, pig, coin);
        auto fadeouts=Spawn::create(FadeOut::create(1),
                                    remove_pig,
                                    createSoundAction("stone-break.mp3"), NULL);
        
        auto seq_axe=TargetedAction::create(axe, Sequence::create(fadein,moveup,DelayTime::create(.5),movedown,smoke,fadeouts, NULL));
        actions.pushBack(seq_axe);
        actions.pushBack(DelayTime::create(2));
        
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="photo"){
        Common::playSE("p.mp3");
        auto clip=createClippingNodeToCenter("sup_57_clip.png", parent);
        auto photo=createSpriteToCenter("sup_57_photo.png", true, parent);
        clip->addChild(photo);
        
        auto gauge_0=createSpriteToCenter("sup_57_gauge_0.png", false, parent);
        clip->addChild(gauge_0);
        
        //auto shredder=parent->getChildByName("sup_57_shredder.png");
        auto gauge=createSpriteToCenter("sup_57_full.png", true, parent,true);
        
        
        auto seq_photo=TargetedAction::create(photo, Sequence::create(FadeIn::create(1),
                                                                      createSoundAction("printer_0.mp3"),
                                                                      Repeat::create(Sequence::create(MoveBy::create(.3, Vec2(0, -parent->getContentSize().height*.125)),
                                                                                                      DelayTime::create(.3), NULL), 3)
                                                                      , NULL));
        auto delay=DelayTime::create(1);
        auto move_gauge_0=TargetedAction::create(gauge_0, Spawn::create(MoveBy::create(2, Vec2(parent->getContentSize().width*.075, 0)),
                                                                        createSoundAction("pop.accend.mp3"),
                                                                        NULL));
        auto delay_1=DelayTime::create(.5);
        auto fadein_gauge=TargetedAction::create(gauge, Spawn::create(Repeat::create(Sequence::create(FadeOut::create(.25),FadeIn::create(.25), NULL), 3),
                                                                      createSoundAction("alarm.mp3"), NULL));
        
        auto call=CallFunc::create([callback]{
            Common::playSE("gatagata_1.mp3");
            callback(true);
        });
        
        parent->runAction(Sequence::create(seq_photo,delay,move_gauge_0,delay_1,fadein_gauge,delay->clone(),createSoundAction("pinpon.mp3"),delay_1->clone(),call,NULL));
    }
    else if(key.compare(0,4,"robo")==0){
        Common::playSE("pi.mp3");
        auto index=atoi(key.substr(5,1).c_str());
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(armAction(parent,index));
        
        auto call=CallFunc::create([callback]{
            //Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        parent->runAction(Sequence::create(actions));
    }
    else if (key=="money")
    {
        Common::playSE("p.mp3");
        auto spine=parent->getChildByName<spine::SkeletonAnimation*>("moneyPig");
        spine->setAnimation(0, "putMoney", false);
        spine->setTimeScale(.6);
        spine->setEventListener([](spTrackEntry* entry, spEvent* event){
            if (StringUtils::format("%s",event->data->name)=="money") {
                Common::playSE("coin.mp3");
            }
        });
        spine->setCompleteListener([parent,callback,spine,this](spTrackEntry*entry){
            auto delay=DelayTime::create(1);
            auto pig_0=createSpriteToCenter("sup_8_pig_0.png", true, parent, true);
            auto pig_1=createSpriteToCenter("sup_8_pig_1.png", true, parent, true);

            auto change=createChangeAction(1, .5, spine, pig_0,"pop_short.mp3");
            auto change_1=createChangeAction(1, .5, pig_0, pig_1,"pop_short.mp3");
            
            auto call=CallFunc::create([callback]{
                Common::playSE("pinpon.mp3");
                callback(true);
            });
            
            parent->runAction(Sequence::create(delay,
                                               change,
                                               delay->clone(),
                                               change_1,
                                               delay->clone(),
                                               call, NULL));
        });
    }
}

#pragma mark - Item
void BoyFriendActionManager::itemBackAction(std::string key, PopUpLayer *parent)
{
    if (key=="presentbox"&&
        DataManager::sharedManager()->getShowingAngle()==1) {
        auto eye=createSpriteToCenter("presentbox_eye.png", false, parent->itemImage, true);
        Vector<FiniteTimeAction*>actions;
        actions.pushBack(DelayTime::create(1));
        std::vector<int>indexs={0,3,0,2};
        auto distance=parent->itemImage->getContentSize().width*.04;
        for (auto index : indexs) {
            auto x=0;
            if (index==3) {
                x=-distance;
            }
            else if (index==1){
                x=distance;
            }
            
            auto y=0;
            if (index==0) {
                y=distance;
            }
            else if (index==2){
                y=-distance;
            }
            
            actions.pushBack(jumpAction(Vec2(x, y), 1, eye, 1.3));
        }
        actions.pushBack(DelayTime::create(1));

        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
}

void BoyFriendActionManager::itemAction(std::string key, PopUpLayer *parent, const onFinished &callback)
{//補完はparent/backImage/itemImage/supplementsの構造 addする際はcreateSpriteOnClipを使用するとよい
    if (key.compare(0,7,"battery")==0) {
        auto index=atoi(key.substr(8,1).c_str());
        auto item=createSpriteToCenter(StringUtils::format("switch_device_sup_%d.png",index), true, parent->itemImage,true);
        
        auto fadein=TargetedAction::create(item, Sequence::create(FadeIn::create(.5),
                                                                  createSoundAction("kacha.mp3"),
                                                                  DelayTime::create(1), NULL));
        auto call=CallFunc::create([callback]{
            callback(true);
        });
        
        parent->runAction(Sequence::create(fadein,call, NULL));
    }
    else if(key=="presentbox")
    {//はさみでカット
        Common::playSE("p.mp3");
        
        Vector<FiniteTimeAction*>actions;
        auto ribon_0=parent->itemImage->getChildByName("presentbox_ribon.png");
        auto ribon_1=parent->itemImage->getChildByName("presentbox_ribon_1.png");
        ribon_1->setLocalZOrder(1);

        auto item=createSpriteToCenter("presentbox_scissors.png", true, parent->itemImage,true);
        auto fadein_scissors=TargetedAction::create(item, FadeIn::create(1));
        actions.pushBack(fadein_scissors);
        
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        
        auto fadeout_ribons=Spawn::create(createSoundAction("chokichoki.mp3"),
                                          TargetedAction::create(ribon_0, FadeOut::create(1)),
                                          TargetedAction::create(ribon_1, FadeOut::create(1)), NULL);
        actions.pushBack(fadeout_ribons);
        actions.pushBack(TargetedAction::create(item, FadeOut::create(1)));
        actions.pushBack(delay->clone());

        auto remove_lid=TargetedAction::create(parent->itemImage->getChildByName("presentbox_lid.png"), FadeOut::create(1));
        actions.pushBack(Spawn::create(remove_lid,
                                       createSoundAction("pop_short.mp3"), NULL));
        
        //doll
        auto doll=createAnchorSprite("presentbox_doll.png", .5, .25, true, parent->itemImage, true);
        auto original_scale=doll->getScale();
        doll->setScaleY(.2);
        auto cover=createSpriteToCenter("presentbox_cover.png", true, parent->itemImage, true);
        actions.pushBack(TargetedAction::create(cover, FadeIn::create(.2)));
        actions.pushBack(TargetedAction::create(doll, FadeIn::create(.2)));
        actions.pushBack(createSoundAction("spring.mp3"));
        actions.pushBack(createBounceActionToTargetScale(doll, .3, .1, original_scale, 1.3, doll->getScale()));
        actions.pushBack(delay->clone());
        auto call=CallFunc::create([callback]{
            Common::playSE("pinpon.mp3");
            callback(true);
        });
        actions.pushBack(call);
        
        Director::getInstance()->getRunningScene()->runAction(Sequence::create(actions));
    }
    else if(key=="open_ring")
    {
        Vector<FiniteTimeAction*>actions;
        
        auto box=createSpriteOnClip("ringbox.png", true, parent, true);
        auto change=createChangeAction(1, .5, parent->itemImage, box);
        actions.pushBack(change);
        
        auto delay=DelayTime::create(1);
        actions.pushBack(delay);
        
        auto box_open=createSpriteOnClip("ringbox_open.png", true, parent, true);
        auto change_1=createChangeAction(1, .5, box, box_open);
        actions.pushBack(change_1);
        actions.pushBack(delay->clone());
        
        actions.pushBack(parent->fadeOutAll(1));
        actions.pushBack(delay->clone());

        auto call=CallFunc::create([parent]{
            //Common::playSE("pinpon.mp3");
            //callback(true);
            EscapeDataManager::getInstance()->playSound(DataManager::sharedManager()->getBgmName(true).c_str(), true);

            auto story=StoryLayer::create("ending", [parent]{
                Director::getInstance()->replaceScene(TransitionFade::create(8, ClearScene::createScene(), Color3B::WHITE));
            });
            Director::getInstance()->getRunningScene()->addChild(story);
        });
        actions.pushBack(call);
        parent->runAction(Sequence::create(actions));
    }
}

#pragma mark - Story
void BoyFriendActionManager::storyAction(std::string key, cocos2d::Node *parent, const onFinished &callback)
{
#pragma mark ending
    if (key=="ring") {
        auto ring=createSpriteOnStoryCutIn("finalbox.png", true, parent->getContentSize().width*.4, (StoryLayer*)parent);
        auto ring_1=createSpriteOnStoryCutIn("finalbox_open.png", true, parent->getContentSize().width*.4, (StoryLayer*)parent);

        parent->runAction(Sequence::create(DelayTime::create(1),
                                           createSoundAction("pop.mp3"),
                                           TargetedAction::create(ring, FadeIn::create(1)),
                                           DelayTime::create(1),
                                           createChangeAction(1, .5, ring, ring_1,"kacha.mp3"),
                                           DelayTime::create(1),
                                           CallFunc::create([callback]{
            callback(true);
        }), NULL));
    }
    else if(key=="changeScene"){
        Vector<FiniteTimeAction*>actions;
        if (DataManager::sharedManager()->getNowBack()!=1) {
            auto scene=((GameScene*)(Director::getInstance()->getRunningScene()->getChildren().at(1)));//0はCamera*が入ってるらしい
            scene->mainSprite->stopAllActions();
            auto back=createSpriteToCenter("back_1.png", true, scene->mainSprite, true);
            back->setLocalZOrder(2);
            SupplementsManager::getInstance()->addSupplementToMain(back, 1, true);
            
            actions.pushBack(TargetedAction::create(back, FadeIn::create(1)));
        }
        
        actions.pushBack(CallFunc::create([callback]{callback(true);}));
        
        parent->runAction(Sequence::create(actions));
    }
    else if(key=="door"){
        Vector<FiniteTimeAction*>actions;
        auto scene=((GameScene*)(Director::getInstance()->getRunningScene()->getChildren().at(1)));//0はCamera*が入ってるらしい
        auto door=createSpriteToCenter("sup_1_open.png", true, scene->mainSprite, true);
        door->setLocalZOrder(2);
        Common::playSE("gacha.mp3");
        actions.pushBack(TargetedAction::create(door, FadeIn::create(1)));
        actions.pushBack(CallFunc::create([callback]{callback(true);}));
        
        parent->runAction(Sequence::create(actions));
    }
#pragma mark opening
    else if(key=="run"){
        Vector<FiniteTimeAction*>actions;
        auto pig=parent->getChildByName("piggirl.png");
        
        actions.pushBack(TargetedAction::create(pig, Spawn::create(MoveTo::create(.5, Vec2(pig->getBoundingBox().size.width/2, pig->getPositionY())), NULL)));
        actions.pushBack(DelayTime::create(.5));
        actions.pushBack(CallFunc::create([callback]{callback(true);}));
        
        parent->runAction(Sequence::create(actions));
    }
    else{
        callback(true);
    }
}

#pragma mark - Answer
void BoyFriendActionManager::showAnswerAction(std::string key, Node *parent)
{
    if (key=="pc") {
        Vector<FiniteTimeAction*>actions;
        auto backMap=DataManager::sharedManager()->getBackData(43);
        auto answerMap=DataManager::sharedManager()->getCustomSupplementsMap(CustomActionNameOnBack_Input, backMap)["answer"].asValueMap();
        auto answer=answerMap["answer"].asString();
        actions.pushBack(DelayTime::create(.3));
        for (int i=0;i<answer.size();i++) {
            auto index=atoi(answer.substr(i,1).c_str());
            actions.pushBack(Spawn::create(createSoundAction("pi.mp3"),
                                           armAction(parent, index), NULL));
            actions.pushBack(DelayTime::create(.3));
        }
        
        parent->runAction(Repeat::create(Sequence::create(actions), -1));
    }
}

#pragma mark - Custom
void BoyFriendActionManager::customAction(std::string key, cocos2d::Node *parent)
{
    
}

#pragma mark - Private
FiniteTimeAction* BoyFriendActionManager::armAction(Node*parent,int index)
{
    auto arm=parent->getChildByName(StringUtils::format("sup_43_arm_%d.png",index));
    auto angle=120*pow(-1, index);
    return swingAction(angle, .3, arm, 1.5);
}


FiniteTimeAction* BoyFriendActionManager::popCornAction(Node *parent)
{
    AnswerManager::getInstance()->enFlagIDs.push_back(22);
    AnswerManager::getInstance()->getItemIDs.push_back(10);
    
    auto popcorn_0=parent->getChildByName<Sprite*>("sup_67_popcorn_0.png");
    auto popcorn_1=createSpriteToCenter("sup_67_popcorn_1.png", true, popcorn_0,true);
    popcorn_1->setLocalZOrder(popcorn_0->getLocalZOrder());

    auto shake=jumpAction(parent->getContentSize().height*.02, .1, popcorn_0, 1.2, 6);
    auto change=TargetedAction::create(popcorn_1, Spawn::create(createSoundAction("pop_short.mp3"),
                                                                FadeIn::create(1), NULL));
    
    return Sequence::create(shake,DelayTime::create(1),change,DelayTime::create(1), NULL);
}
