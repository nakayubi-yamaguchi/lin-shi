//
//  TempleActionManager.h
//  EscapeContainer
//
//  Created by 成田凌平 on 2017/10/03.
//
//

#ifndef __EscapeContainer__GhostHouseActionManager__
#define __EscapeContainer__GhostHouseActionManager__

#include "cocos2d.h"
#include "CustomActionManager.h"

USING_NS_CC;

class GhostHouseActionManager:public CustomActionManager
{
public:
    static GhostHouseActionManager* manager;
    static GhostHouseActionManager* getInstance();
    
    void backAction(std::string key,int backID,Node*parent);
    void touchAction(std::string key,Node*parent,const onFinished& callback);
    void itemAction(std::string key,PopUpLayer*parent,const onFinished& callback);
    void customAction(std::string key,Node*parent);

private:
    void skullAction(Node*parent,int backID,bool animate,const onFinished&callback);
    void photoAction(Node*parent,int backID,bool animate,const onFinished&callback);
    ParticleSystemQuad* getFireParticle(cocos2d::Node *parent,int backID);
};

#endif /* defined(__EscapeContainer__TempleActionManager__) */
