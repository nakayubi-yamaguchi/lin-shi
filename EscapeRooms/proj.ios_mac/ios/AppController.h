#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>


@class RootViewController;

@interface AppController : NSObject <UIApplicationDelegate> {

}

@property(nonatomic, readonly) RootViewController* viewController;

@end

