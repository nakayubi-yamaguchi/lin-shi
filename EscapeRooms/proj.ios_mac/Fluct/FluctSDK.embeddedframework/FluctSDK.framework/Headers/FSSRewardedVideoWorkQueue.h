//
//  FSSRewardedVideoWorkQueue.h
//  FluctSDK
//
//  Copyright © 2017年 fluct, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

extern dispatch_queue_t FSSRewardedVideoWorkQueue(void);

NS_ASSUME_NONNULL_END
