//
//  FSSNativeTableViewCell.h
//  FluctSDK
//
//  Copyright © 2017年 fluct, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FSSNativeTableViewCell : UITableViewCell
- (void)loadAdWithGroupID:(NSString *)groupID unitID:(NSString *)unitID;
@end
