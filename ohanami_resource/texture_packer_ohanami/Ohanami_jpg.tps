<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.3.1</string>
        <key>fileName</key>
        <string>/Users/nakayubi/Desktop/amusementpark_resource_1/texture_packer_amusementpark/AmusementPark_jpg.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">jpg</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../atlas_j{n}.plist</filename>
            </struct>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGB888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>10</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">back/back_1.png</key>
            <key type="filename">back/back_10.png</key>
            <key type="filename">back/back_11.png</key>
            <key type="filename">back/back_12.png</key>
            <key type="filename">back/back_13.png</key>
            <key type="filename">back/back_14.png</key>
            <key type="filename">back/back_15.png</key>
            <key type="filename">back/back_16.png</key>
            <key type="filename">back/back_18.png</key>
            <key type="filename">back/back_19.png</key>
            <key type="filename">back/back_2.png</key>
            <key type="filename">back/back_23.png</key>
            <key type="filename">back/back_24.png</key>
            <key type="filename">back/back_25.png</key>
            <key type="filename">back/back_26.png</key>
            <key type="filename">back/back_27.png</key>
            <key type="filename">back/back_28.png</key>
            <key type="filename">back/back_29.png</key>
            <key type="filename">back/back_3.png</key>
            <key type="filename">back/back_30.png</key>
            <key type="filename">back/back_31.png</key>
            <key type="filename">back/back_32.png</key>
            <key type="filename">back/back_33.png</key>
            <key type="filename">back/back_34.png</key>
            <key type="filename">back/back_35.png</key>
            <key type="filename">back/back_4.png</key>
            <key type="filename">back/back_40.png</key>
            <key type="filename">back/back_41.png</key>
            <key type="filename">back/back_42.png</key>
            <key type="filename">back/back_43.png</key>
            <key type="filename">back/back_44.png</key>
            <key type="filename">back/back_46.png</key>
            <key type="filename">back/back_47.png</key>
            <key type="filename">back/back_48.png</key>
            <key type="filename">back/back_49.png</key>
            <key type="filename">back/back_5.png</key>
            <key type="filename">back/back_50.png</key>
            <key type="filename">back/back_51.png</key>
            <key type="filename">back/back_52.png</key>
            <key type="filename">back/back_53.png</key>
            <key type="filename">back/back_55.png</key>
            <key type="filename">back/back_56.png</key>
            <key type="filename">back/back_6.png</key>
            <key type="filename">back/back_60.png</key>
            <key type="filename">back/back_61.png</key>
            <key type="filename">back/back_62.png</key>
            <key type="filename">back/back_63.png</key>
            <key type="filename">back/back_64.png</key>
            <key type="filename">back/back_65.png</key>
            <key type="filename">back/back_66.png</key>
            <key type="filename">back/back_67.png</key>
            <key type="filename">back/back_68.png</key>
            <key type="filename">back/back_69.png</key>
            <key type="filename">back/back_7.png</key>
            <key type="filename">back/back_70.png</key>
            <key type="filename">back/back_71.png</key>
            <key type="filename">back/back_72.png</key>
            <key type="filename">back/back_75.png</key>
            <key type="filename">back/back_76.png</key>
            <key type="filename">back/back_77.png</key>
            <key type="filename">back/back_78.png</key>
            <key type="filename">back/back_79.png</key>
            <key type="filename">back/back_8.png</key>
            <key type="filename">back/back_80.png</key>
            <key type="filename">back/back_81.png</key>
            <key type="filename">back/back_9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>150,165,300,330</rect>
                <key>scale9Paddings</key>
                <rect>150,165,300,330</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">etc_jpg/room_image.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>281,500,563,1000</rect>
                <key>scale9Paddings</key>
                <rect>281,500,563,1000</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>back</filename>
            <filename>etc_jpg</filename>
            <filename>supplements_jpg</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
